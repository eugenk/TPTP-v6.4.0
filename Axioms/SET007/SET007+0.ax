%------------------------------------------------------------------------------
% File     : SET007+0 : TPTP v6.4.0. Released v3.4.0.
% Domain   : Set Theory
% Axioms   : Mizar Built-in Notions
% Version  : [Urb08] axioms.
% English  :

% Refs     : [Mat90] Matuszewski (1990), Formalized Mathematics
%          : [Urb07] Urban (2007), MPTP 0.2: Design, Implementation, and In
%          : [Urb08] Urban (2006), Email to G. Sutcliffe
% Source   : [Urb08]
% Names    : hidden [Urb08]

% Status   : Satisfiable
% Syntax   : Number of formulae    :    1 (   0 unit)
%            Number of atoms       :    2 (   0 equality)
%            Maximal formula depth :    5 (   5 average)
%            Number of connectives :    2 (   1 ~  ;   0  |;   0  &)
%                                         (   0 <=>;   1 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    0 (   0 constant; --- arity)
%            Number of variables   :    2 (   0 singleton;   2 !;   0 ?)
%            Maximal term depth    :    1 (   1 average)
% SPC      : 

% Comments : The individual reference can be found in [Mat90] by looking for
%            the name provided by [Urb08].
%          : Translated by MPTP from the Mizar Mathematical Library 4.48.930.
%          : These set theory axioms are used in encodings of problems in
%            various domains, including ALG, CAT, GRP, LAT, SET, and TOP.
%------------------------------------------------------------------------------
fof(antisymmetry_r2_hidden,axiom,(
    ! [A,B] :
      ( r2_hidden(A,B)
     => ~ r2_hidden(B,A) ) )).
%------------------------------------------------------------------------------
