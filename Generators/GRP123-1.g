%--------------------------------------------------------------------------
% File     : GRP123-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (3,2,1) conjugate orthogonality
% Version  : [Sla93] axioms. 
% English  : If ab=xy and a*b = x*y then a=x and b=y, where c*b=a iff ab=c.
%            Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [Zha94] Zhang (1994), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [Sla93]
% Names    : QG1 [Sla93]
%          : QG1 [FSB93]
%          : QG1 [SFS95]
%          : Bennett QG1 [TPTP]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [Sla93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : [Zha94] has pointed out that either one of qg1_1
%            or qg1_2 may be used, as each implies the other in this
%            scenario, with the help of cancellation. The dependence
%            cannot be proved, so both have been left in here.
%--------------------------------------------------------------------------
%----Inequality of any two elements in a list, modulo symmetry
'GRP123-1_inequality_clause'(Elements,input_clause(Name,axiom,
[--equalish(FirstElement,OtherElement)])):-
    tptp2X_select(FirstElement,Elements,OtherElements),
    tptp2X_member(OtherElement,OtherElements),
    concatenate_atoms([FirstElement,'_is_not_',OtherElement],Name).
%--------------------------------------------------------------------------
%----Clauses to establish the existence and uniqueness of group elements
'GRP123-1_qg_element_clauses'(Size,UniqueElementClauses):-
    generate_terms(1,Size,e,Elements),
    add_each_argument_to_literal(++group_element,Elements,
GroupElementLiterals),
    findall([ElementLiteral],tptp2X_member(ElementLiteral,
GroupElementLiterals),GroupElementLiteralSets),
    make_input_clauses(GroupElementLiteralSets,element,1,axiom,
GroupElementClauses),
    findall(ElementInequalityClause,'GRP123-1_inequality_clause'(Elements,
ElementInequalityClause),ElementInequalityClauses),
    tptp2X_append(GroupElementClauses,ElementInequalityClauses,
UniqueElementClauses).
%--------------------------------------------------------------------------
'GRP123-1_very_standard_qg_clauses'(Size,Operator,[
input_clause(TotalFunction1Name,axiom,
    [--group_element('X'),
     --group_element('Y')|
     ProductLiterals]),
input_clause(TotalFunction2Name,axiom,
    [--XYWAtom,
     --XYZAtom,
     ++equalish('W','Z')]),
input_clause(RightCancellationName,axiom,
    [--XWYAtom,
     --XZYAtom,
     ++equalish('W','Z')]),
input_clause(LeftCancellationName,axiom,
    [--WYXAtom,
     --ZYXAtom,
     ++equalish('W','Z')])
]):-
%----Total function 1
    generate_terms(1,Size,e,Elements),
    OperatorAtom =.. [Operator,'X','Y'],
    add_each_argument_to_literal(++OperatorAtom,Elements,
ProductLiterals),
    concatenate_atoms([Operator,'_total_function1'],TotalFunction1Name),
%----Total function 2
    XYWAtom =.. [Operator,'X','Y','W'],
    XYZAtom =.. [Operator,'X','Y','Z'],
    concatenate_atoms([Operator,'_total_function2'],TotalFunction2Name),
%----Right cancellation
    XWYAtom =.. [Operator,'X','W','Y'],
    XZYAtom =.. [Operator,'X','Z','Y'],
    concatenate_atoms([Operator,'_right_cancellation'],RightCancellationName),
%----Left cancellation
    WYXAtom =.. [Operator,'W','Y','X'],
    ZYXAtom =.. [Operator,'Z','Y','X'],
    concatenate_atoms([Operator,'_left_cancellation'],LeftCancellationName).
%--------------------------------------------------------------------------
'GRP123-1_standard_qg_clauses'(Size,Operator,Clauses,no):-
    'GRP123-1_very_standard_qg_clauses'(Size,Operator,Clauses).

'GRP123-1_standard_qg_clauses'(Size,Operator,Clauses,yes):-
    'GRP123-1_very_standard_qg_clauses'(Size,Operator,VeryStandardClauses),
%----Idempotence
    IdempotenceAtom =.. [Operator,'X','X','X'],
    concatenate_atoms([Operator,'_idempotence'],IdempotenceName),
    tptp2X_append(VeryStandardClauses,[input_clause(IdempotenceName,
axiom,[++IdempotenceAtom])],Clauses).
%--------------------------------------------------------------------------
%----The status of this problem
'GRP123-1_status'(Size,unsatisfiable):-
    tptp2X_member(Size,[2,3,6]),
    !.

'GRP123-1_status'(_,satisfiable):-
    !.
%--------------------------------------------------------------------------
'GRP123-1'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1_qg_element_clauses'(Size,ElementClauses),
    'GRP123-1_standard_qg_clauses'(Size,product,StandardClauses,yes),
    tptp2X_append(ElementClauses,StandardClauses,AxiomClauses),
    tptp2X_append(AxiomClauses,[
input_clause(qg1_1,conjecture,
    [--product('X1','Y1','Z1'),
     --product('X2','Y2','Z1'),
     --product('Z2','Y1','X1'),
     --product('Z2','Y2','X2'),
     ++equalish('X1','X2')]),
input_clause(qg1_2,conjecture,
    [--product('X1','Y1','Z1'),
     --product('X2','Y2','Z1'),
     --product('Z2','Y1','X1'),
     --product('Z2','Y2','X2'),
     ++equalish('Y1','Y2')])],Clauses),
    'GRP123-1_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the GRP123-1 generator
'GRP123-1_file_information'(generator,sizes(X,(X>=1)),sota(3,5)).
%----Unsatisfiable size 3 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
