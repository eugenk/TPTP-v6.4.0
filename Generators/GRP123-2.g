%--------------------------------------------------------------------------
% File     : GRP123-2.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (3,2,1) conjugate orthogonality
% Version  : [Sla93] axioms : Augmented. 
% English  : If ab=xy and a*b = x*y then a=x and b=y, where c*b=a iff ab=c.
%            Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [Zha94] Zhang (1994), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [TPTP]
% Names    : 

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : [Zha94] has pointed out that either one of qg1_1
%            or qg1_2 may be used, as each implies the other in this
%            scenario, with the help of cancellation. The dependence
%            cannot be proved, so both have been left in here.
%          : Version 2 has simple isomorphism avoidance (as mentioned in 
%            [FSB93])
%--------------------------------------------------------------------------
%----Include basic QG code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----Clauses which says which element comes next
'GRP123-2_next_clause'([E1,E2|_],input_clause(Name,axiom,
[++next(E1,E2)])):-
    concatenate_atoms([E1,'_then_',E2],Name).

'GRP123-2_next_clause'([_|RestOfElements],Clause):-
    'GRP123-2_next_clause'(RestOfElements,Clause).
%--------------------------------------------------------------------------
%----Clauses which say an element is greater than another
'GRP123-2_greater_clause'([E1|RestOfElements],input_clause(Name,axiom,
[++greater(E2,E1)])):-
    tptp2X_member(E2,RestOfElements),
    concatenate_atoms([E2,'_greater_',E1],Name).

'GRP123-2_greater_clause'([_|RestOfElements],Clause):-
    'GRP123-2_greater_clause'(RestOfElements,Clause).
%--------------------------------------------------------------------------
%----Clauses that determine order between the elements, for isomorphism 
%----avoidance.
'GRP123-2_qg_simple_redundancy_clauses'(Size,Operator,RedundancyClauses):-
    generate_terms(1,Size,e,[FirstElement|RestOfElements]),
    findall(NextClause,
        'GRP123-2_next_clause'([FirstElement|RestOfElements],NextClause),
            NextClauses),
    findall(GreaterClause,
        'GRP123-2_greater_clause'([FirstElement|RestOfElements],
GreaterClause),
            GreaterClauses),
    tptp2X_append(NextClauses,GreaterClauses,OrderClauses),
    OperatorAtom =.. [Operator,'X',FirstElement,'Y'],
    tptp2X_append(OrderClauses,[input_clause(no_redundancy,axiom,
[--OperatorAtom,--next('X','X1'),--greater('Y','X1')])],RedundancyClauses).
%--------------------------------------------------------------------------
'GRP123-2'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1'(Size,BasicClauses,Status),
%----Create any extra clauses required
    'GRP123-2_qg_simple_redundancy_clauses'(Size,product,ExtraClauses),
    tptp2X_append(ExtraClauses,BasicClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the GRP123-2 generator
'GRP123-2_file_information'(generator,sizes(X,(X>=1)),sota(3,5)).
%----Unsatisfiable size 3 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
