%--------------------------------------------------------------------------
% File     : GRP123-3.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (3,2,1) conjugate orthogonality
% Version  : [Sla93] axioms : Augmented. 
% English  : If ab=xy and a*b = x*y then a=x and b=y, where c*b=a iff ab=c.
%            Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [Zha94] Zhang (1994), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [TPTP]
% Names    : 

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : [Zha94] has pointed out that either one of qg1_1
%            or qg1_2 may be used, as each implies the other in this
%            scenario, with the help of cancellation. The dependence
%            cannot be proved, so both have been left in here.
%          : Version 3 has complex isomorphism avoidance (mentioned in 
%            [SFS95]
%--------------------------------------------------------------------------
%----Include basic QG code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----Clauses which says which element comes next
'GRP123-3_next_clause'([E1,E2|_],input_clause(Name,axiom,
[++next(E1,E2)])):-
    concatenate_atoms([E1,'_then_',E2],Name).

'GRP123-3_next_clause'([_|RestOfElements],Clause):-
    'GRP123-3_next_clause'(RestOfElements,Clause).
%--------------------------------------------------------------------------
%----Clauses which say an element is greater than another
'GRP123-3_greater_clause'([E1|RestOfElements],input_clause(Name,axiom,
[++greater(E2,E1)])):-
    tptp2X_member(E2,RestOfElements),
    concatenate_atoms([E2,'_greater_',E1],Name).

'GRP123-3_greater_clause'([_|RestOfElements],Clause):-
    'GRP123-3_greater_clause'(RestOfElements,Clause).
%--------------------------------------------------------------------------
%----Clauses that forces cycles to occur in monotone increasing order of
%----length. Clauses specified by Slaney, and I don't understand. Using
%----the domain for integers, because I have ordering clauses already.
'GRP123-3_qg_complex_redundancy_clauses'(Size,Operator,RedundancyClauses):-
%----Make elements, and one extra below
    generate_terms(0,Size,e,[ZeroElement,FirstElement|RestOfElements]),
%----Make the ordering clauses
    findall(NextClause,
        'GRP123-3_next_clause'([ZeroElement,FirstElement|RestOfElements],
NextClause),
            NextClauses),
    findall(GreaterClause,
        'GRP123-3_greater_clause'([ZeroElement,FirstElement|RestOfElements],
GreaterClause),
            GreaterClauses),
    tptp2X_append(NextClauses,GreaterClauses,OrderClauses),
%----Get the last element
    tptp2X_append(FrontElements,[LastElement],
[ZeroElement,FirstElement|RestOfElements]),
%----Make the positive cycle clause literals and atoms
    add_each_argument_to_literal(++cycle('X'),FrontElements,
PositiveCycleLiterals),
    OperatorAtom1 =.. [Operator,'X',FirstElement,'Y'],
    OperatorAtom2 =.. [Operator,'X',FirstElement,'Z'],
%----Put together all the clauses
    tptp2X_append(OrderClauses,
[
input_clause(cycle1,axiom,
    [--cycle('X','Y'),
     --cycle('X','Z'),
     ++equalish('Y','Z')]),
input_clause(cycle2,axiom,
    [--group_element('X')|PositiveCycleLiterals]),
input_clause(cycle3,axiom,
    [++cycle(LastElement,ZeroElement)]),
input_clause(cycle4,axiom,
    [--cycle('X','Y'),
     --cycle('W','Z'),
     --next('X','W'),
     --greater('Y',ZeroElement),
     --next('Z','Z1'),
     ++equalish('Y','Z1')]),
input_clause(cycle5,axiom,
    [--cycle('X','Z1'),
     --cycle('Y',ZeroElement),
     --cycle('W','Z2'),
     --next('Y','W'),
     --greater('Y','X'),
     --greater('Z1','Z2')]),
input_clause(cycle6,axiom,
    [--cycle('X',ZeroElement),
     --OperatorAtom1,
     --greater('Y','X')]),
input_clause(cycle7,axiom,
    [--cycle('X','Y'),
     --OperatorAtom2,
     --greater('Y',ZeroElement),
     --next('X','X1'),
     ++equalish('Z','X1')])
],
RedundancyClauses).
%--------------------------------------------------------------------------
'GRP123-3'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1'(Size,BasicClauses,Status),
    'GRP123-3_qg_complex_redundancy_clauses'(Size,product,ExtraClauses),
    tptp2X_append(ExtraClauses,BasicClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the GRP123-3 generator
'GRP123-3_file_information'(generator,sizes(X,(X>=1)),sota(3,4)).
%----Unsatisfiable size 3 (Otter and SPASS)
%----Satisfiable size 4 (Otter)
%--------------------------------------------------------------------------
