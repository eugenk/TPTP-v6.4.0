%--------------------------------------------------------------------------
% File     : GRP123-4.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Group Theory (Quasigroups)
% Problem  : (3,2,1) conjugate orthogonality
% Version  : [Sla93] axioms : Augmented. 
% English  : If ab=xy and a*b = x*y then a=x and b=y, where c*b=a iff ab=c.
%            Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [Zha94] Zhang (1994), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [TPTP]
% Names    : 

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : [Zha94] has pointed out that either one of qg1_1
%            or qg1_2 may be used, as each implies the other in this
%            scenario, with the help of cancellation. The dependence
%            cannot be proved, so both have been left in here.
%          : Version 4 has surjectivity and rotation
% Bugfixes : v1.2.1 - Clauses row_surjectivity and column_surjectivity fixed.
%--------------------------------------------------------------------------
%----Include basic QG code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----Do rotations of the condition clause
'GRP123-4_rotation'(input_clause(Name,Status,Literals),input_clause(NewName,
Status,[++NegativeAtom,--PositiveAtom|OtherNegativeLiterals])):-
    tptp2X_select(++PositiveAtom,Literals,NegativeLiterals),
    tptp2X_length(NegativeLiterals,NumberOfNegativeLiterals),
    tptp2X_integer_in_range(1,NumberOfNegativeLiterals,NegativeSelection),
    tptp2X_select_Nth(--NegativeAtom,NegativeLiterals,NegativeSelection,
OtherNegativeLiterals),
    concatenate_atoms([Name,'_',NegativeSelection],NewName).
%--------------------------------------------------------------------------
%----Rotatable if all negative operator literals
'GRP123-4_rotatable'([],_).

'GRP123-4_rotatable'([--Atom|RestOfLiterals],Operator):-
    Atom =.. [Operator|_],
    'GRP123-4_rotatable'(RestOfLiterals,Operator).
%--------------------------------------------------------------------------
%----Make rotations of the condition clauses
'GRP123-4_rotated_clauses'(BasicClauses,Operator,RotatedClauses):-
    findall(RotatedClause,(
%----Find a genuine rotatable conjecture clause
        tptp2X_member(input_clause(Name,conjecture,Literals),BasicClauses),
        PositiveOperatorAtom =.. [Operator,_,_,_],
        tptp2X_select(++PositiveOperatorAtom,Literals,OtherLiterals),
        'GRP123-4_rotatable'(OtherLiterals,Operator),
        'GRP123-4_rotation'(input_clause(Name,conjecture,Literals),
RotatedClause)),
            RotatedClauses).
%--------------------------------------------------------------------------
%----Extra surjectivity clauses
'GRP123-4_surjectivity_clauses'(Size,[
input_clause(row_surjectivity,axiom,
    [--group_element('X'),--group_element('Y')|RowProductLiterals]),
input_clause(column_surjectivity,axiom,
    [--group_element('X'),--group_element('Y')|ColumnProductLiterals])
]):-
    generate_terms(1,Size,e,Elements),
    findall(++product(Element,'X','Y'),tptp2X_member(Element,Elements),
RowProductLiterals),
    findall(++product('X',Element,'Y'),tptp2X_member(Element,Elements),
ColumnProductLiterals).
%--------------------------------------------------------------------------
'GRP123-4'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1'(Size,BasicClauses,Status),
    'GRP123-4_surjectivity_clauses'(Size,SurjectivityClauses),
    'GRP123-4_rotated_clauses'(BasicClauses,product,RotatedClauses),
    tptp2X_append(SurjectivityClauses,RotatedClauses,ExtraClauses),
    tptp2X_append(ExtraClauses,BasicClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the GRP123-4 generator
'GRP123-4_file_information'(generator,sizes(X,(X>=1)),sota(3,4)).
%----Unsatisfiable size 3 (Otter and SPASS)
%----Satisfiable size 4 (Otter)
%--------------------------------------------------------------------------
