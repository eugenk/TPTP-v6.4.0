%--------------------------------------------------------------------------
% File     : GRP123-6.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (3,2,1) conjugate orthogonality
% Version  : [Sla93] axioms. 
%            Theorem formulation : Uses a second group.
% English  : If ab=xy and a*b = x*y then a=x and b=y, where c*b=a iff ab=c
%            Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [Sla93]
% Names    : QG1a [Sla93]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----The status of this problem (including some open!)
'GRP123-6_status'(Size,Status):-
    'GRP123-1_status'(Size,Status).
%--------------------------------------------------------------------------
'GRP123-6'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1_qg_element_clauses'(Size,ElementClauses),
    'GRP123-1_standard_qg_clauses'(Size,product1,StandardClauses1,yes),
    'GRP123-1_standard_qg_clauses'(Size,product2,StandardClauses2,yes),
    tptp2X_append(ElementClauses,StandardClauses1,Clauses1),
    tptp2X_append(Clauses1,StandardClauses2,AxiomClauses),
    tptp2X_append(AxiomClauses,[
input_clause(qg1a,conjecture,
    [--product1('X','Y','Z1'),
     --product1('Z1','Y','Z2'),
     ++product2('Z2','X','Y')])],Clauses),
    'GRP123-6_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the GRP123-6 generator
'GRP123-6_file_information'(generator,sizes(X,(X>=1)),sota(3,5)).
%----Unsatisfiable size 3 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
