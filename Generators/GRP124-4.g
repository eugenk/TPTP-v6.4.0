%--------------------------------------------------------------------------
% File     : GRP124-4.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Group Theory (Quasigroups)
% Problem  : (3,1,2) conjugate orthogonality
% Version  : [Sla93] axioms : Augmented. 
% English  : If ab=xy and a*b = x*y then a=x and b=y, where c*a=b iff ab=c.
%            Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [TPTP]
% Names    : 

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : As in GRP123-1, either one of qg2_1 or qg2_2 may be used, as 
%            each implies the other in this scenario, with the help of 
%            cancellation. The dependence cannot be proved, so both have 
%            been left in here.
%          : Version 4 has surjectivity and rotation
% Bugfixes : v1.2.1 - Clauses row_surjectivity and column_surjectivity fixed.
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP124-1.g').
%----Include redundancy code
:-tptp2X_include('Generators/GRP123-4.g').
%--------------------------------------------------------------------------
'GRP124-4'(Size,Clauses,Status):-
    integer(Size),
    'GRP124-1'(Size,BasicClauses,Status),
    'GRP123-4_surjectivity_clauses'(Size,SurjectivityClauses),
    'GRP123-4_rotated_clauses'(BasicClauses,product,RotatedClauses),
    tptp2X_append(SurjectivityClauses,RotatedClauses,ExtraClauses),
    tptp2X_append(ExtraClauses,BasicClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the GRP124-4 generator
'GRP124-4_file_information'(generator,sizes(X,(X>=1)),sota(4,5)).
%----Unsatisfiable size 4 (Otter and SPASS)
%----Satisfiable size 5 ()
%--------------------------------------------------------------------------
