%--------------------------------------------------------------------------
% File     : GRP125-3.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (a.b).(b.a) = a
% Version  : [Sla93] axioms : Augmented. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [TPTP]
% Names    : 

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : Version 3 has complex isomorphism avoidance (mentioned in
%            [SFS95]
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP125-1.g').
%----Include redundancy code
:-tptp2X_include('Generators/GRP123-3.g').
%--------------------------------------------------------------------------
'GRP125-3'(Size,Clauses,Status):-
    integer(Size),
    'GRP125-1'(Size,BasicClauses,Status),
    'GRP123-3_qg_complex_redundancy_clauses'(Size,product,ExtraClauses),
    tptp2X_append(ExtraClauses,BasicClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the GRP125-3 generator
'GRP125-3_file_information'(generator,sizes(X,(X>=1)),sota(5,4)).
%----Unsatisfiable size 5 (SPASS)
%----Satisfiable size 4 (Otter)
%--------------------------------------------------------------------------
