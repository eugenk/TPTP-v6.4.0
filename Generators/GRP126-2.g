%--------------------------------------------------------------------------
% File     : GRP126-2.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (a.b).(b.a) = b
% Version  : [Sla93] axioms : Augmented. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [TPTP]
% Names    : 

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : Version 2 has simple isomorphism avoidance (as mentioned in
%            [FSB93])
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP126-1.g').
%----Include redundancy code
:-tptp2X_include('Generators/GRP123-2.g').
%--------------------------------------------------------------------------
'GRP126-2'(Size,Clauses,Status):-
    integer(Size),
    Size >= 3,
    'GRP126-1'(Size,BasicClauses,Status),
%----Create any extra clauses required
    'GRP123-2_qg_simple_redundancy_clauses'(Size,product,ExtraClauses),
    tptp2X_append(ExtraClauses,BasicClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the GRP126-2 generator
'GRP126-2_file_information'(generator,sizes(X,(X>=3)),sota(4,5)).
%----Unsatisfiable size 4 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
