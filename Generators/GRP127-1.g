%--------------------------------------------------------------------------
% File     : GRP127-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : ((b.a).b).b) = a
% Version  : [Sla93] axioms. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [Ben89] Bennett (1989), Quasigroup Identities and Mendelsohn D
%          : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [Sla93]
% Names    : QG5 [Sla93]
%          : QG5 [FSB93]
%          : QG5 [SFS95]
%          : Bennett QG5 [TPTP]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : This problem is extensively investigated in [Ben89].
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----The status of this problem (including some open!)
'GRP127-1_status'(Size,unsatisfiable):-
    tptp2X_member(Size,[2,3,4,6,9,12,13,14,15]),
    !.

%----This list was extracted from [Bennett 1989]
'GRP127-1_status'(Size,open):-
    tptp2X_member(Size,[10,16,18,20,22,24,26,28,30,34,38,42,44,46,52,58,
60,62,66,68,70,72,74,76,86,90,94,98,100,102,106,108,110,114,116,118,122,
132,142,146,154,158,164,170,174]),
    !.

'GRP127-1_status'(_,satisfiable):-
    !.
%--------------------------------------------------------------------------
'GRP127-1'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1_qg_element_clauses'(Size,ElementClauses),
    'GRP123-1_standard_qg_clauses'(Size,product,StandardClauses,yes),
    tptp2X_append(ElementClauses,StandardClauses,AxiomClauses),
    tptp2X_append(AxiomClauses,[
input_clause(qg3,conjecture,
    [--product('Y','X','Z1'),
     --product('Z1','Y','Z2'),
     ++product('Z2','Y','X')])],Clauses),
    'GRP127-1_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the GRP127-1 generator
'GRP127-1_file_information'(generator,sizes(X,(X>=1)),sota(4,5)).
%----Unsatisfiable size 4 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
