%--------------------------------------------------------------------------
% File     : GRP127-4.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Group Theory (Quasigroups)
% Problem  : ((b.a).b).b) = a
% Version  : [Sla93] axioms : Augmented. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [Ben89] Bennett (1989), Quasigroup Identities and Mendelsohn D
%          : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [TPTP]
% Names    : 

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : This problem is extensively investigated in [Ben89].
%          : Version 4 has surjectivity and rotation
% Bugfixes : v1.2.1 - Clauses row_surjectivity and column_surjectivity fixed.
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP127-1.g').
%----Include redundancy code
:-tptp2X_include('Generators/GRP123-4.g').
%--------------------------------------------------------------------------
'GRP127-4'(Size,Clauses,Status):-
    integer(Size),
    'GRP127-1'(Size,BasicClauses,Status),
    'GRP123-4_surjectivity_clauses'(Size,SurjectivityClauses),
    'GRP123-4_rotated_clauses'(BasicClauses,product,RotatedClauses),
    tptp2X_append(SurjectivityClauses,RotatedClauses,ExtraClauses),
    tptp2X_append(ExtraClauses,BasicClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the GRP127-4 generator
'GRP127-4_file_information'(generator,sizes(X,(X>=1)),sota(4,5)).
%----Unsatisfiable size 4 (Otter and SPASS)
%----Satisfiable size 5 ()
%--------------------------------------------------------------------------
