%--------------------------------------------------------------------------
% File     : GRP128-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (a.b).b = a.(a.b)
% Version  : [Sla93] axioms. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [BZ92]  Bennett & Zhu (1992), Conjugate-Orthogonal Latin Squar
%          : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [Sla93]
% Names    : QG6 [Sla93]
%          : QG6 [FSB93]
%          : QG6 [SFS95]
%          : Bennett QG6 [TPTP]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----The status of this problem (including some open!)
'GRP128-1_status'(Size,unsatisfiable):-
    tptp2X_member(Size,[2,3,5,6,7,10,11,12]),
    !.

%----This list is taken from [Bennett & Zhu, 1992], with 9 and 12 removed
%----due to results in [FSB93]
'GRP128-1_status'(Size,open):-
    tptp2X_member(Size,[17,20,21,24,33,41,44,45,48,53,60,65,68,69,72,77,
81,89,93,96,101,105,108,117,129,153,156,161,164,165,168,173,177]),
    !.

'GRP128-1_status'(Size,satisfiable):-
    0 is Size mod 4,
    !.

'GRP128-1_status'(Size,satisfiable):-
    1 is Size mod 4,
    !.

'GRP128-1_status'(_,open):-
    !.
%--------------------------------------------------------------------------
'GRP128-1'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1_qg_element_clauses'(Size,ElementClauses),
    'GRP123-1_standard_qg_clauses'(Size,product,StandardClauses,no),
    tptp2X_append(ElementClauses,StandardClauses,AxiomClauses),
    tptp2X_append(AxiomClauses,[
input_clause(qg3,conjecture,
    [--product('X','Y','Z1'),
     --product('Z1','Y','Z2'),
     ++product('X','Z1','Z2')])],Clauses),
    'GRP128-1_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the GRP128-1 generator
'GRP128-1_file_information'(generator,sizes(X,(X>=1)),sota(3,4)).
%----Unsatisfiable size 3 (Otter and SPASS)
%----Satisfiable size 4 (Otter)
%--------------------------------------------------------------------------
