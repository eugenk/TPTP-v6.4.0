%--------------------------------------------------------------------------
% File     : GRP129-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : a.(b.a) = (b.a).b
% Version  : [Sla93] axioms. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [Sla93]
% Names    : QG7 [Sla93]
%          : QG7 [FSB93]
%          : QG7 [SFS95]
%          : Bennett QG7 [TPTP]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----The status of this problem (including some open!)
'GRP129-1_status'(Size,unsatisfiable):-
    tptp2X_member(Size,[2,3,4,6,7,8,10,11,12,14]),
    !.

'GRP129-1_status'(33,open):-
    !.

'GRP129-1_status'(Size,satisfiable):-
    1 is Size mod 4,
    !.

'GRP129-1_status'(_,open):-
    !.
%--------------------------------------------------------------------------
'GRP129-1'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1_qg_element_clauses'(Size,ElementClauses),
    'GRP123-1_standard_qg_clauses'(Size,product,StandardClauses,no),
    tptp2X_append(ElementClauses,StandardClauses,AxiomClauses),
    tptp2X_append(AxiomClauses,[
input_clause(qg3,conjecture,
    [--product('Y','X','Z1'),
     --product('X','Z1','Z2'),
     ++product('Z1','Y','Z2')])],Clauses),
    'GRP129-1_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the GRP129-1 generator
'GRP129-1_file_information'(generator,sizes(X,(X>=1)),sota(3,5)).
%----Unsatisfiable size 3 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
