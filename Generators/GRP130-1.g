%--------------------------------------------------------------------------
% File     : GRP130-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (a.(a.b)).b = a
% Version  : [Sla93] axioms. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [Sla93]
% Names    : QG8 [Sla93]
%          : Bennett QG8 [TPTP]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----Status of this problem, including some open!
'GRP130-1_status'(Size,unsatisfiable):-
    tptp2X_member(Size,[2,3,6,7,8,10,12,14,15,18]),
    !.

'GRP130-1_status'(Size,open):-
    tptp2X_member(Size,[22,23,26,27,30,34,38,42,43,46,50,54,62,66,74,90,
98,102,114,126]),
    !.

'GRP130-1_status'(_,satisfiable):-
    !.
%--------------------------------------------------------------------------
'GRP130-1'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1_qg_element_clauses'(Size,ElementClauses),
    'GRP123-1_standard_qg_clauses'(Size,product,StandardClauses,no),
    tptp2X_append(ElementClauses,StandardClauses,AxiomClauses),
    tptp2X_append(AxiomClauses,[
input_clause(qg3,conjecture,
    [--product('X','Y','Z1'),
     --product('X','Z1','Z2'),
     ++product('Z2','Y','X')])],Clauses),
    'GRP130-1_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the GRP130-1 generator
'GRP130-1_file_information'(generator,sizes(X,(X>=1)),sota(3,5)).
%----Unsatisfiable size 3 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
