%--------------------------------------------------------------------------
% File     : GRP131-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (3,2,1) conjugate orthogonality, no idempotence
% Version  : [Sla93] axioms. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [Zha94] Zhang (1994), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [Sla93]
% Names    : QG1-ni [Sla93]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [SFS93]'s axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : [Zha94] has pointed out that either one of qg1_1
%            or qg1_2 may be used, as each implies the other in this
%            scenario, with the help of cancellation. The dependence
%            cannot be proved, so both have been left in here.
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----The status of this problem (including some open!)
'GRP131-1_status'(Size,unsatisfiable):-
    tptp2X_member(Size,[2,6]),
    !.

'GRP131-1_status'(_,satisfiable):-
    !.
%--------------------------------------------------------------------------
'GRP131-1'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1_qg_element_clauses'(Size,ElementClauses),
    'GRP123-1_standard_qg_clauses'(Size,product,StandardClauses,no),
    tptp2X_append(ElementClauses,StandardClauses,AxiomClauses),
    tptp2X_append(AxiomClauses,[
input_clause(qg1_1,conjecture,
    [--product('X1','Y1','Z1'),
     --product('X2','Y2','Z1'),
     --product('Z2','Y1','X1'),
     --product('Z2','Y2','X2'),
     ++equalish('X1','X2')]),
input_clause(qg1_2,conjecture,
    [--product('X1','Y1','Z1'),
     --product('X2','Y2','Z1'),
     --product('Z2','Y1','X1'),
     --product('Z2','Y2','X2'),
     ++equalish('Y1','Y2')])],Clauses),
    'GRP131-1_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the GRP131-1 generator
'GRP131-1_file_information'(generator,sizes(X,(X>=1)),sota(2,5)).
%----Unsatisfiable size 2 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
