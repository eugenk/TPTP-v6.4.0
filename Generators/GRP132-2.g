%--------------------------------------------------------------------------
% File     : GRP132-2.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (3,1,2) conjugate orthogonality, no idempotence
% Version  : [Sla93] axioms : Augmented. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [TPTP]
% Names    : 

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Slaney's [1993] axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : As in GRP130-1, either one of qg2_1 or qg2_2 may be used, as 
%            each implies the other in this scenario, with the help of 
%            cancellation. The dependence cannot be proved, so both have 
%            been left in here.
%          : This version adds a simple isomorphism avoidance clause,
%            mentioned in [FSB93].
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP132-1.g').
%----Include redundancy code
:-tptp2X_include('Generators/GRP123-2.g').
%--------------------------------------------------------------------------
'GRP132-2'(Size,Clauses,Status):-
    integer(Size),
    'GRP132-1'(Size,BasicClauses,Status),
%----Create any extra clauses required
    'GRP123-2_qg_simple_redundancy_clauses'(Size,product,ExtraClauses),
    tptp2X_append(ExtraClauses,BasicClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the GRP132-2 generator
'GRP132-2_file_information'(generator,sizes(X,(X>=1)),sota(2,5)).
%----Unsatisfiable size 2 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
