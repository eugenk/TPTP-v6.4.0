%--------------------------------------------------------------------------
% File     : GRP133-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (a.b).(b.a) = a, no idempotence
% Version  : [Sla93] axioms. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [Sla93]
% Names    : QG3-ni [Sla93]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Slaney's [1993] axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----The status of this problem (including some open!)
'GRP133-1_status'(Size,unsatisfiable):-
    tptp2X_member(Size,[5]),
    !.

'GRP133-1_status'(Size,open):-
    tptp2X_member(Size,[12]),
    !.

'GRP133-1_status'(Size,satisfiable):-
    0 is Size mod 4,
    !.

'GRP133-1_status'(Size,satisfiable):-
    1 is Size mod 4,
    !.

'GRP133-1_status'(Size,unsatisfiable):-
    2 is Size mod 4,
    !.

'GRP133-1_status'(Size,unsatisfiable):-
    3 is Size mod 4,
    !.
%--------------------------------------------------------------------------
'GRP133-1'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1_qg_element_clauses'(Size,ElementClauses),
    'GRP123-1_standard_qg_clauses'(Size,product,StandardClauses,no),
    tptp2X_append(ElementClauses,StandardClauses,AxiomClauses),
    tptp2X_append(AxiomClauses,[
input_clause(qg3,conjecture,
    [--product('X','Y','Z1'),
     --product('Y','X','Z2'),
     ++product('Z1','Z2','X')])],Clauses),
    'GRP133-1_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the GRP133-1 generator
'GRP133-1_file_information'(generator,sizes(X,(X>=1)),sota(3,4)).
%----Unsatisfiable size 3 (Otter and SPASS)
%----Satisfiable size 4 (Otter)
%--------------------------------------------------------------------------
