%--------------------------------------------------------------------------
% File     : GRP134-2.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : (a.b).(b.a) = b, no idempotence
% Version  : [Sla93] axioms : Augmented. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [TPTP]
% Names    : 

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Slaney's [1993] axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : This version adds a simple isomorphism avoidance clause,
%            mentioned in [FSB93].
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP134-1.g').
%----Include redundancy code
:-tptp2X_include('Generators/GRP123-2.g').
%--------------------------------------------------------------------------
'GRP134-2'(Size,Clauses,Status):-
    integer(Size),
    'GRP134-1'(Size,BasicClauses,Status),
%----Create any extra clauses required
    'GRP123-2_qg_simple_redundancy_clauses'(Size,product,ExtraClauses),
    tptp2X_append(ExtraClauses,BasicClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the GRP134-2 generator
'GRP134-2_file_information'(generator,sizes(X,(X>=1)),sota(3,5)).
%----Unsatisfiable size 3 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
