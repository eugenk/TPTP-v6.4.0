%--------------------------------------------------------------------------
% File     : GRP135-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : ((b.a).b).b) = a, no idempotence
% Version  : [Sla93] axioms. 
% English  : Generate the multiplication table for the specified quasi-
%            group with SIZE elements.

% Refs     : [Ben89] Bennett (1989), Quasigroup Identities and Mendelsohn D
%          : [FSB93] Fujita et al. (1993), Automatic Generation of Some Res
%          : [Sla93] Slaney (1993), Email to G. Sutcliffe
%          : [SFS95] Slaney et al. (1995), Automated Reasoning and Exhausti
% Source   : [Sla93]
% Names    : QG5-ni [Sla93]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Slaney's [1993] axiomatization has been modified for this.
%          : Substitution axioms are not needed, as any positive equality
%            literals should resolve on negative ones directly.
%          : This problem is extensively investigated in [Ben89].
%--------------------------------------------------------------------------
%----Include basic quasigroup code
:-tptp2X_include('Generators/GRP123-1.g').
%--------------------------------------------------------------------------
%----The status of this problem (including some open!)
'GRP135-1_status'(Size,unsatisfiable):-
    tptp2X_member(Size,[2,6,10]),
    !.

'GRP135-1_status'(Size,open):-
    tptp2X_member(Size,[14,18,26,30,38,42,158]),
    !.

'GRP135-1_status'(_,satisfiable):-
    !.
%--------------------------------------------------------------------------
'GRP135-1'(Size,Clauses,Status):-
    integer(Size),
    'GRP123-1_qg_element_clauses'(Size,ElementClauses),
    'GRP123-1_standard_qg_clauses'(Size,product,StandardClauses,no),
    tptp2X_append(ElementClauses,StandardClauses,AxiomClauses),
    tptp2X_append(AxiomClauses,[
input_clause(qg3,conjecture,
    [--product('Y','X','Z1'),
     --product('Z1','Y','Z2'),
     ++product('Z2','Y','X')])],Clauses),
    'GRP135-1_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the GRP135-1 generator
'GRP135-1_file_information'(generator,sizes(X,(X>=1)),sota(2,5)).
%----Unsatisfiable size 2 (Otter and SPASS)
%----Satisfiable size 5 (Otter)
%--------------------------------------------------------------------------
