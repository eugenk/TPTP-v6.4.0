%--------------------------------------------------------------------------
% File     : MSC007-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Miscellaneous
% Problem  : Cook pigeon-hole problem
% Version  : [Pel86] axioms : Exotic.
%            Theorem formulation : Propositional.
% English  : Suppose there are N holes and N+1 pigeons to put in the 
%            holes. Every pigeon is in a hole and no hole contains more 
%            than one pigeon. Prove that this is impossible. The size is 
%            the number of pigeons.

% Refs     : [CR79]  Cook & Reckhow (1979), The Relative Efficiency of Prop
%          : [Pel86] Pelletier (1986), Seventy-five Problems for Testing Au
% Source   : [Pel86]
% Names    : Pelletier 72 (Size 4) [Pel86]
%          : pigeon.in (Size 4) [OTTER]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : For an N hole problem, the number of propositions is N^2 + N
%            and the number of clauses is (N^3 + N^2)/2 + N+1. Thus the
%            number of propositions increases quadratically and the number
%            of clauses increases cubically.
%--------------------------------------------------------------------------
'MSC007-1_pigeon_somewhere_literals'([],_,[]).

'MSC007-1_pigeon_somewhere_literals'([FirstPigeon|RestOfPigeons],
NumberOfHoles,[FirstLiterals|RestOfPigeonSomewhereLiteralLists]):-
    concatenate_atoms([FirstPigeon,'_in_hole'],LiteralBase),
    generate_literals(1,NumberOfHoles,LiteralBase,_,FirstLiterals,_),
    'MSC007-1_pigeon_somewhere_literals'(RestOfPigeons,NumberOfHoles,
RestOfPigeonSomewhereLiteralLists).
%--------------------------------------------------------------------------
'MSC007-1_one_per_hole_clause'(NumberOfPigeons,NumberOfHoles,
input_clause(Name,conjecture,[--Atom1,--Atom2])):-
    tptp2X_integer_in_range(1,NumberOfHoles,Hole),
    tptp2X_integer_in_range(1,NumberOfPigeons,Pigeon1),
    NextPigeon is Pigeon1 + 1,
    tptp2X_integer_in_range(NextPigeon,NumberOfPigeons,Pigeon2),
    concatenate_atoms([hole_,Hole,'_pigeons_',Pigeon1,'_and_',Pigeon2],
Name),
    concatenate_atoms([pigeon_,Pigeon1,'_in_hole_',Hole],Atom1),
    concatenate_atoms([pigeon_,Pigeon2,'_in_hole_',Hole],Atom2).
%--------------------------------------------------------------------------
'MSC007-1'(NumberOfPigeons,Clauses,unsatisfiable):-
    integer(NumberOfPigeons),
    NumberOfHoles is NumberOfPigeons - 1,
    generate_terms(1,NumberOfPigeons,pigeon,PigeonTerms),
    'MSC007-1_pigeon_somewhere_literals'(PigeonTerms,NumberOfHoles,
PigeonSomewhereLiteralLists),
    make_input_clauses(PigeonSomewhereLiteralLists,pigeon,1,conjecture,
PigeonSomewhereClauses),
    findall(OnePerHoleClause,
        'MSC007-1_one_per_hole_clause'(NumberOfPigeons,NumberOfHoles,
OnePerHoleClause),OnePerHoleClauses),
    tptp2X_append(PigeonSomewhereClauses,OnePerHoleClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the MSC007-1 generator
'MSC007-1_file_information'(generator,sizes(X,(X>=2)),sota(8)).
%----Unsatisfiable size 8 (SPASS)
%----Satisfiable size -
%--------------------------------------------------------------------------
