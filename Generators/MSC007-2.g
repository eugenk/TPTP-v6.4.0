%--------------------------------------------------------------------------
% File     : MSC007-2.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Miscellaneous
% Problem  : Cook pigeon-hole problem
% Version  : [Pel88] axioms : Exotic.
% English  : Suppose there are N holes and N+1 pigeons to put in the 
%            holes. Every pigeon is in a hole and no hole contains more 
%            than one pigeon. Prove that this is impossible. The size is 
%            the number of pigeons.

% Refs     : [CR79]  Cook & Reckhow (1979), The Relative Efficiency of Prop
%          : [Pel86] Pelletier (1986), Seventy-five Problems for Testing Au
%          : [Pel88] Pelletier (1988), Errata
% Source   : [Pel86]
% Names    : Pelletier 73 (Size 4) [Pel86]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : This problem is incorrect in [Pel86] and is corrected in [Pel88].
%--------------------------------------------------------------------------
'MSC007-2_fixed_pigeon_clauses'(FixedClauses):-
    read_formulae_from_file('Axioms/EQU001-0.ax',EqualityClauses,
Dictionary),
    instantiate_variables_from_dictionary(Dictionary),
    tptp2X_append(EqualityClauses,[
input_clause(pigeon_substitutivity1,axiom,
    [--equal('X','Y'),
     --pigeon('X'),
     ++pigeon('Y')]),
input_clause(hole_of_substitutivity1,axiom,
    [--equal('X','Y'),
     ++equal(hole_of('X'),hole_of('Y'))]),
input_clause(hole_substitutivity1,axiom,
    [--equal('X','Y'),
     --hole('X'),
     ++hole('Y')]),
input_clause(in_substitutivity1,axiom,
    [--equal('X','Y'),
     --in('X','Z'),
     ++in('Y','Z')]),
input_clause(in_substitutivity2,axiom,
    [--equal('X','Y'),
     --in('Z','X'),
     ++in('Z','Y')])
],FixedClauses).
%--------------------------------------------------------------------------
%----Inequality of any two elements in a list, modulo symmetry
'MSC007-2_inequality_clause'([FirstElement|RestOfElements],
input_clause(Name,axiom,[--equal(FirstElement,OtherElement)])):-
    tptp2X_member(OtherElement,RestOfElements),
    concatenate_atoms([FirstElement,'_is_not_',OtherElement],Name).

'MSC007-2_inequality_clause'([_|RestOfElements],Clause):-
    'MSC007-2_inequality_clause'(RestOfElements,Clause).
%--------------------------------------------------------------------------
%----Generate clause to say these are all the objects
'MSC007-2_generate_all_objects_clause'(Predicate,Objects,
input_clause(Name,axiom,[--Atom|EachObjectLiterals])):-
    Atom =.. [Predicate,'Hole'],
    concatenate_atoms(['all_',Predicate,'s'],Name),
    findall(++equal('Hole',Object),tptp2X_member(Object,Objects),
EachObjectLiterals).
%--------------------------------------------------------------------------
%----Generate clauses specific to the pigeon hole problem
'MSC007-2_pigeon_hole_conjecture_clauses'([
input_clause(each_pigeons_hole1,axiom,
    [--pigeon('X'),
     ++hole(hole_of('X'))]),
input_clause(each_pigeons_hole2,axiom,
    [--pigeon('X'),
     ++in('X',hole_of('X'))]),
input_clause(only_one_per_hole,conjecture,
    [--hole('Hole'),
     --pigeon('Pigeon1'),
     --pigeon('Pigeon2'),
     --in('Pigeon1','Hole'),
     --in('Pigeon2','Hole'),
     ++equal('Pigeon1','Pigeon2')])
]).
%--------------------------------------------------------------------------
'MSC007-2'(NumberOfPigeons,Clauses,unsatisfiable):-
    integer(NumberOfPigeons),
    NumberOfHoles is NumberOfPigeons - 1,
    'MSC007-2_fixed_pigeon_clauses'(FixedClauses),
%----Clauses that establish the existence and uniqueness of the pigeons 
    generate_literals(1,NumberOfPigeons,pigeon,PigeonAtoms,_,_),
    findall(input_clause(PigeonAtom,axiom,[++pigeon(PigeonAtom)]),
tptp2X_member(PigeonAtom,PigeonAtoms),PigeonObjectClauses),
    findall(PigeonInequalityClause,'MSC007-2_inequality_clause'(PigeonAtoms,
PigeonInequalityClause),PigeonInequalityClauses),
%----Clauses that establish the existence and uniqueness of the holes 
    generate_literals(1,NumberOfHoles,hole,HoleAtoms,_,_),
    findall(input_clause(HoleAtom,axiom,[++hole(HoleAtom)]),
tptp2X_member(HoleAtom,HoleAtoms),HoleObjectClauses),
    findall(HoleInequalityClause,'MSC007-2_inequality_clause'(HoleAtoms,
HoleInequalityClause),HoleInequalityClauses),
    'MSC007-2_generate_all_objects_clause'(hole,HoleAtoms,AllHolesClause),
    'MSC007-2_pigeon_hole_conjecture_clauses'(PigeonHoleTheoremClauses),
    tptp2X_append(FixedClauses,PigeonObjectClauses,Clauses1),
    tptp2X_append(Clauses1,PigeonInequalityClauses,Clauses2),
    tptp2X_append(Clauses2,HoleObjectClauses,Clauses3),
    tptp2X_append(Clauses3,HoleInequalityClauses,Clauses4),
    tptp2X_append(Clauses4,[AllHolesClause|PigeonHoleTheoremClauses],
Clauses).
%--------------------------------------------------------------------------
%----Provide information about the MSC007-2 generator
'MSC007-2_file_information'(generator,sizes(X,(X>=2)),sota(5)).
%----Unsatisfiable size 5 (SPASS)
%----Satisfiable size -
%--------------------------------------------------------------------------
