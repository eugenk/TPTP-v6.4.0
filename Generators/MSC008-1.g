%--------------------------------------------------------------------------
% File     : MSC008-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Miscellaneous
% Problem  : The (in)constructability of Graeco-Latin Squares
% Version  : [Rob63] axioms : Exotic.
% English  : The constructibility of Graeco-Latin squares of order 4t+2. 
%            This is impossible for t=0,1, but possible for all other 
%            cases. The size is the size of the squares.

% Refs     : [Rob63] Robinson (1963), Theorem Proving on the Computer
% Source   : [TPTP]
% Names    : 

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : 
%--------------------------------------------------------------------------
'MSC008-1_fixed_latinsq_clauses'([
input_clause(reflexivity,axiom,
    [++eq('X','X')]),
input_clause(symmetry,axiom,
    [--eq('X','Y'),
     ++eq('Y','X')]),
input_clause(latin_element_is_unique,axiom,
    [--latin('Row','Column','Label1'),
     --latin('Row','Column','Label2'),
     ++eq('Label1','Label2')]),
input_clause(latin_column_is_unique,axiom,
    [--latin('Row','Column1','Label'),
     --latin('Row','Column2','Label'),
     ++eq('Column1','Column2')]),
input_clause(latin_row_is_unique,axiom,
    [--latin('Row1','Column','Label'),
     --latin('Row2','Column','Label'),
     ++eq('Row1','Row2')]),
input_clause(greek_element_is_unique,axiom,
    [--greek('Row','Column','Label1'),
     --greek('Row','Column','Label2'),
     ++eq('Label1','Label2')]),
input_clause(greek_column_is_unique,axiom,
    [--greek('Row','Column1','Label'),
     --greek('Row','Column2','Label'),
     ++eq('Column1','Column2')]),
input_clause(greek_row_is_unique,axiom,
    [--greek('Row1','Column','Label'),
     --greek('Row2','Column','Label'),
     ++eq('Row1','Row2')])
]).
%--------------------------------------------------------------------------
%----Inequality of any two elements in a list, modulo symmetry
'MSC008-1_inequality_clause'([FirstElement|RestOfElements],
input_clause(Name,axiom,[--eq(FirstElement,OtherElement)])):-
    tptp2X_member(OtherElement,RestOfElements),
    concatenate_atoms([FirstElement,'_is_not_',OtherElement],Name).

'MSC008-1_inequality_clause'([_|RestOfElements],Clause):-
    'MSC008-1_inequality_clause'(RestOfElements,Clause).
%--------------------------------------------------------------------------
'MSC008-1_latinsq_restriction_clauses'(Labels,[
input_clause(latin_cell_element,axiom,LatinCellLiterals),
input_clause(latin_column_required,axiom,LatinColumnLiterals),
input_clause(latin_row_required,axiom,LatinRowLiterals),
input_clause(greek_cell_element,axiom,GreekCellLiterals),
input_clause(greek_column_required,axiom,GreekColumnLiterals),
input_clause(greek_row_required,axiom,GreekRowLiterals)
]):-
    findall(++latin('Row','Column',Label),tptp2X_member(Label,Labels),
LatinCellLiterals),
    findall(++latin('Row',Column,'Label'),tptp2X_member(Column,Labels),
LatinColumnLiterals),
    findall(++latin(Row,'Column','Label'),tptp2X_member(Row,Labels),
LatinRowLiterals),
    findall(++greek('Row','Column',Label),tptp2X_member(Label,Labels),
GreekCellLiterals),
    findall(++greek('Row',Column,'Label'),tptp2X_member(Column,Labels),
GreekColumnLiterals),
    findall(++greek(Row,'Column','Label'),tptp2X_member(Row,Labels),
GreekRowLiterals).
%--------------------------------------------------------------------------
'MSC008-1_latinsq_conjecture_clauses'([
input_clause(no_two_same1,conjecture,
    [--greek('Row1','Column1','Label1'),
     --latin('Row1','Column1','Label2'),
     --greek('Row2','Column2','Label1'),
     --latin('Row2','Column2','Label2'),
     ++eq('Column1','Column2')]),
input_clause(no_two_same2,conjecture,
    [--greek('Row1','Column1','Label1'),
     --latin('Row1','Column1','Label2'),
     --greek('Row2','Column2','Label1'),
     --latin('Row2','Column2','Label2'),
     ++eq('Row1','Row2')])
]).
%--------------------------------------------------------------------------
%----Generate the clauses for the latin squares problem
'MSC008-1_generate_latinsq_clauses'(Labels,Clauses):-
    'MSC008-1_fixed_latinsq_clauses'(FixedClauses),
    findall(InequalityClause,
        'MSC008-1_inequality_clause'(Labels,InequalityClause),
InequalityClauses),
    'MSC008-1_latinsq_restriction_clauses'(Labels,RestrictionClauses),
    'MSC008-1_latinsq_conjecture_clauses'(TheoremClauses),
    tptp2X_append(InequalityClauses,FixedClauses,C1),
    tptp2X_append(C1,RestrictionClauses,C2),
    tptp2X_append(C2,TheoremClauses,Clauses).
%--------------------------------------------------------------------------
'MSC008-1'(Size,Clauses,Status):-
    integer(Size),
%----The SquareSize is 4*Size + 2. Check the order exists
    Size >=2,
    0 is (Size - 2) mod 4,
    (tptp2X_member(Size,[2,6]) ->
        Status = unsatisfiable;
        Status = satisfiable),
    generate_literals(1,Size,p,Atoms,_,_),
    'MSC008-1_generate_latinsq_clauses'(Atoms,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the MSC008-1 generator
'MSC008-1_generator_information'(6).
%--------------------------------------------------------------------------
'MSC008-1_file_information'(generator,sizes(X,(X>=2,X mod 4 =:= 2)),sota(2,10)).
%----Unsatisfiable size 2 (Otter and SPASS)
%----Satisfiable size 10 ()
%--------------------------------------------------------------------------
