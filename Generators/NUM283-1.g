%--------------------------------------------------------------------------
% File     : NUM283-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Number Theory
% Problem  : Calculation of factorial
% Version  : [SETHEO] axioms : Biased.
% English  : Compute SIZE factorial.

% Refs     : 
% Source   : [SETHEO]
% Names    : fac2.lop (Size 2) [SETHEO]
%          : fac3.lop (Size 3) [SETHEO]
%          : fac4.lop (Size 4) [SETHEO]
%          : fac5.lop (Size 5) [SETHEO]
%          : fac6.lop (Size 7) [SETHEO]
%          : fac7.lop (Size 7) [SETHEO]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased towards SETHEO.
%--------------------------------------------------------------------------
'NUM283-1_fixed_factorial_clauses'([
input_clause(add_0,axiom,
    [++sum('X',0,'X')]),
input_clause(add,axiom,
    [--sum('X','Y','Z'),
     ++sum('X',s('Y'),s('Z'))]),
input_clause(times1,axiom,
    [++product(s(0),'X','X')]),
input_clause(times,axiom,
    [--sum('R','Y','Z'),
     --product('X','Y','R'),
     ++product(s('X'),'Y','Z')]),
input_clause(factorial_0,axiom,
    [++factorial(0,s(0))]),
input_clause(factorial,axiom,
    [--factorial('X','Z'),
     --product(s('X'),'Z','Y'),
     ++factorial(s('X'),'Y')])
]).
%--------------------------------------------------------------------------
'NUM283-1_successor_integer'(0,0).

'NUM283-1_successor_integer'(Size,s(SuccessorInteger)):-
    Size > 0,
    Size1 is Size - 1,
    'NUM283-1_successor_integer'(Size1,SuccessorInteger).
%--------------------------------------------------------------------------
'NUM283-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    'NUM283-1_fixed_factorial_clauses'(FixedClauses),
    'NUM283-1_successor_integer'(Size,SuccessorInteger),
    tptp2X_append(FixedClauses,[input_clause(prove_factorial,conjecture,
[--factorial(SuccessorInteger,'Result')])],Clauses).
%--------------------------------------------------------------------------
%----Provide information about the NUM283-1 generator
'NUM283-1_file_information'(generator,sizes(X,(X>=1)),sota(5)).
%----Unsatisfiable size 5 (Otter)
%----Satisfiable size -
%--------------------------------------------------------------------------
