%--------------------------------------------------------------------------
% File     : NUM284-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Number Theory
% Problem  : Calculation of fibonacci numbers
% Version  : [SETHEO] axioms : Biased.
% English  : Compute the SIZEth Fibonacci number.

% Refs     : 
% Source   : [SETHEO]
% Names    : fib3.lop (Size 3) [SETHEO]
%          : fib4.lop (Size 4) [SETHEO]
%          : fib5.lop (Size 5) [SETHEO]
%          : fib6.lop (Size 6) [SETHEO]
%          : fib9.lop (Size 9) [SETHEO]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased towards SETHEO.
%--------------------------------------------------------------------------
'NUM184-1_fixed_fibonacci_clauses'([
input_clause(fibonacci_0,axiom,
    [++fibonacci(0,s(0))]),
input_clause(fibonacci_1,axiom,
    [++fibonacci(s(0),s(0))]),
input_clause(fibonacci_N,axiom,
    [--sum('N1',s(0),'N'),
     --sum('N2',s(s(0)),'N'),
     --fibonacci('N1','F1'),
     --fibonacci('N2','F2'),
     --sum('F1','F2','FN'),
     ++fibonacci('N','FN')]),
input_clause(add_0,axiom,
    [++sum('X',0,'X')]),
input_clause(add,axiom,
    [--sum('X','Y','Z'),
     ++sum('X',s('Y'),s('Z'))])
]).
%--------------------------------------------------------------------------
'NUM184-1_successor_integer'(0,0).

'NUM184-1_successor_integer'(Size,s(SuccessorInteger)):-
    Size > 0,
    Size1 is Size - 1,
    'NUM184-1_successor_integer'(Size1,SuccessorInteger).
%--------------------------------------------------------------------------
'NUM284-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    'NUM184-1_fixed_fibonacci_clauses'(FixedClauses),
    'NUM184-1_successor_integer'(Size,SuccessorInteger),
    tptp2X_append(FixedClauses,[input_clause(prove_fibonacci,conjecture,
[--fibonacci(SuccessorInteger,'Result')])],Clauses).
%--------------------------------------------------------------------------
%----Provide information about the NUM284-1 generator
'NUM284-1_generator_information'(unknown).
%--------------------------------------------------------------------------
'NUM284-1_file_information'(generator,sizes(X,(X>=1)),sota(14)).
%----Unsatisfiable size 14 (Otter)
%----Satisfiable size -
%--------------------------------------------------------------------------
