%--------------------------------------------------------------------------
% File     : PUZ015-2.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Puzzles
% Problem  : Checkerboard and Dominoes : Opposing corners removed
% Version  : [Sti93] axioms : Exotic. 
%            Theorem formulation : Propositional.
% English  : There is a checker board whose upper left and lower right 
%            squares have been removed. There is a box of dominoes that 
%            are one square by two squares in size. Can you exactly cover 
%            the checker board with dominoes? The size is the dimension of
%            the checker board.

% Refs     : [Sti93] Stickel (1993), Email to G. Sutcliffe
% Source   : [Sti93]
% Names    : - [Sti93]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : horizontal(R,C) means there is a horizontal tile from (R,C)
%            to (R,C+1). vertical(R,C) means there is a vertical tile from 
%            (R,C) to (R+1,C).
%--------------------------------------------------------------------------
%----Literals that show where dominoes cannot go, due to board mutilation
'PUZ015-2_uncovered_clause'(Size,MissingSquares,input_clause(Name,conjecture,
[--Atom])):-
    tptp2X_member(Row-Column,MissingSquares),
    Column \== Size,
    concatenate_atoms([horizontal,'_',Row,'_',Column],Atom),
    concatenate_atoms([uncovered1,'_',Row,'_',Column],Name).

'PUZ015-2_uncovered_clause'(Size,MissingSquares,input_clause(Name,conjecture,
[--Atom])):-
    tptp2X_member(Row-Column,MissingSquares),
    Row \== Size,
    concatenate_atoms([vertical,'_',Row,'_',Column],Atom),
    concatenate_atoms([uncovered2,'_',Row,'_',Column],Name).

'PUZ015-2_uncovered_clause'(_,MissingSquares,input_clause(Name,conjecture,
[--Atom])):-
    tptp2X_member(Row-Column,MissingSquares),
    Column \== 1,
    Column1 is Column - 1,
    concatenate_atoms([horizontal,'_',Row,'_',Column1],Atom),
    concatenate_atoms([uncovered3,'_',Row,'_',Column],Name).

'PUZ015-2_uncovered_clause'(_,MissingSquares,input_clause(Name,conjecture,
[--Atom])):-
    tptp2X_member(Row-Column,MissingSquares),
    Row \== 1,
    Row1 is Row - 1,
    concatenate_atoms([vertical,'_',Row1,'_',Column],Atom),
    concatenate_atoms([uncovered4,'_',Row,'_',Column],Name).
%--------------------------------------------------------------------------
'PUZ015-2_covered_literal'(Size,Row,Column,++Atom):-
    Size \== Column,
    concatenate_atoms([horizontal,'_',Row,'_',Column],Atom).

'PUZ015-2_covered_literal'(Size,Row,Column,++Atom):-
    Size \== Row,
    concatenate_atoms([vertical,'_',Row,'_',Column],Atom).

'PUZ015-2_covered_literal'(_,Row,Column,++Atom):-
    1 \== Column,
    Column1 is Column - 1,
    concatenate_atoms([horizontal,'_',Row,'_',Column1],Atom).

'PUZ015-2_covered_literal'(_,Row,Column,++Atom):-
    1 \== Row,
    Row1 is Row - 1,
    concatenate_atoms([vertical,'_',Row1,'_',Column],Atom).
%--------------------------------------------------------------------------
%----Clauses that say a square can be covered from various angles
'PUZ015-2_covered_clause'(Size,MissingSquares,
input_clause(Name,conjecture,Literals)):-
    tptp2X_integer_in_range(1,Size,Row),
    tptp2X_integer_in_range(1,Size,Column),
    \+ tptp2X_member(Row-Column,MissingSquares),
    findall(CoveredLiteral,
        'PUZ015-2_covered_literal'(Size,Row,Column,CoveredLiteral),
Literals),
    concatenate_atoms([covered,'_',Row,'_',Column],Name).
%--------------------------------------------------------------------------
'PUZ015-2_no_two_dominoes_literals'(Size,Row,Column,[--Atom1,--Atom2],Name):-
    Row \== Size,
    Column \== Size,
    concatenate_atoms([horizontal,'_',Row,'_',Column],Atom1),
    concatenate_atoms([vertical,'_',Row,'_',Column],Atom2),
    concatenate_atoms([unique_cover1,'_',Row,'_',Column],Name).

'PUZ015-2_no_two_dominoes_literals'(Size,Row,Column,[--Atom1,--Atom2],Name):-
    Column \== 1,
    Column \== Size,
    Column1 is Column - 1,
    concatenate_atoms([horizontal,'_',Row,'_',Column],Atom1),
    concatenate_atoms([horizontal,'_',Row,'_',Column1],Atom2),
    concatenate_atoms([unique_cover2,'_',Row,'_',Column],Name).

'PUZ015-2_no_two_dominoes_literals'(Size,Row,Column,[--Atom1,--Atom2],Name):-
    Row \== 1,
    Column \== Size,
    Row1 is Row - 1,
    concatenate_atoms([horizontal,'_',Row,'_',Column],Atom1),
    concatenate_atoms([vertical,'_',Row1,'_',Column],Atom2),
    concatenate_atoms([unique_cover3,'_',Row,'_',Column],Name).

'PUZ015-2_no_two_dominoes_literals'(Size,Row,Column,[--Atom1,--Atom2],Name):-
    Row \== Size,
    Column \== 1,
    Column1 is Column - 1,
    concatenate_atoms([vertical,'_',Row,'_',Column],Atom1),
    concatenate_atoms([horizontal,'_',Row,'_',Column1],Atom2),
    concatenate_atoms([unique_cover4,'_',Row,'_',Column],Name).

'PUZ015-2_no_two_dominoes_literals'(Size,Row,Column,[--Atom1,--Atom2],Name):-
    Row \== 1,
    Row \== Size,
    Row1 is Row - 1,
    concatenate_atoms([vertical,'_',Row,'_',Column],Atom1),
    concatenate_atoms([vertical,'_',Row1,'_',Column],Atom2),
    concatenate_atoms([unique_cover5,'_',Row,'_',Column],Name).

'PUZ015-2_no_two_dominoes_literals'(_,Row,Column,[--Atom1,--Atom2],Name):-
    Row \== 1,
    Column \== 1,
    Row1 is Row - 1,
    Column1 is Column - 1,
    concatenate_atoms([horizontal,'_',Row,'_',Column1],Atom1),
    concatenate_atoms([vertical,'_',Row1,'_',Column],Atom2),
    concatenate_atoms([unique_cover6,'_',Row,'_',Column],Name).
%--------------------------------------------------------------------------
%----Clauses that say a square is covered from only one angle
'PUZ015-2_unique_cover_clause'(Size,MissingSquares,
input_clause(Name,conjecture,Literals)):-
    tptp2X_integer_in_range(1,Size,Row),
    tptp2X_integer_in_range(1,Size,Column),
    \+ tptp2X_member(Row-Column,MissingSquares),
    'PUZ015-2_no_two_dominoes_literals'(Size,Row,Column,Literals,Name).
%--------------------------------------------------------------------------
'PUZ015-2_make_checkerboard_clauses'(Size,MissingSquares,Clauses):-
    findall(UncoveredClause,'PUZ015-2_uncovered_clause'(Size,MissingSquares,
UncoveredClause),UncoveredClauses),
    findall(CoveredClause,
        'PUZ015-2_covered_clause'(Size,MissingSquares,CoveredClause),
CoveredClauses),
    findall(UniqueCoverClause,
        'PUZ015-2_unique_cover_clause'(Size,MissingSquares,
UniqueCoverClause),UniqueCoverClauses),
    tptp2X_append(UncoveredClauses,CoveredClauses,Clauses1),
    tptp2X_append(Clauses1,UniqueCoverClauses,Clauses).
%--------------------------------------------------------------------------
%----The status of this problem
%----Status for sizes 5,6 according to Tim Geisler
'PUZ015-2_status'(Size,unsatisfiable):-
    tptp2X_member(Size,[2,3,4,5,6]),
    !.

'PUZ015-2_status'(Size,unknown).
%--------------------------------------------------------------------------
'PUZ015-2'(Size,Clauses,Status):-
    integer(Size),
    Size >=2,
    'PUZ015-2_make_checkerboard_clauses'(Size,[1-1,Size-Size],Clauses),
    'PUZ015-2_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the PUZ015-2 generator
'PUZ015-2_file_information'(generator,sizes(X,(X>=2)),sota(6)).
%----Unsatisfiable size 6 (Otter and SPASS)
%----Satisfiable size -
%--------------------------------------------------------------------------
