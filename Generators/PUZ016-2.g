%--------------------------------------------------------------------------
% File     : PUZ016-2.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Puzzles
% Problem  : Checkerboard and Dominoes : Row 1, columns 2 and 3 removed
% Version  : [Sti93] axioms : Exotic. 
%            Theorem formulation : Propositional.
% English  : There is a checker board whose second and third squares from 
%            the first row have been removed. There is a box of dominoes 
%            that are one square by two squares in size. Can you exactly 
%            cover the checker board with dominoes? The size is the 
%            dimension of the checker board.

% Refs     : [Sti93] Stickel (1993), Email to G. Sutcliffe
% Source   : [Sti93]
% Names    : - [Sti93]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : horizontal(R,C) means there is a horizontal tile from (R,C)
%            to (R,C+1). vertical(R,C) means there is a vertical tile from
%            (R,C) to (R+1,C).
%--------------------------------------------------------------------------
%----Include Checkerboard and Dominoes : Opposing corners removed
:-tptp2X_include('Generators/PUZ015-2.g').
%--------------------------------------------------------------------------
%----The status of this problem
%----Status for sizes 5,7 according to Tim Geisler
'PUZ016-2_status'(Size,unsatisfiable):-
    tptp2X_member(Size,[2,3,5,7]),
    !.

%----Status for sizes 4,6,8,10 according to Tim Geisler
'PUZ016-2_status'(Size,satisfiable):-
    tptp2X_member(Size,[4,6,8,10]),
    !.

'PUZ016-2_status'(Size,unknown).
%--------------------------------------------------------------------------
'PUZ016-2'(Size,Clauses,Status):-
    integer(Size),
    Size >= 3,
    'PUZ015-2_make_checkerboard_clauses'(Size,[1-2,1-3],Clauses),
    'PUZ016-2_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the PUZ016-2 generator
'PUZ016-2_file_information'(generator,sizes(X,(X>=3)),sota(5,4)).
%----Unsatisfiable size 5 (Otter and SPASS)
%----Satisfiable size 4 (Otter and SPASS)
%--------------------------------------------------------------------------
