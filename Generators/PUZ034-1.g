%--------------------------------------------------------------------------
% File     : PUZ034-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Puzzles
% Problem  : N queens problem
% Version  : [SETHEO] axioms : Biased.
% English  : The problem is to place SIZE queens on an SIZExSIZE chess board, 
%            so that no queen can attack another.

% Refs     : 
% Source   : [SETHEO]
% Names    : q1-2.lop (Size 8) [SETHEO]
%          : q1-9.lop (Size 9) [SETHEO]
%          : q1-10.lop (Size 10) [SETHEO]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased towards SETHEO.
%--------------------------------------------------------------------------
'PUZ034-1_fixed_N_queens_clauses'([
input_clause(make_list_of_numbers1,axiom,
    [++range('Low','High',cons('Low','RestOfNumbers')),
     --less('Low','High'),
     --sum('Low',s(0),'NewLow'),
     --range('NewLow','High','RestOfNumbers')]),
input_clause(make_list_of_numbers2,axiom,
    [++range('Same','Same',cons('Same',empty_list))]),
input_clause(less1,axiom,
    [++less(0,s('X'))]),
input_clause(less2,axiom,
    [++less(s('X'),s('Y')),
     --less('X','Y')]),
input_clause(add_0,axiom,
    [++sum('X',0,'X')]),
input_clause(add,axiom,
    [--sum('X','Y','Z'),
     ++sum('X',s('Y'),s('Z'))]),
input_clause(select1,axiom,
    [++select('Head',cons('Head','Tail'),'Tail')]),
input_clause(select2,axiom,
    [++select('Element',cons('Head','Tail'),cons('Head','NewTail')),
     --select('Element','Tail','NewTail')]),
input_clause(same_definition1,axiom,
    [--same(s('X'),0)]),
input_clause(same_definition2,axiom,
    [--same(0,s('X'))]),
input_clause(same_definition3,axiom,
    [--same(s('X'),s('Y')),
     ++same('X','Y')]),
input_clause(attack,axiom,
    [++diagonal_attack('Queen',s(0),'PlacedQueens'),
     --attack('Queen','PlacedQueens')]),
input_clause(check_diagonals1,axiom,
    [--diagonal_attack('Queen','QueenNumber',cons('PlacedQueen',
'OtherPlacedQueens')),
     --sum('Diagonal1','QueenNumber','PlacedQueen'),
     ++same('Diagonal1','Queen'),
     --sum('PlacedQueen','QueenNumber','Diagonal2'),
     ++same('Diagonal2','Queen'),
     --sum('QueenNumber',s(0),'NextQueenNumber'),
     ++diagonal_attack('Queen','NextQueenNumber','OtherPlacedQueens')]),
input_clause(check_diagonals2,axiom,
    [--diagonal_attack('Queen','LastQueen',empty_list)]),
input_clause(place_a_queen1,axiom,
    [++do_queens('UnplacedQueens','SafeQueens','Placement'),
     --select('AQueen','UnplacedQueens','RestOfUnplacedQueens'),
     ++attack('AQueen','SafeQueens'),
     --do_queens('RestOfUnplacedQueens',cons('AQueen','SafeQueens'),
'Placement')]),
input_clause(place_a_queen2,axiom,
    [++do_queens(empty_list,'Placement','Placement')]),
input_clause(set_up_queens,axiom,
    [++queens('NumberOfQueens','Placement'),
     --sum('NumberOfQueens',s(0),'Low'),
     --sum('NumberOfQueens','NumberOfQueens','High'),
     --range('Low','High','Positions'),
     --do_queens('Positions',empty_list,'Placement')])
]).
%--------------------------------------------------------------------------
'PUZ034-1_successor_integer'(0,0).

'PUZ034-1_successor_integer'(Size,s(SuccessorInteger)):-
    Size > 0,
    Size1 is Size - 1,
    'PUZ034-1_successor_integer'(Size1,SuccessorInteger).
%--------------------------------------------------------------------------
%----The status of this problem
'PUZ034-1_status'(Size,satisfiable):-
    tptp2X_member(Size,[2,3]),
    !.

'PUZ034-1_status'(_,unknown):-
    !.
%--------------------------------------------------------------------------
'PUZ034-1'(Size,Clauses,Status):-
    integer(Size),
    'PUZ034-1_fixed_N_queens_clauses'(FixedClauses),
    'PUZ034-1_successor_integer'(Size,SuccessorInteger),
    tptp2X_append(FixedClauses,[input_clause(place_queens,conjecture,
[--queens(SuccessorInteger,'Placement')])],Clauses),
    'PUZ034-1_status'(Size,Status).
%--------------------------------------------------------------------------
%----Provide information about the PUZ034-1 generator
'PUZ034-1_file_information'(generator,sizes(X,(X>=2)),sota(4,3)).
%----Unsatisfiable size 4 ()
%----Satisfiable size 3 ()
%--------------------------------------------------------------------------
