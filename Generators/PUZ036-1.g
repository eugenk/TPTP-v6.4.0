%--------------------------------------------------------------------------
% File     : PUZ036-1.SIZE : TPTP v6.4.0. Released v2.0.0.
% Domain   : Puzzles
% Problem  : TopSpin
% Version  : Exotic.
%            Theorem formulation : Reverse the first SIZE pieces.
% English  : TopSpin consists of a circular track with 20 pieces numbered
%            1..20 placed in the track, with a turnstile in the track that 
%            always holds four consecutive pieces. There are three legal 
%            moves in TopSpin: slide all the pieces round the track in
%            either direction, or flip the turnstile. Given any initial
%            board with scrambled pieces on the track, the problem is to
%            find a sequence of moves that unscrambles the pieces. 

% Refs     : [Hua96] Huang (1996)Using OTTER and Prolog to Solve TopSpin
% Source   : [Hua96]
% Names    : TopSpin [Hua96]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Could do lots of other permutations of the pieces.
%          : Increasing size does not correspond to increasing difficulty.
%--------------------------------------------------------------------------
'PUZ036-1_move_axioms'([
input_clause(move_left,axiom,
    [--state('X1','X2','X3','X4','X5','X6','X7','X8','X9','X10',
'X11','X12','X13','X14','X15','X16','X17','X18','X19','X20'),
     ++state('X2','X3','X4','X5','X6','X7','X8','X9','X10','X11',
'X12','X13','X14','X15','X16','X17','X18','X19','X20','X1')]),
input_clause(move_right,axiom,
    [--state('X1','X2','X3','X4','X5','X6','X7','X8','X9','X10',
'X11','X12','X13','X14','X15','X16','X17','X18','X19','X20'),
     ++state('X20','X1','X2','X3','X4','X5','X6','X7','X8','X9','X10','X11',
'X12','X13','X14','X15','X16','X17','X18','X19')]),
input_clause(flip,axiom,
    [--state('X1','X2','X3','X4','X5','X6','X7','X8','X9','X10',
'X11','X12','X13','X14','X15','X16','X17','X18','X19','X20'),
     ++state('X4','X3','X2','X1','X5','X6','X7','X8','X9','X10','X11',
'X12','X13','X14','X15','X16','X17','X18','X19','X20')])]).
%--------------------------------------------------------------------------
'PUZ036-1_start_configuration'(Size,
input_clause(initial_configuration,hypothesis,[++Atom])):-
    generate_terms(1,Size,p,StartPieces),
    fast_reverse_list(StartPieces,[],ReversedStartPieces),
    Size1 is Size + 1,
    generate_terms(Size1,20,p,EndPieces),
    tptp2X_append(ReversedStartPieces,EndPieces,Pieces),
    Atom =.. [state|Pieces].
%--------------------------------------------------------------------------
'PUZ036-1_goal'(input_clause(make_like_this,conjecture,[--Atom])):-
    generate_terms(1,20,p,Pieces),
    Atom =.. [state|Pieces].
%--------------------------------------------------------------------------
'PUZ036-1'(Size,[Goal,StartClause|Axioms],unsatisfiable):-
    'PUZ036-1_move_axioms'(Axioms),
    'PUZ036-1_start_configuration'(Size,StartClause),
    'PUZ036-1_goal'(Goal).
%--------------------------------------------------------------------------
%----Provide information about the PUZ036-1 generator
'PUZ036-1_file_information'(generator,sizes(X,(X>=1,20>=X)),sota(5)).
%----Unsatisfiable size 5 ()
%----Satisfiable size -
%--------------------------------------------------------------------------
