%--------------------------------------------------------------------------
% File     : SYN001-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Syntactic
% Problem  : All signed combinations of some propositions.
% Version  : Exotic.
% English  : Pelletier 2: A biconditional version of the 'most difficult' 
%            theorem proved by the new logic theorist.
%          : Pelletier 6: The Law of Excluded Middle: can be quite 
%            difficult for 'natural' systems.
%          : Pelletier 7: Expanded Law of Excluded Middle. The strategies 
%            of the original Logic Theorist cannot prove this.
%          : Pelletier 8: Pierce's Law. Unprovable by Logic Theorist, and 
%            tricky for 'natural' systems.
%          : Pelletier 11: A simple problem designed to see whether 
%            'natural' systems can do it efficiently (or whether they
%            incorrectly try to prove the -> each way).
%          : The size is the number of propositions.

% Refs     : [NS72]  Newell & Simon (1972), Human Problem Solving
%          : [LS74]  Lawrence & Starkey (1974), Experimental Tests of Resol
%          : [WM76]  Wilson & Minker (1976), Resolution, Refinements, and S
%          : [Pel86] Pelletier (1986), Seventy-five Problems for Testing Au
% Source   : [SPRFN], [Pel86]
% Names    : ls5 (Size 2) [LS74]
%          : ls5 (Size 2) [WM76]
%          : Pelletier 2 (Size 1) [Pel86]
%          : Pelletier 6 (Size 1) [Pel86]
%          : Pelletier 7 (Size 1) [Pel86]
%          : Pelletier 8 (Size 1) [Pel86]
%          : Pelletier 9 (Size 2) [Pel86]
%          : Pelletier 11 (Size 1) [Pel86]
%          : Pelletier 12 (Size 3) [Pel86]
%          : Pelletier 14 (Size 2) [Pel86]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : 
%--------------------------------------------------------------------------
%----For each proposition, give it a sign to make a literal, and record
%----the sign letter
'SYN001-1_select_signs'([],[],[],[]).

%----Take positive literal
'SYN001-1_select_signs'([FirstPositiveLiteral|RestOfPositiveLiterals],
[_|RestOfNegativeLiterals],[FirstPositiveLiteral|RestOfLiterals],
[p|RestOfSignLetters]):-
    'SYN001-1_select_signs'(RestOfPositiveLiterals,RestOfNegativeLiterals,
RestOfLiterals,RestOfSignLetters).

%----Take negative literal
'SYN001-1_select_signs'([_|RestOfPositiveLiterals],[FirstNegativeLiteral|
RestOfNegativeLiterals],[FirstNegativeLiteral|RestOfLiterals],
[n|RestOfSignLetters]):-
    'SYN001-1_select_signs'(RestOfPositiveLiterals,RestOfNegativeLiterals,
RestOfLiterals,RestOfSignLetters).
%--------------------------------------------------------------------------
%----Make the clauses for all signed combination of Order propositions
'SYN001-1_generate_allways_clause'(PositiveLiterals,NegativeLiterals,
input_clause(Name,conjecture,Literals)):-
    'SYN001-1_select_signs'(PositiveLiterals,NegativeLiterals,Literals,
NameList),
    concatenate_atoms(NameList,Name).
%--------------------------------------------------------------------------
'SYN001-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    generate_literals(1,Size,'p',_,PositiveLiterals,NegativeLiterals),
    findall(Clause,'SYN001-1_generate_allways_clause'(PositiveLiterals,
NegativeLiterals,Clause),Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN001-1 generator
'SYN001-1_file_information'(generator,sizes(X,(X>=1)),sota(5)).
%----Unsatisfiable size 5 (13 by SPASS, but too big)
%----Satisfiable size -
%--------------------------------------------------------------------------
