%--------------------------------------------------------------------------
% File     : SYN002-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Syntactic
% Problem  : Odd and Even Problem
% Version  : Exotic.
% English  : Given by the clauses C1: p(X) v p(f^M(X)) and C2: ~p(X) 
%            v ~p(f^N(X)), where if M is odd N is even and vice versa,
%            N > M. The sizes are used for N and M.

% Refs     : [Soc92] Socher-Ambrosius (1992), How to Avoid the Derivation o
% Source   : [Soc92]
% Names    : ederX-Y.lop (Size X:Y) [TUM]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : 
%--------------------------------------------------------------------------
%----Generate f^Order(X)
'SYN002-1_generate_argument'(0,'X'):-
    !.

'SYN002-1_generate_argument'(Order,f(Argument)):-
    Order >= 1,
    NextOrder is Order - 1,
    'SYN002-1_generate_argument'(NextOrder,Argument).
%--------------------------------------------------------------------------
'SYN002-1'(FixedDepth:VariableDepth,[
input_clause(positive,conjecture,
    [++p('X'),++p(FixedArgument)]),
input_clause(negative,conjecture,
    [--p('X'),--p(VariableArgument)])
],unsatisfiable):-
    integer(FixedDepth),
    integer(VariableDepth),
    FixedDepth >= 1,
    VariableDepth > FixedDepth,
    1 is (FixedDepth+VariableDepth) mod 2,
    !,
    'SYN002-1_generate_argument'(FixedDepth,FixedArgument),
    'SYN002-1_generate_argument'(VariableDepth,VariableArgument).
%--------------------------------------------------------------------------
%----Provide information about the SYN002-1 generator
'SYN002-1_file_information'(generator,sizes(X:Y,(X>=1,Y>X,(X+Y) mod 2 =:= 1)),sota(7:8)).
%----Unsatisfiable size 7:8 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
