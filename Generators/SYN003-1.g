%--------------------------------------------------------------------------
% File     : SYN003-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Syntactic
% Problem  : Implications that form a contradiction
% Version  : Biased.
% English  : P1 & Q1 -> P2     P1 & R1 -> P2     Q -> Q1     R -> R1   :
%            P2 & Q2 -> P3     P2 & R2 -> P3     Q -> Q2     R -> R2   :
%              ......           ......        ....      ....           :
%            Pk-1 & Qk-1 ->Pk  Pk-1 & Rk-1 -> Pk Q -> Qk-1   R -> Rk-1 :
%            P1                ~Pk               Q           R         :
%          : The size is k, in the above.

% Refs     : [Pla82] Plaisted (1982), A Simplified Problem Reduction Format
% Source   : [Pla82]
% Names    : Problem 5.2 [Pla82]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : "This set of clauses can cause the following strategies to 
%            generate a search space which is exponential in k: All-negative
%            resolution, set-of-support with ~Pk as the support set, input
%            resolution, SL-resolution, locking resolution with a bad choice
%            of indices, and ancestor-filter form (linear resolution)."
%            [Pla82] p.243.
%--------------------------------------------------------------------------
'SYN003-1_indexed_literal_set'(FixedLiterals,LowIndex,HighIndex,
IndexedLiteralLists,LiteralSet):-
    tptp2X_integer_in_range(LowIndex,HighIndex,Index),
    findall(IndexedLiteral,
        (tptp2X_member(IndexedList,IndexedLiteralLists),
            tptp2X_select_Nth(IndexedLiteral,IndexedList,Index,_)),
                           IndexedLiterals),
    tptp2X_append(FixedLiterals,IndexedLiterals,LiteralSet).
%--------------------------------------------------------------------------
'SYN003-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    generate_literals(1,Size,p,_,[FirstPositivePLiteral|
RestOfPositivePLiterals],NegativePLiterals),
    Size1 is Size - 1,
    generate_literals(1,Size1,q,_,PositiveQLiterals,NegativeQLiterals),
    generate_literals(1,Size1,r,_,PositiveRLiterals,NegativeRLiterals),
    findall(LiteralSet1,
        'SYN003-1_indexed_literal_set'([],1,Size1,[NegativePLiterals,
NegativeQLiterals,RestOfPositivePLiterals],LiteralSet1),LiteralSets1),
    make_input_clauses(LiteralSets1,pqp,1,conjecture,ClauseSet1),
    findall(LiteralSet2,
        'SYN003-1_indexed_literal_set'([],1,Size1,[NegativePLiterals,
NegativeRLiterals,RestOfPositivePLiterals],LiteralSet2),LiteralSets2),
    make_input_clauses(LiteralSets2,prp,1,conjecture,ClauseSet2),
    findall(LiteralSet3,
        'SYN003-1_indexed_literal_set'([--q],1,Size1,[PositiveQLiterals],
LiteralSet3),LiteralSets3),
    make_input_clauses(LiteralSets3,qq,1,conjecture,ClauseSet3),
    findall(LiteralSet4,
        'SYN003-1_indexed_literal_set'([--r],1,Size1,[PositiveRLiterals],
LiteralSet4),LiteralSets4),
    make_input_clauses(LiteralSets4,rr,1,conjecture,ClauseSet4),
    tptp2X_append(ClauseSet1,ClauseSet2,Clauses1),
    tptp2X_append(Clauses1,ClauseSet3,Clauses2),
    tptp2X_append(Clauses2,ClauseSet4,Clauses3),
    tptp2X_select_Nth(LastNegativePLiteral,NegativePLiterals,Size,_),
    make_input_clauses([[FirstPositivePLiteral],[LastNegativePLiteral],
[++q],[++r]],base,1,conjecture,ClauseSet5),
    tptp2X_append(Clauses3,ClauseSet5,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN003-1 generator
'SYN003-1_file_information'(generator,sizes(X,(X>=2)),sota(6)).
%----Unsatisfiable size 6 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
