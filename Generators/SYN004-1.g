%--------------------------------------------------------------------------
% File     : SYN004-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Syntactic
% Problem  : Implications that form a contradiction
% Version  : Biased.
% English  : P1 & Q1 -> P2     P1 & Q1 -> Q2     :
%            P2 & Q2 -> P3     P2 & Q2 -> Q3     :
%              ......           ......           :
%            Pk-1 & Qk-1 ->Pk  Pk-1 & Qk-1 -> Qk :
%            P1     Q1         ~Pk v ~Qk         :
%          : The size is k, in the above.

% Refs     : [Pla82] Plaisted (1982), A Simplified Problem Reduction Format
% Source   : [Pla82]
% Names    : Problem 5.3 [Pla82]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : "This set of clauses can cause the following strategies to
%            generate minimal length proofs having length exponential in k:
%            SL-resolution, locking resolution with a bad choice of indices.
%            However, all of the following strategies can always generate
%            proofs polynomial in the size of S if S is an inconsistent set
%            of propositional Horn clauses: all-negative resolution,
%            set-of-support strategy, ancestry-filter form, and input
%            resolution." [Pla82] p.244.
%--------------------------------------------------------------------------
'SYN004-1_indexed_literal_set'(FixedLiterals,LowIndex,HighIndex,
IndexedLiteralLists,LiteralSet):-
    tptp2X_integer_in_range(LowIndex,HighIndex,Index),
    findall(IndexedLiteral,
        (tptp2X_member(IndexedList,IndexedLiteralLists),
            tptp2X_select_Nth(IndexedLiteral,IndexedList,Index,_)),
                           IndexedLiterals),
    tptp2X_append(FixedLiterals,IndexedLiterals,LiteralSet).
%--------------------------------------------------------------------------
'SYN004-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    generate_literals(1,Size,p,_,[FirstPositivePLiteral|
RestOfPositivePLiterals],NegativePLiterals),
    generate_literals(1,Size,q,_,[FirstPositiveQLiteral|
RestOfPositiveQLiterals],NegativeQLiterals),
    Size1 is Size - 1,
    findall(LiteralSet1,
        'SYN004-1_indexed_literal_set'([],1,Size1,[NegativePLiterals,
NegativeQLiterals,RestOfPositivePLiterals],LiteralSet1),LiteralSets1),
    make_input_clauses(LiteralSets1,pqp,1,conjecture,ClauseSet1),
    findall(LiteralSet2,
        'SYN004-1_indexed_literal_set'([],1,Size1,[NegativePLiterals,
NegativeQLiterals,RestOfPositiveQLiterals],LiteralSet2),LiteralSets2),
    make_input_clauses(LiteralSets2,pqq,1,conjecture,ClauseSet2),
    tptp2X_append(ClauseSet1,ClauseSet2,Clauses1),
    tptp2X_select_Nth(LastNegativePLiteral,NegativePLiterals,Size,_),
    tptp2X_select_Nth(LastNegativeQLiteral,NegativeQLiterals,Size,_),
    make_input_clauses([[FirstPositivePLiteral],[FirstPositiveQLiteral],
[LastNegativePLiteral,LastNegativeQLiteral]],base,1,conjecture,ClauseSet3),
    tptp2X_append(Clauses1,ClauseSet3,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN004-1 generator
'SYN004-1_file_information'(generator,sizes(X,(X>=2)),sota(7)).
%----Unsatisfiable size 7 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
