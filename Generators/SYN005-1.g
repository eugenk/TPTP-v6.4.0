%--------------------------------------------------------------------------
% File     : SYN005-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Syntactic
% Problem  : Disjunctions that form a contradiction
% Version  : Biased.
% English  : ~p1(X1,X2) v ~p2(X2,X3) v ... v ~pSIZE(XSIZE,X1).
%             p1(a,a)      p2(a,a) ...        pSIZE(a,a)

% Refs     : [Pla82] Plaisted (1982), A Simplified Problem Reduction Format
% Source   : [Pla82]
% Names    : Problem 5.4 [Pla82]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : "On this example locking resolution (even with a bad choice 
%            of indices) and SL-resolution generate search spaces of size 
%            polynomial in n, but positive unit resolution, all-negative 
%            resolution, set-of-support strategy, ancestry-filter form, 
%            and input resolution all generate search spaces of size 
%            exponential in n." [Pla82] p.244.
%--------------------------------------------------------------------------
%----Thread literals, ala DCGs
'SYN005-1_thread_literals'([--Predicate],SequenceNumber,[--NewAtom]):-
    !,
    concatenate_atoms(['X',SequenceNumber],Variable1),
    NewAtom =.. [Predicate,Variable1,'X1'].

'SYN005-1_thread_literals'([--Predicate|RestOfLiterals],SequenceNumber,
[--NewAtom|RestOfThreadedLiterals]):-
    concatenate_atoms(['X',SequenceNumber],Variable1),
    NextSequenceNumber is SequenceNumber + 1,
    concatenate_atoms(['X',NextSequenceNumber],Variable2),
    NewAtom =.. [Predicate,Variable1,Variable2],
    'SYN005-1_thread_literals'(RestOfLiterals,NextSequenceNumber,
RestOfThreadedLiterals).
%--------------------------------------------------------------------------
'SYN005-1'(Size,[input_clause(disjunction,conjecture,ThreadedNegativeLiterals)|
PositiveClauses],unsatisfiable):-
    integer(Size),
    generate_literals(1,Size,p,_,PositivePLiterals,NegativePLiterals),
    add_arguments_to_each_literal(PositivePLiterals,[a,a],PositiveLiterals),
    findall([PositiveLiteral],
        tptp2X_member(PositiveLiteral,PositiveLiterals),
                              PositiveLiteralSets),
    make_input_clauses(PositiveLiteralSets,p,1,conjecture,PositiveClauses),
    'SYN005-1_thread_literals'(NegativePLiterals,1,ThreadedNegativeLiterals).
%--------------------------------------------------------------------------
%----Provide information about the SYN005-1 generator
'SYN005-1_file_information'(generator,sizes(X,(X>=1)),sota(10)).
%----Unsatisfiable size 10 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
