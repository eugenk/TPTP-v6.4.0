%--------------------------------------------------------------------------
% File     : SYN007+1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Syntactic
% Problem  : Pelletier Problem 71
% Version  : Exotic.
%            Theorem formulation : For N = SIZE.
% English  : Clausal forms of statements of the form :
%            (p1 <-> (p2 <->...(pN <-> (p1 <-> (p2 <->...<-> pN)...)

% Refs     : [Pel86] Pelletier (1986), Seventy-five Problems for Testing Au
%          : [Urq87] Urquart (1987), Hard Problems for Resolution
% Source   : [Pel86]
% Names    : Pelletier 71 [Pel86]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : The number of distinct letters in U-N is N. The number of
%            occurrences of sentence letters in 2N. The number of clauses
%            goes up dramatically as N increases, but I don't think it
%            shows that the problems are dramatically more difficult as N
%            increases. Rather, it's that the awkward clause form
%            representation comes to the fore most dramatically with
%            embedded biconditionals. On all other measures of complexity,
%            one should say that the problems increase linearly in
%            difficulty. Urquhart says that the proof size of any resolution
%            system increases exponentially with increase in N.
%          : This problem can also be done in terms of graphs, as described 
%            in [Pel86] Problem 74.
%--------------------------------------------------------------------------
%----Complete the inner section
make_equivalence_formula([OneProposition],[],OneProposition):-
    !.

%----Completed the outer section, make inner section
make_equivalence_formula([OneProposition],Propositions,
(OneProposition <=> InnerFormula)):-
    make_equivalence_formula(Propositions,[],InnerFormula).

make_equivalence_formula([FirstProposition|RestOfPropositions],
Propositions,(FirstProposition <=> RestOfFormula)):-
    make_equivalence_formula(RestOfPropositions,Propositions,RestOfFormula).
%--------------------------------------------------------------------------
'SYN007+1'(Size,[input_formula(prove_this,conjecture,Formula)],theorem):-
    generate_terms(1,Size,p,Propositions),
    make_equivalence_formula(Propositions,Propositions,Formula).
%--------------------------------------------------------------------------
%----Provide information about the SYN003-1 generator
'SYN007+1_generator_information'(unknown).
%--------------------------------------------------------------------------
'SYN007+1_file_information'(generator,sizes(X,(X>=1)),sota(14)).
%----Unsatisfiable size 14 (SPASS)
%----Satisfiable size -
%--------------------------------------------------------------------------
