%--------------------------------------------------------------------------
% File     : SYN010-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Syntactic
% Problem  : Example for Proposition 5.2 in [LMG94]
% Version  : Biased.
% English  : Example to show that connection tableaux with factorization
%            cannot polynomially simulate simulate connection tableaux with
%            folding up.

% Refs     : [LMG94] Letz et al. (1994), Controlled Integration of the Cut 
% Source   : [LMG94]
% Names    : Example 5.1 [LMG94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased towards folding up.
%--------------------------------------------------------------------------
'SYN010-1_add_positive_to_each'([],_,[]):-
    !.

'SYN010-1_add_positive_to_each'([FirstPositiveLiteral|RestOfPositiveLiterals],
NegativeLiterals,[[FirstPositiveLiteral|NegativeLiterals]|
RestOfMixedClauses]):-
'SYN010-1_add_positive_to_each'(RestOfPositiveLiterals,NegativeLiterals,
RestOfMixedClauses).
%--------------------------------------------------------------------------
'SYN010-1_make_mixed_clauses'(Index,HighIndex,_,[]):-
    Index > HighIndex,
    !.

'SYN010-1_make_mixed_clauses'(Index,HighIndex,M,MixedClauses):-
    concatenate_atoms([p_,Index],NegativeBase),
    generate_literals(1,M,NegativeBase,_,_,NegativeLiterals),
    Index1 is Index - 1,
    concatenate_atoms([p_,Index1],PositiveBase),
    generate_literals(1,M,PositiveBase,_,PositiveLiterals,_),
    'SYN010-1_add_positive_to_each'(PositiveLiterals,NegativeLiterals,
OneBatchOfMixedClauses),
    NextIndex is Index + 1,
    'SYN010-1_make_mixed_clauses'(NextIndex,HighIndex,M,MoreMixedClauses),
    tptp2X_append(OneBatchOfMixedClauses,MoreMixedClauses,
MixedClauses).
%--------------------------------------------------------------------------
'SYN010-1'(N:M,Clauses,unsatisfiable):-
    integer(N),
    integer(M),
    generate_literals(1,M,p_1,_,_,NegativeLiterals),
    'SYN010-1_make_mixed_clauses'(2,N,M,MixedClauses),
    concatenate_atoms([p_,N],UnitBase),
    generate_literals(1,M,UnitBase,_,PositiveLiterals,_),
    findall([PositiveLiteral],tptp2X_member(PositiveLiteral,
PositiveLiterals),PositiveUnits),
    tptp2X_append([[--p_0],[++p_0|NegativeLiterals]|MixedClauses],
PositiveUnits,AllClausesLiterals),
    make_input_clauses(AllClausesLiterals,clause,1,conjecture,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN010-1 generator
'SYN010-1_file_information'(generator,sizes(X:Y,(X>=1,Y>=1)),sota(5:5)).
%----Unsatisfiable size 5:5 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
