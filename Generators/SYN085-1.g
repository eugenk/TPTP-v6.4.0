%--------------------------------------------------------------------------
% File     : SYN085-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Syntactic
% Problem  : Plaisted problem s(1,SIZE)
% Version  : Biased.
% English  :

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : S1n [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
%--------------------------------------------------------------------------
%----Generator for Plaisted's s(1,SIZE) problems
'SYN085-1'(Size,[GoalClause|AxiomClauses],unsatisfiable):-
    integer(Size),
    generate_literals(1,Size,p,_,PositiveLiterals,NegativeLiterals),
    findall([PositiveLiteral],tptp2X_member(PositiveLiteral,
PositiveLiterals),PositiveUnits),
    make_input_clauses([[--p_0]],s1_goal,1,conjecture,[GoalClause]),
    make_input_clauses([[++p_0|NegativeLiterals]|PositiveUnits],
s1,1,axiom,AxiomClauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN085-1 generator
'SYN085-1_file_information'(generator,sizes(X,(X>=0)),sota(10)).
%----Unsatisfiable size 10 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
