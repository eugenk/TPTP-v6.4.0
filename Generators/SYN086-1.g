%--------------------------------------------------------------------------
% File     : SYN086-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem s(2,SIZE)
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : S2n [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.0 - Errors in type 1 clauses fixed.
%          : v1.2.1 - Incorrect range in type 1 clauses fixed.
%--------------------------------------------------------------------------
%----Clauses of the form
%----    P(i,j):-P(i+1,j),P(i,j-i)    and     P(i,j):-Q(i+1,j),Q(i,j-i)
%----    1 <= i < j <= Size
'SYN086-1_s2_type_1_1_literals'(Size,[++Head,--Tail1,--Tail2]):-
    tptp2X_integer_in_range(1,Size,I),
    tptp2X_integer_in_range(1,Size,J),
    I < J,
    IPlus1 is I + 1,
    JMinus1 is J - 1,
    tptp2X_member(TailSymbol,[p,q]),
    concatenate_atoms(['p_',I,'_',J],Head),
    concatenate_atoms([TailSymbol,'_',IPlus1,'_',J],Tail1),
    concatenate_atoms([TailSymbol,'_',I,'_',JMinus1],Tail2).
%--------------------------------------------------------------------------
%----Clauses of the form
%----    Q(i,j):-P(i+1,j),Q(i,j-i)    and     Q(i,j):-Q(i+1,j),P(i,j-i)
%----    1 <= i < j <= Size
'SYN086-1_s2_type_1_2_literals'(Size,[++Head,--Tail1,--Tail2]):-
    tptp2X_integer_in_range(1,Size,I),
    tptp2X_integer_in_range(1,Size,J),
    IPlus1 is I + 1,
    JMinus1 is J - 1,
    I < J,
    tptp2X_select(TailSymbol1,[p,q],[TailSymbol2]),
    concatenate_atoms(['q_',I,'_',J],Head),
    concatenate_atoms([TailSymbol1,'_',IPlus1,'_',J],Tail1),
    concatenate_atoms([TailSymbol2,'_',I,'_',JMinus1],Tail2).
%--------------------------------------------------------------------------
%----Clauses of the form
%----    P(i,i):-P(i,i+Size/2)    and    Q(i,i):-Q(i,i+Size/2)
%----    1 <= i <= Size/2
'SYN086-1_s2_type_2_1_literals'(Size,[++Head,--Tail]):-
    tptp2X_member(Symbol,[p,q]),
    SizeBy2 is Size // 2,
    tptp2X_integer_in_range(1,SizeBy2,I),
    IPlusSizeBy2 is I + SizeBy2,
    concatenate_atoms([Symbol,'_',I,'_',I],Head),
    concatenate_atoms([Symbol,'_',I,'_',IPlusSizeBy2],Tail).
%--------------------------------------------------------------------------
%----Clauses of the form
%----    P(i,i):-P(i-Size/2,i)    and    Q(i,i):-Q(i-Size/2,i)
%----    Size/2 > i >= Size
'SYN086-1_s2_type_2_2_literals'(Size,[++Head,--Tail]):-
    tptp2X_member(Symbol,[p,q]),
    SizeBy2 is Size // 2,
    SizeBy2Plus1 is SizeBy2 + 1,
    tptp2X_integer_in_range(SizeBy2Plus1,Size,I),
    IMinusSizeBy2 is I - SizeBy2,
    concatenate_atoms([Symbol,'_',I,'_',I],Head),
    concatenate_atoms([Symbol,'_',IMinusSizeBy2,'_',I],Tail).
%--------------------------------------------------------------------------
%----Generator for Plaisted's s(2,SIZE) problems
'SYN086-1'(Size,Clauses,satisfiable):-
    integer(Size),
%----Type 1 clauses
    findall(Type11LiteralSet,'SYN086-1_s2_type_1_1_literals'(Size,
Type11LiteralSet),Type11LiteralSets),
    make_input_clauses(Type11LiteralSets,s2_type11,1,axiom,Type11Clauses),
    findall(Type12LiteralSet,'SYN086-1_s2_type_1_2_literals'(Size,
Type12LiteralSet),Type12LiteralSets),
    make_input_clauses(Type12LiteralSets,s2_type12,1,axiom,Type12Clauses),
    tptp2X_append(Type11Clauses,Type12Clauses,Type1Clauses),
%----Type 2 clauses
    findall(Type2_1LiteralSet,'SYN086-1_s2_type_2_1_literals'(Size,
Type2_1LiteralSet),Type2_1LiteralSets),
    make_input_clauses(Type2_1LiteralSets,s2_type21,1,axiom,
Type2_1Clauses),
    findall(Type2_2LiteralSet,'SYN086-1_s2_type_2_2_literals'(Size,
Type2_2LiteralSet),Type2_2LiteralSets),
    make_input_clauses(Type2_2LiteralSets,s2_type22,1,axiom,
Type2_2Clauses),
    tptp2X_append(Type2_1Clauses,Type2_2Clauses,Type2Clauses),
    concatenate_atoms([p_1_,Size],GoalAtom),
    make_input_clauses([[--GoalAtom]],s2_goal,1,conjecture,[GoalClause]),
    tptp2X_append([GoalClause|Type1Clauses],Type2Clauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN086-1 generator
'SYN086-1_file_information'(generator,sizes(X,(X>=1)),sota(3)).
%----Unsatisfiable size -
%----Satisfiable size 3 (Historically easy)
%--------------------------------------------------------------------------
