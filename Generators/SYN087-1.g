%--------------------------------------------------------------------------
% File     : SYN087-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem s(3,SIZE)
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : S3n [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.1 - Errors in type 1 clauses fixed.
%--------------------------------------------------------------------------
%----Clauses of the form
%----    P(i):-P(i+1),P(i+2)    and   P(i):-Q(i+1),Q(i+2)
%----    0 <= i < 2*Size-2
'SYN087-1_s3_type_1_1_literals'(Size,[++Head,--Tail1,--Tail2]):-
    TwoN is 2 * Size,
    TwoNMinus3 is TwoN - 3,
    tptp2X_integer_in_range(0,TwoNMinus3,I),
    IPlus1 is I + 1,
    IPlus2 is I + 2,
    tptp2X_member(TailSymbol,[p,q]),
    concatenate_atoms(['p_',I],Head),
    concatenate_atoms([TailSymbol,'_',IPlus1],Tail1),
    concatenate_atoms([TailSymbol,'_',IPlus2],Tail2).
%--------------------------------------------------------------------------
%----Clauses of the form
%----    Q(i):-P(i+1),Q(i+2)    and   Q(i):-Q(i+1),P(i+2)
%----    0 <= i < 2*Size-2
'SYN087-1_s3_type_1_2_literals'(Size,[++Head,--Tail1,--Tail2]):-
    TwoN is 2 * Size,
    TwoNMinus3 is TwoN - 3,
    tptp2X_integer_in_range(0,TwoNMinus3,I),
    IPlus1 is I + 1,
    IPlus2 is I + 2,
    tptp2X_select(TailSymbol1,[p,q],[TailSymbol2]),
    concatenate_atoms(['q_',I],Head),
    concatenate_atoms([TailSymbol1,'_',IPlus1],Tail1),
    concatenate_atoms([TailSymbol2,'_',IPlus2],Tail2).
%--------------------------------------------------------------------------
'SYN087-1_s3_type_2_literals'(Size,[[++Head1,--Tail1],[++Head2,--Tail2],
[++Head3,--Tail3],[++Head4,--Tail4]]):-
    TwoN is 2 * Size,
    TwoNMinus1 is TwoN - 1,
    NMinus1 is Size - 1,
    concatenate_atoms([p,'_',TwoNMinus1],Head1),
    concatenate_atoms([p,'_',NMinus1],Tail1),
    concatenate_atoms([p,'_',TwoN],Head2),
    concatenate_atoms([p,'_',Size],Tail2),
    concatenate_atoms([q,'_',TwoNMinus1],Head3),
    concatenate_atoms([q,'_',NMinus1],Tail3),
    concatenate_atoms([q,'_',TwoN],Head4),
    concatenate_atoms([q,'_',Size],Tail4).
%--------------------------------------------------------------------------
'SYN087-1'(Size,Clauses,satisfiable):-
    integer(Size),
%----Type 1 clauses
    findall(Type11LiteralSet,'SYN087-1_s3_type_1_1_literals'(Size,
Type11LiteralSet),Type11LiteralSets),
    make_input_clauses(Type11LiteralSets,s3_type11,1,axiom,Type11Clauses),
    findall(Type12LiteralSet,'SYN087-1_s3_type_1_2_literals'(Size,
Type12LiteralSet),Type12LiteralSets),
    make_input_clauses(Type12LiteralSets,s3_type12,1,axiom,Type12Clauses),
    tptp2X_append(Type11Clauses,Type12Clauses,Type1Clauses),
%----Type 2 clauses
    'SYN087-1_s3_type_2_literals'(Size,Type2LiteralSets),
    make_input_clauses(Type2LiteralSets,s3_type2,1,axiom,Type2Clauses),
    make_input_clauses([[--p_0,--q_0]],s3_goal,1,conjecture,[GoalClause]),
    tptp2X_append([GoalClause|Type1Clauses],Type2Clauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN087-1 generator
'SYN087-1_file_information'(generator,sizes(X,(X>=1)),sota(3)).
%----Unsatisfiable size -
%----Satisfiable size 3 (Historically easy)
%--------------------------------------------------------------------------
