%--------------------------------------------------------------------------
% File     : SYN088-1.SIZE : TPTP v6.4.0. Released v1.0.0.
% Domain   : Syntactic
% Problem  : Plaisted problem s(4,SIZE)
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : S4n [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
%--------------------------------------------------------------------------
'SYN088-1_s4_type_1_literal'(Size,--Atom):-
    tptp2X_integer_in_range(1,Size,I),
    concatenate_atoms([q_,I],Symbol),
    concatenate_atoms(['X_',I],Variable),
    Atom =.. [Symbol,Variable].
%--------------------------------------------------------------------------
'SYN088-1_s4_type_2_literal'(Size,++Atom):-
    tptp2X_integer_in_range(1,Size,I),
    concatenate_atoms([q_,I],Symbol),
    tptp2X_member(Argument,[a,b]),
    Atom =.. [Symbol,Argument].
%--------------------------------------------------------------------------
'SYN088-1'(Size,[GoalClause|AxiomClauses],unsatisfiable):-
    integer(Size),
    generate_literals(1,Size,'X',VariableList,_,_),
    HeadAtom =.. [p|VariableList],
    findall(NegativeLiteral,
        'SYN088-1_s4_type_1_literal'(Size,NegativeLiteral),
            Type1NegativeLiterals),
    findall([PositiveLiteral],
        'SYN088-1_s4_type_2_literal'(Size,PositiveLiteral),Type2LiteralSets),
    make_input_clauses([[++HeadAtom|Type1NegativeLiterals]|
Type2LiteralSets],s4,1,axiom,AxiomClauses),
    findall(a,tptp2X_integer_in_range(1,Size,_),ListOfAs),
    GoalAtom =.. [p|ListOfAs],
    make_input_clauses([[--GoalAtom]],s4_goal,1,conjecture,[GoalClause]).
%--------------------------------------------------------------------------
%----Provide information about the SYN088-1 generator
'SYN088-1_file_information'(generator,sizes(X,(X>=1)),sota(10)).
%----Unsatisfiable size 10 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
