%--------------------------------------------------------------------------
% File     : SYN089-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem t(2,SIZE)
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : T2n [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.0 - Bugfix in SYN086-1.
%          : v1.2.1 - Bugfix in SYN086-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem s(2,SIZE)
:-tptp2X_include('Generators/SYN086-1.g').
%--------------------------------------------------------------------------
'SYN089-1_t2_literal_sets'(Size,++Atom):-
    tptp2X_integer_in_range(1,Size,I),
    tptp2X_member(Symbol,[p,q]),
    concatenate_atoms([Symbol,'_',I,'_',I],Atom).
%--------------------------------------------------------------------------
'SYN089-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    'SYN086-1'(Size,SClauses,_),
    findall([PositiveUnit],'SYN089-1_t2_literal_sets'(Size,PositiveUnit),
PositiveUnits),
    make_input_clauses(PositiveUnits,t2,1,axiom,TypeT2Clauses),
    tptp2X_append(SClauses,TypeT2Clauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN089-1 generator
'SYN089-1_file_information'(generator,sizes(X,(X>=1)),sota(2)).
%----Unsatisfiable size 2 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
