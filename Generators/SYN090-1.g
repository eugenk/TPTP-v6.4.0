%--------------------------------------------------------------------------
% File     : SYN090-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem t(3,SIZE)
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : T3n [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.1 - Bugfix in SYN087-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem s(3,SIZE)
:-tptp2X_include('Generators/SYN087-1.g').
%--------------------------------------------------------------------------
'SYN090-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    'SYN087-1'(Size,SClauses,_),
    TwoN is 2 * Size,
    TwoNMinus1 is TwoN - 1,
    TwoNMinus2 is TwoN - 2,
    concatenate_atoms([p_,TwoNMinus2],Head1),
    concatenate_atoms([p_,TwoNMinus1],Head2),
    concatenate_atoms([q_,TwoNMinus2],Head3),
    concatenate_atoms([q_,TwoNMinus1],Head4),
    make_input_clauses([[++Head1],[++Head2],[++Head3],[++Head4]],t3,1,axiom,
TypeT3Clauses),
    tptp2X_append(SClauses,TypeT3Clauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN090-1 generator
'SYN090-1_file_information'(generator,sizes(X,(X>=1)),sota(8)).
%----Unsatisfiable size 8 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
