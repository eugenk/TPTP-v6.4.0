%--------------------------------------------------------------------------
% File     : SYN091-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem sym(s(2,SIZE))
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : Sym(S2n) [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.0 - Bugfix in SYN086-1.
%          : v1.2.1 - Bugfix in SYN086-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem s(2,SIZE)
:-tptp2X_include('Generators/SYN086-1.g').
%--------------------------------------------------------------------------
'SYN091-1_sym_transform_each_literal'([],[]):-
    !.

'SYN091-1_sym_transform_each_literal'([Literal|RestOfLiterals],[NewLiteral|
RestOfTransformedLiterals]):-
    Literal =.. [Sign,Atom],
    tptp2X_invert_sign(Sign,InvertedSign),
    Atom =.. [Symbol|Arguments],
    concatenate_atoms([sym_,Symbol],NewSymbol),
    NewAtom =.. [NewSymbol|Arguments],
    NewLiteral =.. [InvertedSign,NewAtom],
    'SYN091-1_sym_transform_each_literal'(RestOfLiterals,
RestOfTransformedLiterals).
%--------------------------------------------------------------------------
'SYN091-1_sym_transform_each_clause'([],[]):-
    !.

'SYN091-1_sym_transform_each_clause'([input_clause(Name,Status,Literals)|
RestOfBaseClauses],[input_clause(NewName,Status,NewLiterals)|
RestOfClauses]):-
    concatenate_atoms([sym_,Name],NewName),
    'SYN091-1_sym_transform_each_literal'(Literals,NewLiterals),
    'SYN091-1_sym_transform_each_clause'(RestOfBaseClauses,RestOfClauses).
%--------------------------------------------------------------------------
'SYN091-1_sym_transform'(Clauses,TransformedClauses):-
    'SYN091-1_sym_transform_each_clause'(Clauses,Clauses1),
    tptp2X_select(input_clause(Name,conjecture,Literals),Clauses1,
OtherTransformedClauses),
    tptp2X_append(Clauses,[input_clause(Name,axiom,Literals)|
OtherTransformedClauses],TransformedClauses).
%--------------------------------------------------------------------------
'SYN091-1'(Size,Clauses,satisfiable):-
    integer(Size),
    'SYN086-1'(Size,SClauses,_),
    'SYN091-1_sym_transform'(SClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN091-1 generator
'SYN091-1_file_information'(generator,sizes(X,(X>=1)),sota(3)).
%----Unsatisfiable size -
%----Satisfiable size 3 (Historically easy)
%--------------------------------------------------------------------------
