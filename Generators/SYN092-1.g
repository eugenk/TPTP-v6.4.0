%--------------------------------------------------------------------------
% File     : SYN092-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem sym(s(3,SIZE))
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : Sym(S3n) [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.1 - Bugfix in SYN087-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem s(3,SIZE)
:-tptp2X_include('Generators/SYN087-1.g').
%----Include Plaisted problem sym(s(2,SIZE))
:-tptp2X_include('Generators/SYN091-1.g').
%--------------------------------------------------------------------------
'SYN092-1'(Size,Clauses,satisfiable):-
    integer(Size),
    'SYN087-1'(Size,SClauses,_),
    'SYN091-1_sym_transform'(SClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN092-1 generator
'SYN092-1_file_information'(generator,sizes(X,(X>=1)),sota(3)).
%----Unsatisfiable size -
%----Satisfiable size 3 (Historically easy)
%--------------------------------------------------------------------------
