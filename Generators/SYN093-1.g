%--------------------------------------------------------------------------
% File     : SYN093-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem u(t(2,SIZE))
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : U(T2n) [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.0 - Bugfix in SYN086-1.
%          : v1.2.1 - Bugfix in SYN086-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem t(2,SIZE)
:-tptp2X_include('Generators/SYN089-1.g').
%--------------------------------------------------------------------------
'SYN093-1_u_change_horn_clauses'([],_,[]):-
    !.

%----All negative case                
'SYN093-1_u_change_horn_clauses'([input_clause(Name,Status,Literals)|
RestOfClauses],UNumber,[input_clause(NewName,Status,[++NewAtom|Literals]),
input_clause(NewClauseName,axiom,[--NewAtom])|RestOfChangedClauses]):-
    \+ tptp2X_member(++_,Literals),
    !,
    concatenate_atoms(['u_',UNumber],NewAtom),
    concatenate_atoms([NewAtom,'_',Name],NewClauseName),
    concatenate_atoms([u_,Name],NewName),
    NextUNumber is UNumber + 1,
    'SYN093-1_u_change_horn_clauses'(RestOfClauses,NextUNumber,
RestOfChangedClauses).

%----One positive case
'SYN093-1_u_change_horn_clauses'([input_clause(Name,Status,Literals)|
RestOfClauses],UNumber,[input_clause(NewName,Status,[++NewAtom|Literals]),
input_clause(NewClauseName,axiom,[++Atom,--NewAtom])|RestOfChangedClauses]):-
    tptp2X_select(++Atom,Literals,NegativeLiterals),
    \+ tptp2X_member(++_,NegativeLiterals),
    !,
    concatenate_atoms(['u_',UNumber],NewAtom),
    concatenate_atoms([NewAtom,'_',Name],NewClauseName),
    concatenate_atoms([u_,Name],NewName),
    NextUNumber is UNumber + 1,
    'SYN093-1_u_change_horn_clauses'(RestOfClauses,NextUNumber,
RestOfChangedClauses).

'SYN093-1_u_change_horn_clauses'([FirstClause|RestOfClauses],UNumber,
[FirstClause|RestOfChangedClauses]):-
    'SYN093-1_u_change_horn_clauses'(RestOfClauses,UNumber,
RestOfChangedClauses).
%--------------------------------------------------------------------------
'SYN093-1_u_transform'(Clauses,TransformedClauses):-
    'SYN093-1_u_change_horn_clauses'(Clauses,1,TransformedClauses).
%--------------------------------------------------------------------------
'SYN093-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    'SYN089-1'(Size,SClauses,_),
    'SYN093-1_u_transform'(SClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN093-1 generator
'SYN093-1_file_information'(generator,sizes(X,(X>=1)),sota(2)).
%----Unsatisfiable size 2 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
