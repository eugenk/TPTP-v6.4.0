%--------------------------------------------------------------------------
% File     : SYN094-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem u(t(3,SIZE))
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : U(T3n) [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.1 - Bugfix in SYN087-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem t(3,SIZE)
:-tptp2X_include('Generators/SYN090-1.g').
%----Include Plaisted problem u(t(2,SIZE))
:-tptp2X_include('Generators/SYN093-1.g').
%--------------------------------------------------------------------------
'SYN094-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    'SYN090-1'(Size,SClauses,_),
    'SYN093-1_u_transform'(SClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN094-1 generator
'SYN094-1_file_information'(generator,sizes(X,(X>=1)),sota(5)).
%----Unsatisfiable size 5 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
