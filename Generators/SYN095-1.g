%--------------------------------------------------------------------------
% File     : SYN095-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem m(t(2,SIZE))
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : M(T2n) [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.0 - Bugfix in SYN086-1.
%          : v1.2.1 - Bugfix in SYN086-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem t(2,SIZE)
:-tptp2X_include('Generators/SYN089-1.g').
%--------------------------------------------------------------------------
'SYN095-1_m_transform_each_clause'([],[]):-
    !.

%----Positive unit clause case
'SYN095-1_m_transform_each_clause'([input_clause(Name,Status,
[++Proposition])|RestOfBaseClauses],[input_clause(NewName,Status,
[++NewAtom])|RestOfClauses]):-
    !,
    NewAtom =.. [Proposition,a],
    concatenate_atoms([m_,Name],NewName),
    'SYN095-1_m_transform_each_clause'(RestOfBaseClauses,RestOfClauses).

%----Negative and non-unit cases
'SYN095-1_m_transform_each_clause'([input_clause(Name,Status,Literals)|
RestOfBaseClauses],[input_clause(NewName,Status,TransformedLiterals)|
RestOfClauses]):-
    add_arguments_to_each_literal(Literals,['X'],TransformedLiterals),
    concatenate_atoms([m_,Name],NewName),
    'SYN095-1_m_transform_each_clause'(RestOfBaseClauses,RestOfClauses).
%--------------------------------------------------------------------------
'SYN095-1_m_transform'(Clauses,TransformedClauses):-
    'SYN095-1_m_transform_each_clause'(Clauses,TransformedClauses).
%--------------------------------------------------------------------------
'SYN095-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    'SYN089-1'(Size,SClauses,_),
    'SYN095-1_m_transform'(SClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN095-1 generator
'SYN095-1_file_information'(generator,sizes(X,(X>=1)),sota(2)).
%----Unsatisfiable size 2 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
