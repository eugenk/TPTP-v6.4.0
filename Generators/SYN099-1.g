%--------------------------------------------------------------------------
% File     : SYN099-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem sym(m(t(2,SIZE)))
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : Sym(M(T2n)) [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.0 - Bugfix in SYN086-1.
%          : v1.2.1 - Bugfix in SYN086-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem sym(s(2,SIZE))
:-tptp2X_include('Generators/SYN091-1.g').
%----Include Plaisted problem m(t(2,SIZE))
:-tptp2X_include('Generators/SYN095-1.g').
%--------------------------------------------------------------------------
'SYN099-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    'SYN095-1'(Size,MTClauses,_),
    'SYN091-1_sym_transform'(MTClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN099-1 generator
'SYN099-1_file_information'(generator,sizes(X,(X>=1)),sota(3)).
%----Unsatisfiable size 3 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
