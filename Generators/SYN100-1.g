%--------------------------------------------------------------------------
% File     : SYN100-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem sym(m(t(3,SIZE)))
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : Sym(M(T3n)) [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.0 - Bugfix in SYN086-1.
%          : v1.2.1 - Bugfix in SYN086-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem sym(s(2,SIZE))
:-tptp2X_include('Generators/SYN091-1.g').
%----Include Plaisted problem m(t(3,SIZE))
:-tptp2X_include('Generators/SYN096-1.g').
%--------------------------------------------------------------------------
'SYN100-1'(Size,Clauses,unsatisfiable):-
    integer(Size),
    'SYN096-1'(Size,MTClauses,_),
    'SYN091-1_sym_transform'(MTClauses,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN100-1 generator
'SYN100-1_file_information'(generator,sizes(X,(X>=1)),sota(5)).
%----Unsatisfiable size 5 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
