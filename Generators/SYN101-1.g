%--------------------------------------------------------------------------
% File     : SYN101-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Syntactic
% Problem  : Plaisted problem n(t(2,SIZE1),SIZE2)
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : N(T2n)) [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.0 - Bugfix in SYN086-1.
%          : v1.2.1 - Bugfix in SYN086-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem t(2,SIZE)
:-tptp2X_include('Generators/SYN089-1.g').
%--------------------------------------------------------------------------
'SYN101-1_n_transform_each_clause'([],_,_,_,[]):-
    !.

%----Positive unit case
'SYN101-1_n_transform_each_clause'([input_clause(Name,Status,[++Atom])|
RestOfBaseClauses],Variables,ListOfAs,Type1NegativeLiterals,
[input_clause(NewName,Status,[++NewAtom|Type1NegativeLiterals])|
RestOfClauses]):-
    !,
    NewAtom =.. [Atom|Variables],
    concatenate_atoms([n_,Name],NewName),
    'SYN101-1_n_transform_each_clause'(RestOfBaseClauses,Variables,ListOfAs,
Type1NegativeLiterals,RestOfClauses).

%----Mixed Horn case
'SYN101-1_n_transform_each_clause'([input_clause(Name,Status,Literals)|
RestOfBaseClauses],Variables,ListOfAs,Type1NegativeLiterals,
[input_clause(NewName,Status,NewLiterals)|RestOfClauses]):-
    tptp2X_select(++_,Literals,OtherLiterals),
    \+ tptp2X_member(++_,OtherLiterals),
    !,
    add_arguments_to_each_literal(Literals,Variables,NewLiterals),
    concatenate_atoms([n_,Name],NewName),
    'SYN101-1_n_transform_each_clause'(RestOfBaseClauses,Variables,ListOfAs,
Type1NegativeLiterals,RestOfClauses).

%----Negative case
'SYN101-1_n_transform_each_clause'([input_clause(Name,Status,Literals)|
RestOfBaseClauses],Variables,ListOfAs,Type1NegativeLiterals,
[input_clause(NewName,Status,NewLiterals)|RestOfClauses]):-
    \+ tptp2X_member(++_,Literals),
    add_arguments_to_each_literal(Literals,ListOfAs,NewLiterals),
    concatenate_atoms([n_,Name],NewName),
    'SYN101-1_n_transform_each_clause'(RestOfBaseClauses,Variables,ListOfAs,
Type1NegativeLiterals,RestOfClauses).
%--------------------------------------------------------------------------
%----Used in findall to generate literals of the form --Qi(Xi) (here Q=nq)
'SYN101-1_transform_n_type_1_literal'(Size,--Atom):-
    tptp2X_integer_in_range(1,Size,I),
    concatenate_atoms([nq_,I],Symbol),
    concatenate_atoms(['X_',I],Variable),
    Atom =.. [Symbol,Variable].
%--------------------------------------------------------------------------
%----Used in findall to generate clauses of the form ++Qi(a) and ++Qi(b)
'SYN101-1_transform_n_type_2_literal'(Size,++Atom):-
    tptp2X_integer_in_range(1,Size,I),
    concatenate_atoms([nq_,I],Symbol),
    tptp2X_member(Argument,[a,b]),
    Atom =.. [Symbol,Argument].
%--------------------------------------------------------------------------
'SYN101-1_n_transform'(Clauses,Size,TransformedClauses):-
    generate_literals(1,Size,'X',Variables,_,_),
%----Make list of as
    findall(a,tptp2X_integer_in_range(1,Size,_),ListOfAs),
%----Make list of new negative literals
    findall(NegativeLiteral,'SYN101-1_transform_n_type_1_literal'(Size,
NegativeLiteral),Type1NegativeLiterals),
%----Transform clauses
    'SYN101-1_n_transform_each_clause'(Clauses,Variables,ListOfAs,
Type1NegativeLiterals,Clauses1),
%----Make Qi(a) and Qi(b) literals sets
    findall([PositiveLiteral],'SYN101-1_transform_n_type_2_literal'(Size,
PositiveLiteral),Type2LiteralSets),
    make_input_clauses(Type2LiteralSets,n,1,axiom,AxiomClauses),
    tptp2X_append(Clauses1,AxiomClauses,TransformedClauses).
%--------------------------------------------------------------------------
'SYN101-1'(Size:NumberOfVariables,Clauses,unsatisfiable):-
    integer(Size),
    integer(NumberOfVariables),
    'SYN089-1'(Size,TClauses,_),
    'SYN101-1_n_transform'(TClauses,NumberOfVariables,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN101-1 generator
'SYN101-1_file_information'(generator,sizes(X:Y,(X>=1,Y>=1)),sota(2:2)).
%----Unsatisfiable size 2:2 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
