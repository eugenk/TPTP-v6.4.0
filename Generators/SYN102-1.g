%--------------------------------------------------------------------------
% File     : SYN102-1.SIZE : TPTP v6.4.0. Bugfixed v1.2.0.
% Domain   : Syntactic
% Problem  : Plaisted problem n(t(3,SIZE1),SIZE2)
% Version  : Biased.
% English  : 

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : N(T3n)) [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : Biased away from various calculi.
% Bugfixes : v1.2.0 - Bugfix in SYN087-1.
%--------------------------------------------------------------------------
%----Include Plaisted problem t(3,SIZE)
:-tptp2X_include('Generators/SYN090-1.g').
%----Include Plaisted problem n(t(2,SIZE),SIZE)
:-tptp2X_include('Generators/SYN101-1.g').
%--------------------------------------------------------------------------
'SYN102-1'(Size:NumberOfVariables,Clauses,unsatisfiable):-
    integer(Size),
    integer(NumberOfVariables),
    'SYN090-1'(Size,TClauses,_),
    'SYN101-1_n_transform'(TClauses,NumberOfVariables,Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN102-1 generator
'SYN102-1_file_information'(generator,sizes(X:Y,(X>=1,Y>=1)),sota(7:7)).
%----Unsatisfiable size 7:7 (Historically easy)
%----Satisfiable size -
%--------------------------------------------------------------------------
