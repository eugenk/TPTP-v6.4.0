%--------------------------------------------------------------------------
% File     : SYN302-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Syntactic
% Problem  : Plaisted problem a(SIZE)
% Version  : Biased.
% English  :

% Refs     : [Pla94] Plaisted (1994), The Search Efficiency of Theorem Prov
% Source   : [Pla94]
% Names    : An [Pla94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [Pla94] says "This set of clauses ... is trivial for
%            forward and backward chaning strategies, due to the lack of 
%            positive and negative clauses. However, for the A-ordering 
%            strategy, larger and larger clauses will be generated ..."
%          : Biased away from various calculi.
%--------------------------------------------------------------------------
'SYN302-1_make_DAPa_clause'(Size1,input_clause(Name,conjecture,[++PositiveAtom,
--NegativeAtom1,--NegativeAtom2])):-
    tptp2X_member(PositivePredicate,[p,q]),
    tptp2X_member(NegativePredicate1,[p,q]),
    tptp2X_member(NegativePredicate2,[p,q]),
    tptp2X_integer_in_range(0,Size1,I),
    tptp2X_integer_in_range(0,Size1,J),
    concatenate_atoms([PositivePredicate,'_',I,'_',J],PositiveAtom),
%----Calculate modulo size to identify p_i_n with p_i_0, and p_n_j with
%----p_0_j.
    I1 is (I + 1) mod (Size1 + 1),
    J1 is (J + 1) mod (Size1 + 1),
    concatenate_atoms([NegativePredicate1,'_',I,'_',J1],NegativeAtom1),
    concatenate_atoms([NegativePredicate2,'_',I1,'_',J1],NegativeAtom2),
    concatenate_atoms([PositiveAtom,'_',NegativeAtom1,'_',NegativeAtom2],Name).
%--------------------------------------------------------------------------
%----Generator for Plaisted's a(SIZE) problems
'SYN302-1'(Size,Clauses,satisfiable):-
    integer(Size),
    Size1 is Size - 1,
    findall(Clause,'SYN302-1_make_DAPa_clause'(Size1,Clause),Clauses).
%--------------------------------------------------------------------------
%----Provide information about the SYN302-1 generator
'SYN302-1_file_information'(generator,sizes(X,(X>=1)),sota(3)).
%----Unsatisfiable size -
%----Satisfiable size 3 (Historically easy)
%--------------------------------------------------------------------------
