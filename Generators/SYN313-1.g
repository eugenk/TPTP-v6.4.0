%--------------------------------------------------------------------------
% File     : SYN313-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Syntactic
% Problem  : Problem for testing satisfiability
% Version  : Exotic.
% English  :

% Refs     : [Fer94] Fermueller (1994), Email to G. Sutcliffe
% Source   : [Fer94]
% Names    : - [Fer94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [Fer94] claims that this is "out of range for any prover if
%            n,m>4 or so" (n,m being the two size parameters).
%--------------------------------------------------------------------------
%----Literals of the form ~p(x0,fm(xm),xm+1), m=0 to Size
'SYN313-1_clause1_literal'(Size,--p('X0',Function,NextVariable)):-
    tptp2X_integer_in_range(0,Size,Subscript),
    Subscript1 is Subscript + 1,
    concatenate_atoms([f,Subscript],Functor),
    concatenate_atoms(['X',Subscript],Variable),
    Function =.. [Functor,Variable],
    concatenate_atoms(['X',Subscript1],NextVariable).
%--------------------------------------------------------------------------
%----Literals of the form p(hn(yn-1),yn,jn(ySize)), n=0 to Size
'SYN313-1_clause2_literal'(Size,++p(Function1,Variable,Function2)):-
    concatenate_atoms(['Y',Size],LastVariable),
    tptp2X_integer_in_range(0,Size,Subscript),
    Subscript1 is Subscript - 1,
    concatenate_atoms([h,Subscript],Functor1),
    concatenate_atoms(['Y',Subscript1],PreviousVariable),
    (Subscript == 0 ->
        Function1 = Functor1;
        Function1 =.. [Functor1,PreviousVariable]),
    concatenate_atoms(['Y',Subscript],Variable),
    concatenate_atoms([j,Subscript],Functor2),
    Function2 =.. [Functor2,LastVariable].
%--------------------------------------------------------------------------
%----Generator for Fermueller's first problem
'SYN313-1'(Size1:Size2,[input_clause(clause1,conjecture,Clause1Literals),
input_clause(clause2,conjecture,Clause2Literals)],unsatisfiable):-
    integer(Size1),
    integer(Size2),
    findall(Clause1Literal,'SYN313-1_clause1_literal'(Size1,Clause1Literal),
Clause1Literals),
    findall(Clause2Literal,'SYN313-1_clause2_literal'(Size2,Clause2Literal),
Clause2Literals).
%--------------------------------------------------------------------------
%----Provide information about the SYN313-1 generator
'SYN313-1_file_information'(generator,sizes(X:Y,(X>=1,Y>=1)),sota(1:2)).
%----Unsatisfiable size 1:2 (OS)
%----Satisfiable size -
%--------------------------------------------------------------------------
