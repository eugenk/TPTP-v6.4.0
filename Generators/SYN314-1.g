%--------------------------------------------------------------------------
% File     : SYN314-1.SIZE : TPTP v6.4.0. Released v1.2.0.
% Domain   : Syntactic
% Problem  : Problem for testing satisfiability
% Version  : Exotic.
% English  :

% Refs     : [Fer94] Fermueller (1994), Email to G. Sutcliffe
% Source   : [Fer94]
% Names    : - [Fer94]

% Status   : STATUS
% Rating   : ? v2.0.0
% Syntax   : 

% Comments : [Fer94] claims that this is "out of range for any prover if
%            n,m>4 or so" (n,m being the two size parameters).
%--------------------------------------------------------------------------
%----Literals of the form p(x0,fm(xm), xm+1, gm(xSize+1), m=0 to Size
'SYN314-1_clause1_literal'(Size,++p('X0',Function1,NextVariable,Function2)):-
    Size1 is Size + 1,
    concatenate_atoms(['X',Size1],LastVariable),
    tptp2X_integer_in_range(0,Size,Subscript),
    concatenate_atoms([f,Subscript],Functor1),
    concatenate_atoms(['X',Subscript],Variable),
    Function1 =.. [Functor1,Variable],
    Subscript1 is Subscript + 1,
    concatenate_atoms(['X',Subscript1],NextVariable),
    concatenate_atoms([g,Subscript],Functor2),
    Function2 =.. [Functor2,LastVariable].
%--------------------------------------------------------------------------
%----Literals of the form ~p(hn(yn-1),yn,jn(ySize+n),ySize+n), n=0 to Size
'SYN314-1_clause2_literal'(Size,--p(Function1,Variable1,Function2,
Variable2)):-
    tptp2X_integer_in_range(0,Size,Subscript),
    Subscript1 is Subscript - 1,
    SubscriptN is Size + Subscript,
    SubscriptN1 is Size + 1 + Subscript,
    concatenate_atoms([h,Subscript],Functor1),
    concatenate_atoms(['Y',Subscript1],PreviousVariable),
    (Subscript == 0 ->
        Function1 = Functor1;
        Function1 =.. [Functor1,PreviousVariable]),
    concatenate_atoms(['Y',Subscript],Variable1),
    concatenate_atoms([j,Subscript],Functor2),
    concatenate_atoms(['Y',SubscriptN],NVariable),
    Function2 =.. [Functor2,NVariable],
    concatenate_atoms(['Y',SubscriptN1],Variable2).
%--------------------------------------------------------------------------
%----Generator for Fermueller's first problem
'SYN314-1'(Size1:Size2,[input_clause(clause1,conjecture,Clause1Literals),
input_clause(clause2,conjecture,Clause2Literals)],unsatisfiable):-
    integer(Size1),
    integer(Size2),
    findall(Clause1Literal,'SYN314-1_clause1_literal'(Size1,Clause1Literal),
Clause1Literals),
    findall(Clause2Literal,'SYN314-1_clause2_literal'(Size2,Clause2Literal),
Clause2Literals).
%--------------------------------------------------------------------------
%----Provide information about the SYN314-1 generator
'SYN314-1_file_information'(generator,sizes(X:Y,(X>=0,Y>=0)),sota(2:1)).
%----Unsatisfiable size 2:1 (Historical)
%----Satisfiable size -
%--------------------------------------------------------------------------
