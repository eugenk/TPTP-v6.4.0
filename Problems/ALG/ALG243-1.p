%------------------------------------------------------------------------------
% File     : ALG243-1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Algebra (Non-associative)
% Problem  : Idempotent selfdistributive groupoids are symmetric-by-medial - 2
% Version  : Especial.
% English  :

% Refs     : [PS08]  Phillips & Stanovsky (2008), Using Automated Theorem P
%          : [Sta08a] Stanovsky (2008), Distributive Groupoids are Symmetri
%          : [Sta08b] Stanovsky (2008), Email to G. Sutcliffe
% Source   : [Sta08b]
% Names    : S08_7 [Sta08b]

% Status   : Unsatisfiable
% Rating   : 1.00 v4.0.0
% Syntax   : Number of clauses     :    4 (   0 non-Horn;   4 unit;   1 RR)
%            Number of atoms       :    4 (   4 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    5 (   4 constant; 0-2 arity)
%            Number of variables   :    7 (   0 singleton)
%            Maximal term depth    :    5 (   3 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments :
%------------------------------------------------------------------------------
cnf(c01,axiom,
    ( mult(A,mult(B,C)) = mult(mult(A,B),mult(A,C)) )).

cnf(c02,axiom,
    ( mult(mult(A,B),C) = mult(mult(A,C),mult(B,C)) )).

cnf(c03,axiom,
    ( mult(A,A) = A )).

cnf(goals,negated_conjecture,
    ( mult(mult(mult(a,b),mult(c,d)),mult(mult(mult(a,b),mult(c,d)),mult(mult(a,c),mult(b,d)))) != mult(mult(a,c),mult(b,d)) )).

%------------------------------------------------------------------------------
