%------------------------------------------------------------------------------
% File     : ARI018=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Integer: 12 lesseq to something
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.1.0, 0.11 v6.0.0, 0.12 v5.3.0, 0.14 v5.2.0, 0.60 v5.1.0, 0.50 v5.0.0
% Syntax   : Number of formulae    :    1 (   1 unit;   0 type)
%            Number of atoms       :    1 (   0 equality)
%            Maximal formula depth :    2 (   2 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    2 (   1 propositional; 0-2 arity)
%            Number of functors    :    1 (   1 constant; 0-0 arity)
%            Number of variables   :    1 (   0 sgn;   0   !;   1   ?)
%                                         (   1   :;   0  !>;   0  ?*)
%            Maximal term depth    :    1 (   1 average)
%            Arithmetic symbols    :    3 (   1 prd;   0 fun;   1 num;   1 var)
% SPC      : TF0_THM_NEQ_ARI

% Comments :
%------------------------------------------------------------------------------
tff(prove_12_lesseq_something,conjecture,(
    ? [X: $int] : $lesseq(12,X) )).
%------------------------------------------------------------------------------
