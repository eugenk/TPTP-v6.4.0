%------------------------------------------------------------------------------
% File     : ARI121=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Integer: Commutative product of 6 and 7
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.12 v6.1.0, 0.11 v6.0.0, 0.00 v5.1.0, 0.20 v5.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type)
%            Number of atoms       :    3 (   3 equality)
%            Maximal formula depth :    5 (   5 average)
%            Number of connectives :    2 (   0   ~;   0   |;   1   &)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    2 (   1 propositional; 0-2 arity)
%            Number of functors    :    3 (   2 constant; 0-2 arity)
%            Number of variables   :    2 (   0 sgn;   2   !;   0   ?)
%                                         (   2   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   1 average)
%            Arithmetic symbols    :    5 (   0 prd;   1 fun;   2 num;   2 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(commutative_product_6_7,conjecture,(
    ! [Z1: $int,Z2: $int] : 
      ( ( $product(6,7) = Z1
        & $product(7,6) = Z2 )
     => Z1 = Z2 ) )).
%------------------------------------------------------------------------------
