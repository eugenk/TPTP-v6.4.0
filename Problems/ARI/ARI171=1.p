%------------------------------------------------------------------------------
% File     : ARI171=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Integer: Not 7 less than sum of 2 and 3
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : CounterSatisfiable
% Rating   : 0.00 v5.2.0, 1.00 v5.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type)
%            Number of atoms       :    2 (   1 equality)
%            Maximal formula depth :    3 (   3 average)
%            Number of connectives :    1 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    3 (   1 propositional; 0-2 arity)
%            Number of functors    :    4 (   3 constant; 0-2 arity)
%            Number of variables   :    1 (   0 sgn;   1   !;   0   ?)
%                                         (   1   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   1 average)
%            Arithmetic symbols    :    6 (   1 prd;   1 fun;   3 num;   1 var)
% SPC      : TF0_CSA_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(anti_sum_2_3_greater_7,conjecture,(
    ! [X: $int] : 
      ( $sum(2,3) = X
     => $less(7,X) ) )).
%------------------------------------------------------------------------------
