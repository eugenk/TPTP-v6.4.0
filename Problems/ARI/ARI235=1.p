%------------------------------------------------------------------------------
% File     : ARI235=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Rational: Something greatereq something else
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.3.0, 0.25 v6.2.0, 0.40 v6.1.0, 0.22 v6.0.0, 0.12 v5.5.0, 0.25 v5.3.0, 0.29 v5.2.0, 0.40 v5.1.0, 0.50 v5.0.0
% Syntax   : Number of formulae    :    1 (   1 unit;   0 type)
%            Number of atoms       :    1 (   0 equality)
%            Maximal formula depth :    3 (   3 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    2 (   1 propositional; 0-2 arity)
%            Number of functors    :    0 (   0 constant; --- arity)
%            Number of variables   :    2 (   0 sgn;   0   !;   2   ?)
%                                         (   2   :;   0  !>;   0  ?*)
%            Maximal term depth    :    1 (   1 average)
%            Arithmetic symbols    :    3 (   1 prd;   0 fun;   0 num;   2 var)
% SPC      : TF0_THM_NEQ_ARI

% Comments :
%------------------------------------------------------------------------------
tff(rat_greatereq_problem_6,conjecture,(
    ? [X: $rat,Y: $rat] : $greatereq(X,Y) )).
%------------------------------------------------------------------------------
