%------------------------------------------------------------------------------
% File     : ARI269=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Rational: Sum something -11/2 is -13/2
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.00 v6.3.0, 0.14 v6.2.0, 0.50 v6.1.0, 0.33 v6.0.0, 0.14 v5.5.0, 0.33 v5.4.0, 0.25 v5.3.0, 0.50 v5.2.0, 0.33 v5.1.0, 0.40 v5.0.0
% Syntax   : Number of formulae    :    1 (   1 unit;   0 type)
%            Number of atoms       :    1 (   1 equality)
%            Maximal formula depth :    2 (   2 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    2 (   1 propositional; 0-2 arity)
%            Number of functors    :    3 (   2 constant; 0-2 arity)
%            Number of variables   :    1 (   0 sgn;   0   !;   1   ?)
%                                         (   1   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   2 average)
%            Arithmetic symbols    :    2 (   0 prd;   1 fun;   0 num;   1 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(rat_sum_problem_24,conjecture,(
    ? [X: $rat] : $sum(X,-11/2) = -13/2 )).
%------------------------------------------------------------------------------
