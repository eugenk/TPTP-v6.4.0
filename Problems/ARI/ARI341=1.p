%------------------------------------------------------------------------------
% File     : ARI341=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Rational: -2/5 * (145/2 - 569/5) less than (- 76/25) + 271/10
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.3.0, 0.25 v6.2.0, 0.60 v6.1.0, 0.44 v6.0.0, 0.38 v5.3.0, 0.29 v5.2.0, 0.20 v5.1.0, 0.25 v5.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type)
%            Number of atoms       :    1 (   0 equality)
%            Maximal formula depth :    3 (   2 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    1 (   1   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    4 (   3 propositional; 0-2 arity)
%            Number of functors    :    9 (   5 constant; 0-2 arity)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?)
%                                         (   0   :;   0  !>;   0  ?*)
%            Maximal term depth    :    3 (   3 average)
%            Arithmetic symbols    :    9 (   1 prd;   4 fun;   4 num;   0 var)
% SPC      : TF0_THM_NEQ_ARI

% Comments :
%------------------------------------------------------------------------------
tff(p_type,type,(
    p: $rat > $o )).

tff(rat_combined_problem_7,conjecture,(
    $less($product(-2/5,$difference(145/2,569/5)),$sum($uminus(76/25),271/10)) )).
%------------------------------------------------------------------------------
