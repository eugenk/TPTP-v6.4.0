%------------------------------------------------------------------------------
% File     : ARI449=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Real: Product 7.25 and only 4.0 is 29.0
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.00 v6.3.0, 0.14 v6.2.0, 0.25 v6.1.0, 0.33 v6.0.0, 0.14 v5.5.0, 0.33 v5.4.0, 0.25 v5.3.0, 0.60 v5.2.0, 0.67 v5.1.0, 0.60 v5.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type)
%            Number of atoms       :    2 (   2 equality)
%            Maximal formula depth :    3 (   3 average)
%            Number of connectives :    1 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    2 (   1 propositional; 0-2 arity)
%            Number of functors    :    4 (   3 constant; 0-2 arity)
%            Number of variables   :    1 (   0 sgn;   1   !;   0   ?)
%                                         (   1   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   1 average)
%            Arithmetic symbols    :    5 (   0 prd;   1 fun;   3 num;   1 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(real_product_problem_14,conjecture,(
    ! [X: $real] : 
      ( $product(7.25,X) = 29.0
     => X = 4.0 ) )).
%------------------------------------------------------------------------------
