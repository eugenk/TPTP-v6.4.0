%------------------------------------------------------------------------------
% File     : ARI512=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Mixed: -2 is lesseq to 2.0 coerced to integer
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.40 v6.1.0, 0.56 v6.0.0, 0.62 v5.5.0, 0.50 v5.3.0, 0.43 v5.2.0, 0.60 v5.1.0, 0.75 v5.0.0
% Syntax   : Number of formulae    :    1 (   1 unit;   0 type)
%            Number of atoms       :    1 (   0 equality)
%            Maximal formula depth :    1 (   1 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    3 (   2 constant; 0-1 arity)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?)
%                                         (   0   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   2 average)
%            Arithmetic symbols    :    3 (   1 prd;   1 fun;   1 num;   0 var)
% SPC      : TF0_THM_NEQ_ARI

% Comments :
%------------------------------------------------------------------------------
tff(mixed_types_problem_17,conjecture,(
    $lesseq(-2,$to_int(2.0)) )).
%------------------------------------------------------------------------------
