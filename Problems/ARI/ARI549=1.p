%------------------------------------------------------------------------------
% File     : ARI549=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Integer: Product of sum -2 and -3, and 0 is 0
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.25 v6.1.0, 0.11 v6.0.0, 0.00 v5.4.0, 0.12 v5.3.0, 0.20 v5.2.0, 0.00 v5.0.0
% Syntax   : Number of formulae    :    1 (   1 unit;   0 type)
%            Number of atoms       :    1 (   1 equality)
%            Maximal formula depth :    1 (   1 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    5 (   3 constant; 0-2 arity)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?)
%                                         (   0   :;   0  !>;   0  ?*)
%            Maximal term depth    :    3 (   2 average)
%            Arithmetic symbols    :    3 (   0 prd;   2 fun;   1 num;   0 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(int_combined_problem_13,conjecture,
    ( $product($sum(-2,-3),0) = 0 )).
%------------------------------------------------------------------------------
