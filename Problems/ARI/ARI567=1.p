%------------------------------------------------------------------------------
% File     : ARI567=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Arithmetic
% Problem  : Real: (-0.25 - (something * -0.05) lesseq to 0.0
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.20 v6.1.0, 0.22 v6.0.0, 0.25 v5.5.0, 0.38 v5.3.0, 0.57 v5.2.0, 0.60 v5.1.0, 0.75 v5.0.0
% Syntax   : Number of formulae    :    1 (   1 unit;   0 type)
%            Number of atoms       :    1 (   0 equality)
%            Maximal formula depth :    2 (   2 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    2 (   1 propositional; 0-2 arity)
%            Number of functors    :    5 (   3 constant; 0-2 arity)
%            Number of variables   :    1 (   0 sgn;   0   !;   1   ?)
%                                         (   1   :;   0  !>;   0  ?*)
%            Maximal term depth    :    3 (   2 average)
%            Arithmetic symbols    :    5 (   1 prd;   2 fun;   1 num;   1 var)
% SPC      : TF0_THM_NEQ_ARI

% Comments :
%------------------------------------------------------------------------------
tff(real_combined_problem_18,conjecture,(
    ? [X: $real] : $lesseq($difference(-0.25,$product(X,-0.05)),0.0) )).
%------------------------------------------------------------------------------
