%------------------------------------------------------------------------------
% File     : ARI575=2 : TPTP v6.4.0. Released v5.1.0.
% Domain   : Arithmetic
% Problem  : Inequation system has more than one rational solution
% Version  : Especial.
% English  :

% Refs     : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    :

% Status   : CounterSatisfiable
% Rating   : 0.67 v6.4.0, 0.33 v6.0.0, 0.50 v5.4.0, 0.00 v5.3.0, 0.33 v5.2.0, 1.00 v5.1.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type)
%            Number of atoms       :    7 (   3 equality)
%            Maximal formula depth :    8 (   8 average)
%            Number of connectives :    6 (   0   ~;   0   |;   5   &)
%                                         (   1 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    3 (   1 propositional; 0-2 arity)
%            Number of functors    :    8 (   6 constant; 0-2 arity)
%            Number of variables   :    3 (   0 sgn;   3   !;   0   ?)
%                                         (   3   :;   0  !>;   0  ?*)
%            Maximal term depth    :    4 (   1 average)
%            Arithmetic symbols    :   12 (   1 prd;   2 fun;   6 num;   3 var)
% SPC      : TF0_CSA_EQU_ARI

% Comments : 
%------------------------------------------------------------------------------
tff(ineq_sys_has_1_int_sol,conjecture,(
    ! [X: $rat,Y: $rat,Z: $rat] :
      ( ( $less(3/1,X)
        & $less(X,Y)
        & $less(Y,Z)
        & $less($sum($sum(X,$product(2/1,Y)),$product(3/1,Z)),35/1) )
    <=> ( X = 4/1
        & Y = 5/1
        & Z = 6/1 ) ) )).

%------------------------------------------------------------------------------
