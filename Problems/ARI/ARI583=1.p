%------------------------------------------------------------------------------
% File     : ARI583=1 : TPTP v6.4.0. Released v5.1.0.
% Domain   : Arithmetic
% Problem  : Inequation system is solvable (choose, e.g., W = 3 - X)
% Version  : Especial.
% English  :

% Refs     : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.1.0, 0.33 v6.0.0, 0.38 v5.4.0, 0.25 v5.3.0, 0.43 v5.2.0, 0.60 v5.1.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type)
%            Number of atoms       :    7 (   0 equality)
%            Maximal formula depth :    9 (   9 average)
%            Number of connectives :    6 (   0   ~;   1   |;   4   &)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    3 (   1 propositional; 0-2 arity)
%            Number of functors    :    4 (   3 constant; 0-2 arity)
%            Number of variables   :    4 (   0 sgn;   3   !;   1   ?)
%                                         (   4   :;   0  !>;   0  ?*)
%            Maximal term depth    :    3 (   1 average)
%            Arithmetic symbols    :   10 (   2 prd;   1 fun;   3 num;   4 var)
% SPC      : TF0_THM_NEQ_ARI

% Comments :
%------------------------------------------------------------------------------
tff(mix_quant_ineq_sys_solvable_4,conjecture,(
    ! [X: $int,Y: $int,Z: $int] :
      ( ( $less(0,X)
        & $lesseq(0,Y)
        & $lesseq(0,Z)
        & ( $less(X,Y)
          | $less(X,Z) ) )
     => ? [W: $int] :
          ( $less($sum(X,W),4)
          & $less(3,$sum($sum(Y,Z),W)) ) ) )).

%------------------------------------------------------------------------------
