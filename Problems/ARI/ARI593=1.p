%------------------------------------------------------------------------------
% File     : ARI593=1 : TPTP v6.4.0. Released v5.1.0.
% Domain   : Arithmetic
% Problem  : There is a number in {5,6,7} that is divisible by 3
% Version  : Especial.
% English  :

% Refs     : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.4.0, 0.33 v6.3.0, 0.25 v6.2.0, 0.60 v6.1.0, 0.56 v6.0.0, 0.62 v5.5.0, 0.75 v5.4.0, 0.62 v5.3.0, 0.71 v5.2.0, 0.80 v5.1.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type)
%            Number of atoms       :    4 (   0 equality)
%            Maximal formula depth :    4 (   4 average)
%            Number of connectives :    3 (   0   ~;   0   |;   2   &)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    1 (   1   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    4 (   3 propositional; 0-1 arity)
%            Number of functors    :    5 (   4 constant; 0-2 arity)
%            Number of variables   :    1 (   0 sgn;   0   !;   1   ?)
%                                         (   1   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   1 average)
%            Arithmetic symbols    :    6 (   0 prd;   1 fun;   4 num;   1 var)
% SPC      : TF0_THM_NEQ_ARI

% Comments : Also a theorem for $rat and $real, but much easier.
%------------------------------------------------------------------------------
tff(p_type,type,(
    p: $int > $o )).

tff(exists_X_in_5_6_7_div_3,conjecture,
    ( ( p(5)
      & p(6)
      & p(7) )
   => ? [X: $int] : p($product(3,X)) )).

%------------------------------------------------------------------------------
