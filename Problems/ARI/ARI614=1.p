%------------------------------------------------------------------------------
% File     : ARI614=1 : TPTP v6.4.0. Released v5.1.0.
% Domain   : Arithmetic
% Problem  : There is an X>a and a Y<1 whose sum is 0 (X = max(a+1,0), Y = -X)
% Version  : Especial.
% English  :

% Refs     : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.4.0, 0.67 v6.3.0, 0.29 v6.2.0, 0.62 v6.1.0, 0.78 v6.0.0, 0.71 v5.5.0, 0.67 v5.4.0, 0.75 v5.3.0, 0.90 v5.2.0, 0.83 v5.1.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type)
%            Number of atoms       :    7 (   1 equality)
%            Maximal formula depth :    6 (   4 average)
%            Number of connectives :    6 (   0   ~;   0   |;   3   &)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    9 (   5 propositional; 0-2 arity)
%            Number of functors    :    3 (   2 constant; 0-2 arity)
%            Number of variables   :    4 (   0 sgn;   2   !;   2   ?)
%                                         (   4   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   1 average)
%            Arithmetic symbols    :    7 (   1 prd;   1 fun;   1 num;   4 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(p_type,type,(
    p: $int > $o )).

tff(q_type,type,(
    q: $int > $o )).

tff(a_type,type,(
    a: $int )).

tff(interv_a_infty_and_neginfty_1_contain_compl,conjecture,
    ( ( ! [X: $int] :
          ( $less(a,X)
         => p(X) )
      & ! [X: $int] :
          ( $less(X,0)
         => q(X) ) )
   => ? [X: $int,Y: $int] :
        ( p(X)
        & q(Y)
        & $sum(X,Y) = 0 ) )).

%------------------------------------------------------------------------------
