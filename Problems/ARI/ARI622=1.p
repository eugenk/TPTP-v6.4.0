%------------------------------------------------------------------------------
% File     : ARI622=1 : TPTP v6.4.0. Released v5.1.0.
% Domain   : Arithmetic
% Problem  : There exist two powers of 2 whose sum equals 10
% Version  : Especial.
% English  :

% Refs     : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    :

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.00 v6.3.0, 0.14 v6.2.0, 0.50 v6.1.0, 0.67 v6.0.0, 0.71 v5.5.0, 0.78 v5.4.0, 0.88 v5.3.0, 0.80 v5.2.0, 0.83 v5.1.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type)
%            Number of atoms       :    8 (   3 equality)
%            Maximal formula depth :    8 (   6 average)
%            Number of connectives :    7 (   0   ~;   1   |;   4   &)
%                                         (   1 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    1 (   1   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    6 (   3 propositional; 0-2 arity)
%            Number of functors    :    5 (   3 constant; 0-2 arity)
%            Number of variables   :    4 (   0 sgn;   1   !;   3   ?)
%                                         (   4   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   1 average)
%            Arithmetic symbols    :   10 (   1 prd;   2 fun;   3 num;   4 var)
% SPC      : TF0_THM_EQU_ARI

% Comments : Also a theorem for $rat and $real
%------------------------------------------------------------------------------
tff(pow2_type,type,(
    pow2: $int > $o )).

tff(sum_of_pows_of_2_eq_10,conjecture,
    ( ! [X: $int] :
        ( pow2(X)
      <=> ( X = 1
          | ( $lesseq(2,X)
            & ? [Y: $int] :
                ( $product(2,Y) = X
                & pow2(Y) ) ) ) )
   => ? [X: $int,Y: $int] :
        ( pow2(X)
        & pow2(Y)
        & $sum(X,Y) = 10 ) )).

%------------------------------------------------------------------------------
