%------------------------------------------------------------------------------
% File     : ARI625=1 : TPTP v6.4.0. Bugfixed v5.2.0.
% Domain   : Arithmetic
% Problem  : There is no enumeration of the reals
% Version  : Especial.
% English  :

% Refs     : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    :

% Status   : Theorem
% Rating   : 1.00 v5.2.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type)
%            Number of atoms       :    3 (   1 equality)
%            Maximal formula depth :    6 (   4 average)
%            Number of connectives :    3 (   1   ~;   2   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    1 (   1   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    5 (   2 propositional; 0-2 arity)
%            Number of functors    :    3 (   1 constant; 0-2 arity)
%            Number of variables   :    2 (   0 sgn;   2   !;   0   ?)
%                                         (   2   :;   0  !>;   0  ?*)
%            Maximal term depth    :    3 (   2 average)
%            Arithmetic symbols    :    6 (   2 prd;   1 fun;   1 num;   2 var)
% SPC      : TF0_THM_EQU_ARI

% Comments : Countersatisfiable for $rat (interpret f by an enumeration 
%            of $rat).
% Bugfixes : v5.2.0 - Made numbers into reals.
%------------------------------------------------------------------------------
tff(f_type,type,(
    f: $real > $real )).

tff(not_ex_enum_of_reals,conjecture,(
    ~ ! [X: $real,Y: $real] :
        ( X = Y
        | $less(f(X),f(Y))
        | $greatereq(f(X),$sum(f(Y),1.0)) ) )).

%------------------------------------------------------------------------------
