%------------------------------------------------------------------------------
% File     : ARI641=1 : TPTP v6.4.0. Released v6.3.0.
% Domain   : Arithmetic
% Problem  : Example 22
% Version  : Especial.
% English  :

% Refs     : [LAR14] Lewis et al. (2014), A Heuristic Prover for Real Inequ
%          : [Lew14] Lewis (2014), Email to Geoff Sutcliffe
% Source   : [Lew14]
% Names    : Example 22 [Lew14]

% Status   : Theorem
% Rating   : 1.00 v6.3.0
% Syntax   : Number of formulae    :    8 (   4 unit;   4 type)
%            Number of atoms       :    4 (   0 equality)
%            Maximal formula depth :    2 (   2 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    8 (   5 propositional; 0-2 arity)
%            Number of functors    :   11 (   7 constant; 0-2 arity)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?)
%                                         (   0   :;   0  !>;   0  ?*)
%            Maximal term depth    :    6 (   2 average)
%            Arithmetic symbols    :    9 (   3 prd;   4 fun;   2 num;   0 var)
% SPC      : TF0_THM_NEQ_ARI

% Comments :
%------------------------------------------------------------------------------
%----Should be known property Axiom: Forall([x], ceil(x) >= x)
tff(m_type,type,(
    m: $real )).

tff(x_type,type,(
    x: $real )).

tff(a_type,type,(
    a: $real )).

tff(b_type,type,(
    b: $real )).

tff(hypothesis,hypothesis,(
    $less(a,b) )).

tff(hypothesis_01,hypothesis,(
    $greater(x,a) )).

tff(hypothesis_02,hypothesis,(
    $greatereq(m,$ceiling($product($quotient(1.0,$sum($product(-1.0,a),x)),$sum($product(-1.0,a),b)))) )).

tff(conclusion,conjecture,(
    $less($sum(a,$product($quotient(1.0,$sum(1.0,m)),$sum($product(-1.0,a),b))),x) )).

%------------------------------------------------------------------------------
