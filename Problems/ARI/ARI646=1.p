%------------------------------------------------------------------------------
% File     : ARI646=1 : TPTP v6.4.0. Released v6.3.0.
% Domain   : Arithmetic
% Problem  : Simple reasoning about linear inequalities
% Version  : Especial.
% English  :

% Refs     : [BHS07] Beckert et al. (2007), Verification of Object-Oriented
%          : [Rue14] Ruemmer (2014), Email to Geoff Sutcliffe
% Source   : [Rue14]
% Names    : simplify2.pri [BHS07]
%          : ineqs_simplify2.p [Rue14]

% Status   : Theorem
% Rating   : 0.00 v6.3.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type)
%            Number of atoms       :   12 (  10 equality)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :   11 (   0   ~;   9   |;   1   &)
%                                         (   1 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    5 (   2 propositional; 0-2 arity)
%            Number of functors    :   12 (  12 constant; 0-0 arity)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?)
%                                         (   0   :;   0  !>;   0  ?*)
%            Maximal term depth    :    1 (   1 average)
%            Arithmetic symbols    :   13 (   2 prd;   0 fun;  11 num;   0 var)
% SPC      : TF0_THM_EQU_ARI

% Comments : KeY arithmetic regression test, http://www.key-project.org
%------------------------------------------------------------------------------
tff(a_type,type,(
    a: $int )).

tff(conj,conjecture,
    ( ( $greater(10,a)
      & $lesseq(0,a) )
  <=> ( a = 9
      | a = 8
      | a = 7
      | a = 6
      | a = 5
      | a = 4
      | a = 3
      | a = 2
      | a = 1
      | a = 0 ) )).

%------------------------------------------------------------------------------
