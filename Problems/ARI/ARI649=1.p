%------------------------------------------------------------------------------
% File     : ARI649=1 : TPTP v6.4.0. Released v6.3.0.
% Domain   : Arithmetic
% Problem  : a*a = 25, b*b*b = -125, a < 0 imply a = b
% Version  : Especial.
% English  :

% Refs     : [BHS07] Beckert et al. (2007), Verification of Object-Oriented
%          : [Rue14] Ruemmer (2014), Email to Geoff Sutcliffe
% Source   : [Rue14]
% Names    : simplify5.pri [BHS07]
%          : ineqs_simplify5a.p [Rue14]

% Status   : Theorem
% Rating   : 0.57 v6.4.0, 1.00 v6.3.0
% Syntax   : Number of formulae    :    6 (   4 unit;   2 type)
%            Number of atoms       :    4 (   3 equality)
%            Maximal formula depth :    2 (   1 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    5 (   3 propositional; 0-2 arity)
%            Number of functors    :    6 (   5 constant; 0-2 arity)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?)
%                                         (   0   :;   0  !>;   0  ?*)
%            Maximal term depth    :    3 (   1 average)
%            Arithmetic symbols    :    4 (   1 prd;   1 fun;   2 num;   0 var)
% SPC      : TF0_THM_EQU_ARI

% Comments : KeY arithmetic regression test, http://www.key-project.org
%------------------------------------------------------------------------------
tff(a_type,type,(
    a: $int )).

tff(b_type,type,(
    b: $int )).

tff(conj,axiom,(
    $less(a,0) )).

tff(conj_001,axiom,(
    $product(a,a) = 25 )).

tff(conj_002,axiom,(
    $product($product(b,b),b) = -125 )).

tff(conj_003,conjecture,(
    a = b )).

%------------------------------------------------------------------------------
