%------------------------------------------------------------------------------
% File     : ARI707=1 : TPTP v6.4.0. Released v6.3.0.
% Domain   : Arithmetic
% Problem  : Simple rewriting: d*d + c = 2*d*d implies c*d*d = c*c
% Version  : Especial.
% English  :

% Refs     : [BHS07] Beckert et al. (2007), Verification of Object-Oriented
%          : [Rue14] Ruemmer (2014), Email to Geoff Sutcliffe
% Source   : [Rue14]
% Names    : simplify4.pri [BHS07]
%          : poly_simplify4.p [Rue14]

% Status   : Theorem
% Rating   : 0.43 v6.4.0, 0.33 v6.3.0
% Syntax   : Number of formulae    :    4 (   2 unit;   2 type)
%            Number of atoms       :    2 (   2 equality)
%            Maximal formula depth :    2 (   2 average)
%            Number of connectives :    0 (   0   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    4 (   3 propositional; 0-2 arity)
%            Number of functors    :    5 (   3 constant; 0-2 arity)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?)
%                                         (   0   :;   0  !>;   0  ?*)
%            Maximal term depth    :    3 (   3 average)
%            Arithmetic symbols    :    3 (   0 prd;   2 fun;   1 num;   0 var)
% SPC      : TF0_THM_EQU_ARI

% Comments : KeY arithmetic regression test, http://www.key-project.org
%------------------------------------------------------------------------------
tff(d_type,type,(
    d: $int )).

tff(c_type,type,(
    c: $int )).

tff(eq,axiom,(
    $sum($product(d,d),c) = $product($product(2,d),d) )).

tff(conj,conjecture,(
    $product($product(c,d),d) = $product(c,c) )).

%------------------------------------------------------------------------------
