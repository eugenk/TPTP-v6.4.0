%--------------------------------------------------------------------------
% File     : BOO009-1 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Boolean Algebra
% Problem  : Multiplication absorption (X * (X + Y) = X)
% Version  : [MOW76] axioms.
% English  :

% Refs     : [Whi61] Whitesitt (1961), Boolean Algebra and Its Applications
%          : [MOW76] McCharen et al. (1976), Problems and Experiments for a
%          : [OMW76] Overbeek et al. (1976), Complexity and Related Enhance
% Source   : [MOW76]
% Names    : B4 part 1 [MOW76]
%          : Lemma proved [OMW76]
%          : prob4_part1.ver1 [ANL]

% Status   : Unsatisfiable
% Rating   : 0.00 v6.4.0, 0.14 v6.3.0, 0.00 v6.0.0, 0.22 v5.5.0, 0.38 v5.4.0, 0.33 v5.3.0, 0.50 v5.2.0, 0.25 v5.1.0, 0.14 v4.1.0, 0.00 v3.1.0, 0.11 v2.7.0, 0.00 v2.6.0, 0.29 v2.5.0, 0.00 v2.2.1, 0.22 v2.2.0, 0.14 v2.1.0, 0.20 v2.0.0
% Syntax   : Number of clauses     :   23 (   0 non-Horn;  11 unit;  13 RR)
%            Number of atoms       :   61 (   2 equality)
%            Maximal clause size   :    5 (   3 average)
%            Number of predicates  :    3 (   0 propositional; 2-3 arity)
%            Number of functors    :    7 (   4 constant; 0-2 arity)
%            Number of variables   :   82 (   0 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include boolean algebra axioms
include('Axioms/BOO002-0.ax').
%--------------------------------------------------------------------------
cnf(prove_equations,negated_conjecture,
    ( ~ product(x,add(x,y),x) )).

%--------------------------------------------------------------------------
