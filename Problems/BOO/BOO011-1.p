%--------------------------------------------------------------------------
% File     : BOO011-1 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Boolean Algebra
% Problem  : Inverse of additive identity = Multiplicative identity
% Version  : [MOW76] axioms.
% English  : The inverse of the additive identity is the multiplicative
%            identity.

% Refs     : [Whi61] Whitesitt (1961), Boolean Algebra and Its Applications
%          : [MOW76] McCharen et al. (1976), Problems and Experiments for a
% Source   : [MOW76]
% Names    : B7 [MOW76]
%          : prob7.ver1 [ANL]

% Status   : Unsatisfiable
% Rating   : 0.00 v5.3.0, 0.08 v5.2.0, 0.00 v2.0.0
% Syntax   : Number of clauses     :   23 (   0 non-Horn;  11 unit;  13 RR)
%            Number of atoms       :   61 (   3 equality)
%            Maximal clause size   :    5 (   3 average)
%            Number of predicates  :    3 (   0 propositional; 2-3 arity)
%            Number of functors    :    5 (   2 constant; 0-2 arity)
%            Number of variables   :   82 (   0 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include Boolean algebra axioms
include('Axioms/BOO002-0.ax').
%--------------------------------------------------------------------------
cnf(prove_equation,negated_conjecture,
    (  inverse(additive_identity) != multiplicative_identity )).

%--------------------------------------------------------------------------
