%--------------------------------------------------------------------------
% File     : BOO045-1 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Boolean Algebra
% Problem  : Single axiom C6 for Boolean algebra in the Sheffer stroke
% Version  : [EF+02] axioms.
% English  :

% Refs     : [EF+02] Ernst et al. (2002), More First-order Test Problems in
%          : [MV+02] McCune et al. (2002), Short Single Axioms for Boolean
% Source   : [EF+02]
% Names    : sheffer-cstar [EF+02]

% Status   : Open
% Rating   : 1.00 v2.5.0
% Syntax   : Number of clauses     :    2 (   0 non-Horn;   1 unit;   1 RR)
%            Number of atoms       :    3 (   3 equality)
%            Maximal clause size   :    2 (   2 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    4 (   3 constant; 0-2 arity)
%            Number of variables   :    3 (   0 singleton)
%            Maximal term depth    :    5 (   3 average)
% SPC      : CNF_OPN_RFO_PEQ_NUE

% Comments :
%--------------------------------------------------------------------------
%----C6
cnf(c6,axiom,
    ( nand(nand(A,nand(A,nand(B,C))),nand(C,nand(A,B))) = C )).

%----Denial of Meredith 2-basis
cnf(prove_meredith_2_basis,negated_conjecture,
    ( nand(nand(a,a),nand(b,a)) != a
    | nand(a,nand(b,nand(a,c))) != nand(nand(nand(c,b),b),a) )).

%--------------------------------------------------------------------------
