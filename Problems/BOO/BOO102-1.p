%--------------------------------------------------------------------------
% File     : BOO102-1 : TPTP v6.4.0. Released v2.6.0.
% Domain   : Boolean Algebra
% Problem  : Axiom C13 for Boolean algebra in the Sheffer stroke, part 2
% Version  : [EF+02] axioms.
% English  :

% Refs     : [EF+02] Ernst et al. (2002), More First-order Test Problems in
%          : [MV+02] McCune et al. (2002), Short Single Axioms for Boolean
% Source   : [TPTP]
% Names    :

% Status   : Unknown
% Rating   : 1.00 v2.6.0
% Syntax   : Number of clauses     :    2 (   0 non-Horn;   2 unit;   1 RR)
%            Number of atoms       :    2 (   2 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    4 (   3 constant; 0-2 arity)
%            Number of variables   :    3 (   1 singleton)
%            Maximal term depth    :    5 (   4 average)
% SPC      : CNF_UNK_RFO_PEQ_UEQ

% Comments : A UEQ part of BOO052-1
%--------------------------------------------------------------------------
cnf(c13,axiom,
    ( nand(nand(nand(nand(A,B),A),A),nand(B,nand(C,A))) = B )).

cnf(prove_meredith_2_basis_2,negated_conjecture,
    (  nand(a,nand(b,nand(a,c))) != nand(nand(nand(c,b),b),a) )).

%--------------------------------------------------------------------------
