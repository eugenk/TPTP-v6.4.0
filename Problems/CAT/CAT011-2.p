%--------------------------------------------------------------------------
% File     : CAT011-2 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Category Theory
% Problem  : domain(domain(x)) = domain(x)
% Version  : [Qua89] (equality) axioms.
% English  :

% Refs     : [Qua89] Quaife (1989), Email to L. Wos
% Source   : [ANL]
% Names    : p11.ver2.in [ANL]

% Status   : Unsatisfiable
% Rating   : 0.00 v6.3.0, 0.10 v6.2.0, 0.20 v6.1.0, 0.09 v6.0.0, 0.00 v5.3.0, 0.10 v5.2.0, 0.00 v2.0.0
% Syntax   : Number of clauses     :    8 (   0 non-Horn;   5 unit;   4 RR)
%            Number of atoms       :   12 (  12 equality)
%            Maximal clause size   :    3 (   2 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    4 (   1 constant; 0-2 arity)
%            Number of variables   :   11 (   0 singleton)
%            Maximal term depth    :    3 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
%--------------------------------------------------------------------------
%----Include Quaife's axioms for category theory
include('Axioms/CAT002-0.ax').
%--------------------------------------------------------------------------
cnf(prove_domain_is_idempotent,negated_conjecture,
    (  domain(domain(a)) != domain(a) )).

%--------------------------------------------------------------------------
