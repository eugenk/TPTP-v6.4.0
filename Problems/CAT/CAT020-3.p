%--------------------------------------------------------------------------
% File     : CAT020-3 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Category Theory
% Problem  : Category theory axioms
% Version  : [Sco79] axioms : Reduced > Complete.
% English  :

% Refs     : [Sco79] Scott (1979), Identity and Existence in Intuitionist L
% Source   : [ANL]
% Names    :

% Status   : Satisfiable
% Rating   : 0.29 v6.4.0, 0.14 v6.3.0, 0.12 v6.2.0, 0.10 v6.1.0, 0.22 v6.0.0, 0.14 v5.5.0, 0.12 v5.4.0, 0.40 v5.3.0, 0.33 v5.2.0, 0.40 v5.0.0, 0.44 v4.1.0, 0.43 v4.0.1, 0.40 v4.0.0, 0.25 v3.7.0, 0.33 v3.4.0, 0.50 v3.3.0, 0.00 v3.2.0, 0.20 v3.1.0, 0.00 v2.6.0, 0.33 v2.5.0
% Syntax   : Number of clauses     :   17 (   2 non-Horn;   3 unit;  12 RR)
%            Number of atoms       :   37 (  15 equality)
%            Maximal clause size   :    4 (   2 average)
%            Number of predicates  :    3 (   0 propositional; 1-2 arity)
%            Number of functors    :    4 (   0 constant; 1-2 arity)
%            Number of variables   :   31 (   4 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_SAT_RFO_EQU_NUE

% Comments :
%--------------------------------------------------------------------------
%----Include Category theory axioms
include('Axioms/CAT003-0.ax').
%--------------------------------------------------------------------------
