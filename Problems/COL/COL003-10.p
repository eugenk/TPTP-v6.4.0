%--------------------------------------------------------------------------
% File     : COL003-10 : TPTP v6.4.0. Released v1.2.0.
% Domain   : Combinatory Logic
% Problem  : Strong fixed point for B and W
% Version  : [WM88] (equality) axioms : Augmented > Especial.
%            Theorem formulation : The fixed point is provided and checked.
% English  : The strong fixed point property holds for the set
%            P consisting of the combinators B and W alone, where ((Bx)y)z
%            = x(yz) and (Wx)y = (xy)y.

% Refs     : [WM88]  Wos & McCune (1988), Challenge Problems Focusing on Eq
%          : [Wos93] Wos (1993), The Kernel Strategy and Its Use for the St
% Source   : [TPTP]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.29 v6.3.0, 0.33 v6.2.0, 0.00 v6.1.0, 0.20 v6.0.0, 0.56 v5.5.0, 0.75 v5.4.0, 0.73 v5.3.0, 0.75 v5.2.0, 0.50 v5.1.0, 0.43 v5.0.0, 0.57 v4.1.0, 0.56 v4.0.1, 0.50 v4.0.0, 0.67 v3.5.0, 0.50 v3.3.0, 0.43 v3.1.0, 0.56 v2.7.0, 0.33 v2.6.0, 0.43 v2.5.0, 0.40 v2.4.0, 0.50 v2.2.1, 0.88 v2.2.0, 0.83 v2.1.0, 1.00 v2.0.0
% Syntax   : Number of clauses     :    4 (   0 non-Horn;   3 unit;   2 RR)
%            Number of atoms       :    5 (   3 equality)
%            Maximal clause size   :    2 (   1 average)
%            Number of predicates  :    2 (   0 propositional; 1-2 arity)
%            Number of functors    :    4 (   3 constant; 0-2 arity)
%            Number of variables   :    6 (   0 singleton)
%            Maximal term depth    :    5 (   3 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
cnf(b_definition,axiom,
    ( apply(apply(apply(b,X),Y),Z) = apply(X,apply(Y,Z)) )).

cnf(w_definition,axiom,
    ( apply(apply(w,X),Y) = apply(apply(X,Y),Y) )).

cnf(strong_fixed_point,axiom,
    ( apply(Strong_fixed_point,fixed_pt) != apply(fixed_pt,apply(Strong_fixed_point,fixed_pt))
    | fixed_point(Strong_fixed_point) )).

cnf(prove_strong_fixed_point,negated_conjecture,
    ( ~ fixed_point(apply(apply(b,apply(w,w)),apply(apply(b,apply(b,w)),apply(apply(b,b),b)))) )).

%--------------------------------------------------------------------------
