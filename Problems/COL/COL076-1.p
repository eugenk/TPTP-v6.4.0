%--------------------------------------------------------------------------
% File     : COL076-1 : TPTP v6.4.0. Released v1.2.0.
% Domain   : Combinatory Logic
% Problem  : Lemma 2 for showing the unsatisfiable variant of TRC
% Version  : [Jec95] (equality) axioms : Augmented.
% English  : Searching for the self-referential combinator with the
%            property s = eq <k s,k p2>.

% Refs     : [Jec95] Jech (1995), Otter Experiments in a System of Combinat
% Source   : [Jec95]
% Names    : - [Jec95]

% Status   : Unsatisfiable
% Rating   : 0.85 v6.4.0, 0.86 v6.3.0, 0.80 v6.1.0, 0.82 v6.0.0, 0.86 v5.5.0, 0.88 v5.4.0, 0.89 v5.3.0, 0.90 v5.2.0, 0.75 v5.1.0, 0.89 v5.0.0, 0.80 v4.1.0, 0.89 v4.0.1, 0.88 v4.0.0, 0.86 v3.7.0, 0.57 v3.4.0, 0.67 v3.3.0, 0.56 v3.2.0, 0.67 v3.1.0, 0.60 v2.7.0, 0.88 v2.6.0, 0.83 v2.5.0, 0.75 v2.4.0, 1.00 v2.0.0
% Syntax   : Number of clauses     :   12 (   1 non-Horn;  10 unit;   3 RR)
%            Number of atoms       :   14 (  14 equality)
%            Maximal clause size   :    2 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    9 (   6 constant; 0-2 arity)
%            Number of variables   :   21 (   4 singleton)
%            Maximal term depth    :    4 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
%--------------------------------------------------------------------------
%----Don't include axioms of Type-respecting combinators
%include('Axioms/COL001-0.ax').
%--------------------------------------------------------------------------
%----Replace k function by k combinator
%input_clause(k_definition,axiom,
%    [++equal(apply(k(X),Y),X)]).
cnf(k_definition,axiom,
    ( apply(apply(k,X),Y) = X )).

cnf(projection1,axiom,
    ( apply(projection1,pair(X,Y)) = X )).

cnf(projection2,axiom,
    ( apply(projection2,pair(X,Y)) = Y )).

cnf(pairing,axiom,
    ( pair(apply(projection1,X),apply(projection2,X)) = X )).

cnf(pairwise_application,axiom,
    ( apply(pair(X,Y),Z) = pair(apply(X,Z),apply(Y,Z)) )).

%----Replace k function by k combinator
%input_clause(abstraction,axiom,
%    [++equal(apply(apply(apply(abstraction,X),Y),Z),apply(apply(X,k(Z)),
%apply(Y,Z)))]).
cnf(abstraction,axiom,
    ( apply(apply(apply(abstraction,X),Y),Z) = apply(apply(X,apply(k,Z)),apply(Y,Z)) )).

cnf(equality,axiom,
    ( apply(eq,pair(X,X)) = projection1 )).

cnf(extensionality1,axiom,
    ( X = Y
    | apply(eq,pair(X,Y)) = projection2 )).

cnf(extensionality2,axiom,
    ( X = Y
    | apply(X,n(X,Y)) != apply(Y,n(X,Y)) )).

cnf(different_projections,axiom,
    (  projection1 != projection2 )).

%----Subsitution axioms
%----This is the extra lemma
cnf(diagonal_combinator,axiom,
    ( apply(apply(f,X),Y) = apply(X,X) )).

cnf(prove_self_referential,negated_conjecture,
    (  Y != apply(eq,pair(apply(k,Y),apply(k,projection2))) )).

%--------------------------------------------------------------------------
