%--------------------------------------------------------------------------
% File     : COL079-2 : TPTP v6.4.0. Released v1.2.0.
% Domain   : Combinatory Logic
% Problem  : Abst(Abst(Abst X)) = Abst X
% Version  : [Jec95] (equality) axioms : Reduced > Incomplete.
% English  :

% Refs     : [Jec95] Jech (1995), Otter Experiments in a System of Combinat
% Source   : [TPTP]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.62 v6.4.0, 0.64 v6.3.0, 0.60 v6.2.0, 0.50 v6.1.0, 0.55 v6.0.0, 0.57 v5.5.0, 0.62 v5.4.0, 0.56 v5.3.0, 0.60 v5.2.0, 0.50 v5.1.0, 0.56 v5.0.0, 0.60 v4.1.0, 0.56 v4.0.1, 0.62 v4.0.0, 0.57 v3.7.0, 0.43 v3.4.0, 0.33 v3.3.0, 0.44 v3.2.0, 0.56 v3.1.0, 0.60 v2.7.0, 0.50 v2.6.0, 0.33 v2.5.0, 0.25 v2.4.0, 0.75 v2.3.0, 0.50 v2.2.1, 0.67 v2.2.0, 0.50 v2.1.0, 1.00 v2.0.0
% Syntax   : Number of clauses     :    4 (   0 non-Horn;   3 unit;   2 RR)
%            Number of atoms       :    5 (   5 equality)
%            Maximal clause size   :    2 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    5 (   2 constant; 0-2 arity)
%            Number of variables   :    7 (   1 singleton)
%            Maximal term depth    :    4 (   3 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments : Reduced to use only the axioms used in [Jec95].
%--------------------------------------------------------------------------
%----Include axioms of Type-respecting combinators
%include('Axioms/COL001-0.ax').
%--------------------------------------------------------------------------
cnf(k_definition,axiom,
    ( apply(k(X),Y) = X )).

cnf(abstraction,axiom,
    ( apply(apply(apply(abstraction,X),Y),Z) = apply(apply(X,k(Z)),apply(Y,Z)) )).

cnf(extensionality2,axiom,
    ( X = Y
    | apply(X,n(X,Y)) != apply(Y,n(X,Y)) )).

%----Subsitution axioms
cnf(prove_TRC2a,negated_conjecture,
    (  apply(abstraction,apply(abstraction,apply(abstraction,b))) != apply(abstraction,b) )).

%--------------------------------------------------------------------------
