%------------------------------------------------------------------------------
% File     : COM082_5 : TPTP v6.4.0. Released v6.0.0.
% Domain   : Number Theory
% Problem  : Quantifier elimination for Presburger arithmetic line 191
% Version  : Especial.
% English  : 

% Refs     : [BN10]  Boehme & Nipkow (2010), Sledgehammer: Judgement Day
%          : [Nip08] Nipkow (2008), Linear Quantifier Elimination
%          : [Bla13] Blanchette (2011), Email to Geoff Sutcliffe
% Source   : [Bla13]
% Names    : qe_191 [Bla13]

% Status   : Unknown
% Rating   : 1.00 v6.4.0
% Syntax   : Number of formulae    :  142 (  77 unit;  39 type)
%            Number of atoms       :  138 (  92 equality)
%            Maximal formula depth :    9 (   4 average)
%            Number of connectives :   86 (  51   ~;   2   |;   5   &)
%                                         (  20 <=>;   8  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   39 (  24   >;  15   *;   0   +;   0  <<)
%            Number of predicates  :   51 (  43 propositional; 0-5 arity)
%            Number of functors    :   35 (  10 constant; 0-5 arity)
%            Number of variables   :  310 (   2 sgn; 280   !;   3   ?)
%                                         ( 310   :;  27  !>;   0  ?*)
%            Maximal term depth    :    5 (   2 average)
% SPC      : TF1_UNK_EQU_NAR

% Comments : This file was generated by Isabelle (most likely Sledgehammer)
%            2011-12-13 16:21:35
%------------------------------------------------------------------------------
%----Should-be-implicit typings (6)
tff(ty_tc_HOL_Obool,type,(
    bool: $tType )).

tff(ty_tc_Int_Oint,type,(
    int: $tType )).

tff(ty_tc_List_Olist,type,(
    list: $tType > $tType )).

tff(ty_tc_Logic_Ofm,type,(
    fm: $tType > $tType )).

tff(ty_tc_PresArith_Oatom,type,(
    atom: $tType )).

tff(ty_tc_fun,type,(
    fun: ( $tType * $tType ) > $tType )).

%----Explicit typings (33)
tff(sy_cl_Groups_Ozero,type,(
    zero: 
      !>[A: $tType] : $o )).

tff(sy_c_Groups_Ozero__class_Ozero,type,(
    zero_zero: 
      !>[A: $tType] : A )).

tff(sy_c_List_Ofoldr,type,(
    foldr: 
      !>[A: $tType,B: $tType] :
        ( ( fun(A,fun(B,B)) * list(A) * B ) > B ) )).

tff(sy_c_List_Olist_OCons,type,(
    cons: 
      !>[A: $tType] :
        ( ( A * list(A) ) > list(A) ) )).

tff(sy_c_List_Oset,type,(
    set: 
      !>[A: $tType] :
        ( list(A) > fun(A,bool) ) )).

tff(sy_c_Logic_OATOM_Onnf,type,(
    nnf: 
      !>[A: $tType] :
        ( ( fun(A,fm(A)) * fm(A) ) > fm(A) ) )).

tff(sy_c_Logic_Oamap__fm,type,(
    amap_fm: 
      !>[A: $tType,B: $tType] :
        ( ( fun(A,fm(B)) * fm(A) ) > fm(B) ) )).

tff(sy_c_Logic_Oand,type,(
    c_Logic_Oand: 
      !>[A: $tType] : fun(fm(A),fun(fm(A),fm(A))) )).

tff(sy_c_Logic_Ofm_OAnd,type,(
    c_Logic_Ofm_OAnd: 
      !>[A: $tType] :
        ( ( fm(A) * fm(A) ) > fm(A) ) )).

tff(sy_c_Logic_Ofm_OAtom,type,(
    atom1: 
      !>[A: $tType] :
        ( A > fm(A) ) )).

tff(sy_c_Logic_Ofm_OExQ,type,(
    exQ: 
      !>[A: $tType] :
        ( fm(A) > fm(A) ) )).

tff(sy_c_Logic_Ofm_OFalseF,type,(
    falseF: 
      !>[A: $tType] : fm(A) )).

tff(sy_c_Logic_Ofm_ONeg,type,(
    neg: 
      !>[A: $tType] :
        ( fm(A) > fm(A) ) )).

tff(sy_c_Logic_Ofm_OOr,type,(
    c_Logic_Ofm_OOr: 
      !>[A: $tType] :
        ( ( fm(A) * fm(A) ) > fm(A) ) )).

tff(sy_c_Logic_Ofm_OTrueF,type,(
    trueF: 
      !>[A: $tType] : fm(A) )).

tff(sy_c_Logic_Ointerpret,type,(
    interpret: 
      !>[A: $tType,B: $tType] :
        ( ( fun(A,fun(list(B),bool)) * fm(A) * list(B) ) > $o ) )).

tff(sy_c_Logic_Olist__conj,type,(
    list_conj: 
      !>[A: $tType] :
        ( list(fm(A)) > fm(A) ) )).

tff(sy_c_Logic_Olist__disj,type,(
    list_disj: 
      !>[A: $tType] :
        ( list(fm(A)) > fm(A) ) )).

tff(sy_c_Logic_Oneg,type,(
    neg1: 
      !>[A: $tType] :
        ( fm(A) > fm(A) ) )).

tff(sy_c_Logic_Oor,type,(
    c_Logic_Oor: 
      !>[A: $tType] :
        ( ( fm(A) * fm(A) ) > fm(A) ) )).

tff(sy_c_PresArith_OI_092_060_094isub_062Z,type,(
    i_Z: fun(atom,fun(list(int),bool)) )).

tff(sy_c_PresArith_Odivisor,type,(
    divisor: atom > int )).

tff(sy_c_PresArith_Oneg_092_060_094isub_062Z,type,(
    neg_Z: fun(atom,fm(atom)) )).

tff(sy_c_QE_OATOM_Olift__nnf__qe,type,(
    lift_nnf_qe: 
      !>[A: $tType] :
        ( ( fun(A,fm(A)) * fun(fm(A),fm(A)) * fm(A) ) > fm(A) ) )).

tff(sy_c_QEpres__Mirabelle__iocckttzyp_Oqe__pres_092_060_094isub_0621,type,(
    qEpres896714165pres_1: list(atom) > fm(atom) )).

tff(sy_c_aa,type,(
    aa: 
      !>[A: $tType,B: $tType] :
        ( ( fun(A,B) * A ) > B ) )).

tff(sy_c_fFalse,type,(
    fFalse: bool )).

tff(sy_c_fTrue,type,(
    fTrue: bool )).

tff(sy_c_member,type,(
    member: 
      !>[A: $tType] :
        ( ( A * fun(A,bool) ) > $o ) )).

tff(sy_c_pp,type,(
    pp: bool > $o )).

tff(sy_v_as,type,(
    as: list(atom) )).

tff(sy_v_x____,type,(
    x: int )).

tff(sy_v_xs,type,(
    xs: list(int) )).

%----Relevant facts (99)
tff(fact_0_x,axiom,(
    ! [X3: atom] :
      ( member(atom,X3,set(atom,as))
     => pp(aa(list(int),bool,aa(atom,fun(list(int),bool),i_Z,X3),cons(int,x,xs))) ) )).

tff(fact_1__096EX_Ax_O_AALL_Aa_058set_Aas_O_AI_092_060_094isub_062Z_Aa_A_Ix_A_D_Axs_J_096,axiom,(
    ? [X: int] :
    ! [Xa1: atom] :
      ( member(atom,Xa1,set(atom,as))
     => pp(aa(list(int),bool,aa(atom,fun(list(int),bool),i_Z,Xa1),cons(int,X,xs))) ) )).

tff(fact_2__096_B_Bthesis_O_A_I_B_Bx_O_AALL_Aa_058set_Aas_O_AI_092_060_094isub_062Z_Aa_A_Ix_A_D_Axs_J_A_061_061_062_Athesis_J_A_061_061_062_Athesis_096,axiom,(
    ~ ! [X: int] :
        ~ ! [Xa1: atom] :
            ( member(atom,Xa1,set(atom,as))
           => pp(aa(list(int),bool,aa(atom,fun(list(int),bool),i_Z,Xa1),cons(int,X,xs))) ) )).

tff(fact_3_Z_OI_092_060_094isub_062a__aneg,axiom,(
    ! [Xsa: list(int),A4: atom] :
      ( interpret(atom,int,i_Z,aa(atom,fm(atom),neg_Z,A4),Xsa)
    <=> ~ pp(aa(list(int),bool,aa(atom,fun(list(int),bool),i_Z,A4),Xsa)) ) )).

tff(fact_4_norm,axiom,(
    ! [X3: atom] :
      ( member(atom,X3,set(atom,as))
     => divisor(X3) != zero_zero(int) ) )).

tff(fact_5_Z_OI__list__conj,axiom,(
    ! [Xsa: list(int),Fs: list(fm(atom))] :
      ( interpret(atom,int,i_Z,list_conj(atom,Fs),Xsa)
    <=> ! [X1: fm(atom)] :
          ( member(fm(atom),X1,set(fm(atom),Fs))
         => interpret(atom,int,i_Z,X1,Xsa) ) ) )).

tff(fact_6_Z_OI__and,axiom,(
    ! [Xsa: list(int),Phi_2: fm(atom),Phi_1: fm(atom)] :
      ( interpret(atom,int,i_Z,aa(fm(atom),fm(atom),aa(fm(atom),fun(fm(atom),fm(atom)),c_Logic_Oand(atom),Phi_1),Phi_2),Xsa)
    <=> interpret(atom,int,i_Z,c_Logic_Ofm_OAnd(atom,Phi_1,Phi_2),Xsa) ) )).

tff(fact_7_Z_OI__or,axiom,(
    ! [Xsa: list(int),Phi_2: fm(atom),Phi_1: fm(atom)] :
      ( interpret(atom,int,i_Z,c_Logic_Oor(atom,Phi_1,Phi_2),Xsa)
    <=> interpret(atom,int,i_Z,c_Logic_Ofm_OOr(atom,Phi_1,Phi_2),Xsa) ) )).

tff(fact_8_Z_OI__neg,axiom,(
    ! [Xsa: list(int),Phi: fm(atom)] :
      ( interpret(atom,int,i_Z,neg1(atom,Phi),Xsa)
    <=> interpret(atom,int,i_Z,neg(atom,Phi),Xsa) ) )).

tff(fact_9_Z_OI__list__disj,axiom,(
    ! [Xsa: list(int),Fs: list(fm(atom))] :
      ( interpret(atom,int,i_Z,list_disj(atom,Fs),Xsa)
    <=> ? [X1: fm(atom)] :
          ( member(fm(atom),X1,set(fm(atom),Fs))
          & interpret(atom,int,i_Z,X1,Xsa) ) ) )).

tff(fact_10_interpret_Osimps_I3_J,axiom,(
    ! [A: $tType,B: $tType,Xsa: list(B),A4: A,H: fun(A,fun(list(B),bool))] :
      ( interpret(A,B,H,atom1(A,A4),Xsa)
    <=> pp(aa(list(B),bool,aa(A,fun(list(B),bool),H,A4),Xsa)) ) )).

tff(fact_11_interpret_Osimps_I1_J,axiom,(
    ! [A: $tType,B: $tType,Xsa: list(B),H: fun(A,fun(list(B),bool))] : interpret(A,B,H,trueF(A),Xsa) )).

tff(fact_12_fm_Osimps_I4_J,axiom,(
    ! [A: $tType,Fm5: fm(A),Fm4: fm(A)] :
      ( neg(A,Fm4) = neg(A,Fm5)
    <=> Fm4 = Fm5 ) )).

tff(fact_13_fm_Osimps_I1_J,axiom,(
    ! [A: $tType,A5: A,A4: A] :
      ( atom1(A,A4) = atom1(A,A5)
    <=> A4 = A5 ) )).

tff(fact_14_fm_Osimps_I3_J,axiom,(
    ! [A: $tType,Fm23: fm(A),Fm13: fm(A),Fm22: fm(A),Fm12: fm(A)] :
      ( c_Logic_Ofm_OOr(A,Fm12,Fm22) = c_Logic_Ofm_OOr(A,Fm13,Fm23)
    <=> ( Fm12 = Fm13
        & Fm22 = Fm23 ) ) )).

tff(fact_15_fm_Osimps_I2_J,axiom,(
    ! [A: $tType,Fm23: fm(A),Fm13: fm(A),Fm22: fm(A),Fm12: fm(A)] :
      ( c_Logic_Ofm_OAnd(A,Fm12,Fm22) = c_Logic_Ofm_OAnd(A,Fm13,Fm23)
    <=> ( Fm12 = Fm13
        & Fm22 = Fm23 ) ) )).

tff(fact_16_interpret_Osimps_I6_J,axiom,(
    ! [A: $tType,B: $tType,Xsa: list(B),Phi: fm(A),H: fun(A,fun(list(B),bool))] :
      ( interpret(A,B,H,neg(A,Phi),Xsa)
    <=> ~ interpret(A,B,H,Phi,Xsa) ) )).

tff(fact_17_interpret_Osimps_I5_J,axiom,(
    ! [A: $tType,B: $tType,Xsa: list(B),Phi_2: fm(A),Phi_1: fm(A),H: fun(A,fun(list(B),bool))] :
      ( interpret(A,B,H,c_Logic_Ofm_OOr(A,Phi_1,Phi_2),Xsa)
    <=> ( interpret(A,B,H,Phi_1,Xsa)
        | interpret(A,B,H,Phi_2,Xsa) ) ) )).

tff(fact_18_interpret_Osimps_I4_J,axiom,(
    ! [A: $tType,B: $tType,Xsa: list(B),Phi_2: fm(A),Phi_1: fm(A),H: fun(A,fun(list(B),bool))] :
      ( interpret(A,B,H,c_Logic_Ofm_OAnd(A,Phi_1,Phi_2),Xsa)
    <=> ( interpret(A,B,H,Phi_1,Xsa)
        & interpret(A,B,H,Phi_2,Xsa) ) ) )).

tff(fact_19_fm_Osimps_I14_J,axiom,(
    ! [A: $tType,Fm: fm(A)] : trueF(A) != neg(A,Fm) )).

tff(fact_20_fm_Osimps_I8_J,axiom,(
    ! [A: $tType,A2: A] : trueF(A) != atom1(A,A2) )).

tff(fact_21_fm_Osimps_I12_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A)] : trueF(A) != c_Logic_Ofm_OOr(A,Fm11,Fm21) )).

tff(fact_22_fm_Osimps_I10_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A)] : trueF(A) != c_Logic_Ofm_OAnd(A,Fm11,Fm21) )).

tff(fact_23_fm_Osimps_I15_J,axiom,(
    ! [A: $tType,Fm: fm(A)] : neg(A,Fm) != trueF(A) )).

tff(fact_24_fm_Osimps_I9_J,axiom,(
    ! [A: $tType,A2: A] : atom1(A,A2) != trueF(A) )).

tff(fact_25_fm_Osimps_I33_J,axiom,(
    ! [A: $tType,A1: A,Fm: fm(A)] : neg(A,Fm) != atom1(A,A1) )).

tff(fact_26_fm_Osimps_I32_J,axiom,(
    ! [A: $tType,Fm: fm(A),A1: A] : atom1(A,A1) != neg(A,Fm) )).

tff(fact_27_fm_Osimps_I43_J,axiom,(
    ! [A: $tType,Fm2: fm(A),Fm1: fm(A),Fm: fm(A)] : neg(A,Fm) != c_Logic_Ofm_OOr(A,Fm1,Fm2) )).

tff(fact_28_fm_Osimps_I39_J,axiom,(
    ! [A: $tType,Fm2: fm(A),Fm1: fm(A),Fm: fm(A)] : neg(A,Fm) != c_Logic_Ofm_OAnd(A,Fm1,Fm2) )).

tff(fact_29_fm_Osimps_I30_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A),A1: A] : atom1(A,A1) != c_Logic_Ofm_OOr(A,Fm11,Fm21) )).

tff(fact_30_fm_Osimps_I28_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A),A1: A] : atom1(A,A1) != c_Logic_Ofm_OAnd(A,Fm11,Fm21) )).

tff(fact_31_fm_Osimps_I13_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A)] : c_Logic_Ofm_OOr(A,Fm11,Fm21) != trueF(A) )).

tff(fact_32_fm_Osimps_I11_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A)] : c_Logic_Ofm_OAnd(A,Fm11,Fm21) != trueF(A) )).

tff(fact_33_fm_Osimps_I42_J,axiom,(
    ! [A: $tType,Fm: fm(A),Fm2: fm(A),Fm1: fm(A)] : c_Logic_Ofm_OOr(A,Fm1,Fm2) != neg(A,Fm) )).

tff(fact_34_fm_Osimps_I31_J,axiom,(
    ! [A: $tType,A1: A,Fm21: fm(A),Fm11: fm(A)] : c_Logic_Ofm_OOr(A,Fm11,Fm21) != atom1(A,A1) )).

tff(fact_35_fm_Osimps_I38_J,axiom,(
    ! [A: $tType,Fm: fm(A),Fm2: fm(A),Fm1: fm(A)] : c_Logic_Ofm_OAnd(A,Fm1,Fm2) != neg(A,Fm) )).

tff(fact_36_fm_Osimps_I29_J,axiom,(
    ! [A: $tType,A1: A,Fm21: fm(A),Fm11: fm(A)] : c_Logic_Ofm_OAnd(A,Fm11,Fm21) != atom1(A,A1) )).

tff(fact_37_fm_Osimps_I37_J,axiom,(
    ! [A: $tType,Fm2: fm(A),Fm1: fm(A),Fm21: fm(A),Fm11: fm(A)] : c_Logic_Ofm_OOr(A,Fm11,Fm21) != c_Logic_Ofm_OAnd(A,Fm1,Fm2) )).

tff(fact_38_fm_Osimps_I36_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A),Fm2: fm(A),Fm1: fm(A)] : c_Logic_Ofm_OAnd(A,Fm1,Fm2) != c_Logic_Ofm_OOr(A,Fm11,Fm21) )).

tff(fact_39_list_Oinject,axiom,(
    ! [A: $tType,List1: list(A),A5: A,List: list(A),A4: A] :
      ( cons(A,A4,List) = cons(A,A5,List1)
    <=> ( A4 = A5
        & List = List1 ) ) )).

tff(fact_40_set__ConsD,axiom,(
    ! [A: $tType,Xsa: list(A),Xa: A,Y: A] :
      ( member(A,Y,set(A,cons(A,Xa,Xsa)))
     => ( Y = Xa
        | member(A,Y,set(A,Xsa)) ) ) )).

tff(fact_41_Z_Onnf_Osimps_I7_J,axiom,(
    ! [Phi_2: fm(atom),Phi_1: fm(atom)] : nnf(atom,neg_Z,neg(atom,c_Logic_Ofm_OOr(atom,Phi_1,Phi_2))) = c_Logic_Ofm_OAnd(atom,nnf(atom,neg_Z,neg(atom,Phi_1)),nnf(atom,neg_Z,neg(atom,Phi_2))) )).

tff(fact_42_Z_Onnf_Osimps_I6_J,axiom,(
    ! [Phi_2: fm(atom),Phi_1: fm(atom)] : nnf(atom,neg_Z,neg(atom,c_Logic_Ofm_OAnd(atom,Phi_1,Phi_2))) = c_Logic_Ofm_OOr(atom,nnf(atom,neg_Z,neg(atom,Phi_1)),nnf(atom,neg_Z,neg(atom,Phi_2))) )).

tff(fact_43_Z_Olift__nnf__qe_Osimps_I3_J,axiom,(
    ! [Phi: fm(atom),Qe: fun(fm(atom),fm(atom))] : lift_nnf_qe(atom,neg_Z,Qe,neg(atom,Phi)) = neg1(atom,lift_nnf_qe(atom,neg_Z,Qe,Phi)) )).

tff(fact_44_Z_Olift__nnf__qe_Osimps_I2_J,axiom,(
    ! [Phi_2: fm(atom),Phi_1: fm(atom),Qe: fun(fm(atom),fm(atom))] : lift_nnf_qe(atom,neg_Z,Qe,c_Logic_Ofm_OOr(atom,Phi_1,Phi_2)) = c_Logic_Oor(atom,lift_nnf_qe(atom,neg_Z,Qe,Phi_1),lift_nnf_qe(atom,neg_Z,Qe,Phi_2)) )).

tff(fact_45_Z_Olift__nnf__qe_Osimps_I1_J,axiom,(
    ! [Phi_2: fm(atom),Phi_1: fm(atom),Qe: fun(fm(atom),fm(atom))] : lift_nnf_qe(atom,neg_Z,Qe,c_Logic_Ofm_OAnd(atom,Phi_1,Phi_2)) = aa(fm(atom),fm(atom),aa(fm(atom),fun(fm(atom),fm(atom)),c_Logic_Oand(atom),lift_nnf_qe(atom,neg_Z,Qe,Phi_1)),lift_nnf_qe(atom,neg_Z,Qe,Phi_2)) )).

tff(fact_46_amap__fm_Osimps_I6_J,axiom,(
    ! [A: $tType,B: $tType,Phi: fm(B),H: fun(B,fm(A))] : amap_fm(B,A,H,neg(B,Phi)) = neg1(A,amap_fm(B,A,H,Phi)) )).

tff(fact_47_Z_Onnf_Osimps_I11_J,axiom,(
    ! [V: atom] : nnf(atom,neg_Z,atom1(atom,V)) = atom1(atom,V) )).

tff(fact_48_Z_Onnf_Osimps_I9_J,axiom,(
    nnf(atom,neg_Z,trueF(atom)) = trueF(atom) )).

tff(fact_49_Z_Olift__nnf__qe_Osimps_I7_J,axiom,(
    ! [V: atom,Qe: fun(fm(atom),fm(atom))] : lift_nnf_qe(atom,neg_Z,Qe,atom1(atom,V)) = atom1(atom,V) )).

tff(fact_50_Z_Olift__nnf__qe_Osimps_I5_J,axiom,(
    ! [Qe: fun(fm(atom),fm(atom))] : lift_nnf_qe(atom,neg_Z,Qe,trueF(atom)) = trueF(atom) )).

tff(fact_51_amap__fm_Osimps_I1_J,axiom,(
    ! [B: $tType,A: $tType,H: fun(B,fm(A))] : amap_fm(B,A,H,trueF(B)) = trueF(A) )).

tff(fact_52_amap__fm_Osimps_I3_J,axiom,(
    ! [A: $tType,B: $tType,A4: B,H: fun(B,fm(A))] : amap_fm(B,A,H,atom1(B,A4)) = aa(B,fm(A),H,A4) )).

tff(fact_53_Z_Onnf_Osimps_I5_J,axiom,(
    ! [Phi: fm(atom)] : nnf(atom,neg_Z,neg(atom,neg(atom,Phi))) = nnf(atom,neg_Z,Phi) )).

tff(fact_54_Z_Onnf_Osimps_I8_J,axiom,(
    ! [A4: atom] : nnf(atom,neg_Z,neg(atom,atom1(atom,A4))) = aa(atom,fm(atom),neg_Z,A4) )).

tff(fact_55_Z_Onnf_Osimps_I1_J,axiom,(
    ! [Phi_2: fm(atom),Phi_1: fm(atom)] : nnf(atom,neg_Z,c_Logic_Ofm_OAnd(atom,Phi_1,Phi_2)) = c_Logic_Ofm_OAnd(atom,nnf(atom,neg_Z,Phi_1),nnf(atom,neg_Z,Phi_2)) )).

tff(fact_56_Z_Onnf_Osimps_I2_J,axiom,(
    ! [Phi_2: fm(atom),Phi_1: fm(atom)] : nnf(atom,neg_Z,c_Logic_Ofm_OOr(atom,Phi_1,Phi_2)) = c_Logic_Ofm_OOr(atom,nnf(atom,neg_Z,Phi_1),nnf(atom,neg_Z,Phi_2)) )).

tff(fact_57_amap__fm_Osimps_I5_J,axiom,(
    ! [A: $tType,B: $tType,Phi_2: fm(B),Phi_1: fm(B),H: fun(B,fm(A))] : amap_fm(B,A,H,c_Logic_Ofm_OOr(B,Phi_1,Phi_2)) = c_Logic_Oor(A,amap_fm(B,A,H,Phi_1),amap_fm(B,A,H,Phi_2)) )).

tff(fact_58_amap__fm_Osimps_I4_J,axiom,(
    ! [A: $tType,B: $tType,Phi_2: fm(B),Phi_1: fm(B),H: fun(B,fm(A))] : amap_fm(B,A,H,c_Logic_Ofm_OAnd(B,Phi_1,Phi_2)) = aa(fm(A),fm(A),aa(fm(A),fun(fm(A),fm(A)),c_Logic_Oand(A),amap_fm(B,A,H,Phi_1)),amap_fm(B,A,H,Phi_2)) )).

tff(fact_59_zero__reorient,axiom,(
    ! [A: $tType] :
      ( zero(A)
     => ! [Xa: A] :
          ( zero_zero(A) = Xa
        <=> Xa = zero_zero(A) ) ) )).

tff(fact_60_not__Cons__self2,axiom,(
    ! [A: $tType,Xs: list(A),X2: A] : cons(A,X2,Xs) != Xs )).

tff(fact_61_not__Cons__self,axiom,(
    ! [A: $tType,X2: A,Xs: list(A)] : Xs != cons(A,X2,Xs) )).

tff(fact_62_Z_OI__nnf,axiom,(
    ! [Xsa: list(int),Phi: fm(atom)] :
      ( interpret(atom,int,i_Z,nnf(atom,neg_Z,Phi),Xsa)
    <=> interpret(atom,int,i_Z,Phi,Xsa) ) )).

tff(fact_63_list__conj__def,axiom,(
    ! [A: $tType,Fs: list(fm(A))] : list_conj(A,Fs) = foldr(fm(A),fm(A),c_Logic_Oand(A),Fs,trueF(A)) )).

tff(fact_64_Z_Onnf_Osimps_I12_J,axiom,(
    ! [Va: fm(atom)] : nnf(atom,neg_Z,neg(atom,exQ(atom,Va))) = neg(atom,exQ(atom,Va)) )).

tff(fact_65_Z_Olift__nnf__qe_Osimps_I4_J,axiom,(
    ! [Phi: fm(atom),Qe: fun(fm(atom),fm(atom))] : lift_nnf_qe(atom,neg_Z,Qe,exQ(atom,Phi)) = aa(fm(atom),fm(atom),Qe,nnf(atom,neg_Z,lift_nnf_qe(atom,neg_Z,Qe,Phi))) )).

tff(fact_66_Z_Onnf_Osimps_I4_J,axiom,(
    nnf(atom,neg_Z,neg(atom,falseF(atom))) = trueF(atom) )).

tff(fact_67_fm_Osimps_I5_J,axiom,(
    ! [A: $tType,Fm5: fm(A),Fm4: fm(A)] :
      ( exQ(A,Fm4) = exQ(A,Fm5)
    <=> Fm4 = Fm5 ) )).

tff(fact_68_interpret_Osimps_I2_J,axiom,(
    ! [A: $tType,B: $tType,Xsa: list(B),H: fun(A,fun(list(B),bool))] : ~ interpret(A,B,H,falseF(A),Xsa) )).

tff(fact_69_amap__fm_Osimps_I2_J,axiom,(
    ! [B: $tType,A: $tType,H: fun(B,fm(A))] : amap_fm(B,A,H,falseF(B)) = falseF(A) )).

tff(fact_70_interpret_Osimps_I7_J,axiom,(
    ! [A: $tType,B: $tType,Xsa: list(B),Phi: fm(A),H: fun(A,fun(list(B),bool))] :
      ( interpret(A,B,H,exQ(A,Phi),Xsa)
    <=> ? [X1: B] : interpret(A,B,H,Phi,cons(B,X1,Xsa)) ) )).

tff(fact_71_Z_Onnf_Osimps_I10_J,axiom,(
    nnf(atom,neg_Z,falseF(atom)) = falseF(atom) )).

tff(fact_72_Z_Olift__nnf__qe_Osimps_I6_J,axiom,(
    ! [Qe: fun(fm(atom),fm(atom))] : lift_nnf_qe(atom,neg_Z,Qe,falseF(atom)) = falseF(atom) )).

tff(fact_73_Z_Onnf_Osimps_I13_J,axiom,(
    ! [V: fm(atom)] : nnf(atom,neg_Z,exQ(atom,V)) = exQ(atom,V) )).

tff(fact_74_ext,axiom,(
    ! [B: $tType,A: $tType,G: fun(A,B),F: fun(A,B)] :
      ( ! [X: A] : aa(A,B,F,X) = aa(A,B,G,X)
     => F = G ) )).

tff(fact_75_mem__def,axiom,(
    ! [A: $tType,A3: fun(A,bool),Xa: A] :
      ( member(A,Xa,A3)
    <=> pp(aa(A,bool,A3,Xa)) ) )).

tff(fact_76_Z_Onnf_Osimps_I3_J,axiom,(
    nnf(atom,neg_Z,neg(atom,trueF(atom))) = falseF(atom) )).

tff(fact_77_fm_Osimps_I27_J,axiom,(
    ! [A: $tType,Fm: fm(A)] : exQ(A,Fm) != falseF(A) )).

tff(fact_78_fm_Osimps_I26_J,axiom,(
    ! [A: $tType,Fm: fm(A)] : falseF(A) != exQ(A,Fm) )).

tff(fact_79_fm_Osimps_I24_J,axiom,(
    ! [A: $tType,Fm: fm(A)] : falseF(A) != neg(A,Fm) )).

tff(fact_80_fm_Osimps_I25_J,axiom,(
    ! [A: $tType,Fm: fm(A)] : neg(A,Fm) != falseF(A) )).

tff(fact_81_fm_Osimps_I6_J,axiom,(
    ! [A: $tType] : trueF(A) != falseF(A) )).

tff(fact_82_fm_Osimps_I7_J,axiom,(
    ! [A: $tType] : falseF(A) != trueF(A) )).

tff(fact_83_fm_Osimps_I18_J,axiom,(
    ! [A: $tType,A2: A] : falseF(A) != atom1(A,A2) )).

tff(fact_84_fm_Osimps_I19_J,axiom,(
    ! [A: $tType,A2: A] : atom1(A,A2) != falseF(A) )).

tff(fact_85_fm_Osimps_I22_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A)] : falseF(A) != c_Logic_Ofm_OOr(A,Fm11,Fm21) )).

tff(fact_86_fm_Osimps_I23_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A)] : c_Logic_Ofm_OOr(A,Fm11,Fm21) != falseF(A) )).

tff(fact_87_fm_Osimps_I20_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A)] : falseF(A) != c_Logic_Ofm_OAnd(A,Fm11,Fm21) )).

tff(fact_88_fm_Osimps_I21_J,axiom,(
    ! [A: $tType,Fm21: fm(A),Fm11: fm(A)] : c_Logic_Ofm_OAnd(A,Fm11,Fm21) != falseF(A) )).

tff(fact_89_fm_Osimps_I47_J,axiom,(
    ! [A: $tType,Fm3: fm(A),Fm: fm(A)] : exQ(A,Fm) != neg(A,Fm3) )).

tff(fact_90_fm_Osimps_I46_J,axiom,(
    ! [A: $tType,Fm: fm(A),Fm3: fm(A)] : neg(A,Fm3) != exQ(A,Fm) )).

tff(fact_91_fm_Osimps_I16_J,axiom,(
    ! [A: $tType,Fm: fm(A)] : trueF(A) != exQ(A,Fm) )).

tff(fact_92_fm_Osimps_I17_J,axiom,(
    ! [A: $tType,Fm: fm(A)] : exQ(A,Fm) != trueF(A) )).

tff(fact_93_fm_Osimps_I35_J,axiom,(
    ! [A: $tType,A1: A,Fm: fm(A)] : exQ(A,Fm) != atom1(A,A1) )).

tff(fact_94_fm_Osimps_I34_J,axiom,(
    ! [A: $tType,Fm: fm(A),A1: A] : atom1(A,A1) != exQ(A,Fm) )).

tff(fact_95_fm_Osimps_I45_J,axiom,(
    ! [A: $tType,Fm2: fm(A),Fm1: fm(A),Fm: fm(A)] : exQ(A,Fm) != c_Logic_Ofm_OOr(A,Fm1,Fm2) )).

tff(fact_96_fm_Osimps_I44_J,axiom,(
    ! [A: $tType,Fm: fm(A),Fm2: fm(A),Fm1: fm(A)] : c_Logic_Ofm_OOr(A,Fm1,Fm2) != exQ(A,Fm) )).

tff(fact_97_fm_Osimps_I41_J,axiom,(
    ! [A: $tType,Fm2: fm(A),Fm1: fm(A),Fm: fm(A)] : exQ(A,Fm) != c_Logic_Ofm_OAnd(A,Fm1,Fm2) )).

tff(fact_98_fm_Osimps_I40_J,axiom,(
    ! [A: $tType,Fm: fm(A),Fm2: fm(A),Fm1: fm(A)] : c_Logic_Ofm_OAnd(A,Fm1,Fm2) != exQ(A,Fm) )).

%----Arities (1)
tff(arity_Int_Oint___Groups_Ozero,axiom,(
    zero(int) )).

%----Helper facts (2)
tff(help_pp_1_1_U,axiom,(
    ~ pp(fFalse) )).

tff(help_pp_2_1_U,axiom,(
    pp(fTrue) )).

%----Conjectures (1)
tff(conj_0,conjecture,(
    interpret(atom,int,i_Z,qEpres896714165pres_1(as),xs) )).

%------------------------------------------------------------------------------
