%------------------------------------------------------------------------------
% File     : COM147+1 : TPTP v6.4.0. Released v6.4.0.
% Domain   : Computing Theory
% Problem  : T-Progress-T-abs step in progress/preservation proof
% Version  : Augmented > Especial.
% English  : This problem is a step within the proof of progress and 
%            preservation for the standard type system for the simply-typed 
%            lambda calculus.

% Refs     : [Pie02] Pierce (2002), Programming Languages
%          : [Gre15] Grewe (2015), Email to Geoff Sutcliffe
%          : [GE+15] Grewe et al. (2015), Type Systems for the Masses: Deri
% Source   : [Gre15]
% Names    : Progress-T-Progress-T-abs [Gre15]

% Status   : Theorem
% Rating   : 0.23 v6.4.0
% Syntax   : Number of formulae    :   58 (   6 unit)
%            Number of atoms       :  297 ( 230 equality)
%            Maximal formula depth :   23 (   8 average)
%            Number of connectives :  278 (  39   ~;  17   |; 124   &)
%                                         (   0 <=>;  98  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :    7 (   1 propositional; 0-3 arity)
%            Number of functors    :   17 (   4 constant; 0-3 arity)
%            Number of variables   :  323 (   0 sgn; 253   !;  70   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Generated by Veritas: https://github.com/stg-tud/type-pragmatics
%          : This is an expanded version of the original, with most axioms
%            combined into COM001+0.ax.
%------------------------------------------------------------------------------
include('Axioms/COM001+0.ax').
%------------------------------------------------------------------------------
fof('T-Weak',axiom,(
    ! [Vx,VS,VC,Ve,VT] :
      ( ( vlookup(Vx,VC) = vnoType
        & vtcheck(VC,Ve,VT) )
     => vtcheck(vbind(Vx,VS,VC),Ve,VT) ) )).

fof('T-Strong',axiom,(
    ! [Vx,VS,VC,Ve,VT] :
      ( ( ~ visFreeVar(Vx,Ve)
        & vtcheck(vbind(Vx,VS,VC),Ve,VT) )
     => vtcheck(VC,Ve,VT) ) )).

fof('T-Progress-T-abs-IH',axiom,(
    ! [VT] :
      ( ( vtcheck(vempty,ve1,VT)
        & ~ visValue(ve1) )
     => ? [Veout] : vreduce(ve1) = vsomeExp(Veout) ) )).

fof('T-Progress-T-abs',conjecture,(
    ! [VTin,Vx,VS] :
      ( ( vtcheck(vempty,vabs(Vx,VS,ve1),VTin)
        & ~ visValue(vabs(Vx,VS,ve1)) )
     => ? [Veout] : vreduce(vabs(Vx,VS,ve1)) = vsomeExp(Veout) ) )).

%------------------------------------------------------------------------------
