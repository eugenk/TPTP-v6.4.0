%------------------------------------------------------------------------------
% File     : CSR049+4 : TPTP v6.4.0. Released v3.4.0.
% Domain   : Common Sense Reasoning
% Problem  : Autogenerated Cyc Problem CSR049+4
% Version  : Especial.
% English  :

% Refs     : [RS+]   Reagan Smith et al., The Cyc TPTP Challenge Problem
% Source   : [RS+]
% Names    :

% Status   : Theorem
% Rating   : 0.87 v6.4.0, 0.88 v6.2.0, 0.84 v6.1.0, 0.90 v6.0.0, 0.87 v5.5.0, 0.89 v5.3.0, 0.93 v5.2.0, 0.85 v5.1.0, 0.86 v5.0.0, 0.92 v4.1.0, 0.96 v4.0.1, 1.00 v3.4.0
% Syntax   : Number of formulae    : 44217 (23808 unit)
%            Number of atoms       : 65455 (   8 equality)
%            Maximal formula depth :    8 (   2 average)
%            Number of connectives : 21420 ( 182 ~  ;   0  |; 913  &)
%                                         (   0 <=>;20325 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  : 6814 (   0 propositional; 1-6 arity)
%            Number of functors    : 15051 (14712 constant; 0-4 arity)
%            Number of variables   : 24045 (   0 singleton;24045 !;   0 ?)
%            Maximal term depth    :    6 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Autogenerated from the OpenCyc KB. Documentation can be found at
%            http://opencyc.org/doc/#TPTP_Challenge_Problem_Set
%          : Cyc(R) Knowledge Base Copyright(C) 1995-2007 Cycorp, Inc., Austin,
%            TX, USA. All rights reserved.
%          : OpenCyc Knowledge Base Copyright(C) 2001-2007 Cycorp, Inc.,
%            Austin, TX, USA. All rights reserved.
%------------------------------------------------------------------------------
%$problem_series(cyc_scaling_4,[CSR025+4,CSR026+4,CSR027+4,CSR028+4,CSR029+4,CSR030+4,CSR031+4,CSR032+4,CSR033+4,CSR034+4,CSR035+4,CSR036+4,CSR037+4,CSR038+4,CSR039+4,CSR040+4,CSR041+4,CSR042+4,CSR043+4,CSR044+4,CSR045+4,CSR046+4,CSR047+4,CSR048+4,CSR049+4,CSR050+4,CSR051+4,CSR052+4,CSR053+4,CSR054+4,CSR055+4,CSR056+4,CSR057+4,CSR058+4,CSR059+4,CSR060+4,CSR061+4,CSR062+4,CSR063+4,CSR064+4,CSR065+4,CSR066+4,CSR067+4,CSR068+4,CSR069+4,CSR070+4,CSR071+4,CSR072+4,CSR073+4,CSR074+4])
%$static(cyc_scaling_4,include('Axioms/CSR002+3.ax'))
include('Axioms/CSR002+3.ax').
%------------------------------------------------------------------------------
fof(query199,conjecture,
    ( mtvisible(c_unitedstatesgeographypeoplemt)
   => disjointwith(c_tptpcol_16_26926,c_tptpcol_16_92269) )).

%------------------------------------------------------------------------------
