%------------------------------------------------------------------------------
% File     : CSR062+5 : TPTP v6.4.0. Bugfixed v3.5.0.
% Domain   : Common Sense Reasoning
% Problem  : Autogenerated Cyc Problem CSR062+5
% Version  : Especial.
% English  :

% Refs     : [RS+]   Reagan Smith et al., The Cyc TPTP Challenge Problem
% Source   : [RS+]
% Names    :

% Status   : Theorem
% Rating   : 0.83 v6.4.0, 0.77 v6.3.0, 0.83 v6.2.0, 0.92 v6.1.0, 0.97 v6.0.0, 0.87 v5.5.0, 0.96 v5.4.0, 0.93 v5.3.0, 0.96 v5.2.0, 0.95 v5.0.0, 0.92 v4.1.0, 0.96 v4.0.1, 1.00 v3.5.0
% Syntax   : Number of formulae    : 540251 (168244 unit)
%            Number of atoms       : 920070 (  24 equality)
%            Maximal formula depth :   10 (   2 average)
%            Number of connectives : 389470 (9651 ~  ;   0  |;15895  &)
%                                         (   0 <=>;363924 =>;   0 <=)
%                                         (   0 <=>;   0 =>;   0 <=)
%            Number of predicates  : 103247 (   0 propositional; 1-7 arity)
%            Number of functors    : 255473 (253178 constant; 0-5 arity)
%            Number of variables   : 326598 (   0 singleton326597 !   1 ?)
%            Maximal term depth    :    8 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Autogenerated from the OpenCyc KB. Documentation can be found at
%            http://opencyc.org/doc/#TPTP_Challenge_Problem_Set
%          : Cyc(R) Knowledge Base Copyright(C) 1995-2007 Cycorp, Inc., Austin,
%            TX, USA. All rights reserved.
%          : OpenCyc Knowledge Base Copyright(C) 2001-2007 Cycorp, Inc.,
%            Austin, TX, USA. All rights reserved.
% Bugfixes : v3.5.0 - Bugfixes in CSR002+4.ax
%------------------------------------------------------------------------------
%$problem_series(cyc_scaling_5,[CSR025+5,CSR026+5,CSR027+5,CSR028+5,CSR029+5,CSR030+5,CSR031+5,CSR032+5,CSR033+5,CSR034+5,CSR035+5,CSR036+5,CSR037+5,CSR038+5,CSR039+5,CSR040+5,CSR041+5,CSR042+5,CSR043+5,CSR044+5,CSR045+5,CSR046+5,CSR047+5,CSR048+5,CSR049+5,CSR050+5,CSR051+5,CSR052+5,CSR053+5,CSR054+5,CSR055+5,CSR056+5,CSR057+5,CSR058+5,CSR059+5,CSR060+5,CSR061+5,CSR062+5,CSR063+5,CSR064+5,CSR065+5,CSR066+5,CSR067+5,CSR068+5,CSR069+5,CSR070+5,CSR071+5,CSR072+5,CSR073+5,CSR074+5])
%$static(cyc_scaling_5,include('Axioms/CSR002+4.ax'))
include('Axioms/CSR002+4.ax').
%------------------------------------------------------------------------------
fof(query262,conjecture,(
    ? [ARG1] :
      ( mtvisible(c_tptp_member3205_mt)
     => tptptypes_5_387(ARG1,c_pushingwithfingers) ) )).

%------------------------------------------------------------------------------
