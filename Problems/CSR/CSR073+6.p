%------------------------------------------------------------------------------
% File     : CSR073+6 : TPTP v6.4.0. Bugfixed v5.3.0.
% Domain   : Common Sense Reasoning
% Problem  : Autogenerated Cyc Problem CSR073+6
% Version  : Especial.
% English  :

% Refs     : [RS+]   Reagan Smith et al., The Cyc TPTP Challenge Problem
% Source   : [RS+]
% Names    :

% Status   : Theorem
% Rating   : 0.83 v6.4.0, 0.81 v6.3.0, 0.88 v6.2.0, 0.96 v6.1.0, 1.00 v6.0.0, 0.96 v5.5.0, 1.00 v5.4.0, 0.96 v5.3.0
% Syntax   : Number of formulae    : 3341984 (1400118 unit)
%            Number of atoms       : 5328210 (  67 equality)
%            Maximal formula depth :   11 (   2 average)
%            Number of connectives : 2065418 (79192 ~  ;   0  |;118845  &)
%                                         (   0 <=>;1867381 =>;   0 <=)
%                                         (   0 <=>;   0 =>;   0 <=)
%            Number of predicates  : 204678 (   0 propositional; 1-8 arity)
%            Number of functors    : 1050014 (1047140 constant; 0-5 arity)
%            Number of variables   : 972236 (   0 singleton972235 !   1 ?)
%            Maximal term depth    :    8 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Autogenerated from the OpenCyc KB. Documentation can be found at
%            http://opencyc.org/doc/#TPTP_Challenge_Problem_Set
%          : Cyc(R) Knowledge Base Copyright(C) 1995-2007 Cycorp, Inc., Austin,
%            TX, USA. All rights reserved.
%          : OpenCyc Knowledge Base Copyright(C) 2001-2007 Cycorp, Inc.,
%            Austin, TX, USA. All rights reserved.
% Bugfixes : v3.5.0 - Bugfixes in CSR002+5.ax
%          : v5.3.0 - Bugfixes in CSR002+5.ax
%------------------------------------------------------------------------------
%$problem_series(cyc_scaling_6,[CSR025+6,CSR026+6,CSR027+6,CSR028+6,CSR029+6,CSR030+6,CSR031+6,CSR032+6,CSR033+6,CSR034+6,CSR035+6,CSR036+6,CSR037+6,CSR038+6,CSR039+6,CSR040+6,CSR041+6,CSR042+6,CSR043+6,CSR044+6,CSR045+6,CSR046+6,CSR047+6,CSR048+6,CSR049+6,CSR050+6,CSR051+6,CSR052+6,CSR053+6,CSR054+6,CSR055+6,CSR056+6,CSR057+6,CSR058+6,CSR059+6,CSR060+6,CSR061+6,CSR062+6,CSR063+6,CSR064+6,CSR065+6,CSR066+6,CSR067+6,CSR068+6,CSR069+6,CSR070+6,CSR071+6,CSR072+6,CSR073+6,CSR074+6])
%$static(cyc_scaling_6,include('Axioms/CSR002+5.ax'))
include('Axioms/CSR002+5.ax').
%------------------------------------------------------------------------------
fof(query323,conjecture,(
    ? [ARG1] :
      ( mtvisible(c_tptp_spindlecollectormt)
     => tptptypes_5_387(ARG1,c_pushingababycarriage) ) )).

%------------------------------------------------------------------------------
