%------------------------------------------------------------------------------
% File     : CSR075+4 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Class subsumption, skolemization
% Version  : Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG1

% Status   : ContradictoryAxioms
% Rating   : 0.20 v6.4.0, 0.00 v6.1.0, 0.93 v6.0.0, 0.91 v5.5.0, 0.93 v5.4.0
% Syntax   : Number of formulae    : 7197 (4488 unit)
%            Number of atoms       : 17887 (1092 equality)
%            Maximal formula depth :   26 (   3 average)
%            Number of connectives : 11236 ( 546   ~;  69   |;4796   &)
%                                         ( 102 <=>;5723  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :  287 (   0 propositional; 1-8 arity)
%            Number of functors    : 3167 (3051 constant; 0-8 arity)
%            Number of variables   : 7427 (   1 sgn;6947   !; 480   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : 
% Bugfixes : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from SUMO
include('Axioms/CSR003+0.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Org1_1,s__Organization) )).

fof(prove_from_SUMO,conjecture,(
    ? [V_MEMBER] : s__member(V_MEMBER,s__Org1_1) )).

%------------------------------------------------------------------------------
