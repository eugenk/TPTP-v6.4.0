%------------------------------------------------------------------------------
% File     : CSR076+4 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Relation subsumption
% Version  : Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG2

% Status   : ContradictoryAxioms
% Rating   : 0.20 v6.4.0, 0.00 v6.2.0, 0.33 v6.1.0, 0.93 v6.0.0, 0.91 v5.5.0, 0.96 v5.4.0
% Syntax   : Number of formulae    : 7203 (4493 unit)
%            Number of atoms       : 17898 (1092 equality)
%            Maximal formula depth :   26 (   3 average)
%            Number of connectives : 11241 ( 546   ~;  69   |;4799   &)
%                                         ( 102 <=>;5725  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :  287 (   0 propositional; 1-8 arity)
%            Number of functors    : 3167 (3051 constant; 0-8 arity)
%            Number of variables   : 7429 (   1 sgn;6950   !; 479   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : 
% Bugfixes : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from SUMO
include('Axioms/CSR003+0.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__TheKB2_1,s__ComputerProgram) )).

fof(local_2,axiom,(
    s__instance(s__Inconsistent,s__Attribute) )).

fof(local_3,axiom,(
    ! [V_ATTR1,V_ATTR2,V_X] :
      ( ( s__instance(V_ATTR1,s__Attribute)
        & s__instance(V_ATTR2,s__Attribute) )
     => ( ( s__contraryAttribute_2(V_ATTR1,V_ATTR2)
          & s__property(V_X,V_ATTR1)
          & s__property(V_X,V_ATTR2) )
       => s__property(s__TheKB2_1,s__Inconsistent) ) ) )).

fof(local_4,axiom,(
    s__instance(s__Entity2_1,s__Organism) )).

fof(local_5,axiom,(
    s__instance(s__Entity2_2,s__Organism) )).

fof(local_6,axiom,(
    s__mother(s__Entity2_1,s__Entity2_2) )).

fof(local_7,axiom,(
    s__father(s__Entity2_1,s__Entity2_2) )).

fof(prove_from_SUMO,conjecture,(
    s__property(s__TheKB2_1,s__Inconsistent) )).

%------------------------------------------------------------------------------
