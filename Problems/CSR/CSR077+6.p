%------------------------------------------------------------------------------
% File     : CSR077+6 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Case elimination reasoning
% Version  : Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG3

% Status   : ContradictoryAxioms
% Rating   : 0.20 v6.4.0, 0.00 v6.2.0, 0.33 v6.1.0, 0.93 v6.0.0, 0.96 v5.5.0, 1.00 v5.4.0
% Syntax   : Number of formulae    : 103089 (88647 unit)
%            Number of atoms       : 195676 (13889 equality)
%            Maximal formula depth :   33 (   2 average)
%            Number of connectives : 96482 (3895   ~; 276   |;58598   &)
%                                         ( 248 <=>;33465  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  : 1166 (   0 propositional; 1-8 arity)
%            Number of functors    : 70559 (70331 constant; 0-8 arity)
%            Number of variables   : 55456 (  38 sgn;47948   !;7508   ?)
%            Maximal term depth    :    7 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : 
% Bugfixes : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from all Sigma constituents
include('Axioms/CSR003+2.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Number3_1,s__NonnegativeRealNumber) )).

fof(prove_from_ALL,conjecture,(
    ~ s__instance(s__Number3_1,s__NegativeRealNumber) )).

%------------------------------------------------------------------------------
