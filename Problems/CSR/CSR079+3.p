%------------------------------------------------------------------------------
% File     : CSR079+3 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Class equality and subsumption reasoning
% Version  : Especial > Augmented > Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG5

% Status   : ContradictoryAxioms
% Rating   : 0.20 v6.4.0, 0.33 v6.2.0, 0.67 v6.1.0, 0.97 v6.0.0, 0.96 v5.4.0
% Syntax   : Number of formulae    : 337574 (323132 unit)
%            Number of atoms       : 430161 (13898 equality)
%            Maximal formula depth :   33 (   1 average)
%            Number of connectives : 96481 (3894   ~; 276   |;58598   &)
%                                         ( 248 <=>;33465  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   20 (   0 propositional; 1-8 arity)
%            Number of functors    : 25492 (25492 constant; 0-8 arity)
%            Number of variables   : 55456 (  38 sgn;47948   !;7508   ?)
%            Maximal term depth    :    7 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : This version includes the cache axioms.
% Bugfixes : v3.4.1 - Bugfixes in CSR003+*.ax
%          : v3.4.2 - Bugfixes in CSR003+1.ax, CSR003+3.ax, CSR003+10.ax
%          : v3.5.0 - Bugfixes in CSR003+1.ax
%          : v4.0.0 - Bugfixes in CSR003 axiom files.
%          : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from all Sigma constituents
include('Axioms/CSR003+2.ax').
%----Include cache axioms for all Sigma constituents
include('Axioms/CSR003+5.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Lizard5_1,s__Class) )).

fof(local_2,axiom,(
    s__instance(s__Organism5_1,s__Lizard5_1) )).

fof(local_3,axiom,(
    s__instance(s__Class5_1,s__Class) )).

fof(local_4,axiom,(
    s__subclass(s__Class5_1,s__Organism) )).

fof(local_5,axiom,(
    s__instance(s__Class5_2,s__Class) )).

fof(local_6,axiom,(
    s__subclass(s__Class5_2,s__Organism) )).

fof(local_7,axiom,(
    s__instance(s__Class5_3,s__Class) )).

fof(local_8,axiom,(
    s__subclass(s__Class5_3,s__Organism) )).

fof(local_9,axiom,(
    s__instance(s__Class5_4,s__Class) )).

fof(local_10,axiom,(
    s__subclass(s__Class5_4,s__Organism) )).

fof(local_11,axiom,(
    s__instance(s__Class5_5,s__Class) )).

fof(local_12,axiom,(
    s__subclass(s__Class5_5,s__Organism) )).

fof(local_13,axiom,(
    s__instance(s__Class5_6,s__Class) )).

fof(local_14,axiom,(
    s__subclass(s__Class5_6,s__Organism) )).

fof(local_15,axiom,(
    s__instance(s__Class5_7,s__Class) )).

fof(local_16,axiom,(
    s__subclass(s__Class5_7,s__Organism) )).

fof(local_17,axiom,(
    s__instance(s__Class5_8,s__Class) )).

fof(local_18,axiom,(
    s__subclass(s__Class5_8,s__Organism) )).

fof(local_19,axiom,(
    s__instance(s__Class5_9,s__Class) )).

fof(local_20,axiom,(
    s__subclass(s__Class5_9,s__Organism) )).

fof(local_21,axiom,(
    s__instance(s__Class5_10,s__Class) )).

fof(local_22,axiom,(
    s__subclass(s__Class5_10,s__Organism) )).

fof(local_23,axiom,(
    s__Class5_1 = s__Class5_2 )).

fof(local_24,axiom,(
    s__Class5_2 = s__Class5_3 )).

fof(local_25,axiom,(
    s__Class5_3 = s__Class5_4 )).

fof(local_26,axiom,(
    s__Class5_4 = s__Class5_5 )).

fof(local_27,axiom,(
    s__Class5_5 = s__Class5_6 )).

fof(local_28,axiom,(
    s__Class5_6 = s__Class5_7 )).

fof(local_29,axiom,(
    s__Class5_7 = s__Class5_8 )).

fof(local_30,axiom,(
    s__Class5_8 = s__Class5_9 )).

fof(local_31,axiom,(
    s__Class5_9 = s__Class5_10 )).

fof(local_32,axiom,(
    s__subclass(s__Lizard5_1,s__Class5_1) )).

fof(local_33,axiom,(
    s__subclass(s__Class5_10,s__Reptile) )).

fof(prove_from_ALL,conjecture,(
    s__instance(s__Organism5_1,s__Animal) )).

%------------------------------------------------------------------------------
