%------------------------------------------------------------------------------
% File     : CSR084+3 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Case elimination with multiple rules
% Version  : Especial > Augmented > Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG10

% Status   : ContradictoryAxioms
% Rating   : 0.40 v6.4.0, 0.33 v6.1.0, 0.93 v6.0.0, 1.00 v5.5.0, 0.96 v5.4.0
% Syntax   : Number of formulae    : 337548 (323101 unit)
%            Number of atoms       : 430153 (13889 equality)
%            Maximal formula depth :   33 (   1 average)
%            Number of connectives : 96503 (3898   ~; 276   |;58610   &)
%                                         ( 248 <=>;33471  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   20 (   0 propositional; 1-8 arity)
%            Number of functors    : 25492 (25492 constant; 0-8 arity)
%            Number of variables   : 55466 (  38 sgn;47956   !;7510   ?)
%            Maximal term depth    :    7 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : This version includes the cache axioms.
% Bugfixes : v3.4.1 - Bugfixes in CSR003+*.ax
%          : v3.4.2 - Bugfixes in CSR003+1.ax, CSR003+3.ax, CSR003+10.ax
%          : v3.5.0 - Bugfixes in CSR003+1.ax
%          : v4.0.0 - Bugfixes in CSR003 axiom files.
%          : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from all Sigma constituents
include('Axioms/CSR003+2.ax').
%----Include cache axioms for all Sigma constituents
include('Axioms/CSR003+5.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    ! [V_A] :
      ( s__instance(V_A,s__Object)
     => ( ( s__instance(V_A,s__Animal)
          & ~ ? [V_PART] :
                ( s__instance(V_PART,s__Object)
                & s__instance(V_PART,s__SpinalColumn)
                & s__part(V_PART,V_A) ) )
       => ~ s__instance(V_A,s__Vertebrate) ) ) )).

fof(local_2,axiom,(
    ~ ? [V_SPINE] :
        ( s__instance(V_SPINE,s__Object)
        & s__instance(V_SPINE,s__SpinalColumn)
        & s__part(V_SPINE,s__BananaSlug10_1) ) )).

fof(local_3,axiom,(
    s__partition_3(s__Animal,s__Vertebrate,s__Invertebrate) )).

fof(local_4,axiom,(
    ! [V_SUPER,V_SUB1,V_SUB2] :
      ( ( s__instance(V_SUPER,s__Class)
        & s__instance(V_SUB1,s__Class)
        & s__instance(V_SUB2,s__Class) )
     => ( s__partition_3(V_SUPER,V_SUB1,V_SUB2)
       => s__partition_3(V_SUPER,V_SUB2,V_SUB1) ) ) )).

fof(local_5,axiom,(
    ! [V_SUPER,V_SUB1,V_SUB2,V_INST] :
      ( ( s__instance(V_SUPER,s__Class)
        & s__instance(V_SUB1,s__Class)
        & s__instance(V_SUB2,s__Class) )
     => ( ( s__partition_3(V_SUPER,V_SUB1,V_SUB2)
          & s__instance(V_INST,V_SUPER)
          & ~ s__instance(V_INST,V_SUB1) )
       => s__instance(V_INST,V_SUB2) ) ) )).

fof(local_6,axiom,(
    s__instance(s__BananaSlug10_1,s__Animal) )).

fof(local_7,axiom,
    ( s__instance(s__BodyPart10_1,s__BodyPart)
    & s__component(s__BodyPart10_1,s__BananaSlug10_1) )).

fof(prove_from_ALL,conjecture,(
    s__instance(s__BananaSlug10_1,s__Invertebrate) )).

%------------------------------------------------------------------------------
