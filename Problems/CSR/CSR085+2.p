%------------------------------------------------------------------------------
% File     : CSR085+2 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : One simple rule
% Version  : Especial > Augmented > Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG12

% Status   : ContradictoryAxioms
% Rating   : 0.00 v6.1.0, 0.80 v6.0.0, 0.74 v5.5.0, 0.85 v5.4.0
% Syntax   : Number of formulae    : 37712 (32455 unit)
%            Number of atoms       : 59122 (1481 equality)
%            Maximal formula depth :   26 (   2 average)
%            Number of connectives : 22314 ( 904   ~; 114   |;10143   &)
%                                         ( 139 <=>;11014  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :    8 (   0 propositional; 1-8 arity)
%            Number of functors    : 5082 (5082 constant; 0-8 arity)
%            Number of variables   : 12890 (  12 sgn;11438   !;1452   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : This version includes the cache axioms.
% Bugfixes : v3.4.1 - Bugfixes in CSR003+*.ax
%          : v3.4.2 - Bugfixes in CSR003+1.ax
%          : v3.5.0 - Bugfixes in CSR003+1.ax
%          : v4.0.0 - Bugfixes in CSR003 axiom files.
%          : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from SUMO_MILO
include('Axioms/CSR003+1.ax').
%----Include cache axioms for SUMO+MILO
include('Axioms/CSR003+4.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Organism12_1,s__Object) )).

fof(local_2,axiom,(
    s__attribute(s__Organism12_1,s__Living) )).

fof(local_3,axiom,(
    ! [V_X] :
      ( s__instance(V_X,s__Object)
     => ( s__attribute(V_X,s__Living)
       => s__instance(V_X,s__Organism) ) ) )).

fof(prove_from_SUMO_MILO,conjecture,(
    s__instance(s__Organism12_1,s__Organism) )).

%------------------------------------------------------------------------------
