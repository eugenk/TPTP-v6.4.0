%------------------------------------------------------------------------------
% File     : CSR085+7 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : One simple rule
% Version  : Especial.
%            Theorem formulation : Existentially quantified, as a question.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG12

% Status   : ContradictoryAxioms
% Rating   : 0.20 v6.4.0, 0.33 v6.1.0, 0.87 v6.0.0, 0.78 v5.5.0, 0.93 v5.4.0
% Syntax   : Number of formulae    : 103091 (88648 unit)
%            Number of atoms       : 195680 (13889 equality)
%            Maximal formula depth :   33 (   2 average)
%            Number of connectives : 96483 (3894   ~; 276   |;58598   &)
%                                         ( 248 <=>;33467  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  : 1166 (   0 propositional; 1-8 arity)
%            Number of functors    : 70559 (70331 constant; 0-8 arity)
%            Number of variables   : 55458 (  38 sgn;47949   !;7509   ?)
%            Maximal term depth    :    7 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : 
% Bugfixes : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from all Sigma constituents
include('Axioms/CSR003+2.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Organism12_1,s__Object) )).

fof(local_2,axiom,(
    s__attribute(s__Organism12_1,s__Living) )).

fof(local_3,axiom,(
    ! [V_X] :
      ( s__instance(V_X,s__Object)
     => ( s__attribute(V_X,s__Living)
       => s__instance(V_X,s__Organism) ) ) )).

fof(prove_from_ALL,conjecture,(
    ? [X__s__Organism12_1] : s__instance(X__s__Organism12_1,s__Organism) )).

%------------------------------------------------------------------------------
