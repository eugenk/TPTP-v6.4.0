%------------------------------------------------------------------------------
% File     : CSR087+3 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Just one rule that cannot be satisfied
% Version  : Especial > Augmented > Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG14

% Status   : ContradictoryAxioms
% Rating   : 0.40 v6.4.0, 0.67 v6.2.0, 1.00 v5.4.0
% Syntax   : Number of formulae    : 337544 (323101 unit)
%            Number of atoms       : 430133 (13890 equality)
%            Maximal formula depth :   33 (   1 average)
%            Number of connectives : 96485 (3896   ~; 276   |;58600   &)
%                                         ( 248 <=>;33465  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   20 (   0 propositional; 1-8 arity)
%            Number of functors    : 25492 (25492 constant; 0-8 arity)
%            Number of variables   : 55457 (  38 sgn;47948   !;7509   ?)
%            Maximal term depth    :    7 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : This version includes the cache axioms.
% Bugfixes : v3.4.1 - Bugfixes in CSR003+*.ax
%          : v3.4.2 - Bugfixes in CSR003+1.ax, CSR003+3.ax, CSR003+10.ax
%          : v3.5.0 - Bugfixes in CSR003+1.ax
%          : v4.0.0 - Bugfixes in CSR003 axiom files.
%          : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from all Sigma constituents
include('Axioms/CSR003+2.ax').
%----Include cache axioms for all Sigma constituents
include('Axioms/CSR003+5.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Atom14_1,s__Atom) )).

fof(local_2,axiom,(
    s__instance(s__Nucleus14_1,s__AtomicNucleus) )).

fof(local_3,axiom,(
    s__component(s__Nucleus14_1,s__Atom14_1) )).

fof(prove_from_ALL,conjecture,(
    ~ ? [V_NUCLEUS] :
        ( s__instance(V_NUCLEUS,s__AtomicNucleus)
        & s__component(V_NUCLEUS,s__Atom14_1)
        & V_NUCLEUS != s__Nucleus14_1 ) )).

%------------------------------------------------------------------------------
