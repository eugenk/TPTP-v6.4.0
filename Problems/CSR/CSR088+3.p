%------------------------------------------------------------------------------
% File     : CSR088+3 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Overlapping and meeting time
% Version  : Especial > Augmented > Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG15

% Status   : ContradictoryAxioms
% Rating   : 0.60 v6.4.0, 0.33 v6.1.0, 1.00 v6.0.0, 0.96 v5.5.0, 1.00 v5.4.0
% Syntax   : Number of formulae    : 337544 (323102 unit)
%            Number of atoms       : 430131 (13889 equality)
%            Maximal formula depth :   33 (   1 average)
%            Number of connectives : 96482 (3895   ~; 276   |;58598   &)
%                                         ( 248 <=>;33465  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   20 (   0 propositional; 1-8 arity)
%            Number of functors    : 25492 (25492 constant; 0-8 arity)
%            Number of variables   : 55456 (  38 sgn;47948   !;7508   ?)
%            Maximal term depth    :    7 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : This version includes the cache axioms.
% Bugfixes : v3.4.1 - Bugfixes in CSR003+*.ax
%          : v3.4.2 - Bugfixes in CSR003+1.ax, CSR003+3.ax, CSR003+10.ax
%          : v3.5.0 - Bugfixes in CSR003+1.ax
%          : v4.0.0 - Bugfixes in CSR003 axiom files.
%          : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from all Sigma constituents
include('Axioms/CSR003+2.ax').
%----Include cache axioms for all Sigma constituents
include('Axioms/CSR003+5.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Time15_1,s__TimeInterval) )).

fof(local_2,axiom,(
    s__instance(s__Time15_2,s__TimeInterval) )).

fof(local_3,axiom,(
    s__meetsTemporally(s__Time15_1,s__Time15_2) )).

fof(prove_from_ALL,conjecture,(
    ~ s__overlapsTemporally(s__Time15_1,s__Time15_2) )).

%------------------------------------------------------------------------------
