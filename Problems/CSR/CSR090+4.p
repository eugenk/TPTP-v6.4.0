%------------------------------------------------------------------------------
% File     : CSR090+4 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Pieces of time
% Version  : Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG17

% Status   : ContradictoryAxioms
% Rating   : 0.20 v6.4.0, 0.00 v6.2.0, 0.33 v6.1.0, 0.93 v6.0.0, 0.91 v5.5.0, 0.93 v5.4.0
% Syntax   : Number of formulae    : 7201 (4492 unit)
%            Number of atoms       : 17891 (1092 equality)
%            Maximal formula depth :   26 (   3 average)
%            Number of connectives : 11236 ( 546   ~;  69   |;4796   &)
%                                         ( 102 <=>;5723  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :  287 (   0 propositional; 1-8 arity)
%            Number of functors    : 3167 (3051 constant; 0-8 arity)
%            Number of variables   : 7426 (   1 sgn;6947   !; 479   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : 
% Bugfixes : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from SUMO
include('Axioms/CSR003+0.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Time17_1,s__TimeInterval) )).

fof(local_2,axiom,(
    s__instance(s__Time17_2,s__TimeInterval) )).

fof(local_3,axiom,(
    s__instance(s__Time17_3,s__TimeInterval) )).

fof(local_4,axiom,(
    s__temporalPart(s__Time17_1,s__Time17_2) )).

fof(local_5,axiom,(
    s__temporalPart(s__Time17_2,s__Time17_3) )).

fof(prove_from_SUMO,conjecture,(
    s__temporalPart(s__Time17_1,s__Time17_3) )).

%------------------------------------------------------------------------------
