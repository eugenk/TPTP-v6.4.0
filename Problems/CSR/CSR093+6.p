%------------------------------------------------------------------------------
% File     : CSR093+6 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Distant ancestry
% Version  : Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG23

% Status   : ContradictoryAxioms
% Rating   : 0.20 v6.4.0, 0.33 v6.2.0, 0.67 v6.1.0, 1.00 v5.4.0
% Syntax   : Number of formulae    : 103119 (88676 unit)
%            Number of atoms       : 195707 (13889 equality)
%            Maximal formula depth :   33 (   2 average)
%            Number of connectives : 96482 (3894   ~; 276   |;58599   &)
%                                         ( 248 <=>;33465  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  : 1166 (   0 propositional; 1-8 arity)
%            Number of functors    : 70559 (70331 constant; 0-8 arity)
%            Number of variables   : 55456 (  38 sgn;47948   !;7508   ?)
%            Maximal term depth    :    7 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : Probably requires at least 75 steps. 
% Bugfixes : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from all Sigma constituents
include('Axioms/CSR003+2.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Human23_1,s__Human) )).

fof(local_2,axiom,(
    s__instance(s__Human23_2,s__Human) )).

fof(local_3,axiom,(
    s__instance(s__Human23_3,s__Human) )).

fof(local_4,axiom,(
    s__instance(s__Human23_4,s__Human) )).

fof(local_5,axiom,(
    s__instance(s__Human23_5,s__Human) )).

fof(local_6,axiom,(
    s__instance(s__Human23_6,s__Human) )).

fof(local_7,axiom,(
    s__instance(s__Human23_7,s__Human) )).

fof(local_8,axiom,(
    s__instance(s__Human23_8,s__Human) )).

fof(local_9,axiom,(
    s__instance(s__Human23_9,s__Human) )).

fof(local_10,axiom,(
    s__instance(s__Human23_10,s__Human) )).

fof(local_11,axiom,(
    s__instance(s__Human23_11,s__Human) )).

fof(local_12,axiom,(
    s__instance(s__Human23_12,s__Human) )).

fof(local_13,axiom,(
    s__instance(s__Human23_13,s__Human) )).

fof(local_14,axiom,(
    s__instance(s__Human23_14,s__Human) )).

fof(local_15,axiom,(
    s__instance(s__Human23_15,s__Human) )).

fof(local_16,axiom,(
    s__instance(s__Human23_16,s__Human) )).

fof(local_17,axiom,(
    s__ancestor(s__Human23_1,s__Human23_2) )).

fof(local_18,axiom,(
    s__ancestor(s__Human23_2,s__Human23_3) )).

fof(local_19,axiom,(
    s__ancestor(s__Human23_3,s__Human23_4) )).

fof(local_20,axiom,(
    s__ancestor(s__Human23_4,s__Human23_5) )).

fof(local_21,axiom,(
    s__ancestor(s__Human23_5,s__Human23_6) )).

fof(local_22,axiom,(
    s__ancestor(s__Human23_6,s__Human23_7) )).

fof(local_23,axiom,(
    s__ancestor(s__Human23_7,s__Human23_8) )).

fof(local_24,axiom,(
    s__ancestor(s__Human23_8,s__Human23_9) )).

fof(local_25,axiom,(
    s__ancestor(s__Human23_9,s__Human23_10) )).

fof(local_26,axiom,(
    s__ancestor(s__Human23_10,s__Human23_11) )).

fof(local_27,axiom,(
    s__ancestor(s__Human23_11,s__Human23_12) )).

fof(local_28,axiom,(
    s__ancestor(s__Human23_12,s__Human23_13) )).

fof(local_29,axiom,(
    s__ancestor(s__Human23_13,s__Human23_14) )).

fof(local_30,axiom,(
    s__ancestor(s__Human23_14,s__Human23_15) )).

fof(local_31,axiom,(
    s__ancestor(s__Human23_14,s__Human23_16) )).

fof(prove_from_ALL,conjecture,
    ( s__ancestor(s__Human23_1,s__Human23_15)
    & s__ancestor(s__Human23_1,s__Human23_16) )).

%------------------------------------------------------------------------------
