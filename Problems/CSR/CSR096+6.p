%------------------------------------------------------------------------------
% File     : CSR096+6 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Case elimination
% Version  : Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG26

% Status   : ContradictoryAxioms
% Rating   : 0.40 v6.4.0, 0.00 v6.1.0, 0.93 v6.0.0, 0.96 v5.5.0, 1.00 v5.4.0
% Syntax   : Number of formulae    : 103095 (88650 unit)
%            Number of atoms       : 195691 (13889 equality)
%            Maximal formula depth :   33 (   2 average)
%            Number of connectives : 96492 (3896   ~; 278   |;58599   &)
%                                         ( 248 <=>;33471  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  : 1166 (   0 propositional; 1-8 arity)
%            Number of functors    : 70559 (70331 constant; 0-8 arity)
%            Number of variables   : 55459 (  38 sgn;47951   !;7508   ?)
%            Maximal term depth    :    7 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : 
% Bugfixes : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from all Sigma constituents
include('Axioms/CSR003+2.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Planet26_1,s__Class) )).

fof(local_2,axiom,(
    s__subclass(s__Planet26_1,s__AstronomicalBody) )).

fof(local_3,axiom,(
    ! [V_P] :
      ( s__instance(V_P,s__Object)
     => ( s__instance(V_P,s__Planet26_1)
       => ( s__attribute(V_P,s__Solid)
          | s__attribute(V_P,s__Gaseous) ) ) ) )).

fof(local_4,axiom,(
    ! [V_P] :
      ( s__instance(V_P,s__Object)
     => ( s__instance(V_P,s__Planet26_1)
       => ( s__attribute(V_P,s__Earthlike)
          | s__attribute(V_P,s__HostileToEarthLife) ) ) ) )).

fof(local_5,axiom,(
    ! [V_X] :
      ( s__instance(V_X,s__Object)
     => ( ( s__instance(V_X,s__Planet26_1)
          & s__attribute(V_X,s__Gaseous) )
       => ~ s__attribute(V_X,s__Earthlike) ) ) )).

fof(local_6,axiom,(
    s__instance(s__Object26_1,s__Planet26_1) )).

fof(local_7,axiom,(
    ~ s__attribute(s__Object26_1,s__Solid) )).

fof(prove_from_ALL,conjecture,(
    s__attribute(s__Object26_1,s__HostileToEarthLife) )).

%------------------------------------------------------------------------------
