%------------------------------------------------------------------------------
% File     : CSR097+4 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : A gaseous object
% Version  : Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG28

% Status   : ContradictoryAxioms
% Rating   : 0.40 v6.4.0, 0.00 v6.2.0, 0.33 v6.1.0, 0.82 v6.0.0, 0.77 v5.5.0, 1.00 v5.4.0
% Syntax   : Number of formulae    : 7205 (4496 unit)
%            Number of atoms       : 17895 (1092 equality)
%            Maximal formula depth :   26 (   3 average)
%            Number of connectives : 11237 ( 547   ~;  69   |;4796   &)
%                                         ( 102 <=>;5723  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :  287 (   0 propositional; 1-8 arity)
%            Number of functors    : 3167 (3051 constant; 0-8 arity)
%            Number of variables   : 7426 (   1 sgn;6947   !; 479   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : 
% Bugfixes : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from SUMO
include('Axioms/CSR003+0.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Planet28_1,s__Class) )).

fof(local_2,axiom,(
    s__subclass(s__Planet28_1,s__AstronomicalBody) )).

fof(local_3,axiom,(
    s__instance(s__Rocky,s__Attribute) )).

fof(local_4,axiom,(
    s__instance(s__Icy,s__Attribute) )).

fof(local_5,axiom,(
    s__instance(s__Watery,s__Attribute) )).

fof(local_6,axiom,(
    s__instance(s__Gaseous,s__Attribute) )).

fof(local_7,axiom,(
    s__contraryAttribute_4(s__Rocky,s__Icy,s__Watery,s__Gaseous) )).

fof(local_8,axiom,(
    s__instance(s__Object28_1,s__Planet28_1) )).

fof(local_9,axiom,(
    s__attribute(s__Object28_1,s__Watery) )).

fof(prove_from_SUMO,conjecture,(
    ~ s__attribute(s__Object28_1,s__Gaseous) )).

%------------------------------------------------------------------------------
