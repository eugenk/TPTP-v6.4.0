%------------------------------------------------------------------------------
% File     : CSR099+5 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Reasoning about class equality
% Version  : Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG30

% Status   : ContradictoryAxioms
% Rating   : 0.00 v6.2.0, 0.33 v6.1.0, 0.93 v6.0.0, 0.87 v5.5.0, 0.93 v5.4.0
% Syntax   : Number of formulae    : 16585 (11329 unit)
%            Number of atoms       : 37993 (1482 equality)
%            Maximal formula depth :   26 (   3 average)
%            Number of connectives : 22312 ( 904   ~; 114   |;10143   &)
%                                         ( 139 <=>;11012  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :  476 (   0 propositional; 1-8 arity)
%            Number of functors    : 9359 (9221 constant; 0-8 arity)
%            Number of variables   : 12889 (  12 sgn;11437   !;1452   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : 
% Bugfixes : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from SUMO_MILO
include('Axioms/CSR003+1.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Class30_1,s__Class) )).

fof(local_2,axiom,(
    s__subclass(s__Class30_1,s__Reptile) )).

fof(local_3,axiom,(
    s__subclass(s__Reptile,s__Class30_1) )).

fof(prove_from_SUMO_MILO,conjecture,(
    s__Class30_1 = s__Reptile )).

%------------------------------------------------------------------------------
