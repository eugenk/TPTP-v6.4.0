%------------------------------------------------------------------------------
% File     : CSR101+6 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : An "intensional" query requiring circular subclass reasoning
% Version  : Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG32

% Status   : ContradictoryAxioms
% Rating   : 0.20 v6.4.0, 0.00 v6.1.0, 0.97 v6.0.0, 0.91 v5.5.0, 0.96 v5.4.0
% Syntax   : Number of formulae    : 103097 (88654 unit)
%            Number of atoms       : 195685 (13889 equality)
%            Maximal formula depth :   33 (   2 average)
%            Number of connectives : 96482 (3894   ~; 276   |;58598   &)
%                                         ( 248 <=>;33466  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  : 1166 (   0 propositional; 1-8 arity)
%            Number of functors    : 70559 (70331 constant; 0-8 arity)
%            Number of variables   : 55457 (  38 sgn;47949   !;7508   ?)
%            Maximal term depth    :    7 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : 
% Bugfixes : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from all Sigma constituents
include('Axioms/CSR003+2.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__Class32_1,s__Class) )).

fof(local_2,axiom,(
    s__instance(s__Class32_2,s__Class) )).

fof(local_3,axiom,(
    s__instance(s__Class32_3,s__Class) )).

fof(local_4,axiom,(
    s__subclass(s__Class32_1,s__Animal) )).

fof(local_5,axiom,(
    s__subclass(s__Class32_2,s__Animal) )).

fof(local_6,axiom,(
    s__subclass(s__Class32_3,s__Animal) )).

fof(local_7,axiom,(
    s__subclass(s__Class32_1,s__Class32_2) )).

fof(local_8,axiom,(
    s__subclass(s__Class32_2,s__Class32_3) )).

fof(local_9,axiom,(
    s__subclass(s__Class32_3,s__Class32_1) )).

fof(prove_from_ALL,conjecture,(
    ! [V_X] :
      ( s__instance(V_X,s__Class32_2)
     => s__instance(V_X,s__Class32_1) ) )).

%------------------------------------------------------------------------------
