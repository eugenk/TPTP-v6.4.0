%------------------------------------------------------------------------------
% File     : CSR108+4 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Defines a new predicate of 10 arguments
% Version  : Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
%          : [Sie07] Siegel (2007), Email to G. Sutcliffe
% Source   : [Sie07]
% Names    : TQG44

% Status   : ContradictoryAxioms
% Rating   : 0.20 v6.4.0, 0.00 v6.2.0, 0.33 v6.1.0, 0.90 v6.0.0, 0.91 v5.5.0, 0.93 v5.4.0
% Syntax   : Number of formulae    : 7220 (4509 unit)
%            Number of atoms       : 17917 (1092 equality)
%            Maximal formula depth :   26 (   3 average)
%            Number of connectives : 11243 ( 546   ~;  69   |;4802   &)
%                                         ( 102 <=>;5724  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :  287 (   0 propositional; 1-10 arity)
%            Number of functors    : 3167 (3051 constant; 0-8 arity)
%            Number of variables   : 7436 (   1 sgn;6957   !; 479   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : 
% Bugfixes : v4.0.1 - Bugfixes in CSR003 axiom files.
%          : v4.1.0 - Bugfixes in CSR003 axiom files.
%          : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from SUMO
include('Axioms/CSR003+0.ax').
%------------------------------------------------------------------------------
fof(local_1,axiom,(
    s__instance(s__testPred44_1_M,s__Predicate) )).

fof(local_2,axiom,(
    s__valence(s__testPred44_1_M,n10) )).

fof(local_3,axiom,(
    s__domain(s__testPred44_1_M,n1,s__Entity) )).

fof(local_4,axiom,(
    s__domain(s__testPred44_1_M,n2,s__Entity) )).

fof(local_5,axiom,(
    s__domain(s__testPred44_1_M,n3,s__Entity) )).

fof(local_6,axiom,(
    s__domain(s__testPred44_1_M,n4,s__Entity) )).

fof(local_7,axiom,(
    s__domain(s__testPred44_1_M,n5,s__Entity) )).

fof(local_8,axiom,(
    s__domain(s__testPred44_1_M,n6,s__Entity) )).

fof(local_9,axiom,(
    s__domain(s__testPred44_1_M,n7,s__Entity) )).

fof(local_10,axiom,(
    s__domain(s__testPred44_1_M,n8,s__Entity) )).

fof(local_11,axiom,(
    s__domain(s__testPred44_1_M,n9,s__Entity) )).

fof(local_12,axiom,(
    s__domain(s__testPred44_1_M,n10,s__Entity) )).

fof(local_13,axiom,(
    s__instance(s__Entity44_1,s__Entity) )).

fof(local_14,axiom,(
    s__instance(s__Entity44_2,s__Entity) )).

fof(local_15,axiom,(
    s__instance(s__Entity44_3,s__Entity) )).

fof(local_16,axiom,(
    s__instance(s__Entity44_4,s__Entity) )).

fof(local_17,axiom,(
    s__instance(s__Entity44_5,s__Entity) )).

fof(local_18,axiom,(
    s__instance(s__Entity44_6,s__Entity) )).

fof(local_19,axiom,(
    s__instance(s__Entity44_7,s__Entity) )).

fof(local_20,axiom,(
    s__instance(s__Entity44_8,s__Entity) )).

fof(local_21,axiom,(
    s__instance(s__Entity44_9,s__Entity) )).

fof(local_22,axiom,(
    s__instance(s__Entity44_10,s__Entity) )).

fof(local_23,axiom,(
    s__testPred44_1__10(s__Entity44_1,s__Entity44_2,s__Entity44_3,s__Entity44_4,s__Entity44_5,s__Entity44_6,s__Entity44_7,s__Entity44_8,s__Entity44_9,s__Entity44_10) )).

fof(local_24,axiom,(
    ! [V_ARG1,V_ARG2,V_ARG3,V_ARG4,V_ARG5,V_ARG6,V_ARG7,V_ARG8,V_ARG9,V_ARG10] :
      ( s__testPred44_1__10(V_ARG1,V_ARG2,V_ARG3,V_ARG4,V_ARG5,V_ARG6,V_ARG7,V_ARG8,V_ARG9,V_ARG10)
     => ( s__instance(V_ARG1,s__Amphibian)
        & s__instance(V_ARG2,s__Bird)
        & s__instance(V_ARG9,s__Mammal)
        & s__instance(V_ARG10,s__Reptile) ) ) )).

fof(prove_from_SUMO,conjecture,
    ( s__instance(s__Entity44_1,s__Animal)
    & s__instance(s__Entity44_2,s__Animal)
    & s__instance(s__Entity44_9,s__Animal)
    & s__instance(s__Entity44_10,s__Animal) )).

%------------------------------------------------------------------------------
