%------------------------------------------------------------------------------
% File     : CSR113+29 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Commonsense Reasoning
% Problem  : Where can you find the Statue of Liberty?
% Version  : [Pel09] axioms.
% English  :

% Refs     : [Glo07] Gloeckner (2007), University of Hagen at CLEF 2007: An
%          : [PW07]  Pelzer & Wernhard (2007), System Description: E-KRHype
%          : [FG+08] Furbach et al. (2008), LogAnswer - A Deduction-Based Q
%          : [Pel09] Pelzer (2009), Email to Geoff Sutcliffe
% Source   : [Pel09]
% Names    : synth_qa07_003_qapw_40_a270_tptp [Pel09]

% Status   : Theorem
% Rating   : 0.43 v6.4.0, 0.50 v6.3.0, 0.54 v6.2.0, 0.73 v6.1.0, 0.72 v6.0.0, 0.50 v5.5.0, 0.75 v5.4.0, 0.74 v5.3.0, 0.78 v5.2.0, 0.57 v5.0.0, 0.65 v4.1.0, 0.67 v4.0.1, 0.68 v4.0.0
% Syntax   : Number of formulae    : 10189 (10061 unit)
%            Number of atoms       : 10889 (   0 equality)
%            Maximal formula depth :  187 (   1 average)
%            Number of connectives :  700 (   0   ~;  18   |; 556   &)
%                                         (   0 <=>; 126  =>;   0  <=)
%                                         (   0 <~>;   0  ~|;   0  ~&)
%            Number of predicates  :   87 (   0 propositional; 2-6 arity)
%            Number of functors    : 16655 (16654 constant; 0-2 arity)
%            Number of variables   :  472 (   0 sgn; 405   !;  67   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_THM_RFO_NEQ

% Comments : The different versions of this problem stem from the use of
%            different text snippet retrieval modules, and different snippets
%            being found. The problem tries to prove the questions from the
%            snippet and the background knowledge.
%------------------------------------------------------------------------------
%----Include LogAnswer axioms
include('Axioms/CSR004+0.ax').
%------------------------------------------------------------------------------
fof(synth_qa07_003_qapw_40_a270,conjecture,(
    ? [X0,X1,X2,X3] :
      ( attr(X1,X0)
      & scar(X2,X3)
      & sub(X0,name_1_1)
      & val(X0,usa_0) ) )).

fof(ave07_era5_synth_qa07_003_qapw_40_a270,hypothesis,
    ( pred(c573,land_1_1)
    & prop(c573,vereinigt_1_1)
    & sub(c574,in_2_1)
    & attr(c581,c582)
    & sub(c581,stadt__1_1)
    & sub(c582,name_1_1)
    & val(c582,seattle_0)
    & sub(c586,westkueste_1_1)
    & attch(c628,c586)
    & attr(c628,c629)
    & sub(c628,land_1_1)
    & sub(c629,name_1_1)
    & val(c629,usa_0)
    & prop(c633,weit_1_1)
    & sub(c633,freiheitsstatue_1_1)
    & tupl_p6(c770,c573,c574,c581,c586,c633)
    & assoc(freiheitsstatue_1_1,freiheit_1_1)
    & sub(freiheitsstatue_1_1,statue_1_1)
    & chsp2(vereinigen_1_1,vereinigt_1_1)
    & assoc(westkueste_1_1,west__1_1)
    & sub(westkueste_1_1,kueste_1_1)
    & sort(c573,d)
    & sort(c573,io)
    & card(c573,cons(x_constant,cons(int1,nil)))
    & etype(c573,int1)
    & fact(c573,real)
    & gener(c573,gener_c)
    & quant(c573,mult)
    & refer(c573,refer_c)
    & varia(c573,varia_c)
    & sort(land_1_1,d)
    & sort(land_1_1,io)
    & card(land_1_1,int1)
    & etype(land_1_1,int0)
    & fact(land_1_1,real)
    & gener(land_1_1,ge)
    & quant(land_1_1,one)
    & refer(land_1_1,refer_c)
    & varia(land_1_1,varia_c)
    & sort(vereinigt_1_1,tq)
    & sort(c574,o)
    & card(c574,int1)
    & etype(c574,int0)
    & fact(c574,real)
    & gener(c574,gener_c)
    & quant(c574,one)
    & refer(c574,refer_c)
    & varia(c574,varia_c)
    & sort(in_2_1,o)
    & card(in_2_1,int1)
    & etype(in_2_1,int0)
    & fact(in_2_1,real)
    & gener(in_2_1,ge)
    & quant(in_2_1,one)
    & refer(in_2_1,refer_c)
    & varia(in_2_1,varia_c)
    & sort(c581,d)
    & sort(c581,io)
    & card(c581,int1)
    & etype(c581,int0)
    & fact(c581,real)
    & gener(c581,sp)
    & quant(c581,one)
    & refer(c581,det)
    & varia(c581,con)
    & sort(c582,na)
    & card(c582,int1)
    & etype(c582,int0)
    & fact(c582,real)
    & gener(c582,sp)
    & quant(c582,one)
    & refer(c582,indet)
    & varia(c582,varia_c)
    & sort(stadt__1_1,d)
    & sort(stadt__1_1,io)
    & card(stadt__1_1,int1)
    & etype(stadt__1_1,int0)
    & fact(stadt__1_1,real)
    & gener(stadt__1_1,ge)
    & quant(stadt__1_1,one)
    & refer(stadt__1_1,refer_c)
    & varia(stadt__1_1,varia_c)
    & sort(name_1_1,na)
    & card(name_1_1,int1)
    & etype(name_1_1,int0)
    & fact(name_1_1,real)
    & gener(name_1_1,ge)
    & quant(name_1_1,one)
    & refer(name_1_1,refer_c)
    & varia(name_1_1,varia_c)
    & sort(seattle_0,fe)
    & sort(c586,d)
    & card(c586,int1)
    & etype(c586,int0)
    & fact(c586,real)
    & gener(c586,sp)
    & quant(c586,one)
    & refer(c586,det)
    & varia(c586,con)
    & sort(westkueste_1_1,d)
    & card(westkueste_1_1,int1)
    & etype(westkueste_1_1,int0)
    & fact(westkueste_1_1,real)
    & gener(westkueste_1_1,ge)
    & quant(westkueste_1_1,one)
    & refer(westkueste_1_1,refer_c)
    & varia(westkueste_1_1,varia_c)
    & sort(c628,d)
    & sort(c628,io)
    & card(c628,int1)
    & etype(c628,int0)
    & fact(c628,real)
    & gener(c628,sp)
    & quant(c628,one)
    & refer(c628,det)
    & varia(c628,con)
    & sort(c629,na)
    & card(c629,int1)
    & etype(c629,int0)
    & fact(c629,real)
    & gener(c629,sp)
    & quant(c629,one)
    & refer(c629,indet)
    & varia(c629,varia_c)
    & sort(usa_0,fe)
    & sort(c633,d)
    & card(c633,int1)
    & etype(c633,int0)
    & fact(c633,real)
    & gener(c633,sp)
    & quant(c633,one)
    & refer(c633,indet)
    & varia(c633,varia_c)
    & sort(weit_1_1,mq)
    & sort(freiheitsstatue_1_1,d)
    & card(freiheitsstatue_1_1,int1)
    & etype(freiheitsstatue_1_1,int0)
    & fact(freiheitsstatue_1_1,real)
    & gener(freiheitsstatue_1_1,ge)
    & quant(freiheitsstatue_1_1,one)
    & refer(freiheitsstatue_1_1,refer_c)
    & varia(freiheitsstatue_1_1,varia_c)
    & sort(c770,ent)
    & card(c770,card_c)
    & etype(c770,etype_c)
    & fact(c770,real)
    & gener(c770,gener_c)
    & quant(c770,quant_c)
    & refer(c770,refer_c)
    & varia(c770,varia_c)
    & sort(freiheit_1_1,as)
    & sort(freiheit_1_1,io)
    & card(freiheit_1_1,int1)
    & etype(freiheit_1_1,int0)
    & fact(freiheit_1_1,real)
    & gener(freiheit_1_1,ge)
    & quant(freiheit_1_1,one)
    & refer(freiheit_1_1,refer_c)
    & varia(freiheit_1_1,varia_c)
    & sort(statue_1_1,d)
    & card(statue_1_1,int1)
    & etype(statue_1_1,int0)
    & fact(statue_1_1,real)
    & gener(statue_1_1,ge)
    & quant(statue_1_1,one)
    & refer(statue_1_1,refer_c)
    & varia(statue_1_1,varia_c)
    & sort(vereinigen_1_1,dn)
    & fact(vereinigen_1_1,real)
    & gener(vereinigen_1_1,ge)
    & sort(west__1_1,d)
    & sort(west__1_1,io)
    & card(west__1_1,int1)
    & etype(west__1_1,int0)
    & fact(west__1_1,real)
    & gener(west__1_1,ge)
    & quant(west__1_1,one)
    & refer(west__1_1,refer_c)
    & varia(west__1_1,varia_c)
    & sort(kueste_1_1,d)
    & card(kueste_1_1,int1)
    & etype(kueste_1_1,int0)
    & fact(kueste_1_1,real)
    & gener(kueste_1_1,ge)
    & quant(kueste_1_1,one)
    & refer(kueste_1_1,refer_c)
    & varia(kueste_1_1,varia_c) )).

%------------------------------------------------------------------------------
