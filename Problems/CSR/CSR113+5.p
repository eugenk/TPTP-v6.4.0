%------------------------------------------------------------------------------
% File     : CSR113+5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Commonsense Reasoning
% Problem  : Where can you find the Statue of Liberty?
% Version  : [Pel09] axioms.
% English  :

% Refs     : [Glo07] Gloeckner (2007), University of Hagen at CLEF 2007: An
%          : [PW07]  Pelzer & Wernhard (2007), System Description: E-KRHype
%          : [FG+08] Furbach et al. (2008), LogAnswer - A Deduction-Based Q
%          : [Pel09] Pelzer (2009), Email to Geoff Sutcliffe
% Source   : [Pel09]
% Names    : synth_qa07_003_mira_news_508_tptp [Pel09]

% Status   : Theorem
% Rating   : 0.50 v6.3.0, 0.54 v6.2.0, 0.73 v6.1.0, 0.72 v6.0.0, 0.50 v5.5.0, 0.75 v5.4.0, 0.70 v5.3.0, 0.78 v5.2.0, 0.57 v5.0.0, 0.65 v4.1.0, 0.61 v4.0.1, 0.68 v4.0.0
% Syntax   : Number of formulae    : 10189 (10061 unit)
%            Number of atoms       : 10918 (   0 equality)
%            Maximal formula depth :  216 (   1 average)
%            Number of connectives :  729 (   0   ~;  18   |; 585   &)
%                                         (   0 <=>; 126  =>;   0  <=)
%                                         (   0 <~>;   0  ~|;   0  ~&)
%            Number of predicates  :   87 (   0 propositional; 2-9 arity)
%            Number of functors    : 16659 (16658 constant; 0-2 arity)
%            Number of variables   :  472 (   0 sgn; 405   !;  67   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_THM_RFO_NEQ

% Comments : The different versions of this problem stem from the use of
%            different text snippet retrieval modules, and different snippets
%            being found. The problem tries to prove the questions from the
%            snippet and the background knowledge.
%------------------------------------------------------------------------------
%----Include LogAnswer axioms
include('Axioms/CSR004+0.ax').
%------------------------------------------------------------------------------
fof(synth_qa07_003_mira_news_508,conjecture,(
    ? [X0,X1,X2,X3] :
      ( attr(X1,X0)
      & scar(X2,X3)
      & sub(X0,name_1_1)
      & val(X0,usa_0) ) )).

fof(ave07_era5_synth_qa07_003_mira_news_508,hypothesis,
    ( sub(c61153,freiheitsstatue_1_1)
    & attch(c61204,c61207)
    & sub(c61207,fackel_1_1)
    & sub(c61214,lilie_1_1)
    & sub(c61231,sich_1_1)
    & pred(c61236,alkoholfahne_1_1)
    & attch(c61243,c61236)
    & attr(c61243,c61244)
    & sub(c61243,land_1_1)
    & sub(c61244,name_1_1)
    & val(c61244,usa_0)
    & attr(c61248,c61249)
    & sub(c61248,gebiet_1_1)
    & sub(c61249,name_1_1)
    & val(c61249,bosnien_0)
    & sub(c61250,herzegowina_1_1)
    & tupl_p9(c62887,c61153,c61207,c61214,c61228,c61231,c61236,c61248,c61250)
    & assoc(freiheitsstatue_1_1,freiheit_1_1)
    & sub(freiheitsstatue_1_1,statue_1_1)
    & sort(c61153,d)
    & card(c61153,int1)
    & etype(c61153,int0)
    & fact(c61153,real)
    & gener(c61153,sp)
    & quant(c61153,one)
    & refer(c61153,det)
    & varia(c61153,con)
    & sort(freiheitsstatue_1_1,d)
    & card(freiheitsstatue_1_1,int1)
    & etype(freiheitsstatue_1_1,int0)
    & fact(freiheitsstatue_1_1,real)
    & gener(freiheitsstatue_1_1,ge)
    & quant(freiheitsstatue_1_1,one)
    & refer(freiheitsstatue_1_1,refer_c)
    & varia(freiheitsstatue_1_1,varia_c)
    & sort(c61204,o)
    & card(c61204,int1)
    & etype(c61204,int0)
    & fact(c61204,real)
    & gener(c61204,sp)
    & quant(c61204,one)
    & refer(c61204,det)
    & varia(c61204,varia_c)
    & sort(c61207,d)
    & card(c61207,int1)
    & etype(c61207,int0)
    & fact(c61207,real)
    & gener(c61207,sp)
    & quant(c61207,one)
    & refer(c61207,det)
    & varia(c61207,varia_c)
    & sort(fackel_1_1,d)
    & card(fackel_1_1,int1)
    & etype(fackel_1_1,int0)
    & fact(fackel_1_1,real)
    & gener(fackel_1_1,ge)
    & quant(fackel_1_1,one)
    & refer(fackel_1_1,refer_c)
    & varia(fackel_1_1,varia_c)
    & sort(c61214,d)
    & card(c61214,int1)
    & etype(c61214,int0)
    & fact(c61214,real)
    & gener(c61214,sp)
    & quant(c61214,one)
    & refer(c61214,indet)
    & varia(c61214,varia_c)
    & sort(lilie_1_1,d)
    & card(lilie_1_1,int1)
    & etype(lilie_1_1,int0)
    & fact(lilie_1_1,real)
    & gener(lilie_1_1,ge)
    & quant(lilie_1_1,one)
    & refer(lilie_1_1,refer_c)
    & varia(lilie_1_1,varia_c)
    & sort(c61231,o)
    & card(c61231,int1)
    & etype(c61231,int0)
    & fact(c61231,real)
    & gener(c61231,gener_c)
    & quant(c61231,one)
    & refer(c61231,refer_c)
    & varia(c61231,varia_c)
    & sort(sich_1_1,o)
    & card(sich_1_1,int1)
    & etype(sich_1_1,int0)
    & fact(sich_1_1,real)
    & gener(sich_1_1,gener_c)
    & quant(sich_1_1,one)
    & refer(sich_1_1,refer_c)
    & varia(sich_1_1,varia_c)
    & sort(c61236,d)
    & card(c61236,cons(x_constant,cons(int1,nil)))
    & etype(c61236,int1)
    & fact(c61236,real)
    & gener(c61236,sp)
    & quant(c61236,mult)
    & refer(c61236,det)
    & varia(c61236,con)
    & sort(alkoholfahne_1_1,d)
    & card(alkoholfahne_1_1,int1)
    & etype(alkoholfahne_1_1,int0)
    & fact(alkoholfahne_1_1,real)
    & gener(alkoholfahne_1_1,ge)
    & quant(alkoholfahne_1_1,one)
    & refer(alkoholfahne_1_1,refer_c)
    & varia(alkoholfahne_1_1,varia_c)
    & sort(c61243,d)
    & sort(c61243,io)
    & card(c61243,int1)
    & etype(c61243,int0)
    & fact(c61243,real)
    & gener(c61243,sp)
    & quant(c61243,one)
    & refer(c61243,det)
    & varia(c61243,con)
    & sort(c61244,na)
    & card(c61244,int1)
    & etype(c61244,int0)
    & fact(c61244,real)
    & gener(c61244,sp)
    & quant(c61244,one)
    & refer(c61244,indet)
    & varia(c61244,varia_c)
    & sort(land_1_1,d)
    & sort(land_1_1,io)
    & card(land_1_1,int1)
    & etype(land_1_1,int0)
    & fact(land_1_1,real)
    & gener(land_1_1,ge)
    & quant(land_1_1,one)
    & refer(land_1_1,refer_c)
    & varia(land_1_1,varia_c)
    & sort(name_1_1,na)
    & card(name_1_1,int1)
    & etype(name_1_1,int0)
    & fact(name_1_1,real)
    & gener(name_1_1,ge)
    & quant(name_1_1,one)
    & refer(name_1_1,refer_c)
    & varia(name_1_1,varia_c)
    & sort(usa_0,fe)
    & sort(c61248,d)
    & card(c61248,int1)
    & etype(c61248,int0)
    & fact(c61248,real)
    & gener(c61248,sp)
    & quant(c61248,one)
    & refer(c61248,det)
    & varia(c61248,con)
    & sort(c61249,na)
    & card(c61249,int1)
    & etype(c61249,int0)
    & fact(c61249,real)
    & gener(c61249,sp)
    & quant(c61249,one)
    & refer(c61249,indet)
    & varia(c61249,varia_c)
    & sort(gebiet_1_1,d)
    & card(gebiet_1_1,int1)
    & etype(gebiet_1_1,int0)
    & fact(gebiet_1_1,real)
    & gener(gebiet_1_1,ge)
    & quant(gebiet_1_1,one)
    & refer(gebiet_1_1,refer_c)
    & varia(gebiet_1_1,varia_c)
    & sort(bosnien_0,fe)
    & sort(c61250,o)
    & card(c61250,int1)
    & etype(c61250,int0)
    & fact(c61250,real)
    & gener(c61250,gener_c)
    & quant(c61250,one)
    & refer(c61250,refer_c)
    & varia(c61250,varia_c)
    & sort(herzegowina_1_1,o)
    & card(herzegowina_1_1,int1)
    & etype(herzegowina_1_1,int0)
    & fact(herzegowina_1_1,real)
    & gener(herzegowina_1_1,ge)
    & quant(herzegowina_1_1,one)
    & refer(herzegowina_1_1,refer_c)
    & varia(herzegowina_1_1,varia_c)
    & sort(c62887,ent)
    & card(c62887,card_c)
    & etype(c62887,etype_c)
    & fact(c62887,real)
    & gener(c62887,gener_c)
    & quant(c62887,quant_c)
    & refer(c62887,refer_c)
    & varia(c62887,varia_c)
    & sort(c61228,o)
    & card(c61228,int1)
    & etype(c61228,int0)
    & fact(c61228,real)
    & gener(c61228,sp)
    & quant(c61228,one)
    & refer(c61228,det)
    & varia(c61228,varia_c)
    & sort(freiheit_1_1,as)
    & sort(freiheit_1_1,io)
    & card(freiheit_1_1,int1)
    & etype(freiheit_1_1,int0)
    & fact(freiheit_1_1,real)
    & gener(freiheit_1_1,ge)
    & quant(freiheit_1_1,one)
    & refer(freiheit_1_1,refer_c)
    & varia(freiheit_1_1,varia_c)
    & sort(statue_1_1,d)
    & card(statue_1_1,int1)
    & etype(statue_1_1,int0)
    & fact(statue_1_1,real)
    & gener(statue_1_1,ge)
    & quant(statue_1_1,one)
    & refer(statue_1_1,refer_c)
    & varia(statue_1_1,varia_c) )).

%------------------------------------------------------------------------------
