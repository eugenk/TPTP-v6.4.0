%------------------------------------------------------------------------------
% File     : CSR114+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Commonsense Reasoning
% Problem  : In which Italian city can you find the Colosseum?
% Version  : [Pel09] axioms.
% English  :

% Refs     : [Glo07] Gloeckner (2007), University of Hagen at CLEF 2007: An
%          : [PW07]  Pelzer & Wernhard (2007), System Description: E-KRHype
%          : [FG+08] Furbach et al. (2008), LogAnswer - A Deduction-Based Q
%          : [Pel09] Pelzer (2009), Email to Geoff Sutcliffe
% Source   : [Pel09]
% Names    : synth_qa07_004_insicht_11_tptp [Pel09]

% Status   : Theorem
% Rating   : 0.29 v6.4.0, 0.21 v6.3.0, 0.23 v6.2.0, 0.36 v6.1.0, 0.48 v6.0.0, 0.50 v5.5.0, 0.54 v5.4.0, 0.48 v5.3.0, 0.57 v5.2.0, 0.36 v5.0.0, 0.40 v4.1.0, 0.50 v4.0.1, 0.47 v4.0.0
% Syntax   : Number of formulae    : 10189 (10061 unit)
%            Number of atoms       : 10950 (   0 equality)
%            Maximal formula depth :  243 (   1 average)
%            Number of connectives :  761 (   0   ~;  18   |; 617   &)
%                                         (   0 <=>; 126  =>;   0  <=)
%                                         (   0 <~>;   0  ~|;   0  ~&)
%            Number of predicates  :   87 (   0 propositional; 2-3 arity)
%            Number of functors    : 16665 (16664 constant; 0-2 arity)
%            Number of variables   :  473 (   0 sgn; 405   !;  68   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_THM_RFO_NEQ

% Comments : The different versions of this problem stem from the use of
%            different text snippet retrieval modules, and different snippets
%            being found. The problem tries to prove the questions from the
%            snippet and the background knowledge.
%------------------------------------------------------------------------------
%----Include LogAnswer axioms
include('Axioms/CSR004+0.ax').
%------------------------------------------------------------------------------
fof(synth_qa07_004_insicht_11,conjecture,(
    ? [X0,X1,X2,X3,X4] :
      ( in(X2,X0)
      & attr(X0,X1)
      & loc(X4,X2)
      & scar(X4,X3)
      & sub(X1,name_1_1)
      & sub(X0,stadt__1_1)
      & sub(X3,kolosseum_1_1)
      & subs(X4,stehen_1_1)
      & val(X1,rom_0) ) )).

fof(ave07_era5_synth_qa07_004_insicht_11,hypothesis,
    ( assoc(c10,c0)
    & assoc(c10,c130)
    & ctxt(c10,c171)
    & loc(c10,c170)
    & obj(c10,c150)
    & scar(c10,c127)
    & subs(c10,haben_1_1)
    & temp(c10,abend_2_1)
    & temp(c10,c135)
    & attr(c127,c128)
    & attr(c127,c129)
    & sub(c127,kirchenf__374rst_1_1)
    & sub(c128,eigenname_1_1)
    & val(c128,johannes_0)
    & sub(c129,familiename_1_1)
    & val(c129,paul_0)
    & sub(c135,freiertag_1_1)
    & loc(c140,c169)
    & sub(c140,kolosseum_1_1)
    & attr(c147,c148)
    & sub(c147,stadt__1_1)
    & sub(c148,name_1_1)
    & val(c148,rom_0)
    & prop(c150,althergebracht_1_1)
    & sub(c150,kreuzung_1_1)
    & in(c169,c147)
    & an(c170,c140)
    & subm(c171,c9)
    & pred(c9,gl__344ubig_2_1)
    & assoc(kreuzung_1_1,kreuz_1_1)
    & sub(kreuzung_1_1,weg_1_1)
    & chsp2(zelebrieren_1_1,c0)
    & sort(c10,st)
    & fact(c10,real)
    & gener(c10,sp)
    & sort(c0,tq)
    & sort(c130,oq)
    & card(c130,int2)
    & sort(c171,o)
    & card(c171,card_c)
    & etype(c171,int1)
    & fact(c171,real)
    & gener(c171,gener_c)
    & quant(c171,quant_c)
    & refer(c171,refer_c)
    & varia(c171,varia_c)
    & sort(c170,l)
    & card(c170,int1)
    & etype(c170,int0)
    & fact(c170,real)
    & gener(c170,sp)
    & quant(c170,one)
    & refer(c170,det)
    & varia(c170,con)
    & sort(c150,d)
    & card(c150,int1)
    & etype(c150,int0)
    & fact(c150,real)
    & gener(c150,sp)
    & quant(c150,one)
    & refer(c150,det)
    & varia(c150,con)
    & sort(c127,d)
    & card(c127,int1)
    & etype(c127,int0)
    & fact(c127,real)
    & gener(c127,sp)
    & quant(c127,one)
    & refer(c127,det)
    & varia(c127,varia_c)
    & sort(haben_1_1,st)
    & fact(haben_1_1,real)
    & gener(haben_1_1,ge)
    & sort(abend_2_1,t)
    & card(abend_2_1,int1)
    & etype(abend_2_1,int0)
    & fact(abend_2_1,real)
    & gener(abend_2_1,sp)
    & quant(abend_2_1,one)
    & refer(abend_2_1,refer_c)
    & varia(abend_2_1,varia_c)
    & sort(c135,ta)
    & card(c135,int1)
    & etype(c135,int0)
    & fact(c135,real)
    & gener(c135,sp)
    & quant(c135,one)
    & refer(c135,det)
    & varia(c135,con)
    & sort(c128,na)
    & card(c128,int1)
    & etype(c128,int0)
    & fact(c128,real)
    & gener(c128,sp)
    & quant(c128,one)
    & refer(c128,indet)
    & varia(c128,varia_c)
    & sort(c129,na)
    & card(c129,int1)
    & etype(c129,int0)
    & fact(c129,real)
    & gener(c129,sp)
    & quant(c129,one)
    & refer(c129,det)
    & varia(c129,varia_c)
    & sort(kirchenf__374rst_1_1,d)
    & card(kirchenf__374rst_1_1,int1)
    & etype(kirchenf__374rst_1_1,int0)
    & fact(kirchenf__374rst_1_1,real)
    & gener(kirchenf__374rst_1_1,ge)
    & quant(kirchenf__374rst_1_1,one)
    & refer(kirchenf__374rst_1_1,refer_c)
    & varia(kirchenf__374rst_1_1,varia_c)
    & sort(eigenname_1_1,na)
    & card(eigenname_1_1,int1)
    & etype(eigenname_1_1,int0)
    & fact(eigenname_1_1,real)
    & gener(eigenname_1_1,ge)
    & quant(eigenname_1_1,one)
    & refer(eigenname_1_1,refer_c)
    & varia(eigenname_1_1,varia_c)
    & sort(johannes_0,fe)
    & sort(familiename_1_1,na)
    & card(familiename_1_1,int1)
    & etype(familiename_1_1,int0)
    & fact(familiename_1_1,real)
    & gener(familiename_1_1,ge)
    & quant(familiename_1_1,one)
    & refer(familiename_1_1,refer_c)
    & varia(familiename_1_1,varia_c)
    & sort(paul_0,fe)
    & sort(freiertag_1_1,ta)
    & card(freiertag_1_1,int1)
    & etype(freiertag_1_1,int0)
    & fact(freiertag_1_1,real)
    & gener(freiertag_1_1,ge)
    & quant(freiertag_1_1,one)
    & refer(freiertag_1_1,refer_c)
    & varia(freiertag_1_1,varia_c)
    & sort(c140,d)
    & card(c140,int1)
    & etype(c140,int0)
    & fact(c140,real)
    & gener(c140,sp)
    & quant(c140,one)
    & refer(c140,det)
    & varia(c140,con)
    & sort(c169,l)
    & card(c169,int1)
    & etype(c169,int0)
    & fact(c169,real)
    & gener(c169,sp)
    & quant(c169,one)
    & refer(c169,det)
    & varia(c169,con)
    & sort(kolosseum_1_1,d)
    & card(kolosseum_1_1,int1)
    & etype(kolosseum_1_1,int0)
    & fact(kolosseum_1_1,real)
    & gener(kolosseum_1_1,sp)
    & quant(kolosseum_1_1,one)
    & refer(kolosseum_1_1,det)
    & varia(kolosseum_1_1,con)
    & sort(c147,d)
    & sort(c147,io)
    & card(c147,int1)
    & etype(c147,int0)
    & fact(c147,real)
    & gener(c147,sp)
    & quant(c147,one)
    & refer(c147,det)
    & varia(c147,con)
    & sort(c148,na)
    & card(c148,int1)
    & etype(c148,int0)
    & fact(c148,real)
    & gener(c148,sp)
    & quant(c148,one)
    & refer(c148,indet)
    & varia(c148,varia_c)
    & sort(stadt__1_1,d)
    & sort(stadt__1_1,io)
    & card(stadt__1_1,int1)
    & etype(stadt__1_1,int0)
    & fact(stadt__1_1,real)
    & gener(stadt__1_1,ge)
    & quant(stadt__1_1,one)
    & refer(stadt__1_1,refer_c)
    & varia(stadt__1_1,varia_c)
    & sort(name_1_1,na)
    & card(name_1_1,int1)
    & etype(name_1_1,int0)
    & fact(name_1_1,real)
    & gener(name_1_1,ge)
    & quant(name_1_1,one)
    & refer(name_1_1,refer_c)
    & varia(name_1_1,varia_c)
    & sort(rom_0,fe)
    & sort(althergebracht_1_1,nq)
    & sort(kreuzung_1_1,d)
    & card(kreuzung_1_1,int1)
    & etype(kreuzung_1_1,int0)
    & fact(kreuzung_1_1,real)
    & gener(kreuzung_1_1,ge)
    & quant(kreuzung_1_1,one)
    & refer(kreuzung_1_1,refer_c)
    & varia(kreuzung_1_1,varia_c)
    & sort(c9,o)
    & card(c9,cons(x_constant,cons(int1,nil)))
    & etype(c9,int1)
    & fact(c9,real)
    & gener(c9,gener_c)
    & quant(c9,mult)
    & refer(c9,indet)
    & varia(c9,varia_c)
    & sort(gl__344ubig_2_1,o)
    & card(gl__344ubig_2_1,int1)
    & etype(gl__344ubig_2_1,int0)
    & fact(gl__344ubig_2_1,real)
    & gener(gl__344ubig_2_1,ge)
    & quant(gl__344ubig_2_1,one)
    & refer(gl__344ubig_2_1,refer_c)
    & varia(gl__344ubig_2_1,varia_c)
    & sort(kreuz_1_1,d)
    & sort(kreuz_1_1,io)
    & card(kreuz_1_1,int1)
    & etype(kreuz_1_1,int0)
    & fact(kreuz_1_1,real)
    & gener(kreuz_1_1,ge)
    & quant(kreuz_1_1,one)
    & refer(kreuz_1_1,refer_c)
    & varia(kreuz_1_1,varia_c)
    & sort(weg_1_1,d)
    & card(weg_1_1,int1)
    & etype(weg_1_1,int0)
    & fact(weg_1_1,real)
    & gener(weg_1_1,ge)
    & quant(weg_1_1,one)
    & refer(weg_1_1,refer_c)
    & varia(weg_1_1,varia_c)
    & sort(zelebrieren_1_1,da)
    & fact(zelebrieren_1_1,real)
    & gener(zelebrieren_1_1,ge) )).

%------------------------------------------------------------------------------
