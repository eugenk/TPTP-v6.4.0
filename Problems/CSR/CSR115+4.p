%------------------------------------------------------------------------------
% File     : CSR115+4 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Commonsense Reasoning
% Problem  : Which British company was taken over by BMW in 1994?
% Version  : [Pel09] axioms.
% English  :

% Refs     : [Glo07] Gloeckner (2007), University of Hagen at CLEF 2007: An
%          : [PW07]  Pelzer & Wernhard (2007), System Description: E-KRHype
%          : [FG+08] Furbach et al. (2008), LogAnswer - A Deduction-Based Q
%          : [Pel09] Pelzer (2009), Email to Geoff Sutcliffe
% Source   : [Pel09]
% Names    : synth_qa07_007_mira_news_1087_tptp [Pel09]

% Status   : Theorem
% Rating   : 0.29 v6.4.0, 0.21 v6.3.0, 0.31 v6.2.0, 0.45 v6.1.0, 0.52 v6.0.0, 0.25 v5.5.0, 0.46 v5.4.0, 0.43 v5.3.0, 0.52 v5.2.0, 0.50 v5.0.0, 0.40 v4.1.0, 0.44 v4.0.1, 0.42 v4.0.0
% Syntax   : Number of formulae    : 10189 (10062 unit)
%            Number of atoms       : 10744 (   0 equality)
%            Maximal formula depth :   45 (   1 average)
%            Number of connectives :  555 (   0   ~;  18   |; 411   &)
%                                         (   0 <=>; 126  =>;   0  <=)
%                                         (   0 <~>;   0  ~|;   0  ~&)
%            Number of predicates  :   88 (   0 propositional; 2-4 arity)
%            Number of functors    : 16646 (16645 constant; 0-2 arity)
%            Number of variables   :  470 (   0 sgn; 405   !;  65   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_THM_RFO_NEQ

% Comments : The different versions of this problem stem from the use of
%            different text snippet retrieval modules, and different snippets
%            being found. The problem tries to prove the questions from the
%            snippet and the background knowledge.
%------------------------------------------------------------------------------
%----Include LogAnswer axioms
include('Axioms/CSR004+0.ax').
%------------------------------------------------------------------------------
fof(synth_qa07_007_mira_news_1087,conjecture,(
    ? [X0,X1] : obj(X1,X0) )).

fof(ave07_era5_synth_qa07_007_mira_news_1087,hypothesis,
    ( sub(c2383,mark_1_1)
    & sub(c2403,beitrag_1_1)
    & quant_p3(c2409,c2406,million_1_1)
    & tupl_p4(c2590,c2383,c2403,c2409)
    & sort(c2383,me)
    & fact(c2383,real)
    & sort(mark_1_1,me)
    & gener(mark_1_1,ge)
    & sort(c2403,io)
    & card(c2403,int1)
    & etype(c2403,int0)
    & fact(c2403,real)
    & gener(c2403,sp)
    & quant(c2403,one)
    & refer(c2403,indet)
    & varia(c2403,varia_c)
    & sort(beitrag_1_1,io)
    & card(beitrag_1_1,int1)
    & etype(beitrag_1_1,int0)
    & fact(beitrag_1_1,real)
    & gener(beitrag_1_1,ge)
    & quant(beitrag_1_1,one)
    & refer(beitrag_1_1,refer_c)
    & varia(beitrag_1_1,varia_c)
    & sort(c2409,co)
    & sort(c2409,m)
    & card(c2409,card_c)
    & etype(c2409,etype_c)
    & fact(c2409,real)
    & gener(c2409,gener_c)
    & quant(c2409,quant_c)
    & refer(c2409,refer_c)
    & varia(c2409,con)
    & sort(c2406,nu)
    & card(c2406,int12)
    & sort(million_1_1,me)
    & gener(million_1_1,ge)
    & sort(c2590,ent)
    & card(c2590,card_c)
    & etype(c2590,etype_c)
    & fact(c2590,real)
    & gener(c2590,gener_c)
    & quant(c2590,quant_c)
    & refer(c2590,refer_c)
    & varia(c2590,varia_c) )).

%------------------------------------------------------------------------------
