%------------------------------------------------------------------------------
% File     : CSR115+79 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Commonsense Reasoning
% Problem  : Which British company was taken over by BMW in 1994?
% Version  : [Pel09] axioms.
% English  :

% Refs     : [Glo07] Gloeckner (2007), University of Hagen at CLEF 2007: An
%          : [PW07]  Pelzer & Wernhard (2007), System Description: E-KRHype
%          : [FG+08] Furbach et al. (2008), LogAnswer - A Deduction-Based Q
%          : [Pel09] Pelzer (2009), Email to Geoff Sutcliffe
% Source   : [Pel09]
% Names    : synth_qa07_007_mira_wp_489_tptp [Pel09]

% Status   : Theorem
% Rating   : 0.29 v6.4.0, 0.21 v6.3.0, 0.23 v6.2.0, 0.36 v6.1.0, 0.48 v6.0.0, 0.50 v5.5.0, 0.54 v5.4.0, 0.52 v5.3.0, 0.61 v5.2.0, 0.43 v5.0.0, 0.45 v4.1.0, 0.44 v4.0.1, 0.47 v4.0.0
% Syntax   : Number of formulae    : 10189 (10061 unit)
%            Number of atoms       : 10835 (   0 equality)
%            Maximal formula depth :  128 (   1 average)
%            Number of connectives :  646 (   0   ~;  18   |; 502   &)
%                                         (   0 <=>; 126  =>;   0  <=)
%                                         (   0 <~>;   0  ~|;   0  ~&)
%            Number of predicates  :   87 (   0 propositional; 2-5 arity)
%            Number of functors    : 16651 (16650 constant; 0-2 arity)
%            Number of variables   :  475 (   0 sgn; 405   !;  70   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_THM_RFO_NEQ

% Comments : The different versions of this problem stem from the use of
%            different text snippet retrieval modules, and different snippets
%            being found. The problem tries to prove the questions from the
%            snippet and the background knowledge.
%------------------------------------------------------------------------------
%----Include LogAnswer axioms
include('Axioms/CSR004+0.ax').
%------------------------------------------------------------------------------
fof(synth_qa07_007_mira_wp_489,conjecture,(
    ? [X0,X1,X2,X3,X4,X5,X6] :
      ( attr(X0,X1)
      & attr(X3,X2)
      & attr(X5,X6)
      & obj(X4,X0)
      & sub(X1,name_1_1)
      & sub(X0,firma_1_1)
      & sub(X2,name_1_1)
      & val(X1,bmw_0)
      & val(X2,bmw_0) ) )).

fof(ave07_era5_synth_qa07_007_mira_wp_489,hypothesis,
    ( sub(c104,roadster_1_1)
    & attr(c129,c130)
    & sub(c129,firma_1_1)
    & sub(c130,name_1_1)
    & val(c130,bmw_0)
    & sub(c134,basis_1_1)
    & sub(c138,standbein_1_1)
    & attch(c142,c138)
    & sub(c142,firma_1_1)
    & tupl_p5(c287,c104,c129,c134,c138)
    & assoc(standbein_1_1,stand_1_1)
    & sub(standbein_1_1,bein_1_1)
    & sort(c104,d)
    & card(c104,int1)
    & etype(c104,int0)
    & fact(c104,real)
    & gener(c104,gener_c)
    & quant(c104,one)
    & refer(c104,refer_c)
    & varia(c104,varia_c)
    & sort(roadster_1_1,d)
    & card(roadster_1_1,int1)
    & etype(roadster_1_1,int0)
    & fact(roadster_1_1,real)
    & gener(roadster_1_1,ge)
    & quant(roadster_1_1,one)
    & refer(roadster_1_1,refer_c)
    & varia(roadster_1_1,varia_c)
    & sort(c129,d)
    & sort(c129,io)
    & card(c129,int1)
    & etype(c129,int0)
    & fact(c129,real)
    & gener(c129,sp)
    & quant(c129,one)
    & refer(c129,det)
    & varia(c129,con)
    & sort(c130,na)
    & card(c130,int1)
    & etype(c130,int0)
    & fact(c130,real)
    & gener(c130,sp)
    & quant(c130,one)
    & refer(c130,indet)
    & varia(c130,varia_c)
    & sort(firma_1_1,d)
    & sort(firma_1_1,io)
    & card(firma_1_1,int1)
    & etype(firma_1_1,int0)
    & fact(firma_1_1,real)
    & gener(firma_1_1,ge)
    & quant(firma_1_1,one)
    & refer(firma_1_1,refer_c)
    & varia(firma_1_1,varia_c)
    & sort(name_1_1,na)
    & card(name_1_1,int1)
    & etype(name_1_1,int0)
    & fact(name_1_1,real)
    & gener(name_1_1,ge)
    & quant(name_1_1,one)
    & refer(name_1_1,refer_c)
    & varia(name_1_1,varia_c)
    & sort(bmw_0,fe)
    & sort(c134,io)
    & card(c134,int1)
    & etype(c134,int0)
    & fact(c134,real)
    & gener(c134,gener_c)
    & quant(c134,one)
    & refer(c134,refer_c)
    & varia(c134,varia_c)
    & sort(basis_1_1,io)
    & card(basis_1_1,int1)
    & etype(basis_1_1,int0)
    & fact(basis_1_1,real)
    & gener(basis_1_1,ge)
    & quant(basis_1_1,one)
    & refer(basis_1_1,refer_c)
    & varia(basis_1_1,varia_c)
    & sort(c138,d)
    & card(c138,int1)
    & etype(c138,int0)
    & fact(c138,real)
    & gener(c138,sp)
    & quant(c138,one)
    & refer(c138,det)
    & varia(c138,con)
    & sort(standbein_1_1,d)
    & card(standbein_1_1,int1)
    & etype(standbein_1_1,int0)
    & fact(standbein_1_1,real)
    & gener(standbein_1_1,ge)
    & quant(standbein_1_1,one)
    & refer(standbein_1_1,refer_c)
    & varia(standbein_1_1,varia_c)
    & sort(c142,d)
    & sort(c142,io)
    & card(c142,int1)
    & etype(c142,int0)
    & fact(c142,real)
    & gener(c142,sp)
    & quant(c142,one)
    & refer(c142,det)
    & varia(c142,con)
    & sort(c287,ent)
    & card(c287,card_c)
    & etype(c287,etype_c)
    & fact(c287,real)
    & gener(c287,gener_c)
    & quant(c287,quant_c)
    & refer(c287,refer_c)
    & varia(c287,varia_c)
    & sort(stand_1_1,d)
    & card(stand_1_1,int1)
    & etype(stand_1_1,int0)
    & fact(stand_1_1,real)
    & gener(stand_1_1,ge)
    & quant(stand_1_1,one)
    & refer(stand_1_1,refer_c)
    & varia(stand_1_1,varia_c)
    & sort(bein_1_1,d)
    & card(bein_1_1,int1)
    & etype(bein_1_1,int0)
    & fact(bein_1_1,real)
    & gener(bein_1_1,ge)
    & quant(bein_1_1,one)
    & refer(bein_1_1,refer_c)
    & varia(bein_1_1,varia_c) )).

%------------------------------------------------------------------------------
