%------------------------------------------------------------------------------
% File     : CSR115+97 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Commonsense Reasoning
% Problem  : Which British company was taken over by BMW in 1994?
% Version  : [Pel09] axioms.
% English  :

% Refs     : [Glo07] Gloeckner (2007), University of Hagen at CLEF 2007: An
%          : [PW07]  Pelzer & Wernhard (2007), System Description: E-KRHype
%          : [FG+08] Furbach et al. (2008), LogAnswer - A Deduction-Based Q
%          : [Pel09] Pelzer (2009), Email to Geoff Sutcliffe
% Source   : [Pel09]
% Names    : synth_qa07_007_mw3_195_tptp [Pel09]

% Status   : Theorem
% Rating   : 0.29 v6.4.0, 0.21 v6.3.0, 0.23 v6.2.0, 0.36 v6.1.0, 0.52 v6.0.0, 0.50 v5.5.0, 0.58 v5.4.0, 0.57 v5.3.0, 0.65 v5.2.0, 0.36 v5.0.0, 0.50 v4.1.0, 0.56 v4.0.1, 0.63 v4.0.0
% Syntax   : Number of formulae    : 10189 (10061 unit)
%            Number of atoms       : 10979 (   0 equality)
%            Maximal formula depth :  271 (   1 average)
%            Number of connectives :  790 (   0   ~;  18   |; 646   &)
%                                         (   0 <=>; 126  =>;   0  <=)
%                                         (   0 <~>;   0  ~|;   0  ~&)
%            Number of predicates  :   88 (   0 propositional; 2-10 arity)
%            Number of functors    : 16669 (16668 constant; 0-2 arity)
%            Number of variables   :  475 (   0 sgn; 405   !;  70   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_THM_RFO_NEQ

% Comments : The different versions of this problem stem from the use of
%            different text snippet retrieval modules, and different snippets
%            being found. The problem tries to prove the questions from the
%            snippet and the background knowledge.
%------------------------------------------------------------------------------
%----Include LogAnswer axioms
include('Axioms/CSR004+0.ax').
%------------------------------------------------------------------------------
fof(synth_qa07_007_mw3_195,conjecture,(
    ? [X0,X1,X2,X3,X4,X5,X6] :
      ( attr(X0,X1)
      & attr(X3,X2)
      & attr(X5,X6)
      & obj(X4,X0)
      & sub(X1,name_1_1)
      & sub(X0,firma_1_1)
      & sub(X2,name_1_1)
      & sub(X6,jahr__1_1)
      & val(X1,bmw_0)
      & val(X2,bmw_0) ) )).

fof(ave07_era5_synth_qa07_007_mw3_195,hypothesis,
    ( sub(c631,bau_1_1)
    & attch(c634,c631)
    & sub(c634,goggomobil_1_1)
    & sub(c640,man_1_1)
    & attr(c649,c650)
    & attr(c649,c651)
    & attr(c649,c652)
    & sub(c650,tag_1_1)
    & val(c650,c646)
    & sub(c651,monat_1_1)
    & val(c651,c647)
    & sub(c652,jahr__1_1)
    & val(c652,c648)
    & quant_p3(c658,c654,jahr__1_1)
    & quant_p3(c663,c659,monat_1_1)
    & attr(c703,c704)
    & sub(c703,firma_1_1)
    & sub(c704,name_1_1)
    & val(c704,bmw_0)
    & sub(c705,firma_1_1)
    & sub(c711,glas_1_1)
    & attr(c713,c714)
    & sub(c713,mensch_1_1)
    & sub(c714,eigenname_1_1)
    & val(c714,hans_0)
    & tupl_p10(c745,c631,c640,c649,c658,c663,c703,c705,c713,c711)
    & sort(c631,d)
    & card(c631,int1)
    & etype(c631,int0)
    & fact(c631,real)
    & gener(c631,sp)
    & quant(c631,one)
    & refer(c631,det)
    & varia(c631,con)
    & sort(bau_1_1,d)
    & card(bau_1_1,int1)
    & etype(bau_1_1,int0)
    & fact(bau_1_1,real)
    & gener(bau_1_1,ge)
    & quant(bau_1_1,one)
    & refer(bau_1_1,refer_c)
    & varia(bau_1_1,varia_c)
    & sort(c634,o)
    & card(c634,int1)
    & etype(c634,int0)
    & fact(c634,real)
    & gener(c634,sp)
    & quant(c634,one)
    & refer(c634,det)
    & varia(c634,con)
    & sort(goggomobil_1_1,o)
    & card(goggomobil_1_1,int1)
    & etype(goggomobil_1_1,int0)
    & fact(goggomobil_1_1,real)
    & gener(goggomobil_1_1,ge)
    & quant(goggomobil_1_1,one)
    & refer(goggomobil_1_1,refer_c)
    & varia(goggomobil_1_1,varia_c)
    & sort(c640,d)
    & card(c640,int1)
    & etype(c640,int0)
    & fact(c640,real)
    & gener(c640,ge)
    & quant(c640,one)
    & refer(c640,refer_c)
    & varia(c640,varia_c)
    & sort(man_1_1,d)
    & card(man_1_1,int1)
    & etype(man_1_1,int0)
    & fact(man_1_1,real)
    & gener(man_1_1,ge)
    & quant(man_1_1,one)
    & refer(man_1_1,refer_c)
    & varia(man_1_1,varia_c)
    & sort(c649,t)
    & card(c649,int1)
    & etype(c649,int0)
    & fact(c649,real)
    & gener(c649,sp)
    & quant(c649,one)
    & refer(c649,det)
    & varia(c649,con)
    & sort(c650,me)
    & sort(c650,oa)
    & sort(c650,ta)
    & card(c650,card_c)
    & etype(c650,etype_c)
    & fact(c650,real)
    & gener(c650,sp)
    & quant(c650,quant_c)
    & refer(c650,refer_c)
    & varia(c650,varia_c)
    & sort(c651,me)
    & sort(c651,oa)
    & sort(c651,ta)
    & card(c651,card_c)
    & etype(c651,etype_c)
    & fact(c651,real)
    & gener(c651,sp)
    & quant(c651,quant_c)
    & refer(c651,refer_c)
    & varia(c651,varia_c)
    & sort(c652,me)
    & sort(c652,oa)
    & sort(c652,ta)
    & card(c652,card_c)
    & etype(c652,etype_c)
    & fact(c652,real)
    & gener(c652,sp)
    & quant(c652,quant_c)
    & refer(c652,refer_c)
    & varia(c652,varia_c)
    & sort(tag_1_1,me)
    & sort(tag_1_1,oa)
    & sort(tag_1_1,ta)
    & card(tag_1_1,card_c)
    & etype(tag_1_1,etype_c)
    & fact(tag_1_1,real)
    & gener(tag_1_1,ge)
    & quant(tag_1_1,quant_c)
    & refer(tag_1_1,refer_c)
    & varia(tag_1_1,varia_c)
    & sort(c646,nu)
    & card(c646,int30)
    & sort(monat_1_1,me)
    & sort(monat_1_1,oa)
    & sort(monat_1_1,ta)
    & card(monat_1_1,card_c)
    & etype(monat_1_1,etype_c)
    & fact(monat_1_1,real)
    & gener(monat_1_1,ge)
    & quant(monat_1_1,quant_c)
    & refer(monat_1_1,refer_c)
    & varia(monat_1_1,varia_c)
    & sort(c647,nu)
    & card(c647,int6)
    & sort(jahr__1_1,me)
    & sort(jahr__1_1,oa)
    & sort(jahr__1_1,ta)
    & card(jahr__1_1,card_c)
    & etype(jahr__1_1,etype_c)
    & fact(jahr__1_1,real)
    & gener(jahr__1_1,ge)
    & quant(jahr__1_1,quant_c)
    & refer(jahr__1_1,refer_c)
    & varia(jahr__1_1,varia_c)
    & sort(c648,nu)
    & card(c648,int1969)
    & sort(c658,m)
    & sort(c658,ta)
    & card(c658,card_c)
    & etype(c658,etype_c)
    & fact(c658,real)
    & gener(c658,gener_c)
    & quant(c658,quant_c)
    & refer(c658,refer_c)
    & varia(c658,varia_c)
    & sort(c654,nu)
    & card(c654,int2)
    & sort(c663,m)
    & sort(c663,ta)
    & card(c663,card_c)
    & etype(c663,etype_c)
    & fact(c663,real)
    & gener(c663,gener_c)
    & quant(c663,quant_c)
    & refer(c663,refer_c)
    & varia(c663,varia_c)
    & sort(c659,nu)
    & card(c659,int6)
    & sort(c703,d)
    & sort(c703,io)
    & card(c703,int1)
    & etype(c703,int0)
    & fact(c703,real)
    & gener(c703,sp)
    & quant(c703,one)
    & refer(c703,det)
    & varia(c703,con)
    & sort(c704,na)
    & card(c704,int1)
    & etype(c704,int0)
    & fact(c704,real)
    & gener(c704,sp)
    & quant(c704,one)
    & refer(c704,indet)
    & varia(c704,varia_c)
    & sort(firma_1_1,d)
    & sort(firma_1_1,io)
    & card(firma_1_1,int1)
    & etype(firma_1_1,int0)
    & fact(firma_1_1,real)
    & gener(firma_1_1,ge)
    & quant(firma_1_1,one)
    & refer(firma_1_1,refer_c)
    & varia(firma_1_1,varia_c)
    & sort(name_1_1,na)
    & card(name_1_1,int1)
    & etype(name_1_1,int0)
    & fact(name_1_1,real)
    & gener(name_1_1,ge)
    & quant(name_1_1,one)
    & refer(name_1_1,refer_c)
    & varia(name_1_1,varia_c)
    & sort(bmw_0,fe)
    & sort(c705,d)
    & sort(c705,io)
    & card(c705,int1)
    & etype(c705,int0)
    & fact(c705,real)
    & gener(c705,sp)
    & quant(c705,one)
    & refer(c705,det)
    & varia(c705,con)
    & sort(c711,s)
    & card(c711,int1)
    & etype(c711,int0)
    & fact(c711,real)
    & gener(c711,gener_c)
    & quant(c711,one)
    & refer(c711,refer_c)
    & varia(c711,varia_c)
    & sort(glas_1_1,s)
    & card(glas_1_1,int1)
    & etype(glas_1_1,int0)
    & fact(glas_1_1,real)
    & gener(glas_1_1,ge)
    & quant(glas_1_1,one)
    & refer(glas_1_1,refer_c)
    & varia(glas_1_1,varia_c)
    & sort(c713,d)
    & card(c713,int1)
    & etype(c713,int0)
    & fact(c713,real)
    & gener(c713,sp)
    & quant(c713,one)
    & refer(c713,det)
    & varia(c713,con)
    & sort(c714,na)
    & card(c714,int1)
    & etype(c714,int0)
    & fact(c714,real)
    & gener(c714,sp)
    & quant(c714,one)
    & refer(c714,indet)
    & varia(c714,varia_c)
    & sort(mensch_1_1,d)
    & card(mensch_1_1,int1)
    & etype(mensch_1_1,int0)
    & fact(mensch_1_1,real)
    & gener(mensch_1_1,ge)
    & quant(mensch_1_1,one)
    & refer(mensch_1_1,refer_c)
    & varia(mensch_1_1,varia_c)
    & sort(eigenname_1_1,na)
    & card(eigenname_1_1,int1)
    & etype(eigenname_1_1,int0)
    & fact(eigenname_1_1,real)
    & gener(eigenname_1_1,ge)
    & quant(eigenname_1_1,one)
    & refer(eigenname_1_1,refer_c)
    & varia(eigenname_1_1,varia_c)
    & sort(hans_0,fe)
    & sort(c745,ent)
    & card(c745,card_c)
    & etype(c745,etype_c)
    & fact(c745,real)
    & gener(c745,gener_c)
    & quant(c745,quant_c)
    & refer(c745,refer_c)
    & varia(c745,varia_c) )).

%------------------------------------------------------------------------------
