%------------------------------------------------------------------------------
% File     : CSR118+1 : TPTP v6.4.0. Bugfixed v5.4.0.
% Domain   : Commonsense Reasoning
% Problem  : Abraham Lincoln is a mammal
% Version  : Especial > Augmented > Especial.
% English  :

% Refs     : [NP01]  Niles & Pease (2001), Towards A Standard Upper Ontology
% Source   : [TPTP]
% Names    : 

% Status   : ContradictoryAxioms
% Rating   : 0.00 v6.1.0, 0.90 v6.0.0, 0.91 v5.5.0, 0.93 v5.4.0
% Syntax   : Number of formulae    : 14765 (12056 unit)
%            Number of atoms       : 25455 (1092 equality)
%            Maximal formula depth :   26 (   2 average)
%            Number of connectives : 11236 ( 546   ~;  69   |;4796   &)
%                                         ( 102 <=>;5723  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :    5 (   0 propositional; 1-8 arity)
%            Number of functors    : 1808 (1808 constant; 0-8 arity)
%            Number of variables   : 7426 (   1 sgn;6947   !; 479   ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_CAX_RFO_SEQ

% Comments : This version includes the cache axioms.
% Bugfixes : v5.3.0 - Bugfixes in CSR003 axiom files.
%          : v5.4.0 - Bugfixes in CSR003 axiom files.
%------------------------------------------------------------------------------
%----Include axioms from SUMO
include('Axioms/CSR003+0.ax').
%----Include cache axioms for SUMO
include('Axioms/CSR003+3.ax').
%------------------------------------------------------------------------------
fof(abe_human,axiom,(
    s__instance(s__AbrahamLincoln,s__Human) )).

fof(abe_mammal,conjecture,(
    s__instance(s__AbrahamLincoln,s__Mammal) )).

%------------------------------------------------------------------------------
