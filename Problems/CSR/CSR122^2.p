%------------------------------------------------------------------------------
% File     : CSR122^2 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Commonsense Reasoning
% Problem  : Do Mary and Sue like Bill in 2009?
% Version  : Especial > Augmented > Especial.
% English  : Mary likes Bill and Sue likes Bill. Does this also hold during 
%            year 2009? In order to make this statement provable we assume 
%            that 'True' holds during each time context.

% Refs     : [Ben10] Benzmueller (2010), Email to Geoff Sutcliffe
% Source   : [Ben10]
% Names    : ef_4.tq_SUMO_sine [Ben10]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.14 v6.1.0, 0.71 v6.0.0, 0.29 v5.5.0, 0.33 v5.4.0, 0.40 v5.2.0, 0.60 v5.1.0, 0.80 v4.1.0
% Syntax   : Number of formulae    :  282 (   0 unit;  89 type;   0 defn)
%            Number of atoms       :  887 (   9 equality; 215 variable)
%            Maximal formula depth :   12 (   4 average)
%            Number of connectives :  676 (   0   ~;   4   |;  29   &; 604   @)
%                                         (   6 <=>;  33  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  124 ( 124   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   94 (  89   :;   0   =)
%            Number of variables   :  101 (   0 sgn; 100   !;   1   ?;   0   ^)
%                                         ( 101   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This is a simple test problem for reasoning in/about SUMO.
%            Initally the problem has been hand generated in KIF syntax in
%            SigmaKEE and then automatically translated by Benzmueller's
%            KIF2TH0 translator into THF syntax.
%          : The translation has been applied in two modes: local and SInE.
%            The local mode only translates the local assumptions and the
%            query. The SInE mode additionally translates the SInE-extract
%            of the loaded knowledge base (usually SUMO).
%          : The examples are selected to illustrate the benefits of
%            higher-order reasoning in ontology reasoning.
%------------------------------------------------------------------------------
%----The extracted signature
thf(numbers,type,(
    num: $tType )).

thf(agent_THFTYPE_i,type,(
    agent_THFTYPE_i: $i )).

thf(attribute_THFTYPE_i,type,(
    attribute_THFTYPE_i: $i )).

thf(disjointRelation_THFTYPE_IiioI,type,(
    disjointRelation_THFTYPE_IiioI: $i > $i > $o )).

thf(disjoint_THFTYPE_IiioI,type,(
    disjoint_THFTYPE_IiioI: $i > $i > $o )).

thf(documentation_THFTYPE_i,type,(
    documentation_THFTYPE_i: $i )).

thf(domainSubclass_THFTYPE_IIiioIiioI,type,(
    domainSubclass_THFTYPE_IIiioIiioI: ( $i > $i > $o ) > $i > $i > $o )).

thf(domainSubclass_THFTYPE_IiiioI,type,(
    domainSubclass_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(domain_THFTYPE_IIIiioIIiioIoIiioI,type,(
    domain_THFTYPE_IIIiioIIiioIoIiioI: ( ( $i > $i > $o ) > ( $i > $i > $o ) > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiiIiioI,type,(
    domain_THFTYPE_IIiiIiioI: ( $i > $i ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiiioIiioI,type,(
    domain_THFTYPE_IIiiioIiioI: ( $i > $i > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiioIiioI,type,(
    domain_THFTYPE_IIiioIiioI: ( $i > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IiiioI,type,(
    domain_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(duration_THFTYPE_IiioI,type,(
    duration_THFTYPE_IiioI: $i > $i > $o )).

thf(equal_THFTYPE_i,type,(
    equal_THFTYPE_i: $i )).

thf(greaterThan_THFTYPE_i,type,(
    greaterThan_THFTYPE_i: $i )).

thf(holdsDuring_THFTYPE_IiooI,type,(
    holdsDuring_THFTYPE_IiooI: $i > $o > $o )).

thf(instance_THFTYPE_IIIiioIiioIioI,type,(
    instance_THFTYPE_IIIiioIiioIioI: ( ( $i > $i > $o ) > $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiiIioI,type,(
    instance_THFTYPE_IIiiIioI: ( $i > $i ) > $i > $o )).

thf(instance_THFTYPE_IIiiiIioI,type,(
    instance_THFTYPE_IIiiiIioI: ( $i > $i > $i ) > $i > $o )).

thf(instance_THFTYPE_IIiiioIioI,type,(
    instance_THFTYPE_IIiiioIioI: ( $i > $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiioIioI,type,(
    instance_THFTYPE_IIiioIioI: ( $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiooIioI,type,(
    instance_THFTYPE_IIiooIioI: ( $i > $o > $o ) > $i > $o )).

thf(instance_THFTYPE_IiioI,type,(
    instance_THFTYPE_IiioI: $i > $i > $o )).

thf(instrument_THFTYPE_i,type,(
    instrument_THFTYPE_i: $i )).

thf(lAdditionFn_THFTYPE_i,type,(
    lAdditionFn_THFTYPE_i: $i )).

thf(lAsymmetricRelation_THFTYPE_i,type,(
    lAsymmetricRelation_THFTYPE_i: $i )).

thf(lBeginFn_THFTYPE_IiiI,type,(
    lBeginFn_THFTYPE_IiiI: $i > $i )).

thf(lBill_THFTYPE_i,type,(
    lBill_THFTYPE_i: $i )).

thf(lBinaryFunction_THFTYPE_i,type,(
    lBinaryFunction_THFTYPE_i: $i )).

thf(lBinaryPredicate_THFTYPE_i,type,(
    lBinaryPredicate_THFTYPE_i: $i )).

thf(lCardinalityFn_THFTYPE_IiiI,type,(
    lCardinalityFn_THFTYPE_IiiI: $i > $i )).

thf(lDayDuration_THFTYPE_i,type,(
    lDayDuration_THFTYPE_i: $i )).

thf(lDay_THFTYPE_i,type,(
    lDay_THFTYPE_i: $i )).

thf(lEndFn_THFTYPE_IiiI,type,(
    lEndFn_THFTYPE_IiiI: $i > $i )).

thf(lEntity_THFTYPE_i,type,(
    lEntity_THFTYPE_i: $i )).

thf(lInheritableRelation_THFTYPE_i,type,(
    lInheritableRelation_THFTYPE_i: $i )).

thf(lInteger_THFTYPE_i,type,(
    lInteger_THFTYPE_i: $i )).

thf(lIrreflexiveRelation_THFTYPE_i,type,(
    lIrreflexiveRelation_THFTYPE_i: $i )).

thf(lMary_THFTYPE_i,type,(
    lMary_THFTYPE_i: $i )).

thf(lMeasureFn_THFTYPE_IiiiI,type,(
    lMeasureFn_THFTYPE_IiiiI: $i > $i > $i )).

thf(lMonthFn_THFTYPE_i,type,(
    lMonthFn_THFTYPE_i: $i )).

thf(lMonth_THFTYPE_i,type,(
    lMonth_THFTYPE_i: $i )).

thf(lMultiplicationFn_THFTYPE_i,type,(
    lMultiplicationFn_THFTYPE_i: $i )).

thf(lObject_THFTYPE_i,type,(
    lObject_THFTYPE_i: $i )).

thf(lProcess_THFTYPE_i,type,(
    lProcess_THFTYPE_i: $i )).

thf(lQuantity_THFTYPE_i,type,(
    lQuantity_THFTYPE_i: $i )).

thf(lRelationExtendedToQuantities_THFTYPE_i,type,(
    lRelationExtendedToQuantities_THFTYPE_i: $i )).

thf(lRelation_THFTYPE_i,type,(
    lRelation_THFTYPE_i: $i )).

thf(lSetOrClass_THFTYPE_i,type,(
    lSetOrClass_THFTYPE_i: $i )).

thf(lSubtractionFn_THFTYPE_i,type,(
    lSubtractionFn_THFTYPE_i: $i )).

thf(lSue_THFTYPE_i,type,(
    lSue_THFTYPE_i: $i )).

thf(lTemporalCompositionFn_THFTYPE_IiiiI,type,(
    lTemporalCompositionFn_THFTYPE_IiiiI: $i > $i > $i )).

thf(lTemporalCompositionFn_THFTYPE_i,type,(
    lTemporalCompositionFn_THFTYPE_i: $i )).

thf(lTemporalRelation_THFTYPE_i,type,(
    lTemporalRelation_THFTYPE_i: $i )).

thf(lTernaryPredicate_THFTYPE_i,type,(
    lTernaryPredicate_THFTYPE_i: $i )).

thf(lTimeInterval_THFTYPE_i,type,(
    lTimeInterval_THFTYPE_i: $i )).

thf(lTotalValuedRelation_THFTYPE_i,type,(
    lTotalValuedRelation_THFTYPE_i: $i )).

thf(lTransitiveRelation_THFTYPE_i,type,(
    lTransitiveRelation_THFTYPE_i: $i )).

thf(lUnaryFunction_THFTYPE_i,type,(
    lUnaryFunction_THFTYPE_i: $i )).

thf(lWhenFn_THFTYPE_IiiI,type,(
    lWhenFn_THFTYPE_IiiI: $i > $i )).

thf(lWhenFn_THFTYPE_i,type,(
    lWhenFn_THFTYPE_i: $i )).

thf(lYearFn_THFTYPE_IiiI,type,(
    lYearFn_THFTYPE_IiiI: $i > $i )).

thf(lYearFn_THFTYPE_i,type,(
    lYearFn_THFTYPE_i: $i )).

thf(lYear_THFTYPE_i,type,(
    lYear_THFTYPE_i: $i )).

thf(lessThan_THFTYPE_i,type,(
    lessThan_THFTYPE_i: $i )).

thf(likes_THFTYPE_IiioI,type,(
    likes_THFTYPE_IiioI: $i > $i > $o )).

thf(located_THFTYPE_IiioI,type,(
    located_THFTYPE_IiioI: $i > $i > $o )).

thf(meetsTemporally_THFTYPE_IiioI,type,(
    meetsTemporally_THFTYPE_IiioI: $i > $i > $o )).

thf(minus_THFTYPE_IiiiI,type,(
    minus_THFTYPE_IiiiI: $i > $i > $i )).

thf(n12_THFTYPE_i,type,(
    n12_THFTYPE_i: $i )).

thf(n1_THFTYPE_i,type,(
    n1_THFTYPE_i: $i )).

thf(n2009_THFTYPE_i,type,(
    n2009_THFTYPE_i: $i )).

thf(n2_THFTYPE_i,type,(
    n2_THFTYPE_i: $i )).

thf(n3_THFTYPE_i,type,(
    n3_THFTYPE_i: $i )).

thf(orientation_THFTYPE_i,type,(
    orientation_THFTYPE_i: $i )).

thf(part_THFTYPE_IiioI,type,(
    part_THFTYPE_IiioI: $i > $i > $o )).

thf(patient_THFTYPE_i,type,(
    patient_THFTYPE_i: $i )).

thf(rangeSubclass_THFTYPE_IiioI,type,(
    rangeSubclass_THFTYPE_IiioI: $i > $i > $o )).

thf(range_THFTYPE_IiioI,type,(
    range_THFTYPE_IiioI: $i > $i > $o )).

thf(relatedInternalConcept_THFTYPE_IIiioIIiioIoI,type,(
    relatedInternalConcept_THFTYPE_IIiioIIiioIoI: ( $i > $i > $o ) > ( $i > $i > $o ) > $o )).

thf(relatedInternalConcept_THFTYPE_IiIiiIoI,type,(
    relatedInternalConcept_THFTYPE_IiIiiIoI: $i > ( $i > $i ) > $o )).

thf(relatedInternalConcept_THFTYPE_IiioI,type,(
    relatedInternalConcept_THFTYPE_IiioI: $i > $i > $o )).

thf(result_THFTYPE_i,type,(
    result_THFTYPE_i: $i )).

thf(subProcess_THFTYPE_IiioI,type,(
    subProcess_THFTYPE_IiioI: $i > $i > $o )).

thf(subclass_THFTYPE_IiioI,type,(
    subclass_THFTYPE_IiioI: $i > $i > $o )).

thf(subrelation_THFTYPE_IIioIIioIoI,type,(
    subrelation_THFTYPE_IIioIIioIoI: ( $i > $o ) > ( $i > $o ) > $o )).

thf(subrelation_THFTYPE_IiioI,type,(
    subrelation_THFTYPE_IiioI: $i > $i > $o )).

thf(temporalPart_THFTYPE_IiioI,type,(
    temporalPart_THFTYPE_IiioI: $i > $i > $o )).

%----The translated axioms
thf(ax,axiom,(
    ! [REL2: $i,CLASS1: $i,CLASS2: $i,REL1: $i] :
      ( ( ( rangeSubclass_THFTYPE_IiioI @ REL1 @ CLASS1 )
        & ( rangeSubclass_THFTYPE_IiioI @ REL2 @ CLASS2 )
        & ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 ) )
     => ( disjointRelation_THFTYPE_IiioI @ REL1 @ REL2 ) ) )).

thf(ax_001,axiom,
    ( subclass_THFTYPE_IiioI @ lInheritableRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

%KIF documentation:(documentation instance EnglishLanguage "An object is an &%instance of a &%SetOrClass if it is included in that &%SetOrClass. An individual may be an instance of many classes, some of which may be subclasses of others. Thus, there is no assumption in the meaning of &%instance about specificity or uniqueness.")
%KIF documentation:(documentation range EnglishLanguage "Gives the range of a function. In other words, (&%range ?FUNCTION ?CLASS) means that all of the values assigned by ?FUNCTION are &%instances of ?CLASS.")
%KIF documentation:(documentation Process EnglishLanguage "The class of things that happen and have temporal parts or stages. Examples include extended events like a football match or a race, actions like &%Pursuing and &%Reading, and biological processes. The formal definition is: anything that occurs in time but is not an &%Object. Note that a &%Process may have participants 'inside' it which are &%Objects, such as the players in a football match. In a 4D ontology, a &%Process is something whose spatiotemporal extent is thought of as dividing into temporal stages roughly perpendicular to the time-axis.")
thf(ax_002,axiom,(
    ! [X: $i,Y: $i,Z: $i] :
      ( ( ( subclass_THFTYPE_IiioI @ X @ Y )
        & ( instance_THFTYPE_IiioI @ Z @ X ) )
     => ( instance_THFTYPE_IiioI @ Z @ Y ) ) )).

%KIF documentation:(documentation TemporalRelation EnglishLanguage "The &%Class of temporal &%Relations. This &%Class includes notions of (temporal) topology of intervals, (temporal) schemata, and (temporal) extension.")
thf(ax_003,axiom,(
    ! [X: $i,Y: $i] :
      ( ( subclass_THFTYPE_IiioI @ X @ Y )
     => ( ( instance_THFTYPE_IiioI @ X @ lSetOrClass_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ Y @ lSetOrClass_THFTYPE_i ) ) ) )).

%KIF documentation:(documentation disjointRelation EnglishLanguage "This predicate relates two &%Relations. (&%disjointRelation ?REL1 ?REL2) means that the two relations have no tuples in common.")
%KIF documentation:(documentation lessThan EnglishLanguage "(&%lessThan ?NUMBER1 ?NUMBER2) is true just in case the &%Quantity ?NUMBER1 is less than the &%Quantity ?NUMBER2.")
%KIF documentation:(documentation AsymmetricRelation EnglishLanguage "A &%BinaryRelation is asymmetric if and only if it is both an &%AntisymmetricRelation and an &%IrreflexiveRelation.")
%KIF documentation:(documentation MonthFn EnglishLanguage "A &%BinaryFunction that maps a subclass of &%Month and a subclass of &%Year to the class containing the &%Months corresponding to thos &%Years. For example (&%MonthFn &%January (&%YearFn 1912)) is the class containing the eighth &%Month, i.e. August, of the &%Year 1912. For another example, (&%MonthFn &%August &%Year) is equal to &%August, the class of all months of August. Note that this function returns a &%Class as a value. The reason for this is that the related functions, viz. DayFn, HourFn, MinuteFn, and SecondFn, are used to generate both specific &%TimeIntervals and recurrent intervals, and the only way to do this is to make the domains and ranges of these functions classes rather than individuals.")
%KIF documentation:(documentation instrument EnglishLanguage "(instrument ?EVENT ?TOOL) means that ?TOOL is used by an agent in bringing about ?EVENT and that ?TOOL is not changed by ?EVENT. For example, the key is an &%instrument in the following proposition: The key opened the door. Note that &%instrument and &%resource cannot be satisfied by the same ordered pair.")
thf(ax_004,axiom,(
    ! [X: $i] :
      ( holdsDuring_THFTYPE_IiooI @ X @ $true ) )).

thf(ax_005,axiom,(
    ! [THING: $i] :
      ( instance_THFTYPE_IiioI @ THING @ lEntity_THFTYPE_i ) )).

thf(ax_006,axiom,(
    ! [NUMBER: $i,MONTH: $i] :
      ( ( ( instance_THFTYPE_IiioI @ MONTH @ lMonth_THFTYPE_i )
        & ( duration_THFTYPE_IiioI @ MONTH @ ( lMeasureFn_THFTYPE_IiiiI @ NUMBER @ lDayDuration_THFTYPE_i ) ) )
     => ( ( lCardinalityFn_THFTYPE_IiiI @ ( lTemporalCompositionFn_THFTYPE_IiiiI @ MONTH @ lDay_THFTYPE_i ) )
        = NUMBER ) ) )).

thf(ax_007,axiom,(
    ! [OBJ1: $i,OBJ2: $i] :
      ( ( located_THFTYPE_IiioI @ OBJ1 @ OBJ2 )
     => ! [SUB: $i] :
          ( ( part_THFTYPE_IiioI @ SUB @ OBJ1 )
         => ( located_THFTYPE_IiioI @ SUB @ OBJ2 ) ) ) )).

%KIF documentation:(documentation disjoint EnglishLanguage "&%Classes are &%disjoint only if they share no instances, i.e. just in case the result of applying &%IntersectionFn to them is empty.")
%KIF documentation:(documentation MultiplicationFn EnglishLanguage "If ?NUMBER1 and ?NUMBER2 are &%Numbers, then (&%MultiplicationFn ?NUMBER1 ?NUMBER2) is the arithmetical product of these numbers.")
%KIF documentation:(documentation domain EnglishLanguage "Provides a computationally and heuristically convenient mechanism for declaring the argument types of a given relation. The formula (&%domain ?REL ?INT ?CLASS) means that the ?INT'th element of each tuple in the relation ?REL must be an instance of ?CLASS. Specifying argument types is very helpful in maintaining ontologies. Representation systems can use these specifications to classify terms and check integrity constraints. If the restriction on the argument type of a &%Relation is not captured by a &%SetOrClass already defined in the ontology, one can specify a &%SetOrClass compositionally with the functions &%UnionFn, &%IntersectionFn, etc.")
%KIF documentation:(documentation rangeSubclass EnglishLanguage "(&%rangeSubclass ?FUNCTION ?CLASS) means that all of the values assigned by ?FUNCTION are &%subclasses of ?CLASS.")
%KIF documentation:(documentation TimeInterval EnglishLanguage "An interval of time. Note that a &%TimeInterval has both an extent and a location on the universal timeline. Note too that a &%TimeInterval has no gaps, i.e. this class contains only convex time intervals.")
%KIF documentation:(documentation TernaryPredicate EnglishLanguage "The &%Class of &%Predicates that require exactly three arguments.")
thf(ax_008,axiom,
    ( subclass_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_009,axiom,
    ( subclass_THFTYPE_IiioI @ lTotalValuedRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation Relation EnglishLanguage "The &%Class of relations. There are three kinds of &%Relation: &%Predicate, &%Function, and &%List. &%Predicates and &%Functions both denote sets of ordered n-tuples. The difference between these two &%Classes is that &%Predicates cover formula-forming operators, while &%Functions cover term-forming operators. A &%List, on the other hand, is a particular ordered n-tuple.")
thf(ax_010,axiom,(
    ! [NUMBER: $i,CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( domainSubclass_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS1 )
        & ( domainSubclass_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_011,axiom,(
    ! [DAY: $i] :
      ( ( instance_THFTYPE_IiioI @ DAY @ lDay_THFTYPE_i )
     => ( duration_THFTYPE_IiioI @ DAY @ ( lMeasureFn_THFTYPE_IiiiI @ n1_THFTYPE_i @ lDayDuration_THFTYPE_i ) ) ) )).

thf(ax_012,axiom,(
    ! [REL2: $i,NUMBER: $i,CLASS1: $i,CLASS2: $i,REL1: $i] :
      ( ( ( domainSubclass_THFTYPE_IiiioI @ REL1 @ NUMBER @ CLASS1 )
        & ( domainSubclass_THFTYPE_IiiioI @ REL2 @ NUMBER @ CLASS2 )
        & ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 ) )
     => ( disjointRelation_THFTYPE_IiioI @ REL1 @ REL2 ) ) )).

%KIF documentation:(documentation documentation EnglishLanguage "A relation between objects in the domain of discourse and strings of natural language text stated in a particular &%HumanLanguage. The domain of &%documentation is not constants (names), but the objects themselves. This means that one does not quote the names when associating them with their documentation.")
thf(ax_013,axiom,
    ( subclass_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_014,axiom,
    ( subclass_THFTYPE_IiioI @ lYear_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_015,axiom,(
    ! [NUMBER: $i,CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( domain_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS1 )
        & ( domain_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_016,axiom,(
    ! [REL: $i > $i > $o] :
      ( ( instance_THFTYPE_IIiioIioI @ REL @ lTransitiveRelation_THFTYPE_i )
    <=> ! [INST1: $i,INST2: $i,INST3: $i] :
          ( ( ( REL @ INST1 @ INST2 )
            & ( REL @ INST2 @ INST3 ) )
         => ( REL @ INST1 @ INST3 ) ) ) )).

%KIF documentation:(documentation TemporalCompositionFn EnglishLanguage "The basic &%Function for expressing the composition of larger &%TimeIntervals out of smaller &%TimeIntervals. For example, if &%ThisSeptember is an &%instance of &%September, (&%TemporalCompositionFn &%ThisSeptember &%Day) denotes the &%Class of consecutive days that make up &%ThisSeptember. Note that one can obtain the number of instances of this &%Class by using the function &%CardinalityFn.")
thf(ax_017,axiom,
    ( rangeSubclass_THFTYPE_IiioI @ lTemporalCompositionFn_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

%KIF documentation:(documentation RelationExtendedToQuantities EnglishLanguage "A &%RelationExtendedToQuantities is a &%Relation that, when it is true on a sequence of arguments that are &%RealNumbers, it is also true on a sequence of instances of &%ConstantQuantity with those magnitudes in some unit of measure. For example, the &%lessThan relation is extended to quantities. This means that for all pairs of quantities ?QUANTITY1 and ?QUANTITY2, (&%lessThan ?QUANTITY1 ?QUANTITY2) if and only if, for some ?NUMBER1, ?NUMBER2, and ?UNIT, ?QUANTITY1 = (&%MeasureFn ?NUMBER1 ?UNIT), ?QUANTITY2 = (&%MeasureFn ?NUMBER2 ?UNIT), and (&%lessThan ?NUMBER1 ?NUMBER2), for all units ?UNIT on which ?QUANTITY1 and ?QUANTITY2 can be measured. Note that, when a &%RelationExtendedToQuantities is extended from &%RealNumbers to instances of &%ConstantQuantity, the &%ConstantQuantity must be measured along the same physical dimension.")
%KIF documentation:(documentation equal EnglishLanguage "(equal ?ENTITY1 ?ENTITY2) is true just in case ?ENTITY1 is identical with ?ENTITY2.")
%KIF documentation:(documentation CardinalityFn EnglishLanguage "(CardinalityFn ?CLASS) returns the number of instances in the &%SetOrClass ?CLASS or the number of members in the ?CLASS &%Collection.")
thf(ax_018,axiom,(
    ! [REL2: $i,CLASS1: $i,CLASS2: $i,REL1: $i] :
      ( ( ( range_THFTYPE_IiioI @ REL1 @ CLASS1 )
        & ( range_THFTYPE_IiioI @ REL2 @ CLASS2 )
        & ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 ) )
     => ( disjointRelation_THFTYPE_IiioI @ REL1 @ REL2 ) ) )).

thf(ax_019,axiom,
    ( subclass_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_020,axiom,
    ( subclass_THFTYPE_IiioI @ lMonth_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_021,axiom,(
    ! [TIME: $i,SITUATION: $o] :
      ( ( holdsDuring_THFTYPE_IiooI @ TIME @ ( ~ @ SITUATION ) )
     => ( ~ @ ( holdsDuring_THFTYPE_IiooI @ TIME @ SITUATION ) ) ) )).

%KIF documentation:(documentation holdsDuring EnglishLanguage "(&%holdsDuring ?TIME ?FORMULA) means that the proposition denoted by ?FORMULA is true in the time frame ?TIME. Note that this implies that ?FORMULA is true at every &%TimePoint which is a &%temporalPart of ?TIME.")
thf(ax_022,axiom,
    ( likes_THFTYPE_IiioI @ lMary_THFTYPE_i @ lBill_THFTYPE_i )).

thf(ax_023,axiom,
    ( likes_THFTYPE_IiioI @ lMary_THFTYPE_i @ lBill_THFTYPE_i )).

%KIF documentation:(documentation Integer EnglishLanguage "A negative or nonnegative whole number.")
%KIF documentation:(documentation attribute EnglishLanguage "(&%attribute ?OBJECT ?PROPERTY) means that ?PROPERTY is a &%Attribute of ?OBJECT. For example, (&%attribute &%MyLittleRedWagon &%Red).")
thf(ax_024,axiom,
    ( range_THFTYPE_IiioI @ lWhenFn_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

%KIF documentation:(documentation greaterThan EnglishLanguage "(&%greaterThan ?NUMBER1 ?NUMBER2) is true just in case the &%Quantity ?NUMBER1 is greater than the &%Quantity ?NUMBER2.")
thf(ax_025,axiom,(
    ! [INTERVAL1: $i,INTERVAL2: $i] :
      ( ( meetsTemporally_THFTYPE_IiioI @ INTERVAL1 @ INTERVAL2 )
    <=> ( ( lEndFn_THFTYPE_IiiI @ INTERVAL1 )
        = ( lBeginFn_THFTYPE_IiiI @ INTERVAL2 ) ) ) )).

thf(ax_026,axiom,(
    ! [SITUATION: $o,TIME2: $i,TIME1: $i] :
      ( ( ( holdsDuring_THFTYPE_IiooI @ TIME1 @ SITUATION )
        & ( temporalPart_THFTYPE_IiioI @ TIME2 @ TIME1 ) )
     => ( holdsDuring_THFTYPE_IiooI @ TIME2 @ SITUATION ) ) )).

thf(ax_027,axiom,
    ( range_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_028,axiom,(
    ? [THING: $i] :
      ( instance_THFTYPE_IiioI @ THING @ lEntity_THFTYPE_i ) )).

thf(ax_029,axiom,(
    ! [REL: $i > $i > $o] :
      ( ( instance_THFTYPE_IIiioIioI @ REL @ lIrreflexiveRelation_THFTYPE_i )
    <=> ! [INST: $i] :
          ( ~ @ ( REL @ INST @ INST ) ) ) )).

thf(ax_030,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryFunction_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_031,axiom,(
    ! [NUMBER: $i,PRED1: $i,CLASS1: $i,PRED2: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ PRED1 @ PRED2 )
        & ( domain_THFTYPE_IiiioI @ PRED2 @ NUMBER @ CLASS1 ) )
     => ( domain_THFTYPE_IiiioI @ PRED1 @ NUMBER @ CLASS1 ) ) )).

%KIF documentation:(documentation domainSubclass EnglishLanguage "&%Predicate used to specify argument type restrictions of &%Predicates. The formula (&%domainSubclass ?REL ?INT ?CLASS) means that the ?INT'th element of each tuple in the relation ?REL must be a subclass of ?CLASS.")
thf(ax_032,axiom,
    ( subclass_THFTYPE_IiioI @ lTotalValuedRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

%KIF documentation:(documentation located EnglishLanguage "(&%located ?PHYS ?OBJ) means that ?PHYS is &%partlyLocated at ?OBJ, and there is no &%part or &%subProcess of ?PHYS that is not &%located at ?OBJ.")
thf(ax_033,axiom,
    ( subclass_THFTYPE_IiioI @ lTernaryPredicate_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_034,axiom,
    ( subclass_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_035,axiom,(
    ! [YEAR: $i] :
      ( ( instance_THFTYPE_IiioI @ YEAR @ lYear_THFTYPE_i )
     => ( ( lCardinalityFn_THFTYPE_IiiI @ ( lTemporalCompositionFn_THFTYPE_IiiiI @ YEAR @ lMonth_THFTYPE_i ) )
        = n12_THFTYPE_i ) ) )).

%KIF documentation:(documentation SubtractionFn EnglishLanguage "If ?NUMBER1 and ?NUMBER2 are &%Numbers, then (&%SubtractionFn ?NUMBER1 ?NUMBER2) is the arithmetical difference between ?NUMBER1 and ?NUMBER2, i.e. ?NUMBER1 minus ?NUMBER2. An exception occurs when ?NUMBER1 is equal to 0, in which case (&%SubtractionFn ?NUMBER1 ?NUMBER2) is the negation of ?NUMBER2.")
%KIF documentation:(documentation IrreflexiveRelation EnglishLanguage "&%Relation ?REL is irreflexive iff (?REL ?INST ?INST) holds for no value of ?INST.")
%KIF documentation:(documentation Day EnglishLanguage "The &%Class of all calendar &%Days.")
thf(ax_036,axiom,(
    ! [CLASS1: $i,CLASS2: $i] :
      ( ( CLASS1 = CLASS2 )
     => ! [THING: $i] :
          ( ( instance_THFTYPE_IiioI @ THING @ CLASS1 )
        <=> ( instance_THFTYPE_IiioI @ THING @ CLASS2 ) ) ) )).

%KIF documentation:(documentation TotalValuedRelation EnglishLanguage "A &%Relation is a &%TotalValuedRelation just in case there exists an assignment for the last argument position of the &%Relation given any assignment of values to every argument position except the last one. Note that declaring a &%Relation to be both a &%TotalValuedRelation and a &%SingleValuedRelation means that it is a total function.")
%KIF documentation:(documentation duration EnglishLanguage "(&%duration ?POS ?TIME) means that the duration of the &%TimePosition ?POS is ?TIME. Note that this &%Predicate can be used in conjunction with the &%Function &%WhenFn to specify the duration of any instance of &%Physical.")
thf(ax_037,axiom,(
    ! [REL2: $i,CLASS1: $i,REL1: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ REL1 @ REL2 )
        & ( rangeSubclass_THFTYPE_IiioI @ REL2 @ CLASS1 ) )
     => ( rangeSubclass_THFTYPE_IiioI @ REL1 @ CLASS1 ) ) )).

%KIF documentation:(documentation Quantity EnglishLanguage "Any specification of how many or how much of something there is. Accordingly, there are two subclasses of &%Quantity: &%Number (how many) and &%PhysicalQuantity (how much).")
thf(ax_038,axiom,(
    ! [YEAR2: $i,YEAR1: $i] :
      ( ( ( instance_THFTYPE_IiioI @ YEAR1 @ lYear_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ YEAR2 @ lYear_THFTYPE_i )
        & ( ( minus_THFTYPE_IiiiI @ YEAR2 @ YEAR1 )
          = n1_THFTYPE_i ) )
     => ( meetsTemporally_THFTYPE_IiioI @ YEAR1 @ YEAR2 ) ) )).

thf(ax_039,axiom,(
    ! [REL2: $i > $o,ROW: $i,REL1: $i > $o] :
      ( ( ( subrelation_THFTYPE_IIioIIioIoI @ REL1 @ REL2 )
        & ( REL1 @ ROW ) )
     => ( REL2 @ ROW ) ) )).

%KIF documentation:(documentation YearFn EnglishLanguage "A &%UnaryFunction that maps a number to the corresponding calendar &%Year. For example, (&%YearFn 1912) returns the &%Class containing just one instance, the year of 1912. As might be expected, positive integers return years in the Common Era, while negative integers return years in B.C.E. Note that this function returns a &%Class as a value. The reason for this is that the related functions, viz. &%MonthFn, &%DayFn, &%HourFn, &%MinuteFn, and &%SecondFn, are used to generate both specific &%TimeIntervals and recurrent intervals, and the only way to do this is to make the domains and ranges of these functions classes rather than individuals.")
%KIF documentation:(documentation subProcess EnglishLanguage "(&%subProcess ?SUBPROC ?PROC) means that ?SUBPROC is a subprocess of ?PROC. A subprocess is here understood as a temporally distinguished part (proper or not) of a &%Process.")
thf(ax_040,axiom,(
    ! [THING2: $i,THING1: $i] :
      ( ( THING1 = THING2 )
     => ! [CLASS: $i] :
          ( ( instance_THFTYPE_IiioI @ THING1 @ CLASS )
        <=> ( instance_THFTYPE_IiioI @ THING2 @ CLASS ) ) ) )).

thf(ax_041,axiom,(
    ! [CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( range_THFTYPE_IiioI @ REL @ CLASS1 )
        & ( range_THFTYPE_IiioI @ REL @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_042,axiom,(
    ! [CLASS1: $i,CLASS2: $i] :
      ( ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
    <=> ! [INST: $i] :
          ( ~
          @ ( ( instance_THFTYPE_IiioI @ INST @ CLASS1 )
            & ( instance_THFTYPE_IiioI @ INST @ CLASS2 ) ) ) ) )).

%KIF documentation:(documentation orientation EnglishLanguage "A general &%Predicate for indicating how two &%Objects are oriented with respect to one another. For example, (orientation ?OBJ1 ?OBJ2 North) means that ?OBJ1 is north of ?OBJ2, and (orientation ?OBJ1 ?OBJ2 Vertical) means that ?OBJ1 is positioned vertically with respect to ?OBJ2.")
%KIF documentation:(documentation UnaryFunction EnglishLanguage "The &%Class of &%Functions that require a single argument.")
thf(ax_043,axiom,(
    ! [SUBPROC: $i,PROC: $i] :
      ( ( subProcess_THFTYPE_IiioI @ SUBPROC @ PROC )
     => ( temporalPart_THFTYPE_IiioI @ ( lWhenFn_THFTYPE_IiiI @ SUBPROC ) @ ( lWhenFn_THFTYPE_IiiI @ PROC ) ) ) )).

%KIF documentation:(documentation AdditionFn EnglishLanguage "If ?NUMBER1 and ?NUMBER2 are &%Numbers, then (&%AdditionFn ?NUMBER1 ?NUMBER2) is the arithmetical sum of these numbers.")
%KIF documentation:(documentation relatedInternalConcept EnglishLanguage "Means that the two arguments are related concepts within the SUMO, i.e. there is a significant similarity of meaning between them. To indicate a meaning relation between a SUMO concept and a concept from another source, use the Predicate &%relatedExternalConcept.")
thf(ax_044,axiom,
    ( rangeSubclass_THFTYPE_IiioI @ lMonthFn_THFTYPE_i @ lMonth_THFTYPE_i )).

thf(ax_045,axiom,(
    ! [INTERVAL1: $i,INTERVAL2: $i] :
      ( ( ( ( lBeginFn_THFTYPE_IiiI @ INTERVAL1 )
          = ( lBeginFn_THFTYPE_IiiI @ INTERVAL2 ) )
        & ( ( lEndFn_THFTYPE_IiiI @ INTERVAL1 )
          = ( lEndFn_THFTYPE_IiiI @ INTERVAL2 ) ) )
     => ( INTERVAL1 = INTERVAL2 ) ) )).

%KIF documentation:(documentation BinaryPredicate EnglishLanguage "A &%Predicate relating two items - its valence is two.")
%KIF documentation:(documentation SetOrClass EnglishLanguage "The &%SetOrClass of &%Sets and &%Classes, i.e. any instance of &%Abstract that has &%elements or &%instances.")
thf(ax_046,axiom,(
    ! [REL2: $i,CLASS1: $i,REL1: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ REL1 @ REL2 )
        & ( range_THFTYPE_IiioI @ REL2 @ CLASS1 ) )
     => ( range_THFTYPE_IiioI @ REL1 @ CLASS1 ) ) )).

%KIF documentation:(documentation MeasureFn EnglishLanguage "This &%BinaryFunction maps a &%RealNumber and a &%UnitOfMeasure to that &%Number of units. It is used to express `measured' instances of &%PhysicalQuantity. Example: the concept of three meters is represented as (&%MeasureFn 3 &%Meter).")
%KIF documentation:(documentation subclass EnglishLanguage "(&%subclass ?CLASS1 ?CLASS2) means that ?CLASS1 is a subclass of ?CLASS2, i.e. every instance of ?CLASS1 is also an instance of ?CLASS2. A class may have multiple superclasses and subclasses.")
%KIF documentation:(documentation part EnglishLanguage "The basic mereological relation. All other mereological relations are defined in terms of this one. (&%part ?PART ?WHOLE) simply means that the &%Object ?PART is part of the &%Object ?WHOLE. Note that, since &%part is a &%ReflexiveRelation, every &%Object is a part of itself.")
%KIF documentation:(documentation BinaryFunction EnglishLanguage "The &%Class of &%Functions that require two arguments.")
%KIF documentation:(documentation Entity EnglishLanguage "The universal class of individuals. This is the root node of the ontology.")
%KIF documentation:(documentation WhenFn EnglishLanguage "A &%UnaryFunction that maps an &%Object or &%Process to the exact &%TimeInterval during which it exists. Note that, for every &%TimePoint ?TIME outside of the &%TimeInterval (WhenFn ?THING), (time ?THING ?TIME) does not hold.")
thf(ax_047,axiom,
    ( subclass_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_048,axiom,(
    ! [REL2: $i,NUMBER: $i,CLASS1: $i,REL1: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ REL1 @ REL2 )
        & ( domainSubclass_THFTYPE_IiiioI @ REL2 @ NUMBER @ CLASS1 ) )
     => ( domainSubclass_THFTYPE_IiiioI @ REL1 @ NUMBER @ CLASS1 ) ) )).

%KIF documentation:(documentation agent EnglishLanguage "(&%agent ?PROCESS ?AGENT) means that ?AGENT is an active determinant, either animate or inanimate, of the &%Process ?PROCESS, with or without voluntary intention. For example, Eve is an &%agent in the following proposition: Eve bit an apple.")
thf(ax_049,axiom,
    ( likes_THFTYPE_IiioI @ lSue_THFTYPE_i @ lBill_THFTYPE_i )).

thf(ax_050,axiom,
    ( likes_THFTYPE_IiioI @ lSue_THFTYPE_i @ lBill_THFTYPE_i )).

thf(ax_051,axiom,(
    ! [SUBPROC: $i,PROC: $i] :
      ( ( subProcess_THFTYPE_IiioI @ SUBPROC @ PROC )
     => ! [REGION: $i] :
          ( ( located_THFTYPE_IiioI @ PROC @ REGION )
         => ( located_THFTYPE_IiioI @ SUBPROC @ REGION ) ) ) )).

%KIF documentation:(documentation BeginFn EnglishLanguage "A &%UnaryFunction that maps a &%TimeInterval to the &%TimePoint at which the interval begins.")
thf(ax_052,axiom,
    ( range_THFTYPE_IiioI @ lSubtractionFn_THFTYPE_i @ lQuantity_THFTYPE_i )).

%KIF documentation:(documentation Month EnglishLanguage "The &%Class of all calendar &%Months.")
%KIF documentation:(documentation EnglishLanguage EnglishLanguage "A Germanic language that incorporates many roots from the Romance languages. It is the official language of the &%UnitedStates, the &%UnitedKingdom, and many other countries.")
%KIF documentation:(documentation TransitiveRelation EnglishLanguage "A &%BinaryRelation ?REL is transitive if (?REL ?INST1 ?INST2) and (?REL ?INST2 ?INST3) imply (?REL ?INST1 ?INST3), for all ?INST1, ?INST2, and ?INST3.")
%KIF documentation:(documentation patient EnglishLanguage "(&%patient ?PROCESS ?ENTITY) means that ?ENTITY is a participant in ?PROCESS that may be moved, said, experienced, etc. For example, the direct objects in the sentences 'The cat swallowed the canary' and 'Billy likes the beer' would be examples of &%patients. Note that the &%patient of a &%Process may or may not undergo structural change as a result of the &%Process. The &%CaseRole of &%patient is used when one wants to specify as broadly as possible the object of a &%Process.")
%KIF documentation:(documentation Year EnglishLanguage "The &%Class of all calendar &%Years.")
%KIF documentation:(documentation result EnglishLanguage "(result ?ACTION ?OUTPUT) means that ?OUTPUT is a product of ?ACTION. For example, house is a &%result in the following proposition: Eric built a house.")
thf(ax_053,axiom,
    ( range_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lQuantity_THFTYPE_i )).

%KIF documentation:(documentation temporalPart EnglishLanguage "The temporal analogue of the spatial &%part predicate. (&%temporalPart ?POS1 ?POS2) means that &%TimePosition ?POS1 is part of &%TimePosition ?POS2. Note that since &%temporalPart is a &%ReflexiveRelation every &%TimePostion is a &%temporalPart of itself.")
%KIF documentation:(documentation subrelation EnglishLanguage "(&%subrelation ?REL1 ?REL2) means that every tuple of ?REL1 is also a tuple of ?REL2. In other words, if the &%Relation ?REL1 holds for some arguments arg_1, arg_2, ... arg_n, then the &%Relation ?REL2 holds for the same arguments. A consequence of this is that a &%Relation and its subrelations must have the same &%valence.")
%KIF documentation:(documentation EndFn EnglishLanguage "A &%UnaryFunction that maps a &%TimeInterval to the &%TimePoint at which the interval ends.")
%KIF documentation:(documentation meetsTemporally EnglishLanguage "(&%meetsTemporally ?INTERVAL1 ?INTERVAL2) means that the terminal point of the &%TimeInterval ?INTERVAL1 is the initial point of the &%TimeInterval ?INTERVAL2.")
thf(ax_054,axiom,(
    ! [OBJ: $i,PROCESS: $i] :
      ( ( located_THFTYPE_IiioI @ PROCESS @ OBJ )
     => ! [SUB: $i] :
          ( ( subProcess_THFTYPE_IiioI @ SUB @ PROCESS )
         => ( located_THFTYPE_IiioI @ SUB @ OBJ ) ) ) )).

thf(ax_055,axiom,
    ( subclass_THFTYPE_IiioI @ lUnaryFunction_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_056,axiom,
    ( subclass_THFTYPE_IiioI @ lDay_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

%KIF documentation:(documentation DayDuration EnglishLanguage "Time unit. 1 day = 24 hours.")
%KIF documentation:(documentation InheritableRelation EnglishLanguage "The class of &%Relations whose properties can be inherited downward in the class hierarchy via the &%subrelation &%Predicate.")
thf(ax_057,axiom,(
    ! [REL2: $i,NUMBER: $i,CLASS1: $i,CLASS2: $i,REL1: $i] :
      ( ( ( domain_THFTYPE_IiiioI @ REL1 @ NUMBER @ CLASS1 )
        & ( domain_THFTYPE_IiiioI @ REL2 @ NUMBER @ CLASS2 )
        & ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 ) )
     => ( disjointRelation_THFTYPE_IiioI @ REL1 @ REL2 ) ) )).

thf(ax_058,axiom,
    ( rangeSubclass_THFTYPE_IiioI @ lYearFn_THFTYPE_i @ lYear_THFTYPE_i )).

thf(ax_059,axiom,(
    ! [CLASS: $i,PRED1: $i,PRED2: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ PRED1 @ PRED2 )
        & ( instance_THFTYPE_IiioI @ PRED2 @ CLASS )
        & ( subclass_THFTYPE_IiioI @ CLASS @ lInheritableRelation_THFTYPE_i ) )
     => ( instance_THFTYPE_IiioI @ PRED1 @ CLASS ) ) )).

%KIF documentation:(documentation Object EnglishLanguage "Corresponds roughly to the class of ordinary objects. Examples include normal physical objects, geographical regions, and locations of &%Processes, the complement of &%Objects in the &%Physical class. In a 4D ontology, an &%Object is something whose spatiotemporal extent is thought of as dividing into spatial parts roughly parallel to the time-axis.")
thf(ax_060,axiom,(
    ! [CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( rangeSubclass_THFTYPE_IiioI @ REL @ CLASS1 )
        & ( rangeSubclass_THFTYPE_IiioI @ REL @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_061,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_062,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i )).

thf(ax_063,axiom,
    ( instance_THFTYPE_IIiioIioI @ duration_THFTYPE_IiioI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_064,axiom,
    ( instance_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_065,axiom,
    ( instance_THFTYPE_IIiioIioI @ range_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_066,axiom,
    ( instance_THFTYPE_IiioI @ lMonthFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_067,axiom,
    ( domain_THFTYPE_IIiioIiioI @ disjointRelation_THFTYPE_IiioI @ n2_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_068,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_069,axiom,
    ( domainSubclass_THFTYPE_IiiioI @ lMonthFn_THFTYPE_i @ n2_THFTYPE_i @ lYear_THFTYPE_i )).

thf(ax_070,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subProcess_THFTYPE_IiioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_071,axiom,
    ( relatedInternalConcept_THFTYPE_IiioI @ lMonth_THFTYPE_i @ lMonthFn_THFTYPE_i )).

thf(ax_072,axiom,
    ( instance_THFTYPE_IiioI @ lessThan_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_073,axiom,
    ( domain_THFTYPE_IIiioIiioI @ meetsTemporally_THFTYPE_IiioI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_074,axiom,
    ( domain_THFTYPE_IiiioI @ equal_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_075,axiom,
    ( domain_THFTYPE_IIiioIiioI @ disjoint_THFTYPE_IiioI @ n2_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_076,axiom,
    ( instance_THFTYPE_IiioI @ lSubtractionFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_077,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subProcess_THFTYPE_IiioI @ n2_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_078,axiom,
    ( domain_THFTYPE_IiiioI @ agent_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_079,axiom,
    ( instance_THFTYPE_IIiioIioI @ relatedInternalConcept_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_080,axiom,
    ( instance_THFTYPE_IiioI @ equal_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_081,axiom,
    ( instance_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_082,axiom,
    ( instance_THFTYPE_IiioI @ lessThan_THFTYPE_i @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_083,axiom,
    ( domain_THFTYPE_IIiioIiioI @ disjointRelation_THFTYPE_IiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_084,axiom,
    ( domain_THFTYPE_IiiioI @ greaterThan_THFTYPE_i @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_085,axiom,
    ( domain_THFTYPE_IIiioIiioI @ range_THFTYPE_IiioI @ n2_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_086,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subrelation_THFTYPE_IiioI @ n2_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_087,axiom,
    ( instance_THFTYPE_IIiiIioI @ lYearFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_088,axiom,
    ( instance_THFTYPE_IiioI @ attribute_THFTYPE_i @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_089,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_090,axiom,
    ( subrelation_THFTYPE_IiioI @ result_THFTYPE_i @ patient_THFTYPE_i )).

thf(ax_091,axiom,
    ( domain_THFTYPE_IiiioI @ lMultiplicationFn_THFTYPE_i @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_092,axiom,
    ( domain_THFTYPE_IiiioI @ patient_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_093,axiom,
    ( instance_THFTYPE_IIiiIioI @ lCardinalityFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_094,axiom,
    ( instance_THFTYPE_IiioI @ greaterThan_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_095,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_096,axiom,
    ( domain_THFTYPE_IiiioI @ patient_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_097,axiom,
    ( instance_THFTYPE_IiioI @ documentation_THFTYPE_i @ lTernaryPredicate_THFTYPE_i )).

thf(ax_098,axiom,
    ( instance_THFTYPE_IIiioIioI @ duration_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_099,axiom,
    ( instance_THFTYPE_IiioI @ orientation_THFTYPE_i @ lTernaryPredicate_THFTYPE_i )).

thf(ax_100,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ domain_THFTYPE_IiiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_101,axiom,
    ( instance_THFTYPE_IiioI @ greaterThan_THFTYPE_i @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_102,axiom,
    ( domain_THFTYPE_IiiioI @ result_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_103,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_104,axiom,
    ( instance_THFTYPE_IiioI @ lTemporalCompositionFn_THFTYPE_i @ lTemporalRelation_THFTYPE_i )).

thf(ax_105,axiom,
    ( domain_THFTYPE_IIiioIiioI @ relatedInternalConcept_THFTYPE_IiioI @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_106,axiom,
    ( instance_THFTYPE_IIiioIioI @ subrelation_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_107,axiom,
    ( relatedInternalConcept_THFTYPE_IIiioIIiioIoI @ disjointRelation_THFTYPE_IiioI @ disjoint_THFTYPE_IiioI )).

thf(ax_108,axiom,
    ( domain_THFTYPE_IiiioI @ greaterThan_THFTYPE_i @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_109,axiom,
    ( domain_THFTYPE_IIiioIiioI @ part_THFTYPE_IiioI @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_110,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_111,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lBeginFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_112,axiom,
    ( instance_THFTYPE_IiioI @ lSubtractionFn_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_113,axiom,
    ( instance_THFTYPE_IIiioIioI @ instance_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_114,axiom,
    ( instance_THFTYPE_IiioI @ attribute_THFTYPE_i @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_115,axiom,
    ( domain_THFTYPE_IiiioI @ lessThan_THFTYPE_i @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_116,axiom,
    ( domain_THFTYPE_IIiioIiioI @ instance_THFTYPE_IiioI @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_117,axiom,
    ( instance_THFTYPE_IIiooIioI @ holdsDuring_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_118,axiom,
    ( domain_THFTYPE_IIiioIiioI @ part_THFTYPE_IiioI @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_119,axiom,
    ( instance_THFTYPE_IIiioIioI @ temporalPart_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i )).

thf(ax_120,axiom,
    ( domain_THFTYPE_IiiioI @ instrument_THFTYPE_i @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_121,axiom,
    ( domain_THFTYPE_IiiioI @ lAdditionFn_THFTYPE_i @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_122,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lYearFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lInteger_THFTYPE_i )).

thf(ax_123,axiom,
    ( instance_THFTYPE_IIiioIioI @ range_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_124,axiom,
    ( instance_THFTYPE_IIIiioIiioIioI @ domain_THFTYPE_IIiioIiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_125,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ domain_THFTYPE_IiiioI @ n3_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_126,axiom,
    ( instance_THFTYPE_IIiiIioI @ lCardinalityFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_127,axiom,
    ( domain_THFTYPE_IiiioI @ documentation_THFTYPE_i @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_128,axiom,
    ( instance_THFTYPE_IIiioIioI @ rangeSubclass_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_129,axiom,
    ( relatedInternalConcept_THFTYPE_IiIiiIoI @ lYear_THFTYPE_i @ lYearFn_THFTYPE_IiiI )).

thf(ax_130,axiom,
    ( domain_THFTYPE_IiiioI @ equal_THFTYPE_i @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_131,axiom,
    ( instance_THFTYPE_IIiiIioI @ lCardinalityFn_THFTYPE_IiiI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_132,axiom,
    ( instance_THFTYPE_IIiioIioI @ disjoint_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_133,axiom,
    ( instance_THFTYPE_IIiioIioI @ temporalPart_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_134,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_135,axiom,
    ( instance_THFTYPE_IIiioIioI @ subProcess_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_136,axiom,
    ( instance_THFTYPE_IiioI @ greaterThan_THFTYPE_i @ lTransitiveRelation_THFTYPE_i )).

thf(ax_137,axiom,
    ( instance_THFTYPE_IIiioIioI @ disjointRelation_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_138,axiom,
    ( instance_THFTYPE_IiioI @ lMonthFn_THFTYPE_i @ lTemporalRelation_THFTYPE_i )).

thf(ax_139,axiom,
    ( instance_THFTYPE_IiioI @ greaterThan_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_140,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_141,axiom,
    ( instance_THFTYPE_IIiiiIioI @ lMeasureFn_THFTYPE_IiiiI @ lBinaryFunction_THFTYPE_i )).

thf(ax_142,axiom,
    ( domain_THFTYPE_IiiioI @ lMultiplicationFn_THFTYPE_i @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_143,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_144,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_145,axiom,
    ( domainSubclass_THFTYPE_IiiioI @ lMonthFn_THFTYPE_i @ n1_THFTYPE_i @ lMonth_THFTYPE_i )).

thf(ax_146,axiom,
    ( instance_THFTYPE_IIiioIioI @ subclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_147,axiom,
    ( domain_THFTYPE_IiiioI @ result_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_148,axiom,
    ( instance_THFTYPE_IIiiIioI @ lEndFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_149,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subclass_THFTYPE_IiioI @ n2_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_150,axiom,
    ( instance_THFTYPE_IIiiIioI @ lEndFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_151,axiom,
    ( domain_THFTYPE_IiiioI @ lTemporalCompositionFn_THFTYPE_i @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_152,axiom,
    ( domain_THFTYPE_IIIiioIIiioIoIiioI @ relatedInternalConcept_THFTYPE_IIiioIIiioIoI @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_153,axiom,
    ( instance_THFTYPE_IiioI @ lTemporalCompositionFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_154,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subclass_THFTYPE_IiioI @ n1_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_155,axiom,
    ( domain_THFTYPE_IiiioI @ lSubtractionFn_THFTYPE_i @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_156,axiom,
    ( instance_THFTYPE_IiioI @ equal_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_157,axiom,
    ( domainSubclass_THFTYPE_IiiioI @ lTemporalCompositionFn_THFTYPE_i @ n2_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_158,axiom,
    ( instance_THFTYPE_IIiiIioI @ lYearFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_159,axiom,
    ( domain_THFTYPE_IiiioI @ orientation_THFTYPE_i @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_160,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ domainSubclass_THFTYPE_IiiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_161,axiom,
    ( relatedInternalConcept_THFTYPE_IiioI @ lDay_THFTYPE_i @ lDayDuration_THFTYPE_i )).

thf(ax_162,axiom,
    ( disjointRelation_THFTYPE_IiioI @ result_THFTYPE_i @ instrument_THFTYPE_i )).

thf(ax_163,axiom,
    ( instance_THFTYPE_IIiiiIioI @ lMeasureFn_THFTYPE_IiiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_164,axiom,
    ( domain_THFTYPE_IiiioI @ instrument_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_165,axiom,
    ( instance_THFTYPE_IIiioIioI @ duration_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_166,axiom,
    ( instance_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_167,axiom,
    ( instance_THFTYPE_IiioI @ lSubtractionFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_168,axiom,
    ( domain_THFTYPE_IIiioIiioI @ instance_THFTYPE_IiioI @ n2_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_169,axiom,
    ( domain_THFTYPE_IiiioI @ lSubtractionFn_THFTYPE_i @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_170,axiom,
    ( domain_THFTYPE_IIiioIiioI @ duration_THFTYPE_IiioI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_171,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subrelation_THFTYPE_IiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_172,axiom,
    ( instance_THFTYPE_IIiiIioI @ lEndFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_173,axiom,
    ( instance_THFTYPE_IIiiioIioI @ domainSubclass_THFTYPE_IiiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_174,axiom,
    ( domain_THFTYPE_IIiioIiioI @ meetsTemporally_THFTYPE_IiioI @ n2_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_175,axiom,
    ( domain_THFTYPE_IiiioI @ lessThan_THFTYPE_i @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_176,axiom,
    ( instance_THFTYPE_IIiooIioI @ holdsDuring_THFTYPE_IiooI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_177,axiom,
    ( domainSubclass_THFTYPE_IIiioIiioI @ rangeSubclass_THFTYPE_IiioI @ n2_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_178,axiom,
    ( instance_THFTYPE_IiioI @ lessThan_THFTYPE_i @ lTransitiveRelation_THFTYPE_i )).

thf(ax_179,axiom,
    ( domain_THFTYPE_IIiioIiioI @ disjoint_THFTYPE_IiioI @ n1_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_180,axiom,
    ( subrelation_THFTYPE_IiioI @ instrument_THFTYPE_i @ patient_THFTYPE_i )).

thf(ax_181,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lEndFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_182,axiom,
    ( instance_THFTYPE_IIiioIioI @ disjointRelation_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_183,axiom,
    ( domain_THFTYPE_IiiioI @ orientation_THFTYPE_i @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_184,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_185,axiom,
    ( instance_THFTYPE_IiioI @ lessThan_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_186,axiom,
    ( domain_THFTYPE_IiiioI @ attribute_THFTYPE_i @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_187,axiom,
    ( domain_THFTYPE_IiiioI @ lAdditionFn_THFTYPE_i @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_188,axiom,
    ( instance_THFTYPE_IIiioIioI @ located_THFTYPE_IiioI @ lTransitiveRelation_THFTYPE_i )).

thf(ax_189,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ domainSubclass_THFTYPE_IiiioI @ n3_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_190,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_191,axiom,
    ( instance_THFTYPE_IIiioIioI @ rangeSubclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

%----The translated conjecture
thf(con,conjecture,
    ( holdsDuring_THFTYPE_IiooI @ ( lYearFn_THFTYPE_IiiI @ n2009_THFTYPE_i )
    @ ( ( likes_THFTYPE_IiioI @ lMary_THFTYPE_i @ lBill_THFTYPE_i )
      & ( likes_THFTYPE_IiioI @ lSue_THFTYPE_i @ lBill_THFTYPE_i ) ) )).

%------------------------------------------------------------------------------
