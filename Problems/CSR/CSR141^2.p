%------------------------------------------------------------------------------
% File     : CSR141^2 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Commonsense Reasoning
% Problem  : Reiner and MariaPaola are not connected at the CADE meeting
% Version  : Especial > Augmented > Especial.
% English  : CADE_BM is a Meeting. One agent of this meeting is MariaPaola and
%            one is Reiner. It holds that both agents are not connected during
%            the meeting.

% Refs     : [Ben10] Benzmueller (2010), Email to Geoff Sutcliffe
% Source   : [Ben10]
% Names    : re_1.tq_SUMO_sine [Ben10]

% Status   : Theorem
% Rating   : 0.57 v6.4.0, 0.67 v6.3.0, 0.60 v6.2.0, 0.43 v6.1.0, 1.00 v6.0.0, 0.57 v5.5.0, 0.67 v5.4.0, 1.00 v5.2.0, 0.80 v4.1.0
% Syntax   : Number of formulae    :  252 (   0 unit;  90 type;   0 defn)
%            Number of atoms       :  770 (   7 equality; 196 variable)
%            Maximal formula depth :   13 (   4 average)
%            Number of connectives :  594 (   0   ~;   3   |;  29   &; 525   @)
%                                         (   4 <=>;  33  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  191 ( 191   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   95 (  90   :;   0   =)
%            Number of variables   :   90 (   0 sgn;  80   !;  10   ?;   0   ^)
%                                         (  90   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This is a simple test problem for reasoning in/about SUMO.
%            Initally the problem has been hand generated in KIF syntax in
%            SigmaKEE and then automatically translated by Benzmueller's
%            KIF2TH0 translator into THF syntax.
%          : The translation has been applied in three modes: handselected,
%            SInE, and local. The local mode only translates the local
%            assumptions and the query. The SInE mode additionally translates
%            the SInE extract of the loaded knowledge base (usually SUMO). The
%            handselected mode contains a hand-selected relevant axioms.
%          : The examples are selected to illustrate the benefits of
%            higher-order reasoning in ontology reasoning.
%------------------------------------------------------------------------------
%----The extracted signature
thf(numbers,type,(
    num: $tType )).

thf(agent_THFTYPE_IiioI,type,(
    agent_THFTYPE_IiioI: $i > $i > $o )).

thf(attribute_THFTYPE_i,type,(
    attribute_THFTYPE_i: $i )).

thf(capability_THFTYPE_IiIiioIioI,type,(
    capability_THFTYPE_IiIiioIioI: $i > ( $i > $i > $o ) > $i > $o )).

thf(connected_THFTYPE_IiioI,type,(
    connected_THFTYPE_IiioI: $i > $i > $o )).

thf(destination_THFTYPE_i,type,(
    destination_THFTYPE_i: $i )).

thf(documentation_THFTYPE_i,type,(
    documentation_THFTYPE_i: $i )).

thf(domainSubclass_THFTYPE_IIiIiioIioIiioI,type,(
    domainSubclass_THFTYPE_IIiIiioIioIiioI: ( $i > ( $i > $i > $o ) > $i > $o ) > $i > $i > $o )).

thf(domainSubclass_THFTYPE_IiiioI,type,(
    domainSubclass_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(domain_THFTYPE_IIIiIiioIioIiioIiioI,type,(
    domain_THFTYPE_IIIiIiioIioIiioIiioI: ( ( $i > ( $i > $i > $o ) > $i > $o ) > $i > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIIiiioIioIiioI,type,(
    domain_THFTYPE_IIIiiioIioIiioI: ( ( $i > $i > $i > $o ) > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIIiioIIiioIoIiioI,type,(
    domain_THFTYPE_IIIiioIIiioIoIiioI: ( ( $i > $i > $o ) > ( $i > $i > $o ) > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiIiioIioIiioI,type,(
    domain_THFTYPE_IIiIiioIioIiioI: ( $i > ( $i > $i > $o ) > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiiIiioI,type,(
    domain_THFTYPE_IIiiIiioI: ( $i > $i ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiiioIiioI,type,(
    domain_THFTYPE_IIiiioIiioI: ( $i > $i > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiioIiioI,type,(
    domain_THFTYPE_IIiioIiioI: ( $i > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiooIiioI,type,(
    domain_THFTYPE_IIiooIiioI: ( $i > $o > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IiiioI,type,(
    domain_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(equal_THFTYPE_i,type,(
    equal_THFTYPE_i: $i )).

thf(experiencer_THFTYPE_i,type,(
    experiencer_THFTYPE_i: $i )).

thf(hasPurpose_THFTYPE_IiooI,type,(
    hasPurpose_THFTYPE_IiooI: $i > $o > $o )).

thf(holdsDuring_THFTYPE_IiooI,type,(
    holdsDuring_THFTYPE_IiooI: $i > $o > $o )).

thf(instance_THFTYPE_IIIiIiioIioIiioIioI,type,(
    instance_THFTYPE_IIIiIiioIioIiioIioI: ( ( $i > ( $i > $i > $o ) > $i > $o ) > $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIIiioIIiioIoIioI,type,(
    instance_THFTYPE_IIIiioIIiioIoIioI: ( ( $i > $i > $o ) > ( $i > $i > $o ) > $o ) > $i > $o )).

thf(instance_THFTYPE_IIIiioIiioIioI,type,(
    instance_THFTYPE_IIIiioIiioIioI: ( ( $i > $i > $o ) > $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiIiioIioIioI,type,(
    instance_THFTYPE_IIiIiioIioIioI: ( $i > ( $i > $i > $o ) > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiiIioI,type,(
    instance_THFTYPE_IIiiIioI: ( $i > $i ) > $i > $o )).

thf(instance_THFTYPE_IIiiioIioI,type,(
    instance_THFTYPE_IIiiioIioI: ( $i > $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiioIioI,type,(
    instance_THFTYPE_IIiioIioI: ( $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiooIioI,type,(
    instance_THFTYPE_IIiooIioI: ( $i > $o > $o ) > $i > $o )).

thf(instance_THFTYPE_IiioI,type,(
    instance_THFTYPE_IiioI: $i > $i > $o )).

thf(involvedInEvent_THFTYPE_IiioI,type,(
    involvedInEvent_THFTYPE_IiioI: $i > $i > $o )).

thf(involvedInEvent_THFTYPE_i,type,(
    involvedInEvent_THFTYPE_i: $i )).

thf(lAgent_THFTYPE_i,type,(
    lAgent_THFTYPE_i: $i )).

thf(lAsymmetricRelation_THFTYPE_i,type,(
    lAsymmetricRelation_THFTYPE_i: $i )).

thf(lBeginFn_THFTYPE_IiiI,type,(
    lBeginFn_THFTYPE_IiiI: $i > $i )).

thf(lBinaryFunction_THFTYPE_i,type,(
    lBinaryFunction_THFTYPE_i: $i )).

thf(lBinaryPredicate_THFTYPE_i,type,(
    lBinaryPredicate_THFTYPE_i: $i )).

thf(lCADE_BM_THFTYPE_i,type,(
    lCADE_BM_THFTYPE_i: $i )).

thf(lCaseRole_THFTYPE_i,type,(
    lCaseRole_THFTYPE_i: $i )).

thf(lCognitiveAgent_THFTYPE_i,type,(
    lCognitiveAgent_THFTYPE_i: $i )).

thf(lCommunication_THFTYPE_i,type,(
    lCommunication_THFTYPE_i: $i )).

thf(lEndFn_THFTYPE_IiiI,type,(
    lEndFn_THFTYPE_IiiI: $i > $i )).

thf(lEntity_THFTYPE_i,type,(
    lEntity_THFTYPE_i: $i )).

thf(lFormula_THFTYPE_i,type,(
    lFormula_THFTYPE_i: $i )).

thf(lHuman_THFTYPE_i,type,(
    lHuman_THFTYPE_i: $i )).

thf(lInheritableRelation_THFTYPE_i,type,(
    lInheritableRelation_THFTYPE_i: $i )).

thf(lIntentionalProcess_THFTYPE_i,type,(
    lIntentionalProcess_THFTYPE_i: $i )).

thf(lMariaPaola_THFTYPE_i,type,(
    lMariaPaola_THFTYPE_i: $i )).

thf(lMeasureFn_THFTYPE_i,type,(
    lMeasureFn_THFTYPE_i: $i )).

thf(lMeeting_THFTYPE_i,type,(
    lMeeting_THFTYPE_i: $i )).

thf(lMultiplicationFn_THFTYPE_i,type,(
    lMultiplicationFn_THFTYPE_i: $i )).

thf(lNear_THFTYPE_i,type,(
    lNear_THFTYPE_i: $i )).

thf(lObject_THFTYPE_i,type,(
    lObject_THFTYPE_i: $i )).

thf(lOrganism_THFTYPE_i,type,(
    lOrganism_THFTYPE_i: $i )).

thf(lOrganization_THFTYPE_i,type,(
    lOrganization_THFTYPE_i: $i )).

thf(lPhysical_THFTYPE_i,type,(
    lPhysical_THFTYPE_i: $i )).

thf(lProcess_THFTYPE_i,type,(
    lProcess_THFTYPE_i: $i )).

thf(lReiner_THFTYPE_i,type,(
    lReiner_THFTYPE_i: $i )).

thf(lRelation_THFTYPE_i,type,(
    lRelation_THFTYPE_i: $i )).

thf(lSelfConnectedObject_THFTYPE_i,type,(
    lSelfConnectedObject_THFTYPE_i: $i )).

thf(lSocialInteraction_THFTYPE_i,type,(
    lSocialInteraction_THFTYPE_i: $i )).

thf(lTemporalRelation_THFTYPE_i,type,(
    lTemporalRelation_THFTYPE_i: $i )).

thf(lTernaryPredicate_THFTYPE_i,type,(
    lTernaryPredicate_THFTYPE_i: $i )).

thf(lTimeInterval_THFTYPE_i,type,(
    lTimeInterval_THFTYPE_i: $i )).

thf(lTotalValuedRelation_THFTYPE_i,type,(
    lTotalValuedRelation_THFTYPE_i: $i )).

thf(lUnaryFunction_THFTYPE_i,type,(
    lUnaryFunction_THFTYPE_i: $i )).

thf(lWhenFn_THFTYPE_IiiI,type,(
    lWhenFn_THFTYPE_IiiI: $i > $i )).

thf(lWhenFn_THFTYPE_i,type,(
    lWhenFn_THFTYPE_i: $i )).

thf(located_THFTYPE_IiioI,type,(
    located_THFTYPE_IiioI: $i > $i > $o )).

thf(meetsTemporally_THFTYPE_IiioI,type,(
    meetsTemporally_THFTYPE_IiioI: $i > $i > $o )).

thf(member_THFTYPE_IiioI,type,(
    member_THFTYPE_IiioI: $i > $i > $o )).

thf(n1_THFTYPE_i,type,(
    n1_THFTYPE_i: $i )).

thf(n2_THFTYPE_i,type,(
    n2_THFTYPE_i: $i )).

thf(n3_THFTYPE_i,type,(
    n3_THFTYPE_i: $i )).

thf(orientation_THFTYPE_IiiioI,type,(
    orientation_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(origin_THFTYPE_i,type,(
    origin_THFTYPE_i: $i )).

thf(part_THFTYPE_IiioI,type,(
    part_THFTYPE_IiioI: $i > $i > $o )).

thf(partition_THFTYPE_IiiioI,type,(
    partition_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(patient_THFTYPE_i,type,(
    patient_THFTYPE_i: $i )).

thf(range_THFTYPE_IiioI,type,(
    range_THFTYPE_IiioI: $i > $i > $o )).

thf(relatedInternalConcept_THFTYPE_IIiioIIIiiioIioIoI,type,(
    relatedInternalConcept_THFTYPE_IIiioIIIiiioIioIoI: ( $i > $i > $o ) > ( ( $i > $i > $i > $o ) > $i > $o ) > $o )).

thf(relatedInternalConcept_THFTYPE_i,type,(
    relatedInternalConcept_THFTYPE_i: $i )).

thf(subProcess_THFTYPE_IiioI,type,(
    subProcess_THFTYPE_IiioI: $i > $i > $o )).

thf(subclass_THFTYPE_IiioI,type,(
    subclass_THFTYPE_IiioI: $i > $i > $o )).

thf(subrelation_THFTYPE_IIiioIIiioIoI,type,(
    subrelation_THFTYPE_IIiioIIiioIoI: ( $i > $i > $o ) > ( $i > $i > $o ) > $o )).

thf(subrelation_THFTYPE_IIiioIioI,type,(
    subrelation_THFTYPE_IIiioIioI: ( $i > $i > $o ) > $i > $o )).

thf(subrelation_THFTYPE_IIioIIioIoI,type,(
    subrelation_THFTYPE_IIioIIioIoI: ( $i > $o ) > ( $i > $o ) > $o )).

thf(subrelation_THFTYPE_IiioI,type,(
    subrelation_THFTYPE_IiioI: $i > $i > $o )).

thf(temporalPart_THFTYPE_IiioI,type,(
    temporalPart_THFTYPE_IiioI: $i > $i > $o )).

%----The translated axioms
thf(ax,axiom,
    ( subclass_THFTYPE_IiioI @ lInheritableRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

%KIF documentation:(documentation instance EnglishLanguage "An object is an &%instance of a &%SetOrClass if it is included in that &%SetOrClass. An individual may be an instance of many classes, some of which may be subclasses of others. Thus, there is no assumption in the meaning of &%instance about specificity or uniqueness.")
thf(ax_001,axiom,(
    ! [MEET: $i,AGENT2: $i,AGENT1: $i] :
      ( ( ( instance_THFTYPE_IiioI @ MEET @ lMeeting_THFTYPE_i )
        & ( agent_THFTYPE_IiioI @ MEET @ AGENT1 )
        & ( agent_THFTYPE_IiioI @ MEET @ AGENT2 ) )
     => ( holdsDuring_THFTYPE_IiooI @ ( lWhenFn_THFTYPE_IiiI @ MEET ) @ ( orientation_THFTYPE_IiiioI @ AGENT1 @ AGENT2 @ lNear_THFTYPE_i ) ) ) )).

%KIF documentation:(documentation range EnglishLanguage "Gives the range of a function. In other words, (&%range ?FUNCTION ?CLASS) means that all of the values assigned by ?FUNCTION are &%instances of ?CLASS.")
%KIF documentation:(documentation Process EnglishLanguage "The class of things that happen and have temporal parts or stages. Examples include extended events like a football match or a race, actions like &%Pursuing and &%Reading, and biological processes. The formal definition is: anything that occurs in time but is not an &%Object. Note that a &%Process may have participants 'inside' it which are &%Objects, such as the players in a football match. In a 4D ontology, a &%Process is something whose spatiotemporal extent is thought of as dividing into temporal stages roughly perpendicular to the time-axis.")
thf(ax_002,axiom,(
    ! [X: $i,Y: $i,Z: $i] :
      ( ( ( subclass_THFTYPE_IiioI @ X @ Y )
        & ( instance_THFTYPE_IiioI @ Z @ X ) )
     => ( instance_THFTYPE_IiioI @ Z @ Y ) ) )).

%KIF documentation:(documentation TemporalRelation EnglishLanguage "The &%Class of temporal &%Relations. This &%Class includes notions of (temporal) topology of intervals, (temporal) schemata, and (temporal) extension.")
%KIF documentation:(documentation Agent EnglishLanguage "Something or someone that can act on its own and produce changes in the world.")
thf(ax_003,axiom,(
    ! [R: $i] :
      ( ( instance_THFTYPE_IiioI @ R @ lCaseRole_THFTYPE_i )
     => ( subrelation_THFTYPE_IiioI @ R @ involvedInEvent_THFTYPE_i ) ) )).

thf(ax_004,axiom,(
    ! [ROLE: $i > $i > $o,ARG1: $i,ARG2: $i,PROC: $i] :
      ( ( ( instance_THFTYPE_IIiioIioI @ ROLE @ lCaseRole_THFTYPE_i )
        & ( ROLE @ ARG1 @ ARG2 )
        & ( instance_THFTYPE_IiioI @ ARG1 @ PROC )
        & ( subclass_THFTYPE_IiioI @ PROC @ lProcess_THFTYPE_i ) )
     => ( capability_THFTYPE_IiIiioIioI @ PROC @ ROLE @ ARG2 ) ) )).

%KIF documentation:(documentation AsymmetricRelation EnglishLanguage "A &%BinaryRelation is asymmetric if and only if it is both an &%AntisymmetricRelation and an &%IrreflexiveRelation.")
thf(ax_005,axiom,(
    ! [OBJ1: $i,OBJ2: $i] :
      ( ( orientation_THFTYPE_IiiioI @ OBJ1 @ OBJ2 @ lNear_THFTYPE_i )
     => ( orientation_THFTYPE_IiiioI @ OBJ2 @ OBJ1 @ lNear_THFTYPE_i ) ) )).

thf(ax_006,axiom,(
    ! [THING: $i] :
      ( instance_THFTYPE_IiioI @ THING @ lEntity_THFTYPE_i ) )).

%KIF documentation:(documentation Communication EnglishLanguage "A &%SocialInteraction that involves the transfer of information between two or more &%CognitiveAgents. Note that &%Communication is closely related to, but essentially different from, &%ContentDevelopment. The latter involves the creation or modification of a &%ContentBearingObject, while &%Communication is the transfer of information for the purpose of conveying a message.")
thf(ax_007,axiom,(
    ! [OBJ1: $i,OBJ2: $i] :
      ( ( located_THFTYPE_IiioI @ OBJ1 @ OBJ2 )
     => ! [SUB: $i] :
          ( ( part_THFTYPE_IiioI @ SUB @ OBJ1 )
         => ( located_THFTYPE_IiioI @ SUB @ OBJ2 ) ) ) )).

%KIF documentation:(documentation MultiplicationFn EnglishLanguage "If ?NUMBER1 and ?NUMBER2 are &%Numbers, then (&%MultiplicationFn ?NUMBER1 ?NUMBER2) is the arithmetical product of these numbers.")
thf(ax_008,axiom,(
    ! [T: $i,E: $i] :
      ( ( involvedInEvent_THFTYPE_IiioI @ E @ T )
     => ? [R: $i > $i > $o] :
          ( ( instance_THFTYPE_IIiioIioI @ R @ lCaseRole_THFTYPE_i )
          & ( subrelation_THFTYPE_IIiioIIiioIoI @ R @ involvedInEvent_THFTYPE_IiioI )
          & ( R @ E @ T ) ) ) )).

%KIF documentation:(documentation experiencer EnglishLanguage "(&%experiencer ?PROCESS ?AGENT) means that ?AGENT experiences the &%Process ?PROCESS. For example, Yojo is the &%experiencer of seeing in the following proposition: Yojo sees the fish. Note that &%experiencer, unlike &%agent, does not entail a causal relation between its arguments.")
%KIF documentation:(documentation capability EnglishLanguage "(&%capability ?PROCESS ?ROLE ?OBJ) means that ?OBJ has the ability to play the role of ?ROLE in &%Processes of type ?PROCESS.")
thf(ax_009,axiom,(
    ! [INTERACTION: $i] :
      ( ( instance_THFTYPE_IiioI @ INTERACTION @ lSocialInteraction_THFTYPE_i )
     => ? [AGENT1: $i,AGENT2: $i] :
          ( ( involvedInEvent_THFTYPE_IiioI @ INTERACTION @ AGENT1 )
          & ( involvedInEvent_THFTYPE_IiioI @ INTERACTION @ AGENT2 )
          & ( instance_THFTYPE_IiioI @ AGENT1 @ lAgent_THFTYPE_i )
          & ( instance_THFTYPE_IiioI @ AGENT2 @ lAgent_THFTYPE_i )
          & ( ~ @ ( AGENT1 = AGENT2 ) ) ) ) )).

%KIF documentation:(documentation domain EnglishLanguage "Provides a computationally and heuristically convenient mechanism for declaring the argument types of a given relation. The formula (&%domain ?REL ?INT ?CLASS) means that the ?INT'th element of each tuple in the relation ?REL must be an instance of ?CLASS. Specifying argument types is very helpful in maintaining ontologies. Representation systems can use these specifications to classify terms and check integrity constraints. If the restriction on the argument type of a &%Relation is not captured by a &%SetOrClass already defined in the ontology, one can specify a &%SetOrClass compositionally with the functions &%UnionFn, &%IntersectionFn, etc.")
%KIF documentation:(documentation involvedInEvent EnglishLanguage "(involvedInEvent ?EVENT ?THING) means that in the &%Process ?EVENT, the &%Entity ?THING plays some &%CaseRole.")
thf(ax_010,axiom,(
    ! [AGENT: $i] :
      ( ( instance_THFTYPE_IiioI @ AGENT @ lAgent_THFTYPE_i )
    <=> ? [PROC: $i] :
          ( agent_THFTYPE_IiioI @ PROC @ AGENT ) ) )).

%KIF documentation:(documentation destination EnglishLanguage "(destination ?PROCESS ?GOAL) means that ?GOAL is the target or goal of the Process ?PROCESS. For example, Danbury would be the destination in the following proposition: Bob went to Danbury. Note that this is a very general &%CaseRole and, in particular, that it covers the concepts of 'recipient' and 'beneficiary'. Thus, John would be the &%destination in the following proposition: Tom gave a book to John.")
thf(ax_011,axiom,
    ( subclass_THFTYPE_IiioI @ lCaseRole_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation TimeInterval EnglishLanguage "An interval of time. Note that a &%TimeInterval has both an extent and a location on the universal timeline. Note too that a &%TimeInterval has no gaps, i.e. this class contains only convex time intervals.")
%KIF documentation:(documentation TernaryPredicate EnglishLanguage "The &%Class of &%Predicates that require exactly three arguments.")
thf(ax_012,axiom,
    ( subclass_THFTYPE_IiioI @ lTotalValuedRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation Relation EnglishLanguage "The &%Class of relations. There are three kinds of &%Relation: &%Predicate, &%Function, and &%List. &%Predicates and &%Functions both denote sets of ordered n-tuples. The difference between these two &%Classes is that &%Predicates cover formula-forming operators, while &%Functions cover term-forming operators. A &%List, on the other hand, is a particular ordered n-tuple.")
thf(ax_013,axiom,(
    ! [NUMBER: $i,CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( domainSubclass_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS1 )
        & ( domainSubclass_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

%KIF documentation:(documentation documentation EnglishLanguage "A relation between objects in the domain of discourse and strings of natural language text stated in a particular &%HumanLanguage. The domain of &%documentation is not constants (names), but the objects themselves. This means that one does not quote the names when associating them with their documentation.")
thf(ax_014,axiom,
    ( subclass_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

%KIF documentation:(documentation Organization EnglishLanguage "An &%Organization is a corporate or similar institution. The &%members of an &%Organization typically have a common purpose or function. Note that this class also covers divisions, departments, etc. of organizations. For example, both the Shell Corporation and the accounting department at Shell would both be instances of &%Organization. Note too that the existence of an &%Organization is dependent on the existence of at least one &%member (since &%Organization is a subclass of &%Collection). Accordingly, in cases of purely legal organizations, a fictitious &%member should be assumed.")
thf(ax_015,axiom,
    ( subclass_THFTYPE_IiioI @ lIntentionalProcess_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_016,axiom,(
    ! [NUMBER: $i,CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( domain_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS1 )
        & ( domain_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_017,axiom,
    ( subclass_THFTYPE_IiioI @ lCommunication_THFTYPE_i @ lSocialInteraction_THFTYPE_i )).

thf(ax_018,axiom,
    ( subclass_THFTYPE_IiioI @ lProcess_THFTYPE_i @ lPhysical_THFTYPE_i )).

%KIF documentation:(documentation Physical EnglishLanguage "An entity that has a location in space-time. Note that locations are themselves understood to have a location in space-time.")
%KIF documentation:(documentation member EnglishLanguage "A specialized common sense notion of part for uniform parts of &%Collections. For example, each sheep in a flock of sheep would have the relationship of member to the flock.")
thf(ax_019,axiom,
    ( subclass_THFTYPE_IiioI @ lMeeting_THFTYPE_i @ lSocialInteraction_THFTYPE_i )).

%KIF documentation:(documentation equal EnglishLanguage "(equal ?ENTITY1 ?ENTITY2) is true just in case ?ENTITY1 is identical with ?ENTITY2.")
thf(ax_020,axiom,
    ( subclass_THFTYPE_IiioI @ lHuman_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_021,axiom,
    ( subclass_THFTYPE_IiioI @ lCaseRole_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_022,axiom,
    ( agent_THFTYPE_IiioI @ lCADE_BM_THFTYPE_i @ lReiner_THFTYPE_i )).

thf(ax_023,axiom,
    ( agent_THFTYPE_IiioI @ lCADE_BM_THFTYPE_i @ lReiner_THFTYPE_i )).

thf(ax_024,axiom,(
    ! [TIME: $i,SITUATION: $o] :
      ( ( holdsDuring_THFTYPE_IiooI @ TIME @ ( ~ @ SITUATION ) )
     => ( ~ @ ( holdsDuring_THFTYPE_IiooI @ TIME @ SITUATION ) ) ) )).

thf(ax_025,axiom,
    ( agent_THFTYPE_IiioI @ lCADE_BM_THFTYPE_i @ lMariaPaola_THFTYPE_i )).

thf(ax_026,axiom,
    ( agent_THFTYPE_IiioI @ lCADE_BM_THFTYPE_i @ lMariaPaola_THFTYPE_i )).

%KIF documentation:(documentation holdsDuring EnglishLanguage "(&%holdsDuring ?TIME ?FORMULA) means that the proposition denoted by ?FORMULA is true in the time frame ?TIME. Note that this implies that ?FORMULA is true at every &%TimePoint which is a &%temporalPart of ?TIME.")
thf(ax_027,axiom,(
    ! [MEET: $i] :
      ( ( instance_THFTYPE_IiioI @ MEET @ lMeeting_THFTYPE_i )
     => ? [AGENT1: $i,AGENT2: $i] :
          ( ( agent_THFTYPE_IiioI @ MEET @ AGENT1 )
          & ( agent_THFTYPE_IiioI @ MEET @ AGENT2 )
          & ( hasPurpose_THFTYPE_IiooI @ MEET
            @ ? [COMM: $i] :
                ( ( instance_THFTYPE_IiioI @ COMM @ lCommunication_THFTYPE_i )
                & ( agent_THFTYPE_IiioI @ COMM @ AGENT1 )
                & ( agent_THFTYPE_IiioI @ COMM @ AGENT2 ) ) ) ) ) )).

%KIF documentation:(documentation attribute EnglishLanguage "(&%attribute ?OBJECT ?PROPERTY) means that ?PROPERTY is a &%Attribute of ?OBJECT. For example, (&%attribute &%MyLittleRedWagon &%Red).")
thf(ax_028,axiom,
    ( range_THFTYPE_IiioI @ lWhenFn_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

%KIF documentation:(documentation Organism EnglishLanguage "Generally, a living individual, including all &%Plants and &%Animals.")
thf(ax_029,axiom,(
    ! [INTERVAL1: $i,INTERVAL2: $i] :
      ( ( meetsTemporally_THFTYPE_IiioI @ INTERVAL1 @ INTERVAL2 )
    <=> ( ( lEndFn_THFTYPE_IiiI @ INTERVAL1 )
        = ( lBeginFn_THFTYPE_IiiI @ INTERVAL2 ) ) ) )).

thf(ax_030,axiom,(
    ! [SITUATION: $o,TIME2: $i,TIME1: $i] :
      ( ( ( holdsDuring_THFTYPE_IiooI @ TIME1 @ SITUATION )
        & ( temporalPart_THFTYPE_IiioI @ TIME2 @ TIME1 ) )
     => ( holdsDuring_THFTYPE_IiooI @ TIME2 @ SITUATION ) ) )).

%KIF documentation:(documentation Formula EnglishLanguage "A syntactically well-formed formula in the SUO-KIF knowledge representation language.")
thf(ax_031,axiom,(
    ? [THING: $i] :
      ( instance_THFTYPE_IiioI @ THING @ lEntity_THFTYPE_i ) )).

thf(ax_032,axiom,(
    ! [ORG: $i] :
      ( ( instance_THFTYPE_IiioI @ ORG @ lOrganization_THFTYPE_i )
     => ? [PURP: $o] :
        ! [MEMBER: $i] :
          ( ( member_THFTYPE_IiioI @ MEMBER @ ORG )
         => ( hasPurpose_THFTYPE_IiooI @ MEMBER @ PURP ) ) ) )).

%KIF documentation:(documentation SocialInteraction EnglishLanguage "The &%subclass of &%IntentionalProcess that involves interactions between &%CognitiveAgents.")
thf(ax_033,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryFunction_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_034,axiom,(
    ! [NUMBER: $i,PRED1: $i,CLASS1: $i,PRED2: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ PRED1 @ PRED2 )
        & ( domain_THFTYPE_IiiioI @ PRED2 @ NUMBER @ CLASS1 ) )
     => ( domain_THFTYPE_IiiioI @ PRED1 @ NUMBER @ CLASS1 ) ) )).

%KIF documentation:(documentation domainSubclass EnglishLanguage "&%Predicate used to specify argument type restrictions of &%Predicates. The formula (&%domainSubclass ?REL ?INT ?CLASS) means that the ?INT'th element of each tuple in the relation ?REL must be a subclass of ?CLASS.")
thf(ax_035,axiom,
    ( subclass_THFTYPE_IiioI @ lTotalValuedRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

%KIF documentation:(documentation located EnglishLanguage "(&%located ?PHYS ?OBJ) means that ?PHYS is &%partlyLocated at ?OBJ, and there is no &%part or &%subProcess of ?PHYS that is not &%located at ?OBJ.")
thf(ax_036,axiom,
    ( subclass_THFTYPE_IiioI @ lTernaryPredicate_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_037,axiom,
    ( subclass_THFTYPE_IiioI @ lObject_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_038,axiom,
    ( subclass_THFTYPE_IiioI @ lSelfConnectedObject_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_039,axiom,(
    ! [CLASS1: $i,CLASS2: $i] :
      ( ( CLASS1 = CLASS2 )
     => ! [THING: $i] :
          ( ( instance_THFTYPE_IiioI @ THING @ CLASS1 )
        <=> ( instance_THFTYPE_IiioI @ THING @ CLASS2 ) ) ) )).

%KIF documentation:(documentation TotalValuedRelation EnglishLanguage "A &%Relation is a &%TotalValuedRelation just in case there exists an assignment for the last argument position of the &%Relation given any assignment of values to every argument position except the last one. Note that declaring a &%Relation to be both a &%TotalValuedRelation and a &%SingleValuedRelation means that it is a total function.")
thf(ax_040,axiom,
    ( partition_THFTYPE_IiiioI @ lPhysical_THFTYPE_i @ lObject_THFTYPE_i @ lProcess_THFTYPE_i )).

%KIF documentation:(documentation Meeting EnglishLanguage "The coming together of two or more &%CognitiveAgents for the purpose of &%Communication. This covers informal meetings, e.g. visits with family members, and formal meetings, e.g. a board of directors meeting.")
thf(ax_041,axiom,(
    ! [REL2: $i > $o,ROW: $i,REL1: $i > $o] :
      ( ( ( subrelation_THFTYPE_IIioIIioIoI @ REL1 @ REL2 )
        & ( REL1 @ ROW ) )
     => ( REL2 @ ROW ) ) )).

%KIF documentation:(documentation subProcess EnglishLanguage "(&%subProcess ?SUBPROC ?PROC) means that ?SUBPROC is a subprocess of ?PROC. A subprocess is here understood as a temporally distinguished part (proper or not) of a &%Process.")
thf(ax_042,axiom,(
    ! [ORG: $i,AGENT: $i] :
      ( ( ( instance_THFTYPE_IiioI @ ORG @ lOrganization_THFTYPE_i )
        & ( member_THFTYPE_IiioI @ AGENT @ ORG ) )
     => ( instance_THFTYPE_IiioI @ AGENT @ lAgent_THFTYPE_i ) ) )).

thf(ax_043,axiom,(
    ! [THING2: $i,THING1: $i] :
      ( ( THING1 = THING2 )
     => ! [CLASS: $i] :
          ( ( instance_THFTYPE_IiioI @ THING1 @ CLASS )
        <=> ( instance_THFTYPE_IiioI @ THING2 @ CLASS ) ) ) )).

thf(ax_044,axiom,(
    ! [CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( range_THFTYPE_IiioI @ REL @ CLASS1 )
        & ( range_THFTYPE_IiioI @ REL @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_045,axiom,
    ( subclass_THFTYPE_IiioI @ lOrganism_THFTYPE_i @ lAgent_THFTYPE_i )).

%KIF documentation:(documentation orientation EnglishLanguage "A general &%Predicate for indicating how two &%Objects are oriented with respect to one another. For example, (orientation ?OBJ1 ?OBJ2 North) means that ?OBJ1 is north of ?OBJ2, and (orientation ?OBJ1 ?OBJ2 Vertical) means that ?OBJ1 is positioned vertically with respect to ?OBJ2.")
%KIF documentation:(documentation UnaryFunction EnglishLanguage "The &%Class of &%Functions that require a single argument.")
thf(ax_046,axiom,(
    ! [SUBPROC: $i,PROC: $i] :
      ( ( subProcess_THFTYPE_IiioI @ SUBPROC @ PROC )
     => ( temporalPart_THFTYPE_IiioI @ ( lWhenFn_THFTYPE_IiiI @ SUBPROC ) @ ( lWhenFn_THFTYPE_IiiI @ PROC ) ) ) )).

%KIF documentation:(documentation connected EnglishLanguage "(connected ?OBJ1 ?OBJ2) means that ?OBJ1 &%meetsSpatially ?OBJ2 or that ?OBJ1 &%overlapsSpatially ?OBJ2.")
%KIF documentation:(documentation relatedInternalConcept EnglishLanguage "Means that the two arguments are related concepts within the SUMO, i.e. there is a significant similarity of meaning between them. To indicate a meaning relation between a SUMO concept and a concept from another source, use the Predicate &%relatedExternalConcept.")
thf(ax_047,axiom,(
    ! [INTERVAL1: $i,INTERVAL2: $i] :
      ( ( ( ( lBeginFn_THFTYPE_IiiI @ INTERVAL1 )
          = ( lBeginFn_THFTYPE_IiiI @ INTERVAL2 ) )
        & ( ( lEndFn_THFTYPE_IiiI @ INTERVAL1 )
          = ( lEndFn_THFTYPE_IiiI @ INTERVAL2 ) ) )
     => ( INTERVAL1 = INTERVAL2 ) ) )).

%KIF documentation:(documentation BinaryPredicate EnglishLanguage "A &%Predicate relating two items - its valence is two.")
thf(ax_048,axiom,
    ( subclass_THFTYPE_IiioI @ lOrganization_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_049,axiom,(
    ! [REL2: $i,CLASS1: $i,REL1: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ REL1 @ REL2 )
        & ( range_THFTYPE_IiioI @ REL2 @ CLASS1 ) )
     => ( range_THFTYPE_IiioI @ REL1 @ CLASS1 ) ) )).

%KIF documentation:(documentation subclass EnglishLanguage "(&%subclass ?CLASS1 ?CLASS2) means that ?CLASS1 is a subclass of ?CLASS2, i.e. every instance of ?CLASS1 is also an instance of ?CLASS2. A class may have multiple superclasses and subclasses.")
%KIF documentation:(documentation MeasureFn EnglishLanguage "This &%BinaryFunction maps a &%RealNumber and a &%UnitOfMeasure to that &%Number of units. It is used to express `measured' instances of &%PhysicalQuantity. Example: the concept of three meters is represented as (&%MeasureFn 3 &%Meter).")
%KIF documentation:(documentation CaseRole EnglishLanguage "The &%Class of &%Predicates relating the spatially distinguished parts of a &%Process. &%CaseRoles include, for example, the &%agent, &%patient or &%destination of an action, the flammable substance in a burning process, or the water that falls in rain.")
%KIF documentation:(documentation part EnglishLanguage "The basic mereological relation. All other mereological relations are defined in terms of this one. (&%part ?PART ?WHOLE) simply means that the &%Object ?PART is part of the &%Object ?WHOLE. Note that, since &%part is a &%ReflexiveRelation, every &%Object is a part of itself.")
%KIF documentation:(documentation BinaryFunction EnglishLanguage "The &%Class of &%Functions that require two arguments.")
%KIF documentation:(documentation Entity EnglishLanguage "The universal class of individuals. This is the root node of the ontology.")
%KIF documentation:(documentation Near EnglishLanguage "The relation of common sense adjacency. Note that, if an object is &%Near another object, then the objects are not &%connected.")
%KIF documentation:(documentation WhenFn EnglishLanguage "A &%UnaryFunction that maps an &%Object or &%Process to the exact &%TimeInterval during which it exists. Note that, for every &%TimePoint ?TIME outside of the &%TimeInterval (WhenFn ?THING), (time ?THING ?TIME) does not hold.")
thf(ax_050,axiom,
    ( subclass_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation CognitiveAgent EnglishLanguage "A &%SentientAgent with responsibilities and the ability to reason, deliberate, make plans, etc. This is essentially the legal/ ethical notion of a person. Note that, although &%Human is a subclass of &%CognitiveAgent, there may be instances of &%CognitiveAgent which are not also instances of &%Human. For example, chimpanzees, gorillas, dolphins, whales, and some extraterrestrials (if they exist) may be &%CognitiveAgents.")
%KIF documentation:(documentation origin EnglishLanguage "(&%origin ?PROCESS ?SOURCE) means that ?SOURCE indicates where the ?Process began. Note that this relation implies that ?SOURCE is present at the beginning of the process, but need not participate throughout the process. For example, the submarine is the &%origin in the following proposition: the missile was launched from a submarine.")
thf(ax_051,axiom,(
    ! [PROC: $i] :
      ( ( instance_THFTYPE_IiioI @ PROC @ lIntentionalProcess_THFTYPE_i )
     => ? [AGENT: $i] :
          ( ( instance_THFTYPE_IiioI @ AGENT @ lCognitiveAgent_THFTYPE_i )
          & ( agent_THFTYPE_IiioI @ PROC @ AGENT ) ) ) )).

thf(ax_052,axiom,
    ( subclass_THFTYPE_IiioI @ lAgent_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_053,axiom,(
    ! [REL2: $i,NUMBER: $i,CLASS1: $i,REL1: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ REL1 @ REL2 )
        & ( domainSubclass_THFTYPE_IiiioI @ REL2 @ NUMBER @ CLASS1 ) )
     => ( domainSubclass_THFTYPE_IiiioI @ REL1 @ NUMBER @ CLASS1 ) ) )).

%KIF documentation:(documentation agent EnglishLanguage "(&%agent ?PROCESS ?AGENT) means that ?AGENT is an active determinant, either animate or inanimate, of the &%Process ?PROCESS, with or without voluntary intention. For example, Eve is an &%agent in the following proposition: Eve bit an apple.")
thf(ax_054,axiom,(
    ! [SUBPROC: $i,PROC: $i] :
      ( ( subProcess_THFTYPE_IiioI @ SUBPROC @ PROC )
     => ! [REGION: $i] :
          ( ( located_THFTYPE_IiioI @ PROC @ REGION )
         => ( located_THFTYPE_IiioI @ SUBPROC @ REGION ) ) ) )).

%KIF documentation:(documentation BeginFn EnglishLanguage "A &%UnaryFunction that maps a &%TimeInterval to the &%TimePoint at which the interval begins.")
%KIF documentation:(documentation EnglishLanguage EnglishLanguage "A Germanic language that incorporates many roots from the Romance languages. It is the official language of the &%UnitedStates, the &%UnitedKingdom, and many other countries.")
%KIF documentation:(documentation patient EnglishLanguage "(&%patient ?PROCESS ?ENTITY) means that ?ENTITY is a participant in ?PROCESS that may be moved, said, experienced, etc. For example, the direct objects in the sentences 'The cat swallowed the canary' and 'Billy likes the beer' would be examples of &%patients. Note that the &%patient of a &%Process may or may not undergo structural change as a result of the &%Process. The &%CaseRole of &%patient is used when one wants to specify as broadly as possible the object of a &%Process.")
thf(ax_055,axiom,
    ( subclass_THFTYPE_IiioI @ lCaseRole_THFTYPE_i @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_056,axiom,
    ( subclass_THFTYPE_IiioI @ lSocialInteraction_THFTYPE_i @ lIntentionalProcess_THFTYPE_i )).

%KIF documentation:(documentation hasPurpose EnglishLanguage "This &%Predicate expresses the concept of a conventional goal, i.e. a goal with a neutralized agent's intention. Accordingly, (&%hasPurpose ?THING ?FORMULA) means that the instance of &%Physical ?THING has, as its purpose, the &%Proposition expressed by ?FORMULA. Note that there is an important difference in meaning between the &%Predicates &%hasPurpose and &%result. Although the second argument of the latter can satisfy the second argument of the former, a conventional goal is an expected and desired outcome, while a result may be neither expected nor desired. For example, a machine process may have outcomes but no goals, aimless wandering may have an outcome but no goal, a learning process may have goals with no outcomes, and so on.")
%KIF documentation:(documentation SelfConnectedObject EnglishLanguage "A &%SelfConnectedObject is any &%Object that does not consist of two or more disconnected parts.")
thf(ax_057,axiom,
    ( subclass_THFTYPE_IiioI @ lPhysical_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_058,axiom,(
    ! [OBJ1: $i,OBJ2: $i] :
      ( ( orientation_THFTYPE_IiiioI @ OBJ1 @ OBJ2 @ lNear_THFTYPE_i )
     => ( ~ @ ( connected_THFTYPE_IiioI @ OBJ1 @ OBJ2 ) ) ) )).

%KIF documentation:(documentation temporalPart EnglishLanguage "The temporal analogue of the spatial &%part predicate. (&%temporalPart ?POS1 ?POS2) means that &%TimePosition ?POS1 is part of &%TimePosition ?POS2. Note that since &%temporalPart is a &%ReflexiveRelation every &%TimePostion is a &%temporalPart of itself.")
%KIF documentation:(documentation subrelation EnglishLanguage "(&%subrelation ?REL1 ?REL2) means that every tuple of ?REL1 is also a tuple of ?REL2. In other words, if the &%Relation ?REL1 holds for some arguments arg_1, arg_2, ... arg_n, then the &%Relation ?REL2 holds for the same arguments. A consequence of this is that a &%Relation and its subrelations must have the same &%valence.")
%KIF documentation:(documentation EndFn EnglishLanguage "A &%UnaryFunction that maps a &%TimeInterval to the &%TimePoint at which the interval ends.")
%KIF documentation:(documentation meetsTemporally EnglishLanguage "(&%meetsTemporally ?INTERVAL1 ?INTERVAL2) means that the terminal point of the &%TimeInterval ?INTERVAL1 is the initial point of the &%TimeInterval ?INTERVAL2.")
thf(ax_059,axiom,(
    ! [OBJ: $i,PROCESS: $i] :
      ( ( located_THFTYPE_IiioI @ PROCESS @ OBJ )
     => ! [SUB: $i] :
          ( ( subProcess_THFTYPE_IiioI @ SUB @ PROCESS )
         => ( located_THFTYPE_IiioI @ SUB @ OBJ ) ) ) )).

thf(ax_060,axiom,
    ( subclass_THFTYPE_IiioI @ lUnaryFunction_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation partition EnglishLanguage "A &%partition of a class C is a set of mutually &%disjoint classes (a subclass partition) which covers C. Every instance of C is an instance of exactly one of the subclasses in the partition.")
%KIF documentation:(documentation InheritableRelation EnglishLanguage "The class of &%Relations whose properties can be inherited downward in the class hierarchy via the &%subrelation &%Predicate.")
%KIF documentation:(documentation IntentionalProcess EnglishLanguage "A &%Process that has a specific purpose for the &%CognitiveAgent who performs it.")
%KIF documentation:(documentation Human EnglishLanguage "Modern man, the only remaining species of the Homo genus.")
thf(ax_061,axiom,(
    ! [CLASS: $i,PRED1: $i,PRED2: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ PRED1 @ PRED2 )
        & ( instance_THFTYPE_IiioI @ PRED2 @ CLASS )
        & ( subclass_THFTYPE_IiioI @ CLASS @ lInheritableRelation_THFTYPE_i ) )
     => ( instance_THFTYPE_IiioI @ PRED1 @ CLASS ) ) )).

%KIF documentation:(documentation Object EnglishLanguage "Corresponds roughly to the class of ordinary objects. Examples include normal physical objects, geographical regions, and locations of &%Processes, the complement of &%Objects in the &%Physical class. In a 4D ontology, an &%Object is something whose spatiotemporal extent is thought of as dividing into spatial parts roughly parallel to the time-axis.")
thf(ax_062,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_063,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i )).

thf(ax_064,axiom,
    ( instance_THFTYPE_IIiioIioI @ connected_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_065,axiom,
    ( instance_THFTYPE_IIiioIioI @ agent_THFTYPE_IiioI @ lCaseRole_THFTYPE_i )).

thf(ax_066,axiom,
    ( domain_THFTYPE_IiiioI @ origin_THFTYPE_i @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_067,axiom,
    ( instance_THFTYPE_IiioI @ lCADE_BM_THFTYPE_i @ lMeeting_THFTYPE_i )).

thf(ax_068,axiom,
    ( instance_THFTYPE_IiioI @ lCADE_BM_THFTYPE_i @ lMeeting_THFTYPE_i )).

thf(ax_069,axiom,
    ( instance_THFTYPE_IIiioIioI @ range_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_070,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_071,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subProcess_THFTYPE_IiioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_072,axiom,
    ( instance_THFTYPE_IiioI @ destination_THFTYPE_i @ lCaseRole_THFTYPE_i )).

thf(ax_073,axiom,
    ( domain_THFTYPE_IIiioIiioI @ meetsTemporally_THFTYPE_IiioI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_074,axiom,
    ( domain_THFTYPE_IiiioI @ equal_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_075,axiom,
    ( instance_THFTYPE_IiioI @ experiencer_THFTYPE_i @ lCaseRole_THFTYPE_i )).

thf(ax_076,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subProcess_THFTYPE_IiioI @ n2_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_077,axiom,
    ( domain_THFTYPE_IIiioIiioI @ agent_THFTYPE_IiioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_078,axiom,
    ( instance_THFTYPE_IiioI @ equal_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_079,axiom,
    ( instance_THFTYPE_IiioI @ relatedInternalConcept_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_080,axiom,
    ( domain_THFTYPE_IIIiioIIiioIoIiioI @ subrelation_THFTYPE_IIiioIIiioIoI @ n2_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_081,axiom,
    ( instance_THFTYPE_IiioI @ attribute_THFTYPE_i @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_082,axiom,
    ( domain_THFTYPE_IIiooIiioI @ hasPurpose_THFTYPE_IiooI @ n1_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_083,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_084,axiom,
    ( domainSubclass_THFTYPE_IIiIiioIioIiioI @ capability_THFTYPE_IiIiioIioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_085,axiom,
    ( instance_THFTYPE_IiioI @ patient_THFTYPE_i @ lCaseRole_THFTYPE_i )).

thf(ax_086,axiom,
    ( domain_THFTYPE_IiiioI @ patient_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_087,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_088,axiom,
    ( domain_THFTYPE_IIiioIiioI @ member_THFTYPE_IiioI @ n1_THFTYPE_i @ lSelfConnectedObject_THFTYPE_i )).

thf(ax_089,axiom,
    ( domain_THFTYPE_IiiioI @ patient_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_090,axiom,
    ( instance_THFTYPE_IiioI @ documentation_THFTYPE_i @ lTernaryPredicate_THFTYPE_i )).

thf(ax_091,axiom,
    ( subrelation_THFTYPE_IiioI @ experiencer_THFTYPE_i @ involvedInEvent_THFTYPE_i )).

thf(ax_092,axiom,
    ( domain_THFTYPE_IiiioI @ experiencer_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_093,axiom,
    ( instance_THFTYPE_IIiiioIioI @ orientation_THFTYPE_IiiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_094,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ domain_THFTYPE_IiiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_095,axiom,
    ( domain_THFTYPE_IIiioIiioI @ connected_THFTYPE_IiioI @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_096,axiom,
    ( instance_THFTYPE_IiioI @ involvedInEvent_THFTYPE_i @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_097,axiom,
    ( domain_THFTYPE_IiiioI @ involvedInEvent_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_098,axiom,
    ( domain_THFTYPE_IiiioI @ relatedInternalConcept_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_099,axiom,
    ( instance_THFTYPE_IIIiioIIiioIoIioI @ subrelation_THFTYPE_IIiioIIiioIoI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_100,axiom,
    ( subrelation_THFTYPE_IIiioIIiioIoI @ member_THFTYPE_IiioI @ part_THFTYPE_IiioI )).

thf(ax_101,axiom,
    ( domain_THFTYPE_IIiioIiioI @ part_THFTYPE_IiioI @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_102,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_103,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lBeginFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_104,axiom,
    ( instance_THFTYPE_IIiioIioI @ instance_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_105,axiom,
    ( domain_THFTYPE_IIIiiioIioIiioI @ instance_THFTYPE_IIiiioIioI @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_106,axiom,
    ( instance_THFTYPE_IIiooIioI @ holdsDuring_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_107,axiom,
    ( subrelation_THFTYPE_IIiioIioI @ agent_THFTYPE_IiioI @ involvedInEvent_THFTYPE_i )).

thf(ax_108,axiom,
    ( domain_THFTYPE_IiiioI @ experiencer_THFTYPE_i @ n2_THFTYPE_i @ lAgent_THFTYPE_i )).

thf(ax_109,axiom,
    ( domain_THFTYPE_IiiioI @ destination_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_110,axiom,
    ( domain_THFTYPE_IIiioIiioI @ part_THFTYPE_IiioI @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_111,axiom,
    ( subrelation_THFTYPE_IiioI @ destination_THFTYPE_i @ involvedInEvent_THFTYPE_i )).

thf(ax_112,axiom,
    ( instance_THFTYPE_IIiioIioI @ temporalPart_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i )).

thf(ax_113,axiom,
    ( instance_THFTYPE_IIiioIioI @ range_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_114,axiom,
    ( instance_THFTYPE_IIIiioIiioIioI @ domain_THFTYPE_IIiioIiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_115,axiom,
    ( domain_THFTYPE_IiiioI @ documentation_THFTYPE_i @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_116,axiom,
    ( domain_THFTYPE_IiiioI @ equal_THFTYPE_i @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_117,axiom,
    ( instance_THFTYPE_IIiioIioI @ temporalPart_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_118,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_119,axiom,
    ( instance_THFTYPE_IIiioIioI @ subProcess_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_120,axiom,
    ( instance_THFTYPE_IIiIiioIioIioI @ capability_THFTYPE_IiIiioIioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_121,axiom,
    ( domain_THFTYPE_IIiooIiioI @ holdsDuring_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_122,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_123,axiom,
    ( instance_THFTYPE_IiioI @ lMeasureFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_124,axiom,
    ( domain_THFTYPE_IIiIiioIioIiioI @ capability_THFTYPE_IiIiioIioI @ n3_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_125,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_126,axiom,
    ( instance_THFTYPE_IiioI @ involvedInEvent_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_127,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_128,axiom,
    ( subrelation_THFTYPE_IiioI @ origin_THFTYPE_i @ involvedInEvent_THFTYPE_i )).

thf(ax_129,axiom,
    ( instance_THFTYPE_IIiioIioI @ subclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_130,axiom,
    ( instance_THFTYPE_IIiiIioI @ lEndFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_131,axiom,
    ( instance_THFTYPE_IiioI @ origin_THFTYPE_i @ lCaseRole_THFTYPE_i )).

thf(ax_132,axiom,
    ( instance_THFTYPE_IIiiIioI @ lEndFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_133,axiom,
    ( domain_THFTYPE_IiiioI @ relatedInternalConcept_THFTYPE_i @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_134,axiom,
    ( domain_THFTYPE_IiiioI @ destination_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_135,axiom,
    ( domain_THFTYPE_IiiioI @ involvedInEvent_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_136,axiom,
    ( subrelation_THFTYPE_IiioI @ patient_THFTYPE_i @ involvedInEvent_THFTYPE_i )).

thf(ax_137,axiom,
    ( domain_THFTYPE_IIiooIiioI @ hasPurpose_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_138,axiom,
    ( domain_THFTYPE_IiiioI @ origin_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_139,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ orientation_THFTYPE_IiiioI @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_140,axiom,
    ( domain_THFTYPE_IIIiIiioIioIiioIiioI @ domainSubclass_THFTYPE_IIiIiioIioIiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_141,axiom,
    ( instance_THFTYPE_IiioI @ lMeasureFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_142,axiom,
    ( instance_THFTYPE_IIiooIioI @ hasPurpose_THFTYPE_IiooI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_143,axiom,
    ( instance_THFTYPE_IIiioIioI @ member_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_144,axiom,
    ( domain_THFTYPE_IIIiioIIiioIoIiioI @ subrelation_THFTYPE_IIiioIIiioIoI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_145,axiom,
    ( instance_THFTYPE_IIIiIiioIioIiioIioI @ domainSubclass_THFTYPE_IIiIiioIioIiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_146,axiom,
    ( instance_THFTYPE_IIiiIioI @ lEndFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_147,axiom,
    ( domain_THFTYPE_IIiioIiioI @ agent_THFTYPE_IiioI @ n2_THFTYPE_i @ lAgent_THFTYPE_i )).

thf(ax_148,axiom,
    ( domain_THFTYPE_IIiioIiioI @ meetsTemporally_THFTYPE_IiioI @ n2_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_149,axiom,
    ( instance_THFTYPE_IIiooIioI @ holdsDuring_THFTYPE_IiooI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_150,axiom,
    ( relatedInternalConcept_THFTYPE_IIiioIIIiiioIioIoI @ member_THFTYPE_IiioI @ instance_THFTYPE_IIiiioIioI )).

thf(ax_151,axiom,
    ( domain_THFTYPE_IIiIiioIioIiioI @ capability_THFTYPE_IiIiioIioI @ n2_THFTYPE_i @ lCaseRole_THFTYPE_i )).

thf(ax_152,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lWhenFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_153,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lEndFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_154,axiom,
    ( domain_THFTYPE_IIiioIiioI @ connected_THFTYPE_IiioI @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_155,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ orientation_THFTYPE_IiiioI @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_156,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_157,axiom,
    ( domain_THFTYPE_IiiioI @ attribute_THFTYPE_i @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_158,axiom,
    ( instance_THFTYPE_IIiooIioI @ hasPurpose_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_159,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_160,axiom,
    ( holdsDuring_THFTYPE_IiooI @ ( lWhenFn_THFTYPE_IiiI @ lCADE_BM_THFTYPE_i ) @ $true )).

%----The translated conjectures
thf(con,conjecture,
    ( holdsDuring_THFTYPE_IiooI @ ( lWhenFn_THFTYPE_IiiI @ lCADE_BM_THFTYPE_i ) @ ( ~ @ ( connected_THFTYPE_IiioI @ lMariaPaola_THFTYPE_i @ lReiner_THFTYPE_i ) ) )).

%------------------------------------------------------------------------------
