%------------------------------------------------------------------------------
% File     : CSR144^1 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Commonsense Reasoning
% Problem  : Does Max think he's single?
% Version  : Especial.
% English  : There is no time during which Max considers to have a wife. Is it
%            true that Max does not believe that he is a husband of somebody?.

% Refs     : [Ben10] Benzmueller (2010), Email to Geoff Sutcliffe
% Source   : [Ben10]
% Names    : ex_3.tq_SUMO_handselected [Ben10]

% Status   : Theorem
% Rating   : 0.38 v6.4.0, 0.43 v6.3.0, 0.50 v6.2.0, 0.33 v6.1.0, 0.67 v6.0.0, 0.33 v5.5.0, 0.20 v5.4.0, 0.00 v5.3.0, 0.50 v5.0.0, 0.25 v4.1.0
% Syntax   : Number of formulae    :   13 (   0 unit;   8 type;   0 defn)
%            Number of atoms       :   34 (   0 equality;  16 variable)
%            Maximal formula depth :    9 (   5 average)
%            Number of connectives :   29 (   0   ~;   0   |;   0   &;  26   @)
%                                         (   1 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   20 (  20   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   12 (   8   :;   0   =)
%            Number of variables   :   10 (   0 sgn;   7   !;   3   ?;   0   ^)
%                                         (  10   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This is a simple test problem for reasoning in/about SUMO.
%            Initally the problem has been hand generated in KIF syntax in
%            SigmaKEE and then automatically translated by Benzmueller's
%            KIF2TH0 translator into THF syntax.
%          : The translation has been applied in three modes: handselected,
%            SInE, and local. The local mode only translates the local
%            assumptions and the query. The SInE mode additionally translates
%            the SInE extract of the loaded knowledge base (usually SUMO). The
%            handselected mode contains a hand-selected relevant axioms.
%          : The examples are selected to illustrate the benefits of
%            higher-order reasoning in ontology reasoning.
%------------------------------------------------------------------------------
%----The extracted signature
thf(numbers,type,(
    num: $tType )).

thf(believes_THFTYPE_IiooI,type,(
    believes_THFTYPE_IiooI: $i > $o > $o )).

thf(considers_THFTYPE_IiooI,type,(
    considers_THFTYPE_IiooI: $i > $o > $o )).

thf(holdsDuring_THFTYPE_IiooI,type,(
    holdsDuring_THFTYPE_IiooI: $i > $o > $o )).

thf(husband_THFTYPE_IiioI,type,(
    husband_THFTYPE_IiioI: $i > $i > $o )).

thf(lMax_THFTYPE_i,type,(
    lMax_THFTYPE_i: $i )).

thf(wife_THFTYPE_IiioI,type,(
    wife_THFTYPE_IiioI: $i > $i > $o )).

%----The handselected axioms from the knowledge base
thf(inverse_THFTYPE_IIiioIIiioIoI,type,(
    inverse_THFTYPE_IIiioIIiioIoI: ( $i > $i > $o ) > ( $i > $i > $o ) > $o )).

thf(ax,axiom,
    ( inverse_THFTYPE_IIiioIIiioIoI @ husband_THFTYPE_IiioI @ wife_THFTYPE_IiioI )).

thf(ax_001,axiom,(
    ! [REL2: $i > $i > $o,REL1: $i > $i > $o] :
      ( ( inverse_THFTYPE_IIiioIIiioIoI @ REL1 @ REL2 )
     => ! [INST1: $i,INST2: $i] :
          ( ( REL1 @ INST1 @ INST2 )
        <=> ( REL2 @ INST2 @ INST1 ) ) ) )).

thf(ax_002,axiom,(
    ! [FORMULA: $o,AGENT: $i] :
      ( ( believes_THFTYPE_IiooI @ AGENT @ FORMULA )
     => ? [TIME: $i] :
          ( holdsDuring_THFTYPE_IiooI @ TIME @ ( considers_THFTYPE_IiooI @ AGENT @ FORMULA ) ) ) )).

%----The translated axioms
thf(ax_003,axiom,(
    ! [X: $i] :
      ( ~
      @ ? [Z: $i] :
          ( holdsDuring_THFTYPE_IiooI @ Z @ ( considers_THFTYPE_IiooI @ lMax_THFTYPE_i @ ( wife_THFTYPE_IiioI @ X @ lMax_THFTYPE_i ) ) ) ) )).

%----The translated conjectures
thf(con,conjecture,(
    ? [Z: $i] :
      ( ~ @ ( believes_THFTYPE_IiooI @ lMax_THFTYPE_i @ ( husband_THFTYPE_IiioI @ lMax_THFTYPE_i @ Z ) ) ) )).

%------------------------------------------------------------------------------
