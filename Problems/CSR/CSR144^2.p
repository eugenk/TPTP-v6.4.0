%------------------------------------------------------------------------------
% File     : CSR144^2 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Commonsense Reasoning
% Problem  : Does Max think he's single?
% Version  : Especial.
% English  : There is no time during which Max considers to have a wife. Is it
%            true that Max does not believe that he is a husband of somebody?.

% Refs     : [Ben10] Benzmueller (2010), Email to Geoff Sutcliffe
% Source   : [Ben10]
% Names    : ex_3.tq_SUMO_sine [Ben10]

% Status   : Theorem
% Rating   : 0.57 v6.4.0, 0.67 v6.3.0, 0.60 v6.2.0, 0.43 v6.1.0, 0.86 v6.0.0, 0.71 v5.5.0, 0.67 v5.4.0, 1.00 v5.3.0, 0.80 v5.2.0, 1.00 v4.1.0
% Syntax   : Number of formulae    :  457 (   0 unit; 133 type;   0 defn)
%            Number of atoms       : 1516 (  15 equality; 381 variable)
%            Maximal formula depth :   13 (   4 average)
%            Number of connectives : 1162 (   0   ~;   5   |;  47   &;1033   @)
%                                         (  14 <=>;  63  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  259 ( 259   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :  137 ( 133   :;   0   =)
%            Number of variables   :  180 (   0 sgn; 162   !;  18   ?;   0   ^)
%                                         ( 180   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This is a simple test problem for reasoning in/about SUMO.
%            Initally the problem has been hand generated in KIF syntax in
%            SigmaKEE and then automatically translated by Benzmueller's
%            KIF2TH0 translator into THF syntax.
%          : The translation has been applied in three modes: handselected,
%            SInE, and local. The local mode only translates the local
%            assumptions and the query. The SInE mode additionally translates
%            the SInE extract of the loaded knowledge base (usually SUMO). The
%            handselected mode contains a hand-selected relevant axioms.
%          : The examples are selected to illustrate the benefits of
%            higher-order reasoning in ontology reasoning.
%------------------------------------------------------------------------------
%----The extracted signature
thf(numbers,type,(
    num: $tType )).

thf(agent_THFTYPE_IiioI,type,(
    agent_THFTYPE_IiioI: $i > $i > $o )).

thf(attribute_THFTYPE_IiioI,type,(
    attribute_THFTYPE_IiioI: $i > $i > $o )).

thf(before_THFTYPE_IiioI,type,(
    before_THFTYPE_IiioI: $i > $i > $o )).

thf(believes_THFTYPE_IiooI,type,(
    believes_THFTYPE_IiooI: $i > $o > $o )).

thf(connected_THFTYPE_i,type,(
    connected_THFTYPE_i: $i )).

thf(considers_THFTYPE_IiooI,type,(
    considers_THFTYPE_IiooI: $i > $o > $o )).

thf(contraryAttribute_THFTYPE_IiioI,type,(
    contraryAttribute_THFTYPE_IiioI: $i > $i > $o )).

thf(contraryAttribute_THFTYPE_IioI,type,(
    contraryAttribute_THFTYPE_IioI: $i > $o )).

thf(desires_THFTYPE_IiooI,type,(
    desires_THFTYPE_IiooI: $i > $o > $o )).

thf(disjoint_THFTYPE_IiioI,type,(
    disjoint_THFTYPE_IiioI: $i > $i > $o )).

thf(documentation_THFTYPE_i,type,(
    documentation_THFTYPE_i: $i )).

thf(domainSubclass_THFTYPE_IiiioI,type,(
    domainSubclass_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(domain_THFTYPE_IIIiioIIiioIoIiioI,type,(
    domain_THFTYPE_IIIiioIIiioIoIiioI: ( ( $i > $i > $o ) > ( $i > $i > $o ) > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIIiioIIiooIoIiioI,type,(
    domain_THFTYPE_IIIiioIIiooIoIiioI: ( ( $i > $i > $o ) > ( $i > $o > $o ) > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiiIiioI,type,(
    domain_THFTYPE_IIiiIiioI: ( $i > $i ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiiiIiioI,type,(
    domain_THFTYPE_IIiiiIiioI: ( $i > $i > $i ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiiioIiioI,type,(
    domain_THFTYPE_IIiiioIiioI: ( $i > $i > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiioIiioI,type,(
    domain_THFTYPE_IIiioIiioI: ( $i > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIioioIiioI,type,(
    domain_THFTYPE_IIioioIiioI: ( $i > $o > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiooIiioI,type,(
    domain_THFTYPE_IIiooIiioI: ( $i > $o > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IiiioI,type,(
    domain_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(equal_THFTYPE_i,type,(
    equal_THFTYPE_i: $i )).

thf(father_THFTYPE_IiioI,type,(
    father_THFTYPE_IiioI: $i > $i > $o )).

thf(greaterThanOrEqualTo_THFTYPE_IiioI,type,(
    greaterThanOrEqualTo_THFTYPE_IiioI: $i > $i > $o )).

thf(greaterThan_THFTYPE_IiioI,type,(
    greaterThan_THFTYPE_IiioI: $i > $i > $o )).

thf(gt_THFTYPE_IiioI,type,(
    gt_THFTYPE_IiioI: $i > $i > $o )).

thf(gtet_THFTYPE_IiioI,type,(
    gtet_THFTYPE_IiioI: $i > $i > $o )).

thf(hasPurposeForAgent_THFTYPE_IioioI,type,(
    hasPurposeForAgent_THFTYPE_IioioI: $i > $o > $i > $o )).

thf(hasPurpose_THFTYPE_IiooI,type,(
    hasPurpose_THFTYPE_IiooI: $i > $o > $o )).

thf(holdsDuring_THFTYPE_IiooI,type,(
    holdsDuring_THFTYPE_IiooI: $i > $o > $o )).

thf(husband_THFTYPE_IiioI,type,(
    husband_THFTYPE_IiioI: $i > $i > $o )).

thf(inList_THFTYPE_IiioI,type,(
    inList_THFTYPE_IiioI: $i > $i > $o )).

thf(inScopeOfInterest_THFTYPE_IiioI,type,(
    inScopeOfInterest_THFTYPE_IiioI: $i > $i > $o )).

thf(instance_THFTYPE_IIIiioIIiioIoIioI,type,(
    instance_THFTYPE_IIIiioIIiioIoIioI: ( ( $i > $i > $o ) > ( $i > $i > $o ) > $o ) > $i > $o )).

thf(instance_THFTYPE_IIIiioIiioIioI,type,(
    instance_THFTYPE_IIIiioIiioIioI: ( ( $i > $i > $o ) > $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiiIioI,type,(
    instance_THFTYPE_IIiiIioI: ( $i > $i ) > $i > $o )).

thf(instance_THFTYPE_IIiiiIioI,type,(
    instance_THFTYPE_IIiiiIioI: ( $i > $i > $i ) > $i > $o )).

thf(instance_THFTYPE_IIiiioIioI,type,(
    instance_THFTYPE_IIiiioIioI: ( $i > $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiioIioI,type,(
    instance_THFTYPE_IIiioIioI: ( $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIioioIioI,type,(
    instance_THFTYPE_IIioioIioI: ( $i > $o > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiooIioI,type,(
    instance_THFTYPE_IIiooIioI: ( $i > $o > $o ) > $i > $o )).

thf(instance_THFTYPE_IiioI,type,(
    instance_THFTYPE_IiioI: $i > $i > $o )).

thf(inverse_THFTYPE_IIiioIIiioIoI,type,(
    inverse_THFTYPE_IIiioIIiioIoI: ( $i > $i > $o ) > ( $i > $i > $o ) > $o )).

thf(knows_THFTYPE_IiooI,type,(
    knows_THFTYPE_IiooI: $i > $o > $o )).

thf(lAdditionFn_THFTYPE_i,type,(
    lAdditionFn_THFTYPE_i: $i )).

thf(lAgent_THFTYPE_i,type,(
    lAgent_THFTYPE_i: $i )).

thf(lAsymmetricRelation_THFTYPE_i,type,(
    lAsymmetricRelation_THFTYPE_i: $i )).

thf(lBeginFn_THFTYPE_IiiI,type,(
    lBeginFn_THFTYPE_IiiI: $i > $i )).

thf(lBeginFn_THFTYPE_i,type,(
    lBeginFn_THFTYPE_i: $i )).

thf(lBinaryFunction_THFTYPE_i,type,(
    lBinaryFunction_THFTYPE_i: $i )).

thf(lBinaryPredicate_THFTYPE_i,type,(
    lBinaryPredicate_THFTYPE_i: $i )).

thf(lBinaryRelation_THFTYPE_i,type,(
    lBinaryRelation_THFTYPE_i: $i )).

thf(lBodyPart_THFTYPE_i,type,(
    lBodyPart_THFTYPE_i: $i )).

thf(lCognitiveAgent_THFTYPE_i,type,(
    lCognitiveAgent_THFTYPE_i: $i )).

thf(lEndFn_THFTYPE_IiiI,type,(
    lEndFn_THFTYPE_IiiI: $i > $i )).

thf(lEndFn_THFTYPE_i,type,(
    lEndFn_THFTYPE_i: $i )).

thf(lEntity_THFTYPE_i,type,(
    lEntity_THFTYPE_i: $i )).

thf(lFemale_THFTYPE_i,type,(
    lFemale_THFTYPE_i: $i )).

thf(lFormula_THFTYPE_i,type,(
    lFormula_THFTYPE_i: $i )).

thf(lHuman_THFTYPE_i,type,(
    lHuman_THFTYPE_i: $i )).

thf(lInheritableRelation_THFTYPE_i,type,(
    lInheritableRelation_THFTYPE_i: $i )).

thf(lIntentionalProcess_THFTYPE_i,type,(
    lIntentionalProcess_THFTYPE_i: $i )).

thf(lIrreflexiveRelation_THFTYPE_i,type,(
    lIrreflexiveRelation_THFTYPE_i: $i )).

thf(lListFn_THFTYPE_IiiI,type,(
    lListFn_THFTYPE_IiiI: $i > $i )).

thf(lMale_THFTYPE_i,type,(
    lMale_THFTYPE_i: $i )).

thf(lMan_THFTYPE_i,type,(
    lMan_THFTYPE_i: $i )).

thf(lMax_THFTYPE_i,type,(
    lMax_THFTYPE_i: $i )).

thf(lMeasureFn_THFTYPE_IiiiI,type,(
    lMeasureFn_THFTYPE_IiiiI: $i > $i > $i )).

thf(lMultiplicationFn_THFTYPE_i,type,(
    lMultiplicationFn_THFTYPE_i: $i )).

thf(lObject_THFTYPE_i,type,(
    lObject_THFTYPE_i: $i )).

thf(lOrganism_THFTYPE_i,type,(
    lOrganism_THFTYPE_i: $i )).

thf(lOrganization_THFTYPE_i,type,(
    lOrganization_THFTYPE_i: $i )).

thf(lPartialOrderingRelation_THFTYPE_i,type,(
    lPartialOrderingRelation_THFTYPE_i: $i )).

thf(lPhysical_THFTYPE_i,type,(
    lPhysical_THFTYPE_i: $i )).

thf(lProcess_THFTYPE_i,type,(
    lProcess_THFTYPE_i: $i )).

thf(lPropositionalAttitude_THFTYPE_i,type,(
    lPropositionalAttitude_THFTYPE_i: $i )).

thf(lQuantity_THFTYPE_i,type,(
    lQuantity_THFTYPE_i: $i )).

thf(lRealNumber_THFTYPE_i,type,(
    lRealNumber_THFTYPE_i: $i )).

thf(lRelationExtendedToQuantities_THFTYPE_i,type,(
    lRelationExtendedToQuantities_THFTYPE_i: $i )).

thf(lRelation_THFTYPE_i,type,(
    lRelation_THFTYPE_i: $i )).

thf(lReproductiveBody_THFTYPE_i,type,(
    lReproductiveBody_THFTYPE_i: $i )).

thf(lSelfConnectedObject_THFTYPE_i,type,(
    lSelfConnectedObject_THFTYPE_i: $i )).

thf(lSingleValuedRelation_THFTYPE_i,type,(
    lSingleValuedRelation_THFTYPE_i: $i )).

thf(lSymmetricRelation_THFTYPE_i,type,(
    lSymmetricRelation_THFTYPE_i: $i )).

thf(lTemporalRelation_THFTYPE_i,type,(
    lTemporalRelation_THFTYPE_i: $i )).

thf(lTernaryPredicate_THFTYPE_i,type,(
    lTernaryPredicate_THFTYPE_i: $i )).

thf(lTimeInterval_THFTYPE_i,type,(
    lTimeInterval_THFTYPE_i: $i )).

thf(lTimePoint_THFTYPE_i,type,(
    lTimePoint_THFTYPE_i: $i )).

thf(lTimePosition_THFTYPE_i,type,(
    lTimePosition_THFTYPE_i: $i )).

thf(lTotalValuedRelation_THFTYPE_i,type,(
    lTotalValuedRelation_THFTYPE_i: $i )).

thf(lTransitiveRelation_THFTYPE_i,type,(
    lTransitiveRelation_THFTYPE_i: $i )).

thf(lUnaryFunction_THFTYPE_i,type,(
    lUnaryFunction_THFTYPE_i: $i )).

thf(lUnitOfMeasure_THFTYPE_i,type,(
    lUnitOfMeasure_THFTYPE_i: $i )).

thf(lWhenFn_THFTYPE_IiiI,type,(
    lWhenFn_THFTYPE_IiiI: $i > $i )).

thf(lWhenFn_THFTYPE_i,type,(
    lWhenFn_THFTYPE_i: $i )).

thf(lWoman_THFTYPE_i,type,(
    lWoman_THFTYPE_i: $i )).

thf(lessThanOrEqualTo_THFTYPE_IiioI,type,(
    lessThanOrEqualTo_THFTYPE_IiioI: $i > $i > $o )).

thf(lessThan_THFTYPE_IiioI,type,(
    lessThan_THFTYPE_IiioI: $i > $i > $o )).

thf(located_THFTYPE_IiioI,type,(
    located_THFTYPE_IiioI: $i > $i > $o )).

thf(lt_THFTYPE_IiioI,type,(
    lt_THFTYPE_IiioI: $i > $i > $o )).

thf(ltet_THFTYPE_IiioI,type,(
    ltet_THFTYPE_IiioI: $i > $i > $o )).

thf(meetsTemporally_THFTYPE_IiioI,type,(
    meetsTemporally_THFTYPE_IiioI: $i > $i > $o )).

thf(member_THFTYPE_IiioI,type,(
    member_THFTYPE_IiioI: $i > $i > $o )).

thf(mother_THFTYPE_IiioI,type,(
    mother_THFTYPE_IiioI: $i > $i > $o )).

thf(n1_THFTYPE_i,type,(
    n1_THFTYPE_i: $i )).

thf(n2_THFTYPE_i,type,(
    n2_THFTYPE_i: $i )).

thf(n3_THFTYPE_i,type,(
    n3_THFTYPE_i: $i )).

thf(orientation_THFTYPE_IiiioI,type,(
    orientation_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(parent_THFTYPE_IiioI,type,(
    parent_THFTYPE_IiioI: $i > $i > $o )).

thf(part_THFTYPE_IiioI,type,(
    part_THFTYPE_IiioI: $i > $i > $o )).

thf(partition_THFTYPE_IiiioI,type,(
    partition_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(patient_THFTYPE_IiioI,type,(
    patient_THFTYPE_IiioI: $i > $i > $o )).

thf(possesses_THFTYPE_IiioI,type,(
    possesses_THFTYPE_IiioI: $i > $i > $o )).

thf(range_THFTYPE_IiioI,type,(
    range_THFTYPE_IiioI: $i > $i > $o )).

thf(relatedInternalConcept_THFTYPE_IIiioIIiioIoI,type,(
    relatedInternalConcept_THFTYPE_IIiioIIiioIoI: ( $i > $i > $o ) > ( $i > $i > $o ) > $o )).

thf(relatedInternalConcept_THFTYPE_IIiioIIiooIoI,type,(
    relatedInternalConcept_THFTYPE_IIiioIIiooIoI: ( $i > $i > $o ) > ( $i > $o > $o ) > $o )).

thf(relatedInternalConcept_THFTYPE_IIiooIIiioIoI,type,(
    relatedInternalConcept_THFTYPE_IIiooIIiioIoI: ( $i > $o > $o ) > ( $i > $i > $o ) > $o )).

thf(relatedInternalConcept_THFTYPE_i,type,(
    relatedInternalConcept_THFTYPE_i: $i )).

thf(result_THFTYPE_IiioI,type,(
    result_THFTYPE_IiioI: $i > $i > $o )).

thf(spouse_THFTYPE_i,type,(
    spouse_THFTYPE_i: $i )).

thf(subAttribute_THFTYPE_IiioI,type,(
    subAttribute_THFTYPE_IiioI: $i > $i > $o )).

thf(subProcess_THFTYPE_IiioI,type,(
    subProcess_THFTYPE_IiioI: $i > $i > $o )).

thf(subclass_THFTYPE_IiioI,type,(
    subclass_THFTYPE_IiioI: $i > $i > $o )).

thf(subrelation_THFTYPE_IIiioIIiioIoI,type,(
    subrelation_THFTYPE_IIiioIIiioIoI: ( $i > $i > $o ) > ( $i > $i > $o ) > $o )).

thf(subrelation_THFTYPE_IIiioIioI,type,(
    subrelation_THFTYPE_IIiioIioI: ( $i > $i > $o ) > $i > $o )).

thf(subrelation_THFTYPE_IIioIIioIoI,type,(
    subrelation_THFTYPE_IIioIIioIoI: ( $i > $o ) > ( $i > $o ) > $o )).

thf(subrelation_THFTYPE_IIiooIIiioIoI,type,(
    subrelation_THFTYPE_IIiooIIiioIoI: ( $i > $o > $o ) > ( $i > $i > $o ) > $o )).

thf(subrelation_THFTYPE_IiioI,type,(
    subrelation_THFTYPE_IiioI: $i > $i > $o )).

thf(temporalPart_THFTYPE_IiioI,type,(
    temporalPart_THFTYPE_IiioI: $i > $i > $o )).

thf(time_THFTYPE_IiioI,type,(
    time_THFTYPE_IiioI: $i > $i > $o )).

thf(wants_THFTYPE_IiioI,type,(
    wants_THFTYPE_IiioI: $i > $i > $o )).

thf(wife_THFTYPE_IiioI,type,(
    wife_THFTYPE_IiioI: $i > $i > $o )).

%----The translated axioms
thf(ax,axiom,
    ( subclass_THFTYPE_IiioI @ lPropositionalAttitude_THFTYPE_i @ lAsymmetricRelation_THFTYPE_i )).

%KIF documentation:(documentation instance EnglishLanguage "An object is an &%instance of a &%SetOrClass if it is included in that &%SetOrClass. An individual may be an instance of many classes, some of which may be subclasses of others. Thus, there is no assumption in the meaning of &%instance about specificity or uniqueness.")
%KIF documentation:(documentation range EnglishLanguage "Gives the range of a function. In other words, (&%range ?FUNCTION ?CLASS) means that all of the values assigned by ?FUNCTION are &%instances of ?CLASS.")
thf(ax_001,axiom,(
    ! [REL: $i > $i > $o] :
      ( ( instance_THFTYPE_IIiioIioI @ REL @ lSingleValuedRelation_THFTYPE_i )
     => ! [ROW: $i,ITEM1: $i,ITEM2: $i] :
          ( ( ( REL @ ROW @ ITEM1 )
            & ( REL @ ROW @ ITEM2 ) )
         => ( ITEM1 = ITEM2 ) ) ) )).

thf(ax_002,axiom,(
    ! [X: $i,Y: $i,Z: $i] :
      ( ( ( subclass_THFTYPE_IiioI @ X @ Y )
        & ( instance_THFTYPE_IiioI @ Z @ X ) )
     => ( instance_THFTYPE_IiioI @ Z @ Y ) ) )).

%KIF documentation:(documentation Agent EnglishLanguage "Something or someone that can act on its own and produce changes in the world.")
%KIF documentation:(documentation wants EnglishLanguage "(&%wants ?AGENT ?OBJECT) means that ?OBJECT is desired by ?AGENT, i.e. ?AGENT believes that ?OBJECT will satisfy one of its goals. Note that there is no implication that what is wanted by an agent is not already possessed by the agent.")
%KIF documentation:(documentation lessThan EnglishLanguage "(&%lessThan ?NUMBER1 ?NUMBER2) is true just in case the &%Quantity ?NUMBER1 is less than the &%Quantity ?NUMBER2.")
thf(ax_003,axiom,(
    ! [WOMAN: $i] :
      ( ( instance_THFTYPE_IiioI @ WOMAN @ lWoman_THFTYPE_i )
     => ( attribute_THFTYPE_IiioI @ WOMAN @ lFemale_THFTYPE_i ) ) )).

%KIF documentation:(documentation AsymmetricRelation EnglishLanguage "A &%BinaryRelation is asymmetric if and only if it is both an &%AntisymmetricRelation and an &%IrreflexiveRelation.")
thf(ax_004,axiom,(
    ! [OBJ: $i,PROC: $i] :
      ( ( result_THFTYPE_IiioI @ PROC @ OBJ )
     => ! [TIME: $i] :
          ( ( before_THFTYPE_IiioI @ TIME @ ( lBeginFn_THFTYPE_IiiI @ ( lWhenFn_THFTYPE_IiiI @ PROC ) ) )
         => ( ~ @ ( time_THFTYPE_IiioI @ OBJ @ TIME ) ) ) ) )).

thf(ax_005,axiom,(
    ! [REL2: $i > $i > $o,REL1: $i > $i > $o] :
      ( ( inverse_THFTYPE_IIiioIIiioIoI @ REL1 @ REL2 )
     => ! [INST1: $i,INST2: $i] :
          ( ( REL1 @ INST1 @ INST2 )
        <=> ( REL2 @ INST2 @ INST1 ) ) ) )).

%KIF documentation:(documentation mother EnglishLanguage "The general relationship of motherhood. (&%mother ?CHILD ?MOTHER) means that ?MOTHER is the biological mother of ?CHILD.")
thf(ax_006,axiom,(
    ! [OBJ1: $i,OBJ2: $i] :
      ( ( located_THFTYPE_IiioI @ OBJ1 @ OBJ2 )
     => ! [SUB: $i] :
          ( ( part_THFTYPE_IiioI @ SUB @ OBJ1 )
         => ( located_THFTYPE_IiioI @ SUB @ OBJ2 ) ) ) )).

thf(ax_007,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

%KIF documentation:(documentation disjoint EnglishLanguage "&%Classes are &%disjoint only if they share no instances, i.e. just in case the result of applying &%IntersectionFn to them is empty.")
%KIF documentation:(documentation MultiplicationFn EnglishLanguage "If ?NUMBER1 and ?NUMBER2 are &%Numbers, then (&%MultiplicationFn ?NUMBER1 ?NUMBER2) is the arithmetical product of these numbers.")
thf(ax_008,axiom,(
    ! [ATTR2: $i,OBJ1: $i,ROW: $i,OBJ2: $i,ATTR1: $i] :
      ( ( ( orientation_THFTYPE_IiiioI @ OBJ1 @ OBJ2 @ ATTR1 )
        & ( contraryAttribute_THFTYPE_IioI @ ROW )
        & ( inList_THFTYPE_IiioI @ ATTR1 @ ( lListFn_THFTYPE_IiiI @ ROW ) )
        & ( inList_THFTYPE_IiioI @ ATTR2 @ ( lListFn_THFTYPE_IiiI @ ROW ) )
        & ( ~ @ ( ATTR1 = ATTR2 ) ) )
     => ( ~ @ ( orientation_THFTYPE_IiiioI @ OBJ1 @ OBJ2 @ ATTR2 ) ) ) )).

%KIF documentation:(documentation Man EnglishLanguage "The class of &%Male &%Humans.")
%KIF documentation:(documentation TimeInterval EnglishLanguage "An interval of time. Note that a &%TimeInterval has both an extent and a location on the universal timeline. Note too that a &%TimeInterval has no gaps, i.e. this class contains only convex time intervals.")
%KIF documentation:(documentation hasPurposeForAgent EnglishLanguage "Expresses a cognitive attitude of an agent with respect to a particular instance of Physical. More precisely, (&%hasPurposeForAgent ?THING ?FORMULA ?AGENT) means that the purpose of ?THING for ?AGENT is the proposition expressed by ?FORMULA. Very complex issues are involved here. In particular, the rules of inference of the first order predicate calculus are not truth-preserving for the second argument position of this &%Predicate.")
thf(ax_009,axiom,
    ( subclass_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_010,axiom,(
    ! [CLASS: $i,ATTR2: $i,ATTR1: $i] :
      ( ( ( subAttribute_THFTYPE_IiioI @ ATTR1 @ ATTR2 )
        & ( instance_THFTYPE_IiioI @ ATTR2 @ CLASS ) )
     => ( instance_THFTYPE_IiioI @ ATTR1 @ CLASS ) ) )).

%KIF documentation:(documentation Relation EnglishLanguage "The &%Class of relations. There are three kinds of &%Relation: &%Predicate, &%Function, and &%List. &%Predicates and &%Functions both denote sets of ordered n-tuples. The difference between these two &%Classes is that &%Predicates cover formula-forming operators, while &%Functions cover term-forming operators. A &%List, on the other hand, is a particular ordered n-tuple.")
thf(ax_011,axiom,(
    ! [CHILD: $i,PARENT: $i] :
      ( ( ( parent_THFTYPE_IiioI @ CHILD @ PARENT )
        & ( attribute_THFTYPE_IiioI @ PARENT @ lFemale_THFTYPE_i ) )
     => ( mother_THFTYPE_IiioI @ CHILD @ PARENT ) ) )).

thf(ax_012,axiom,
    ( subclass_THFTYPE_IiioI @ lReproductiveBody_THFTYPE_i @ lBodyPart_THFTYPE_i )).

thf(ax_013,axiom,(
    ! [THING: $i,POS: $i] :
      ( ( temporalPart_THFTYPE_IiioI @ POS @ ( lWhenFn_THFTYPE_IiiI @ THING ) )
    <=> ( time_THFTYPE_IiioI @ THING @ POS ) ) )).

thf(ax_014,axiom,(
    ! [NUMBER: $i,CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( domain_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS1 )
        & ( domain_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_015,axiom,(
    ! [REL: $i > $i > $o] :
      ( ( instance_THFTYPE_IIiioIioI @ REL @ lTransitiveRelation_THFTYPE_i )
    <=> ! [INST1: $i,INST2: $i,INST3: $i] :
          ( ( ( REL @ INST1 @ INST2 )
            & ( REL @ INST2 @ INST3 ) )
         => ( REL @ INST1 @ INST3 ) ) ) )).

thf(ax_016,axiom,
    ( subclass_THFTYPE_IiioI @ lProcess_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_017,axiom,(
    ! [NUMBER2: $i,NUMBER1: $i] :
      ( ( gtet_THFTYPE_IiioI @ NUMBER1 @ NUMBER2 )
    <=> ( ( NUMBER1 = NUMBER2 )
        | ( gt_THFTYPE_IiioI @ NUMBER1 @ NUMBER2 ) ) ) )).

thf(ax_018,axiom,(
    ! [AGENT: $i,PROC: $i] :
      ( ( ( instance_THFTYPE_IiioI @ PROC @ lIntentionalProcess_THFTYPE_i )
        & ( agent_THFTYPE_IiioI @ PROC @ AGENT ) )
     => ? [PURP: $o] :
          ( hasPurposeForAgent_THFTYPE_IioioI @ PROC @ PURP @ AGENT ) ) )).

%KIF documentation:(documentation Physical EnglishLanguage "An entity that has a location in space-time. Note that locations are themselves understood to have a location in space-time.")
%KIF documentation:(documentation member EnglishLanguage "A specialized common sense notion of part for uniform parts of &%Collections. For example, each sheep in a flock of sheep would have the relationship of member to the flock.")
thf(ax_019,axiom,
    ( subclass_THFTYPE_IiioI @ lUnaryFunction_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

thf(ax_020,axiom,
    ( subclass_THFTYPE_IiioI @ lHuman_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

%KIF documentation:(documentation BinaryRelation EnglishLanguage "&%BinaryRelations are relations that are true only of pairs of things. &%BinaryRelations are represented as slots in frame systems.")
thf(ax_021,axiom,
    ( subclass_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_022,axiom,(
    ! [OBJ: $i,AGENT: $i] :
      ( ( wants_THFTYPE_IiioI @ AGENT @ OBJ )
     => ? [PURP: $o] :
          ( hasPurposeForAgent_THFTYPE_IioioI @ OBJ @ PURP @ AGENT ) ) )).

thf(ax_023,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation holdsDuring EnglishLanguage "(&%holdsDuring ?TIME ?FORMULA) means that the proposition denoted by ?FORMULA is true in the time frame ?TIME. Note that this implies that ?FORMULA is true at every &%TimePoint which is a &%temporalPart of ?TIME.")
%KIF documentation:(documentation attribute EnglishLanguage "(&%attribute ?OBJECT ?PROPERTY) means that ?PROPERTY is a &%Attribute of ?OBJECT. For example, (&%attribute &%MyLittleRedWagon &%Red).")
thf(ax_024,axiom,
    ( subclass_THFTYPE_IiioI @ lSingleValuedRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation greaterThan EnglishLanguage "(&%greaterThan ?NUMBER1 ?NUMBER2) is true just in case the &%Quantity ?NUMBER1 is greater than the &%Quantity ?NUMBER2.")
thf(ax_025,axiom,(
    ! [FORMULA: $i,AGENT: $i,REL: $i > $i > $o] :
      ( ( ( instance_THFTYPE_IIiioIioI @ REL @ lPropositionalAttitude_THFTYPE_i )
        & ( REL @ AGENT @ FORMULA ) )
     => ( instance_THFTYPE_IiioI @ FORMULA @ lFormula_THFTYPE_i ) ) )).

thf(ax_026,axiom,(
    ! [SITUATION: $o,TIME2: $i,TIME1: $i] :
      ( ( ( holdsDuring_THFTYPE_IiooI @ TIME1 @ SITUATION )
        & ( temporalPart_THFTYPE_IiioI @ TIME2 @ TIME1 ) )
     => ( holdsDuring_THFTYPE_IiooI @ TIME2 @ SITUATION ) ) )).

%KIF documentation:(documentation Formula EnglishLanguage "A syntactically well-formed formula in the SUO-KIF knowledge representation language.")
%KIF documentation:(documentation Woman EnglishLanguage "The class of &%Female &%Humans.")
thf(ax_027,axiom,
    ( subclass_THFTYPE_IiioI @ lMan_THFTYPE_i @ lHuman_THFTYPE_i )).

thf(ax_028,axiom,(
    ! [ORG: $i] :
      ( ( instance_THFTYPE_IiioI @ ORG @ lOrganization_THFTYPE_i )
     => ? [PURP: $o] :
        ! [MEMBER: $i] :
          ( ( member_THFTYPE_IiioI @ MEMBER @ ORG )
         => ( hasPurpose_THFTYPE_IiooI @ MEMBER @ PURP ) ) ) )).

thf(ax_029,axiom,(
    ! [REL: $i > $i > $o] :
      ( ( instance_THFTYPE_IIiioIioI @ REL @ lIrreflexiveRelation_THFTYPE_i )
    <=> ! [INST: $i] :
          ( ~ @ ( REL @ INST @ INST ) ) ) )).

thf(ax_030,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryFunction_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_031,axiom,(
    ! [NUMBER: $i,PRED1: $i,CLASS1: $i,PRED2: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ PRED1 @ PRED2 )
        & ( domain_THFTYPE_IiiioI @ PRED2 @ NUMBER @ CLASS1 ) )
     => ( domain_THFTYPE_IiiioI @ PRED1 @ NUMBER @ CLASS1 ) ) )).

%KIF documentation:(documentation domainSubclass EnglishLanguage "&%Predicate used to specify argument type restrictions of &%Predicates. The formula (&%domainSubclass ?REL ?INT ?CLASS) means that the ?INT'th element of each tuple in the relation ?REL must be a subclass of ?CLASS.")
thf(ax_032,axiom,
    ( subclass_THFTYPE_IiioI @ lTotalValuedRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

%KIF documentation:(documentation located EnglishLanguage "(&%located ?PHYS ?OBJ) means that ?PHYS is &%partlyLocated at ?OBJ, and there is no &%part or &%subProcess of ?PHYS that is not &%located at ?OBJ.")
thf(ax_033,axiom,(
    ! [CHILD: $i,PARENT: $i] :
      ( ( parent_THFTYPE_IiioI @ CHILD @ PARENT )
     => ( before_THFTYPE_IiioI @ ( lBeginFn_THFTYPE_IiiI @ ( lWhenFn_THFTYPE_IiiI @ PARENT ) ) @ ( lBeginFn_THFTYPE_IiiI @ ( lWhenFn_THFTYPE_IiiI @ CHILD ) ) ) ) )).

thf(ax_034,axiom,
    ( subclass_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_035,axiom,
    ( subclass_THFTYPE_IiioI @ lObject_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_036,axiom,
    ( subclass_THFTYPE_IiioI @ lSelfConnectedObject_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_037,axiom,(
    ! [CLASS1: $i,CLASS2: $i] :
      ( ( CLASS1 = CLASS2 )
     => ! [THING: $i] :
          ( ( instance_THFTYPE_IiioI @ THING @ CLASS1 )
        <=> ( instance_THFTYPE_IiioI @ THING @ CLASS2 ) ) ) )).

%KIF documentation:(documentation TotalValuedRelation EnglishLanguage "A &%Relation is a &%TotalValuedRelation just in case there exists an assignment for the last argument position of the &%Relation given any assignment of values to every argument position except the last one. Note that declaring a &%Relation to be both a &%TotalValuedRelation and a &%SingleValuedRelation means that it is a total function.")
thf(ax_038,axiom,(
    ! [REL: $i > $i > $o,INTERVAL: $i,INST2: $i,INST1: $i] :
      ( ( ( holdsDuring_THFTYPE_IiooI @ INTERVAL @ ( REL @ INST1 @ INST2 ) )
        & ( instance_THFTYPE_IiioI @ INST1 @ lPhysical_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ INST2 @ lPhysical_THFTYPE_i ) )
     => ( ( time_THFTYPE_IiioI @ INST1 @ INTERVAL )
        & ( time_THFTYPE_IiioI @ INST2 @ INTERVAL ) ) ) )).

%KIF documentation:(documentation inverse EnglishLanguage "The inverse of a &%BinaryRelation is a relation in which all the tuples of the original relation are reversed. In other words, one &%BinaryRelation is the inverse of another if they are equivalent when their arguments are swapped.")
thf(ax_039,axiom,
    ( partition_THFTYPE_IiiioI @ lHuman_THFTYPE_i @ lMan_THFTYPE_i @ lWoman_THFTYPE_i )).

thf(ax_040,axiom,
    ( partition_THFTYPE_IiiioI @ lTimePosition_THFTYPE_i @ lTimeInterval_THFTYPE_i @ lTimePoint_THFTYPE_i )).

thf(ax_041,axiom,(
    ! [FORMULA: $o,AGENT: $i] :
      ( ( knows_THFTYPE_IiooI @ AGENT @ FORMULA )
     => ( believes_THFTYPE_IiooI @ AGENT @ FORMULA ) ) )).

thf(ax_042,axiom,(
    ! [ORG: $i,AGENT: $i] :
      ( ( ( instance_THFTYPE_IiioI @ ORG @ lOrganization_THFTYPE_i )
        & ( member_THFTYPE_IiioI @ AGENT @ ORG ) )
     => ( instance_THFTYPE_IiioI @ AGENT @ lAgent_THFTYPE_i ) ) )).

thf(ax_043,axiom,(
    ! [THING2: $i,THING1: $i] :
      ( ( THING1 = THING2 )
     => ! [CLASS: $i] :
          ( ( instance_THFTYPE_IiioI @ THING1 @ CLASS )
        <=> ( instance_THFTYPE_IiioI @ THING2 @ CLASS ) ) ) )).

thf(ax_044,axiom,
    ( subclass_THFTYPE_IiioI @ lOrganism_THFTYPE_i @ lAgent_THFTYPE_i )).

%KIF documentation:(documentation father EnglishLanguage "The general relationship of fatherhood. (&%father ?CHILD ?FATHER) means that ?FATHER is the biological father of ?CHILD.")
%KIF documentation:(documentation orientation EnglishLanguage "A general &%Predicate for indicating how two &%Objects are oriented with respect to one another. For example, (orientation ?OBJ1 ?OBJ2 North) means that ?OBJ1 is north of ?OBJ2, and (orientation ?OBJ1 ?OBJ2 Vertical) means that ?OBJ1 is positioned vertically with respect to ?OBJ2.")
%KIF documentation:(documentation UnaryFunction EnglishLanguage "The &%Class of &%Functions that require a single argument.")
thf(ax_045,axiom,
    ( subclass_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

thf(ax_046,axiom,(
    ! [SUBPROC: $i,PROC: $i] :
      ( ( subProcess_THFTYPE_IiioI @ SUBPROC @ PROC )
     => ( temporalPart_THFTYPE_IiioI @ ( lWhenFn_THFTYPE_IiiI @ SUBPROC ) @ ( lWhenFn_THFTYPE_IiiI @ PROC ) ) ) )).

%KIF documentation:(documentation greaterThanOrEqualTo EnglishLanguage "(&%greaterThanOrEqualTo ?NUMBER1 ?NUMBER2) is true just in case the &%Quantity ?NUMBER1 is greater than the &%Quantity ?NUMBER2.")
thf(ax_047,axiom,(
    ! [FATHER: $i,CHILD: $i] :
      ( ( father_THFTYPE_IiioI @ CHILD @ FATHER )
     => ( attribute_THFTYPE_IiioI @ FATHER @ lMale_THFTYPE_i ) ) )).

%KIF documentation:(documentation connected EnglishLanguage "(connected ?OBJ1 ?OBJ2) means that ?OBJ1 &%meetsSpatially ?OBJ2 or that ?OBJ1 &%overlapsSpatially ?OBJ2.")
thf(ax_048,axiom,(
    ! [INTERVAL1: $i,INTERVAL2: $i] :
      ( ( ( ( lBeginFn_THFTYPE_IiiI @ INTERVAL1 )
          = ( lBeginFn_THFTYPE_IiiI @ INTERVAL2 ) )
        & ( ( lEndFn_THFTYPE_IiiI @ INTERVAL1 )
          = ( lEndFn_THFTYPE_IiiI @ INTERVAL2 ) ) )
     => ( INTERVAL1 = INTERVAL2 ) ) )).

thf(ax_049,axiom,(
    ! [REL2: $i,CLASS1: $i,REL1: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ REL1 @ REL2 )
        & ( range_THFTYPE_IiioI @ REL2 @ CLASS1 ) )
     => ( range_THFTYPE_IiioI @ REL1 @ CLASS1 ) ) )).

%KIF documentation:(documentation subclass EnglishLanguage "(&%subclass ?CLASS1 ?CLASS2) means that ?CLASS1 is a subclass of ?CLASS2, i.e. every instance of ?CLASS1 is also an instance of ?CLASS2. A class may have multiple superclasses and subclasses.")
%KIF documentation:(documentation BinaryFunction EnglishLanguage "The &%Class of &%Functions that require two arguments.")
thf(ax_050,axiom,
    ( subclass_THFTYPE_IiioI @ lTimeInterval_THFTYPE_i @ lTimePosition_THFTYPE_i )).

%KIF documentation:(documentation Entity EnglishLanguage "The universal class of individuals. This is the root node of the ontology.")
%KIF documentation:(documentation WhenFn EnglishLanguage "A &%UnaryFunction that maps an &%Object or &%Process to the exact &%TimeInterval during which it exists. Note that, for every &%TimePoint ?TIME outside of the &%TimeInterval (WhenFn ?THING), (time ?THING ?TIME) does not hold.")
thf(ax_051,axiom,
    ( subclass_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_052,axiom,
    ( range_THFTYPE_IiioI @ lBeginFn_THFTYPE_i @ lTimePoint_THFTYPE_i )).

%KIF documentation:(documentation TimePosition EnglishLanguage "Any &%TimePoint or &%TimeInterval along the universal timeline from &%NegativeInfinity to &%PositiveInfinity.")
thf(ax_053,axiom,(
    ! [PROC: $i] :
      ( ( instance_THFTYPE_IiioI @ PROC @ lIntentionalProcess_THFTYPE_i )
     => ? [AGENT: $i] :
          ( ( instance_THFTYPE_IiioI @ AGENT @ lCognitiveAgent_THFTYPE_i )
          & ( agent_THFTYPE_IiioI @ PROC @ AGENT ) ) ) )).

thf(ax_054,axiom,
    ( subclass_THFTYPE_IiioI @ lAgent_THFTYPE_i @ lObject_THFTYPE_i )).

%KIF documentation:(documentation agent EnglishLanguage "(&%agent ?PROCESS ?AGENT) means that ?AGENT is an active determinant, either animate or inanimate, of the &%Process ?PROCESS, with or without voluntary intention. For example, Eve is an &%agent in the following proposition: Eve bit an apple.")
thf(ax_055,axiom,(
    ! [SUBPROC: $i,PROC: $i] :
      ( ( subProcess_THFTYPE_IiioI @ SUBPROC @ PROC )
     => ! [REGION: $i] :
          ( ( located_THFTYPE_IiioI @ PROC @ REGION )
         => ( located_THFTYPE_IiioI @ SUBPROC @ REGION ) ) ) )).

%KIF documentation:(documentation ReproductiveBody EnglishLanguage "Reproductive structure of &%Organisms. Consists of an &%Embryonic &%Object and a nutritive/ protective envelope. Note that this class includes seeds, spores, and &%FruitOrVegetables, as well as the eggs produced by &%Animals.")
%KIF documentation:(documentation patient EnglishLanguage "(&%patient ?PROCESS ?ENTITY) means that ?ENTITY is a participant in ?PROCESS that may be moved, said, experienced, etc. For example, the direct objects in the sentences 'The cat swallowed the canary' and 'Billy likes the beer' would be examples of &%patients. Note that the &%patient of a &%Process may or may not undergo structural change as a result of the &%Process. The &%CaseRole of &%patient is used when one wants to specify as broadly as possible the object of a &%Process.")
%KIF documentation:(documentation believes EnglishLanguage "The epistemic predicate of belief. (&%believes ?AGENT ?FORMULA) means that ?AGENT believes the proposition expressed by ?FORMULA.")
thf(ax_056,axiom,(
    ! [SUBPROC: $i,PROC: $i] :
      ( ( ( instance_THFTYPE_IiioI @ PROC @ lProcess_THFTYPE_i )
        & ( subProcess_THFTYPE_IiioI @ SUBPROC @ PROC ) )
     => ? [TIME: $i] :
          ( time_THFTYPE_IiioI @ SUBPROC @ TIME ) ) )).

thf(ax_057,axiom,(
    ! [MAN: $i] :
      ( ( instance_THFTYPE_IiioI @ MAN @ lMan_THFTYPE_i )
     => ( attribute_THFTYPE_IiioI @ MAN @ lMale_THFTYPE_i ) ) )).

%KIF documentation:(documentation result EnglishLanguage "(result ?ACTION ?OUTPUT) means that ?OUTPUT is a product of ?ACTION. For example, house is a &%result in the following proposition: Eric built a house.")
thf(ax_058,axiom,(
    ! [FORMULA: $o,AGENT: $i] :
      ( ( believes_THFTYPE_IiooI @ AGENT @ FORMULA )
     => ? [TIME: $i] :
          ( holdsDuring_THFTYPE_IiooI @ TIME @ ( considers_THFTYPE_IiooI @ AGENT @ FORMULA ) ) ) )).

%KIF documentation:(documentation hasPurpose EnglishLanguage "This &%Predicate expresses the concept of a conventional goal, i.e. a goal with a neutralized agent's intention. Accordingly, (&%hasPurpose ?THING ?FORMULA) means that the instance of &%Physical ?THING has, as its purpose, the &%Proposition expressed by ?FORMULA. Note that there is an important difference in meaning between the &%Predicates &%hasPurpose and &%result. Although the second argument of the latter can satisfy the second argument of the former, a conventional goal is an expected and desired outcome, while a result may be neither expected nor desired. For example, a machine process may have outcomes but no goals, aimless wandering may have an outcome but no goal, a learning process may have goals with no outcomes, and so on.")
thf(ax_059,axiom,(
    ! [REL: $i > $i > $o] :
      ( ( instance_THFTYPE_IIiioIioI @ REL @ lSymmetricRelation_THFTYPE_i )
    <=> ! [INST1: $i,INST2: $i] :
          ( ( REL @ INST1 @ INST2 )
         => ( REL @ INST2 @ INST1 ) ) ) )).

thf(ax_060,axiom,
    ( subclass_THFTYPE_IiioI @ lPhysical_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_061,axiom,(
    ! [OBJ: $i,AGENT: $i] :
      ( ( wants_THFTYPE_IiioI @ AGENT @ OBJ )
     => ( desires_THFTYPE_IiooI @ AGENT @ ( possesses_THFTYPE_IiioI @ AGENT @ OBJ ) ) ) )).

thf(ax_062,axiom,
    ( range_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lQuantity_THFTYPE_i )).

%KIF documentation:(documentation temporalPart EnglishLanguage "The temporal analogue of the spatial &%part predicate. (&%temporalPart ?POS1 ?POS2) means that &%TimePosition ?POS1 is part of &%TimePosition ?POS2. Note that since &%temporalPart is a &%ReflexiveRelation every &%TimePostion is a &%temporalPart of itself.")
%KIF documentation:(documentation subrelation EnglishLanguage "(&%subrelation ?REL1 ?REL2) means that every tuple of ?REL1 is also a tuple of ?REL2. In other words, if the &%Relation ?REL1 holds for some arguments arg_1, arg_2, ... arg_n, then the &%Relation ?REL2 holds for the same arguments. A consequence of this is that a &%Relation and its subrelations must have the same &%valence.")
%KIF documentation:(documentation spouse EnglishLanguage "The relationship of marriage between two &%Humans.")
%KIF documentation:(documentation EndFn EnglishLanguage "A &%UnaryFunction that maps a &%TimeInterval to the &%TimePoint at which the interval ends.")
thf(ax_063,axiom,(
    ! [BODY: $i,ORG: $i] :
      ( ( ( instance_THFTYPE_IiioI @ BODY @ lReproductiveBody_THFTYPE_i )
        & ( part_THFTYPE_IiioI @ BODY @ ORG )
        & ( instance_THFTYPE_IiioI @ ORG @ lOrganism_THFTYPE_i ) )
     => ( attribute_THFTYPE_IiioI @ ORG @ lFemale_THFTYPE_i ) ) )).

%KIF documentation:(documentation considers EnglishLanguage "(&%considers ?AGENT ?FORMULA) means that ?AGENT considers or wonders about the truth of the proposition expressed by ?FORMULA.")
thf(ax_064,axiom,(
    ! [INTERVAL: $i] :
      ( ( instance_THFTYPE_IiioI @ INTERVAL @ lTimeInterval_THFTYPE_i )
     => ? [POINT: $i] :
          ( ( instance_THFTYPE_IiioI @ POINT @ lTimePoint_THFTYPE_i )
          & ( temporalPart_THFTYPE_IiioI @ POINT @ INTERVAL ) ) ) )).

thf(ax_065,axiom,
    ( subclass_THFTYPE_IiioI @ lUnaryFunction_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation partition EnglishLanguage "A &%partition of a class C is a set of mutually &%disjoint classes (a subclass partition) which covers C. Every instance of C is an instance of exactly one of the subclasses in the partition.")
%KIF documentation:(documentation lessThanOrEqualTo EnglishLanguage "(&%lessThanOrEqualTo ?NUMBER1 ?NUMBER2) is true just in case the &%Quantity ?NUMBER1 is less than or equal to the &%Quantity ?NUMBER2.")
%KIF documentation:(documentation InheritableRelation EnglishLanguage "The class of &%Relations whose properties can be inherited downward in the class hierarchy via the &%subrelation &%Predicate.")
%KIF documentation:(documentation husband EnglishLanguage "(&%husband ?MAN ?WOMAN) means that ?MAN is the husband of ?WOMAN.")
%KIF documentation:(documentation IntentionalProcess EnglishLanguage "A &%Process that has a specific purpose for the &%CognitiveAgent who performs it.")
thf(ax_066,axiom,(
    ! [CLASS: $i,PRED1: $i,PRED2: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ PRED1 @ PRED2 )
        & ( instance_THFTYPE_IiioI @ PRED2 @ CLASS )
        & ( subclass_THFTYPE_IiioI @ CLASS @ lInheritableRelation_THFTYPE_i ) )
     => ( instance_THFTYPE_IiioI @ PRED1 @ CLASS ) ) )).

thf(ax_067,axiom,(
    ! [PURPOSE: $o,THING: $i] :
      ( ( hasPurpose_THFTYPE_IiooI @ THING @ PURPOSE )
     => ? [AGENT: $i] :
          ( hasPurposeForAgent_THFTYPE_IioioI @ THING @ PURPOSE @ AGENT ) ) )).

%KIF documentation:(documentation Object EnglishLanguage "Corresponds roughly to the class of ordinary objects. Examples include normal physical objects, geographical regions, and locations of &%Processes, the complement of &%Objects in the &%Physical class. In a 4D ontology, an &%Object is something whose spatiotemporal extent is thought of as dividing into spatial parts roughly parallel to the time-axis.")
thf(ax_068,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_069,axiom,
    ( subclass_THFTYPE_IiioI @ lInheritableRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_070,axiom,(
    ! [NUMBER2: $i,NUMBER1: $i] :
      ( ( ltet_THFTYPE_IiioI @ NUMBER1 @ NUMBER2 )
    <=> ( ( NUMBER1 = NUMBER2 )
        | ( lt_THFTYPE_IiioI @ NUMBER1 @ NUMBER2 ) ) ) )).

%KIF documentation:(documentation Process EnglishLanguage "The class of things that happen and have temporal parts or stages. Examples include extended events like a football match or a race, actions like &%Pursuing and &%Reading, and biological processes. The formal definition is: anything that occurs in time but is not an &%Object. Note that a &%Process may have participants 'inside' it which are &%Objects, such as the players in a football match. In a 4D ontology, a &%Process is something whose spatiotemporal extent is thought of as dividing into temporal stages roughly perpendicular to the time-axis.")
%KIF documentation:(documentation TemporalRelation EnglishLanguage "The &%Class of temporal &%Relations. This &%Class includes notions of (temporal) topology of intervals, (temporal) schemata, and (temporal) extension.")
thf(ax_071,axiom,
    ( subclass_THFTYPE_IiioI @ lTimePoint_THFTYPE_i @ lTimePosition_THFTYPE_i )).

%KIF documentation:(documentation time EnglishLanguage "This relation holds between an instance of &%Physical and an instance of &%TimePosition just in case the temporal lifespan of the former includes the latter. In other words, (&%time ?THING ?TIME) means that ?THING existed or occurred at ?TIME. Note that &%time does for instances of &%Physical what &%holdsDuring does for instances of &%Formula. The constants &%located and &%time are the basic spatial and temporal predicates, respectively.")
%KIF documentation:(documentation desires EnglishLanguage "(&%desires ?AGENT ?FORMULA) means that ?AGENT wants to bring about the state of affairs expressed by ?FORMULA. Note that there is no implication that what is desired by the agent is not already true. Note too that &%desires is distinguished from &%wants only in that the former is a &%PropositionalAttitude, while &%wants is an &%ObjectAttitude.")
thf(ax_072,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_073,axiom,(
    ! [THING: $i] :
      ( instance_THFTYPE_IiioI @ THING @ lEntity_THFTYPE_i ) )).

thf(ax_074,axiom,(
    ! [INTERVAL: $i] :
      ( ( instance_THFTYPE_IiioI @ INTERVAL @ lTimeInterval_THFTYPE_i )
     => ( before_THFTYPE_IiioI @ ( lBeginFn_THFTYPE_IiiI @ INTERVAL ) @ ( lEndFn_THFTYPE_IiiI @ INTERVAL ) ) ) )).

thf(ax_075,axiom,
    ( range_THFTYPE_IiioI @ lEndFn_THFTYPE_i @ lTimePoint_THFTYPE_i )).

%KIF documentation:(documentation domain EnglishLanguage "Provides a computationally and heuristically convenient mechanism for declaring the argument types of a given relation. The formula (&%domain ?REL ?INT ?CLASS) means that the ?INT'th element of each tuple in the relation ?REL must be an instance of ?CLASS. Specifying argument types is very helpful in maintaining ontologies. Representation systems can use these specifications to classify terms and check integrity constraints. If the restriction on the argument type of a &%Relation is not captured by a &%SetOrClass already defined in the ontology, one can specify a &%SetOrClass compositionally with the functions &%UnionFn, &%IntersectionFn, etc.")
thf(ax_076,axiom,(
    ! [AGENT: $i] :
      ( ( instance_THFTYPE_IiioI @ AGENT @ lAgent_THFTYPE_i )
    <=> ? [PROC: $i] :
          ( agent_THFTYPE_IiioI @ PROC @ AGENT ) ) )).

%KIF documentation:(documentation TernaryPredicate EnglishLanguage "The &%Class of &%Predicates that require exactly three arguments.")
thf(ax_077,axiom,
    ( subclass_THFTYPE_IiioI @ lTotalValuedRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation RealNumber EnglishLanguage "Any &%Number that can be expressed as a (possibly infinite) decimal, i.e. any &%Number that has a position on the number line.")
thf(ax_078,axiom,(
    ! [NUMBER: $i,CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( domainSubclass_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS1 )
        & ( domainSubclass_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_079,axiom,(
    ! [MOTHER: $i,CHILD: $i] :
      ( ( mother_THFTYPE_IiioI @ CHILD @ MOTHER )
     => ( attribute_THFTYPE_IiioI @ MOTHER @ lFemale_THFTYPE_i ) ) )).

%KIF documentation:(documentation documentation EnglishLanguage "A relation between objects in the domain of discourse and strings of natural language text stated in a particular &%HumanLanguage. The domain of &%documentation is not constants (names), but the objects themselves. This means that one does not quote the names when associating them with their documentation.")
%KIF documentation:(documentation Organization EnglishLanguage "An &%Organization is a corporate or similar institution. The &%members of an &%Organization typically have a common purpose or function. Note that this class also covers divisions, departments, etc. of organizations. For example, both the Shell Corporation and the accounting department at Shell would both be instances of &%Organization. Note too that the existence of an &%Organization is dependent on the existence of at least one &%member (since &%Organization is a subclass of &%Collection). Accordingly, in cases of purely legal organizations, a fictitious &%member should be assumed.")
thf(ax_080,axiom,
    ( subclass_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_081,axiom,
    ( subclass_THFTYPE_IiioI @ lIntentionalProcess_THFTYPE_i @ lProcess_THFTYPE_i )).

%KIF documentation:(documentation PartialOrderingRelation EnglishLanguage "A &%BinaryRelation is a partial ordering if it is a &%ReflexiveRelation, an &%AntisymmetricRelation, and a &%TransitiveRelation.")
%KIF documentation:(documentation SymmetricRelation EnglishLanguage "A &%BinaryRelation ?REL is symmetric just iff (?REL ?INST1 ?INST2) imples (?REL ?INST2 ?INST1), for all ?INST1 and ?INST2.")
thf(ax_082,axiom,(
    ! [POINT: $i,INTERVAL: $i] :
      ( ( ( lBeginFn_THFTYPE_IiiI @ INTERVAL )
        = POINT )
     => ! [OTHERPOINT: $i] :
          ( ( ( temporalPart_THFTYPE_IiioI @ OTHERPOINT @ INTERVAL )
            & ( ~ @ ( OTHERPOINT = POINT ) ) )
         => ( before_THFTYPE_IiioI @ POINT @ OTHERPOINT ) ) ) )).

thf(ax_083,axiom,(
    ! [POINT: $i] :
      ( ( instance_THFTYPE_IiioI @ POINT @ lTimePoint_THFTYPE_i )
     => ? [INTERVAL: $i] :
          ( ( instance_THFTYPE_IiioI @ INTERVAL @ lTimeInterval_THFTYPE_i )
          & ( temporalPart_THFTYPE_IiioI @ POINT @ INTERVAL ) ) ) )).

thf(ax_084,axiom,
    ( contraryAttribute_THFTYPE_IiioI @ lMale_THFTYPE_i @ lFemale_THFTYPE_i )).

%KIF documentation:(documentation RelationExtendedToQuantities EnglishLanguage "A &%RelationExtendedToQuantities is a &%Relation that, when it is true on a sequence of arguments that are &%RealNumbers, it is also true on a sequence of instances of &%ConstantQuantity with those magnitudes in some unit of measure. For example, the &%lessThan relation is extended to quantities. This means that for all pairs of quantities ?QUANTITY1 and ?QUANTITY2, (&%lessThan ?QUANTITY1 ?QUANTITY2) if and only if, for some ?NUMBER1, ?NUMBER2, and ?UNIT, ?QUANTITY1 = (&%MeasureFn ?NUMBER1 ?UNIT), ?QUANTITY2 = (&%MeasureFn ?NUMBER2 ?UNIT), and (&%lessThan ?NUMBER1 ?NUMBER2), for all units ?UNIT on which ?QUANTITY1 and ?QUANTITY2 can be measured. Note that, when a &%RelationExtendedToQuantities is extended from &%RealNumbers to instances of &%ConstantQuantity, the &%ConstantQuantity must be measured along the same physical dimension.")
thf(ax_085,axiom,
    ( subclass_THFTYPE_IiioI @ lTransitiveRelation_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

%KIF documentation:(documentation equal EnglishLanguage "(equal ?ENTITY1 ?ENTITY2) is true just in case ?ENTITY1 is identical with ?ENTITY2.")
%KIF documentation:(documentation ListFn EnglishLanguage "A &%Function that takes any number of arguments and returns the &%List containing those arguments in exactly the same order.")
%KIF documentation:(documentation Female EnglishLanguage "An &%Attribute indicating that an &%Organism is female in nature.")
thf(ax_086,axiom,(
    ! [ORGANISM: $i] :
      ( ( instance_THFTYPE_IiioI @ ORGANISM @ lOrganism_THFTYPE_i )
     => ? [PARENT: $i] :
          ( parent_THFTYPE_IiioI @ ORGANISM @ PARENT ) ) )).

thf(ax_087,axiom,(
    ! [TIME: $i,SITUATION: $o] :
      ( ( holdsDuring_THFTYPE_IiooI @ TIME @ ( ~ @ SITUATION ) )
     => ( ~ @ ( holdsDuring_THFTYPE_IiooI @ TIME @ SITUATION ) ) ) )).

thf(ax_088,axiom,(
    ! [X: $i] :
      ( ~
      @ ? [Z: $i] :
          ( holdsDuring_THFTYPE_IiooI @ Z @ ( considers_THFTYPE_IiooI @ lMax_THFTYPE_i @ ( wife_THFTYPE_IiioI @ X @ lMax_THFTYPE_i ) ) ) ) )).

thf(ax_089,axiom,(
    ! [X: $i] :
      ( ~
      @ ? [Z: $i] :
          ( holdsDuring_THFTYPE_IiooI @ Z @ ( considers_THFTYPE_IiooI @ lMax_THFTYPE_i @ ( wife_THFTYPE_IiioI @ X @ lMax_THFTYPE_i ) ) ) ) )).

thf(ax_090,axiom,(
    ! [POINT: $i,INTERVAL: $i] :
      ( ( ( lEndFn_THFTYPE_IiiI @ INTERVAL )
        = POINT )
     => ! [OTHERPOINT: $i] :
          ( ( ( temporalPart_THFTYPE_IiioI @ OTHERPOINT @ INTERVAL )
            & ( ~ @ ( OTHERPOINT = POINT ) ) )
         => ( before_THFTYPE_IiioI @ OTHERPOINT @ POINT ) ) ) )).

thf(ax_091,axiom,
    ( range_THFTYPE_IiioI @ lWhenFn_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_092,axiom,
    ( subclass_THFTYPE_IiioI @ lSingleValuedRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

%KIF documentation:(documentation Organism EnglishLanguage "Generally, a living individual, including all &%Plants and &%Animals.")
thf(ax_093,axiom,(
    ! [INTERVAL1: $i,INTERVAL2: $i] :
      ( ( meetsTemporally_THFTYPE_IiioI @ INTERVAL1 @ INTERVAL2 )
    <=> ( ( lEndFn_THFTYPE_IiiI @ INTERVAL1 )
        = ( lBeginFn_THFTYPE_IiiI @ INTERVAL2 ) ) ) )).

%KIF documentation:(documentation inList EnglishLanguage "The analog of &%element and &%instance for &%Lists. (&%inList ?OBJ ?LIST) means that ?OBJ is in the &%List ?LIST. For example, (&%inList &%Tuesday (&%ListFn &%Monday &%Tuesday &%Wednesday)) would be true.")
%KIF documentation:(documentation BodyPart EnglishLanguage "A collection of &%Cells and &%Tissues which are localized to a specific area of an &%Organism and which are not pathological. The instances of this &%Class range from gross structures to small components of complex &%Organs.")
thf(ax_094,axiom,
    ( range_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_095,axiom,(
    ? [THING: $i] :
      ( instance_THFTYPE_IiioI @ THING @ lEntity_THFTYPE_i ) )).

thf(ax_096,axiom,
    ( inverse_THFTYPE_IIiioIIiioIoI @ husband_THFTYPE_IiioI @ wife_THFTYPE_IiioI )).

thf(ax_097,axiom,
    ( subclass_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i @ lTransitiveRelation_THFTYPE_i )).

thf(ax_098,axiom,(
    ! [PHYS: $i] :
      ( ( instance_THFTYPE_IiioI @ PHYS @ lPhysical_THFTYPE_i )
    <=> ? [LOC: $i,TIME: $i] :
          ( ( located_THFTYPE_IiioI @ PHYS @ LOC )
          & ( time_THFTYPE_IiioI @ PHYS @ TIME ) ) ) )).

thf(ax_099,axiom,
    ( subclass_THFTYPE_IiioI @ lTernaryPredicate_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation wife EnglishLanguage "(&%wife ?WOMAN ?MAN) means that ?WOMAN is the wife of ?MAN.")
%KIF documentation:(documentation possesses EnglishLanguage "&%Relation that holds between an &%Agent and an &%Object when the &%Agent has ownership of the &%Object.")
%KIF documentation:(documentation IrreflexiveRelation EnglishLanguage "&%Relation ?REL is irreflexive iff (?REL ?INST ?INST) holds for no value of ?INST.")
%KIF documentation:(documentation contraryAttribute EnglishLanguage "A &%contraryAttribute is a set of &%Attributes such that something can not simultaneously have more than one of these &%Attributes. For example, (&%contraryAttribute &%Pliable &%Rigid) means that nothing can be both &%Pliable and &%Rigid.")
%KIF documentation:(documentation TimePoint EnglishLanguage "An extensionless point on the universal timeline. The &%TimePoints at which &%Processes occur can be known with various degrees of precision and approximation, but conceptually &%TimePoints are point-like and not interval-like. That is, it doesn't make sense to talk about how long a &%TimePoint lasts.")
thf(ax_100,axiom,
    ( partition_THFTYPE_IiiioI @ lPhysical_THFTYPE_i @ lObject_THFTYPE_i @ lProcess_THFTYPE_i )).

%KIF documentation:(documentation Quantity EnglishLanguage "Any specification of how many or how much of something there is. Accordingly, there are two subclasses of &%Quantity: &%Number (how many) and &%PhysicalQuantity (how much).")
thf(ax_101,axiom,(
    ! [REL2: $i > $o,ROW: $i,REL1: $i > $o] :
      ( ( ( subrelation_THFTYPE_IIioIIioIoI @ REL1 @ REL2 )
        & ( REL1 @ ROW ) )
     => ( REL2 @ ROW ) ) )).

%KIF documentation:(documentation UnitOfMeasure EnglishLanguage "A standard of measurement for some dimension. For example, the &%Meter is a &%UnitOfMeasure for the dimension of length, as is the &%Inch. There is no intrinsic property of a &%UnitOfMeasure that makes it primitive or fundamental, rather, a system of units (e.g. &%SystemeInternationalUnit) defines a set of orthogonal dimensions and assigns units for each.")
%KIF documentation:(documentation subProcess EnglishLanguage "(&%subProcess ?SUBPROC ?PROC) means that ?SUBPROC is a subprocess of ?PROC. A subprocess is here understood as a temporally distinguished part (proper or not) of a &%Process.")
thf(ax_102,axiom,(
    ! [CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( range_THFTYPE_IiioI @ REL @ CLASS1 )
        & ( range_THFTYPE_IiioI @ REL @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_103,axiom,(
    ! [CLASS1: $i,CLASS2: $i] :
      ( ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
    <=> ! [INST: $i] :
          ( ~
          @ ( ( instance_THFTYPE_IiioI @ INST @ CLASS1 )
            & ( instance_THFTYPE_IiioI @ INST @ CLASS2 ) ) ) ) )).

%KIF documentation:(documentation subAttribute EnglishLanguage "Means that the second argument can be ascribed to everything which has the first argument ascribed to it.")
%KIF documentation:(documentation AdditionFn EnglishLanguage "If ?NUMBER1 and ?NUMBER2 are &%Numbers, then (&%AdditionFn ?NUMBER1 ?NUMBER2) is the arithmetical sum of these numbers.")
%KIF documentation:(documentation relatedInternalConcept EnglishLanguage "Means that the two arguments are related concepts within the SUMO, i.e. there is a significant similarity of meaning between them. To indicate a meaning relation between a SUMO concept and a concept from another source, use the Predicate &%relatedExternalConcept.")
thf(ax_104,axiom,(
    ! [CLASS: $i,CHILD: $i,PARENT: $i] :
      ( ( ( parent_THFTYPE_IiioI @ CHILD @ PARENT )
        & ( subclass_THFTYPE_IiioI @ CLASS @ lOrganism_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ PARENT @ CLASS ) )
     => ( instance_THFTYPE_IiioI @ CHILD @ CLASS ) ) )).

%KIF documentation:(documentation BinaryPredicate EnglishLanguage "A &%Predicate relating two items - its valence is two.")
thf(ax_105,axiom,
    ( subclass_THFTYPE_IiioI @ lOrganization_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

%KIF documentation:(documentation MeasureFn EnglishLanguage "This &%BinaryFunction maps a &%RealNumber and a &%UnitOfMeasure to that &%Number of units. It is used to express `measured' instances of &%PhysicalQuantity. Example: the concept of three meters is represented as (&%MeasureFn 3 &%Meter).")
%KIF documentation:(documentation part EnglishLanguage "The basic mereological relation. All other mereological relations are defined in terms of this one. (&%part ?PART ?WHOLE) simply means that the &%Object ?PART is part of the &%Object ?WHOLE. Note that, since &%part is a &%ReflexiveRelation, every &%Object is a part of itself.")
thf(ax_106,axiom,
    ( subclass_THFTYPE_IiioI @ lWoman_THFTYPE_i @ lHuman_THFTYPE_i )).

%KIF documentation:(documentation CognitiveAgent EnglishLanguage "A &%SentientAgent with responsibilities and the ability to reason, deliberate, make plans, etc. This is essentially the legal/ ethical notion of a person. Note that, although &%Human is a subclass of &%CognitiveAgent, there may be instances of &%CognitiveAgent which are not also instances of &%Human. For example, chimpanzees, gorillas, dolphins, whales, and some extraterrestrials (if they exist) may be &%CognitiveAgents.")
thf(ax_107,axiom,(
    ! [OBJ: $i,TIME: $i,AGENT2: $i,AGENT1: $i] :
      ( ( ( instance_THFTYPE_IiioI @ TIME @ lTimePosition_THFTYPE_i )
        & ( holdsDuring_THFTYPE_IiooI @ TIME @ ( possesses_THFTYPE_IiioI @ AGENT1 @ OBJ ) )
        & ( holdsDuring_THFTYPE_IiooI @ TIME @ ( possesses_THFTYPE_IiioI @ AGENT2 @ OBJ ) ) )
     => ( AGENT1 = AGENT2 ) ) )).

thf(ax_108,axiom,
    ( subclass_THFTYPE_IiioI @ lPropositionalAttitude_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation parent EnglishLanguage "The general relationship of parenthood. (&%parent ?CHILD ?PARENT) means that ?PARENT is a biological parent of ?CHILD.")
thf(ax_109,axiom,(
    ! [REL2: $i,NUMBER: $i,CLASS1: $i,REL1: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ REL1 @ REL2 )
        & ( domainSubclass_THFTYPE_IiiioI @ REL2 @ NUMBER @ CLASS1 ) )
     => ( domainSubclass_THFTYPE_IiiioI @ REL1 @ NUMBER @ CLASS1 ) ) )).

%KIF documentation:(documentation BeginFn EnglishLanguage "A &%UnaryFunction that maps a &%TimeInterval to the &%TimePoint at which the interval begins.")
thf(ax_110,axiom,(
    ! [CHILD: $i,PARENT: $i] :
      ( ( ( parent_THFTYPE_IiioI @ CHILD @ PARENT )
        & ( attribute_THFTYPE_IiioI @ PARENT @ lMale_THFTYPE_i ) )
     => ( father_THFTYPE_IiioI @ CHILD @ PARENT ) ) )).

%KIF documentation:(documentation EnglishLanguage EnglishLanguage "A Germanic language that incorporates many roots from the Romance languages. It is the official language of the &%UnitedStates, the &%UnitedKingdom, and many other countries.")
%KIF documentation:(documentation TransitiveRelation EnglishLanguage "A &%BinaryRelation ?REL is transitive if (?REL ?INST1 ?INST2) and (?REL ?INST2 ?INST3) imply (?REL ?INST1 ?INST3), for all ?INST1, ?INST2, and ?INST3.")
thf(ax_111,axiom,(
    ! [OBJECT: $i,AGENT: $i] :
      ( ? [PROCESS: $i] :
          ( ( instance_THFTYPE_IiioI @ PROCESS @ lIntentionalProcess_THFTYPE_i )
          & ( agent_THFTYPE_IiioI @ PROCESS @ AGENT )
          & ( patient_THFTYPE_IiioI @ PROCESS @ OBJECT ) )
    <=> ( inScopeOfInterest_THFTYPE_IiioI @ AGENT @ OBJECT ) ) )).

%KIF documentation:(documentation knows EnglishLanguage "The epistemic predicate of knowing. (&%knows ?AGENT ?FORMULA) means that ?AGENT knows the proposition expressed by ?FORMULA. Note that &%knows entails conscious awareness, so this &%Predicate cannot be used to express tacit or subconscious or unconscious knowledge.")
%KIF documentation:(documentation SelfConnectedObject EnglishLanguage "A &%SelfConnectedObject is any &%Object that does not consist of two or more disconnected parts.")
%KIF documentation:(documentation PropositionalAttitude EnglishLanguage "The &%Class of &%IntentionalRelations where the &%Agent has awareness of a &%Proposition.")
%KIF documentation:(documentation before EnglishLanguage "(&%before ?POINT1 ?POINT2) means that ?POINT1 precedes ?POINT2 on the universal timeline.")
%KIF documentation:(documentation Male EnglishLanguage "An &%Attribute indicating that an &%Organism is male in nature.")
thf(ax_112,axiom,
    ( inverse_THFTYPE_IIiioIIiioIoI @ greaterThan_THFTYPE_IiioI @ lessThan_THFTYPE_IiioI )).

thf(ax_113,axiom,
    ( inverse_THFTYPE_IIiioIIiioIoI @ greaterThanOrEqualTo_THFTYPE_IiioI @ lessThanOrEqualTo_THFTYPE_IiioI )).

thf(ax_114,axiom,
    ( subclass_THFTYPE_IiioI @ lSymmetricRelation_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

%KIF documentation:(documentation meetsTemporally EnglishLanguage "(&%meetsTemporally ?INTERVAL1 ?INTERVAL2) means that the terminal point of the &%TimeInterval ?INTERVAL1 is the initial point of the &%TimeInterval ?INTERVAL2.")
thf(ax_115,axiom,(
    ! [OBJ: $i,PROCESS: $i] :
      ( ( located_THFTYPE_IiioI @ PROCESS @ OBJ )
     => ! [SUB: $i] :
          ( ( subProcess_THFTYPE_IiioI @ SUB @ PROCESS )
         => ( located_THFTYPE_IiioI @ SUB @ OBJ ) ) ) )).

%KIF documentation:(documentation Human EnglishLanguage "Modern man, the only remaining species of the Homo genus.")
%KIF documentation:(documentation inScopeOfInterest EnglishLanguage "A very general &%Predicate. (&%inScopeOfInterest ?AGENT ?ENTITY) means that ?ENTITY is within the scope of interest of ?AGENT. Note that the interest indicated can be either positive or negative, i.e. the ?AGENT can have an interest in avoiding or promoting ?ENTITY.")
%KIF documentation:(documentation SingleValuedRelation EnglishLanguage "A &%Relation is a &%SingleValuedRelation just in case an assignment of values to every argument position except the last one determines at most one assignment for the last argument position. Note that not all &%SingleValuedRelations are &%TotalValuedRelations.")
thf(ax_116,axiom,(
    ! [REL: $i > $i > $o,NUMBER2: $i,NUMBER1: $i] :
      ( ( ( instance_THFTYPE_IIiioIioI @ REL @ lRelationExtendedToQuantities_THFTYPE_i )
        & ( instance_THFTYPE_IIiioIioI @ REL @ lBinaryRelation_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ NUMBER1 @ lRealNumber_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ NUMBER2 @ lRealNumber_THFTYPE_i )
        & ( REL @ NUMBER1 @ NUMBER2 ) )
     => ! [UNIT: $i] :
          ( ( instance_THFTYPE_IiioI @ UNIT @ lUnitOfMeasure_THFTYPE_i )
         => ( REL @ ( lMeasureFn_THFTYPE_IiiiI @ NUMBER1 @ UNIT ) @ ( lMeasureFn_THFTYPE_IiiiI @ NUMBER2 @ UNIT ) ) ) ) )).

thf(ax_117,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i )).

thf(ax_118,axiom,
    ( instance_THFTYPE_IiioI @ connected_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_119,axiom,
    ( instance_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_120,axiom,
    ( instance_THFTYPE_IIiioIioI @ range_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_121,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThanOrEqualTo_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_122,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subProcess_THFTYPE_IiioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_123,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThan_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_124,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThanOrEqualTo_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_125,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThanOrEqualTo_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_126,axiom,
    ( domain_THFTYPE_IIiioIiioI @ temporalPart_THFTYPE_IiioI @ n1_THFTYPE_i @ lTimePosition_THFTYPE_i )).

thf(ax_127,axiom,
    ( subrelation_THFTYPE_IIiioIioI @ husband_THFTYPE_IiioI @ spouse_THFTYPE_i )).

thf(ax_128,axiom,
    ( domain_THFTYPE_IIiiiIiioI @ lMeasureFn_THFTYPE_IiiiI @ n2_THFTYPE_i @ lUnitOfMeasure_THFTYPE_i )).

thf(ax_129,axiom,
    ( domain_THFTYPE_IIiioIiioI @ husband_THFTYPE_IiioI @ n2_THFTYPE_i @ lWoman_THFTYPE_i )).

thf(ax_130,axiom,
    ( instance_THFTYPE_IiioI @ relatedInternalConcept_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_131,axiom,
    ( instance_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_132,axiom,
    ( instance_THFTYPE_IIiooIioI @ believes_THFTYPE_IiooI @ lPropositionalAttitude_THFTYPE_i )).

thf(ax_133,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThan_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_134,axiom,
    ( domain_THFTYPE_IIiooIiioI @ desires_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_135,axiom,
    ( domain_THFTYPE_IIiioIiioI @ greaterThan_THFTYPE_IiioI @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_136,axiom,
    ( instance_THFTYPE_IIiioIioI @ attribute_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_137,axiom,
    ( domain_THFTYPE_IIiooIiioI @ hasPurpose_THFTYPE_IiooI @ n1_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_138,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_139,axiom,
    ( subrelation_THFTYPE_IIiooIIiioIoI @ considers_THFTYPE_IiooI @ inScopeOfInterest_THFTYPE_IiioI )).

thf(ax_140,axiom,
    ( domain_THFTYPE_IIiioIiioI @ greaterThanOrEqualTo_THFTYPE_IiioI @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_141,axiom,
    ( domain_THFTYPE_IiiioI @ lMultiplicationFn_THFTYPE_i @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_142,axiom,
    ( domain_THFTYPE_IIiioIiioI @ patient_THFTYPE_IiioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_143,axiom,
    ( domain_THFTYPE_IIiooIiioI @ believes_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_144,axiom,
    ( instance_THFTYPE_IIiioIioI @ parent_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_145,axiom,
    ( domain_THFTYPE_IIiioIiioI @ mother_THFTYPE_IiioI @ n2_THFTYPE_i @ lOrganism_THFTYPE_i )).

thf(ax_146,axiom,
    ( instance_THFTYPE_IIiioIioI @ time_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i )).

thf(ax_147,axiom,
    ( instance_THFTYPE_IIiioIioI @ temporalPart_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_148,axiom,
    ( domain_THFTYPE_IIiioIiioI @ member_THFTYPE_IiioI @ n1_THFTYPE_i @ lSelfConnectedObject_THFTYPE_i )).

thf(ax_149,axiom,
    ( domain_THFTYPE_IIiioIiioI @ patient_THFTYPE_IiioI @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_150,axiom,
    ( domain_THFTYPE_IIIiioIIiioIoIiioI @ inverse_THFTYPE_IIiioIIiioIoI @ n1_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

thf(ax_151,axiom,
    ( instance_THFTYPE_IIiioIioI @ part_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_152,axiom,
    ( instance_THFTYPE_IiioI @ documentation_THFTYPE_i @ lTernaryPredicate_THFTYPE_i )).

thf(ax_153,axiom,
    ( instance_THFTYPE_IIiioIioI @ inList_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_154,axiom,
    ( subrelation_THFTYPE_IIiooIIiioIoI @ knows_THFTYPE_IiooI @ inScopeOfInterest_THFTYPE_IiioI )).

thf(ax_155,axiom,
    ( instance_THFTYPE_IiioI @ spouse_THFTYPE_i @ lSymmetricRelation_THFTYPE_i )).

thf(ax_156,axiom,
    ( domain_THFTYPE_IIIiioIIiioIoIiioI @ inverse_THFTYPE_IIiioIIiioIoI @ n2_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

thf(ax_157,axiom,
    ( domain_THFTYPE_IIiioIiioI @ greaterThan_THFTYPE_IiioI @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_158,axiom,
    ( subrelation_THFTYPE_IIiioIIiioIoI @ member_THFTYPE_IiioI @ part_THFTYPE_IiioI )).

thf(ax_159,axiom,
    ( instance_THFTYPE_IIiioIioI @ subclass_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_160,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_161,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lBeginFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_162,axiom,
    ( instance_THFTYPE_IIiioIioI @ wife_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_163,axiom,
    ( instance_THFTYPE_IIiioIioI @ attribute_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_164,axiom,
    ( instance_THFTYPE_IIiooIioI @ holdsDuring_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_165,axiom,
    ( domain_THFTYPE_IIiioIiioI @ instance_THFTYPE_IiioI @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_166,axiom,
    ( domain_THFTYPE_IIiioIiioI @ lessThan_THFTYPE_IiioI @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_167,axiom,
    ( domain_THFTYPE_IIiioIiioI @ part_THFTYPE_IiioI @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_168,axiom,
    ( instance_THFTYPE_IIiioIioI @ temporalPart_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i )).

thf(ax_169,axiom,
    ( instance_THFTYPE_IIIiioIiioIioI @ domain_THFTYPE_IIiioIiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_170,axiom,
    ( instance_THFTYPE_IIioioIioI @ hasPurposeForAgent_THFTYPE_IioioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_171,axiom,
    ( instance_THFTYPE_IIiioIioI @ inList_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_172,axiom,
    ( instance_THFTYPE_IIiioIioI @ subAttribute_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_173,axiom,
    ( instance_THFTYPE_IIiioIioI @ inScopeOfInterest_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_174,axiom,
    ( instance_THFTYPE_IIiioIioI @ temporalPart_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_175,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_176,axiom,
    ( domain_THFTYPE_IIiioIiioI @ parent_THFTYPE_IiioI @ n1_THFTYPE_i @ lOrganism_THFTYPE_i )).

thf(ax_177,axiom,
    ( relatedInternalConcept_THFTYPE_IIiioIIiooIoI @ time_THFTYPE_IiioI @ holdsDuring_THFTYPE_IiooI )).

thf(ax_178,axiom,
    ( domain_THFTYPE_IIiioIiioI @ parent_THFTYPE_IiioI @ n2_THFTYPE_i @ lOrganism_THFTYPE_i )).

thf(ax_179,axiom,
    ( subrelation_THFTYPE_IIiooIIiioIoI @ believes_THFTYPE_IiooI @ inScopeOfInterest_THFTYPE_IiioI )).

thf(ax_180,axiom,
    ( domain_THFTYPE_IIiioIiioI @ before_THFTYPE_IiioI @ n1_THFTYPE_i @ lTimePoint_THFTYPE_i )).

thf(ax_181,axiom,
    ( domain_THFTYPE_IiiioI @ lMultiplicationFn_THFTYPE_i @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_182,axiom,
    ( domain_THFTYPE_IIiioIiioI @ possesses_THFTYPE_IiioI @ n1_THFTYPE_i @ lAgent_THFTYPE_i )).

thf(ax_183,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_184,axiom,
    ( instance_THFTYPE_IIiioIioI @ subclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_185,axiom,
    ( domain_THFTYPE_IIiooIiioI @ considers_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_186,axiom,
    ( instance_THFTYPE_IiioI @ lEndFn_THFTYPE_i @ lUnaryFunction_THFTYPE_i )).

thf(ax_187,axiom,
    ( instance_THFTYPE_IIiooIioI @ believes_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_188,axiom,
    ( instance_THFTYPE_IiioI @ spouse_THFTYPE_i @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_189,axiom,
    ( instance_THFTYPE_IIiiiIioI @ lMeasureFn_THFTYPE_IiiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_190,axiom,
    ( instance_THFTYPE_IIiooIioI @ hasPurpose_THFTYPE_IiooI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_191,axiom,
    ( domain_THFTYPE_IIiioIiioI @ time_THFTYPE_IiioI @ n1_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_192,axiom,
    ( domain_THFTYPE_IIiioIiioI @ before_THFTYPE_IiioI @ n2_THFTYPE_i @ lTimePoint_THFTYPE_i )).

thf(ax_193,axiom,
    ( instance_THFTYPE_IIiioIioI @ member_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_194,axiom,
    ( domain_THFTYPE_IIiioIiioI @ temporalPart_THFTYPE_IiioI @ n2_THFTYPE_i @ lTimePosition_THFTYPE_i )).

thf(ax_195,axiom,
    ( domain_THFTYPE_IIiioIiioI @ agent_THFTYPE_IiioI @ n2_THFTYPE_i @ lAgent_THFTYPE_i )).

thf(ax_196,axiom,
    ( domain_THFTYPE_IIiioIiioI @ meetsTemporally_THFTYPE_IiioI @ n2_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_197,axiom,
    ( domain_THFTYPE_IIiioIiioI @ lessThan_THFTYPE_IiioI @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_198,axiom,
    ( domain_THFTYPE_IIiioIiioI @ mother_THFTYPE_IiioI @ n1_THFTYPE_i @ lOrganism_THFTYPE_i )).

thf(ax_199,axiom,
    ( instance_THFTYPE_IIiooIioI @ holdsDuring_THFTYPE_IiooI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_200,axiom,
    ( instance_THFTYPE_IIiioIioI @ mother_THFTYPE_IiioI @ lSingleValuedRelation_THFTYPE_i )).

thf(ax_201,axiom,
    ( domain_THFTYPE_IIiioIiioI @ father_THFTYPE_IiioI @ n2_THFTYPE_i @ lOrganism_THFTYPE_i )).

thf(ax_202,axiom,
    ( domain_THFTYPE_IIiooIiioI @ knows_THFTYPE_IiooI @ n1_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_203,axiom,
    ( instance_THFTYPE_IIiioIioI @ possesses_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_204,axiom,
    ( domain_THFTYPE_IiiioI @ connected_THFTYPE_i @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_205,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ orientation_THFTYPE_IiiioI @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_206,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_207,axiom,
    ( domain_THFTYPE_IIiioIiioI @ time_THFTYPE_IiioI @ n2_THFTYPE_i @ lTimePosition_THFTYPE_i )).

thf(ax_208,axiom,
    ( relatedInternalConcept_THFTYPE_IIiioIIiooIoI @ wants_THFTYPE_IiioI @ desires_THFTYPE_IiooI )).

thf(ax_209,axiom,
    ( domain_THFTYPE_IiiioI @ lAdditionFn_THFTYPE_i @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_210,axiom,
    ( instance_THFTYPE_IIiooIioI @ hasPurpose_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_211,axiom,
    ( domain_THFTYPE_IIiioIiioI @ wants_THFTYPE_IiioI @ n1_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_212,axiom,
    ( instance_THFTYPE_IIiioIioI @ subrelation_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_213,axiom,
    ( instance_THFTYPE_IIiioIioI @ parent_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_214,axiom,
    ( instance_THFTYPE_IIiioIioI @ time_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_215,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_216,axiom,
    ( subrelation_THFTYPE_IIiooIIiioIoI @ desires_THFTYPE_IiooI @ inScopeOfInterest_THFTYPE_IiioI )).

thf(ax_217,axiom,
    ( subrelation_THFTYPE_IIiioIIiioIoI @ wants_THFTYPE_IiioI @ inScopeOfInterest_THFTYPE_IiioI )).

thf(ax_218,axiom,
    ( domain_THFTYPE_IIiooIiioI @ desires_THFTYPE_IiooI @ n1_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_219,axiom,
    ( instance_THFTYPE_IIiioIioI @ possesses_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_220,axiom,
    ( subrelation_THFTYPE_IIiioIIiioIoI @ mother_THFTYPE_IiioI @ parent_THFTYPE_IiioI )).

thf(ax_221,axiom,
    ( domain_THFTYPE_IIiioIiioI @ meetsTemporally_THFTYPE_IiioI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_222,axiom,
    ( instance_THFTYPE_IIiioIioI @ time_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_223,axiom,
    ( domain_THFTYPE_IiiioI @ equal_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_224,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subProcess_THFTYPE_IiioI @ n2_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_225,axiom,
    ( domain_THFTYPE_IiiioI @ spouse_THFTYPE_i @ n1_THFTYPE_i @ lHuman_THFTYPE_i )).

thf(ax_226,axiom,
    ( domain_THFTYPE_IIiioIiioI @ agent_THFTYPE_IiioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_227,axiom,
    ( instance_THFTYPE_IiioI @ equal_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_228,axiom,
    ( relatedInternalConcept_THFTYPE_IIiooIIiioIoI @ desires_THFTYPE_IiooI @ wants_THFTYPE_IiioI )).

thf(ax_229,axiom,
    ( instance_THFTYPE_IIiioIioI @ subProcess_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_230,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThanOrEqualTo_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_231,axiom,
    ( instance_THFTYPE_IIiooIioI @ knows_THFTYPE_IiooI @ lPropositionalAttitude_THFTYPE_i )).

thf(ax_232,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subrelation_THFTYPE_IiioI @ n2_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_233,axiom,
    ( subrelation_THFTYPE_IIiioIIiioIoI @ result_THFTYPE_IiioI @ patient_THFTYPE_IiioI )).

thf(ax_234,axiom,
    ( instance_THFTYPE_IIiooIioI @ considers_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_235,axiom,
    ( instance_THFTYPE_IIiooIioI @ desires_THFTYPE_IiooI @ lPropositionalAttitude_THFTYPE_i )).

thf(ax_236,axiom,
    ( instance_THFTYPE_IIiioIioI @ before_THFTYPE_IiioI @ lTransitiveRelation_THFTYPE_i )).

thf(ax_237,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThan_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_238,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_239,axiom,
    ( domain_THFTYPE_IIiioIiioI @ wife_THFTYPE_IiioI @ n1_THFTYPE_i @ lWoman_THFTYPE_i )).

thf(ax_240,axiom,
    ( domain_THFTYPE_IIiooIiioI @ knows_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_241,axiom,
    ( domain_THFTYPE_IIiioIiioI @ possesses_THFTYPE_IiioI @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_242,axiom,
    ( instance_THFTYPE_IIiiioIioI @ orientation_THFTYPE_IiiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_243,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ domain_THFTYPE_IiiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_244,axiom,
    ( instance_THFTYPE_IiioI @ connected_THFTYPE_i @ lSymmetricRelation_THFTYPE_i )).

thf(ax_245,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThan_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_246,axiom,
    ( domain_THFTYPE_IiiioI @ connected_THFTYPE_i @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_247,axiom,
    ( instance_THFTYPE_IIiooIioI @ knows_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_248,axiom,
    ( domain_THFTYPE_IIiioIiioI @ lessThanOrEqualTo_THFTYPE_IiioI @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_249,axiom,
    ( domain_THFTYPE_IIiioIiioI @ result_THFTYPE_IiioI @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_250,axiom,
    ( instance_THFTYPE_IIiioIioI @ father_THFTYPE_IiioI @ lSingleValuedRelation_THFTYPE_i )).

thf(ax_251,axiom,
    ( domain_THFTYPE_IIiooIiioI @ considers_THFTYPE_IiooI @ n1_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_252,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_253,axiom,
    ( domain_THFTYPE_IIIiioIIiooIoIiioI @ relatedInternalConcept_THFTYPE_IIiioIIiooIoI @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_254,axiom,
    ( instance_THFTYPE_IIiioIioI @ subrelation_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_255,axiom,
    ( instance_THFTYPE_IIiooIioI @ considers_THFTYPE_IiooI @ lPropositionalAttitude_THFTYPE_i )).

thf(ax_256,axiom,
    ( instance_THFTYPE_IIiioIioI @ husband_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_257,axiom,
    ( domain_THFTYPE_IIiioIiioI @ part_THFTYPE_IiioI @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_258,axiom,
    ( instance_THFTYPE_IIIiioIIiioIoIioI @ inverse_THFTYPE_IIiioIIiioIoI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_259,axiom,
    ( instance_THFTYPE_IIiioIioI @ instance_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_260,axiom,
    ( domain_THFTYPE_IIiioIiioI @ wife_THFTYPE_IiioI @ n2_THFTYPE_i @ lMan_THFTYPE_i )).

thf(ax_261,axiom,
    ( instance_THFTYPE_IIiioIioI @ disjoint_THFTYPE_IiioI @ lSymmetricRelation_THFTYPE_i )).

thf(ax_262,axiom,
    ( domain_THFTYPE_IiiioI @ lAdditionFn_THFTYPE_i @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_263,axiom,
    ( instance_THFTYPE_IIiioIioI @ range_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_264,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThanOrEqualTo_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_265,axiom,
    ( instance_THFTYPE_IIIiioIIiioIoIioI @ inverse_THFTYPE_IIiioIIiioIoI @ lSymmetricRelation_THFTYPE_i )).

thf(ax_266,axiom,
    ( domain_THFTYPE_IIiiiIiioI @ lMeasureFn_THFTYPE_IiiiI @ n1_THFTYPE_i @ lRealNumber_THFTYPE_i )).

thf(ax_267,axiom,
    ( instance_THFTYPE_IIiioIioI @ husband_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_268,axiom,
    ( domain_THFTYPE_IiiioI @ documentation_THFTYPE_i @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_269,axiom,
    ( domain_THFTYPE_IiiioI @ equal_THFTYPE_i @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_270,axiom,
    ( instance_THFTYPE_IIiioIioI @ disjoint_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_271,axiom,
    ( instance_THFTYPE_IIiioIioI @ before_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i )).

thf(ax_272,axiom,
    ( instance_THFTYPE_IIIiioIIiioIoIioI @ inverse_THFTYPE_IIiioIIiioIoI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_273,axiom,
    ( instance_THFTYPE_IIiioIioI @ subProcess_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_274,axiom,
    ( domain_THFTYPE_IIiioIiioI @ husband_THFTYPE_IiioI @ n1_THFTYPE_i @ lMan_THFTYPE_i )).

thf(ax_275,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThan_THFTYPE_IiioI @ lTransitiveRelation_THFTYPE_i )).

thf(ax_276,axiom,
    ( subrelation_THFTYPE_IIiioIIiioIoI @ father_THFTYPE_IiioI @ parent_THFTYPE_IiioI )).

thf(ax_277,axiom,
    ( domain_THFTYPE_IIiooIiioI @ holdsDuring_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_278,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThan_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_279,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_280,axiom,
    ( instance_THFTYPE_IIiiiIioI @ lMeasureFn_THFTYPE_IiiiI @ lBinaryFunction_THFTYPE_i )).

thf(ax_281,axiom,
    ( domain_THFTYPE_IIiioIiioI @ inList_THFTYPE_IiioI @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_282,axiom,
    ( domain_THFTYPE_IIioioIiioI @ hasPurposeForAgent_THFTYPE_IioioI @ n1_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_283,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_284,axiom,
    ( domain_THFTYPE_IIiioIiioI @ inScopeOfInterest_THFTYPE_IiioI @ n1_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_285,axiom,
    ( domain_THFTYPE_IIiioIiioI @ result_THFTYPE_IiioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_286,axiom,
    ( instance_THFTYPE_IIiooIioI @ desires_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_287,axiom,
    ( instance_THFTYPE_IIiioIioI @ inList_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_288,axiom,
    ( domain_THFTYPE_IIiooIiioI @ believes_THFTYPE_IiooI @ n1_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_289,axiom,
    ( domain_THFTYPE_IIiioIiioI @ lessThanOrEqualTo_THFTYPE_IiioI @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_290,axiom,
    ( instance_THFTYPE_IiioI @ lEndFn_THFTYPE_i @ lTemporalRelation_THFTYPE_i )).

thf(ax_291,axiom,
    ( instance_THFTYPE_IIiioIioI @ wants_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_292,axiom,
    ( domain_THFTYPE_IIIiioIIiooIoIiioI @ relatedInternalConcept_THFTYPE_IIiioIIiooIoI @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_293,axiom,
    ( instance_THFTYPE_IiioI @ equal_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_294,axiom,
    ( domain_THFTYPE_IIiooIiioI @ hasPurpose_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_295,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ orientation_THFTYPE_IiiioI @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_296,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ domainSubclass_THFTYPE_IiiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_297,axiom,
    ( instance_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_298,axiom,
    ( instance_THFTYPE_IIiioIioI @ wife_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_299,axiom,
    ( instance_THFTYPE_IIiioIioI @ subAttribute_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_300,axiom,
    ( domain_THFTYPE_IIiooIiioI @ holdsDuring_THFTYPE_IiooI @ n1_THFTYPE_i @ lTimePosition_THFTYPE_i )).

thf(ax_301,axiom,
    ( domain_THFTYPE_IIioioIiioI @ hasPurposeForAgent_THFTYPE_IioioI @ n3_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_302,axiom,
    ( domain_THFTYPE_IIiioIiioI @ father_THFTYPE_IiioI @ n1_THFTYPE_i @ lOrganism_THFTYPE_i )).

thf(ax_303,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subrelation_THFTYPE_IiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_304,axiom,
    ( instance_THFTYPE_IIiiioIioI @ domainSubclass_THFTYPE_IiiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_305,axiom,
    ( instance_THFTYPE_IiioI @ lEndFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_306,axiom,
    ( instance_THFTYPE_IIiioIioI @ before_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_307,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThanOrEqualTo_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_308,axiom,
    ( domain_THFTYPE_IiiioI @ spouse_THFTYPE_i @ n2_THFTYPE_i @ lHuman_THFTYPE_i )).

thf(ax_309,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThan_THFTYPE_IiioI @ lTransitiveRelation_THFTYPE_i )).

thf(ax_310,axiom,
    ( relatedInternalConcept_THFTYPE_IIiioIIiioIoI @ member_THFTYPE_IiioI @ instance_THFTYPE_IiioI )).

thf(ax_311,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lWhenFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_312,axiom,
    ( domain_THFTYPE_IIioioIiioI @ hasPurposeForAgent_THFTYPE_IioioI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_313,axiom,
    ( domain_THFTYPE_IiiioI @ lEndFn_THFTYPE_i @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_314,axiom,
    ( subrelation_THFTYPE_IIiioIioI @ wife_THFTYPE_IiioI @ spouse_THFTYPE_i )).

thf(ax_315,axiom,
    ( relatedInternalConcept_THFTYPE_IIiioIIiioIoI @ time_THFTYPE_IiioI @ located_THFTYPE_IiioI )).

thf(ax_316,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThan_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_317,axiom,
    ( domain_THFTYPE_IIiioIiioI @ attribute_THFTYPE_IiioI @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_318,axiom,
    ( domain_THFTYPE_IIiioIiioI @ wants_THFTYPE_IiioI @ n2_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_319,axiom,
    ( instance_THFTYPE_IIiioIioI @ located_THFTYPE_IiioI @ lTransitiveRelation_THFTYPE_i )).

thf(ax_320,axiom,
    ( domain_THFTYPE_IIiioIiioI @ inScopeOfInterest_THFTYPE_IiioI @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_321,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_322,axiom,
    ( domain_THFTYPE_IIiioIiioI @ greaterThanOrEqualTo_THFTYPE_IiioI @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

%----The translated conjectures
thf(con,conjecture,(
    ? [Z: $i] :
      ( ~ @ ( believes_THFTYPE_IiooI @ lMax_THFTYPE_i @ ( husband_THFTYPE_IiioI @ lMax_THFTYPE_i @ Z ) ) ) )).

%------------------------------------------------------------------------------
