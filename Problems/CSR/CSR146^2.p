%------------------------------------------------------------------------------
% File     : CSR146^2 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Commonsense Reasoning
% Problem  : What is the relation between Chris and Corina during 2009?
% Version  : Especial.
% English  : During 2009 Corina is the wife of Chris. True holds at any time. 
%            What is the relation between Chris and Corina during 2009?

% Refs     : [Ben10] Benzmueller (2010), Email to Geoff Sutcliffe
% Source   : [Ben10]
% Names    : ex_5.tq_SUMO_sine [Ben10]

% Status   : Theorem
% Rating   : 0.43 v6.4.0, 0.50 v6.3.0, 0.40 v6.2.0, 0.29 v6.1.0, 0.71 v6.0.0, 0.43 v5.5.0, 0.50 v5.4.0, 0.60 v5.2.0, 1.00 v4.1.0
% Syntax   : Number of formulae    :  500 (   0 unit; 157 type;   0 defn)
%            Number of atoms       : 1622 (  26 equality; 402 variable)
%            Maximal formula depth :   15 (   4 average)
%            Number of connectives : 1227 (   0   ~;   6   |;  52   &;1089   @)
%                                         (  14 <=>;  66  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  274 ( 274   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :  162 ( 157   :;   0   =)
%            Number of variables   :  190 (   2 sgn; 181   !;   7   ?;   2   ^)
%                                         ( 190   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This is a simple test problem for reasoning in/about SUMO.
%            Initally the problem has been hand generated in KIF syntax in
%            SigmaKEE and then automatically translated by Benzmueller's
%            KIF2TH0 translator into THF syntax.
%          : The translation has been applied in three modes: handselected,
%            SInE, and local. The local mode only translates the local
%            assumptions and the query. The SInE mode additionally translates
%            the SInE extract of the loaded knowledge base (usually SUMO). The
%            handselected mode contains a hand-selected relevant axioms.
%          : The examples are selected to illustrate the benefits of
%            higher-order reasoning in ontology reasoning.
%------------------------------------------------------------------------------
%----The extracted signature
thf(numbers,type,(
    num: $tType )).

thf(agent_THFTYPE_IiioI,type,(
    agent_THFTYPE_IiioI: $i > $i > $o )).

thf(attribute_THFTYPE_i,type,(
    attribute_THFTYPE_i: $i )).

thf(believes_THFTYPE_IiooI,type,(
    believes_THFTYPE_IiooI: $i > $o > $o )).

thf(connected_THFTYPE_i,type,(
    connected_THFTYPE_i: $i )).

thf(containsInformation_THFTYPE_i,type,(
    containsInformation_THFTYPE_i: $i )).

thf(contraryAttribute_THFTYPE_IioI,type,(
    contraryAttribute_THFTYPE_IioI: $i > $o )).

thf(disjointDecomposition_THFTYPE_IiioI,type,(
    disjointDecomposition_THFTYPE_IiioI: $i > $i > $o )).

thf(disjointDecomposition_THFTYPE_IioI,type,(
    disjointDecomposition_THFTYPE_IioI: $i > $o )).

thf(disjointRelation_THFTYPE_IiIiioIoI,type,(
    disjointRelation_THFTYPE_IiIiioIoI: $i > ( $i > $i > $o ) > $o )).

thf(disjointRelation_THFTYPE_IiioI,type,(
    disjointRelation_THFTYPE_IiioI: $i > $i > $o )).

thf(disjoint_THFTYPE_IiioI,type,(
    disjoint_THFTYPE_IiioI: $i > $i > $o )).

thf(documentation_THFTYPE_i,type,(
    documentation_THFTYPE_i: $i )).

thf(domainSubclass_THFTYPE_IIiiiIiioI,type,(
    domainSubclass_THFTYPE_IIiiiIiioI: ( $i > $i > $i ) > $i > $i > $o )).

thf(domainSubclass_THFTYPE_IIiioIiioI,type,(
    domainSubclass_THFTYPE_IIiioIiioI: ( $i > $i > $o ) > $i > $i > $o )).

thf(domainSubclass_THFTYPE_IIioIiioI,type,(
    domainSubclass_THFTYPE_IIioIiioI: ( $i > $o ) > $i > $i > $o )).

thf(domainSubclass_THFTYPE_IiiioI,type,(
    domainSubclass_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(domain_THFTYPE_IIIiioIIiioIoIiioI,type,(
    domain_THFTYPE_IIIiioIIiioIoIiioI: ( ( $i > $i > $o ) > ( $i > $i > $o ) > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIIioIiioIiioI,type,(
    domain_THFTYPE_IIIioIiioIiioI: ( ( $i > $o ) > $i > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiiIiioI,type,(
    domain_THFTYPE_IIiiIiioI: ( $i > $i ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiiiIiioI,type,(
    domain_THFTYPE_IIiiiIiioI: ( $i > $i > $i ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiiioIiioI,type,(
    domain_THFTYPE_IIiiioIiioI: ( $i > $i > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiioIiioI,type,(
    domain_THFTYPE_IIiioIiioI: ( $i > $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIioIiioI,type,(
    domain_THFTYPE_IIioIiioI: ( $i > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IIiooIiioI,type,(
    domain_THFTYPE_IIiooIiioI: ( $i > $o > $o ) > $i > $i > $o )).

thf(domain_THFTYPE_IiiioI,type,(
    domain_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(duration_THFTYPE_IiioI,type,(
    duration_THFTYPE_IiioI: $i > $i > $o )).

thf(equal_THFTYPE_i,type,(
    equal_THFTYPE_i: $i )).

thf(greaterThanOrEqualTo_THFTYPE_IiioI,type,(
    greaterThanOrEqualTo_THFTYPE_IiioI: $i > $i > $o )).

thf(greaterThan_THFTYPE_IiioI,type,(
    greaterThan_THFTYPE_IiioI: $i > $i > $o )).

thf(gt_THFTYPE_IiioI,type,(
    gt_THFTYPE_IiioI: $i > $i > $o )).

thf(gtet_THFTYPE_IiioI,type,(
    gtet_THFTYPE_IiioI: $i > $i > $o )).

thf(holdsDuring_THFTYPE_IiooI,type,(
    holdsDuring_THFTYPE_IiooI: $i > $o > $o )).

thf(husband_THFTYPE_IiioI,type,(
    husband_THFTYPE_IiioI: $i > $i > $o )).

thf(inList_THFTYPE_IiioI,type,(
    inList_THFTYPE_IiioI: $i > $i > $o )).

thf(instance_THFTYPE_IIIiioIIiioIoIioI,type,(
    instance_THFTYPE_IIIiioIIiioIoIioI: ( ( $i > $i > $o ) > ( $i > $i > $o ) > $o ) > $i > $o )).

thf(instance_THFTYPE_IIIiioIiioIioI,type,(
    instance_THFTYPE_IIIiioIiioIioI: ( ( $i > $i > $o ) > $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIIioIiioIioI,type,(
    instance_THFTYPE_IIIioIiioIioI: ( ( $i > $o ) > $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiIiioIoIioI,type,(
    instance_THFTYPE_IIiIiioIoIioI: ( $i > ( $i > $i > $o ) > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiiIioI,type,(
    instance_THFTYPE_IIiiIioI: ( $i > $i ) > $i > $o )).

thf(instance_THFTYPE_IIiiiIioI,type,(
    instance_THFTYPE_IIiiiIioI: ( $i > $i > $i ) > $i > $o )).

thf(instance_THFTYPE_IIiiioIioI,type,(
    instance_THFTYPE_IIiiioIioI: ( $i > $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiioIioI,type,(
    instance_THFTYPE_IIiioIioI: ( $i > $i > $o ) > $i > $o )).

thf(instance_THFTYPE_IIiooIioI,type,(
    instance_THFTYPE_IIiooIioI: ( $i > $o > $o ) > $i > $o )).

thf(instance_THFTYPE_IiioI,type,(
    instance_THFTYPE_IiioI: $i > $i > $o )).

thf(instrument_THFTYPE_IiioI,type,(
    instrument_THFTYPE_IiioI: $i > $i > $o )).

thf(inverse_THFTYPE_IIiioIIiioIoI,type,(
    inverse_THFTYPE_IIiioIIiioIoI: ( $i > $i > $o ) > ( $i > $i > $o ) > $o )).

thf(knows_THFTYPE_IiooI,type,(
    knows_THFTYPE_IiooI: $i > $o > $o )).

thf(lAdditionFn_THFTYPE_i,type,(
    lAdditionFn_THFTYPE_i: $i )).

thf(lAgent_THFTYPE_i,type,(
    lAgent_THFTYPE_i: $i )).

thf(lAsymmetricRelation_THFTYPE_i,type,(
    lAsymmetricRelation_THFTYPE_i: $i )).

thf(lAttribute_THFTYPE_i,type,(
    lAttribute_THFTYPE_i: $i )).

thf(lBeginFn_THFTYPE_IiiI,type,(
    lBeginFn_THFTYPE_IiiI: $i > $i )).

thf(lBinaryFunction_THFTYPE_i,type,(
    lBinaryFunction_THFTYPE_i: $i )).

thf(lBinaryPredicate_THFTYPE_i,type,(
    lBinaryPredicate_THFTYPE_i: $i )).

thf(lBinaryRelation_THFTYPE_i,type,(
    lBinaryRelation_THFTYPE_i: $i )).

thf(lCardinalityFn_THFTYPE_IiiI,type,(
    lCardinalityFn_THFTYPE_IiiI: $i > $i )).

thf(lChris_THFTYPE_i,type,(
    lChris_THFTYPE_i: $i )).

thf(lClass_THFTYPE_i,type,(
    lClass_THFTYPE_i: $i )).

thf(lCognitiveAgent_THFTYPE_i,type,(
    lCognitiveAgent_THFTYPE_i: $i )).

thf(lContentBearingObject_THFTYPE_i,type,(
    lContentBearingObject_THFTYPE_i: $i )).

thf(lContentBearingPhysical_THFTYPE_i,type,(
    lContentBearingPhysical_THFTYPE_i: $i )).

thf(lCorina_THFTYPE_i,type,(
    lCorina_THFTYPE_i: $i )).

thf(lDayDuration_THFTYPE_i,type,(
    lDayDuration_THFTYPE_i: $i )).

thf(lDay_THFTYPE_i,type,(
    lDay_THFTYPE_i: $i )).

thf(lEndFn_THFTYPE_IiiI,type,(
    lEndFn_THFTYPE_IiiI: $i > $i )).

thf(lEntity_THFTYPE_i,type,(
    lEntity_THFTYPE_i: $i )).

thf(lFormula_THFTYPE_i,type,(
    lFormula_THFTYPE_i: $i )).

thf(lHumanLanguage_THFTYPE_i,type,(
    lHumanLanguage_THFTYPE_i: $i )).

thf(lHuman_THFTYPE_i,type,(
    lHuman_THFTYPE_i: $i )).

thf(lInheritableRelation_THFTYPE_i,type,(
    lInheritableRelation_THFTYPE_i: $i )).

thf(lInteger_THFTYPE_i,type,(
    lInteger_THFTYPE_i: $i )).

thf(lIntentionalProcess_THFTYPE_i,type,(
    lIntentionalProcess_THFTYPE_i: $i )).

thf(lIrreflexiveRelation_THFTYPE_i,type,(
    lIrreflexiveRelation_THFTYPE_i: $i )).

thf(lKappaFn_THFTYPE_i,type,(
    lKappaFn_THFTYPE_i: $i )).

thf(lLanguage_THFTYPE_i,type,(
    lLanguage_THFTYPE_i: $i )).

thf(lLinguisticExpression_THFTYPE_i,type,(
    lLinguisticExpression_THFTYPE_i: $i )).

thf(lListFn_THFTYPE_IiiI,type,(
    lListFn_THFTYPE_IiiI: $i > $i )).

thf(lListFn_THFTYPE_i,type,(
    lListFn_THFTYPE_i: $i )).

thf(lListOrderFn_THFTYPE_IiiiI,type,(
    lListOrderFn_THFTYPE_IiiiI: $i > $i > $i )).

thf(lListOrderFn_THFTYPE_i,type,(
    lListOrderFn_THFTYPE_i: $i )).

thf(lList_THFTYPE_i,type,(
    lList_THFTYPE_i: $i )).

thf(lMeasureFn_THFTYPE_IiiiI,type,(
    lMeasureFn_THFTYPE_IiiiI: $i > $i > $i )).

thf(lMonthFn_THFTYPE_i,type,(
    lMonthFn_THFTYPE_i: $i )).

thf(lMonth_THFTYPE_i,type,(
    lMonth_THFTYPE_i: $i )).

thf(lMultiplicationFn_THFTYPE_i,type,(
    lMultiplicationFn_THFTYPE_i: $i )).

thf(lObject_THFTYPE_i,type,(
    lObject_THFTYPE_i: $i )).

thf(lOrganism_THFTYPE_i,type,(
    lOrganism_THFTYPE_i: $i )).

thf(lOrganization_THFTYPE_i,type,(
    lOrganization_THFTYPE_i: $i )).

thf(lPartialOrderingRelation_THFTYPE_i,type,(
    lPartialOrderingRelation_THFTYPE_i: $i )).

thf(lPhysical_THFTYPE_i,type,(
    lPhysical_THFTYPE_i: $i )).

thf(lProcess_THFTYPE_i,type,(
    lProcess_THFTYPE_i: $i )).

thf(lQuantity_THFTYPE_i,type,(
    lQuantity_THFTYPE_i: $i )).

thf(lRealNumber_THFTYPE_i,type,(
    lRealNumber_THFTYPE_i: $i )).

thf(lRelationExtendedToQuantities_THFTYPE_i,type,(
    lRelationExtendedToQuantities_THFTYPE_i: $i )).

thf(lRelation_THFTYPE_i,type,(
    lRelation_THFTYPE_i: $i )).

thf(lSelfConnectedObject_THFTYPE_i,type,(
    lSelfConnectedObject_THFTYPE_i: $i )).

thf(lSetOrClass_THFTYPE_i,type,(
    lSetOrClass_THFTYPE_i: $i )).

thf(lSubtractionFn_THFTYPE_i,type,(
    lSubtractionFn_THFTYPE_i: $i )).

thf(lSymbolicString_THFTYPE_i,type,(
    lSymbolicString_THFTYPE_i: $i )).

thf(lSymmetricRelation_THFTYPE_i,type,(
    lSymmetricRelation_THFTYPE_i: $i )).

thf(lTemporalCompositionFn_THFTYPE_IiiiI,type,(
    lTemporalCompositionFn_THFTYPE_IiiiI: $i > $i > $i )).

thf(lTemporalCompositionFn_THFTYPE_i,type,(
    lTemporalCompositionFn_THFTYPE_i: $i )).

thf(lTemporalRelation_THFTYPE_i,type,(
    lTemporalRelation_THFTYPE_i: $i )).

thf(lTernaryPredicate_THFTYPE_i,type,(
    lTernaryPredicate_THFTYPE_i: $i )).

thf(lText_THFTYPE_i,type,(
    lText_THFTYPE_i: $i )).

thf(lTimeInterval_THFTYPE_i,type,(
    lTimeInterval_THFTYPE_i: $i )).

thf(lTotalValuedRelation_THFTYPE_i,type,(
    lTotalValuedRelation_THFTYPE_i: $i )).

thf(lTransitiveRelation_THFTYPE_i,type,(
    lTransitiveRelation_THFTYPE_i: $i )).

thf(lUnaryFunction_THFTYPE_i,type,(
    lUnaryFunction_THFTYPE_i: $i )).

thf(lUnitOfMeasure_THFTYPE_i,type,(
    lUnitOfMeasure_THFTYPE_i: $i )).

thf(lWhenFn_THFTYPE_IiiI,type,(
    lWhenFn_THFTYPE_IiiI: $i > $i )).

thf(lWhenFn_THFTYPE_i,type,(
    lWhenFn_THFTYPE_i: $i )).

thf(lYearFn_THFTYPE_IiiI,type,(
    lYearFn_THFTYPE_IiiI: $i > $i )).

thf(lYearFn_THFTYPE_i,type,(
    lYearFn_THFTYPE_i: $i )).

thf(lYear_THFTYPE_i,type,(
    lYear_THFTYPE_i: $i )).

thf(lessThanOrEqualTo_THFTYPE_IiioI,type,(
    lessThanOrEqualTo_THFTYPE_IiioI: $i > $i > $o )).

thf(lessThan_THFTYPE_IiioI,type,(
    lessThan_THFTYPE_IiioI: $i > $i > $o )).

thf(located_THFTYPE_IiioI,type,(
    located_THFTYPE_IiioI: $i > $i > $o )).

thf(lt_THFTYPE_IiioI,type,(
    lt_THFTYPE_IiioI: $i > $i > $o )).

thf(ltet_THFTYPE_IiioI,type,(
    ltet_THFTYPE_IiioI: $i > $i > $o )).

thf(meetsTemporally_THFTYPE_IiioI,type,(
    meetsTemporally_THFTYPE_IiioI: $i > $i > $o )).

thf(member_THFTYPE_IiioI,type,(
    member_THFTYPE_IiioI: $i > $i > $o )).

thf(minus_THFTYPE_IiiiI,type,(
    minus_THFTYPE_IiiiI: $i > $i > $i )).

thf(modalAttribute_THFTYPE_i,type,(
    modalAttribute_THFTYPE_i: $i )).

thf(n12_THFTYPE_i,type,(
    n12_THFTYPE_i: $i )).

thf(n1_THFTYPE_i,type,(
    n1_THFTYPE_i: $i )).

thf(n2009_THFTYPE_i,type,(
    n2009_THFTYPE_i: $i )).

thf(n2_THFTYPE_i,type,(
    n2_THFTYPE_i: $i )).

thf(n3_THFTYPE_i,type,(
    n3_THFTYPE_i: $i )).

thf(orientation_THFTYPE_IiiioI,type,(
    orientation_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(part_THFTYPE_IiioI,type,(
    part_THFTYPE_IiioI: $i > $i > $o )).

thf(partition_THFTYPE_IiiioI,type,(
    partition_THFTYPE_IiiioI: $i > $i > $i > $o )).

thf(patient_THFTYPE_i,type,(
    patient_THFTYPE_i: $i )).

thf(property_THFTYPE_IiioI,type,(
    property_THFTYPE_IiioI: $i > $i > $o )).

thf(rangeSubclass_THFTYPE_IiioI,type,(
    rangeSubclass_THFTYPE_IiioI: $i > $i > $o )).

thf(range_THFTYPE_IiioI,type,(
    range_THFTYPE_IiioI: $i > $i > $o )).

thf(relatedExternalConcept_THFTYPE_i,type,(
    relatedExternalConcept_THFTYPE_i: $i )).

thf(relatedInternalConcept_THFTYPE_IIiioIIiioIoI,type,(
    relatedInternalConcept_THFTYPE_IIiioIIiioIoI: ( $i > $i > $o ) > ( $i > $i > $o ) > $o )).

thf(relatedInternalConcept_THFTYPE_IIioIIiioIoI,type,(
    relatedInternalConcept_THFTYPE_IIioIIiioIoI: ( $i > $o ) > ( $i > $i > $o ) > $o )).

thf(relatedInternalConcept_THFTYPE_IiIiiIoI,type,(
    relatedInternalConcept_THFTYPE_IiIiiIoI: $i > ( $i > $i ) > $o )).

thf(relatedInternalConcept_THFTYPE_IiIiioIoI,type,(
    relatedInternalConcept_THFTYPE_IiIiioIoI: $i > ( $i > $i > $o ) > $o )).

thf(relatedInternalConcept_THFTYPE_IiioI,type,(
    relatedInternalConcept_THFTYPE_IiioI: $i > $i > $o )).

thf(result_THFTYPE_i,type,(
    result_THFTYPE_i: $i )).

thf(spouse_THFTYPE_i,type,(
    spouse_THFTYPE_i: $i )).

thf(subAttribute_THFTYPE_IiioI,type,(
    subAttribute_THFTYPE_IiioI: $i > $i > $o )).

thf(subProcess_THFTYPE_IiioI,type,(
    subProcess_THFTYPE_IiioI: $i > $i > $o )).

thf(subclass_THFTYPE_IiioI,type,(
    subclass_THFTYPE_IiioI: $i > $i > $o )).

thf(subrelation_THFTYPE_IIiioIIiioIoI,type,(
    subrelation_THFTYPE_IIiioIIiioIoI: ( $i > $i > $o ) > ( $i > $i > $o ) > $o )).

thf(subrelation_THFTYPE_IIiioIioI,type,(
    subrelation_THFTYPE_IIiioIioI: ( $i > $i > $o ) > $i > $o )).

thf(subrelation_THFTYPE_IIioIIioIoI,type,(
    subrelation_THFTYPE_IIioIIioIoI: ( $i > $o ) > ( $i > $o ) > $o )).

thf(subrelation_THFTYPE_IIoooIIiioIoI,type,(
    subrelation_THFTYPE_IIoooIIiioIoI: ( $o > $o > $o ) > ( $i > $i > $o ) > $o )).

thf(subrelation_THFTYPE_IiIiioIoI,type,(
    subrelation_THFTYPE_IiIiioIoI: $i > ( $i > $i > $o ) > $o )).

thf(subrelation_THFTYPE_IiioI,type,(
    subrelation_THFTYPE_IiioI: $i > $i > $o )).

thf(temporalPart_THFTYPE_IiioI,type,(
    temporalPart_THFTYPE_IiioI: $i > $i > $o )).

thf(truth_THFTYPE_IoooI,type,(
    truth_THFTYPE_IoooI: $o > $o > $o )).

thf(wife_THFTYPE_IiioI,type,(
    wife_THFTYPE_IiioI: $i > $i > $o )).

%----The translated axioms
thf(ax,axiom,(
    ! [REL2: $i,CLASS1: $i,CLASS2: $i,REL1: $i] :
      ( ( ( rangeSubclass_THFTYPE_IiioI @ REL1 @ CLASS1 )
        & ( rangeSubclass_THFTYPE_IiioI @ REL2 @ CLASS2 )
        & ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 ) )
     => ( disjointRelation_THFTYPE_IiioI @ REL1 @ REL2 ) ) )).

%KIF documentation:(documentation instance EnglishLanguage "An object is an &%instance of a &%SetOrClass if it is included in that &%SetOrClass. An individual may be an instance of many classes, some of which may be subclasses of others. Thus, there is no assumption in the meaning of &%instance about specificity or uniqueness.")
%KIF documentation:(documentation range EnglishLanguage "Gives the range of a function. In other words, (&%range ?FUNCTION ?CLASS) means that all of the values assigned by ?FUNCTION are &%instances of ?CLASS.")
thf(ax_001,axiom,(
    ! [X: $i,Y: $i,Z: $i] :
      ( ( ( subclass_THFTYPE_IiioI @ X @ Y )
        & ( instance_THFTYPE_IiioI @ Z @ X ) )
     => ( instance_THFTYPE_IiioI @ Z @ Y ) ) )).

%KIF documentation:(documentation Agent EnglishLanguage "Something or someone that can act on its own and produce changes in the world.")
thf(ax_002,axiom,(
    ! [X: $i,Y: $i] :
      ( ( subclass_THFTYPE_IiioI @ X @ Y )
     => ( ( instance_THFTYPE_IiioI @ X @ lSetOrClass_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ Y @ lSetOrClass_THFTYPE_i ) ) ) )).

%KIF documentation:(documentation disjointRelation EnglishLanguage "This predicate relates two &%Relations. (&%disjointRelation ?REL1 ?REL2) means that the two relations have no tuples in common.")
%KIF documentation:(documentation lessThan EnglishLanguage "(&%lessThan ?NUMBER1 ?NUMBER2) is true just in case the &%Quantity ?NUMBER1 is less than the &%Quantity ?NUMBER2.")
%KIF documentation:(documentation property EnglishLanguage "This &%Predicate holds between an instance of &%Entity and an instance of &%Attribute. (&%property ?ENTITY ?ATTR) means that ?ENTITY has the &%Attribute ?ATTR.")
thf(ax_003,axiom,
    ( subclass_THFTYPE_IiioI @ lLanguage_THFTYPE_i @ lLinguisticExpression_THFTYPE_i )).

%KIF documentation:(documentation AsymmetricRelation EnglishLanguage "A &%BinaryRelation is asymmetric if and only if it is both an &%AntisymmetricRelation and an &%IrreflexiveRelation.")
%KIF documentation:(documentation instrument EnglishLanguage "(instrument ?EVENT ?TOOL) means that ?TOOL is used by an agent in bringing about ?EVENT and that ?TOOL is not changed by ?EVENT. For example, the key is an &%instrument in the following proposition: The key opened the door. Note that &%instrument and &%resource cannot be satisfied by the same ordered pair.")
thf(ax_004,axiom,(
    ! [CLASS: $i] :
      ( ( instance_THFTYPE_IiioI @ CLASS @ lClass_THFTYPE_i )
    <=> ( subclass_THFTYPE_IiioI @ CLASS @ lEntity_THFTYPE_i ) ) )).

thf(ax_005,axiom,(
    ! [REL2: $i > $i > $o,REL1: $i > $i > $o] :
      ( ( inverse_THFTYPE_IIiioIIiioIoI @ REL1 @ REL2 )
     => ! [INST1: $i,INST2: $i] :
          ( ( REL1 @ INST1 @ INST2 )
        <=> ( REL2 @ INST2 @ INST1 ) ) ) )).

thf(ax_006,axiom,(
    ! [OBJ1: $i,OBJ2: $i] :
      ( ( located_THFTYPE_IiioI @ OBJ1 @ OBJ2 )
     => ! [SUB: $i] :
          ( ( part_THFTYPE_IiioI @ SUB @ OBJ1 )
         => ( located_THFTYPE_IiioI @ SUB @ OBJ2 ) ) ) )).

thf(ax_007,axiom,(
    ! [NUMBER: $i,MONTH: $i] :
      ( ( ( instance_THFTYPE_IiioI @ MONTH @ lMonth_THFTYPE_i )
        & ( duration_THFTYPE_IiioI @ MONTH @ ( lMeasureFn_THFTYPE_IiiiI @ NUMBER @ lDayDuration_THFTYPE_i ) ) )
     => ( ( lCardinalityFn_THFTYPE_IiiI @ ( lTemporalCompositionFn_THFTYPE_IiiiI @ MONTH @ lDay_THFTYPE_i ) )
        = NUMBER ) ) )).

thf(ax_008,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

%KIF documentation:(documentation disjoint EnglishLanguage "&%Classes are &%disjoint only if they share no instances, i.e. just in case the result of applying &%IntersectionFn to them is empty.")
%KIF documentation:(documentation MultiplicationFn EnglishLanguage "If ?NUMBER1 and ?NUMBER2 are &%Numbers, then (&%MultiplicationFn ?NUMBER1 ?NUMBER2) is the arithmetical product of these numbers.")
thf(ax_009,axiom,(
    ! [ATTR2: $i,OBJ1: $i,ROW: $i,OBJ2: $i,ATTR1: $i] :
      ( ( ( orientation_THFTYPE_IiiioI @ OBJ1 @ OBJ2 @ ATTR1 )
        & ( contraryAttribute_THFTYPE_IioI @ ROW )
        & ( inList_THFTYPE_IiioI @ ATTR1 @ ( lListFn_THFTYPE_IiiI @ ROW ) )
        & ( inList_THFTYPE_IiioI @ ATTR2 @ ( lListFn_THFTYPE_IiiI @ ROW ) )
        & ( ~ @ ( ATTR1 = ATTR2 ) ) )
     => ( ~ @ ( orientation_THFTYPE_IiiioI @ OBJ1 @ OBJ2 @ ATTR2 ) ) ) )).

%KIF documentation:(documentation TimeInterval EnglishLanguage "An interval of time. Note that a &%TimeInterval has both an extent and a location on the universal timeline. Note too that a &%TimeInterval has no gaps, i.e. this class contains only convex time intervals.")
thf(ax_010,axiom,(
    ! [CLASS: $i,ATTR2: $i,ATTR1: $i] :
      ( ( ( subAttribute_THFTYPE_IiioI @ ATTR1 @ ATTR2 )
        & ( instance_THFTYPE_IiioI @ ATTR2 @ CLASS ) )
     => ( instance_THFTYPE_IiioI @ ATTR1 @ CLASS ) ) )).

thf(ax_011,axiom,
    ( subclass_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i @ lIrreflexiveRelation_THFTYPE_i )).

%KIF documentation:(documentation Relation EnglishLanguage "The &%Class of relations. There are three kinds of &%Relation: &%Predicate, &%Function, and &%List. &%Predicates and &%Functions both denote sets of ordered n-tuples. The difference between these two &%Classes is that &%Predicates cover formula-forming operators, while &%Functions cover term-forming operators. A &%List, on the other hand, is a particular ordered n-tuple.")
%KIF documentation:(documentation relatedExternalConcept EnglishLanguage "Used to signify a three-place relation between a concept in an external knowledge source, a concept in the SUMO, and the name of the other knowledge source.")
%KIF documentation:(documentation modalAttribute EnglishLanguage "A &%BinaryRelation that is used to state the normative force of a &%Proposition. (&%modalAttribute ?FORMULA ?PROP) means that the &%Proposition expressed by ?FORMULA has the &%NormativeAttribute ?PROP. For example, (&%modalAttribute (&%exists (?ACT ?OBJ) (&%and (&%instance ?ACT &%Giving) (&%agent ?ACT John) (&%patient ?ACT ?OBJ) (&%destination ?ACT Tom))) &%Obligation) means that John is obligated to give Tom something.")
%KIF documentation:(documentation disjointDecomposition EnglishLanguage "A &%disjointDecomposition of a &%Class C is a set of subclasses of C that are mutually &%disjoint.")
thf(ax_012,axiom,
    ( subclass_THFTYPE_IiioI @ lYear_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_013,axiom,(
    ! [NUMBER: $i,CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( domain_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS1 )
        & ( domain_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_014,axiom,
    ( subclass_THFTYPE_IiioI @ lText_THFTYPE_i @ lContentBearingObject_THFTYPE_i )).

thf(ax_015,axiom,(
    ! [REL: $i > $i > $o] :
      ( ( instance_THFTYPE_IIiioIioI @ REL @ lTransitiveRelation_THFTYPE_i )
    <=> ! [INST1: $i,INST2: $i,INST3: $i] :
          ( ( ( REL @ INST1 @ INST2 )
            & ( REL @ INST2 @ INST3 ) )
         => ( REL @ INST1 @ INST3 ) ) ) )).

thf(ax_016,axiom,(
    ! [THING2: $i,THING1: $i] :
      ( ( THING1 = THING2 )
     => ! [ATTR: $i] :
          ( ( property_THFTYPE_IiioI @ THING1 @ ATTR )
        <=> ( property_THFTYPE_IiioI @ THING2 @ ATTR ) ) ) )).

thf(ax_017,axiom,
    ( subclass_THFTYPE_IiioI @ lProcess_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_018,axiom,(
    ! [NUMBER2: $i,NUMBER1: $i] :
      ( ( gtet_THFTYPE_IiioI @ NUMBER1 @ NUMBER2 )
    <=> ( ( NUMBER1 = NUMBER2 )
        | ( gt_THFTYPE_IiioI @ NUMBER1 @ NUMBER2 ) ) ) )).

thf(ax_019,axiom,(
    ? [X: $i] :
      ( ~ @ ( husband_THFTYPE_IiioI @ lChris_THFTYPE_i @ X ) ) )).

thf(ax_020,axiom,
    ( rangeSubclass_THFTYPE_IiioI @ lTemporalCompositionFn_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

%KIF documentation:(documentation Physical EnglishLanguage "An entity that has a location in space-time. Note that locations are themselves understood to have a location in space-time.")
%KIF documentation:(documentation member EnglishLanguage "A specialized common sense notion of part for uniform parts of &%Collections. For example, each sheep in a flock of sheep would have the relationship of member to the flock.")
thf(ax_021,axiom,
    ( subclass_THFTYPE_IiioI @ lContentBearingObject_THFTYPE_i @ lContentBearingPhysical_THFTYPE_i )).

thf(ax_022,axiom,
    ( subclass_THFTYPE_IiioI @ lUnaryFunction_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

thf(ax_023,axiom,
    ( subclass_THFTYPE_IiioI @ lList_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_024,axiom,
    ( subclass_THFTYPE_IiioI @ lHuman_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

%KIF documentation:(documentation BinaryRelation EnglishLanguage "&%BinaryRelations are relations that are true only of pairs of things. &%BinaryRelations are represented as slots in frame systems.")
thf(ax_025,axiom,
    ( subclass_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_026,axiom,
    ( subclass_THFTYPE_IiioI @ lMonth_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_027,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation holdsDuring EnglishLanguage "(&%holdsDuring ?TIME ?FORMULA) means that the proposition denoted by ?FORMULA is true in the time frame ?TIME. Note that this implies that ?FORMULA is true at every &%TimePoint which is a &%temporalPart of ?TIME.")
%KIF documentation:(documentation attribute EnglishLanguage "(&%attribute ?OBJECT ?PROPERTY) means that ?PROPERTY is a &%Attribute of ?OBJECT. For example, (&%attribute &%MyLittleRedWagon &%Red).")
%KIF documentation:(documentation greaterThan EnglishLanguage "(&%greaterThan ?NUMBER1 ?NUMBER2) is true just in case the &%Quantity ?NUMBER1 is greater than the &%Quantity ?NUMBER2.")
thf(ax_028,axiom,(
    ! [SITUATION: $o,TIME2: $i,TIME1: $i] :
      ( ( ( holdsDuring_THFTYPE_IiooI @ TIME1 @ SITUATION )
        & ( temporalPart_THFTYPE_IiioI @ TIME2 @ TIME1 ) )
     => ( holdsDuring_THFTYPE_IiooI @ TIME2 @ SITUATION ) ) )).

%KIF documentation:(documentation Formula EnglishLanguage "A syntactically well-formed formula in the SUO-KIF knowledge representation language.")
thf(ax_029,axiom,(
    ! [CLASS: $i,ROW: $i] :
      ( ( disjointDecomposition_THFTYPE_IiioI @ CLASS @ ROW )
     => ! [ITEM: $i] :
          ( ( inList_THFTYPE_IiioI @ ITEM @ ( lListFn_THFTYPE_IiiI @ ROW ) )
         => ( subclass_THFTYPE_IiioI @ ITEM @ CLASS ) ) ) )).

thf(ax_030,axiom,
    ( range_THFTYPE_IiioI @ lListOrderFn_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_031,axiom,(
    ! [REL: $i > $i > $o] :
      ( ( instance_THFTYPE_IIiioIioI @ REL @ lIrreflexiveRelation_THFTYPE_i )
    <=> ! [INST: $i] :
          ( ~ @ ( REL @ INST @ INST ) ) ) )).

thf(ax_032,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryFunction_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_033,axiom,(
    ! [NUMBER: $i,PRED1: $i,CLASS1: $i,PRED2: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ PRED1 @ PRED2 )
        & ( domain_THFTYPE_IiiioI @ PRED2 @ NUMBER @ CLASS1 ) )
     => ( domain_THFTYPE_IiiioI @ PRED1 @ NUMBER @ CLASS1 ) ) )).

%KIF documentation:(documentation ContentBearingPhysical EnglishLanguage "Any &%Object or &%Process that expresses content. This covers &%Objects that contain a &%Proposition, such as a book, as well as &%ManualSignLanguage, which may similarly contain a &%Proposition.")
%KIF documentation:(documentation domainSubclass EnglishLanguage "&%Predicate used to specify argument type restrictions of &%Predicates. The formula (&%domainSubclass ?REL ?INT ?CLASS) means that the ?INT'th element of each tuple in the relation ?REL must be a subclass of ?CLASS.")
%KIF documentation:(documentation located EnglishLanguage "(&%located ?PHYS ?OBJ) means that ?PHYS is &%partlyLocated at ?OBJ, and there is no &%part or &%subProcess of ?PHYS that is not &%located at ?OBJ.")
thf(ax_034,axiom,
    ( subclass_THFTYPE_IiioI @ lTotalValuedRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_035,axiom,
    ( subclass_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_036,axiom,(
    ! [YEAR: $i] :
      ( ( instance_THFTYPE_IiioI @ YEAR @ lYear_THFTYPE_i )
     => ( ( lCardinalityFn_THFTYPE_IiiI @ ( lTemporalCompositionFn_THFTYPE_IiiiI @ YEAR @ lMonth_THFTYPE_i ) )
        = n12_THFTYPE_i ) ) )).

thf(ax_037,axiom,
    ( subclass_THFTYPE_IiioI @ lObject_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_038,axiom,
    ( subclass_THFTYPE_IiioI @ lSelfConnectedObject_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_039,axiom,(
    ! [CLASS1: $i,CLASS2: $i] :
      ( ( CLASS1 = CLASS2 )
     => ! [THING: $i] :
          ( ( instance_THFTYPE_IiioI @ THING @ CLASS1 )
        <=> ( instance_THFTYPE_IiioI @ THING @ CLASS2 ) ) ) )).

%KIF documentation:(documentation TotalValuedRelation EnglishLanguage "A &%Relation is a &%TotalValuedRelation just in case there exists an assignment for the last argument position of the &%Relation given any assignment of values to every argument position except the last one. Note that declaring a &%Relation to be both a &%TotalValuedRelation and a &%SingleValuedRelation means that it is a total function.")
%KIF documentation:(documentation inverse EnglishLanguage "The inverse of a &%BinaryRelation is a relation in which all the tuples of the original relation are reversed. In other words, one &%BinaryRelation is the inverse of another if they are equivalent when their arguments are swapped.")
thf(ax_040,axiom,(
    ! [YEAR2: $i,YEAR1: $i] :
      ( ( ( instance_THFTYPE_IiioI @ YEAR1 @ lYear_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ YEAR2 @ lYear_THFTYPE_i )
        & ( ( minus_THFTYPE_IiiiI @ YEAR2 @ YEAR1 )
          = n1_THFTYPE_i ) )
     => ( meetsTemporally_THFTYPE_IiioI @ YEAR1 @ YEAR2 ) ) )).

%KIF documentation:(documentation LinguisticExpression EnglishLanguage "This is the subclass of &%ContentBearingPhysical which are language-related. Note that this &%Class encompasses both &%Language and the the elements of &%Languages, e.g. &%Words.")
thf(ax_041,axiom,
    ( subclass_THFTYPE_IiioI @ lContentBearingPhysical_THFTYPE_i @ lPhysical_THFTYPE_i )).

%KIF documentation:(documentation YearFn EnglishLanguage "A &%UnaryFunction that maps a number to the corresponding calendar &%Year. For example, (&%YearFn 1912) returns the &%Class containing just one instance, the year of 1912. As might be expected, positive integers return years in the Common Era, while negative integers return years in B.C.E. Note that this function returns a &%Class as a value. The reason for this is that the related functions, viz. &%MonthFn, &%DayFn, &%HourFn, &%MinuteFn, and &%SecondFn, are used to generate both specific &%TimeIntervals and recurrent intervals, and the only way to do this is to make the domains and ranges of these functions classes rather than individuals.")
thf(ax_042,axiom,(
    ! [FORMULA: $o,AGENT: $i] :
      ( ( knows_THFTYPE_IiooI @ AGENT @ FORMULA )
     => ( believes_THFTYPE_IiooI @ AGENT @ FORMULA ) ) )).

thf(ax_043,axiom,(
    ! [ORG: $i,AGENT: $i] :
      ( ( ( instance_THFTYPE_IiioI @ ORG @ lOrganization_THFTYPE_i )
        & ( member_THFTYPE_IiioI @ AGENT @ ORG ) )
     => ( instance_THFTYPE_IiioI @ AGENT @ lAgent_THFTYPE_i ) ) )).

thf(ax_044,axiom,(
    ! [THING2: $i,THING1: $i] :
      ( ( THING1 = THING2 )
     => ! [CLASS: $i] :
          ( ( instance_THFTYPE_IiioI @ THING1 @ CLASS )
        <=> ( instance_THFTYPE_IiioI @ THING2 @ CLASS ) ) ) )).

thf(ax_045,axiom,
    ( subclass_THFTYPE_IiioI @ lOrganism_THFTYPE_i @ lAgent_THFTYPE_i )).

%KIF documentation:(documentation orientation EnglishLanguage "A general &%Predicate for indicating how two &%Objects are oriented with respect to one another. For example, (orientation ?OBJ1 ?OBJ2 North) means that ?OBJ1 is north of ?OBJ2, and (orientation ?OBJ1 ?OBJ2 Vertical) means that ?OBJ1 is positioned vertically with respect to ?OBJ2.")
%KIF documentation:(documentation UnaryFunction EnglishLanguage "The &%Class of &%Functions that require a single argument.")
thf(ax_046,axiom,
    ( subclass_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

thf(ax_047,axiom,(
    ! [SUBPROC: $i,PROC: $i] :
      ( ( subProcess_THFTYPE_IiioI @ SUBPROC @ PROC )
     => ( temporalPart_THFTYPE_IiioI @ ( lWhenFn_THFTYPE_IiiI @ SUBPROC ) @ ( lWhenFn_THFTYPE_IiiI @ PROC ) ) ) )).

%KIF documentation:(documentation greaterThanOrEqualTo EnglishLanguage "(&%greaterThanOrEqualTo ?NUMBER1 ?NUMBER2) is true just in case the &%Quantity ?NUMBER1 is greater than the &%Quantity ?NUMBER2.")
%KIF documentation:(documentation connected EnglishLanguage "(connected ?OBJ1 ?OBJ2) means that ?OBJ1 &%meetsSpatially ?OBJ2 or that ?OBJ1 &%overlapsSpatially ?OBJ2.")
thf(ax_048,axiom,
    ( rangeSubclass_THFTYPE_IiioI @ lMonthFn_THFTYPE_i @ lMonth_THFTYPE_i )).

thf(ax_049,axiom,(
    ! [INTERVAL1: $i,INTERVAL2: $i] :
      ( ( ( ( lBeginFn_THFTYPE_IiiI @ INTERVAL1 )
          = ( lBeginFn_THFTYPE_IiiI @ INTERVAL2 ) )
        & ( ( lEndFn_THFTYPE_IiiI @ INTERVAL1 )
          = ( lEndFn_THFTYPE_IiiI @ INTERVAL2 ) ) )
     => ( INTERVAL1 = INTERVAL2 ) ) )).

%KIF documentation:(documentation SetOrClass EnglishLanguage "The &%SetOrClass of &%Sets and &%Classes, i.e. any instance of &%Abstract that has &%elements or &%instances.")
thf(ax_050,axiom,(
    ! [REL2: $i,CLASS1: $i,REL1: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ REL1 @ REL2 )
        & ( range_THFTYPE_IiioI @ REL2 @ CLASS1 ) )
     => ( range_THFTYPE_IiioI @ REL1 @ CLASS1 ) ) )).

%KIF documentation:(documentation subclass EnglishLanguage "(&%subclass ?CLASS1 ?CLASS2) means that ?CLASS1 is a subclass of ?CLASS2, i.e. every instance of ?CLASS1 is also an instance of ?CLASS2. A class may have multiple superclasses and subclasses.")
%KIF documentation:(documentation Language EnglishLanguage "A system of signs for expressing thought. The system can be either natural or artificial, i.e. something that emerges gradually as a cultural artifact or something that is intentionally created by a person or group of people.")
%KIF documentation:(documentation BinaryFunction EnglishLanguage "The &%Class of &%Functions that require two arguments.")
%KIF documentation:(documentation Entity EnglishLanguage "The universal class of individuals. This is the root node of the ontology.")
%KIF documentation:(documentation WhenFn EnglishLanguage "A &%UnaryFunction that maps an &%Object or &%Process to the exact &%TimeInterval during which it exists. Note that, for every &%TimePoint ?TIME outside of the &%TimeInterval (WhenFn ?THING), (time ?THING ?TIME) does not hold.")
thf(ax_051,axiom,
    ( subclass_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation ListOrderFn EnglishLanguage "(&%ListOrderFn ?LIST ?NUMBER) denotes the item that is in the ?NUMBER position in the &%List ?LIST. For example, (&%ListOrderFn (&%ListFn &%Monday &%Tuesday &%Wednesday) 2) would return the value &%Tuesday.")
thf(ax_052,axiom,(
    ! [PROC: $i] :
      ( ( instance_THFTYPE_IiioI @ PROC @ lIntentionalProcess_THFTYPE_i )
     => ? [AGENT: $i] :
          ( ( instance_THFTYPE_IiioI @ AGENT @ lCognitiveAgent_THFTYPE_i )
          & ( agent_THFTYPE_IiioI @ PROC @ AGENT ) ) ) )).

thf(ax_053,axiom,
    ( subclass_THFTYPE_IiioI @ lAgent_THFTYPE_i @ lObject_THFTYPE_i )).

%KIF documentation:(documentation agent EnglishLanguage "(&%agent ?PROCESS ?AGENT) means that ?AGENT is an active determinant, either animate or inanimate, of the &%Process ?PROCESS, with or without voluntary intention. For example, Eve is an &%agent in the following proposition: Eve bit an apple.")
thf(ax_054,axiom,(
    ! [ATTR2: $i,ATTR1: $i] :
      ( ( ATTR1 = ATTR2 )
     => ! [THING: $i] :
          ( ( property_THFTYPE_IiioI @ THING @ ATTR1 )
        <=> ( property_THFTYPE_IiioI @ THING @ ATTR2 ) ) ) )).

thf(ax_055,axiom,(
    ! [SUBPROC: $i,PROC: $i] :
      ( ( subProcess_THFTYPE_IiioI @ SUBPROC @ PROC )
     => ! [REGION: $i] :
          ( ( located_THFTYPE_IiioI @ PROC @ REGION )
         => ( located_THFTYPE_IiioI @ SUBPROC @ REGION ) ) ) )).

thf(ax_056,axiom,
    ( range_THFTYPE_IiioI @ lSubtractionFn_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_057,axiom,(
    ! [Z: $i] :
      ( holdsDuring_THFTYPE_IiooI @ Z @ $true ) )).

thf(ax_058,axiom,(
    ! [Z: $i] :
      ( holdsDuring_THFTYPE_IiooI @ Z @ $true ) )).

%KIF documentation:(documentation patient EnglishLanguage "(&%patient ?PROCESS ?ENTITY) means that ?ENTITY is a participant in ?PROCESS that may be moved, said, experienced, etc. For example, the direct objects in the sentences 'The cat swallowed the canary' and 'Billy likes the beer' would be examples of &%patients. Note that the &%patient of a &%Process may or may not undergo structural change as a result of the &%Process. The &%CaseRole of &%patient is used when one wants to specify as broadly as possible the object of a &%Process.")
%KIF documentation:(documentation believes EnglishLanguage "The epistemic predicate of belief. (&%believes ?AGENT ?FORMULA) means that ?AGENT believes the proposition expressed by ?FORMULA.")
%KIF documentation:(documentation Text EnglishLanguage "A &%LinguisticExpression or set of &%LinguisticExpressions that perform a specific function related to &%Communication, e.g. express a discourse about a particular topic, and that are inscribed in a &%CorpuscularObject by &%Humans.")
%KIF documentation:(documentation result EnglishLanguage "(result ?ACTION ?OUTPUT) means that ?OUTPUT is a product of ?ACTION. For example, house is a &%result in the following proposition: Eric built a house.")
thf(ax_059,axiom,(
    ! [REL: $i > $i > $o] :
      ( ( instance_THFTYPE_IIiioIioI @ REL @ lSymmetricRelation_THFTYPE_i )
    <=> ! [INST1: $i,INST2: $i] :
          ( ( REL @ INST1 @ INST2 )
         => ( REL @ INST2 @ INST1 ) ) ) )).

thf(ax_060,axiom,
    ( subclass_THFTYPE_IiioI @ lPhysical_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_061,axiom,
    ( range_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lQuantity_THFTYPE_i )).

%KIF documentation:(documentation temporalPart EnglishLanguage "The temporal analogue of the spatial &%part predicate. (&%temporalPart ?POS1 ?POS2) means that &%TimePosition ?POS1 is part of &%TimePosition ?POS2. Note that since &%temporalPart is a &%ReflexiveRelation every &%TimePostion is a &%temporalPart of itself.")
%KIF documentation:(documentation subrelation EnglishLanguage "(&%subrelation ?REL1 ?REL2) means that every tuple of ?REL1 is also a tuple of ?REL2. In other words, if the &%Relation ?REL1 holds for some arguments arg_1, arg_2, ... arg_n, then the &%Relation ?REL2 holds for the same arguments. A consequence of this is that a &%Relation and its subrelations must have the same &%valence.")
%KIF documentation:(documentation spouse EnglishLanguage "The relationship of marriage between two &%Humans.")
thf(ax_062,axiom,(
    ! [ROW: $i,ELEMENT: $i] :
      ( ( contraryAttribute_THFTYPE_IioI @ ROW )
     => ( ( inList_THFTYPE_IiioI @ ELEMENT @ ( lListFn_THFTYPE_IiiI @ ROW ) )
       => ( instance_THFTYPE_IiioI @ ELEMENT @ lAttribute_THFTYPE_i ) ) ) )).

%KIF documentation:(documentation EndFn EnglishLanguage "A &%UnaryFunction that maps a &%TimeInterval to the &%TimePoint at which the interval ends.")
thf(ax_063,axiom,
    ( subclass_THFTYPE_IiioI @ lUnaryFunction_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation partition EnglishLanguage "A &%partition of a class C is a set of mutually &%disjoint classes (a subclass partition) which covers C. Every instance of C is an instance of exactly one of the subclasses in the partition.")
%KIF documentation:(documentation lessThanOrEqualTo EnglishLanguage "(&%lessThanOrEqualTo ?NUMBER1 ?NUMBER2) is true just in case the &%Quantity ?NUMBER1 is less than or equal to the &%Quantity ?NUMBER2.")
thf(ax_064,axiom,
    ( range_THFTYPE_IiioI @ lKappaFn_THFTYPE_i @ lClass_THFTYPE_i )).

%KIF documentation:(documentation DayDuration EnglishLanguage "Time unit. 1 day = 24 hours.")
%KIF documentation:(documentation InheritableRelation EnglishLanguage "The class of &%Relations whose properties can be inherited downward in the class hierarchy via the &%subrelation &%Predicate.")
%KIF documentation:(documentation husband EnglishLanguage "(&%husband ?MAN ?WOMAN) means that ?MAN is the husband of ?WOMAN.")
%KIF documentation:(documentation IntentionalProcess EnglishLanguage "A &%Process that has a specific purpose for the &%CognitiveAgent who performs it.")
thf(ax_065,axiom,(
    ! [CLASS: $i,PRED1: $i,PRED2: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ PRED1 @ PRED2 )
        & ( instance_THFTYPE_IiioI @ PRED2 @ CLASS )
        & ( subclass_THFTYPE_IiioI @ CLASS @ lInheritableRelation_THFTYPE_i ) )
     => ( instance_THFTYPE_IiioI @ PRED1 @ CLASS ) ) )).

%KIF documentation:(documentation Object EnglishLanguage "Corresponds roughly to the class of ordinary objects. Examples include normal physical objects, geographical regions, and locations of &%Processes, the complement of &%Objects in the &%Physical class. In a 4D ontology, an &%Object is something whose spatiotemporal extent is thought of as dividing into spatial parts roughly parallel to the time-axis.")
thf(ax_066,axiom,(
    ! [CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( rangeSubclass_THFTYPE_IiioI @ REL @ CLASS1 )
        & ( rangeSubclass_THFTYPE_IiioI @ REL @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

%KIF documentation:(documentation KappaFn EnglishLanguage "A class-forming operator that takes two arguments: a variable and a formula containing at least one unbound occurrence of the variable. The result of applying &%KappaFn to a variable and a formula is the &%SetOrClass of things that satisfy the formula. For example, we can denote the &%SetOrClass of prime numbers that are less than 100 with the following expression: (KappaFn ?NUMBER (and (instance ?NUMBER PrimeNumber) (lessThan ?NUMBER 100))). Note that the use of this function is discouraged, since there is currently no axiomatic support for it.")
thf(ax_067,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

thf(ax_068,axiom,
    ( subclass_THFTYPE_IiioI @ lInheritableRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_069,axiom,(
    ! [NUMBER2: $i,NUMBER1: $i] :
      ( ( ltet_THFTYPE_IiioI @ NUMBER1 @ NUMBER2 )
    <=> ( ( NUMBER1 = NUMBER2 )
        | ( lt_THFTYPE_IiioI @ NUMBER1 @ NUMBER2 ) ) ) )).

%KIF documentation:(documentation Process EnglishLanguage "The class of things that happen and have temporal parts or stages. Examples include extended events like a football match or a race, actions like &%Pursuing and &%Reading, and biological processes. The formal definition is: anything that occurs in time but is not an &%Object. Note that a &%Process may have participants 'inside' it which are &%Objects, such as the players in a football match. In a 4D ontology, a &%Process is something whose spatiotemporal extent is thought of as dividing into temporal stages roughly perpendicular to the time-axis.")
%KIF documentation:(documentation TemporalRelation EnglishLanguage "The &%Class of temporal &%Relations. This &%Class includes notions of (temporal) topology of intervals, (temporal) schemata, and (temporal) extension.")
%KIF documentation:(documentation MonthFn EnglishLanguage "A &%BinaryFunction that maps a subclass of &%Month and a subclass of &%Year to the class containing the &%Months corresponding to thos &%Years. For example (&%MonthFn &%January (&%YearFn 1912)) is the class containing the eighth &%Month, i.e. August, of the &%Year 1912. For another example, (&%MonthFn &%August &%Year) is equal to &%August, the class of all months of August. Note that this function returns a &%Class as a value. The reason for this is that the related functions, viz. DayFn, HourFn, MinuteFn, and SecondFn, are used to generate both specific &%TimeIntervals and recurrent intervals, and the only way to do this is to make the domains and ranges of these functions classes rather than individuals.")
thf(ax_070,axiom,
    ( subclass_THFTYPE_IiioI @ lBinaryRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_071,axiom,(
    ! [THING: $i] :
      ( instance_THFTYPE_IiioI @ THING @ lEntity_THFTYPE_i ) )).

%KIF documentation:(documentation domain EnglishLanguage "Provides a computationally and heuristically convenient mechanism for declaring the argument types of a given relation. The formula (&%domain ?REL ?INT ?CLASS) means that the ?INT'th element of each tuple in the relation ?REL must be an instance of ?CLASS. Specifying argument types is very helpful in maintaining ontologies. Representation systems can use these specifications to classify terms and check integrity constraints. If the restriction on the argument type of a &%Relation is not captured by a &%SetOrClass already defined in the ontology, one can specify a &%SetOrClass compositionally with the functions &%UnionFn, &%IntersectionFn, etc.")
thf(ax_072,axiom,(
    ! [AGENT: $i] :
      ( ( instance_THFTYPE_IiioI @ AGENT @ lAgent_THFTYPE_i )
    <=> ? [PROC: $i] :
          ( agent_THFTYPE_IiioI @ PROC @ AGENT ) ) )).

%KIF documentation:(documentation rangeSubclass EnglishLanguage "(&%rangeSubclass ?FUNCTION ?CLASS) means that all of the values assigned by ?FUNCTION are &%subclasses of ?CLASS.")
%KIF documentation:(documentation TernaryPredicate EnglishLanguage "The &%Class of &%Predicates that require exactly three arguments.")
thf(ax_073,axiom,
    ( range_THFTYPE_IiioI @ lListFn_THFTYPE_i @ lList_THFTYPE_i )).

thf(ax_074,axiom,
    ( subclass_THFTYPE_IiioI @ lTotalValuedRelation_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation RealNumber EnglishLanguage "Any &%Number that can be expressed as a (possibly infinite) decimal, i.e. any &%Number that has a position on the number line.")
thf(ax_075,axiom,(
    ! [NUMBER: $i,CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( domainSubclass_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS1 )
        & ( domainSubclass_THFTYPE_IiiioI @ REL @ NUMBER @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_076,axiom,(
    ! [DAY: $i] :
      ( ( instance_THFTYPE_IiioI @ DAY @ lDay_THFTYPE_i )
     => ( duration_THFTYPE_IiioI @ DAY @ ( lMeasureFn_THFTYPE_IiiiI @ n1_THFTYPE_i @ lDayDuration_THFTYPE_i ) ) ) )).

thf(ax_077,axiom,(
    ! [FORMULA: $o,AGENT: $i] :
      ( ( knows_THFTYPE_IiooI @ AGENT @ FORMULA )
     => ( truth_THFTYPE_IoooI @ FORMULA @ $true ) ) )).

thf(ax_078,axiom,(
    ! [REL2: $i,NUMBER: $i,CLASS1: $i,CLASS2: $i,REL1: $i] :
      ( ( ( domainSubclass_THFTYPE_IiiioI @ REL1 @ NUMBER @ CLASS1 )
        & ( domainSubclass_THFTYPE_IiiioI @ REL2 @ NUMBER @ CLASS2 )
        & ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 ) )
     => ( disjointRelation_THFTYPE_IiioI @ REL1 @ REL2 ) ) )).

%KIF documentation:(documentation documentation EnglishLanguage "A relation between objects in the domain of discourse and strings of natural language text stated in a particular &%HumanLanguage. The domain of &%documentation is not constants (names), but the objects themselves. This means that one does not quote the names when associating them with their documentation.")
thf(ax_079,axiom,
    ( subclass_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i @ lRelation_THFTYPE_i )).

%KIF documentation:(documentation Organization EnglishLanguage "An &%Organization is a corporate or similar institution. The &%members of an &%Organization typically have a common purpose or function. Note that this class also covers divisions, departments, etc. of organizations. For example, both the Shell Corporation and the accounting department at Shell would both be instances of &%Organization. Note too that the existence of an &%Organization is dependent on the existence of at least one &%member (since &%Organization is a subclass of &%Collection). Accordingly, in cases of purely legal organizations, a fictitious &%member should be assumed.")
thf(ax_080,axiom,
    ( subclass_THFTYPE_IiioI @ lIntentionalProcess_THFTYPE_i @ lProcess_THFTYPE_i )).

%KIF documentation:(documentation PartialOrderingRelation EnglishLanguage "A &%BinaryRelation is a partial ordering if it is a &%ReflexiveRelation, an &%AntisymmetricRelation, and a &%TransitiveRelation.")
%KIF documentation:(documentation SymmetricRelation EnglishLanguage "A &%BinaryRelation ?REL is symmetric just iff (?REL ?INST1 ?INST2) imples (?REL ?INST2 ?INST1), for all ?INST1 and ?INST2.")
thf(ax_081,axiom,
    ( subclass_THFTYPE_IiioI @ lClass_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

%KIF documentation:(documentation TemporalCompositionFn EnglishLanguage "The basic &%Function for expressing the composition of larger &%TimeIntervals out of smaller &%TimeIntervals. For example, if &%ThisSeptember is an &%instance of &%September, (&%TemporalCompositionFn &%ThisSeptember &%Day) denotes the &%Class of consecutive days that make up &%ThisSeptember. Note that one can obtain the number of instances of this &%Class by using the function &%CardinalityFn.")
%KIF documentation:(documentation RelationExtendedToQuantities EnglishLanguage "A &%RelationExtendedToQuantities is a &%Relation that, when it is true on a sequence of arguments that are &%RealNumbers, it is also true on a sequence of instances of &%ConstantQuantity with those magnitudes in some unit of measure. For example, the &%lessThan relation is extended to quantities. This means that for all pairs of quantities ?QUANTITY1 and ?QUANTITY2, (&%lessThan ?QUANTITY1 ?QUANTITY2) if and only if, for some ?NUMBER1, ?NUMBER2, and ?UNIT, ?QUANTITY1 = (&%MeasureFn ?NUMBER1 ?UNIT), ?QUANTITY2 = (&%MeasureFn ?NUMBER2 ?UNIT), and (&%lessThan ?NUMBER1 ?NUMBER2), for all units ?UNIT on which ?QUANTITY1 and ?QUANTITY2 can be measured. Note that, when a &%RelationExtendedToQuantities is extended from &%RealNumbers to instances of &%ConstantQuantity, the &%ConstantQuantity must be measured along the same physical dimension.")
thf(ax_082,axiom,
    ( subclass_THFTYPE_IiioI @ lTransitiveRelation_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

%KIF documentation:(documentation equal EnglishLanguage "(equal ?ENTITY1 ?ENTITY2) is true just in case ?ENTITY1 is identical with ?ENTITY2.")
%KIF documentation:(documentation CardinalityFn EnglishLanguage "(CardinalityFn ?CLASS) returns the number of instances in the &%SetOrClass ?CLASS or the number of members in the ?CLASS &%Collection.")
%KIF documentation:(documentation ListFn EnglishLanguage "A &%Function that takes any number of arguments and returns the &%List containing those arguments in exactly the same order.")
thf(ax_083,axiom,(
    ! [REL2: $i,CLASS1: $i,CLASS2: $i,REL1: $i] :
      ( ( ( range_THFTYPE_IiioI @ REL1 @ CLASS1 )
        & ( range_THFTYPE_IiioI @ REL2 @ CLASS2 )
        & ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 ) )
     => ( disjointRelation_THFTYPE_IiioI @ REL1 @ REL2 ) ) )).

thf(ax_084,axiom,(
    ! [TIME: $i,SITUATION: $o] :
      ( ( holdsDuring_THFTYPE_IiooI @ TIME @ ( ~ @ SITUATION ) )
     => ( ~ @ ( holdsDuring_THFTYPE_IiooI @ TIME @ SITUATION ) ) ) )).

thf(ax_085,axiom,
    ( subclass_THFTYPE_IiioI @ lText_THFTYPE_i @ lLinguisticExpression_THFTYPE_i )).

thf(ax_086,axiom,(
    ! [LIST2: $i,LIST1: $i] :
      ( ( ( instance_THFTYPE_IiioI @ LIST1 @ lList_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ LIST2 @ lList_THFTYPE_i )
        & ! [NUMBER: $i] :
            ( ( lListOrderFn_THFTYPE_IiiiI @ LIST1 @ NUMBER )
            = ( lListOrderFn_THFTYPE_IiiiI @ LIST2 @ NUMBER ) ) )
     => ( LIST1 = LIST2 ) ) )).

%KIF documentation:(documentation Integer EnglishLanguage "A negative or nonnegative whole number.")
thf(ax_087,axiom,
    ( range_THFTYPE_IiioI @ lWhenFn_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

%KIF documentation:(documentation Organism EnglishLanguage "Generally, a living individual, including all &%Plants and &%Animals.")
thf(ax_088,axiom,(
    ! [INTERVAL1: $i,INTERVAL2: $i] :
      ( ( meetsTemporally_THFTYPE_IiioI @ INTERVAL1 @ INTERVAL2 )
    <=> ( ( lEndFn_THFTYPE_IiiI @ INTERVAL1 )
        = ( lBeginFn_THFTYPE_IiiI @ INTERVAL2 ) ) ) )).

%KIF documentation:(documentation inList EnglishLanguage "The analog of &%element and &%instance for &%Lists. (&%inList ?OBJ ?LIST) means that ?OBJ is in the &%List ?LIST. For example, (&%inList &%Tuesday (&%ListFn &%Monday &%Tuesday &%Wednesday)) would be true.")
thf(ax_089,axiom,
    ( subclass_THFTYPE_IiioI @ lSymbolicString_THFTYPE_i @ lContentBearingObject_THFTYPE_i )).

%KIF documentation:(documentation SymbolicString EnglishLanguage "The &%Class of alphanumeric sequences.")
thf(ax_090,axiom,
    ( range_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_091,axiom,(
    ? [THING: $i] :
      ( instance_THFTYPE_IiioI @ THING @ lEntity_THFTYPE_i ) )).

thf(ax_092,axiom,
    ( inverse_THFTYPE_IIiioIIiioIoI @ husband_THFTYPE_IiioI @ wife_THFTYPE_IiioI )).

thf(ax_093,axiom,
    ( subclass_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i @ lTransitiveRelation_THFTYPE_i )).

thf(ax_094,axiom,(
    ! [NUMBER: $i,CLASS: $i,REL: $i > $o,ROW: $i] :
      ( ( ( domainSubclass_THFTYPE_IIioIiioI @ REL @ NUMBER @ CLASS )
        & ( REL @ ROW ) )
     => ( subclass_THFTYPE_IiioI @ ( lListOrderFn_THFTYPE_IiiiI @ ( lListFn_THFTYPE_IiiI @ ROW ) @ NUMBER ) @ CLASS ) ) )).

thf(ax_095,axiom,
    ( subclass_THFTYPE_IiioI @ lHumanLanguage_THFTYPE_i @ lLanguage_THFTYPE_i )).

%KIF documentation:(documentation List EnglishLanguage "Every &%List is a particular ordered n-tuple of items. Generally speaking, &%Lists are created by means of the &%ListFn &%Function, which takes any number of items as arguments and returns a &%List with the items in the same order. Anything, including other &%Lists, may be an item in a &%List. Note too that &%Lists are extensional - two lists that have the same items in the same order are identical. Note too that a &%List may contain no items. In that case, the &%List is the &%NullList.")
thf(ax_096,axiom,
    ( subclass_THFTYPE_IiioI @ lTernaryPredicate_THFTYPE_i @ lInheritableRelation_THFTYPE_i )).

%KIF documentation:(documentation wife EnglishLanguage "(&%wife ?WOMAN ?MAN) means that ?WOMAN is the wife of ?MAN.")
%KIF documentation:(documentation SubtractionFn EnglishLanguage "If ?NUMBER1 and ?NUMBER2 are &%Numbers, then (&%SubtractionFn ?NUMBER1 ?NUMBER2) is the arithmetical difference between ?NUMBER1 and ?NUMBER2, i.e. ?NUMBER1 minus ?NUMBER2. An exception occurs when ?NUMBER1 is equal to 0, in which case (&%SubtractionFn ?NUMBER1 ?NUMBER2) is the negation of ?NUMBER2.")
%KIF documentation:(documentation IrreflexiveRelation EnglishLanguage "&%Relation ?REL is irreflexive iff (?REL ?INST ?INST) holds for no value of ?INST.")
%KIF documentation:(documentation Day EnglishLanguage "The &%Class of all calendar &%Days.")
%KIF documentation:(documentation contraryAttribute EnglishLanguage "A &%contraryAttribute is a set of &%Attributes such that something can not simultaneously have more than one of these &%Attributes. For example, (&%contraryAttribute &%Pliable &%Rigid) means that nothing can be both &%Pliable and &%Rigid.")
%KIF documentation:(documentation duration EnglishLanguage "(&%duration ?POS ?TIME) means that the duration of the &%TimePosition ?POS is ?TIME. Note that this &%Predicate can be used in conjunction with the &%Function &%WhenFn to specify the duration of any instance of &%Physical.")
thf(ax_097,axiom,
    ( partition_THFTYPE_IiiioI @ lPhysical_THFTYPE_i @ lObject_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_098,axiom,(
    ! [REL2: $i,CLASS1: $i,REL1: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ REL1 @ REL2 )
        & ( rangeSubclass_THFTYPE_IiioI @ REL2 @ CLASS1 ) )
     => ( rangeSubclass_THFTYPE_IiioI @ REL1 @ CLASS1 ) ) )).

%KIF documentation:(documentation Quantity EnglishLanguage "Any specification of how many or how much of something there is. Accordingly, there are two subclasses of &%Quantity: &%Number (how many) and &%PhysicalQuantity (how much).")
thf(ax_099,axiom,(
    ! [NUMBER: $i,CLASS: $i,REL: $i > $o,ROW: $i] :
      ( ( ( domain_THFTYPE_IIioIiioI @ REL @ NUMBER @ CLASS )
        & ( REL @ ROW ) )
     => ( instance_THFTYPE_IiioI @ ( lListOrderFn_THFTYPE_IiiiI @ ( lListFn_THFTYPE_IiiI @ ROW ) @ NUMBER ) @ CLASS ) ) )).

thf(ax_100,axiom,(
    ! [REL2: $i > $o,ROW: $i,REL1: $i > $o] :
      ( ( ( subrelation_THFTYPE_IIioIIioIoI @ REL1 @ REL2 )
        & ( REL1 @ ROW ) )
     => ( REL2 @ ROW ) ) )).

%KIF documentation:(documentation subProcess EnglishLanguage "(&%subProcess ?SUBPROC ?PROC) means that ?SUBPROC is a subprocess of ?PROC. A subprocess is here understood as a temporally distinguished part (proper or not) of a &%Process.")
%KIF documentation:(documentation UnitOfMeasure EnglishLanguage "A standard of measurement for some dimension. For example, the &%Meter is a &%UnitOfMeasure for the dimension of length, as is the &%Inch. There is no intrinsic property of a &%UnitOfMeasure that makes it primitive or fundamental, rather, a system of units (e.g. &%SystemeInternationalUnit) defines a set of orthogonal dimensions and assigns units for each.")
thf(ax_101,axiom,(
    ! [CLASS1: $i,REL: $i,CLASS2: $i] :
      ( ( ( range_THFTYPE_IiioI @ REL @ CLASS1 )
        & ( range_THFTYPE_IiioI @ REL @ CLASS2 ) )
     => ( ( subclass_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
        | ( subclass_THFTYPE_IiioI @ CLASS2 @ CLASS1 ) ) ) )).

thf(ax_102,axiom,(
    ! [CLASS1: $i,CLASS2: $i] :
      ( ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 )
    <=> ! [INST: $i] :
          ( ~
          @ ( ( instance_THFTYPE_IiioI @ INST @ CLASS1 )
            & ( instance_THFTYPE_IiioI @ INST @ CLASS2 ) ) ) ) )).

%KIF documentation:(documentation subAttribute EnglishLanguage "Means that the second argument can be ascribed to everything which has the first argument ascribed to it.")
%KIF documentation:(documentation AdditionFn EnglishLanguage "If ?NUMBER1 and ?NUMBER2 are &%Numbers, then (&%AdditionFn ?NUMBER1 ?NUMBER2) is the arithmetical sum of these numbers.")
%KIF documentation:(documentation relatedInternalConcept EnglishLanguage "Means that the two arguments are related concepts within the SUMO, i.e. there is a significant similarity of meaning between them. To indicate a meaning relation between a SUMO concept and a concept from another source, use the Predicate &%relatedExternalConcept.")
%KIF documentation:(documentation BinaryPredicate EnglishLanguage "A &%Predicate relating two items - its valence is two.")
thf(ax_103,axiom,
    ( subclass_THFTYPE_IiioI @ lOrganization_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

%KIF documentation:(documentation MeasureFn EnglishLanguage "This &%BinaryFunction maps a &%RealNumber and a &%UnitOfMeasure to that &%Number of units. It is used to express `measured' instances of &%PhysicalQuantity. Example: the concept of three meters is represented as (&%MeasureFn 3 &%Meter).")
thf(ax_104,axiom,(
    ! [ROW: $i,ELEMENT: $i] :
      ( ( disjointDecomposition_THFTYPE_IioI @ ROW )
     => ( ( inList_THFTYPE_IiioI @ ELEMENT @ ( lListFn_THFTYPE_IiiI @ ROW ) )
       => ( instance_THFTYPE_IiioI @ ELEMENT @ lClass_THFTYPE_i ) ) ) )).

%KIF documentation:(documentation part EnglishLanguage "The basic mereological relation. All other mereological relations are defined in terms of this one. (&%part ?PART ?WHOLE) simply means that the &%Object ?PART is part of the &%Object ?WHOLE. Note that, since &%part is a &%ReflexiveRelation, every &%Object is a part of itself.")
%KIF documentation:(documentation Class EnglishLanguage "&%Classes differ from &%Sets in three important respects. First, &%Classes are not assumed to be extensional. That is, distinct &%Classes might well have exactly the same instances. Second, &%Classes typically have an associated `condition' that determines the instances of the &%Class. So, for example, the condition `human' determines the &%Class of &%Humans. Note that some &%Classes might satisfy their own condition (e.g., the &%Class of &%Abstract things is &%Abstract) and hence be instances of themselves. Third, the instances of a class may occur only once within the class, i.e. a class cannot contain duplicate instances.")
thf(ax_105,axiom,(
    ! [OBJ: $i,NUMBER2: $i,ROW: $i,NUMBER1: $i] :
      ( ( contraryAttribute_THFTYPE_IioI @ ROW )
     => ! [ATTR1: $i,ATTR2: $i] :
          ( ( ( ATTR1
              = ( lListOrderFn_THFTYPE_IiiiI @ ( lListFn_THFTYPE_IiiI @ ROW ) @ NUMBER1 ) )
            & ( ATTR2
              = ( lListOrderFn_THFTYPE_IiiiI @ ( lListFn_THFTYPE_IiiI @ ROW ) @ NUMBER2 ) )
            & ( ~ @ ( NUMBER1 = NUMBER2 ) ) )
         => ( ( property_THFTYPE_IiioI @ OBJ @ ATTR1 )
           => ( ~ @ ( property_THFTYPE_IiioI @ OBJ @ ATTR2 ) ) ) ) ) )).

thf(ax_106,axiom,
    ( subclass_THFTYPE_IiioI @ lLinguisticExpression_THFTYPE_i @ lContentBearingPhysical_THFTYPE_i )).

%KIF documentation:(documentation CognitiveAgent EnglishLanguage "A &%SentientAgent with responsibilities and the ability to reason, deliberate, make plans, etc. This is essentially the legal/ ethical notion of a person. Note that, although &%Human is a subclass of &%CognitiveAgent, there may be instances of &%CognitiveAgent which are not also instances of &%Human. For example, chimpanzees, gorillas, dolphins, whales, and some extraterrestrials (if they exist) may be &%CognitiveAgents.")
%KIF documentation:(documentation truth EnglishLanguage "The &%BinaryPredicate that relates a &%Sentence to its &%TruthValue.")
thf(ax_107,axiom,(
    ! [REL2: $i,NUMBER: $i,CLASS1: $i,REL1: $i] :
      ( ( ( subrelation_THFTYPE_IiioI @ REL1 @ REL2 )
        & ( domainSubclass_THFTYPE_IiiioI @ REL2 @ NUMBER @ CLASS1 ) )
     => ( domainSubclass_THFTYPE_IiiioI @ REL1 @ NUMBER @ CLASS1 ) ) )).

%KIF documentation:(documentation HumanLanguage EnglishLanguage "The &%subclass of &%Languages used by &%Humans.")
%KIF documentation:(documentation ContentBearingObject EnglishLanguage "Any &%SelfConnectedObject that expresses content. This content may be a &%Proposition, e.g. when the &%ContentBearingObject is a &%Sentence or &%Text, or it may be a representation of an abstract or physical object, as with an &%Icon, a &%Word or a &%Phrase.")
%KIF documentation:(documentation BeginFn EnglishLanguage "A &%UnaryFunction that maps a &%TimeInterval to the &%TimePoint at which the interval begins.")
%KIF documentation:(documentation Month EnglishLanguage "The &%Class of all calendar &%Months.")
%KIF documentation:(documentation EnglishLanguage EnglishLanguage "A Germanic language that incorporates many roots from the Romance languages. It is the official language of the &%UnitedStates, the &%UnitedKingdom, and many other countries.")
%KIF documentation:(documentation TransitiveRelation EnglishLanguage "A &%BinaryRelation ?REL is transitive if (?REL ?INST1 ?INST2) and (?REL ?INST2 ?INST3) imply (?REL ?INST1 ?INST3), for all ?INST1, ?INST2, and ?INST3.")
thf(ax_108,axiom,(
    ! [LANG: $i,AGENT: $i,PROC: $i] :
      ( ( ( instance_THFTYPE_IiioI @ LANG @ lHumanLanguage_THFTYPE_i )
        & ( agent_THFTYPE_IiioI @ PROC @ AGENT )
        & ( instrument_THFTYPE_IiioI @ PROC @ LANG ) )
     => ( instance_THFTYPE_IiioI @ AGENT @ lHuman_THFTYPE_i ) ) )).

%KIF documentation:(documentation knows EnglishLanguage "The epistemic predicate of knowing. (&%knows ?AGENT ?FORMULA) means that ?AGENT knows the proposition expressed by ?FORMULA. Note that &%knows entails conscious awareness, so this &%Predicate cannot be used to express tacit or subconscious or unconscious knowledge.")
%KIF documentation:(documentation Year EnglishLanguage "The &%Class of all calendar &%Years.")
thf(ax_109,axiom,(
    ! [ATTR2: $i,ATTR1: $i] :
      ( ( subAttribute_THFTYPE_IiioI @ ATTR1 @ ATTR2 )
     => ! [OBJ: $i] :
          ( ( property_THFTYPE_IiioI @ OBJ @ ATTR1 )
         => ( property_THFTYPE_IiioI @ OBJ @ ATTR2 ) ) ) )).

%KIF documentation:(documentation SelfConnectedObject EnglishLanguage "A &%SelfConnectedObject is any &%Object that does not consist of two or more disconnected parts.")
thf(ax_110,axiom,
    ( holdsDuring_THFTYPE_IiooI @ ( lYearFn_THFTYPE_IiiI @ n2009_THFTYPE_i ) @ ( wife_THFTYPE_IiioI @ lCorina_THFTYPE_i @ lChris_THFTYPE_i ) )).

thf(ax_111,axiom,
    ( holdsDuring_THFTYPE_IiooI @ ( lYearFn_THFTYPE_IiiI @ n2009_THFTYPE_i ) @ ( wife_THFTYPE_IiioI @ lCorina_THFTYPE_i @ lChris_THFTYPE_i ) )).

thf(ax_112,axiom,(
    ! [TEXT: $i] :
      ( ( instance_THFTYPE_IiioI @ TEXT @ lText_THFTYPE_i )
     => ? [PART: $i] :
          ( ( part_THFTYPE_IiioI @ PART @ TEXT )
          & ( instance_THFTYPE_IiioI @ PART @ lLinguisticExpression_THFTYPE_i ) ) ) )).

%KIF documentation:(documentation True EnglishLanguage "The &%TruthValue of being true.")
thf(ax_113,axiom,
    ( inverse_THFTYPE_IIiioIIiioIoI @ greaterThanOrEqualTo_THFTYPE_IiioI @ lessThanOrEqualTo_THFTYPE_IiioI )).

thf(ax_114,axiom,
    ( inverse_THFTYPE_IIiioIIiioIoI @ greaterThan_THFTYPE_IiioI @ lessThan_THFTYPE_IiioI )).

thf(ax_115,axiom,
    ( subclass_THFTYPE_IiioI @ lSymmetricRelation_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

%KIF documentation:(documentation meetsTemporally EnglishLanguage "(&%meetsTemporally ?INTERVAL1 ?INTERVAL2) means that the terminal point of the &%TimeInterval ?INTERVAL1 is the initial point of the &%TimeInterval ?INTERVAL2.")
thf(ax_116,axiom,(
    ! [OBJ: $i,PROCESS: $i] :
      ( ( located_THFTYPE_IiioI @ PROCESS @ OBJ )
     => ! [SUB: $i] :
          ( ( subProcess_THFTYPE_IiioI @ SUB @ PROCESS )
         => ( located_THFTYPE_IiioI @ SUB @ OBJ ) ) ) )).

thf(ax_117,axiom,
    ( subclass_THFTYPE_IiioI @ lDay_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

%KIF documentation:(documentation containsInformation EnglishLanguage "A subrelation of &%represents. This predicate relates a &%ContentBearingPhysical to the &%Proposition that is expressed by the &%ContentBearingPhysical. Examples include the relationships between a physical novel and its story and between a printed score and its musical content.")
thf(ax_118,axiom,(
    ! [ROW2: $i,LIST2: $i,LIST1: $i,ROW1: $i] :
      ( ( LIST1 = LIST2 )
     => ( ( ( LIST1
            = ( lListFn_THFTYPE_IiiI @ ROW1 ) )
          & ( LIST2
            = ( lListFn_THFTYPE_IiiI @ ROW2 ) ) )
       => ! [NUMBER: $i] :
            ( ( lListOrderFn_THFTYPE_IiiiI @ ( lListFn_THFTYPE_IiiI @ ROW1 ) @ NUMBER )
            = ( lListOrderFn_THFTYPE_IiiiI @ ( lListFn_THFTYPE_IiiI @ ROW2 ) @ NUMBER ) ) ) ) )).

thf(ax_119,axiom,(
    ! [REL2: $i,NUMBER: $i,CLASS1: $i,CLASS2: $i,REL1: $i] :
      ( ( ( domain_THFTYPE_IiiioI @ REL1 @ NUMBER @ CLASS1 )
        & ( domain_THFTYPE_IiiioI @ REL2 @ NUMBER @ CLASS2 )
        & ( disjoint_THFTYPE_IiioI @ CLASS1 @ CLASS2 ) )
     => ( disjointRelation_THFTYPE_IiioI @ REL1 @ REL2 ) ) )).

%KIF documentation:(documentation Attribute EnglishLanguage "Qualities which we cannot or choose not to reify into subclasses of &%Object.")
thf(ax_120,axiom,(
    ! [CLASS: $i,ROW: $i] :
      ( ( disjointDecomposition_THFTYPE_IiioI @ CLASS @ ROW )
     => ! [ITEM1: $i,ITEM2: $i] :
          ( ( ( inList_THFTYPE_IiioI @ ITEM1 @ ( lListFn_THFTYPE_IiiI @ ROW ) )
            & ( inList_THFTYPE_IiioI @ ITEM2 @ ( lListFn_THFTYPE_IiiI @ ROW ) )
            & ( ~ @ ( ITEM1 = ITEM2 ) ) )
         => ( disjoint_THFTYPE_IiioI @ ITEM1 @ ITEM2 ) ) ) )).

%KIF documentation:(documentation Human EnglishLanguage "Modern man, the only remaining species of the Homo genus.")
thf(ax_121,axiom,
    ( rangeSubclass_THFTYPE_IiioI @ lYearFn_THFTYPE_i @ lYear_THFTYPE_i )).

thf(ax_122,axiom,(
    ! [ITEM: $i,LIST: $i] :
      ( ( inList_THFTYPE_IiioI @ ITEM @ LIST )
     => ? [NUMBER: $i] :
          ( ( lListOrderFn_THFTYPE_IiiiI @ LIST @ NUMBER )
          = ITEM ) ) )).

thf(ax_123,axiom,(
    ! [REL: $i > $i > $o,NUMBER2: $i,NUMBER1: $i] :
      ( ( ( instance_THFTYPE_IIiioIioI @ REL @ lRelationExtendedToQuantities_THFTYPE_i )
        & ( instance_THFTYPE_IIiioIioI @ REL @ lBinaryRelation_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ NUMBER1 @ lRealNumber_THFTYPE_i )
        & ( instance_THFTYPE_IiioI @ NUMBER2 @ lRealNumber_THFTYPE_i )
        & ( REL @ NUMBER1 @ NUMBER2 ) )
     => ! [UNIT: $i] :
          ( ( instance_THFTYPE_IiioI @ UNIT @ lUnitOfMeasure_THFTYPE_i )
         => ( REL @ ( lMeasureFn_THFTYPE_IiiiI @ NUMBER1 @ UNIT ) @ ( lMeasureFn_THFTYPE_IiiiI @ NUMBER2 @ UNIT ) ) ) ) )).

thf(ax_124,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i )).

thf(ax_125,axiom,
    ( instance_THFTYPE_IiioI @ connected_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_126,axiom,
    ( instance_THFTYPE_IiioI @ containsInformation_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_127,axiom,
    ( instance_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_128,axiom,
    ( instance_THFTYPE_IIiioIioI @ duration_THFTYPE_IiioI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_129,axiom,
    ( instance_THFTYPE_IIiioIioI @ range_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_130,axiom,
    ( domain_THFTYPE_IIiioIiioI @ disjointRelation_THFTYPE_IiioI @ n2_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_131,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThanOrEqualTo_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_132,axiom,
    ( domainSubclass_THFTYPE_IiiioI @ lMonthFn_THFTYPE_i @ n2_THFTYPE_i @ lYear_THFTYPE_i )).

thf(ax_133,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subProcess_THFTYPE_IiioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_134,axiom,
    ( relatedInternalConcept_THFTYPE_IiioI @ lMonth_THFTYPE_i @ lMonthFn_THFTYPE_i )).

thf(ax_135,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThan_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_136,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThanOrEqualTo_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_137,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThanOrEqualTo_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_138,axiom,
    ( subrelation_THFTYPE_IIiioIioI @ husband_THFTYPE_IiioI @ spouse_THFTYPE_i )).

thf(ax_139,axiom,
    ( relatedInternalConcept_THFTYPE_IiIiioIoI @ relatedExternalConcept_THFTYPE_i @ relatedInternalConcept_THFTYPE_IiioI )).

thf(ax_140,axiom,
    ( domain_THFTYPE_IIiiiIiioI @ lMeasureFn_THFTYPE_IiiiI @ n2_THFTYPE_i @ lUnitOfMeasure_THFTYPE_i )).

thf(ax_141,axiom,
    ( domain_THFTYPE_IIioIiioI @ disjointDecomposition_THFTYPE_IioI @ n1_THFTYPE_i @ lClass_THFTYPE_i )).

thf(ax_142,axiom,
    ( instance_THFTYPE_IIiioIioI @ relatedInternalConcept_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_143,axiom,
    ( instance_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_144,axiom,
    ( instance_THFTYPE_IIiiiIioI @ lListOrderFn_THFTYPE_IiiiI @ lBinaryFunction_THFTYPE_i )).

thf(ax_145,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThan_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_146,axiom,
    ( domain_THFTYPE_IIiioIiioI @ greaterThan_THFTYPE_IiioI @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_147,axiom,
    ( domain_THFTYPE_IIiioIiioI @ range_THFTYPE_IiioI @ n2_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_148,axiom,
    ( instance_THFTYPE_IiioI @ containsInformation_THFTYPE_i @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_149,axiom,
    ( instance_THFTYPE_IIiiIioI @ lYearFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_150,axiom,
    ( instance_THFTYPE_IiioI @ attribute_THFTYPE_i @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_151,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_152,axiom,
    ( instance_THFTYPE_IiioI @ modalAttribute_THFTYPE_i @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_153,axiom,
    ( domain_THFTYPE_IIiioIiioI @ greaterThanOrEqualTo_THFTYPE_IiioI @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_154,axiom,
    ( domain_THFTYPE_IiiioI @ lMultiplicationFn_THFTYPE_i @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_155,axiom,
    ( domain_THFTYPE_IiiioI @ patient_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_156,axiom,
    ( domain_THFTYPE_IIiooIiioI @ believes_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_157,axiom,
    ( domain_THFTYPE_IiiioI @ documentation_THFTYPE_i @ n3_THFTYPE_i @ lSymbolicString_THFTYPE_i )).

thf(ax_158,axiom,
    ( instance_THFTYPE_IIiiIioI @ lCardinalityFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_159,axiom,
    ( instance_THFTYPE_IIiioIioI @ temporalPart_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_160,axiom,
    ( domain_THFTYPE_IIiioIiioI @ property_THFTYPE_IiioI @ n2_THFTYPE_i @ lAttribute_THFTYPE_i )).

thf(ax_161,axiom,
    ( domain_THFTYPE_IIiioIiioI @ member_THFTYPE_IiioI @ n1_THFTYPE_i @ lSelfConnectedObject_THFTYPE_i )).

thf(ax_162,axiom,
    ( domain_THFTYPE_IiiioI @ patient_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_163,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subAttribute_THFTYPE_IiioI @ n1_THFTYPE_i @ lAttribute_THFTYPE_i )).

thf(ax_164,axiom,
    ( domain_THFTYPE_IIIiioIIiioIoIiioI @ inverse_THFTYPE_IIiioIIiioIoI @ n1_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

thf(ax_165,axiom,
    ( instance_THFTYPE_IIiioIioI @ part_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_166,axiom,
    ( instance_THFTYPE_IiioI @ documentation_THFTYPE_i @ lTernaryPredicate_THFTYPE_i )).

thf(ax_167,axiom,
    ( instance_THFTYPE_IIiioIioI @ inList_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_168,axiom,
    ( instance_THFTYPE_IIiioIioI @ duration_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_169,axiom,
    ( instance_THFTYPE_IiioI @ relatedExternalConcept_THFTYPE_i @ lTernaryPredicate_THFTYPE_i )).

thf(ax_170,axiom,
    ( instance_THFTYPE_IiioI @ spouse_THFTYPE_i @ lSymmetricRelation_THFTYPE_i )).

thf(ax_171,axiom,
    ( domain_THFTYPE_IIIiioIIiioIoIiioI @ inverse_THFTYPE_IIiioIIiioIoI @ n2_THFTYPE_i @ lBinaryRelation_THFTYPE_i )).

thf(ax_172,axiom,
    ( relatedInternalConcept_THFTYPE_IIiioIIiioIoI @ disjointRelation_THFTYPE_IiioI @ disjoint_THFTYPE_IiioI )).

thf(ax_173,axiom,
    ( domain_THFTYPE_IIiioIiioI @ greaterThan_THFTYPE_IiioI @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_174,axiom,
    ( relatedInternalConcept_THFTYPE_IIioIIiioIoI @ disjointDecomposition_THFTYPE_IioI @ disjoint_THFTYPE_IiioI )).

thf(ax_175,axiom,
    ( subrelation_THFTYPE_IIiioIIiioIoI @ member_THFTYPE_IiioI @ part_THFTYPE_IiioI )).

thf(ax_176,axiom,
    ( instance_THFTYPE_IIiioIioI @ subclass_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_177,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_178,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lBeginFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_179,axiom,
    ( instance_THFTYPE_IIiioIioI @ wife_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_180,axiom,
    ( instance_THFTYPE_IiioI @ attribute_THFTYPE_i @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_181,axiom,
    ( instance_THFTYPE_IIiooIioI @ holdsDuring_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_182,axiom,
    ( domain_THFTYPE_IIiioIiioI @ instance_THFTYPE_IiioI @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_183,axiom,
    ( domain_THFTYPE_IIiioIiioI @ lessThan_THFTYPE_IiioI @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_184,axiom,
    ( domain_THFTYPE_IiiioI @ containsInformation_THFTYPE_i @ n1_THFTYPE_i @ lContentBearingPhysical_THFTYPE_i )).

thf(ax_185,axiom,
    ( domain_THFTYPE_IIiioIiioI @ part_THFTYPE_IiioI @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_186,axiom,
    ( instance_THFTYPE_IIiioIioI @ temporalPart_THFTYPE_IiioI @ lTemporalRelation_THFTYPE_i )).

thf(ax_187,axiom,
    ( subrelation_THFTYPE_IiIiioIoI @ modalAttribute_THFTYPE_i @ property_THFTYPE_IiioI )).

thf(ax_188,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lYearFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lInteger_THFTYPE_i )).

thf(ax_189,axiom,
    ( domain_THFTYPE_IiiioI @ relatedExternalConcept_THFTYPE_i @ n1_THFTYPE_i @ lSymbolicString_THFTYPE_i )).

thf(ax_190,axiom,
    ( instance_THFTYPE_IIIiioIiioIioI @ domain_THFTYPE_IIiioIiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_191,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ domain_THFTYPE_IiiioI @ n3_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_192,axiom,
    ( instance_THFTYPE_IIiioIioI @ inList_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_193,axiom,
    ( instance_THFTYPE_IIiioIioI @ subAttribute_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_194,axiom,
    ( instance_THFTYPE_IIiiIioI @ lCardinalityFn_THFTYPE_IiiI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_195,axiom,
    ( subrelation_THFTYPE_IiIiioIoI @ attribute_THFTYPE_i @ property_THFTYPE_IiioI )).

thf(ax_196,axiom,
    ( instance_THFTYPE_IIiioIioI @ temporalPart_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_197,axiom,
    ( domain_THFTYPE_IiiioI @ relatedExternalConcept_THFTYPE_i @ n3_THFTYPE_i @ lLanguage_THFTYPE_i )).

thf(ax_198,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_199,axiom,
    ( instance_THFTYPE_IIiioIioI @ disjointRelation_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_200,axiom,
    ( instance_THFTYPE_IiioI @ lMonthFn_THFTYPE_i @ lTemporalRelation_THFTYPE_i )).

thf(ax_201,axiom,
    ( domain_THFTYPE_IiiioI @ lMultiplicationFn_THFTYPE_i @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_202,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_203,axiom,
    ( instance_THFTYPE_IIiioIioI @ subclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_204,axiom,
    ( instance_THFTYPE_IIiiIioI @ lEndFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_205,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subclass_THFTYPE_IiioI @ n2_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_206,axiom,
    ( instance_THFTYPE_IIiooIioI @ believes_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_207,axiom,
    ( domain_THFTYPE_IIiiiIiioI @ lTemporalCompositionFn_THFTYPE_IiiiI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_208,axiom,
    ( instance_THFTYPE_IIiiiIioI @ lTemporalCompositionFn_THFTYPE_IiiiI @ lBinaryFunction_THFTYPE_i )).

thf(ax_209,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subclass_THFTYPE_IiioI @ n1_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_210,axiom,
    ( domainSubclass_THFTYPE_IIiiiIiioI @ lTemporalCompositionFn_THFTYPE_IiiiI @ n2_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_211,axiom,
    ( instance_THFTYPE_IIiiIioI @ lYearFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_212,axiom,
    ( instance_THFTYPE_IiioI @ spouse_THFTYPE_i @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_213,axiom,
    ( instance_THFTYPE_IIiiiIioI @ lMeasureFn_THFTYPE_IiiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_214,axiom,
    ( domain_THFTYPE_IIiioIiioI @ instrument_THFTYPE_IiioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_215,axiom,
    ( instance_THFTYPE_IiioI @ lSubtractionFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_216,axiom,
    ( domain_THFTYPE_IIiioIiioI @ instance_THFTYPE_IiioI @ n2_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_217,axiom,
    ( domain_THFTYPE_IIiioIiioI @ duration_THFTYPE_IiioI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_218,axiom,
    ( instance_THFTYPE_IIiioIioI @ member_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_219,axiom,
    ( domain_THFTYPE_IIiioIiioI @ agent_THFTYPE_IiioI @ n2_THFTYPE_i @ lAgent_THFTYPE_i )).

thf(ax_220,axiom,
    ( domain_THFTYPE_IIiiiIiioI @ lListOrderFn_THFTYPE_IiiiI @ n1_THFTYPE_i @ lList_THFTYPE_i )).

thf(ax_221,axiom,
    ( domain_THFTYPE_IIiioIiioI @ meetsTemporally_THFTYPE_IiioI @ n2_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_222,axiom,
    ( domain_THFTYPE_IIiioIiioI @ lessThan_THFTYPE_IiioI @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_223,axiom,
    ( domainSubclass_THFTYPE_IIiioIiioI @ rangeSubclass_THFTYPE_IiioI @ n2_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_224,axiom,
    ( instance_THFTYPE_IIiooIioI @ holdsDuring_THFTYPE_IiooI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_225,axiom,
    ( domain_THFTYPE_IiiioI @ modalAttribute_THFTYPE_i @ n1_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_226,axiom,
    ( subrelation_THFTYPE_IIiioIioI @ instrument_THFTYPE_IiioI @ patient_THFTYPE_i )).

thf(ax_227,axiom,
    ( domain_THFTYPE_IIiioIiioI @ disjoint_THFTYPE_IiioI @ n1_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_228,axiom,
    ( domain_THFTYPE_IIiooIiioI @ knows_THFTYPE_IiooI @ n1_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_229,axiom,
    ( domain_THFTYPE_IiiioI @ connected_THFTYPE_i @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_230,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_231,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ orientation_THFTYPE_IiiioI @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_232,axiom,
    ( instance_THFTYPE_IiioI @ modalAttribute_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_233,axiom,
    ( domain_THFTYPE_IiiioI @ lAdditionFn_THFTYPE_i @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_234,axiom,
    ( instance_THFTYPE_IiioI @ modalAttribute_THFTYPE_i @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_235,axiom,
    ( domain_THFTYPE_IIIioIiioIiioI @ domainSubclass_THFTYPE_IIioIiioI @ n3_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_236,axiom,
    ( instance_THFTYPE_IIiioIioI @ rangeSubclass_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_237,axiom,
    ( instance_THFTYPE_IIiioIioI @ subrelation_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_238,axiom,
    ( instance_THFTYPE_IiioI @ lMonthFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_239,axiom,
    ( instance_THFTYPE_IIiioIioI @ meetsTemporally_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_240,axiom,
    ( domain_THFTYPE_IiiioI @ lKappaFn_THFTYPE_i @ n1_THFTYPE_i @ lSymbolicString_THFTYPE_i )).

thf(ax_241,axiom,
    ( domain_THFTYPE_IIiioIiioI @ meetsTemporally_THFTYPE_IiioI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_242,axiom,
    ( domain_THFTYPE_IiiioI @ equal_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_243,axiom,
    ( domain_THFTYPE_IIiioIiioI @ disjoint_THFTYPE_IiioI @ n2_THFTYPE_i @ lSetOrClass_THFTYPE_i )).

thf(ax_244,axiom,
    ( instance_THFTYPE_IiioI @ lSubtractionFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_245,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subProcess_THFTYPE_IiioI @ n2_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_246,axiom,
    ( domain_THFTYPE_IiiioI @ spouse_THFTYPE_i @ n1_THFTYPE_i @ lHuman_THFTYPE_i )).

thf(ax_247,axiom,
    ( domain_THFTYPE_IIiioIiioI @ agent_THFTYPE_IiioI @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_248,axiom,
    ( instance_THFTYPE_IiioI @ equal_THFTYPE_i @ lBinaryPredicate_THFTYPE_i )).

thf(ax_249,axiom,
    ( instance_THFTYPE_IIiioIioI @ subProcess_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_250,axiom,
    ( relatedInternalConcept_THFTYPE_IiioI @ lContentBearingObject_THFTYPE_i @ containsInformation_THFTYPE_i )).

thf(ax_251,axiom,
    ( domain_THFTYPE_IIiioIiioI @ disjointRelation_THFTYPE_IiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_252,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThanOrEqualTo_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_253,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subrelation_THFTYPE_IiioI @ n2_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_254,axiom,
    ( subrelation_THFTYPE_IiioI @ result_THFTYPE_i @ patient_THFTYPE_i )).

thf(ax_255,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThan_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_256,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_257,axiom,
    ( domain_THFTYPE_IIiooIiioI @ knows_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_258,axiom,
    ( instance_THFTYPE_IIiiioIioI @ orientation_THFTYPE_IiiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_259,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ domain_THFTYPE_IiiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_260,axiom,
    ( instance_THFTYPE_IiioI @ connected_THFTYPE_i @ lSymmetricRelation_THFTYPE_i )).

thf(ax_261,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThan_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_262,axiom,
    ( domain_THFTYPE_IiiioI @ connected_THFTYPE_i @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_263,axiom,
    ( instance_THFTYPE_IIiooIioI @ knows_THFTYPE_IiooI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_264,axiom,
    ( domain_THFTYPE_IIiioIiioI @ lessThanOrEqualTo_THFTYPE_IiioI @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_265,axiom,
    ( instance_THFTYPE_IiioI @ lKappaFn_THFTYPE_i @ lBinaryFunction_THFTYPE_i )).

thf(ax_266,axiom,
    ( domain_THFTYPE_IiiioI @ result_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_267,axiom,
    ( instance_THFTYPE_IiioI @ lMultiplicationFn_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_268,axiom,
    ( instance_THFTYPE_IIiiiIioI @ lTemporalCompositionFn_THFTYPE_IiiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_269,axiom,
    ( domain_THFTYPE_IIiioIiioI @ relatedInternalConcept_THFTYPE_IiioI @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_270,axiom,
    ( instance_THFTYPE_IIiioIioI @ subrelation_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_271,axiom,
    ( instance_THFTYPE_IIiioIioI @ husband_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_272,axiom,
    ( domain_THFTYPE_IIiioIiioI @ part_THFTYPE_IiioI @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_273,axiom,
    ( instance_THFTYPE_IIIiioIIiioIoIioI @ inverse_THFTYPE_IIiioIIiioIoI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_274,axiom,
    ( instance_THFTYPE_IiioI @ lSubtractionFn_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_275,axiom,
    ( domain_THFTYPE_IIiioIiioI @ property_THFTYPE_IiioI @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_276,axiom,
    ( instance_THFTYPE_IIiioIioI @ instance_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_277,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ partition_THFTYPE_IiiioI @ n1_THFTYPE_i @ lClass_THFTYPE_i )).

thf(ax_278,axiom,
    ( instance_THFTYPE_IIiioIioI @ disjoint_THFTYPE_IiioI @ lSymmetricRelation_THFTYPE_i )).

thf(ax_279,axiom,
    ( domain_THFTYPE_IIiioIiioI @ instrument_THFTYPE_IiioI @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_280,axiom,
    ( domain_THFTYPE_IiiioI @ lAdditionFn_THFTYPE_i @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_281,axiom,
    ( instance_THFTYPE_IIiioIioI @ range_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_282,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThanOrEqualTo_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_283,axiom,
    ( instance_THFTYPE_IIIiioIIiioIoIioI @ inverse_THFTYPE_IIiioIIiioIoI @ lSymmetricRelation_THFTYPE_i )).

thf(ax_284,axiom,
    ( domain_THFTYPE_IIiiiIiioI @ lMeasureFn_THFTYPE_IiiiI @ n1_THFTYPE_i @ lRealNumber_THFTYPE_i )).

thf(ax_285,axiom,
    ( instance_THFTYPE_IIiioIioI @ husband_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_286,axiom,
    ( instance_THFTYPE_IIiiIioI @ lCardinalityFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_287,axiom,
    ( domain_THFTYPE_IiiioI @ documentation_THFTYPE_i @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_288,axiom,
    ( instance_THFTYPE_IIiioIioI @ rangeSubclass_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_289,axiom,
    ( relatedInternalConcept_THFTYPE_IiIiiIoI @ lYear_THFTYPE_i @ lYearFn_THFTYPE_IiiI )).

thf(ax_290,axiom,
    ( domain_THFTYPE_IiiioI @ equal_THFTYPE_i @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_291,axiom,
    ( instance_THFTYPE_IIiioIioI @ disjoint_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_292,axiom,
    ( instance_THFTYPE_IIIiioIIiioIoIioI @ inverse_THFTYPE_IIiioIIiioIoI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_293,axiom,
    ( domain_THFTYPE_IiiioI @ relatedExternalConcept_THFTYPE_i @ n2_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_294,axiom,
    ( domain_THFTYPE_IiiioI @ documentation_THFTYPE_i @ n2_THFTYPE_i @ lHumanLanguage_THFTYPE_i )).

thf(ax_295,axiom,
    ( domain_THFTYPE_IIiioIiioI @ inList_THFTYPE_IiioI @ n2_THFTYPE_i @ lList_THFTYPE_i )).

thf(ax_296,axiom,
    ( instance_THFTYPE_IIiioIioI @ subProcess_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_297,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThan_THFTYPE_IiioI @ lTransitiveRelation_THFTYPE_i )).

thf(ax_298,axiom,
    ( domain_THFTYPE_IIiooIiioI @ holdsDuring_THFTYPE_IiooI @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_299,axiom,
    ( instance_THFTYPE_IIiioIioI @ greaterThan_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_300,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lUnaryFunction_THFTYPE_i )).

thf(ax_301,axiom,
    ( instance_THFTYPE_IIiiiIioI @ lMeasureFn_THFTYPE_IiiiI @ lBinaryFunction_THFTYPE_i )).

thf(ax_302,axiom,
    ( domain_THFTYPE_IIiioIiioI @ inList_THFTYPE_IiioI @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_303,axiom,
    ( instance_THFTYPE_IIiiIioI @ lBeginFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_304,axiom,
    ( domain_THFTYPE_IiiioI @ lKappaFn_THFTYPE_i @ n2_THFTYPE_i @ lFormula_THFTYPE_i )).

thf(ax_305,axiom,
    ( domainSubclass_THFTYPE_IiiioI @ lMonthFn_THFTYPE_i @ n1_THFTYPE_i @ lMonth_THFTYPE_i )).

thf(ax_306,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subAttribute_THFTYPE_IiioI @ n2_THFTYPE_i @ lAttribute_THFTYPE_i )).

thf(ax_307,axiom,
    ( domain_THFTYPE_IiiioI @ result_THFTYPE_i @ n1_THFTYPE_i @ lProcess_THFTYPE_i )).

thf(ax_308,axiom,
    ( instance_THFTYPE_IIiioIioI @ inList_THFTYPE_IiioI @ lIrreflexiveRelation_THFTYPE_i )).

thf(ax_309,axiom,
    ( domain_THFTYPE_IIiioIiioI @ lessThanOrEqualTo_THFTYPE_IiioI @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_310,axiom,
    ( domain_THFTYPE_IIiooIiioI @ believes_THFTYPE_IiooI @ n1_THFTYPE_i @ lCognitiveAgent_THFTYPE_i )).

thf(ax_311,axiom,
    ( instance_THFTYPE_IIiiIioI @ lEndFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_312,axiom,
    ( domain_THFTYPE_IIiioIiioI @ relatedInternalConcept_THFTYPE_IiioI @ n1_THFTYPE_i @ lEntity_THFTYPE_i )).

thf(ax_313,axiom,
    ( domain_THFTYPE_IiiioI @ lSubtractionFn_THFTYPE_i @ n2_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_314,axiom,
    ( instance_THFTYPE_IiioI @ equal_THFTYPE_i @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_315,axiom,
    ( domain_THFTYPE_IIiiioIiioI @ orientation_THFTYPE_IiiioI @ n2_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_316,axiom,
    ( relatedInternalConcept_THFTYPE_IiioI @ lDay_THFTYPE_i @ lDayDuration_THFTYPE_i )).

thf(ax_317,axiom,
    ( domain_THFTYPE_IIIioIiioIiioI @ domainSubclass_THFTYPE_IIioIiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_318,axiom,
    ( disjointRelation_THFTYPE_IiIiioIoI @ result_THFTYPE_i @ instrument_THFTYPE_IiioI )).

thf(ax_319,axiom,
    ( instance_THFTYPE_IIiioIioI @ duration_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_320,axiom,
    ( instance_THFTYPE_IiioI @ lAdditionFn_THFTYPE_i @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_321,axiom,
    ( instance_THFTYPE_IIiioIioI @ wife_THFTYPE_IiioI @ lAsymmetricRelation_THFTYPE_i )).

thf(ax_322,axiom,
    ( instance_THFTYPE_IIiioIioI @ subAttribute_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_323,axiom,
    ( domain_THFTYPE_IiiioI @ lSubtractionFn_THFTYPE_i @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

thf(ax_324,axiom,
    ( domain_THFTYPE_IIiioIiioI @ subrelation_THFTYPE_IiioI @ n1_THFTYPE_i @ lRelation_THFTYPE_i )).

thf(ax_325,axiom,
    ( instance_THFTYPE_IIIioIiioIioI @ domainSubclass_THFTYPE_IIioIiioI @ lTernaryPredicate_THFTYPE_i )).

thf(ax_326,axiom,
    ( instance_THFTYPE_IIiiIioI @ lEndFn_THFTYPE_IiiI @ lTotalValuedRelation_THFTYPE_i )).

thf(ax_327,axiom,
    ( instance_THFTYPE_IIiioIioI @ property_THFTYPE_IiioI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_328,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThanOrEqualTo_THFTYPE_IiioI @ lPartialOrderingRelation_THFTYPE_i )).

thf(ax_329,axiom,
    ( domain_THFTYPE_IiiioI @ spouse_THFTYPE_i @ n2_THFTYPE_i @ lHuman_THFTYPE_i )).

thf(ax_330,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThan_THFTYPE_IiioI @ lTransitiveRelation_THFTYPE_i )).

thf(ax_331,axiom,
    ( relatedInternalConcept_THFTYPE_IIiioIIiioIoI @ member_THFTYPE_IiioI @ instance_THFTYPE_IiioI )).

thf(ax_332,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lWhenFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lPhysical_THFTYPE_i )).

thf(ax_333,axiom,
    ( domain_THFTYPE_IIiiIiioI @ lEndFn_THFTYPE_IiiI @ n1_THFTYPE_i @ lTimeInterval_THFTYPE_i )).

thf(ax_334,axiom,
    ( instance_THFTYPE_IIiIiioIoIioI @ disjointRelation_THFTYPE_IiIiioIoI @ lBinaryPredicate_THFTYPE_i )).

thf(ax_335,axiom,
    ( subrelation_THFTYPE_IIiioIioI @ wife_THFTYPE_IiioI @ spouse_THFTYPE_i )).

thf(ax_336,axiom,
    ( instance_THFTYPE_IIiioIioI @ lessThan_THFTYPE_IiioI @ lRelationExtendedToQuantities_THFTYPE_i )).

thf(ax_337,axiom,
    ( domain_THFTYPE_IiiioI @ attribute_THFTYPE_i @ n1_THFTYPE_i @ lObject_THFTYPE_i )).

thf(ax_338,axiom,
    ( subrelation_THFTYPE_IIoooIIiioIoI @ truth_THFTYPE_IoooI @ property_THFTYPE_IiioI )).

thf(ax_339,axiom,
    ( instance_THFTYPE_IIiioIioI @ located_THFTYPE_IiioI @ lTransitiveRelation_THFTYPE_i )).

thf(ax_340,axiom,
    ( instance_THFTYPE_IIiiIioI @ lWhenFn_THFTYPE_IiiI @ lTemporalRelation_THFTYPE_i )).

thf(ax_341,axiom,
    ( domain_THFTYPE_IIiioIiioI @ greaterThanOrEqualTo_THFTYPE_IiioI @ n1_THFTYPE_i @ lQuantity_THFTYPE_i )).

%----The translated conjectures
thf(con,conjecture,(
    ? [R: $i > $i > $o] :
      ( ( holdsDuring_THFTYPE_IiooI @ ( lYearFn_THFTYPE_IiiI @ n2009_THFTYPE_i ) @ ( R @ lChris_THFTYPE_i @ lCorina_THFTYPE_i ) )
      & ( ~
        @ ( R
          = ( ^ [X: $i,Y: $i] : $true ) ) ) ) )).

%------------------------------------------------------------------------------
