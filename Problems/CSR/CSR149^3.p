%------------------------------------------------------------------------------
% File     : CSR149^3 : TPTP v6.4.0. Released v5.3.0.
% Domain   : Commonsense Reasoning
% Problem  : Did someone like Bill in 2009?
% Version  : Especial.
% English  : During 2009 Mary liked Bill and Sue liked Bill. Is it the case
%            that someone liked Bill during 2009?

% Refs     : [BP10]  Benzmueller & Pease (2010), Progress in Automating Hig
%          : [Ben11] Benzmueller (2011), Email to Geoff Sutcliffe
% Source   : [Ben11]
% Names    :

% Status   : Theorem
% Rating   : 0.86 v6.4.0, 0.83 v6.3.0, 1.00 v6.2.0, 0.86 v6.1.0, 1.00 v5.5.0, 0.83 v5.4.0, 1.00 v5.3.0
% Syntax   : Number of formulae    : 5016 (   0 unit;1435 type;   0 defn)
%            Number of atoms       : 20870 ( 406 equality;6090 variable)
%            Maximal formula depth :   26 (   4 average)
%            Number of connectives : 16477 (   0   ~;  77   |;1313   &;14033   @)
%                                         ( 105 <=>; 949  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  : 1319 (1319   >;   0   *;   0   +;   0  <<)
%            Number of symbols     : 1441 (1435   :;   0   =)
%            Number of variables   : 2574 (   0 sgn;2077   !; 495   ?;   2   ^)
%                                         (2574   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments :
%------------------------------------------------------------------------------
%----Include SUMO axioms
include('Axioms/CSR005^0.ax').
%------------------------------------------------------------------------------
%----The extracted Signature
thf(lBill_THFTYPE_i,type,(
    lBill_THFTYPE_i: $i )).

thf(lMary_THFTYPE_i,type,(
    lMary_THFTYPE_i: $i )).

thf(lSue_THFTYPE_i,type,(
    lSue_THFTYPE_i: $i )).

thf(likes_THFTYPE_IiioI,type,(
    likes_THFTYPE_IiioI: $i > $i > $o )).

thf(n2009_THFTYPE_i,type,(
    n2009_THFTYPE_i: $i )).

%----The translated axioms
thf(ax,axiom,
    ( likes_THFTYPE_IiioI @ lMary_THFTYPE_i @ lBill_THFTYPE_i )).

thf(ax_001,axiom,(
    ! [Y: $i] :
      ( holdsDuring_THFTYPE_IiooI @ Y @ $true ) )).

thf(ax_002,axiom,
    ( holdsDuring_THFTYPE_IiooI @ ( lYearFn_THFTYPE_IiiI @ n2009_THFTYPE_i )
    @ ! [X: $i] :
        ( ( likes_THFTYPE_IiioI @ lMary_THFTYPE_i @ X )
       => ( likes_THFTYPE_IiioI @ lSue_THFTYPE_i @ X ) ) )).

%----The translated conjectures
thf(con,conjecture,(
    ? [X: $i,Y: $i] :
      ( holdsDuring_THFTYPE_IiooI @ ( lYearFn_THFTYPE_IiiI @ Y ) @ ( likes_THFTYPE_IiioI @ lSue_THFTYPE_i @ X ) ) )).

%------------------------------------------------------------------------------
