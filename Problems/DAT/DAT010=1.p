%------------------------------------------------------------------------------
% File     : DAT010=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Data Structures
% Problem  : All elements are less than 100
% Version  : [PW06] axioms.
% English  :

% Refs     : [PW06]  Prevosto & Waldmann (2006), SPASS+T
%          : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    : (34) [PW06]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.38 v6.1.0, 0.44 v6.0.0, 0.43 v5.5.0, 0.44 v5.4.0, 0.50 v5.1.0, 0.40 v5.0.0
% Syntax   : Number of formulae    :    6 (   1 unit;   3 type)
%            Number of atoms       :    6 (   4 equality)
%            Maximal formula depth :    6 (   4 average)
%            Number of connectives :    3 (   0   ~;   1   |;   1   &)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    5 (   2   >;   3   *;   0   +;   0  <<)
%            Number of predicates  :    7 (   5 propositional; 0-2 arity)
%            Number of functors    :   10 (   8 constant; 0-3 arity)
%            Number of variables   :   11 (   0 sgn;  11   !;   0   ?)
%                                         (  11   :;   0  !>;   0  ?*)
%            Maximal term depth    :    5 (   2 average)
%            Arithmetic symbols    :   16 (   1 prd;   0 fun;   8 num;   7 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
%----Includes axioms for arrays
include('Axioms/DAT001=0.ax').
%------------------------------------------------------------------------------
tff(co1,conjecture,(
    ! [U: array,V: array] :
      ( ( U = write(write(write(write(V,3,33),4,444),5,55),4,44)
        & ! [W: $int] : $less(read(V,W),100) )
     => ! [X: $int] : $less(read(U,X),100) ) )).
%------------------------------------------------------------------------------
