%------------------------------------------------------------------------------
% File     : DAT011=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Data Structures
% Problem  : Compare elements 1
% Version  : [PW06] axioms.
% English  :

% Refs     : [PW06]  Prevosto & Waldmann (2006), SPASS+T
%          : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    : (35) [PW06]

% Status   : Theorem
% Rating   : 0.00 v6.3.0, 0.14 v6.2.0, 0.50 v6.1.0, 0.56 v6.0.0, 0.57 v5.5.0, 0.56 v5.4.0, 0.62 v5.3.0, 0.60 v5.2.0, 0.83 v5.1.0, 0.80 v5.0.0
% Syntax   : Number of formulae    :    6 (   1 unit;   3 type)
%            Number of atoms       :   10 (   4 equality)
%            Maximal formula depth :   10 (   5 average)
%            Number of connectives :    7 (   0   ~;   1   |;   3   &)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    5 (   2   >;   3   *;   0   +;   0  <<)
%            Number of predicates  :    8 (   5 propositional; 0-2 arity)
%            Number of functors    :    6 (   3 constant; 0-3 arity)
%            Number of variables   :   13 (   0 sgn;  13   !;   0   ?)
%                                         (  13   :;   0  !>;   0  ?*)
%            Maximal term depth    :    3 (   2 average)
%            Arithmetic symbols    :   15 (   2 prd;   1 fun;   3 num;   9 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
%----Includes axioms for arrays
include('Axioms/DAT001=0.ax').
%------------------------------------------------------------------------------
tff(co1,conjecture,(
    ! [U: array,V: array,W: $int,X: $int] :
      ( ( ! [Y: $int] :
            ( ( $lesseq(W,Y)
              & $lesseq(Y,X) )
           => $greater(read(V,Y),0) )
        & U = write(V,$sum(X,1),3) )
     => ! [Z: $int] :
          ( ( $lesseq(W,Z)
            & $lesseq(Z,$sum(X,1)) )
         => $greater(read(U,Z),0) ) ) )).
%--------------------------------------------------------------------------
