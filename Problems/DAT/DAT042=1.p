%------------------------------------------------------------------------------
% File     : DAT042=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Data Structures
% Problem  : Some collection has 3 as an element
% Version  : [PW06] axioms.
% English  : 

% Refs     : [PW06]  Prevosto & Waldmann (2006), SPASS+T
%          : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    : (65) [PW06]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.67 v6.3.0, 0.43 v6.2.0, 0.75 v6.1.0, 0.89 v6.0.0, 0.86 v5.5.0, 0.78 v5.4.0, 0.75 v5.3.0, 0.70 v5.2.0, 0.67 v5.1.0, 0.60 v5.0.0
% Syntax   : Number of formulae    :   19 (   5 unit;   6 type)
%            Number of atoms       :   23 (  10 equality)
%            Maximal formula depth :    7 (   4 average)
%            Number of connectives :   15 (   5   ~;   1   |;   1   &)
%                                         (   7 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    7 (   4   >;   3   *;   0   +;   0  <<)
%            Number of predicates  :   12 (   9 propositional; 0-2 arity)
%            Number of functors    :    9 (   4 constant; 0-2 arity)
%            Number of variables   :   24 (   0 sgn;  23   !;   1   ?)
%                                         (  24   :;   0  !>;   0  ?*)
%            Maximal term depth    :    3 (   2 average)
%            Arithmetic symbols    :   18 (   1 prd;   2 fun;   3 num;  12 var)
% SPC      : TF0_THM_EQU_ARI

% Comments : 
%------------------------------------------------------------------------------
%----Includes axioms for collections with counting
include('Axioms/DAT002=0.ax').
include('Axioms/DAT002=1.ax').
%------------------------------------------------------------------------------
tff(co1,conjecture,(
    ? [U: collection] : count(U) = 3 )).
%------------------------------------------------------------------------------
