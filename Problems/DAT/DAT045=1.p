%------------------------------------------------------------------------------
% File     : DAT045=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Data Structures
% Problem  : Adding a larger element to the collection 2
% Version  : [PW06] axioms.
% English  : 

% Refs     : [PW06]  Prevosto & Waldmann (2006), SPASS+T
%          : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    : (68) [PW06]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.25 v6.1.0, 0.33 v6.0.0, 0.29 v5.5.0, 0.44 v5.4.0, 0.50 v5.3.0, 0.60 v5.2.0, 0.83 v5.1.0, 0.80 v5.0.0
% Syntax   : Number of formulae    :   19 (   4 unit;   6 type)
%            Number of atoms       :   25 (   9 equality)
%            Maximal formula depth :    7 (   4 average)
%            Number of connectives :   17 (   5   ~;   1   |;   1   &)
%                                         (   7 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    7 (   4   >;   3   *;   0   +;   0  <<)
%            Number of predicates  :   13 (   9 propositional; 0-2 arity)
%            Number of functors    :    8 (   3 constant; 0-2 arity)
%            Number of variables   :   26 (   0 sgn;  26   !;   0   ?)
%                                         (  26   :;   0  !>;   0  ?*)
%            Maximal term depth    :    3 (   2 average)
%            Arithmetic symbols    :   20 (   2 prd;   2 fun;   2 num;  14 var)
% SPC      : TF0_THM_EQU_ARI

% Comments : 
%------------------------------------------------------------------------------
%----Includes axioms for collections with counting
include('Axioms/DAT002=0.ax').
include('Axioms/DAT002=1.ax').
%------------------------------------------------------------------------------
tff(co1,conjecture,(
    ! [U: collection,V: $int] :
      ( ! [W: $int] :
          ( in(W,U)
         => $greater(V,W) )
     => $greater(count(add(V,U)),count(U)) ) )).
%------------------------------------------------------------------------------
