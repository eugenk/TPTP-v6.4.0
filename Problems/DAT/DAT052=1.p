%------------------------------------------------------------------------------
% File     : DAT052=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Data Structures
% Problem  : Even and odd numbered elements 2
% Version  : [PW06] axioms.
% English  : 

% Refs     : [PW06]  Prevosto & Waldmann (2006), SPASS+T
%          : [Wal10] Waldmann (2010), Email to Geoff Sutcliffe
% Source   : [Wal10]
% Names    : (75) [PW06]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.00 v6.3.0, 0.29 v6.2.0, 0.62 v6.1.0, 0.67 v6.0.0, 0.71 v5.5.0, 0.67 v5.4.0, 0.75 v5.3.0, 0.80 v5.2.0, 1.00 v5.0.0
% Syntax   : Number of formulae    :   21 (   0 unit;   7 type)
%            Number of atoms       :   36 (   9 equality)
%            Maximal formula depth :    7 (   4 average)
%            Number of connectives :   31 (   9   ~;   0   |;   8   &)
%                                         (   0 <=>;  14  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    6 (   6   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :   13 (  10 propositional; 0-2 arity)
%            Number of functors    :    8 (   2 constant; 0-2 arity)
%            Number of variables   :   15 (   0 sgn;  15   !;   0   ?)
%                                         (  15   :;   0  !>;   0  ?*)
%            Maximal term depth    :    4 (   2 average)
%            Arithmetic symbols    :    4 (   1 prd;   1 fun;   2 num;   0 var)
% SPC      : TF0_THM_EQU_ARI

% Comments : 
%------------------------------------------------------------------------------
%----Includes axioms for pointer data types
include('Axioms/DAT003=0.ax').
%------------------------------------------------------------------------------
tff(co1,conjecture,(
    ! [U: record,V: record] :
      ( ( isrecord(U)
        & isrecord(next(U))
        & V = next(next(U))
        & $sum(length(split1(V)),length(split2(V))) = length(V) )
     => $sum(length(split1(U)),length(split2(U))) = length(U) ) )).
%------------------------------------------------------------------------------
