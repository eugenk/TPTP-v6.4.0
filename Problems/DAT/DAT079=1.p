%------------------------------------------------------------------------------
% File     : DAT079=1 : TPTP v6.4.0. Released v6.1.0.
% Domain   : Data Structures
% Problem  : Lists by functions problem 1
% Version  : Especial.
% English  :

% Refs     : [BB14]  Baumgartner & Bax (2014), Proving Infinite Satisfiabil
%            [Bau14] Baumgartner (2014), Email to Geoff Sutcliffe
% Source   : [Bau14]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.12 v6.1.0
% Syntax   : Number of formulae    :   25 (   9 unit;  10 type)
%            Number of atoms       :   28 (  19 equality)
%            Maximal formula depth :   10 (   4 average)
%            Number of connectives :   15 (   2   ~;   3   |;   5   &)
%                                         (   3 <=>;   0  =>;   2  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   13 (   8   >;   5   *;   0   +;   0  <<)
%            Number of predicates  :   19 (  13 propositional; 0-2 arity)
%            Number of functors    :   12 (   5 constant; 0-2 arity)
%            Number of variables   :   34 (   2 sgn;  28   !;   6   ?)
%                                         (  34   :;   0  !>;   0  ?*)
%            Maximal term depth    :    4 (   2 average)
%            Arithmetic symbols    :   26 (   3 prd;   1 fun;   4 num;  18 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(list_type,type,(
    list: $tType )).

tff(nil_type,type,(
    nil: list )).

tff(cons_type,type,(
    cons: ( $int * list ) > list )).

tff(head_type,type,(
    head: list > $int )).

tff(tail_type,type,(
    tail: list > list )).

%----Selectors
tff(l1,axiom,(
    ! [K: $int,L: list] : head(cons(K,L)) = K )).

tff(l2,axiom,(
    ! [K: $int,L: list] : tail(cons(K,L)) = L )).

%----Constructors
tff(l3,axiom,(
    ! [L: list] :
      ( L = nil
      | L = cons(head(L),tail(L)) ) )).

tff(l4,axiom,(
    ! [K: $int,L: list] : cons(K,L) != nil )).

%----List membership
tff(in,type,(
    in: ( $int * list ) > $o )).

tff(in_conv,axiom,(
    ! [X: $int,L: list] :
      ( in(X,L)
    <=> ( ? [H: $int,T: list] :
            ( L = cons(H,T)
            & X = H )
        | ? [H: $int,T: list] :
            ( L = cons(H,T)
            & in(X,T) ) ) ) )).

%----inRange
tff(inRange_type,type,(
    inRange: ( $int * list ) > $o )).

tff(inRange,axiom,(
    ! [N: $int,L: list] :
      ( inRange(N,L)
    <=> ( L = nil
        | ? [K: $int,T: list] :
            ( L = cons(K,T)
            & $lesseq(0,K)
            & $less(K,N)
            & inRange(N,T) ) ) ) )).

%----Length
tff(t,type,(
    length: list > $int )).

tff(l,axiom,(
    length(nil) = 0 )).

tff(l_1,axiom,(
    ! [H: $int,T: list] : length(cons(H,T)) = $sum(1,length(T)) )).

%----Count
tff(t_2,type,(
    count: ( $int * list ) > $int )).

tff(a,axiom,(
    ! [K: $int] : count(K,nil) = 0 )).

tff(a_3,axiom,(
    ! [K: $int,H: $int,T: list,N: $int] :
      ( count(K,cons(H,T)) = count(K,T)
     <= K != H ) )).

tff(a_4,axiom,(
    ! [K: $int,H: $int,T: list,N: $int] :
      ( count(K,cons(H,T)) = $sum(count(K,T),1)
     <= K = H ) )).

%----Append
tff(t_5,type,(
    append: ( list * list ) > list )).

tff(l_6,axiom,(
    ! [L: list] : append(nil,L) = L )).

tff(l_7,axiom,(
    ! [I: $int,K: list,L: list] : append(cons(I,K),L) = cons(I,append(K,L)) )).

tff(a_8,axiom,(
    ! [N: $int,L: list] :
      ( in(N,L)
    <=> $greater(count(N,L),0) ) )).

tff(c,conjecture,(
    in(2,cons(1,cons(2,cons(3,nil)))) )).

%------------------------------------------------------------------------------
