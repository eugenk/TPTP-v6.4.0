%------------------------------------------------------------------------------
% File     : DAT099=1 : TPTP v6.4.0. Released v6.1.0.
% Domain   : Data Structures
% Problem  : Lists by relations problem 2
% Version  : Especial.
% English  :

% Refs     : [BB14]  Baumgartner & Bax (2014), Proving Infinite Satisfiabil
%            [Bau14] Baumgartner (2014), Email to Geoff Sutcliffe
% Source   : [Bau14]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.3.0, 0.14 v6.2.0, 0.38 v6.1.0
% Syntax   : Number of formulae    :   12 (   3 unit;   6 type)
%            Number of atoms       :   13 (   7 equality)
%            Maximal formula depth :   10 (   4 average)
%            Number of connectives :    8 (   1   ~;   2   |;   3   &)
%                                         (   1 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    6 (   4   >;   2   *;   0   +;   0  <<)
%            Number of predicates  :   14 (   9 propositional; 0-2 arity)
%            Number of functors    :    9 (   6 constant; 0-2 arity)
%            Number of variables   :   12 (   0 sgn;  10   !;   2   ?)
%                                         (  12   :;   0  !>;   0  ?*)
%            Maximal term depth    :    4 (   1 average)
%            Arithmetic symbols    :   14 (   3 prd;   0 fun;   5 num;   6 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(list_type,type,(
    list: $tType )).

tff(nil_type,type,(
    nil: list )).

tff(cons_type,type,(
    cons: ( $int * list ) > list )).

tff(head_type,type,(
    head: list > $int )).

tff(tail_type,type,(
    tail: list > list )).

%----Selectors
tff(l1,axiom,(
    ! [K: $int,L: list] : head(cons(K,L)) = K )).

tff(l2,axiom,(
    ! [K: $int,L: list] : tail(cons(K,L)) = L )).

%----Constructors
tff(l3,axiom,(
    ! [L: list] :
      ( L = nil
      | L = cons(head(L),tail(L)) ) )).

tff(l4,axiom,(
    ! [K: $int,L: list] : cons(K,L) != nil )).

%----inRange
tff(inRange_type,type,(
    inRange: ( $int * list ) > $o )).

tff(inRange,axiom,(
    ! [N: $int,L: list] :
      ( inRange(N,L)
    <=> ( L = nil
        | ? [K: $int,T: list] :
            ( L = cons(K,T)
            & $lesseq(0,K)
            & $less(K,N)
            & inRange(N,T) ) ) ) )).

tff(c,conjecture,(
    ! [N: $int] :
      ( $greatereq(N,4)
     => inRange(N,cons(1,cons(3,cons(2,nil)))) ) )).

%------------------------------------------------------------------------------
