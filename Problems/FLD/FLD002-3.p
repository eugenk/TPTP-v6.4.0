%--------------------------------------------------------------------------
% File     : FLD002-3 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Field Theory (Ordered fields)
% Problem  : Transformation multiplicative relation --> additive relation
% Version  : [Dra93] axioms : Especial.
%            Theorem formulation : Functional with re axiom set.
% English  :

% Refs     : [Dra93] Draeger (1993), Anwendung des Theorembeweisers SETHEO
% Source   : [Dra93]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.00 v5.5.0, 0.12 v5.4.0, 0.10 v5.2.0, 0.00 v5.0.0, 0.07 v4.1.0, 0.00 v4.0.1, 0.20 v4.0.0, 0.14 v3.4.0, 0.25 v3.3.0, 0.33 v3.1.0, 0.17 v2.7.0, 0.12 v2.6.0, 0.33 v2.5.0, 0.20 v2.4.0, 0.40 v2.3.0, 0.67 v2.2.1, 0.67 v2.1.0
% Syntax   : Number of clauses     :   30 (   3 non-Horn;   7 unit;  30 RR)
%            Number of atoms       :   81 (   0 equality)
%            Maximal clause size   :    5 (   3 average)
%            Number of predicates  :    4 (   0 propositional; 1-3 arity)
%            Number of functors    :    8 (   4 constant; 0-2 arity)
%            Number of variables   :   73 (   0 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_NEQ_NHN

% Comments :
% Bugfixes : v2.1.0 - Bugfix in FLD002-0.ax
%--------------------------------------------------------------------------
include('Axioms/FLD002-0.ax').
%--------------------------------------------------------------------------
cnf(a_is_defined,hypothesis,
    ( defined(a) )).

cnf(b_is_defined,hypothesis,
    ( defined(b) )).

cnf(product_3,hypothesis,
    ( product(multiplicative_identity,a,b) )).

cnf(not_sum_4,negated_conjecture,
    ( ~ sum(additive_identity,a,b) )).

%--------------------------------------------------------------------------
