%--------------------------------------------------------------------------
% File     : FLD036-1 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Field Theory (Ordered fields)
% Problem  : Only a multiplication by zero can make elements equal
% Version  : [Dra93] axioms : Especial.
%            Theorem formulation : Functional with glxx axiom set.
% English  :

% Refs     : [Dra93] Draeger (1993), Anwendung des Theorembeweisers SETHEO
% Source   : [Dra93]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.75 v6.3.0, 0.71 v6.2.0, 0.78 v6.1.0, 0.86 v5.5.0, 0.75 v5.4.0, 0.80 v5.3.0, 0.90 v5.1.0, 0.91 v5.0.0, 0.93 v4.1.0, 0.88 v4.0.1, 0.80 v4.0.0, 0.71 v3.4.0, 0.50 v3.3.0, 0.33 v3.1.0, 0.83 v2.7.0, 0.88 v2.6.0, 0.67 v2.5.0, 1.00 v2.1.0
% Syntax   : Number of clauses     :   33 (   3 non-Horn;   9 unit;  33 RR)
%            Number of atoms       :   79 (   0 equality)
%            Maximal clause size   :    4 (   2 average)
%            Number of predicates  :    3 (   0 propositional; 1-2 arity)
%            Number of functors    :    9 (   5 constant; 0-2 arity)
%            Number of variables   :   50 (   0 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNS_RFO_NEQ_NHN

% Comments :
% Bugfixes : v2.1.0 - Bugfix in FLD001-0.ax
%--------------------------------------------------------------------------
include('Axioms/FLD001-0.ax').
%--------------------------------------------------------------------------
cnf(a_is_defined,hypothesis,
    ( defined(a) )).

cnf(b_is_defined,hypothesis,
    ( defined(b) )).

cnf(c_is_defined,hypothesis,
    ( defined(c) )).

cnf(multiply_equals_multiply_4,negated_conjecture,
    ( equalish(multiply(a,c),multiply(b,c)) )).

cnf(a_not_equal_to_b_5,negated_conjecture,
    ( ~ equalish(a,b) )).

cnf(c_not_equal_to_additive_identity_6,negated_conjecture,
    ( ~ equalish(c,additive_identity) )).

%--------------------------------------------------------------------------
