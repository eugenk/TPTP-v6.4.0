%--------------------------------------------------------------------------
% File     : FLD061-3 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Field Theory (Ordered fields)
% Problem  : The resulting inequality of a summation of two inequalities
% Version  : [Dra93] axioms : Especial.
%            Theorem formulation : Functional with re axiom set.
% English  :

% Refs     : [Dra93] Draeger (1993), Anwendung des Theorembeweisers SETHEO
% Source   : [Dra93]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.50 v6.3.0, 0.43 v6.2.0, 0.33 v6.1.0, 0.29 v5.5.0, 0.38 v5.4.0, 0.40 v5.2.0, 0.30 v5.1.0, 0.36 v4.1.0, 0.12 v4.0.1, 0.40 v4.0.0, 0.29 v3.4.0, 0.25 v3.3.0, 0.33 v3.2.0, 0.67 v3.1.0, 0.33 v2.7.0, 0.12 v2.6.0, 0.33 v2.5.0, 0.40 v2.3.0, 0.67 v2.2.1, 1.00 v2.1.0
% Syntax   : Number of clauses     :   33 (   3 non-Horn;  10 unit;  33 RR)
%            Number of atoms       :   84 (   0 equality)
%            Maximal clause size   :    5 (   3 average)
%            Number of predicates  :    4 (   0 propositional; 1-3 arity)
%            Number of functors    :   10 (   6 constant; 0-2 arity)
%            Number of variables   :   73 (   0 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_NEQ_NHN

% Comments :
% Bugfixes : v2.1.0 - Bugfix in FLD002-0.ax
%--------------------------------------------------------------------------
include('Axioms/FLD002-0.ax').
%--------------------------------------------------------------------------
cnf(a_is_defined,hypothesis,
    ( defined(a) )).

cnf(b_is_defined,hypothesis,
    ( defined(b) )).

cnf(c_is_defined,hypothesis,
    ( defined(c) )).

cnf(d_is_defined,hypothesis,
    ( defined(d) )).

cnf(less_or_equal_5,negated_conjecture,
    ( less_or_equal(a,b) )).

cnf(less_or_equal_6,negated_conjecture,
    ( less_or_equal(c,d) )).

cnf(not_less_or_equal_7,negated_conjecture,
    ( ~ less_or_equal(add(a,c),add(d,b)) )).

%--------------------------------------------------------------------------
