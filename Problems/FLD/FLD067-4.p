%--------------------------------------------------------------------------
% File     : FLD067-4 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Field Theory (Ordered fields)
% Problem  : Side change in an order relation, part 1
% Version  : [Dra93] axioms : Especial.
%            Theorem formulation : Relational with re axiom set.
% English  :

% Refs     : [Dra93] Draeger (1993), Anwendung des Theorembeweisers SETHEO
% Source   : [Dra93]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.00 v5.5.0, 0.12 v5.4.0, 0.10 v5.2.0, 0.00 v3.1.0, 0.17 v2.7.0, 0.00 v2.6.0, 0.33 v2.5.0, 0.00 v2.2.1, 0.33 v2.1.0
% Syntax   : Number of clauses     :   32 (   3 non-Horn;   9 unit;  32 RR)
%            Number of atoms       :   83 (   0 equality)
%            Maximal clause size   :    5 (   3 average)
%            Number of predicates  :    4 (   0 propositional; 1-3 arity)
%            Number of functors    :    9 (   5 constant; 0-2 arity)
%            Number of variables   :   73 (   0 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_NEQ_NHN

% Comments :
% Bugfixes : v2.1.0 - Bugfix in FLD002-0.ax
%--------------------------------------------------------------------------
include('Axioms/FLD002-0.ax').
%--------------------------------------------------------------------------
cnf(a_is_defined,hypothesis,
    ( defined(a) )).

cnf(b_is_defined,hypothesis,
    ( defined(b) )).

cnf(u_is_defined,hypothesis,
    ( defined(u) )).

cnf(less_or_equal_4,negated_conjecture,
    ( less_or_equal(a,b) )).

cnf(sum_5,negated_conjecture,
    ( sum(b,additive_inverse(a),u) )).

cnf(not_less_or_equal_6,negated_conjecture,
    ( ~ less_or_equal(additive_identity,u) )).

%--------------------------------------------------------------------------
