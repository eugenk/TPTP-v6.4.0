%--------------------------------------------------------------------------
% File     : FLD072-2 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Field Theory (Ordered fields)
% Problem  : One-sided multiplication of two order relations, part 2
% Version  : [Dra93] axioms : Especial.
%            Theorem formulation : Relational with glxx axiom set.
% English  :

% Refs     : [Dra93] Draeger (1993), Anwendung des Theorembeweisers SETHEO
% Source   : [Dra93]
% Names    :

% Status   : Unsatisfiable
% Rating   : 1.00 v6.2.0, 0.89 v6.1.0, 1.00 v2.1.0
% Syntax   : Number of clauses     :   34 (   3 non-Horn;  10 unit;  34 RR)
%            Number of atoms       :   80 (   0 equality)
%            Maximal clause size   :    4 (   2 average)
%            Number of predicates  :    3 (   0 propositional; 1-2 arity)
%            Number of functors    :    9 (   5 constant; 0-2 arity)
%            Number of variables   :   50 (   0 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNS_RFO_NEQ_NHN

% Comments :
% Bugfixes : v2.1.0 - Bugfix in FLD001-0.ax
%--------------------------------------------------------------------------
include('Axioms/FLD001-0.ax').
%--------------------------------------------------------------------------
cnf(a_is_defined,hypothesis,
    ( defined(a) )).

cnf(b_is_defined,hypothesis,
    ( defined(b) )).

cnf(u_is_defined,hypothesis,
    ( defined(u) )).

cnf(less_or_equal_4,negated_conjecture,
    ( less_or_equal(a,additive_identity) )).

cnf(less_or_equal_5,negated_conjecture,
    ( less_or_equal(additive_identity,b) )).

cnf(multiply_equals_u_6,negated_conjecture,
    ( equalish(multiply(a,b),u) )).

cnf(not_less_or_equal_7,negated_conjecture,
    ( ~ less_or_equal(u,additive_identity) )).

%--------------------------------------------------------------------------
