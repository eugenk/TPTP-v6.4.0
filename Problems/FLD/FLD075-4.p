%--------------------------------------------------------------------------
% File     : FLD075-4 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Field Theory (Ordered fields)
% Problem  : Two-sided multiplication in an order relation, part 2
% Version  : [Dra93] axioms : Especial.
%            Theorem formulation : Relational with re axiom set.
% English  :

% Refs     : [Dra93] Draeger (1993), Anwendung des Theorembeweisers SETHEO
% Source   : [Dra93]
% Names    :

% Status   : Unknown
% Rating   : 1.00 v2.1.0
% Syntax   : Number of clauses     :   36 (   3 non-Horn;  13 unit;  36 RR)
%            Number of atoms       :   87 (   0 equality)
%            Maximal clause size   :    5 (   2 average)
%            Number of predicates  :    4 (   0 propositional; 1-3 arity)
%            Number of functors    :   11 (   7 constant; 0-2 arity)
%            Number of variables   :   73 (   0 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNK_RFO_NEQ_NHN

% Comments :
% Bugfixes : v2.1.0 - Bugfix in FLD002-0.ax
%--------------------------------------------------------------------------
include('Axioms/FLD002-0.ax').
%--------------------------------------------------------------------------
cnf(a_is_defined,hypothesis,
    ( defined(a) )).

cnf(b_is_defined,hypothesis,
    ( defined(b) )).

cnf(c_is_defined,hypothesis,
    ( defined(c) )).

cnf(u_is_defined,hypothesis,
    ( defined(u) )).

cnf(v_is_defined,hypothesis,
    ( defined(v) )).

cnf(less_or_equal_6,negated_conjecture,
    ( less_or_equal(a,b) )).

cnf(less_or_equal_7,negated_conjecture,
    ( less_or_equal(c,additive_identity) )).

cnf(product_8,negated_conjecture,
    ( product(a,c,u) )).

cnf(product_9,negated_conjecture,
    ( product(b,c,v) )).

cnf(not_less_or_equal_10,negated_conjecture,
    ( ~ less_or_equal(v,u) )).

%--------------------------------------------------------------------------
