%--------------------------------------------------------------------------
% File     : FLD080-2 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Field Theory (Ordered fields)
% Problem  : The square of an element is always greater or equal 0
% Version  : [Dra93] axioms : Especial.
%            Theorem formulation : Relational with glxx axiom set.
% English  :

% Refs     : [Dra93] Draeger (1993), Anwendung des Theorembeweisers SETHEO
% Source   : [Dra93]
% Names    :

% Status   : Unknown
% Rating   : 1.00 v2.1.0
% Syntax   : Number of clauses     :   31 (   3 non-Horn;   7 unit;  31 RR)
%            Number of atoms       :   77 (   0 equality)
%            Maximal clause size   :    4 (   2 average)
%            Number of predicates  :    3 (   0 propositional; 1-2 arity)
%            Number of functors    :    8 (   4 constant; 0-2 arity)
%            Number of variables   :   50 (   0 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNK_RFO_NEQ_NHN

% Comments :
% Bugfixes : v2.1.0 - Bugfix in FLD001-0.ax
%--------------------------------------------------------------------------
include('Axioms/FLD001-0.ax').
%--------------------------------------------------------------------------
cnf(a_is_defined,hypothesis,
    ( defined(a) )).

cnf(u_is_defined,hypothesis,
    ( defined(u) )).

cnf(multiply_equals_u_3,negated_conjecture,
    ( equalish(multiply(a,a),u) )).

cnf(not_less_or_equal_4,negated_conjecture,
    ( ~ less_or_equal(additive_identity,u) )).

%--------------------------------------------------------------------------
