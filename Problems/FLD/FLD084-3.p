%--------------------------------------------------------------------------
% File     : FLD084-3 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Field Theory (Ordered fields)
% Problem  : Elimination of multiplicative inverses in an order, part 1
% Version  : [Dra93] axioms : Especial.
%            Theorem formulation : Functional with re axiom set.
% English  :

% Refs     : [Dra93] Draeger (1993), Anwendung des Theorembeweisers SETHEO
% Source   : [Dra93]
% Names    :

% Status   : Unknown
% Rating   : 1.00 v2.1.0
% Syntax   : Number of clauses     :   33 (   3 non-Horn;  10 unit;  33 RR)
%            Number of atoms       :   84 (   0 equality)
%            Maximal clause size   :    5 (   3 average)
%            Number of predicates  :    4 (   0 propositional; 1-3 arity)
%            Number of functors    :    8 (   4 constant; 0-2 arity)
%            Number of variables   :   73 (   0 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNK_RFO_NEQ_NHN

% Comments :
% Bugfixes : v2.1.0 - Bugfix in FLD002-0.ax
%--------------------------------------------------------------------------
include('Axioms/FLD002-0.ax').
%--------------------------------------------------------------------------
cnf(a_is_defined,hypothesis,
    ( defined(a) )).

cnf(b_is_defined,hypothesis,
    ( defined(b) )).

cnf(not_sum_3,negated_conjecture,
    ( ~ sum(additive_identity,b,additive_identity) )).

cnf(not_sum_4,negated_conjecture,
    ( ~ sum(additive_identity,a,additive_identity) )).

cnf(less_or_equal_5,negated_conjecture,
    ( less_or_equal(multiplicative_inverse(b),multiplicative_inverse(a)) )).

cnf(less_or_equal_6,negated_conjecture,
    ( less_or_equal(additive_identity,multiplicative_inverse(b)) )).

cnf(not_less_or_equal_7,negated_conjecture,
    ( ~ less_or_equal(a,b) )).

%--------------------------------------------------------------------------
