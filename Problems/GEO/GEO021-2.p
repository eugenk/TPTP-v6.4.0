%--------------------------------------------------------------------------
% File     : GEO021-2 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Geometry
% Problem  : Corollary 5 to symmetries of equidistance
% Version  : [Qua89] axioms.
% English  : Show that if the distance from A to B equals the distance
%            from C to D, then the distance from D to C equals the
%            distance from B to A.

% Refs     : [SST83] Schwabbauser et al. (1983), Metamathematische Methoden
%          : [Qua89] Quaife (1989), Automated Development of Tarski's Geome
% Source   : [TPTP]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.07 v6.3.0, 0.00 v6.2.0, 0.10 v6.1.0, 0.07 v6.0.0, 0.10 v5.3.0, 0.11 v5.2.0, 0.12 v5.1.0, 0.06 v5.0.0, 0.00 v3.7.0, 0.10 v3.5.0, 0.09 v3.4.0, 0.08 v3.3.0, 0.14 v3.2.0, 0.00 v3.1.0, 0.09 v2.7.0, 0.08 v2.6.0, 0.00 v2.0.0
% Syntax   : Number of clauses     :   20 (   5 non-Horn;   8 unit;  17 RR)
%            Number of atoms       :   58 (   7 equality)
%            Maximal clause size   :    8 (   3 average)
%            Number of predicates  :    3 (   0 propositional; 2-4 arity)
%            Number of functors    :   12 (   7 constant; 0-6 arity)
%            Number of variables   :   71 (   3 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments : Proving corollaries from axioms is not usual.
%--------------------------------------------------------------------------
%----Include Tarski geometry axioms
include('Axioms/GEO002-0.ax').
%--------------------------------------------------------------------------
cnf(u_to_v_equals_w_to_x,hypothesis,
    ( equidistant(u,v,w,x) )).

cnf(prove_symmetry,negated_conjecture,
    ( ~ equidistant(x,w,v,u) )).

%--------------------------------------------------------------------------
