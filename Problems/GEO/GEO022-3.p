%--------------------------------------------------------------------------
% File     : GEO022-3 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Geometry
% Problem  : Ordinary transitivity of equidistance
% Version  : [Qua89] axioms : Augmented.
% English  : This form of transitivity is different from that expressed
%            in the axioms.

% Refs     : [SST83] Schwabbauser et al. (1983), Metamathematische Methoden
%          : [Qua89] Quaife (1989), Automated Development of Tarski's Geome
% Source   : [Qua89]
% Names    : D5 [Qua89]

% Status   : Unsatisfiable
% Rating   : 0.07 v6.4.0, 0.00 v6.2.0, 0.10 v6.1.0, 0.07 v6.0.0, 0.10 v5.3.0, 0.11 v5.2.0, 0.12 v5.1.0, 0.06 v5.0.0, 0.00 v3.3.0, 0.07 v3.2.0, 0.08 v3.1.0, 0.09 v2.7.0, 0.08 v2.6.0, 0.00 v2.0.0
% Syntax   : Number of clauses     :   29 (   5 non-Horn;  10 unit;  25 RR)
%            Number of atoms       :   74 (   7 equality)
%            Maximal clause size   :    8 (   3 average)
%            Number of predicates  :    3 (   0 propositional; 2-4 arity)
%            Number of functors    :   14 (   9 constant; 0-6 arity)
%            Number of variables   :  101 (   3 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments : In [Quiafe, 1989] the next problem (D6) is omitted.
%--------------------------------------------------------------------------
%----Include Tarski geometry axioms
include('Axioms/GEO002-0.ax').
%--------------------------------------------------------------------------
cnf(d1,axiom,
    ( equidistant(U,V,U,V) )).

cnf(d2,axiom,
    ( ~ equidistant(U,V,W,X)
    | equidistant(W,X,U,V) )).

cnf(d3,axiom,
    ( ~ equidistant(U,V,W,X)
    | equidistant(V,U,W,X) )).

cnf(d4_1,axiom,
    ( ~ equidistant(U,V,W,X)
    | equidistant(U,V,X,W) )).

cnf(d4_2,axiom,
    ( ~ equidistant(U,V,W,X)
    | equidistant(V,U,X,W) )).

cnf(d4_3,axiom,
    ( ~ equidistant(U,V,W,X)
    | equidistant(W,X,V,U) )).

cnf(d4_4,axiom,
    ( ~ equidistant(U,V,W,X)
    | equidistant(X,W,U,V) )).

cnf(d4_5,axiom,
    ( ~ equidistant(U,V,W,X)
    | equidistant(X,W,V,U) )).

cnf(u_to_v_equals_w_to_x,hypothesis,
    ( equidistant(u,v,w,x) )).

cnf(w_to_x_equals_y_to_z,hypothesis,
    ( equidistant(w,x,y,z) )).

cnf(prove_transitivity,negated_conjecture,
    ( ~ equidistant(u,v,y,z) )).

%--------------------------------------------------------------------------
