%--------------------------------------------------------------------------
% File     : GEO064-2 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Geometry
% Problem  : Corollary 1 to collinearity
% Version  : [Qua89] axioms.
% English  :

% Refs     : [SST83] Schwabbauser et al. (1983), Metamathematische Methoden
%          : [Qua89] Quaife (1989), Automated Development of Tarski's Geome
% Source   : [TPTP]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.20 v6.4.0, 0.13 v6.3.0, 0.09 v6.2.0, 0.30 v6.1.0, 0.29 v6.0.0, 0.20 v5.5.0, 0.35 v5.3.0, 0.39 v5.2.0, 0.25 v5.1.0, 0.35 v5.0.0, 0.43 v4.1.0, 0.38 v4.0.1, 0.27 v4.0.0, 0.36 v3.7.0, 0.20 v3.5.0, 0.36 v3.4.0, 0.33 v3.3.0, 0.29 v3.2.0, 0.23 v3.1.0, 0.09 v2.7.0, 0.17 v2.6.0, 0.30 v2.5.0, 0.25 v2.4.0, 0.33 v2.2.1, 0.22 v2.2.0, 0.44 v2.1.0, 0.78 v2.0.0
% Syntax   : Number of clauses     :   24 (   6 non-Horn;   8 unit;  21 RR)
%            Number of atoms       :   68 (   7 equality)
%            Maximal clause size   :    8 (   3 average)
%            Number of predicates  :    4 (   0 propositional; 2-4 arity)
%            Number of functors    :   11 (   6 constant; 0-6 arity)
%            Number of variables   :   83 (   3 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments : Proving corollaries from axioms is not usual.
%--------------------------------------------------------------------------
%----Include Tarski geometry axioms
include('Axioms/GEO002-0.ax').
%----Include definition of colinearity
include('Axioms/GEO002-1.ax').
%--------------------------------------------------------------------------
cnf(v_between_w_and_u,hypothesis,
    ( between(w,v,u) )).

cnf(prove_uvw_colinear,negated_conjecture,
    ( ~ colinear(u,v,w) )).

%--------------------------------------------------------------------------
