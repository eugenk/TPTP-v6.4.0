%--------------------------------------------------------------------------
% File     : GEO088-1 : TPTP v6.4.0. Released v2.4.0.
% Domain   : Geometry (Oriented curves)
% Problem  : Endpoint of subcurve or curve
% Version  : [EHK99] axioms.
% English  : If an endpoint of a given curve lies on a sub-curve then it is
%            also an endpoint of this sub-curve

% Refs     : [KE99]  Kulik & Eschenbach (1999), A Geometry of Oriented Curv
%          : [EHK99] Eschenbach et al. (1999), Representing Simple Trajecto
% Source   : [TPTP]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.53 v6.3.0, 0.55 v6.2.0, 0.60 v6.1.0, 0.86 v6.0.0, 0.80 v5.5.0, 0.90 v5.4.0, 0.95 v5.3.0, 0.94 v5.0.0, 0.86 v4.1.0, 0.77 v4.0.1, 0.73 v3.7.0, 0.60 v3.5.0, 0.73 v3.4.0, 0.75 v3.3.0, 0.86 v3.2.0, 0.85 v3.1.0, 0.73 v2.7.0, 0.83 v2.6.0, 0.78 v2.4.0
% Syntax   : Number of clauses     :   52 (  21 non-Horn;   5 unit;  47 RR)
%            Number of atoms       :  158 (  21 equality)
%            Maximal clause size   :   12 (   3 average)
%            Number of predicates  :    8 (   0 propositional; 1-3 arity)
%            Number of functors    :   17 (   3 constant; 0-3 arity)
%            Number of variables   :  126 (  10 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments : Created by tptp2X -f tptp -t clausify:otter GEO088+1.p
%--------------------------------------------------------------------------
%----Include simple curve axioms
include('Axioms/GEO004-0.ax').
%--------------------------------------------------------------------------
cnf(theorem_2_10_67,negated_conjecture,
    ( part_of(sk15,sk14) )).

cnf(theorem_2_10_68,negated_conjecture,
    ( end_point(sk16,sk14) )).

cnf(theorem_2_10_69,negated_conjecture,
    ( incident_c(sk16,sk15) )).

cnf(theorem_2_10_70,negated_conjecture,
    ( ~ end_point(sk16,sk15) )).

%--------------------------------------------------------------------------
