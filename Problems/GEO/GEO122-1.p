%--------------------------------------------------------------------------
% File     : GEO122-1 : TPTP v6.4.0. Released v2.4.0.
% Domain   : Geometry (Oriented curves)
% Problem  : Every curve has a finishing point
% Version  : [EHK99] axioms.
% English  :

% Refs     : [KE99]  Kulik & Eschenbach (1999), A Geometry of Oriented Curv
%          : [EHK99] Eschenbach et al. (1999), Representing Simple Trajecto
% Source   : [TPTP]
% Names    :

% Status   : Unknown
% Rating   : 1.00 v2.4.0
% Syntax   : Number of clauses     :   97 (  42 non-Horn;   4 unit;  87 RR)
%            Number of atoms       :  300 (  40 equality)
%            Maximal clause size   :   12 (   3 average)
%            Number of predicates  :   14 (   0 propositional; 1-4 arity)
%            Number of functors    :   27 (   1 constant; 0-5 arity)
%            Number of variables   :  277 (  18 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNK_RFO_SEQ_NHN

% Comments : Created by tptp2X -f tptp -t clausify:otter GEO122+1.p
%--------------------------------------------------------------------------
%----Include simple curve axioms
include('Axioms/GEO004-0.ax').
%----Include axioms of betweenness for simple curves
include('Axioms/GEO004-1.ax').
%----Include oriented curve axioms
include('Axioms/GEO004-2.ax').
%--------------------------------------------------------------------------
cnf(corollary_4_8_133,negated_conjecture,
    ( ~ finish_point(A,sk25) )).

%--------------------------------------------------------------------------
