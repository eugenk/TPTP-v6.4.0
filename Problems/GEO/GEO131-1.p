%--------------------------------------------------------------------------
% File     : GEO131-1 : TPTP v6.4.0. Released v2.4.0.
% Domain   : Geometry (Oriented curves)
% Problem  : Betweenness and precedence for three points, corollary
% Version  : [EHK99] axioms.
% English  :

% Refs     : [KE99]  Kulik & Eschenbach (1999), A Geometry of Oriented Curv
%          : [EHK99] Eschenbach et al. (1999), Representing Simple Trajecto
% Source   : [TPTP]
% Names    :

% Status   : Unknown
% Rating   : 1.00 v2.4.0
% Syntax   : Number of clauses     :  101 (  43 non-Horn;   4 unit;  91 RR)
%            Number of atoms       :  308 (  40 equality)
%            Maximal clause size   :   12 (   3 average)
%            Number of predicates  :   15 (   0 propositional; 1-4 arity)
%            Number of functors    :   30 (   4 constant; 0-5 arity)
%            Number of variables   :  276 (  17 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNK_RFO_SEQ_NHN

% Comments : Created by tptp2X -f tptp -t clausify:otter GEO131+1.p
%--------------------------------------------------------------------------
%----Include simple curve axioms
include('Axioms/GEO004-0.ax').
%----Include axioms of betweenness for simple curves
include('Axioms/GEO004-1.ax').
%----Include oriented curve axioms
include('Axioms/GEO004-2.ax').
%--------------------------------------------------------------------------
cnf(corollary_4_16_133,negated_conjecture,
    ( between(sk28,sk25,sk26,sk27) )).

cnf(corollary_4_16_134,negated_conjecture,
    ( ordered_by(sk28,sk25,sk26)
    | ordered_by(sk28,sk25,sk27) )).

cnf(corollary_4_16_135,negated_conjecture,
    ( ordered_by(sk28,sk25,sk26)
    | ~ ordered_by(sk28,sk25,sk26) )).

cnf(corollary_4_16_136,negated_conjecture,
    ( ~ ordered_by(sk28,sk25,sk27)
    | ordered_by(sk28,sk25,sk27) )).

cnf(corollary_4_16_137,negated_conjecture,
    ( ~ ordered_by(sk28,sk25,sk27)
    | ~ ordered_by(sk28,sk25,sk26) )).

%--------------------------------------------------------------------------
