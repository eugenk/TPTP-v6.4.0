%------------------------------------------------------------------------------
% File     : GEO234+3 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Geometry (Constructive)
% Problem  : Unequally directed lines
% Version  : [vPl98] axioms.
% English  :

% Refs     : [vPl98] von Plato (1998), A Constructive Theory of Ordered Aff
%          : [ROK06] Raths et al. (2006), The ILTP Problem Library for Intu
%          : [Rat07] Raths (2007), Email to Geoff Sutcliffe
% Source   : [Rat07]
% Names    : Therorem 3.8 [vPl98]

% Status   : Theorem
% Rating   : 0.07 v6.4.0, 0.00 v6.1.0, 0.12 v6.0.0, 0.25 v5.5.0, 0.12 v5.4.0, 0.13 v5.3.0, 0.22 v5.2.0, 0.14 v5.0.0, 0.05 v4.1.0, 0.06 v4.0.1, 0.11 v4.0.0
% Syntax   : Number of formulae    :   37 (   6 unit)
%            Number of atoms       :  112 (   0 equality)
%            Maximal formula depth :   13 (   5 average)
%            Number of connectives :   87 (  12   ~;  23   |;  24   &)
%                                         (  10 <=>;  18  =>;   0  <=)
%                                         (   0 <~>;   0  ~|;   0  ~&)
%            Number of predicates  :   18 (   0 propositional; 1-4 arity)
%            Number of functors    :    4 (   0 constant; 1-2 arity)
%            Number of variables   :   84 (   0 sgn;  84   !;   0   ?)
%            Maximal term depth    :    3 (   1 average)
% SPC      : FOF_THM_RFO_NEQ

% Comments :
%------------------------------------------------------------------------------
include('Axioms/GEO009+0.ax').
%------------------------------------------------------------------------------
fof(con,conjecture,(
    ! [L,M,N] :
      ( unequally_directed_opposite_lines(L,M)
     => ( unequally_directed_lines(L,N)
        | unequally_directed_opposite_lines(M,N) ) ) )).

%------------------------------------------------------------------------------
