%------------------------------------------------------------------------------
% File     : GEO255+1 : TPTP v6.4.0. Bugfixed v6.4.0.
% Domain   : Geometry (Constructive)
% Problem  : Property of order and betweeness
% Version  : [vPl98] axioms : Especial.
% English  :

% Refs     : [vPl98] von Plato (1998), A Constructive Theory of Ordered Aff
%          : [ROK06] Raths et al. (2006), The ILTP Problem Library for Intu
% Source   : [ILTP]
% Names    : Theorem 6.2.i [vPl98]

% Status   : Theorem
% Rating   : 0.00 v6.4.0
% Syntax   : Number of formulae    :   32 (   7 unit)
%            Number of atoms       :  106 (   0 equality)
%            Maximal formula depth :   13 (   6 average)
%            Number of connectives :   91 (  17   ~;  24   |;  27   &)
%                                         (   5 <=>;  18  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   12 (   0 propositional; 1-4 arity)
%            Number of functors    :    4 (   0 constant; 1-2 arity)
%            Number of variables   :   74 (   0 sgn;  74   !;   0   ?)
%            Maximal term depth    :    3 (   1 average)
% SPC      : FOF_THM_RFO_NEQ

% Comments : Definitions unfolded, hence Especial.
% Bugfixes : v6.4.0 - Bugfix in GEO007+1.ax, fixed conjecture.
%------------------------------------------------------------------------------
include('Axioms/GEO007+0.ax').
%------------------------------------------------------------------------------
fof(con,conjecture,(
    ! [L,A,B] : 
      ( ( line(L) 
        & distinct_points(A,B) )
     => ~ ( before_on_line(L,A,B)
          & before_on_line(L,B,A) ) ) )).

%------------------------------------------------------------------------------
