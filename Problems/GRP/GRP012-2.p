%--------------------------------------------------------------------------
% File     : GRP012-2 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Group Theory
% Problem  : Inverse of products = Product of inverses
% Version  : [MOW76] axioms.
%            Theorem formulation : Expressed as a.b = c and b^-1.a^-1
%            = d, then c^-1 = d
% English  : The inverse of products equals the product of the inverse,
%            in opposite order.

% Refs     : [LS74]  Lawrence & Starkey (1974), Experimental tests of resol
%          : [WM76]  Wilson & Minker (1976), Resolution, Refinements, and S
% Source   : [SPRFN]
% Names    : ls36 [LS74]
%          : ls36 [WM76]

% Status   : Unsatisfiable
% Rating   : 0.00 v6.0.0, 0.22 v5.5.0, 0.12 v5.4.0, 0.13 v5.3.0, 0.25 v5.1.0, 0.14 v5.0.0, 0.00 v2.0.0
% Syntax   : Number of clauses     :   11 (   0 non-Horn;   8 unit;   6 RR)
%            Number of atoms       :   19 (   2 equality)
%            Maximal clause size   :    4 (   2 average)
%            Number of predicates  :    2 (   0 propositional; 2-3 arity)
%            Number of functors    :    7 (   5 constant; 0-2 arity)
%            Number of variables   :   22 (   0 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include group theory axioms
include('Axioms/GRP003-0.ax').
%--------------------------------------------------------------------------
cnf(a_multiply_b_is_c,hypothesis,
    ( product(a,b,c) )).

cnf(inverse_b_multiply_inverse_a_is_d,hypothesis,
    ( product(inverse(b),inverse(a),d) )).

cnf(prove_c_inverse_equals_d,negated_conjecture,
    (  inverse(c) != d )).

%--------------------------------------------------------------------------
