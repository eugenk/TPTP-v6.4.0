%--------------------------------------------------------------------------
% File     : GRP036-3 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Group Theory (Subgroups)
% Problem  : In subgroups, the identity element is unique
% Version  : [Wos65] axioms.
% English  :

% Refs     : [Wos65] Wos (1965), Unpublished Note
%          : [WM76]  Wilson & Minker (1976), Resolution, Refinements, and S
% Source   : [SPRFN]
% Names    : Problem 16 [Wos65]
%          : wos16 [WM76]

% Status   : Unsatisfiable
% Rating   : 0.00 v5.5.0, 0.06 v5.4.0, 0.00 v5.3.0, 0.08 v5.2.0, 0.00 v2.0.0
% Syntax   : Number of clauses     :   16 (   0 non-Horn;   7 unit;  11 RR)
%            Number of atoms       :   32 (   2 equality)
%            Maximal clause size   :    4 (   2 average)
%            Number of predicates  :    3 (   0 propositional; 1-3 arity)
%            Number of functors    :    5 (   2 constant; 0-2 arity)
%            Number of variables   :   30 (   0 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include group theory axioms
include('Axioms/GRP003-0.ax').
%----Include sub-group theory axioms
include('Axioms/GRP003-2.ax').
%--------------------------------------------------------------------------
cnf(another_left_identity,hypothesis,
    ( ~ subgroup_member(A)
    | product(another_identity,A,A) )).

cnf(another_right_identity,hypothesis,
    ( ~ subgroup_member(A)
    | product(A,another_identity,A) )).

cnf(another_right_inverse,hypothesis,
    ( ~ subgroup_member(A)
    | product(A,another_inverse(A),another_identity) )).

cnf(another_left_inverse,hypothesis,
    ( ~ subgroup_member(A)
    | product(another_inverse(A),A,another_identity) )).

cnf(another_inverse_in_subgroup,hypothesis,
    ( ~ subgroup_member(A)
    | subgroup_member(another_inverse(A)) )).

cnf(another_identity_in_subgroup,hypothesis,
    ( subgroup_member(another_identity) )).

cnf(prove_identity_equals_another_identity,negated_conjecture,
    (  identity != another_identity )).

%--------------------------------------------------------------------------
