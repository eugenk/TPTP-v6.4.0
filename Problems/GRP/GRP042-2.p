%--------------------------------------------------------------------------
% File     : GRP042-2 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Group Theory
% Problem  : Symmetry is dependent
% Version  : [Ver92] axioms.
% English  :

% Refs     : [Ver92] Veroff (1992), Email to G. Sutcliffe
% Source   : [Ver92]
% Names    : - [Ver92]

% Status   : Unsatisfiable
% Rating   : 0.00 v5.4.0, 0.06 v5.3.0, 0.10 v5.2.0, 0.08 v5.1.0, 0.06 v5.0.0, 0.00 v2.0.0
% Syntax   : Number of clauses     :    9 (   0 non-Horn;   5 unit;   6 RR)
%            Number of atoms       :   19 (   0 equality)
%            Maximal clause size   :    4 (   2 average)
%            Number of predicates  :    2 (   0 propositional; 2-3 arity)
%            Number of functors    :    5 (   3 constant; 0-2 arity)
%            Number of variables   :   24 (   0 singleton)
%            Maximal term depth    :    2 (   1 average)
% SPC      : CNF_UNS_RFO_NEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include Veroff's minimal axiom set
include('Axioms/GRP005-0.ax').
%--------------------------------------------------------------------------
cnf(a_equals_b,hypothesis,
    ( equalish(a,b) )).

cnf(prove_symmetry,negated_conjecture,
    ( ~ equalish(b,a) )).

%--------------------------------------------------------------------------
