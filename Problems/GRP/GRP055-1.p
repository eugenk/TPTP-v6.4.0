%--------------------------------------------------------------------------
% File     : GRP055-1 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Group Theory
% Problem  : Single axiom for group theory, in product & inverse
% Version  : [McC93] (equality) axioms.
% English  : This is a single axiom for group theory, in terms of product
%            and inverse.

% Refs     : [Kun92] Kunen (1992), Single Axioms for Groups
%          : [McC93] McCune (1993), Single Axioms for Groups and Abelian Gr
% Source   : [Kun92]
% Names    : C7 [Kun92]

% Status   : Unsatisfiable
% Rating   : 0.38 v6.4.0, 0.43 v6.3.0, 0.30 v6.1.0, 0.55 v6.0.0, 0.43 v5.5.0, 0.50 v5.4.0, 0.56 v5.3.0, 0.60 v5.2.0, 0.38 v5.1.0, 0.33 v5.0.0, 0.40 v4.1.0, 0.33 v4.0.1, 0.38 v4.0.0, 0.29 v3.7.0, 0.14 v3.4.0, 0.17 v3.3.0, 0.22 v3.2.0, 0.11 v3.1.0, 0.20 v2.7.0, 0.12 v2.6.0, 0.17 v2.5.0, 0.25 v2.4.0, 0.50 v2.2.0, 0.75 v2.1.0, 1.00 v2.0.0
% Syntax   : Number of clauses     :    2 (   0 non-Horn;   1 unit;   1 RR)
%            Number of atoms       :    4 (   4 equality)
%            Maximal clause size   :    3 (   2 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    9 (   7 constant; 0-2 arity)
%            Number of variables   :    3 (   0 singleton)
%            Maximal term depth    :   11 (   4 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
%--------------------------------------------------------------------------
cnf(single_axiom,axiom,
    ( inverse(multiply(inverse(multiply(Z,inverse(multiply(inverse(X),multiply(inverse(Y),inverse(multiply(inverse(Y),Y))))))),multiply(Z,Y))) = X )).

cnf(prove_these_axioms,negated_conjecture,
    ( multiply(inverse(a1),a1) != multiply(inverse(b1),b1)
    | multiply(multiply(inverse(b2),b2),a2) != a2
    | multiply(multiply(a3,b3),c3) != multiply(a3,multiply(b3,c3)) )).

%--------------------------------------------------------------------------
