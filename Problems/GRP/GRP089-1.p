%--------------------------------------------------------------------------
% File     : GRP089-1 : TPTP v6.4.0. Bugfixed v2.7.0.
% Domain   : Group Theory (Abelian)
% Problem  : Single axiom for Abelian group theory, in division
% Version  : [McC93] (equality) axioms.
% English  : This is a single axiom for Abelian group theory, in terms
%            of division

% Refs     : [McC93] McCune (1993), Single Axioms for Groups and Abelian Gr
% Source   : [McC93]
% Names    : Axiom 3.8.1 [McC93]

% Status   : Unsatisfiable
% Rating   : 0.15 v6.4.0, 0.29 v6.3.0, 0.20 v6.2.0, 0.30 v6.1.0, 0.18 v6.0.0, 0.14 v5.5.0, 0.25 v5.4.0, 0.11 v5.3.0, 0.30 v5.2.0, 0.12 v5.1.0, 0.22 v5.0.0, 0.30 v4.1.0, 0.22 v4.0.1, 0.25 v4.0.0, 0.14 v3.4.0, 0.00 v3.3.0, 0.11 v3.2.0, 0.00 v2.7.0
% Syntax   : Number of clauses     :    4 (   0 non-Horn;   3 unit;   1 RR)
%            Number of atoms       :    7 (   7 equality)
%            Maximal clause size   :    4 (   2 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :   12 (   9 constant; 0-2 arity)
%            Number of variables   :    8 (   0 singleton)
%            Maximal term depth    :    4 (   3 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
% Bugfixes : v2.3.0 - Deleted division, added multiply and inverse.
%          : v2.7.0 - Grounded conjecture
%--------------------------------------------------------------------------
cnf(single_axiom,axiom,
    ( divide(X,divide(divide(X,Y),divide(Z,Y))) = Z )).

%----Definition of multiply
cnf(multiply,axiom,
    ( multiply(X,Y) = divide(X,divide(divide(Z,Z),Y)) )).

%----Definition of inverse
cnf(inverse,axiom,
    ( inverse(X) = divide(divide(Z,Z),X) )).

cnf(prove_these_axioms,negated_conjecture,
    ( multiply(inverse(a1),a1) != multiply(inverse(b1),b1)
    | multiply(multiply(inverse(b2),b2),a2) != a2
    | multiply(multiply(a3,b3),c3) != multiply(a3,multiply(b3,c3))
    | multiply(a4,b4) != multiply(b4,a4) )).

%--------------------------------------------------------------------------
