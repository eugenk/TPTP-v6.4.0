%--------------------------------------------------------------------------
% File     : GRP166-2 : TPTP v6.4.0. Bugfixed v1.2.1.
% Domain   : Group Theory (Lattice Ordered)
% Problem  : Multiplication with a positive element increases a value
% Version  : [Fuc94] (equality) axioms.
%            Theorem formulation : Dual.
% English  :

% Refs     : [Fuc94] Fuchs (1994), The Application of Goal-Orientated Heuri
%          : [Sch95] Schulz (1995), Explanation Based Learning for Distribu
% Source   : [Sch95]
% Names    : lat2b [Sch95]

% Status   : Unsatisfiable
% Rating   : 0.11 v6.4.0, 0.16 v6.3.0, 0.18 v6.2.0, 0.14 v6.1.0, 0.12 v6.0.0, 0.33 v5.5.0, 0.32 v5.4.0, 0.13 v5.3.0, 0.00 v5.2.0, 0.07 v5.0.0, 0.00 v4.0.1, 0.07 v4.0.0, 0.08 v3.7.0, 0.00 v2.2.1, 0.22 v2.2.0, 0.29 v2.1.0, 0.29 v2.0.0
% Syntax   : Number of clauses     :   18 (   0 non-Horn;  18 unit;   3 RR)
%            Number of atoms       :   18 (  18 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    7 (   3 constant; 0-2 arity)
%            Number of variables   :   33 (   2 singleton)
%            Maximal term depth    :    3 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments : ORDERING LPO inverse > product > greatest_lower_bound >
%            least_upper_bound > identity > a > b
% Bugfixes : v1.2.1 - Duplicate axioms in GRP004-2.ax removed.
%--------------------------------------------------------------------------
%----Include equality group theory axioms
include('Axioms/GRP004-0.ax').
%----Include Lattice ordered group (equality) axioms
include('Axioms/GRP004-2.ax').
%--------------------------------------------------------------------------
cnf(lat2b_1,hypothesis,
    ( greatest_lower_bound(a,identity) = identity )).

cnf(lat2b_2,hypothesis,
    ( greatest_lower_bound(b,identity) = identity )).

cnf(prove_lat2b,negated_conjecture,
    (  greatest_lower_bound(a,multiply(a,b)) != a )).

%--------------------------------------------------------------------------
