%--------------------------------------------------------------------------
% File     : GRP415-1 : TPTP v6.4.0. Released v2.6.0.
% Domain   : Group Theory
% Problem  : Axiom for group theory, in product & inverse, part 1
% Version  : [McC93] (equality) axioms.
% English  :

% Refs     : [Kun92] Kunen (1992), Single Axioms for Groups
%          : [McC93] McCune (1993), Single Axioms for Groups and Abelian Gr
% Source   : [TPTP]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.16 v6.4.0, 0.26 v6.3.0, 0.29 v6.1.0, 0.25 v6.0.0, 0.38 v5.5.0, 0.32 v5.4.0, 0.13 v5.3.0, 0.08 v5.2.0, 0.14 v5.1.0, 0.13 v5.0.0, 0.14 v4.1.0, 0.09 v4.0.1, 0.07 v4.0.0, 0.08 v3.7.0, 0.11 v3.4.0, 0.12 v3.3.0, 0.00 v3.1.0, 0.11 v2.7.0, 0.00 v2.6.0
% Syntax   : Number of clauses     :    2 (   0 non-Horn;   2 unit;   1 RR)
%            Number of atoms       :    2 (   2 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    4 (   2 constant; 0-2 arity)
%            Number of variables   :    3 (   0 singleton)
%            Maximal term depth    :    9 (   4 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments : A UEQ part of GRP053-1
%--------------------------------------------------------------------------
cnf(single_axiom,axiom,
    ( inverse(multiply(A,inverse(multiply(inverse(multiply(inverse(multiply(B,A)),multiply(B,inverse(C)))),inverse(multiply(inverse(A),A)))))) = C )).

cnf(prove_these_axioms_1,negated_conjecture,
    (  multiply(inverse(a1),a1) != multiply(inverse(b1),b1) )).

%--------------------------------------------------------------------------
