%--------------------------------------------------------------------------
% File     : GRP582-1 : TPTP v6.4.0. Released v2.6.0.
% Domain   : Group Theory (Abelian)
% Problem  : Axiom for Abelian group theory, in double div and id, part 2
% Version  : [McC93] (equality) axioms.
% English  :

% Refs     : [McC93] McCune (1993), Single Axioms for Groups and Abelian Gr
% Source   : [TPTP]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.05 v6.4.0, 0.11 v6.3.0, 0.12 v6.2.0, 0.14 v6.1.0, 0.06 v6.0.0, 0.19 v5.5.0, 0.16 v5.4.0, 0.00 v5.2.0, 0.07 v5.1.0, 0.00 v2.6.0
% Syntax   : Number of clauses     :    5 (   0 non-Horn;   5 unit;   1 RR)
%            Number of atoms       :    5 (   5 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    5 (   2 constant; 0-2 arity)
%            Number of variables   :    7 (   0 singleton)
%            Maximal term depth    :    6 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments : A UEQ part of GRP103-1
%--------------------------------------------------------------------------
cnf(single_axiom,axiom,
    ( double_divide(double_divide(A,double_divide(double_divide(identity,B),double_divide(C,double_divide(B,A)))),double_divide(identity,identity)) = C )).

cnf(multiply,axiom,
    ( multiply(A,B) = double_divide(double_divide(B,A),identity) )).

cnf(inverse,axiom,
    ( inverse(A) = double_divide(A,identity) )).

cnf(identity,axiom,
    ( identity = double_divide(A,inverse(A)) )).

cnf(prove_these_axioms_2,negated_conjecture,
    (  multiply(identity,a2) != a2 )).

%--------------------------------------------------------------------------
