%--------------------------------------------------------------------------
% File     : GRP591-1 : TPTP v6.4.0. Released v2.6.0.
% Domain   : Group Theory (Abelian)
% Problem  : Axiom for Abelian group theory, in double div and inv, part 3
% Version  : [McC93] (equality) axioms.
% English  :

% Refs     : [McC93] McCune (1993), Single Axioms for Groups and Abelian Gr
% Source   : [TPTP]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.21 v6.4.0, 0.26 v6.3.0, 0.24 v6.2.0, 0.21 v6.1.0, 0.25 v6.0.0, 0.38 v5.5.0, 0.42 v5.4.0, 0.27 v5.3.0, 0.17 v5.2.0, 0.21 v5.1.0, 0.20 v5.0.0, 0.07 v4.1.0, 0.00 v4.0.1, 0.07 v4.0.0, 0.08 v3.7.0, 0.00 v3.4.0, 0.12 v3.3.0, 0.14 v3.2.0, 0.07 v3.1.0, 0.11 v2.7.0, 0.00 v2.6.0
% Syntax   : Number of clauses     :    3 (   0 non-Horn;   3 unit;   1 RR)
%            Number of atoms       :    3 (   3 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    6 (   3 constant; 0-2 arity)
%            Number of variables   :    5 (   0 singleton)
%            Maximal term depth    :    7 (   3 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments : A UEQ part of GRP105-1
%--------------------------------------------------------------------------
cnf(single_axiom,axiom,
    ( double_divide(inverse(double_divide(double_divide(A,B),inverse(double_divide(A,inverse(C))))),B) = C )).

cnf(multiply,axiom,
    ( multiply(A,B) = inverse(double_divide(B,A)) )).

cnf(prove_these_axioms_3,negated_conjecture,
    (  multiply(multiply(a3,b3),c3) != multiply(a3,multiply(b3,c3)) )).

%--------------------------------------------------------------------------
