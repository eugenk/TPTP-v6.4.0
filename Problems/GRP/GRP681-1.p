%------------------------------------------------------------------------------
% File     : GRP681-1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : Commutants in Bol loops 3
% Version  : Especial.
% English  : If Q is a Bol loop, and if a,b in C(Q), then so are a^2, b^-1,
%            and a^2b.

% Refs     : [KP04]  Kinyon & Phillips (2004), Commutants of Bol Loops of O
%          : [PS08]  Phillips & Stanovsky (2008), Automated Theorem Proving
%          : [Sta08] Stanovsky (2008), Email to G. Sutcliffe
% Source   : [Sta08]
% Names    : KP04 [PS08]

% Status   : Unsatisfiable
% Rating   : 0.47 v6.4.0, 0.63 v6.3.0, 0.59 v6.2.0, 0.57 v6.1.0, 0.69 v6.0.0, 0.81 v5.5.0, 0.79 v5.4.0, 0.73 v5.3.0, 0.67 v5.2.0, 0.64 v5.1.0, 0.67 v5.0.0, 0.64 v4.0.1, 0.79 v4.0.0
% Syntax   : Number of clauses     :   10 (   0 non-Horn;  10 unit;   1 RR)
%            Number of atoms       :   10 (  10 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    7 (   4 constant; 0-2 arity)
%            Number of variables   :   15 (   0 singleton)
%            Maximal term depth    :    4 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments :
%------------------------------------------------------------------------------
cnf(c01,axiom,
    ( mult(A,ld(A,B)) = B )).

cnf(c02,axiom,
    ( ld(A,mult(A,B)) = B )).

cnf(c03,axiom,
    ( mult(rd(A,B),B) = A )).

cnf(c04,axiom,
    ( rd(mult(A,B),B) = A )).

cnf(c05,axiom,
    ( mult(A,unit) = A )).

cnf(c06,axiom,
    ( mult(unit,A) = A )).

cnf(c07,axiom,
    ( mult(A,mult(B,mult(A,C))) = mult(mult(A,mult(B,A)),C) )).

cnf(c08,axiom,
    ( mult(op_c,A) = mult(A,op_c) )).

cnf(c09,axiom,
    ( mult(op_d,A) = mult(A,op_d) )).

cnf(goals,negated_conjecture,
    ( mult(mult(mult(op_c,op_c),op_d),a) != mult(a,mult(mult(op_c,op_c),op_d)) )).

%------------------------------------------------------------------------------
