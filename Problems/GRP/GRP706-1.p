%------------------------------------------------------------------------------
% File     : GRP706-1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : Every F-quasigroup is isotopic to a Moufang loop
% Version  : Especial.
% English  :

% Refs     : [KKP07] Kepka et al. (2007), The Structure of F-Quasigroups
%          : [PS08]  Phillips & Stanovsky (2008), Automated Theorem Proving
%          : [Sta08] Stanovsky (2008), Email to G. Sutcliffe
% Source   : [Sta08]
% Names    : KKP07 [PS08]

% Status   : Unsatisfiable
% Rating   : 0.84 v6.3.0, 0.82 v6.2.0, 0.79 v6.1.0, 0.88 v6.0.0, 0.90 v5.5.0, 0.95 v5.4.0, 0.93 v5.3.0, 0.92 v5.2.0, 0.93 v4.1.0, 1.00 v4.0.0
% Syntax   : Number of clauses     :    8 (   0 non-Horn;   8 unit;   1 RR)
%            Number of atoms       :    8 (   8 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    8 (   4 constant; 0-2 arity)
%            Number of variables   :   16 (   0 singleton)
%            Maximal term depth    :    4 (   3 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments :
%------------------------------------------------------------------------------
cnf(c01,axiom,
    ( mult(A,ld(A,B)) = B )).

cnf(c02,axiom,
    ( ld(A,mult(A,B)) = B )).

cnf(c03,axiom,
    ( mult(rd(A,B),B) = A )).

cnf(c04,axiom,
    ( rd(mult(A,B),B) = A )).

cnf(c05,axiom,
    ( mult(A,mult(B,C)) = mult(mult(A,B),mult(ld(A,A),C)) )).

cnf(c06,axiom,
    ( mult(mult(A,B),C) = mult(mult(A,rd(C,C)),mult(B,C)) )).

cnf(c07,axiom,
    ( f(A,B) = mult(rd(A,op_c),ld(op_c,B)) )).

cnf(goals,negated_conjecture,
    ( f(a,f(b,f(a,c))) != f(f(f(a,b),a),c) )).

%------------------------------------------------------------------------------
