%------------------------------------------------------------------------------
% File     : GRP730-1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : Bruck loops that are centrally nilpotent - 2nd easy part c
% Version  : Especial.
% English  : Bruck loops with abelian inner mapping group are centrally
%            nilpotent of class two - 2nd easy part.

% Refs     : [Sta08] Stanovsky (2008), Email to G. Sutcliffe
% Source   : [Sta08]
% Names    : PSxx_4c [Sta08]

% Status   : Unsatisfiable
% Rating   : 0.37 v6.4.0, 0.42 v6.3.0, 0.41 v6.2.0, 0.50 v6.1.0, 0.56 v6.0.0, 0.62 v5.5.0, 0.63 v5.4.0, 0.60 v5.3.0, 0.67 v5.2.0, 0.64 v5.1.0, 0.67 v5.0.0, 0.64 v4.0.0
% Syntax   : Number of clauses     :   23 (   0 non-Horn;  23 unit;   1 RR)
%            Number of atoms       :   23 (  23 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :   13 (   5 constant; 0-3 arity)
%            Number of variables   :   64 (  10 singleton)
%            Maximal term depth    :    4 (   3 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments :
%------------------------------------------------------------------------------
cnf(c01,axiom,
    ( mult(unit,A) = A )).

cnf(c02,axiom,
    ( mult(A,unit) = A )).

cnf(c03,axiom,
    ( mult(A,i(A)) = unit )).

cnf(c04,axiom,
    ( mult(i(A),A) = unit )).

cnf(c05,axiom,
    ( i(mult(A,B)) = mult(i(A),i(B)) )).

cnf(c06,axiom,
    ( mult(i(A),mult(A,B)) = B )).

cnf(c07,axiom,
    ( rd(mult(A,B),B) = A )).

cnf(c08,axiom,
    ( mult(rd(A,B),B) = A )).

cnf(c09,axiom,
    ( mult(mult(A,mult(B,A)),C) = mult(A,mult(B,mult(A,C))) )).

cnf(c10,axiom,
    ( mult(mult(A,B),C) = mult(mult(A,mult(B,C)),asoc(A,B,C)) )).

cnf(c11,axiom,
    ( mult(A,B) = mult(mult(B,A),op_k(A,B)) )).

cnf(c12,axiom,
    ( op_l(A,B,C) = mult(i(mult(C,B)),mult(C,mult(B,A))) )).

cnf(c13,axiom,
    ( op_r(A,B,C) = rd(mult(mult(A,B),C),mult(B,C)) )).

cnf(c14,axiom,
    ( op_t(A,B) = mult(i(B),mult(A,B)) )).

cnf(c15,axiom,
    ( op_r(op_r(A,B,C),D,E) = op_r(op_r(A,D,E),B,C) )).

cnf(c16,axiom,
    ( op_l(op_r(A,B,C),D,E) = op_r(op_l(A,D,E),B,C) )).

cnf(c17,axiom,
    ( op_l(op_l(A,B,C),D,E) = op_l(op_l(A,D,E),B,C) )).

cnf(c18,axiom,
    ( op_t(op_r(A,B,C),D) = op_r(op_t(A,D),B,C) )).

cnf(c19,axiom,
    ( op_t(op_l(A,B,C),D) = op_l(op_t(A,D),B,C) )).

cnf(c20,axiom,
    ( op_t(op_t(A,B),C) = op_t(op_t(A,C),B) )).

cnf(c21,axiom,
    ( asoc(asoc(A,B,C),D,E) = unit )).

cnf(c22,axiom,
    ( asoc(A,B,asoc(C,D,E)) = unit )).

cnf(goals,negated_conjecture,
    ( asoc(op_k(a,b),c,d) != unit )).

%------------------------------------------------------------------------------
