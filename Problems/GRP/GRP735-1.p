%------------------------------------------------------------------------------
% File     : GRP735-1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Group Theory (Quasigroups)
% Problem  : Nonmedial left distributive quasigroup
% Version  : Especial.
% English  :

% Refs     : [Sta08] Stanovsky (2008), Email to G. Sutcliffe
% Source   : [Sta08]
% Names    :

% Status   : Satisfiable
% Rating   : 0.67 v6.4.0, 0.75 v6.3.0, 0.67 v6.2.0, 0.83 v6.1.0, 1.00 v4.0.0
% Syntax   : Number of clauses     :    6 (   0 non-Horn;   6 unit;   1 RR)
%            Number of atoms       :    6 (   6 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    7 (   4 constant; 0-2 arity)
%            Number of variables   :   11 (   0 singleton)
%            Maximal term depth    :    3 (   2 average)
% SPC      : CNF_SAT_RFO_PEQ_UEQ

% Comments : Size 15
%------------------------------------------------------------------------------
cnf(c01,axiom,
    ( mult(A,mult(B,C)) = mult(mult(A,B),mult(A,C)) )).

cnf(c02,axiom,
    ( mult(A,ld(A,B)) = B )).

cnf(c03,axiom,
    ( ld(A,mult(A,B)) = B )).

cnf(c04,axiom,
    ( rd(mult(A,B),B) = A )).

cnf(c05,axiom,
    ( mult(rd(A,B),B) = A )).

cnf(goals,negated_conjecture,
    ( mult(mult(a,b),mult(c,d)) != mult(mult(a,c),mult(b,d)) )).

%------------------------------------------------------------------------------
