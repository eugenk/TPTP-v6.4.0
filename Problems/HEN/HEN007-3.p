%--------------------------------------------------------------------------
% File     : HEN007-3 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Henkin Models
% Problem  : X <= Y => Z/Y <= Z/X
% Version  : [MOW76] axioms.
% English  :

% Refs     : [MOW76] McCharen et al. (1976), Problems and Experiments for a
% Source   : [ANL]
% Names    : HP7 [ANL]

% Status   : Unsatisfiable
% Rating   : 0.00 v6.0.0, 0.22 v5.5.0, 0.38 v5.4.0, 0.40 v5.3.0, 0.50 v5.2.0, 0.25 v5.1.0, 0.14 v4.1.0, 0.11 v4.0.1, 0.00 v3.3.0, 0.14 v3.2.0, 0.00 v3.1.0, 0.11 v2.7.0, 0.00 v2.6.0, 0.29 v2.5.0, 0.00 v2.2.1, 0.33 v2.2.0, 0.43 v2.1.0, 0.60 v2.0.0
% Syntax   : Number of clauses     :    9 (   0 non-Horn;   6 unit;   5 RR)
%            Number of atoms       :   13 (   3 equality)
%            Maximal clause size   :    3 (   1 average)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :    6 (   5 constant; 0-2 arity)
%            Number of variables   :   13 (   3 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include Henkin model axioms for equality formulation
include('Axioms/HEN002-0.ax').
%--------------------------------------------------------------------------
cnf(a_LE_b,hypothesis,
    ( less_equal(a,b) )).

cnf(prove_c_divide_b_LE_c_divide_a,negated_conjecture,
    ( ~ less_equal(divide(c,b),divide(c,a)) )).

%--------------------------------------------------------------------------
