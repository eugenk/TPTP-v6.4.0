%--------------------------------------------------------------------------
% File     : HEN007-4 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Henkin Models
% Problem  : X <= Y => Z/Y <= Z/X
% Version  : [MOW76] axioms : Augmented.
% English  :

% Refs     : [MOW76] McCharen et al. (1976), Problems and Experiments for a
% Source   : [ANL]
% Names    : hp7.ver2.in [ANL]

% Status   : Unsatisfiable
% Rating   : 0.00 v6.0.0, 0.11 v5.5.0, 0.12 v5.4.0, 0.13 v5.3.0, 0.17 v5.2.0, 0.12 v5.1.0, 0.14 v4.1.0, 0.11 v4.0.1, 0.17 v3.7.0, 0.00 v3.3.0, 0.14 v3.2.0, 0.00 v2.0.0
% Syntax   : Number of clauses     :   15 (   0 non-Horn;  10 unit;   8 RR)
%            Number of atoms       :   22 (   7 equality)
%            Maximal clause size   :    3 (   1 average)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :    6 (   5 constant; 0-2 arity)
%            Number of variables   :   22 (   5 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include Henkin model axioms for equality formulation
include('Axioms/HEN002-0.ax').
%--------------------------------------------------------------------------
cnf(everything_divide_id_is_zero,axiom,
    ( divide(X,identity) = zero )).

cnf(zero_divide_anything_is_zero,axiom,
    ( divide(zero,X) = zero )).

cnf(x_divide_x_is_zero,axiom,
    ( divide(X,X) = zero )).

cnf(x_divide_zero_is_x,axiom,
    ( divide(a,zero) = a )).

cnf(transitivity_of_less_equal,axiom,
    ( ~ less_equal(X,Y)
    | ~ less_equal(Y,Z)
    | less_equal(X,Z) )).

cnf(property_of_divide1,axiom,
    ( ~ less_equal(divide(X,Y),Z)
    | less_equal(divide(X,Z),Y) )).

cnf(a_LE_b,hypothesis,
    ( less_equal(a,b) )).

cnf(prove_c_divide_b_LE_c_divide_a,negated_conjecture,
    ( ~ less_equal(divide(c,b),divide(c,a)) )).

%--------------------------------------------------------------------------
