%--------------------------------------------------------------------------
% File     : HWV006-1 : TPTP v6.4.0. Released v2.1.0.
% Domain   : Hardware Verification
% Problem  : A fulladder built from two halfadders and an or gate
% Version  : [Gei96] axioms.
% English  :

% Refs     : [Gei96] Geisler (1996), Email to G. Sutcliffe
% Source   : [Gei96]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.25 v6.3.0, 0.29 v6.2.0, 0.11 v6.1.0, 0.00 v6.0.0, 0.14 v5.5.0, 0.12 v5.4.0, 0.20 v5.3.0, 0.10 v5.1.0, 0.09 v5.0.0, 0.21 v4.1.0, 0.25 v4.0.1, 0.20 v4.0.0, 0.14 v3.4.0, 0.25 v3.3.0, 0.00 v2.7.0, 0.12 v2.6.0, 0.33 v2.5.0, 0.40 v2.4.0, 0.20 v2.3.0, 0.33 v2.2.1, 0.33 v2.1.0
% Syntax   : Number of clauses     :   58 (   3 non-Horn;  13 unit;  58 RR)
%            Number of atoms       :  139 (   0 equality)
%            Maximal clause size   :    5 (   2 average)
%            Number of predicates  :    5 (   0 propositional; 2-2 arity)
%            Number of functors    :   21 (  13 constant; 0-2 arity)
%            Number of variables   :   52 (   3 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNS_RFO_NEQ_NHN

% Comments :
%--------------------------------------------------------------------------
%----Include basic diagnosis axioms
include('Axioms/HWV001-0.ax').
%----Include model of halfadder
include('Axioms/HWV001-1.ax').
%----Include model of fulladder
include('Axioms/HWV001-2.ax').
%--------------------------------------------------------------------------
%----Observations
cnf(f_isa_fulladder,hypothesis,
    ( type(f,fulladder) )).

cnf(in1_1,hypothesis,
    ( value(in(n1,f),n1) )).

cnf(in2_0,hypothesis,
    ( value(in(n2,f),n0) )).

cnf(inc_1,hypothesis,
    ( value(in(c,f),n1) )).

cnf(outs_1,hypothesis,
    ( value(out(s,f),n1) )).

cnf(outc_0,hypothesis,
    ( value(out(c,f),n0) )).

%----Minimal diagnosis
cnf(diagnosis_or1_not1h2,negated_conjecture,
    ( ~ mode(or1(f),abnormal)
    | ~ mode(not1(h2(f)),abnormal) )).

cnf(diagnosis_and2,negated_conjecture,
    ( ~ mode(and2(h2(f)),abnormal) )).

cnf(diagnosis_or1_and1h2,negated_conjecture,
    ( ~ mode(or1(f),abnormal)
    | ~ mode(and1(h2(f)),abnormal) )).

cnf(diagnosis_or1h1,negated_conjecture,
    ( ~ mode(or1(h1(f)),abnormal) )).

cnf(diagnosis_not1h1,negated_conjecture,
    ( ~ mode(not1(h1(f)),abnormal) )).

cnf(diagnosis_and2h1,negated_conjecture,
    ( ~ mode(and2(h1(f)),abnormal) )).

cnf(diagnosis_and1h1,negated_conjecture,
    ( ~ mode(and1(h1(f)),abnormal) )).

%--------------------------------------------------------------------------
