%--------------------------------------------------------------------------
% File     : HWV010-3 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Hardware Verification
% Problem  : Safelogic VHDL design verification obligation
% Version  : [Mar02] axioms : Especial.
% English  :

% Refs     : [CHM02] Claessen et al. (2002), Verification of Hardware Systems
%          : [Mar02] Martensson (2002), Email to G. Sutcliffe
% Source   : [Mar02]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.13 v6.4.0, 0.07 v6.3.0, 0.00 v6.2.0, 0.10 v6.1.0, 0.14 v6.0.0, 0.10 v5.5.0, 0.15 v5.4.0, 0.20 v5.3.0, 0.11 v5.2.0, 0.12 v5.0.0, 0.07 v4.1.0, 0.00 v3.3.0, 0.14 v3.2.0, 0.08 v3.1.0, 0.09 v2.7.0, 0.08 v2.6.0, 0.00 v2.5.0
% Syntax   : Number of clauses     :  135 (  62 non-Horn;  27 unit;  94 RR)
%            Number of atoms       :  417 (  81 equality)
%            Maximal clause size   :   10 (   3 average)
%            Number of predicates  :    4 (   0 propositional; 1-2 arity)
%            Number of functors    :   46 (   8 constant; 0-3 arity)
%            Number of variables   :  225 (  20 singleton)
%            Maximal term depth    :    5 (   2 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments :
%--------------------------------------------------------------------------
%----Include equality
%----Include VHDL design axioms
include('Axioms/HWV004-0.ax').
%--------------------------------------------------------------------------
cnf(quest_1,negated_conjecture,
    ( fwork_DOTfifo_DOTrtl_DOTlevel_(t_206) = n0 )).

cnf(quest_2,negated_conjecture,
    ( ~ p__pred_(fwork_DOTfifo_DOTrtl_DOTempty_(t_206)) )).

%--------------------------------------------------------------------------
