%--------------------------------------------------------------------------
% File     : HWV012-1 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Hardware Verification
% Problem  : Safelogic VHDL design verification obligation
% Version  : [Mar02] axioms : Especial.
% English  :

% Refs     : [CHM02] Claessen et al. (2002), Verification of Hardware Systems
%          : [Mar02] Martensson (2002), Email to G. Sutcliffe
% Source   : [Mar02]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.13 v6.4.0, 0.07 v6.3.0, 0.09 v6.2.0, 0.10 v6.1.0, 0.07 v6.0.0, 0.10 v5.5.0, 0.30 v5.3.0, 0.22 v5.2.0, 0.25 v5.1.0, 0.12 v5.0.0, 0.07 v4.1.0, 0.00 v4.0.1, 0.09 v4.0.0, 0.00 v3.3.0, 0.07 v3.2.0, 0.00 v3.1.0, 0.09 v2.7.0, 0.17 v2.6.0, 0.00 v2.5.0
% Syntax   : Number of clauses     :   93 (  64 non-Horn;   9 unit;  84 RR)
%            Number of atoms       :  372 (  50 equality)
%            Maximal clause size   :    7 (   4 average)
%            Number of predicates  :   13 (   0 propositional; 1-3 arity)
%            Number of functors    :   11 (   4 constant; 0-2 arity)
%            Number of variables   :  139 (   5 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments :
%--------------------------------------------------------------------------
%----Include equality
%----Include VHDL design axioms
include('Axioms/HWV003-0.ax').
%--------------------------------------------------------------------------
cnf(quest_1,negated_conjecture,
    ( gt(fifo_length,level(t_139)) )).

cnf(quest_2,negated_conjecture,
    ( p_Wr(t_139) )).

cnf(quest_3,negated_conjecture,
    ( p_Wr_error(plus(t_139,n1)) )).

%--------------------------------------------------------------------------
