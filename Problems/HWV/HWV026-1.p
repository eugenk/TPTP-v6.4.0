%--------------------------------------------------------------------------
% File     : HWV026-1 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Hardware Verification
% Problem  : Safelogic VHDL design verification obligation
% Version  : [Mar02] axioms : Especial.
% English  :

% Refs     : [CHM02] Claessen et al. (2002), Verification of Hardware Systems
%          : [Mar02] Martensson (2002), Email to G. Sutcliffe
% Source   : [Mar02]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.07 v6.4.0, 0.00 v5.5.0, 0.10 v5.3.0, 0.06 v5.0.0, 0.00 v3.3.0, 0.07 v3.2.0, 0.00 v3.1.0, 0.09 v2.7.0, 0.17 v2.6.0, 0.00 v2.5.0
% Syntax   : Number of clauses     :   94 (  65 non-Horn;   8 unit;  85 RR)
%            Number of atoms       :  375 (  50 equality)
%            Maximal clause size   :    7 (   4 average)
%            Number of predicates  :   13 (   0 propositional; 1-3 arity)
%            Number of functors    :   11 (   4 constant; 0-2 arity)
%            Number of variables   :  139 (   5 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments :
%--------------------------------------------------------------------------
%----Include equality
%----Include VHDL design axioms
include('Axioms/HWV003-0.ax').
%--------------------------------------------------------------------------
cnf(quest_1,negated_conjecture,
    ( ~ p_Wr(t_139) )).

cnf(quest_2,negated_conjecture,
    ( ~ p_Reset(t_139) )).

cnf(quest_3,negated_conjecture,
    ( ~ p_Wr_error(t_139)
    | ~ p_Wr_error(plus(t_139,n1)) )).

cnf(quest_4,negated_conjecture,
    ( p_Wr_error(t_139)
    | p_Wr_error(plus(t_139,n1)) )).

%--------------------------------------------------------------------------
