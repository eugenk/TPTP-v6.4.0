%--------------------------------------------------------------------------
% File     : HWV031-2 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Hardware Verification
% Problem  : Safelogic VHDL design verification obligation
% Version  : [Mar02] axioms : Especial.
% English  :

% Refs     : [CHM02] Claessen et al. (2002), Verification of Hardware Systems
%          : [Mar02] Martensson (2002), Email to G. Sutcliffe
% Source   : [Mar02]
% Names    :

% Status   : Unsatisfiable
% Rating   : 1.00 v2.5.0
% Syntax   : Number of clauses     :  136 (  62 non-Horn;  28 unit;  95 RR)
%            Number of atoms       :  418 (  80 equality)
%            Maximal clause size   :   10 (   3 average)
%            Number of predicates  :    4 (   0 propositional; 1-2 arity)
%            Number of functors    :   46 (   8 constant; 0-3 arity)
%            Number of variables   :  225 (  20 singleton)
%            Maximal term depth    :    5 (   2 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments :
%--------------------------------------------------------------------------
%----Include VHDL design axioms
include('Axioms/HWV004-0.ax').
%--------------------------------------------------------------------------
cnf(quest_1,negated_conjecture,
    ( p_LES_EQU_(f_ADD_(fwork_DOTfifo_DOTrtl_DOTwr__level_(t_206),n1),fwork_DOTfifo_DOTrtl_DOTfifo__length_) )).

cnf(quest_2,negated_conjecture,
    ( ~ p_LES_EQU_(f_ADD_(fwork_DOTfifo_DOTrtl_DOTwr__level_(f_ADD_(t_206,n1)),n1),fwork_DOTfifo_DOTrtl_DOTfifo__length_) )).

cnf(quest_3,negated_conjecture,
    ( ~ p_LES_EQU_(fwork_DOTfifo_DOTrtl_DOTfifo__length_,n0) )).

%--------------------------------------------------------------------------
