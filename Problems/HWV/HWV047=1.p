%------------------------------------------------------------------------------
% File     : HWV047=1 : TPTP v6.4.0. Released v5.3.0.
% Domain   : Software Verification
% Problem  : Robot verification problem 9
% Version  : Especial.
%            Theorem formulation: Epr encoding mode bitblast
% English  :

% Refs     : [Kha11] Khasidashvili (2011), Email to Geoff Sutcliffe
% Source   : [Kha11]
% Names    : robot_p9b200eBb.tfa [Kha11]

% Status   : Theorem
% Rating   : 0.71 v6.4.0, 1.00 v6.3.0, 0.71 v6.2.0, 0.75 v6.1.0, 0.78 v6.0.0, 1.00 v5.4.0, 0.88 v5.3.0
% Syntax   : Number of formulae    : 1226 ( 620 unit; 391 type)
%            Number of atoms       : 1564 ( 201 equality)
%            Maximal formula depth :  203 (   2 average)
%            Number of connectives :  899 ( 170   ~; 246   |; 116   &)
%                                         ( 282 <=>;  84  =>;   0  <=;   1 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  193 ( 187   >;   6   *;   0   +;   0  <<)
%            Number of predicates  :  584 ( 396 propositional; 0-2 arity)
%            Number of functors    :  204 ( 204 constant; 0-0 arity)
%            Number of variables   :  250 (   0 sgn; 250   !;   0   ?)
%                                         ( 250   :;   0  !>;   0  ?*)
%            Maximal term depth    :    1 (   1 average)
%            Arithmetic symbols    :    3 (   0 prd;   0 fun;   3 num;   0 var)
% SPC      : TF0_THM_EQU_ARI

% Comments : Verification strategy is bmc1 with bound 200, Verification model
%            is Sequential word-level (with or w/o memories)
%------------------------------------------------------------------------------
tff(sort_def_5,type,(
    state_type: $tType )).

tff(sort_def_6,type,(
    address_type: $tType )).

tff(func_def_0,type,(
    constB0: state_type )).

tff(func_def_4,type,(
    constB1: state_type )).

tff(func_def_5,type,(
    constB2: state_type )).

tff(func_def_6,type,(
    constB3: state_type )).

tff(func_def_7,type,(
    constB4: state_type )).

tff(func_def_8,type,(
    constB5: state_type )).

tff(func_def_9,type,(
    constB6: state_type )).

tff(func_def_10,type,(
    constB7: state_type )).

tff(func_def_11,type,(
    constB8: state_type )).

tff(func_def_12,type,(
    constB9: state_type )).

tff(func_def_13,type,(
    constB10: state_type )).

tff(func_def_14,type,(
    constB11: state_type )).

tff(func_def_15,type,(
    constB12: state_type )).

tff(func_def_16,type,(
    constB13: state_type )).

tff(func_def_17,type,(
    constB14: state_type )).

tff(func_def_18,type,(
    constB15: state_type )).

tff(func_def_19,type,(
    constB16: state_type )).

tff(func_def_20,type,(
    constB17: state_type )).

tff(func_def_21,type,(
    constB18: state_type )).

tff(func_def_22,type,(
    constB19: state_type )).

tff(func_def_23,type,(
    constB20: state_type )).

tff(func_def_24,type,(
    constB21: state_type )).

tff(func_def_25,type,(
    constB22: state_type )).

tff(func_def_26,type,(
    constB23: state_type )).

tff(func_def_27,type,(
    constB24: state_type )).

tff(func_def_28,type,(
    constB25: state_type )).

tff(func_def_29,type,(
    constB26: state_type )).

tff(func_def_30,type,(
    constB27: state_type )).

tff(func_def_31,type,(
    constB28: state_type )).

tff(func_def_32,type,(
    constB29: state_type )).

tff(func_def_33,type,(
    constB30: state_type )).

tff(func_def_34,type,(
    constB31: state_type )).

tff(func_def_35,type,(
    constB32: state_type )).

tff(func_def_36,type,(
    constB33: state_type )).

tff(func_def_37,type,(
    constB34: state_type )).

tff(func_def_38,type,(
    constB35: state_type )).

tff(func_def_39,type,(
    constB36: state_type )).

tff(func_def_40,type,(
    constB37: state_type )).

tff(func_def_41,type,(
    constB38: state_type )).

tff(func_def_42,type,(
    constB39: state_type )).

tff(func_def_43,type,(
    constB40: state_type )).

tff(func_def_44,type,(
    constB41: state_type )).

tff(func_def_45,type,(
    constB42: state_type )).

tff(func_def_46,type,(
    constB43: state_type )).

tff(func_def_47,type,(
    constB44: state_type )).

tff(func_def_48,type,(
    constB45: state_type )).

tff(func_def_49,type,(
    constB46: state_type )).

tff(func_def_50,type,(
    constB47: state_type )).

tff(func_def_51,type,(
    constB48: state_type )).

tff(func_def_52,type,(
    constB49: state_type )).

tff(func_def_53,type,(
    constB50: state_type )).

tff(func_def_54,type,(
    constB51: state_type )).

tff(func_def_55,type,(
    constB52: state_type )).

tff(func_def_56,type,(
    constB53: state_type )).

tff(func_def_57,type,(
    constB54: state_type )).

tff(func_def_58,type,(
    constB55: state_type )).

tff(func_def_59,type,(
    constB56: state_type )).

tff(func_def_60,type,(
    constB57: state_type )).

tff(func_def_61,type,(
    constB58: state_type )).

tff(func_def_62,type,(
    constB59: state_type )).

tff(func_def_63,type,(
    constB60: state_type )).

tff(func_def_64,type,(
    constB61: state_type )).

tff(func_def_65,type,(
    constB62: state_type )).

tff(func_def_66,type,(
    constB63: state_type )).

tff(func_def_67,type,(
    constB64: state_type )).

tff(func_def_68,type,(
    constB65: state_type )).

tff(func_def_69,type,(
    constB66: state_type )).

tff(func_def_70,type,(
    constB67: state_type )).

tff(func_def_71,type,(
    constB68: state_type )).

tff(func_def_72,type,(
    constB69: state_type )).

tff(func_def_73,type,(
    constB70: state_type )).

tff(func_def_74,type,(
    constB71: state_type )).

tff(func_def_75,type,(
    constB72: state_type )).

tff(func_def_76,type,(
    constB73: state_type )).

tff(func_def_77,type,(
    constB74: state_type )).

tff(func_def_78,type,(
    constB75: state_type )).

tff(func_def_79,type,(
    constB76: state_type )).

tff(func_def_80,type,(
    constB77: state_type )).

tff(func_def_81,type,(
    constB78: state_type )).

tff(func_def_82,type,(
    constB79: state_type )).

tff(func_def_83,type,(
    constB80: state_type )).

tff(func_def_84,type,(
    constB81: state_type )).

tff(func_def_85,type,(
    constB82: state_type )).

tff(func_def_86,type,(
    constB83: state_type )).

tff(func_def_87,type,(
    constB84: state_type )).

tff(func_def_88,type,(
    constB85: state_type )).

tff(func_def_89,type,(
    constB86: state_type )).

tff(func_def_90,type,(
    constB87: state_type )).

tff(func_def_91,type,(
    constB88: state_type )).

tff(func_def_92,type,(
    constB89: state_type )).

tff(func_def_93,type,(
    constB90: state_type )).

tff(func_def_94,type,(
    constB91: state_type )).

tff(func_def_95,type,(
    constB92: state_type )).

tff(func_def_96,type,(
    constB93: state_type )).

tff(func_def_97,type,(
    constB94: state_type )).

tff(func_def_98,type,(
    constB95: state_type )).

tff(func_def_99,type,(
    constB96: state_type )).

tff(func_def_100,type,(
    constB97: state_type )).

tff(func_def_101,type,(
    constB98: state_type )).

tff(func_def_102,type,(
    constB99: state_type )).

tff(func_def_103,type,(
    constB100: state_type )).

tff(func_def_104,type,(
    constB101: state_type )).

tff(func_def_105,type,(
    constB102: state_type )).

tff(func_def_106,type,(
    constB103: state_type )).

tff(func_def_107,type,(
    constB104: state_type )).

tff(func_def_108,type,(
    constB105: state_type )).

tff(func_def_109,type,(
    constB106: state_type )).

tff(func_def_110,type,(
    constB107: state_type )).

tff(func_def_111,type,(
    constB108: state_type )).

tff(func_def_112,type,(
    constB109: state_type )).

tff(func_def_113,type,(
    constB110: state_type )).

tff(func_def_114,type,(
    constB111: state_type )).

tff(func_def_115,type,(
    constB112: state_type )).

tff(func_def_116,type,(
    constB113: state_type )).

tff(func_def_117,type,(
    constB114: state_type )).

tff(func_def_118,type,(
    constB115: state_type )).

tff(func_def_119,type,(
    constB116: state_type )).

tff(func_def_120,type,(
    constB117: state_type )).

tff(func_def_121,type,(
    constB118: state_type )).

tff(func_def_122,type,(
    constB119: state_type )).

tff(func_def_123,type,(
    constB120: state_type )).

tff(func_def_124,type,(
    constB121: state_type )).

tff(func_def_125,type,(
    constB122: state_type )).

tff(func_def_126,type,(
    constB123: state_type )).

tff(func_def_127,type,(
    constB124: state_type )).

tff(func_def_128,type,(
    constB125: state_type )).

tff(func_def_129,type,(
    constB126: state_type )).

tff(func_def_130,type,(
    constB127: state_type )).

tff(func_def_131,type,(
    constB128: state_type )).

tff(func_def_132,type,(
    constB129: state_type )).

tff(func_def_133,type,(
    constB130: state_type )).

tff(func_def_134,type,(
    constB131: state_type )).

tff(func_def_135,type,(
    constB132: state_type )).

tff(func_def_136,type,(
    constB133: state_type )).

tff(func_def_137,type,(
    constB134: state_type )).

tff(func_def_138,type,(
    constB135: state_type )).

tff(func_def_139,type,(
    constB136: state_type )).

tff(func_def_140,type,(
    constB137: state_type )).

tff(func_def_141,type,(
    constB138: state_type )).

tff(func_def_142,type,(
    constB139: state_type )).

tff(func_def_143,type,(
    constB140: state_type )).

tff(func_def_144,type,(
    constB141: state_type )).

tff(func_def_145,type,(
    constB142: state_type )).

tff(func_def_146,type,(
    constB143: state_type )).

tff(func_def_147,type,(
    constB144: state_type )).

tff(func_def_148,type,(
    constB145: state_type )).

tff(func_def_149,type,(
    constB146: state_type )).

tff(func_def_150,type,(
    constB147: state_type )).

tff(func_def_151,type,(
    constB148: state_type )).

tff(func_def_152,type,(
    constB149: state_type )).

tff(func_def_153,type,(
    constB150: state_type )).

tff(func_def_154,type,(
    constB151: state_type )).

tff(func_def_155,type,(
    constB152: state_type )).

tff(func_def_156,type,(
    constB153: state_type )).

tff(func_def_157,type,(
    constB154: state_type )).

tff(func_def_158,type,(
    constB155: state_type )).

tff(func_def_159,type,(
    constB156: state_type )).

tff(func_def_160,type,(
    constB157: state_type )).

tff(func_def_161,type,(
    constB158: state_type )).

tff(func_def_162,type,(
    constB159: state_type )).

tff(func_def_163,type,(
    constB160: state_type )).

tff(func_def_164,type,(
    constB161: state_type )).

tff(func_def_165,type,(
    constB162: state_type )).

tff(func_def_166,type,(
    constB163: state_type )).

tff(func_def_167,type,(
    constB164: state_type )).

tff(func_def_168,type,(
    constB165: state_type )).

tff(func_def_169,type,(
    constB166: state_type )).

tff(func_def_170,type,(
    constB167: state_type )).

tff(func_def_171,type,(
    constB168: state_type )).

tff(func_def_172,type,(
    constB169: state_type )).

tff(func_def_173,type,(
    constB170: state_type )).

tff(func_def_174,type,(
    constB171: state_type )).

tff(func_def_175,type,(
    constB172: state_type )).

tff(func_def_176,type,(
    constB173: state_type )).

tff(func_def_177,type,(
    constB174: state_type )).

tff(func_def_178,type,(
    constB175: state_type )).

tff(func_def_179,type,(
    constB176: state_type )).

tff(func_def_180,type,(
    constB177: state_type )).

tff(func_def_181,type,(
    constB178: state_type )).

tff(func_def_182,type,(
    constB179: state_type )).

tff(func_def_183,type,(
    constB180: state_type )).

tff(func_def_184,type,(
    constB181: state_type )).

tff(func_def_185,type,(
    constB182: state_type )).

tff(func_def_186,type,(
    constB183: state_type )).

tff(func_def_187,type,(
    constB184: state_type )).

tff(func_def_188,type,(
    constB185: state_type )).

tff(func_def_189,type,(
    constB186: state_type )).

tff(func_def_190,type,(
    constB187: state_type )).

tff(func_def_191,type,(
    constB188: state_type )).

tff(func_def_192,type,(
    constB189: state_type )).

tff(func_def_193,type,(
    constB190: state_type )).

tff(func_def_194,type,(
    constB191: state_type )).

tff(func_def_195,type,(
    constB192: state_type )).

tff(func_def_196,type,(
    constB193: state_type )).

tff(func_def_197,type,(
    constB194: state_type )).

tff(func_def_198,type,(
    constB195: state_type )).

tff(func_def_199,type,(
    constB196: state_type )).

tff(func_def_200,type,(
    constB197: state_type )).

tff(func_def_201,type,(
    constB198: state_type )).

tff(func_def_202,type,(
    constB199: state_type )).

tff(func_def_203,type,(
    constB200: state_type )).

tff(pred_def_1,type,(
    v9: state_type > $o )).

tff(pred_def_2,type,(
    v13: state_type > $o )).

tff(pred_def_3,type,(
    nextState: ( state_type * state_type ) > $o )).

tff(pred_def_4,type,(
    v6: state_type > $o )).

tff(pred_def_5,type,(
    v1: state_type > $o )).

tff(pred_def_6,type,(
    v20: state_type > $o )).

tff(pred_def_7,type,(
    v21: state_type > $o )).

tff(pred_def_8,type,(
    v19: state_type > $o )).

tff(pred_def_9,type,(
    v22: state_type > $o )).

tff(pred_def_10,type,(
    v18: state_type > $o )).

tff(pred_def_11,type,(
    v4: state_type > $o )).

tff(pred_def_12,type,(
    v26: state_type > $o )).

tff(pred_def_13,type,(
    v31: state_type > $o )).

tff(pred_def_14,type,(
    v39: state_type > $o )).

tff(pred_def_15,type,(
    v38: state_type > $o )).

tff(pred_def_16,type,(
    v37: state_type > $o )).

tff(pred_def_17,type,(
    v34: state_type > $o )).

tff(pred_def_18,type,(
    v36: state_type > $o )).

tff(pred_def_19,type,(
    v29: state_type > $o )).

tff(pred_def_20,type,(
    v47: state_type > $o )).

tff(pred_def_21,type,(
    v49: state_type > $o )).

tff(pred_def_22,type,(
    v48: state_type > $o )).

tff(pred_def_23,type,(
    v44: state_type > $o )).

tff(pred_def_24,type,(
    v46: state_type > $o )).

tff(pred_def_25,type,(
    v56: state_type > $o )).

tff(pred_def_26,type,(
    v55: state_type > $o )).

tff(pred_def_27,type,(
    v54: state_type > $o )).

tff(pred_def_28,type,(
    v53: state_type > $o )).

tff(pred_def_29,type,(
    v24: state_type > $o )).

tff(pred_def_30,type,(
    v64: state_type > $o )).

tff(pred_def_31,type,(
    v63: state_type > $o )).

tff(pred_def_32,type,(
    v66: state_type > $o )).

tff(pred_def_33,type,(
    v60: state_type > $o )).

tff(pred_def_34,type,(
    v74: state_type > $o )).

tff(pred_def_35,type,(
    v73: state_type > $o )).

tff(pred_def_36,type,(
    v75: state_type > $o )).

tff(pred_def_37,type,(
    v72: state_type > $o )).

tff(pred_def_38,type,(
    v79: state_type > $o )).

tff(pred_def_39,type,(
    v78: state_type > $o )).

tff(pred_def_40,type,(
    v80: state_type > $o )).

tff(pred_def_41,type,(
    v76: state_type > $o )).

tff(pred_def_42,type,(
    v71: state_type > $o )).

tff(pred_def_43,type,(
    v58: state_type > $o )).

tff(pred_def_44,type,(
    v90: ( state_type * $int ) > $o )).

tff(pred_def_45,type,(
    v104: state_type > $o )).

tff(pred_def_46,type,(
    v102: state_type > $o )).

tff(pred_def_47,type,(
    v101: state_type > $o )).

tff(pred_def_48,type,(
    v111: state_type > $o )).

tff(pred_def_49,type,(
    v119: state_type > $o )).

tff(pred_def_50,type,(
    v120: state_type > $o )).

tff(pred_def_51,type,(
    v118: state_type > $o )).

tff(pred_def_52,type,(
    v121: state_type > $o )).

tff(pred_def_53,type,(
    v117: state_type > $o )).

tff(pred_def_54,type,(
    v122: state_type > $o )).

tff(pred_def_55,type,(
    v116: state_type > $o )).

tff(pred_def_56,type,(
    v123: state_type > $o )).

tff(pred_def_57,type,(
    v115: state_type > $o )).

tff(pred_def_58,type,(
    v124: state_type > $o )).

tff(pred_def_59,type,(
    v114: state_type > $o )).

tff(pred_def_60,type,(
    v125: state_type > $o )).

tff(pred_def_61,type,(
    v113: state_type > $o )).

tff(pred_def_62,type,(
    v126: state_type > $o )).

tff(pred_def_63,type,(
    v112: state_type > $o )).

tff(pred_def_64,type,(
    v108: state_type > $o )).

tff(pred_def_65,type,(
    v110: state_type > $o )).

tff(pred_def_66,type,(
    v100: state_type > $o )).

tff(pred_def_67,type,(
    v130: ( state_type * $int ) > $o )).

tff(pred_def_68,type,(
    v127: ( state_type * $int ) > $o )).

tff(pred_def_69,type,(
    v129: ( state_type * $int ) > $o )).

tff(pred_def_70,type,(
    undeclared: $o )).

tff(pred_def_71,type,(
    v88: ( state_type * $int ) > $o )).

tff(pred_def_72,type,(
    v86: state_type > $o )).

tff(pred_def_73,type,(
    v139: state_type > $o )).

tff(pred_def_74,type,(
    v140: state_type > $o )).

tff(pred_def_75,type,(
    v138: state_type > $o )).

tff(pred_def_76,type,(
    v141: state_type > $o )).

tff(pred_def_77,type,(
    v137: state_type > $o )).

tff(pred_def_78,type,(
    v143: state_type > $o )).

tff(pred_def_79,type,(
    v144: state_type > $o )).

tff(pred_def_80,type,(
    v142: state_type > $o )).

tff(pred_def_81,type,(
    v136: state_type > $o )).

tff(pred_def_82,type,(
    v146: state_type > $o )).

tff(pred_def_83,type,(
    v147: state_type > $o )).

tff(pred_def_84,type,(
    v145: state_type > $o )).

tff(pred_def_85,type,(
    v135: state_type > $o )).

tff(pred_def_86,type,(
    v151: state_type > $o )).

tff(pred_def_87,type,(
    v149: state_type > $o )).

tff(pred_def_88,type,(
    v148: state_type > $o )).

tff(pred_def_89,type,(
    v84: state_type > $o )).

tff(pred_def_90,type,(
    v161: state_type > $o )).

tff(pred_def_91,type,(
    v167: state_type > $o )).

tff(pred_def_92,type,(
    v166: state_type > $o )).

tff(pred_def_93,type,(
    v165: state_type > $o )).

tff(pred_def_94,type,(
    v164: state_type > $o )).

tff(pred_def_95,type,(
    v171: state_type > $o )).

tff(pred_def_96,type,(
    v169: state_type > $o )).

tff(pred_def_97,type,(
    v168: state_type > $o )).

tff(pred_def_98,type,(
    v159: state_type > $o )).

tff(pred_def_99,type,(
    v157: state_type > $o )).

tff(pred_def_100,type,(
    v184: state_type > $o )).

tff(pred_def_101,type,(
    v183: state_type > $o )).

tff(pred_def_102,type,(
    v182: state_type > $o )).

tff(pred_def_103,type,(
    v181: state_type > $o )).

tff(pred_def_104,type,(
    v186: state_type > $o )).

tff(pred_def_105,type,(
    v187: state_type > $o )).

tff(pred_def_106,type,(
    v185: state_type > $o )).

tff(pred_def_107,type,(
    v178: state_type > $o )).

tff(pred_def_108,type,(
    v180: state_type > $o )).

tff(pred_def_109,type,(
    v193: state_type > $o )).

tff(pred_def_110,type,(
    v199: state_type > $o )).

tff(pred_def_111,type,(
    v198: state_type > $o )).

tff(pred_def_112,type,(
    v197: state_type > $o )).

tff(pred_def_113,type,(
    v196: state_type > $o )).

tff(pred_def_114,type,(
    v201: state_type > $o )).

tff(pred_def_115,type,(
    v202: state_type > $o )).

tff(pred_def_116,type,(
    v200: state_type > $o )).

tff(pred_def_117,type,(
    v191: state_type > $o )).

tff(pred_def_118,type,(
    v208: state_type > $o )).

tff(pred_def_119,type,(
    v214: state_type > $o )).

tff(pred_def_120,type,(
    v217: state_type > $o )).

tff(pred_def_121,type,(
    v216: state_type > $o )).

tff(pred_def_122,type,(
    v215: state_type > $o )).

tff(pred_def_123,type,(
    v211: state_type > $o )).

tff(pred_def_124,type,(
    v213: state_type > $o )).

tff(pred_def_125,type,(
    v223: state_type > $o )).

tff(pred_def_126,type,(
    v229: state_type > $o )).

tff(pred_def_127,type,(
    v228: state_type > $o )).

tff(pred_def_128,type,(
    v227: state_type > $o )).

tff(pred_def_129,type,(
    v226: state_type > $o )).

tff(pred_def_130,type,(
    v150: state_type > $o )).

tff(pred_def_131,type,(
    v231: state_type > $o )).

tff(pred_def_132,type,(
    v230: state_type > $o )).

tff(pred_def_133,type,(
    v221: state_type > $o )).

tff(pred_def_134,type,(
    v238: state_type > $o )).

tff(pred_def_135,type,(
    v244: state_type > $o )).

tff(pred_def_136,type,(
    v247: state_type > $o )).

tff(pred_def_137,type,(
    v246: state_type > $o )).

tff(pred_def_138,type,(
    v245: state_type > $o )).

tff(pred_def_139,type,(
    v241: state_type > $o )).

tff(pred_def_140,type,(
    v243: state_type > $o )).

tff(pred_def_141,type,(
    v253: state_type > $o )).

tff(pred_def_142,type,(
    v259: state_type > $o )).

tff(pred_def_143,type,(
    v262: state_type > $o )).

tff(pred_def_144,type,(
    v261: state_type > $o )).

tff(pred_def_145,type,(
    v260: state_type > $o )).

tff(pred_def_146,type,(
    v256: state_type > $o )).

tff(pred_def_147,type,(
    v258: state_type > $o )).

tff(pred_def_148,type,(
    v251: state_type > $o )).

tff(pred_def_149,type,(
    v270: state_type > $o )).

tff(pred_def_150,type,(
    v271: state_type > $o )).

tff(pred_def_151,type,(
    v267: state_type > $o )).

tff(pred_def_152,type,(
    v269: state_type > $o )).

tff(pred_def_153,type,(
    v275: state_type > $o )).

tff(pred_def_154,type,(
    v281: state_type > $o )).

tff(pred_def_155,type,(
    v282: state_type > $o )).

tff(pred_def_156,type,(
    v278: state_type > $o )).

tff(pred_def_157,type,(
    v280: state_type > $o )).

tff(pred_def_158,type,(
    v296: state_type > $o )).

tff(pred_def_159,type,(
    v295: state_type > $o )).

tff(pred_def_160,type,(
    v294: state_type > $o )).

tff(pred_def_161,type,(
    v293: state_type > $o )).

tff(pred_def_162,type,(
    v300: state_type > $o )).

tff(pred_def_163,type,(
    v299: state_type > $o )).

tff(pred_def_164,type,(
    v298: state_type > $o )).

tff(pred_def_165,type,(
    v297: state_type > $o )).

tff(pred_def_166,type,(
    v292: state_type > $o )).

tff(pred_def_167,type,(
    v291: state_type > $o )).

tff(pred_def_168,type,(
    v305: state_type > $o )).

tff(pred_def_169,type,(
    v304: state_type > $o )).

tff(pred_def_170,type,(
    v303: state_type > $o )).

tff(pred_def_171,type,(
    v302: state_type > $o )).

tff(pred_def_172,type,(
    v301: state_type > $o )).

tff(pred_def_173,type,(
    v290: state_type > $o )).

tff(pred_def_174,type,(
    v307: state_type > $o )).

tff(pred_def_175,type,(
    v306: state_type > $o )).

tff(pred_def_176,type,(
    v289: state_type > $o )).

tff(pred_def_177,type,(
    v312: state_type > $o )).

tff(pred_def_178,type,(
    v311: state_type > $o )).

tff(pred_def_179,type,(
    v310: state_type > $o )).

tff(pred_def_180,type,(
    v309: state_type > $o )).

tff(pred_def_181,type,(
    v308: state_type > $o )).

tff(pred_def_182,type,(
    v288: state_type > $o )).

tff(pred_def_183,type,(
    v314: state_type > $o )).

tff(pred_def_184,type,(
    v313: state_type > $o )).

tff(pred_def_185,type,(
    v287: state_type > $o )).

tff(pred_def_186,type,(
    v286: state_type > $o )).

tff(pred_def_187,type,(
    v82: state_type > $o )).

tff(pred_def_188,type,(
    reachableState: state_type > $o )).

tff(clock_pattern_200,axiom,(
    v1(constB200) )).

tff(clock_pattern_199,axiom,(
    ~ v1(constB199) )).

tff(clock_pattern_198,axiom,(
    v1(constB198) )).

tff(clock_pattern_197,axiom,(
    ~ v1(constB197) )).

tff(clock_pattern_196,axiom,(
    v1(constB196) )).

tff(clock_pattern_195,axiom,(
    ~ v1(constB195) )).

tff(clock_pattern_194,axiom,(
    v1(constB194) )).

tff(clock_pattern_193,axiom,(
    ~ v1(constB193) )).

tff(clock_pattern_192,axiom,(
    v1(constB192) )).

tff(clock_pattern_191,axiom,(
    ~ v1(constB191) )).

tff(clock_pattern_190,axiom,(
    v1(constB190) )).

tff(clock_pattern_189,axiom,(
    ~ v1(constB189) )).

tff(clock_pattern_188,axiom,(
    v1(constB188) )).

tff(clock_pattern_187,axiom,(
    ~ v1(constB187) )).

tff(clock_pattern_186,axiom,(
    v1(constB186) )).

tff(clock_pattern_185,axiom,(
    ~ v1(constB185) )).

tff(clock_pattern_184,axiom,(
    v1(constB184) )).

tff(clock_pattern_183,axiom,(
    ~ v1(constB183) )).

tff(clock_pattern_182,axiom,(
    v1(constB182) )).

tff(clock_pattern_181,axiom,(
    ~ v1(constB181) )).

tff(clock_pattern_180,axiom,(
    v1(constB180) )).

tff(clock_pattern_179,axiom,(
    ~ v1(constB179) )).

tff(clock_pattern_178,axiom,(
    v1(constB178) )).

tff(clock_pattern_177,axiom,(
    ~ v1(constB177) )).

tff(clock_pattern_176,axiom,(
    v1(constB176) )).

tff(clock_pattern_175,axiom,(
    ~ v1(constB175) )).

tff(clock_pattern_174,axiom,(
    v1(constB174) )).

tff(clock_pattern_173,axiom,(
    ~ v1(constB173) )).

tff(clock_pattern_172,axiom,(
    v1(constB172) )).

tff(clock_pattern_171,axiom,(
    ~ v1(constB171) )).

tff(clock_pattern_170,axiom,(
    v1(constB170) )).

tff(clock_pattern_169,axiom,(
    ~ v1(constB169) )).

tff(clock_pattern_168,axiom,(
    v1(constB168) )).

tff(clock_pattern_167,axiom,(
    ~ v1(constB167) )).

tff(clock_pattern_166,axiom,(
    v1(constB166) )).

tff(clock_pattern_165,axiom,(
    ~ v1(constB165) )).

tff(clock_pattern_164,axiom,(
    v1(constB164) )).

tff(clock_pattern_163,axiom,(
    ~ v1(constB163) )).

tff(clock_pattern_162,axiom,(
    v1(constB162) )).

tff(clock_pattern_161,axiom,(
    ~ v1(constB161) )).

tff(clock_pattern_160,axiom,(
    v1(constB160) )).

tff(clock_pattern_159,axiom,(
    ~ v1(constB159) )).

tff(clock_pattern_158,axiom,(
    v1(constB158) )).

tff(clock_pattern_157,axiom,(
    ~ v1(constB157) )).

tff(clock_pattern_156,axiom,(
    v1(constB156) )).

tff(clock_pattern_155,axiom,(
    ~ v1(constB155) )).

tff(clock_pattern_154,axiom,(
    v1(constB154) )).

tff(clock_pattern_153,axiom,(
    ~ v1(constB153) )).

tff(clock_pattern_152,axiom,(
    v1(constB152) )).

tff(clock_pattern_151,axiom,(
    ~ v1(constB151) )).

tff(clock_pattern_150,axiom,(
    v1(constB150) )).

tff(clock_pattern_149,axiom,(
    ~ v1(constB149) )).

tff(clock_pattern_148,axiom,(
    v1(constB148) )).

tff(clock_pattern_147,axiom,(
    ~ v1(constB147) )).

tff(clock_pattern_146,axiom,(
    v1(constB146) )).

tff(clock_pattern_145,axiom,(
    ~ v1(constB145) )).

tff(clock_pattern_144,axiom,(
    v1(constB144) )).

tff(clock_pattern_143,axiom,(
    ~ v1(constB143) )).

tff(clock_pattern_142,axiom,(
    v1(constB142) )).

tff(clock_pattern_141,axiom,(
    ~ v1(constB141) )).

tff(clock_pattern_140,axiom,(
    v1(constB140) )).

tff(clock_pattern_139,axiom,(
    ~ v1(constB139) )).

tff(clock_pattern_138,axiom,(
    v1(constB138) )).

tff(clock_pattern_137,axiom,(
    ~ v1(constB137) )).

tff(clock_pattern_136,axiom,(
    v1(constB136) )).

tff(clock_pattern_135,axiom,(
    ~ v1(constB135) )).

tff(clock_pattern_134,axiom,(
    v1(constB134) )).

tff(clock_pattern_133,axiom,(
    ~ v1(constB133) )).

tff(clock_pattern_132,axiom,(
    v1(constB132) )).

tff(clock_pattern_131,axiom,(
    ~ v1(constB131) )).

tff(clock_pattern_130,axiom,(
    v1(constB130) )).

tff(clock_pattern_129,axiom,(
    ~ v1(constB129) )).

tff(clock_pattern_128,axiom,(
    v1(constB128) )).

tff(clock_pattern_127,axiom,(
    ~ v1(constB127) )).

tff(clock_pattern_126,axiom,(
    v1(constB126) )).

tff(clock_pattern_125,axiom,(
    ~ v1(constB125) )).

tff(clock_pattern_124,axiom,(
    v1(constB124) )).

tff(clock_pattern_123,axiom,(
    ~ v1(constB123) )).

tff(clock_pattern_122,axiom,(
    v1(constB122) )).

tff(clock_pattern_121,axiom,(
    ~ v1(constB121) )).

tff(clock_pattern_120,axiom,(
    v1(constB120) )).

tff(clock_pattern_119,axiom,(
    ~ v1(constB119) )).

tff(clock_pattern_118,axiom,(
    v1(constB118) )).

tff(clock_pattern_117,axiom,(
    ~ v1(constB117) )).

tff(clock_pattern_116,axiom,(
    v1(constB116) )).

tff(clock_pattern_115,axiom,(
    ~ v1(constB115) )).

tff(clock_pattern_114,axiom,(
    v1(constB114) )).

tff(clock_pattern_113,axiom,(
    ~ v1(constB113) )).

tff(clock_pattern_112,axiom,(
    v1(constB112) )).

tff(clock_pattern_111,axiom,(
    ~ v1(constB111) )).

tff(clock_pattern_110,axiom,(
    v1(constB110) )).

tff(clock_pattern_109,axiom,(
    ~ v1(constB109) )).

tff(clock_pattern_108,axiom,(
    v1(constB108) )).

tff(clock_pattern_107,axiom,(
    ~ v1(constB107) )).

tff(clock_pattern_106,axiom,(
    v1(constB106) )).

tff(clock_pattern_105,axiom,(
    ~ v1(constB105) )).

tff(clock_pattern_104,axiom,(
    v1(constB104) )).

tff(clock_pattern_103,axiom,(
    ~ v1(constB103) )).

tff(clock_pattern_102,axiom,(
    v1(constB102) )).

tff(clock_pattern_101,axiom,(
    ~ v1(constB101) )).

tff(clock_pattern_100,axiom,(
    v1(constB100) )).

tff(clock_pattern_99,axiom,(
    ~ v1(constB99) )).

tff(clock_pattern_98,axiom,(
    v1(constB98) )).

tff(clock_pattern_97,axiom,(
    ~ v1(constB97) )).

tff(clock_pattern_96,axiom,(
    v1(constB96) )).

tff(clock_pattern_95,axiom,(
    ~ v1(constB95) )).

tff(clock_pattern_94,axiom,(
    v1(constB94) )).

tff(clock_pattern_93,axiom,(
    ~ v1(constB93) )).

tff(clock_pattern_92,axiom,(
    v1(constB92) )).

tff(clock_pattern_91,axiom,(
    ~ v1(constB91) )).

tff(clock_pattern_90,axiom,(
    v1(constB90) )).

tff(clock_pattern_89,axiom,(
    ~ v1(constB89) )).

tff(clock_pattern_88,axiom,(
    v1(constB88) )).

tff(clock_pattern_87,axiom,(
    ~ v1(constB87) )).

tff(clock_pattern_86,axiom,(
    v1(constB86) )).

tff(clock_pattern_85,axiom,(
    ~ v1(constB85) )).

tff(clock_pattern_84,axiom,(
    v1(constB84) )).

tff(clock_pattern_83,axiom,(
    ~ v1(constB83) )).

tff(clock_pattern_82,axiom,(
    v1(constB82) )).

tff(clock_pattern_81,axiom,(
    ~ v1(constB81) )).

tff(clock_pattern_80,axiom,(
    v1(constB80) )).

tff(clock_pattern_79,axiom,(
    ~ v1(constB79) )).

tff(clock_pattern_78,axiom,(
    v1(constB78) )).

tff(clock_pattern_77,axiom,(
    ~ v1(constB77) )).

tff(clock_pattern_76,axiom,(
    v1(constB76) )).

tff(clock_pattern_75,axiom,(
    ~ v1(constB75) )).

tff(clock_pattern_74,axiom,(
    v1(constB74) )).

tff(clock_pattern_73,axiom,(
    ~ v1(constB73) )).

tff(clock_pattern_72,axiom,(
    v1(constB72) )).

tff(clock_pattern_71,axiom,(
    ~ v1(constB71) )).

tff(clock_pattern_70,axiom,(
    v1(constB70) )).

tff(clock_pattern_69,axiom,(
    ~ v1(constB69) )).

tff(clock_pattern_68,axiom,(
    v1(constB68) )).

tff(clock_pattern_67,axiom,(
    ~ v1(constB67) )).

tff(clock_pattern_66,axiom,(
    v1(constB66) )).

tff(clock_pattern_65,axiom,(
    ~ v1(constB65) )).

tff(clock_pattern_64,axiom,(
    v1(constB64) )).

tff(clock_pattern_63,axiom,(
    ~ v1(constB63) )).

tff(clock_pattern_62,axiom,(
    v1(constB62) )).

tff(clock_pattern_61,axiom,(
    ~ v1(constB61) )).

tff(clock_pattern_60,axiom,(
    v1(constB60) )).

tff(clock_pattern_59,axiom,(
    ~ v1(constB59) )).

tff(clock_pattern_58,axiom,(
    v1(constB58) )).

tff(clock_pattern_57,axiom,(
    ~ v1(constB57) )).

tff(clock_pattern_56,axiom,(
    v1(constB56) )).

tff(clock_pattern_55,axiom,(
    ~ v1(constB55) )).

tff(clock_pattern_54,axiom,(
    v1(constB54) )).

tff(clock_pattern_53,axiom,(
    ~ v1(constB53) )).

tff(clock_pattern_52,axiom,(
    v1(constB52) )).

tff(clock_pattern_51,axiom,(
    ~ v1(constB51) )).

tff(clock_pattern_50,axiom,(
    v1(constB50) )).

tff(clock_pattern_49,axiom,(
    ~ v1(constB49) )).

tff(clock_pattern_48,axiom,(
    v1(constB48) )).

tff(clock_pattern_47,axiom,(
    ~ v1(constB47) )).

tff(clock_pattern_46,axiom,(
    v1(constB46) )).

tff(clock_pattern_45,axiom,(
    ~ v1(constB45) )).

tff(clock_pattern_44,axiom,(
    v1(constB44) )).

tff(clock_pattern_43,axiom,(
    ~ v1(constB43) )).

tff(clock_pattern_42,axiom,(
    v1(constB42) )).

tff(clock_pattern_41,axiom,(
    ~ v1(constB41) )).

tff(clock_pattern_40,axiom,(
    v1(constB40) )).

tff(clock_pattern_39,axiom,(
    ~ v1(constB39) )).

tff(clock_pattern_38,axiom,(
    v1(constB38) )).

tff(clock_pattern_37,axiom,(
    ~ v1(constB37) )).

tff(clock_pattern_36,axiom,(
    v1(constB36) )).

tff(clock_pattern_35,axiom,(
    ~ v1(constB35) )).

tff(clock_pattern_34,axiom,(
    v1(constB34) )).

tff(clock_pattern_33,axiom,(
    ~ v1(constB33) )).

tff(clock_pattern_32,axiom,(
    v1(constB32) )).

tff(clock_pattern_31,axiom,(
    ~ v1(constB31) )).

tff(clock_pattern_30,axiom,(
    v1(constB30) )).

tff(clock_pattern_29,axiom,(
    ~ v1(constB29) )).

tff(clock_pattern_28,axiom,(
    v1(constB28) )).

tff(clock_pattern_27,axiom,(
    ~ v1(constB27) )).

tff(clock_pattern_26,axiom,(
    v1(constB26) )).

tff(clock_pattern_25,axiom,(
    ~ v1(constB25) )).

tff(clock_pattern_24,axiom,(
    v1(constB24) )).

tff(clock_pattern_23,axiom,(
    ~ v1(constB23) )).

tff(clock_pattern_22,axiom,(
    v1(constB22) )).

tff(clock_pattern_21,axiom,(
    ~ v1(constB21) )).

tff(clock_pattern_20,axiom,(
    v1(constB20) )).

tff(clock_pattern_19,axiom,(
    ~ v1(constB19) )).

tff(clock_pattern_18,axiom,(
    v1(constB18) )).

tff(clock_pattern_17,axiom,(
    ~ v1(constB17) )).

tff(clock_pattern_16,axiom,(
    v1(constB16) )).

tff(clock_pattern_15,axiom,(
    ~ v1(constB15) )).

tff(clock_pattern_14,axiom,(
    v1(constB14) )).

tff(clock_pattern_13,axiom,(
    ~ v1(constB13) )).

tff(clock_pattern_12,axiom,(
    v1(constB12) )).

tff(clock_pattern_11,axiom,(
    ~ v1(constB11) )).

tff(clock_pattern_10,axiom,(
    v1(constB10) )).

tff(clock_pattern_9,axiom,(
    ~ v1(constB9) )).

tff(clock_pattern_8,axiom,(
    v1(constB8) )).

tff(clock_pattern_7,axiom,(
    ~ v1(constB7) )).

tff(clock_pattern_6,axiom,(
    v1(constB6) )).

tff(clock_pattern_5,axiom,(
    ~ v1(constB5) )).

tff(clock_pattern_4,axiom,(
    v1(constB4) )).

tff(clock_pattern_3,axiom,(
    ~ v1(constB3) )).

tff(clock_pattern_2,axiom,(
    v1(constB2) )).

tff(clock_pattern_1,axiom,(
    ~ v1(constB1) )).

tff(clock_pattern,axiom,(
    v1(constB0) )).

tff(pathAxiom_199,axiom,(
    nextState(constB199,constB200) )).

tff(pathAxiom_198,axiom,(
    nextState(constB198,constB199) )).

tff(pathAxiom_197,axiom,(
    nextState(constB197,constB198) )).

tff(pathAxiom_196,axiom,(
    nextState(constB196,constB197) )).

tff(pathAxiom_195,axiom,(
    nextState(constB195,constB196) )).

tff(pathAxiom_194,axiom,(
    nextState(constB194,constB195) )).

tff(pathAxiom_193,axiom,(
    nextState(constB193,constB194) )).

tff(pathAxiom_192,axiom,(
    nextState(constB192,constB193) )).

tff(pathAxiom_191,axiom,(
    nextState(constB191,constB192) )).

tff(pathAxiom_190,axiom,(
    nextState(constB190,constB191) )).

tff(pathAxiom_189,axiom,(
    nextState(constB189,constB190) )).

tff(pathAxiom_188,axiom,(
    nextState(constB188,constB189) )).

tff(pathAxiom_187,axiom,(
    nextState(constB187,constB188) )).

tff(pathAxiom_186,axiom,(
    nextState(constB186,constB187) )).

tff(pathAxiom_185,axiom,(
    nextState(constB185,constB186) )).

tff(pathAxiom_184,axiom,(
    nextState(constB184,constB185) )).

tff(pathAxiom_183,axiom,(
    nextState(constB183,constB184) )).

tff(pathAxiom_182,axiom,(
    nextState(constB182,constB183) )).

tff(pathAxiom_181,axiom,(
    nextState(constB181,constB182) )).

tff(pathAxiom_180,axiom,(
    nextState(constB180,constB181) )).

tff(pathAxiom_179,axiom,(
    nextState(constB179,constB180) )).

tff(pathAxiom_178,axiom,(
    nextState(constB178,constB179) )).

tff(pathAxiom_177,axiom,(
    nextState(constB177,constB178) )).

tff(pathAxiom_176,axiom,(
    nextState(constB176,constB177) )).

tff(pathAxiom_175,axiom,(
    nextState(constB175,constB176) )).

tff(pathAxiom_174,axiom,(
    nextState(constB174,constB175) )).

tff(pathAxiom_173,axiom,(
    nextState(constB173,constB174) )).

tff(pathAxiom_172,axiom,(
    nextState(constB172,constB173) )).

tff(pathAxiom_171,axiom,(
    nextState(constB171,constB172) )).

tff(pathAxiom_170,axiom,(
    nextState(constB170,constB171) )).

tff(pathAxiom_169,axiom,(
    nextState(constB169,constB170) )).

tff(pathAxiom_168,axiom,(
    nextState(constB168,constB169) )).

tff(pathAxiom_167,axiom,(
    nextState(constB167,constB168) )).

tff(pathAxiom_166,axiom,(
    nextState(constB166,constB167) )).

tff(pathAxiom_165,axiom,(
    nextState(constB165,constB166) )).

tff(pathAxiom_164,axiom,(
    nextState(constB164,constB165) )).

tff(pathAxiom_163,axiom,(
    nextState(constB163,constB164) )).

tff(pathAxiom_162,axiom,(
    nextState(constB162,constB163) )).

tff(pathAxiom_161,axiom,(
    nextState(constB161,constB162) )).

tff(pathAxiom_160,axiom,(
    nextState(constB160,constB161) )).

tff(pathAxiom_159,axiom,(
    nextState(constB159,constB160) )).

tff(pathAxiom_158,axiom,(
    nextState(constB158,constB159) )).

tff(pathAxiom_157,axiom,(
    nextState(constB157,constB158) )).

tff(pathAxiom_156,axiom,(
    nextState(constB156,constB157) )).

tff(pathAxiom_155,axiom,(
    nextState(constB155,constB156) )).

tff(pathAxiom_154,axiom,(
    nextState(constB154,constB155) )).

tff(pathAxiom_153,axiom,(
    nextState(constB153,constB154) )).

tff(pathAxiom_152,axiom,(
    nextState(constB152,constB153) )).

tff(pathAxiom_151,axiom,(
    nextState(constB151,constB152) )).

tff(pathAxiom_150,axiom,(
    nextState(constB150,constB151) )).

tff(pathAxiom_149,axiom,(
    nextState(constB149,constB150) )).

tff(pathAxiom_148,axiom,(
    nextState(constB148,constB149) )).

tff(pathAxiom_147,axiom,(
    nextState(constB147,constB148) )).

tff(pathAxiom_146,axiom,(
    nextState(constB146,constB147) )).

tff(pathAxiom_145,axiom,(
    nextState(constB145,constB146) )).

tff(pathAxiom_144,axiom,(
    nextState(constB144,constB145) )).

tff(pathAxiom_143,axiom,(
    nextState(constB143,constB144) )).

tff(pathAxiom_142,axiom,(
    nextState(constB142,constB143) )).

tff(pathAxiom_141,axiom,(
    nextState(constB141,constB142) )).

tff(pathAxiom_140,axiom,(
    nextState(constB140,constB141) )).

tff(pathAxiom_139,axiom,(
    nextState(constB139,constB140) )).

tff(pathAxiom_138,axiom,(
    nextState(constB138,constB139) )).

tff(pathAxiom_137,axiom,(
    nextState(constB137,constB138) )).

tff(pathAxiom_136,axiom,(
    nextState(constB136,constB137) )).

tff(pathAxiom_135,axiom,(
    nextState(constB135,constB136) )).

tff(pathAxiom_134,axiom,(
    nextState(constB134,constB135) )).

tff(pathAxiom_133,axiom,(
    nextState(constB133,constB134) )).

tff(pathAxiom_132,axiom,(
    nextState(constB132,constB133) )).

tff(pathAxiom_131,axiom,(
    nextState(constB131,constB132) )).

tff(pathAxiom_130,axiom,(
    nextState(constB130,constB131) )).

tff(pathAxiom_129,axiom,(
    nextState(constB129,constB130) )).

tff(pathAxiom_128,axiom,(
    nextState(constB128,constB129) )).

tff(pathAxiom_127,axiom,(
    nextState(constB127,constB128) )).

tff(pathAxiom_126,axiom,(
    nextState(constB126,constB127) )).

tff(pathAxiom_125,axiom,(
    nextState(constB125,constB126) )).

tff(pathAxiom_124,axiom,(
    nextState(constB124,constB125) )).

tff(pathAxiom_123,axiom,(
    nextState(constB123,constB124) )).

tff(pathAxiom_122,axiom,(
    nextState(constB122,constB123) )).

tff(pathAxiom_121,axiom,(
    nextState(constB121,constB122) )).

tff(pathAxiom_120,axiom,(
    nextState(constB120,constB121) )).

tff(pathAxiom_119,axiom,(
    nextState(constB119,constB120) )).

tff(pathAxiom_118,axiom,(
    nextState(constB118,constB119) )).

tff(pathAxiom_117,axiom,(
    nextState(constB117,constB118) )).

tff(pathAxiom_116,axiom,(
    nextState(constB116,constB117) )).

tff(pathAxiom_115,axiom,(
    nextState(constB115,constB116) )).

tff(pathAxiom_114,axiom,(
    nextState(constB114,constB115) )).

tff(pathAxiom_113,axiom,(
    nextState(constB113,constB114) )).

tff(pathAxiom_112,axiom,(
    nextState(constB112,constB113) )).

tff(pathAxiom_111,axiom,(
    nextState(constB111,constB112) )).

tff(pathAxiom_110,axiom,(
    nextState(constB110,constB111) )).

tff(pathAxiom_109,axiom,(
    nextState(constB109,constB110) )).

tff(pathAxiom_108,axiom,(
    nextState(constB108,constB109) )).

tff(pathAxiom_107,axiom,(
    nextState(constB107,constB108) )).

tff(pathAxiom_106,axiom,(
    nextState(constB106,constB107) )).

tff(pathAxiom_105,axiom,(
    nextState(constB105,constB106) )).

tff(pathAxiom_104,axiom,(
    nextState(constB104,constB105) )).

tff(pathAxiom_103,axiom,(
    nextState(constB103,constB104) )).

tff(pathAxiom_102,axiom,(
    nextState(constB102,constB103) )).

tff(pathAxiom_101,axiom,(
    nextState(constB101,constB102) )).

tff(pathAxiom_100,axiom,(
    nextState(constB100,constB101) )).

tff(pathAxiom_99,axiom,(
    nextState(constB99,constB100) )).

tff(pathAxiom_98,axiom,(
    nextState(constB98,constB99) )).

tff(pathAxiom_97,axiom,(
    nextState(constB97,constB98) )).

tff(pathAxiom_96,axiom,(
    nextState(constB96,constB97) )).

tff(pathAxiom_95,axiom,(
    nextState(constB95,constB96) )).

tff(pathAxiom_94,axiom,(
    nextState(constB94,constB95) )).

tff(pathAxiom_93,axiom,(
    nextState(constB93,constB94) )).

tff(pathAxiom_92,axiom,(
    nextState(constB92,constB93) )).

tff(pathAxiom_91,axiom,(
    nextState(constB91,constB92) )).

tff(pathAxiom_90,axiom,(
    nextState(constB90,constB91) )).

tff(pathAxiom_89,axiom,(
    nextState(constB89,constB90) )).

tff(pathAxiom_88,axiom,(
    nextState(constB88,constB89) )).

tff(pathAxiom_87,axiom,(
    nextState(constB87,constB88) )).

tff(pathAxiom_86,axiom,(
    nextState(constB86,constB87) )).

tff(pathAxiom_85,axiom,(
    nextState(constB85,constB86) )).

tff(pathAxiom_84,axiom,(
    nextState(constB84,constB85) )).

tff(pathAxiom_83,axiom,(
    nextState(constB83,constB84) )).

tff(pathAxiom_82,axiom,(
    nextState(constB82,constB83) )).

tff(pathAxiom_81,axiom,(
    nextState(constB81,constB82) )).

tff(pathAxiom_80,axiom,(
    nextState(constB80,constB81) )).

tff(pathAxiom_79,axiom,(
    nextState(constB79,constB80) )).

tff(pathAxiom_78,axiom,(
    nextState(constB78,constB79) )).

tff(pathAxiom_77,axiom,(
    nextState(constB77,constB78) )).

tff(pathAxiom_76,axiom,(
    nextState(constB76,constB77) )).

tff(pathAxiom_75,axiom,(
    nextState(constB75,constB76) )).

tff(pathAxiom_74,axiom,(
    nextState(constB74,constB75) )).

tff(pathAxiom_73,axiom,(
    nextState(constB73,constB74) )).

tff(pathAxiom_72,axiom,(
    nextState(constB72,constB73) )).

tff(pathAxiom_71,axiom,(
    nextState(constB71,constB72) )).

tff(pathAxiom_70,axiom,(
    nextState(constB70,constB71) )).

tff(pathAxiom_69,axiom,(
    nextState(constB69,constB70) )).

tff(pathAxiom_68,axiom,(
    nextState(constB68,constB69) )).

tff(pathAxiom_67,axiom,(
    nextState(constB67,constB68) )).

tff(pathAxiom_66,axiom,(
    nextState(constB66,constB67) )).

tff(pathAxiom_65,axiom,(
    nextState(constB65,constB66) )).

tff(pathAxiom_64,axiom,(
    nextState(constB64,constB65) )).

tff(pathAxiom_63,axiom,(
    nextState(constB63,constB64) )).

tff(pathAxiom_62,axiom,(
    nextState(constB62,constB63) )).

tff(pathAxiom_61,axiom,(
    nextState(constB61,constB62) )).

tff(pathAxiom_60,axiom,(
    nextState(constB60,constB61) )).

tff(pathAxiom_59,axiom,(
    nextState(constB59,constB60) )).

tff(pathAxiom_58,axiom,(
    nextState(constB58,constB59) )).

tff(pathAxiom_57,axiom,(
    nextState(constB57,constB58) )).

tff(pathAxiom_56,axiom,(
    nextState(constB56,constB57) )).

tff(pathAxiom_55,axiom,(
    nextState(constB55,constB56) )).

tff(pathAxiom_54,axiom,(
    nextState(constB54,constB55) )).

tff(pathAxiom_53,axiom,(
    nextState(constB53,constB54) )).

tff(pathAxiom_52,axiom,(
    nextState(constB52,constB53) )).

tff(pathAxiom_51,axiom,(
    nextState(constB51,constB52) )).

tff(pathAxiom_50,axiom,(
    nextState(constB50,constB51) )).

tff(pathAxiom_49,axiom,(
    nextState(constB49,constB50) )).

tff(pathAxiom_48,axiom,(
    nextState(constB48,constB49) )).

tff(pathAxiom_47,axiom,(
    nextState(constB47,constB48) )).

tff(pathAxiom_46,axiom,(
    nextState(constB46,constB47) )).

tff(pathAxiom_45,axiom,(
    nextState(constB45,constB46) )).

tff(pathAxiom_44,axiom,(
    nextState(constB44,constB45) )).

tff(pathAxiom_43,axiom,(
    nextState(constB43,constB44) )).

tff(pathAxiom_42,axiom,(
    nextState(constB42,constB43) )).

tff(pathAxiom_41,axiom,(
    nextState(constB41,constB42) )).

tff(pathAxiom_40,axiom,(
    nextState(constB40,constB41) )).

tff(pathAxiom_39,axiom,(
    nextState(constB39,constB40) )).

tff(pathAxiom_38,axiom,(
    nextState(constB38,constB39) )).

tff(pathAxiom_37,axiom,(
    nextState(constB37,constB38) )).

tff(pathAxiom_36,axiom,(
    nextState(constB36,constB37) )).

tff(pathAxiom_35,axiom,(
    nextState(constB35,constB36) )).

tff(pathAxiom_34,axiom,(
    nextState(constB34,constB35) )).

tff(pathAxiom_33,axiom,(
    nextState(constB33,constB34) )).

tff(pathAxiom_32,axiom,(
    nextState(constB32,constB33) )).

tff(pathAxiom_31,axiom,(
    nextState(constB31,constB32) )).

tff(pathAxiom_30,axiom,(
    nextState(constB30,constB31) )).

tff(pathAxiom_29,axiom,(
    nextState(constB29,constB30) )).

tff(pathAxiom_28,axiom,(
    nextState(constB28,constB29) )).

tff(pathAxiom_27,axiom,(
    nextState(constB27,constB28) )).

tff(pathAxiom_26,axiom,(
    nextState(constB26,constB27) )).

tff(pathAxiom_25,axiom,(
    nextState(constB25,constB26) )).

tff(pathAxiom_24,axiom,(
    nextState(constB24,constB25) )).

tff(pathAxiom_23,axiom,(
    nextState(constB23,constB24) )).

tff(pathAxiom_22,axiom,(
    nextState(constB22,constB23) )).

tff(pathAxiom_21,axiom,(
    nextState(constB21,constB22) )).

tff(pathAxiom_20,axiom,(
    nextState(constB20,constB21) )).

tff(pathAxiom_19,axiom,(
    nextState(constB19,constB20) )).

tff(pathAxiom_18,axiom,(
    nextState(constB18,constB19) )).

tff(pathAxiom_17,axiom,(
    nextState(constB17,constB18) )).

tff(pathAxiom_16,axiom,(
    nextState(constB16,constB17) )).

tff(pathAxiom_15,axiom,(
    nextState(constB15,constB16) )).

tff(pathAxiom_14,axiom,(
    nextState(constB14,constB15) )).

tff(pathAxiom_13,axiom,(
    nextState(constB13,constB14) )).

tff(pathAxiom_12,axiom,(
    nextState(constB12,constB13) )).

tff(pathAxiom_11,axiom,(
    nextState(constB11,constB12) )).

tff(pathAxiom_10,axiom,(
    nextState(constB10,constB11) )).

tff(pathAxiom_9,axiom,(
    nextState(constB9,constB10) )).

tff(pathAxiom_8,axiom,(
    nextState(constB8,constB9) )).

tff(pathAxiom_7,axiom,(
    nextState(constB7,constB8) )).

tff(pathAxiom_6,axiom,(
    nextState(constB6,constB7) )).

tff(pathAxiom_5,axiom,(
    nextState(constB5,constB6) )).

tff(pathAxiom_4,axiom,(
    nextState(constB4,constB5) )).

tff(pathAxiom_3,axiom,(
    nextState(constB3,constB4) )).

tff(pathAxiom_2,axiom,(
    nextState(constB2,constB3) )).

tff(pathAxiom_1,axiom,(
    nextState(constB1,constB2) )).

tff(pathAxiom,axiom,(
    nextState(constB0,constB1) )).

tff(reachableStateAxiom_202,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( reachableState(VarCurr)
        & reachableState(VarNext) ) ) )).

tff(reachableStateAxiom_201,axiom,(
    ! [VarState: state_type] :
      ( reachableState(VarState)
     => ( constB0 = VarState
        | constB1 = VarState
        | constB2 = VarState
        | constB3 = VarState
        | constB4 = VarState
        | constB5 = VarState
        | constB6 = VarState
        | constB7 = VarState
        | constB8 = VarState
        | constB9 = VarState
        | constB10 = VarState
        | constB11 = VarState
        | constB12 = VarState
        | constB13 = VarState
        | constB14 = VarState
        | constB15 = VarState
        | constB16 = VarState
        | constB17 = VarState
        | constB18 = VarState
        | constB19 = VarState
        | constB20 = VarState
        | constB21 = VarState
        | constB22 = VarState
        | constB23 = VarState
        | constB24 = VarState
        | constB25 = VarState
        | constB26 = VarState
        | constB27 = VarState
        | constB28 = VarState
        | constB29 = VarState
        | constB30 = VarState
        | constB31 = VarState
        | constB32 = VarState
        | constB33 = VarState
        | constB34 = VarState
        | constB35 = VarState
        | constB36 = VarState
        | constB37 = VarState
        | constB38 = VarState
        | constB39 = VarState
        | constB40 = VarState
        | constB41 = VarState
        | constB42 = VarState
        | constB43 = VarState
        | constB44 = VarState
        | constB45 = VarState
        | constB46 = VarState
        | constB47 = VarState
        | constB48 = VarState
        | constB49 = VarState
        | constB50 = VarState
        | constB51 = VarState
        | constB52 = VarState
        | constB53 = VarState
        | constB54 = VarState
        | constB55 = VarState
        | constB56 = VarState
        | constB57 = VarState
        | constB58 = VarState
        | constB59 = VarState
        | constB60 = VarState
        | constB61 = VarState
        | constB62 = VarState
        | constB63 = VarState
        | constB64 = VarState
        | constB65 = VarState
        | constB66 = VarState
        | constB67 = VarState
        | constB68 = VarState
        | constB69 = VarState
        | constB70 = VarState
        | constB71 = VarState
        | constB72 = VarState
        | constB73 = VarState
        | constB74 = VarState
        | constB75 = VarState
        | constB76 = VarState
        | constB77 = VarState
        | constB78 = VarState
        | constB79 = VarState
        | constB80 = VarState
        | constB81 = VarState
        | constB82 = VarState
        | constB83 = VarState
        | constB84 = VarState
        | constB85 = VarState
        | constB86 = VarState
        | constB87 = VarState
        | constB88 = VarState
        | constB89 = VarState
        | constB90 = VarState
        | constB91 = VarState
        | constB92 = VarState
        | constB93 = VarState
        | constB94 = VarState
        | constB95 = VarState
        | constB96 = VarState
        | constB97 = VarState
        | constB98 = VarState
        | constB99 = VarState
        | constB100 = VarState
        | constB101 = VarState
        | constB102 = VarState
        | constB103 = VarState
        | constB104 = VarState
        | constB105 = VarState
        | constB106 = VarState
        | constB107 = VarState
        | constB108 = VarState
        | constB109 = VarState
        | constB110 = VarState
        | constB111 = VarState
        | constB112 = VarState
        | constB113 = VarState
        | constB114 = VarState
        | constB115 = VarState
        | constB116 = VarState
        | constB117 = VarState
        | constB118 = VarState
        | constB119 = VarState
        | constB120 = VarState
        | constB121 = VarState
        | constB122 = VarState
        | constB123 = VarState
        | constB124 = VarState
        | constB125 = VarState
        | constB126 = VarState
        | constB127 = VarState
        | constB128 = VarState
        | constB129 = VarState
        | constB130 = VarState
        | constB131 = VarState
        | constB132 = VarState
        | constB133 = VarState
        | constB134 = VarState
        | constB135 = VarState
        | constB136 = VarState
        | constB137 = VarState
        | constB138 = VarState
        | constB139 = VarState
        | constB140 = VarState
        | constB141 = VarState
        | constB142 = VarState
        | constB143 = VarState
        | constB144 = VarState
        | constB145 = VarState
        | constB146 = VarState
        | constB147 = VarState
        | constB148 = VarState
        | constB149 = VarState
        | constB150 = VarState
        | constB151 = VarState
        | constB152 = VarState
        | constB153 = VarState
        | constB154 = VarState
        | constB155 = VarState
        | constB156 = VarState
        | constB157 = VarState
        | constB158 = VarState
        | constB159 = VarState
        | constB160 = VarState
        | constB161 = VarState
        | constB162 = VarState
        | constB163 = VarState
        | constB164 = VarState
        | constB165 = VarState
        | constB166 = VarState
        | constB167 = VarState
        | constB168 = VarState
        | constB169 = VarState
        | constB170 = VarState
        | constB171 = VarState
        | constB172 = VarState
        | constB173 = VarState
        | constB174 = VarState
        | constB175 = VarState
        | constB176 = VarState
        | constB177 = VarState
        | constB178 = VarState
        | constB179 = VarState
        | constB180 = VarState
        | constB181 = VarState
        | constB182 = VarState
        | constB183 = VarState
        | constB184 = VarState
        | constB185 = VarState
        | constB186 = VarState
        | constB187 = VarState
        | constB188 = VarState
        | constB189 = VarState
        | constB190 = VarState
        | constB191 = VarState
        | constB192 = VarState
        | constB193 = VarState
        | constB194 = VarState
        | constB195 = VarState
        | constB196 = VarState
        | constB197 = VarState
        | constB198 = VarState
        | constB199 = VarState
        | constB200 = VarState ) ) )).

tff(reachableStateAxiom_200,axiom,(
    reachableState(constB200) )).

tff(reachableStateAxiom_199,axiom,(
    reachableState(constB199) )).

tff(reachableStateAxiom_198,axiom,(
    reachableState(constB198) )).

tff(reachableStateAxiom_197,axiom,(
    reachableState(constB197) )).

tff(reachableStateAxiom_196,axiom,(
    reachableState(constB196) )).

tff(reachableStateAxiom_195,axiom,(
    reachableState(constB195) )).

tff(reachableStateAxiom_194,axiom,(
    reachableState(constB194) )).

tff(reachableStateAxiom_193,axiom,(
    reachableState(constB193) )).

tff(reachableStateAxiom_192,axiom,(
    reachableState(constB192) )).

tff(reachableStateAxiom_191,axiom,(
    reachableState(constB191) )).

tff(reachableStateAxiom_190,axiom,(
    reachableState(constB190) )).

tff(reachableStateAxiom_189,axiom,(
    reachableState(constB189) )).

tff(reachableStateAxiom_188,axiom,(
    reachableState(constB188) )).

tff(reachableStateAxiom_187,axiom,(
    reachableState(constB187) )).

tff(reachableStateAxiom_186,axiom,(
    reachableState(constB186) )).

tff(reachableStateAxiom_185,axiom,(
    reachableState(constB185) )).

tff(reachableStateAxiom_184,axiom,(
    reachableState(constB184) )).

tff(reachableStateAxiom_183,axiom,(
    reachableState(constB183) )).

tff(reachableStateAxiom_182,axiom,(
    reachableState(constB182) )).

tff(reachableStateAxiom_181,axiom,(
    reachableState(constB181) )).

tff(reachableStateAxiom_180,axiom,(
    reachableState(constB180) )).

tff(reachableStateAxiom_179,axiom,(
    reachableState(constB179) )).

tff(reachableStateAxiom_178,axiom,(
    reachableState(constB178) )).

tff(reachableStateAxiom_177,axiom,(
    reachableState(constB177) )).

tff(reachableStateAxiom_176,axiom,(
    reachableState(constB176) )).

tff(reachableStateAxiom_175,axiom,(
    reachableState(constB175) )).

tff(reachableStateAxiom_174,axiom,(
    reachableState(constB174) )).

tff(reachableStateAxiom_173,axiom,(
    reachableState(constB173) )).

tff(reachableStateAxiom_172,axiom,(
    reachableState(constB172) )).

tff(reachableStateAxiom_171,axiom,(
    reachableState(constB171) )).

tff(reachableStateAxiom_170,axiom,(
    reachableState(constB170) )).

tff(reachableStateAxiom_169,axiom,(
    reachableState(constB169) )).

tff(reachableStateAxiom_168,axiom,(
    reachableState(constB168) )).

tff(reachableStateAxiom_167,axiom,(
    reachableState(constB167) )).

tff(reachableStateAxiom_166,axiom,(
    reachableState(constB166) )).

tff(reachableStateAxiom_165,axiom,(
    reachableState(constB165) )).

tff(reachableStateAxiom_164,axiom,(
    reachableState(constB164) )).

tff(reachableStateAxiom_163,axiom,(
    reachableState(constB163) )).

tff(reachableStateAxiom_162,axiom,(
    reachableState(constB162) )).

tff(reachableStateAxiom_161,axiom,(
    reachableState(constB161) )).

tff(reachableStateAxiom_160,axiom,(
    reachableState(constB160) )).

tff(reachableStateAxiom_159,axiom,(
    reachableState(constB159) )).

tff(reachableStateAxiom_158,axiom,(
    reachableState(constB158) )).

tff(reachableStateAxiom_157,axiom,(
    reachableState(constB157) )).

tff(reachableStateAxiom_156,axiom,(
    reachableState(constB156) )).

tff(reachableStateAxiom_155,axiom,(
    reachableState(constB155) )).

tff(reachableStateAxiom_154,axiom,(
    reachableState(constB154) )).

tff(reachableStateAxiom_153,axiom,(
    reachableState(constB153) )).

tff(reachableStateAxiom_152,axiom,(
    reachableState(constB152) )).

tff(reachableStateAxiom_151,axiom,(
    reachableState(constB151) )).

tff(reachableStateAxiom_150,axiom,(
    reachableState(constB150) )).

tff(reachableStateAxiom_149,axiom,(
    reachableState(constB149) )).

tff(reachableStateAxiom_148,axiom,(
    reachableState(constB148) )).

tff(reachableStateAxiom_147,axiom,(
    reachableState(constB147) )).

tff(reachableStateAxiom_146,axiom,(
    reachableState(constB146) )).

tff(reachableStateAxiom_145,axiom,(
    reachableState(constB145) )).

tff(reachableStateAxiom_144,axiom,(
    reachableState(constB144) )).

tff(reachableStateAxiom_143,axiom,(
    reachableState(constB143) )).

tff(reachableStateAxiom_142,axiom,(
    reachableState(constB142) )).

tff(reachableStateAxiom_141,axiom,(
    reachableState(constB141) )).

tff(reachableStateAxiom_140,axiom,(
    reachableState(constB140) )).

tff(reachableStateAxiom_139,axiom,(
    reachableState(constB139) )).

tff(reachableStateAxiom_138,axiom,(
    reachableState(constB138) )).

tff(reachableStateAxiom_137,axiom,(
    reachableState(constB137) )).

tff(reachableStateAxiom_136,axiom,(
    reachableState(constB136) )).

tff(reachableStateAxiom_135,axiom,(
    reachableState(constB135) )).

tff(reachableStateAxiom_134,axiom,(
    reachableState(constB134) )).

tff(reachableStateAxiom_133,axiom,(
    reachableState(constB133) )).

tff(reachableStateAxiom_132,axiom,(
    reachableState(constB132) )).

tff(reachableStateAxiom_131,axiom,(
    reachableState(constB131) )).

tff(reachableStateAxiom_130,axiom,(
    reachableState(constB130) )).

tff(reachableStateAxiom_129,axiom,(
    reachableState(constB129) )).

tff(reachableStateAxiom_128,axiom,(
    reachableState(constB128) )).

tff(reachableStateAxiom_127,axiom,(
    reachableState(constB127) )).

tff(reachableStateAxiom_126,axiom,(
    reachableState(constB126) )).

tff(reachableStateAxiom_125,axiom,(
    reachableState(constB125) )).

tff(reachableStateAxiom_124,axiom,(
    reachableState(constB124) )).

tff(reachableStateAxiom_123,axiom,(
    reachableState(constB123) )).

tff(reachableStateAxiom_122,axiom,(
    reachableState(constB122) )).

tff(reachableStateAxiom_121,axiom,(
    reachableState(constB121) )).

tff(reachableStateAxiom_120,axiom,(
    reachableState(constB120) )).

tff(reachableStateAxiom_119,axiom,(
    reachableState(constB119) )).

tff(reachableStateAxiom_118,axiom,(
    reachableState(constB118) )).

tff(reachableStateAxiom_117,axiom,(
    reachableState(constB117) )).

tff(reachableStateAxiom_116,axiom,(
    reachableState(constB116) )).

tff(reachableStateAxiom_115,axiom,(
    reachableState(constB115) )).

tff(reachableStateAxiom_114,axiom,(
    reachableState(constB114) )).

tff(reachableStateAxiom_113,axiom,(
    reachableState(constB113) )).

tff(reachableStateAxiom_112,axiom,(
    reachableState(constB112) )).

tff(reachableStateAxiom_111,axiom,(
    reachableState(constB111) )).

tff(reachableStateAxiom_110,axiom,(
    reachableState(constB110) )).

tff(reachableStateAxiom_109,axiom,(
    reachableState(constB109) )).

tff(reachableStateAxiom_108,axiom,(
    reachableState(constB108) )).

tff(reachableStateAxiom_107,axiom,(
    reachableState(constB107) )).

tff(reachableStateAxiom_106,axiom,(
    reachableState(constB106) )).

tff(reachableStateAxiom_105,axiom,(
    reachableState(constB105) )).

tff(reachableStateAxiom_104,axiom,(
    reachableState(constB104) )).

tff(reachableStateAxiom_103,axiom,(
    reachableState(constB103) )).

tff(reachableStateAxiom_102,axiom,(
    reachableState(constB102) )).

tff(reachableStateAxiom_101,axiom,(
    reachableState(constB101) )).

tff(reachableStateAxiom_100,axiom,(
    reachableState(constB100) )).

tff(reachableStateAxiom_99,axiom,(
    reachableState(constB99) )).

tff(reachableStateAxiom_98,axiom,(
    reachableState(constB98) )).

tff(reachableStateAxiom_97,axiom,(
    reachableState(constB97) )).

tff(reachableStateAxiom_96,axiom,(
    reachableState(constB96) )).

tff(reachableStateAxiom_95,axiom,(
    reachableState(constB95) )).

tff(reachableStateAxiom_94,axiom,(
    reachableState(constB94) )).

tff(reachableStateAxiom_93,axiom,(
    reachableState(constB93) )).

tff(reachableStateAxiom_92,axiom,(
    reachableState(constB92) )).

tff(reachableStateAxiom_91,axiom,(
    reachableState(constB91) )).

tff(reachableStateAxiom_90,axiom,(
    reachableState(constB90) )).

tff(reachableStateAxiom_89,axiom,(
    reachableState(constB89) )).

tff(reachableStateAxiom_88,axiom,(
    reachableState(constB88) )).

tff(reachableStateAxiom_87,axiom,(
    reachableState(constB87) )).

tff(reachableStateAxiom_86,axiom,(
    reachableState(constB86) )).

tff(reachableStateAxiom_85,axiom,(
    reachableState(constB85) )).

tff(reachableStateAxiom_84,axiom,(
    reachableState(constB84) )).

tff(reachableStateAxiom_83,axiom,(
    reachableState(constB83) )).

tff(reachableStateAxiom_82,axiom,(
    reachableState(constB82) )).

tff(reachableStateAxiom_81,axiom,(
    reachableState(constB81) )).

tff(reachableStateAxiom_80,axiom,(
    reachableState(constB80) )).

tff(reachableStateAxiom_79,axiom,(
    reachableState(constB79) )).

tff(reachableStateAxiom_78,axiom,(
    reachableState(constB78) )).

tff(reachableStateAxiom_77,axiom,(
    reachableState(constB77) )).

tff(reachableStateAxiom_76,axiom,(
    reachableState(constB76) )).

tff(reachableStateAxiom_75,axiom,(
    reachableState(constB75) )).

tff(reachableStateAxiom_74,axiom,(
    reachableState(constB74) )).

tff(reachableStateAxiom_73,axiom,(
    reachableState(constB73) )).

tff(reachableStateAxiom_72,axiom,(
    reachableState(constB72) )).

tff(reachableStateAxiom_71,axiom,(
    reachableState(constB71) )).

tff(reachableStateAxiom_70,axiom,(
    reachableState(constB70) )).

tff(reachableStateAxiom_69,axiom,(
    reachableState(constB69) )).

tff(reachableStateAxiom_68,axiom,(
    reachableState(constB68) )).

tff(reachableStateAxiom_67,axiom,(
    reachableState(constB67) )).

tff(reachableStateAxiom_66,axiom,(
    reachableState(constB66) )).

tff(reachableStateAxiom_65,axiom,(
    reachableState(constB65) )).

tff(reachableStateAxiom_64,axiom,(
    reachableState(constB64) )).

tff(reachableStateAxiom_63,axiom,(
    reachableState(constB63) )).

tff(reachableStateAxiom_62,axiom,(
    reachableState(constB62) )).

tff(reachableStateAxiom_61,axiom,(
    reachableState(constB61) )).

tff(reachableStateAxiom_60,axiom,(
    reachableState(constB60) )).

tff(reachableStateAxiom_59,axiom,(
    reachableState(constB59) )).

tff(reachableStateAxiom_58,axiom,(
    reachableState(constB58) )).

tff(reachableStateAxiom_57,axiom,(
    reachableState(constB57) )).

tff(reachableStateAxiom_56,axiom,(
    reachableState(constB56) )).

tff(reachableStateAxiom_55,axiom,(
    reachableState(constB55) )).

tff(reachableStateAxiom_54,axiom,(
    reachableState(constB54) )).

tff(reachableStateAxiom_53,axiom,(
    reachableState(constB53) )).

tff(reachableStateAxiom_52,axiom,(
    reachableState(constB52) )).

tff(reachableStateAxiom_51,axiom,(
    reachableState(constB51) )).

tff(reachableStateAxiom_50,axiom,(
    reachableState(constB50) )).

tff(reachableStateAxiom_49,axiom,(
    reachableState(constB49) )).

tff(reachableStateAxiom_48,axiom,(
    reachableState(constB48) )).

tff(reachableStateAxiom_47,axiom,(
    reachableState(constB47) )).

tff(reachableStateAxiom_46,axiom,(
    reachableState(constB46) )).

tff(reachableStateAxiom_45,axiom,(
    reachableState(constB45) )).

tff(reachableStateAxiom_44,axiom,(
    reachableState(constB44) )).

tff(reachableStateAxiom_43,axiom,(
    reachableState(constB43) )).

tff(reachableStateAxiom_42,axiom,(
    reachableState(constB42) )).

tff(reachableStateAxiom_41,axiom,(
    reachableState(constB41) )).

tff(reachableStateAxiom_40,axiom,(
    reachableState(constB40) )).

tff(reachableStateAxiom_39,axiom,(
    reachableState(constB39) )).

tff(reachableStateAxiom_38,axiom,(
    reachableState(constB38) )).

tff(reachableStateAxiom_37,axiom,(
    reachableState(constB37) )).

tff(reachableStateAxiom_36,axiom,(
    reachableState(constB36) )).

tff(reachableStateAxiom_35,axiom,(
    reachableState(constB35) )).

tff(reachableStateAxiom_34,axiom,(
    reachableState(constB34) )).

tff(reachableStateAxiom_33,axiom,(
    reachableState(constB33) )).

tff(reachableStateAxiom_32,axiom,(
    reachableState(constB32) )).

tff(reachableStateAxiom_31,axiom,(
    reachableState(constB31) )).

tff(reachableStateAxiom_30,axiom,(
    reachableState(constB30) )).

tff(reachableStateAxiom_29,axiom,(
    reachableState(constB29) )).

tff(reachableStateAxiom_28,axiom,(
    reachableState(constB28) )).

tff(reachableStateAxiom_27,axiom,(
    reachableState(constB27) )).

tff(reachableStateAxiom_26,axiom,(
    reachableState(constB26) )).

tff(reachableStateAxiom_25,axiom,(
    reachableState(constB25) )).

tff(reachableStateAxiom_24,axiom,(
    reachableState(constB24) )).

tff(reachableStateAxiom_23,axiom,(
    reachableState(constB23) )).

tff(reachableStateAxiom_22,axiom,(
    reachableState(constB22) )).

tff(reachableStateAxiom_21,axiom,(
    reachableState(constB21) )).

tff(reachableStateAxiom_20,axiom,(
    reachableState(constB20) )).

tff(reachableStateAxiom_19,axiom,(
    reachableState(constB19) )).

tff(reachableStateAxiom_18,axiom,(
    reachableState(constB18) )).

tff(reachableStateAxiom_17,axiom,(
    reachableState(constB17) )).

tff(reachableStateAxiom_16,axiom,(
    reachableState(constB16) )).

tff(reachableStateAxiom_15,axiom,(
    reachableState(constB15) )).

tff(reachableStateAxiom_14,axiom,(
    reachableState(constB14) )).

tff(reachableStateAxiom_13,axiom,(
    reachableState(constB13) )).

tff(reachableStateAxiom_12,axiom,(
    reachableState(constB12) )).

tff(reachableStateAxiom_11,axiom,(
    reachableState(constB11) )).

tff(reachableStateAxiom_10,axiom,(
    reachableState(constB10) )).

tff(reachableStateAxiom_9,axiom,(
    reachableState(constB9) )).

tff(reachableStateAxiom_8,axiom,(
    reachableState(constB8) )).

tff(reachableStateAxiom_7,axiom,(
    reachableState(constB7) )).

tff(reachableStateAxiom_6,axiom,(
    reachableState(constB6) )).

tff(reachableStateAxiom_5,axiom,(
    reachableState(constB5) )).

tff(reachableStateAxiom_4,axiom,(
    reachableState(constB4) )).

tff(reachableStateAxiom_3,axiom,(
    reachableState(constB3) )).

tff(reachableStateAxiom_2,axiom,(
    reachableState(constB2) )).

tff(reachableStateAxiom_1,axiom,(
    reachableState(constB1) )).

tff(reachableStateAxiom,axiom,(
    reachableState(constB0) )).

tff(addAssertion,conjecture,(
    ! [VarCurr: state_type] :
      ( reachableState(VarCurr)
     => v82(VarCurr) ) )).

tff(addGlobalAssumption_2,axiom,(
    ! [VarCurr: state_type] : v24(VarCurr) )).

tff(addGlobalAssumption_1,axiom,(
    ! [VarCurr: state_type] : v58(VarCurr) )).

tff(addGlobalAssumption,axiom,(
    ! [VarCurr: state_type] : v4(VarCurr) )).

tff(writeUnaryOperator_26,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v82(VarCurr)
    <=> v286(VarCurr) ) )).

tff(writeUnaryOperator_25,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v286(VarCurr)
    <=> v287(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_99,axiom,(
    ! [VarCurr: state_type] :
      ( v287(VarCurr)
    <=> ( v288(VarCurr)
        & v313(VarCurr) ) ) )).

tff(writeUnaryOperator_24,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v313(VarCurr)
    <=> v314(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_98,axiom,(
    ! [VarCurr: state_type] :
      ( v314(VarCurr)
    <=> ( v303(VarCurr)
        & v253(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_97,axiom,(
    ! [VarCurr: state_type] :
      ( v288(VarCurr)
    <=> ( v289(VarCurr)
        & v308(VarCurr) ) ) )).

tff(writeUnaryOperator_23,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v308(VarCurr)
    <=> v309(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_96,axiom,(
    ! [VarCurr: state_type] :
      ( v309(VarCurr)
    <=> ( v310(VarCurr)
        & v275(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_95,axiom,(
    ! [VarCurr: state_type] :
      ( v310(VarCurr)
    <=> ( v186(VarCurr)
        & v311(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_94,axiom,(
    ! [VarCurr: state_type] :
      ( v311(VarCurr)
    <=> ( v184(VarCurr)
        & v312(VarCurr) ) ) )).

tff(writeUnaryOperator_22,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v312(VarCurr)
    <=> v159(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_93,axiom,(
    ! [VarCurr: state_type] :
      ( v289(VarCurr)
    <=> ( v290(VarCurr)
        & v306(VarCurr) ) ) )).

tff(writeUnaryOperator_21,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v306(VarCurr)
    <=> v307(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_92,axiom,(
    ! [VarCurr: state_type] :
      ( v307(VarCurr)
    <=> ( v294(VarCurr)
        & v251(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_91,axiom,(
    ! [VarCurr: state_type] :
      ( v290(VarCurr)
    <=> ( v291(VarCurr)
        & v301(VarCurr) ) ) )).

tff(writeUnaryOperator_20,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v301(VarCurr)
    <=> v302(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_90,axiom,(
    ! [VarCurr: state_type] :
      ( v302(VarCurr)
    <=> ( v303(VarCurr)
        & v238(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_89,axiom,(
    ! [VarCurr: state_type] :
      ( v303(VarCurr)
    <=> ( v186(VarCurr)
        & v304(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_88,axiom,(
    ! [VarCurr: state_type] :
      ( v304(VarCurr)
    <=> ( v184(VarCurr)
        & v305(VarCurr) ) ) )).

tff(writeUnaryOperator_19,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v305(VarCurr)
    <=> v221(VarCurr) ) )).

tff(writeUnaryOperator_18,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v291(VarCurr)
    <=> v292(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_87,axiom,(
    ! [VarCurr: state_type] :
      ( v292(VarCurr)
    <=> ( v293(VarCurr)
        | v297(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_86,axiom,(
    ! [VarCurr: state_type] :
      ( v297(VarCurr)
    <=> ( v298(VarCurr)
        & v208(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_85,axiom,(
    ! [VarCurr: state_type] :
      ( v298(VarCurr)
    <=> ( v186(VarCurr)
        & v299(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_84,axiom,(
    ! [VarCurr: state_type] :
      ( v299(VarCurr)
    <=> ( v184(VarCurr)
        & v300(VarCurr) ) ) )).

tff(writeUnaryOperator_17,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v300(VarCurr)
    <=> v191(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_83,axiom,(
    ! [VarCurr: state_type] :
      ( v293(VarCurr)
    <=> ( v294(VarCurr)
        & v157(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_82,axiom,(
    ! [VarCurr: state_type] :
      ( v294(VarCurr)
    <=> ( v186(VarCurr)
        & v295(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_81,axiom,(
    ! [VarCurr: state_type] :
      ( v295(VarCurr)
    <=> ( v184(VarCurr)
        & v296(VarCurr) ) ) )).

tff(writeUnaryOperator_16,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v296(VarCurr)
    <=> v84(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_13,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ $true
       => ( v275(VarNext)
        <=> v275(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_13,axiom,(
    ! [VarNext: state_type] :
      ( $true
     => ( v275(VarNext)
      <=> v280(VarNext) ) ) )).

tff(addAssignment_18,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v280(VarNext)
      <=> v278(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_80,axiom,(
    ! [VarCurr: state_type] :
      ( v278(VarCurr)
    <=> ( v281(VarCurr)
        | v282(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_79,axiom,(
    ! [VarCurr: state_type] :
      ( v282(VarCurr)
    <=> ( v216(VarCurr)
        & v251(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_78,axiom,(
    ! [VarCurr: state_type] :
      ( v281(VarCurr)
    <=> ( v182(VarCurr)
        & v275(VarCurr) ) ) )).

tff(addAssignmentInitValue_14,axiom,(
    ~ v275(constB0) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_12,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ $true
       => ( v251(VarNext)
        <=> v251(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_12,axiom,(
    ! [VarNext: state_type] :
      ( $true
     => ( v251(VarNext)
      <=> v269(VarNext) ) ) )).

tff(addAssignment_17,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v269(VarNext)
      <=> v267(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_77,axiom,(
    ! [VarCurr: state_type] :
      ( v267(VarCurr)
    <=> ( v270(VarCurr)
        | v271(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_76,axiom,(
    ! [VarCurr: state_type] :
      ( v271(VarCurr)
    <=> ( v261(VarCurr)
        & v253(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_75,axiom,(
    ! [VarCurr: state_type] :
      ( v270(VarCurr)
    <=> ( v182(VarCurr)
        & v251(VarCurr) ) ) )).

tff(addAssignmentInitValue_13,axiom,(
    ~ v251(constB0) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_11,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ $true
       => ( v253(VarNext)
        <=> v253(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_11,axiom,(
    ! [VarNext: state_type] :
      ( $true
     => ( v253(VarNext)
      <=> v258(VarNext) ) ) )).

tff(addAssignment_16,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v258(VarNext)
      <=> v256(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_74,axiom,(
    ! [VarCurr: state_type] :
      ( v256(VarCurr)
    <=> ( v259(VarCurr)
        | v260(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_73,axiom,(
    ! [VarCurr: state_type] :
      ( v260(VarCurr)
    <=> ( v261(VarCurr)
        & v238(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_72,axiom,(
    ! [VarCurr: state_type] :
      ( v261(VarCurr)
    <=> ( v186(VarCurr)
        & v262(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_71,axiom,(
    ! [VarCurr: state_type] :
      ( v262(VarCurr)
    <=> ( v184(VarCurr)
        & v221(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_70,axiom,(
    ! [VarCurr: state_type] :
      ( v259(VarCurr)
    <=> ( v182(VarCurr)
        & v253(VarCurr) ) ) )).

tff(addAssignmentInitValue_12,axiom,(
    ~ v253(constB0) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_10,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ $true
       => ( v238(VarNext)
        <=> v238(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_10,axiom,(
    ! [VarNext: state_type] :
      ( $true
     => ( v238(VarNext)
      <=> v243(VarNext) ) ) )).

tff(addAssignment_15,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v243(VarNext)
      <=> v241(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_69,axiom,(
    ! [VarCurr: state_type] :
      ( v241(VarCurr)
    <=> ( v244(VarCurr)
        | v245(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_68,axiom,(
    ! [VarCurr: state_type] :
      ( v245(VarCurr)
    <=> ( v246(VarCurr)
        & v208(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_67,axiom,(
    ! [VarCurr: state_type] :
      ( v246(VarCurr)
    <=> ( v186(VarCurr)
        & v247(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_66,axiom,(
    ! [VarCurr: state_type] :
      ( v247(VarCurr)
    <=> ( v184(VarCurr)
        & v191(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_65,axiom,(
    ! [VarCurr: state_type] :
      ( v244(VarCurr)
    <=> ( v182(VarCurr)
        & v238(VarCurr) ) ) )).

tff(addAssignmentInitValue_11,axiom,(
    ~ v238(constB0) )).

tff(addAssignment_14,axiom,(
    ! [VarCurr: state_type] :
      ( v221(VarCurr)
    <=> v223(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_9,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v226(VarNext)
       => ( v223(VarNext)
        <=> v223(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_9,axiom,(
    ! [VarNext: state_type] :
      ( v226(VarNext)
     => ( v223(VarNext)
      <=> v230(VarNext) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_5,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v231(VarCurr)
     => ( v230(VarCurr)
      <=> $true ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_5,axiom,(
    ! [VarCurr: state_type] :
      ( v231(VarCurr)
     => ( v230(VarCurr)
      <=> $false ) ) )).

tff(writeBinaryOperatorShiftedRanges_64,axiom,(
    ! [VarCurr: state_type] :
      ( v231(VarCurr)
    <=> ( v150(VarCurr)
        | v142(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_63,axiom,(
    ! [VarCurr: state_type] :
      ( v150(VarCurr)
    <=> ( v151(VarCurr)
        | v141(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_62,axiom,(
    ! [VarCurr: state_type] :
      ( v226(VarCurr)
    <=> ( v227(VarCurr)
        | v145(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_61,axiom,(
    ! [VarCurr: state_type] :
      ( v227(VarCurr)
    <=> ( v228(VarCurr)
        | v142(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_60,axiom,(
    ! [VarCurr: state_type] :
      ( v228(VarCurr)
    <=> ( v229(VarCurr)
        | v141(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_59,axiom,(
    ! [VarCurr: state_type] :
      ( v229(VarCurr)
    <=> ( v139(VarCurr)
        | v140(VarCurr) ) ) )).

tff(addAssignmentInitValue_10,axiom,(
    ~ v223(constB0) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_8,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ $true
       => ( v208(VarNext)
        <=> v208(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_8,axiom,(
    ! [VarNext: state_type] :
      ( $true
     => ( v208(VarNext)
      <=> v213(VarNext) ) ) )).

tff(addAssignment_13,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v213(VarNext)
      <=> v211(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_58,axiom,(
    ! [VarCurr: state_type] :
      ( v211(VarCurr)
    <=> ( v214(VarCurr)
        | v215(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_57,axiom,(
    ! [VarCurr: state_type] :
      ( v215(VarCurr)
    <=> ( v216(VarCurr)
        & v157(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_56,axiom,(
    ! [VarCurr: state_type] :
      ( v216(VarCurr)
    <=> ( v186(VarCurr)
        & v217(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_55,axiom,(
    ! [VarCurr: state_type] :
      ( v217(VarCurr)
    <=> ( v184(VarCurr)
        & v84(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_54,axiom,(
    ! [VarCurr: state_type] :
      ( v214(VarCurr)
    <=> ( v182(VarCurr)
        & v208(VarCurr) ) ) )).

tff(addAssignmentInitValue_9,axiom,(
    ~ v208(constB0) )).

tff(addAssignment_12,axiom,(
    ! [VarCurr: state_type] :
      ( v191(VarCurr)
    <=> v193(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_7,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v196(VarNext)
       => ( v193(VarNext)
        <=> v193(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_7,axiom,(
    ! [VarNext: state_type] :
      ( v196(VarNext)
     => ( v193(VarNext)
      <=> v200(VarNext) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_3,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v201(VarCurr)
        & ~ v141(VarCurr) )
     => ( v200(VarCurr)
      <=> $false ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_4,axiom,(
    ! [VarCurr: state_type] :
      ( v141(VarCurr)
     => ( v200(VarCurr)
      <=> $true ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_4,axiom,(
    ! [VarCurr: state_type] :
      ( v201(VarCurr)
     => ( v200(VarCurr)
      <=> $false ) ) )).

tff(writeBinaryOperatorShiftedRanges_53,axiom,(
    ! [VarCurr: state_type] :
      ( v202(VarCurr)
    <=> ( v142(VarCurr)
        | v145(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_52,axiom,(
    ! [VarCurr: state_type] :
      ( v201(VarCurr)
    <=> ( v139(VarCurr)
        | v140(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_51,axiom,(
    ! [VarCurr: state_type] :
      ( v196(VarCurr)
    <=> ( v197(VarCurr)
        | v145(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_50,axiom,(
    ! [VarCurr: state_type] :
      ( v197(VarCurr)
    <=> ( v198(VarCurr)
        | v142(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_49,axiom,(
    ! [VarCurr: state_type] :
      ( v198(VarCurr)
    <=> ( v199(VarCurr)
        | v141(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_48,axiom,(
    ! [VarCurr: state_type] :
      ( v199(VarCurr)
    <=> ( v139(VarCurr)
        | v140(VarCurr) ) ) )).

tff(addAssignmentInitValue_8,axiom,(
    ~ v193(constB0) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_6,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ $true
       => ( v157(VarNext)
        <=> v157(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_6,axiom,(
    ! [VarNext: state_type] :
      ( $true
     => ( v157(VarNext)
      <=> v180(VarNext) ) ) )).

tff(addAssignment_11,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v180(VarNext)
      <=> v178(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_47,axiom,(
    ! [VarCurr: state_type] :
      ( v178(VarCurr)
    <=> ( v181(VarCurr)
        | v185(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_46,axiom,(
    ! [VarCurr: state_type] :
      ( v185(VarCurr)
    <=> ( v186(VarCurr)
        & v187(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_45,axiom,(
    ! [VarCurr: state_type] :
      ( v187(VarCurr)
    <=> ( v184(VarCurr)
        & v159(VarCurr) ) ) )).

tff(writeUnaryOperator_15,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v186(VarCurr)
    <=> v26(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_44,axiom,(
    ! [VarCurr: state_type] :
      ( v181(VarCurr)
    <=> ( v182(VarCurr)
        & v157(VarCurr) ) ) )).

tff(writeUnaryOperator_14,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v182(VarCurr)
    <=> v183(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_43,axiom,(
    ! [VarCurr: state_type] :
      ( v183(VarCurr)
    <=> ( v184(VarCurr)
        | v26(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_42,axiom,(
    ! [VarCurr: state_type] :
      ( v184(VarCurr)
    <=> ( v39(VarCurr)
        & v9(VarCurr) ) ) )).

tff(addAssignmentInitValue_7,axiom,(
    ~ v157(constB0) )).

tff(addAssignment_10,axiom,(
    ! [VarCurr: state_type] :
      ( v159(VarCurr)
    <=> v161(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_5,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v164(VarNext)
       => ( v161(VarNext)
        <=> v161(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_5,axiom,(
    ! [VarNext: state_type] :
      ( v164(VarNext)
     => ( v161(VarNext)
      <=> v168(VarNext) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_2,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v139(VarCurr)
        & ~ v140(VarCurr) )
     => ( v168(VarCurr)
      <=> $false ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_3,axiom,(
    ! [VarCurr: state_type] :
      ( v140(VarCurr)
     => ( v168(VarCurr)
      <=> $true ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_3,axiom,(
    ! [VarCurr: state_type] :
      ( v139(VarCurr)
     => ( v168(VarCurr)
      <=> $false ) ) )).

tff(writeBinaryOperatorShiftedRanges_41,axiom,(
    ! [VarCurr: state_type] :
      ( v169(VarCurr)
    <=> ( v171(VarCurr)
        | v145(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_40,axiom,(
    ! [VarCurr: state_type] :
      ( v171(VarCurr)
    <=> ( v141(VarCurr)
        | v142(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_39,axiom,(
    ! [VarCurr: state_type] :
      ( v164(VarCurr)
    <=> ( v165(VarCurr)
        | v145(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_38,axiom,(
    ! [VarCurr: state_type] :
      ( v165(VarCurr)
    <=> ( v166(VarCurr)
        | v142(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_37,axiom,(
    ! [VarCurr: state_type] :
      ( v166(VarCurr)
    <=> ( v167(VarCurr)
        | v141(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_36,axiom,(
    ! [VarCurr: state_type] :
      ( v167(VarCurr)
    <=> ( v139(VarCurr)
        | v140(VarCurr) ) ) )).

tff(addAssignmentInitValue_6,axiom,(
    ~ v161(constB0) )).

tff(addAssignment_9,axiom,(
    ! [VarCurr: state_type] :
      ( v84(VarCurr)
    <=> v86(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_4,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v135(VarNext)
       => ( v86(VarNext)
        <=> v86(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_4,axiom,(
    ! [VarNext: state_type] :
      ( v135(VarNext)
     => ( v86(VarNext)
      <=> v148(VarNext) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_1,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v149(VarCurr)
        & ~ v142(VarCurr) )
     => ( v148(VarCurr)
      <=> $false ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_2,axiom,(
    ! [VarCurr: state_type] :
      ( v142(VarCurr)
     => ( v148(VarCurr)
      <=> $true ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_2,axiom,(
    ! [VarCurr: state_type] :
      ( v149(VarCurr)
     => ( v148(VarCurr)
      <=> $false ) ) )).

tff(writeBinaryOperatorShiftedRanges_35,axiom,(
    ! [VarCurr: state_type] :
      ( v149(VarCurr)
    <=> ( v151(VarCurr)
        | v141(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_34,axiom,(
    ! [VarCurr: state_type] :
      ( v151(VarCurr)
    <=> ( v139(VarCurr)
        | v140(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_33,axiom,(
    ! [VarCurr: state_type] :
      ( v135(VarCurr)
    <=> ( v136(VarCurr)
        | v145(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_32,axiom,(
    ! [VarCurr: state_type] :
      ( v145(VarCurr)
    <=> ( v146(VarCurr)
        | v147(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_15,axiom,(
    ! [VarCurr: state_type] :
      ( v147(VarCurr)
    <=> ( ( v88(VarCurr,2)
        <=> $true )
        & ( v88(VarCurr,1)
        <=> $false )
        & ( v88(VarCurr,0)
        <=> $true ) ) ) )).

tff(addBitVectorEqualityBitBlasted_14,axiom,(
    ! [VarCurr: state_type] :
      ( v146(VarCurr)
    <=> ( ( v88(VarCurr,2)
        <=> $true )
        & ( v88(VarCurr,1)
        <=> $false )
        & ( v88(VarCurr,0)
        <=> $false ) ) ) )).

tff(writeBinaryOperatorShiftedRanges_31,axiom,(
    ! [VarCurr: state_type] :
      ( v136(VarCurr)
    <=> ( v137(VarCurr)
        | v142(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_30,axiom,(
    ! [VarCurr: state_type] :
      ( v142(VarCurr)
    <=> ( v143(VarCurr)
        | v144(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_13,axiom,(
    ! [VarCurr: state_type] :
      ( v144(VarCurr)
    <=> ( ( v88(VarCurr,2)
        <=> $true )
        & ( v88(VarCurr,1)
        <=> $true )
        & ( v88(VarCurr,0)
        <=> $false ) ) ) )).

tff(addBitVectorEqualityBitBlasted_12,axiom,(
    ! [VarCurr: state_type] :
      ( v143(VarCurr)
    <=> ( ( v88(VarCurr,2)
        <=> $false )
        & ( v88(VarCurr,1)
        <=> $true )
        & ( v88(VarCurr,0)
        <=> $false ) ) ) )).

tff(writeBinaryOperatorShiftedRanges_29,axiom,(
    ! [VarCurr: state_type] :
      ( v137(VarCurr)
    <=> ( v138(VarCurr)
        | v141(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_11,axiom,(
    ! [VarCurr: state_type] :
      ( v141(VarCurr)
    <=> ( ( v88(VarCurr,2)
        <=> $false )
        & ( v88(VarCurr,1)
        <=> $true )
        & ( v88(VarCurr,0)
        <=> $true ) ) ) )).

tff(writeBinaryOperatorShiftedRanges_28,axiom,(
    ! [VarCurr: state_type] :
      ( v138(VarCurr)
    <=> ( v139(VarCurr)
        | v140(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_10,axiom,(
    ! [VarCurr: state_type] :
      ( v140(VarCurr)
    <=> ( ( v88(VarCurr,2)
        <=> $false )
        & ( v88(VarCurr,1)
        <=> $false )
        & ( v88(VarCurr,0)
        <=> $true ) ) ) )).

tff(addBitVectorEqualityBitBlasted_9,axiom,(
    ! [VarCurr: state_type] :
      ( v139(VarCurr)
    <=> ( ( v88(VarCurr,2)
        <=> $false )
        & ( v88(VarCurr,1)
        <=> $false )
        & ( v88(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignmentInitValue_5,axiom,(
    ~ v86(constB0) )).

tff(addAssignment_8,axiom,(
    ! [VarCurr: state_type] :
      ( ( v88(VarCurr,2)
      <=> v90(VarCurr,2) )
      & ( v88(VarCurr,1)
      <=> v90(VarCurr,1) )
      & ( v88(VarCurr,0)
      <=> v90(VarCurr,0) ) ) )).

tff(addCaseBooleanConditionEqualRanges1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v100(VarNext)
       => ( ( v90(VarNext,2)
          <=> v90(VarCurr,2) )
          & ( v90(VarNext,1)
          <=> v90(VarCurr,1) )
          & ( v90(VarNext,0)
          <=> v90(VarCurr,0) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0,axiom,(
    ! [VarNext: state_type] :
      ( v100(VarNext)
     => ( ( v90(VarNext,2)
        <=> v129(VarNext,2) )
        & ( v90(VarNext,1)
        <=> v129(VarNext,1) )
        & ( v90(VarNext,0)
        <=> v129(VarNext,0) ) ) ) )).

tff(addAssignment_7,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ( v129(VarNext,2)
        <=> v127(VarCurr,2) )
        & ( v129(VarNext,1)
        <=> v127(VarCurr,1) )
        & ( v129(VarNext,0)
        <=> v127(VarCurr,0) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v111(VarCurr)
     => ( ( v127(VarCurr,2)
        <=> v130(VarCurr,2) )
        & ( v127(VarCurr,1)
        <=> v130(VarCurr,1) )
        & ( v127(VarCurr,0)
        <=> v130(VarCurr,0) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_1,axiom,(
    ! [VarCurr: state_type] :
      ( v111(VarCurr)
     => ( ( v127(VarCurr,2)
        <=> $false )
        & ( v127(VarCurr,1)
        <=> $false )
        & ( v127(VarCurr,0)
        <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges6,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v119(VarCurr)
        & ~ v120(VarCurr)
        & ~ v121(VarCurr)
        & ~ v122(VarCurr)
        & ~ v123(VarCurr)
        & ~ v124(VarCurr) )
     => ( ( v130(VarCurr,2)
        <=> $false )
        & ( v130(VarCurr,1)
        <=> $false )
        & ( v130(VarCurr,0)
        <=> $true ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges5,axiom,(
    ! [VarCurr: state_type] :
      ( v124(VarCurr)
     => ( ( v130(VarCurr,2)
        <=> $true )
        & ( v130(VarCurr,1)
        <=> $true )
        & ( v130(VarCurr,0)
        <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges4,axiom,(
    ! [VarCurr: state_type] :
      ( v123(VarCurr)
     => ( ( v130(VarCurr,2)
        <=> $true )
        & ( v130(VarCurr,1)
        <=> $false )
        & ( v130(VarCurr,0)
        <=> $true ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges3,axiom,(
    ! [VarCurr: state_type] :
      ( v122(VarCurr)
     => ( ( v130(VarCurr,2)
        <=> $true )
        & ( v130(VarCurr,1)
        <=> $false )
        & ( v130(VarCurr,0)
        <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2,axiom,(
    ! [VarCurr: state_type] :
      ( v121(VarCurr)
     => ( ( v130(VarCurr,2)
        <=> $false )
        & ( v130(VarCurr,1)
        <=> $true )
        & ( v130(VarCurr,0)
        <=> $true ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1,axiom,(
    ! [VarCurr: state_type] :
      ( v120(VarCurr)
     => ( ( v130(VarCurr,2)
        <=> $false )
        & ( v130(VarCurr,1)
        <=> $true )
        & ( v130(VarCurr,0)
        <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0,axiom,(
    ! [VarCurr: state_type] :
      ( v119(VarCurr)
     => ( ( v130(VarCurr,2)
        <=> $false )
        & ( v130(VarCurr,1)
        <=> $false )
        & ( v130(VarCurr,0)
        <=> $true ) ) ) )).

tff(writeBinaryOperatorShiftedRanges_27,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v100(VarNext)
      <=> ( v101(VarNext)
          & v110(VarNext) ) ) ) )).

tff(addAssignment_6,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v110(VarNext)
      <=> v108(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_26,axiom,(
    ! [VarCurr: state_type] :
      ( v108(VarCurr)
    <=> ( v111(VarCurr)
        | v112(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_25,axiom,(
    ! [VarCurr: state_type] :
      ( v112(VarCurr)
    <=> ( v113(VarCurr)
        & v126(VarCurr) ) ) )).

tff(writeUnaryOperator_13,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v126(VarCurr)
    <=> v111(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_24,axiom,(
    ! [VarCurr: state_type] :
      ( v113(VarCurr)
    <=> ( v114(VarCurr)
        | v125(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_8,axiom,(
    ! [VarCurr: state_type] :
      ( v125(VarCurr)
    <=> ( ( v90(VarCurr,2)
        <=> $true )
        & ( v90(VarCurr,1)
        <=> $true )
        & ( v90(VarCurr,0)
        <=> $false ) ) ) )).

tff(writeBinaryOperatorShiftedRanges_23,axiom,(
    ! [VarCurr: state_type] :
      ( v114(VarCurr)
    <=> ( v115(VarCurr)
        | v124(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_7,axiom,(
    ! [VarCurr: state_type] :
      ( v124(VarCurr)
    <=> ( ( v90(VarCurr,2)
        <=> $true )
        & ( v90(VarCurr,1)
        <=> $false )
        & ( v90(VarCurr,0)
        <=> $true ) ) ) )).

tff(writeBinaryOperatorShiftedRanges_22,axiom,(
    ! [VarCurr: state_type] :
      ( v115(VarCurr)
    <=> ( v116(VarCurr)
        | v123(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_6,axiom,(
    ! [VarCurr: state_type] :
      ( v123(VarCurr)
    <=> ( ( v90(VarCurr,2)
        <=> $true )
        & ( v90(VarCurr,1)
        <=> $false )
        & ( v90(VarCurr,0)
        <=> $false ) ) ) )).

tff(writeBinaryOperatorShiftedRanges_21,axiom,(
    ! [VarCurr: state_type] :
      ( v116(VarCurr)
    <=> ( v117(VarCurr)
        | v122(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_5,axiom,(
    ! [VarCurr: state_type] :
      ( v122(VarCurr)
    <=> ( ( v90(VarCurr,2)
        <=> $false )
        & ( v90(VarCurr,1)
        <=> $true )
        & ( v90(VarCurr,0)
        <=> $true ) ) ) )).

tff(writeBinaryOperatorShiftedRanges_20,axiom,(
    ! [VarCurr: state_type] :
      ( v117(VarCurr)
    <=> ( v118(VarCurr)
        | v121(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_4,axiom,(
    ! [VarCurr: state_type] :
      ( v121(VarCurr)
    <=> ( ( v90(VarCurr,2)
        <=> $false )
        & ( v90(VarCurr,1)
        <=> $true )
        & ( v90(VarCurr,0)
        <=> $false ) ) ) )).

tff(writeBinaryOperatorShiftedRanges_19,axiom,(
    ! [VarCurr: state_type] :
      ( v118(VarCurr)
    <=> ( v119(VarCurr)
        | v120(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_3,axiom,(
    ! [VarCurr: state_type] :
      ( v120(VarCurr)
    <=> ( ( v90(VarCurr,2)
        <=> $false )
        & ( v90(VarCurr,1)
        <=> $false )
        & ( v90(VarCurr,0)
        <=> $true ) ) ) )).

tff(addBitVectorEqualityBitBlasted_2,axiom,(
    ! [VarCurr: state_type] :
      ( v119(VarCurr)
    <=> ( ( v90(VarCurr,2)
        <=> $false )
        & ( v90(VarCurr,1)
        <=> $false )
        & ( v90(VarCurr,0)
        <=> $false ) ) ) )).

tff(addBitVectorEqualityBitBlasted_1,axiom,(
    ! [VarCurr: state_type] :
      ( v111(VarCurr)
    <=> ( v26(VarCurr)
      <=> $true ) ) )).

tff(writeBinaryOperatorShiftedRanges_18,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v101(VarNext)
      <=> ( v102(VarNext)
          & v1(VarNext) ) ) ) )).

tff(writeUnaryOperator_12,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v102(VarNext)
      <=> v104(VarNext) ) ) )).

tff(addAssignment_5,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v104(VarNext)
      <=> v1(VarCurr) ) ) )).

tff(addAssignmentInitValue_4,axiom,(
    ~ v90(constB0,2) )).

tff(addAssignmentInitValue_3,axiom,(
    ~ v90(constB0,1) )).

tff(addAssignmentInitValue_2,axiom,(
    ~ v90(constB0,0) )).

tff(writeUnaryOperator_11,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v58(VarCurr)
    <=> v71(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_17,axiom,(
    ! [VarCurr: state_type] :
      ( v71(VarCurr)
    <=> ( v72(VarCurr)
        & v76(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_16,axiom,(
    ! [VarCurr: state_type] :
      ( v76(VarCurr)
    <=> ( v78(VarCurr)
        & v80(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_15,axiom,(
    ! [VarCurr: state_type] :
      ( v80(VarCurr)
    <=> ( v1(VarCurr)
        | v9(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_14,axiom,(
    ! [VarCurr: state_type] :
      ( v78(VarCurr)
    <=> ( v20(VarCurr)
        | v79(VarCurr) ) ) )).

tff(writeUnaryOperator_10,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v79(VarCurr)
    <=> v9(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_13,axiom,(
    ! [VarCurr: state_type] :
      ( v72(VarCurr)
    <=> ( v73(VarCurr)
        & v75(VarCurr) ) ) )).

tff(writeUnaryOperator_9,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v75(VarCurr)
    <=> v1(VarCurr) ) )).

tff(writeUnaryOperator_8,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v73(VarCurr)
    <=> v74(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted,axiom,(
    ! [VarCurr: state_type] :
      ( v74(VarCurr)
    <=> ( v26(VarCurr)
      <=> v60(VarCurr) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_3,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v63(VarNext)
       => ( v60(VarNext)
        <=> v60(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_3,axiom,(
    ! [VarNext: state_type] :
      ( v63(VarNext)
     => ( v60(VarNext)
      <=> v66(VarNext) ) ) )).

tff(addAssignment_4,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v66(VarNext)
      <=> v26(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_12,axiom,(
    ! [VarCurr: state_type] :
      ( v63(VarCurr)
    <=> ( $true
        & v64(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_11,axiom,(
    ! [VarCurr: state_type] :
      ( v64(VarCurr)
    <=> ( v9(VarCurr)
      <~> v1(VarCurr) ) ) )).

tff(writeUnaryOperator_7,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v24(VarCurr)
    <=> v53(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_10,axiom,(
    ! [VarCurr: state_type] :
      ( v53(VarCurr)
    <=> ( v54(VarCurr)
        & v29(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_9,axiom,(
    ! [VarCurr: state_type] :
      ( v54(VarCurr)
    <=> ( v55(VarCurr)
        & v38(VarCurr) ) ) )).

tff(writeUnaryOperator_6,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v55(VarCurr)
    <=> v56(VarCurr) ) )).

tff(writeUnaryOperator_5,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v56(VarCurr)
    <=> v26(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ $true
       => ( v29(VarNext)
        <=> v29(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_2,axiom,(
    ! [VarNext: state_type] :
      ( $true
     => ( v29(VarNext)
      <=> v46(VarNext) ) ) )).

tff(addAssignment_3,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v46(VarNext)
      <=> v44(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_8,axiom,(
    ! [VarCurr: state_type] :
      ( v44(VarCurr)
    <=> ( v47(VarCurr)
        | v48(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_7,axiom,(
    ! [VarCurr: state_type] :
      ( v48(VarCurr)
    <=> ( v49(VarCurr)
        & v31(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_6,axiom,(
    ! [VarCurr: state_type] :
      ( v49(VarCurr)
    <=> ( v38(VarCurr)
        & v26(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_5,axiom,(
    ! [VarCurr: state_type] :
      ( v47(VarCurr)
    <=> ( v37(VarCurr)
        & v29(VarCurr) ) ) )).

tff(addAssignmentInitValue_1,axiom,(
    ~ v29(constB0) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ $true
       => ( v31(VarNext)
        <=> v31(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_1,axiom,(
    ! [VarNext: state_type] :
      ( $true
     => ( v31(VarNext)
      <=> v36(VarNext) ) ) )).

tff(addAssignment_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v36(VarNext)
      <=> v34(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_4,axiom,(
    ! [VarCurr: state_type] :
      ( v34(VarCurr)
    <=> ( v37(VarCurr)
        & v31(VarCurr) ) ) )).

tff(writeUnaryOperator_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v37(VarCurr)
    <=> v38(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_3,axiom,(
    ! [VarCurr: state_type] :
      ( v38(VarCurr)
    <=> ( v39(VarCurr)
        & v9(VarCurr) ) ) )).

tff(writeUnaryOperator_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v39(VarCurr)
    <=> v1(VarCurr) ) )).

tff(addAssignmentInitValue,axiom,(
    v31(constB0) )).

tff(addAssignment_1,axiom,(
    ! [VarCurr: state_type] :
      ( v26(VarCurr)
    <=> $false ) )).

tff(writeUnaryOperator_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v4(VarCurr)
    <=> v18(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_2,axiom,(
    ! [VarCurr: state_type] :
      ( v18(VarCurr)
    <=> ( v19(VarCurr)
        & v22(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_1,axiom,(
    ! [VarCurr: state_type] :
      ( v22(VarCurr)
    <=> ( v1(VarCurr)
        | v6(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges,axiom,(
    ! [VarCurr: state_type] :
      ( v19(VarCurr)
    <=> ( v20(VarCurr)
        | v21(VarCurr) ) ) )).

tff(writeUnaryOperator_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v21(VarCurr)
    <=> v6(VarCurr) ) )).

tff(writeUnaryOperator,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v20(VarCurr)
    <=> v1(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ $true
       => ( v6(VarNext)
        <=> v6(VarCurr) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch,axiom,(
    ! [VarNext: state_type] :
      ( $true
     => ( v6(VarNext)
      <=> v13(VarNext) ) ) )).

tff(addAssignment,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v13(VarNext)
      <=> v9(VarCurr) ) ) )).

%------------------------------------------------------------------------------
