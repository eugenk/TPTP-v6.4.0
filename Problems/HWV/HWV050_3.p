%------------------------------------------------------------------------------
% File     : HWV050_3 : TPTP v6.4.0. Released v5.3.0.
% Domain   : Software Verification
% Problem  : Robot verification problem 12
% Version  : Especial.
%            Theorem formulation: Epr encoding mode bitblast
% English  :

% Refs     : [Kha11] Khasidashvili (2011), Email to Geoff Sutcliffe
% Source   : [Kha11]
% Names    : robot_p12b200eBb.tcf [Kha11]

% Status   : Unsatisfiable
% Rating   : 0.00 v5.3.0
% Syntax   : Number of formulae    : 1135 ( 616 unit; 307 type)
%            Number of atoms       : 1370 ( 201 equality)
%            Maximal formula depth :  203 (   2 average)
%            Number of connectives :  959 ( 417   ~; 542   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  103 (  98   >;   5   *;   0   +;   0  <<)
%            Number of predicates  :  379 ( 309 propositional; 0-2 arity)
%            Number of functors    :  205 ( 205 constant; 0-0 arity)
%            Number of variables   :  258 (   0 sgn; 258   !;   0   ?)
%                                         ( 258   :;   0  !>;   0  ?*)
%            Maximal term depth    :    1 (   1 average)
% SPC      : TF0_UNS_EQU_NAR

% Comments : Verification strategy is bmc1 with bound 200, Verification model
%            is Sequential word-level (with or w/o memories)
%------------------------------------------------------------------------------
tff(sort_def_4,type,(
    state_type: $tType )).

tff(sort_def_5,type,(
    address_type: $tType )).

tff(sort_def_6,type,(
    bitindex_type: $tType )).

tff(func_def_0,type,(
    constB0: state_type )).

tff(func_def_1,type,(
    bitIndex0: bitindex_type )).

tff(func_def_2,type,(
    bitIndex1: bitindex_type )).

tff(func_def_3,type,(
    bitIndex2: bitindex_type )).

tff(func_def_4,type,(
    constB1: state_type )).

tff(func_def_5,type,(
    constB2: state_type )).

tff(func_def_6,type,(
    constB3: state_type )).

tff(func_def_7,type,(
    constB4: state_type )).

tff(func_def_8,type,(
    constB5: state_type )).

tff(func_def_9,type,(
    constB6: state_type )).

tff(func_def_10,type,(
    constB7: state_type )).

tff(func_def_11,type,(
    constB8: state_type )).

tff(func_def_12,type,(
    constB9: state_type )).

tff(func_def_13,type,(
    constB10: state_type )).

tff(func_def_14,type,(
    constB11: state_type )).

tff(func_def_15,type,(
    constB12: state_type )).

tff(func_def_16,type,(
    constB13: state_type )).

tff(func_def_17,type,(
    constB14: state_type )).

tff(func_def_18,type,(
    constB15: state_type )).

tff(func_def_19,type,(
    constB16: state_type )).

tff(func_def_20,type,(
    constB17: state_type )).

tff(func_def_21,type,(
    constB18: state_type )).

tff(func_def_22,type,(
    constB19: state_type )).

tff(func_def_23,type,(
    constB20: state_type )).

tff(func_def_24,type,(
    constB21: state_type )).

tff(func_def_25,type,(
    constB22: state_type )).

tff(func_def_26,type,(
    constB23: state_type )).

tff(func_def_27,type,(
    constB24: state_type )).

tff(func_def_28,type,(
    constB25: state_type )).

tff(func_def_29,type,(
    constB26: state_type )).

tff(func_def_30,type,(
    constB27: state_type )).

tff(func_def_31,type,(
    constB28: state_type )).

tff(func_def_32,type,(
    constB29: state_type )).

tff(func_def_33,type,(
    constB30: state_type )).

tff(func_def_34,type,(
    constB31: state_type )).

tff(func_def_35,type,(
    constB32: state_type )).

tff(func_def_36,type,(
    constB33: state_type )).

tff(func_def_37,type,(
    constB34: state_type )).

tff(func_def_38,type,(
    constB35: state_type )).

tff(func_def_39,type,(
    constB36: state_type )).

tff(func_def_40,type,(
    constB37: state_type )).

tff(func_def_41,type,(
    constB38: state_type )).

tff(func_def_42,type,(
    constB39: state_type )).

tff(func_def_43,type,(
    constB40: state_type )).

tff(func_def_44,type,(
    constB41: state_type )).

tff(func_def_45,type,(
    constB42: state_type )).

tff(func_def_46,type,(
    constB43: state_type )).

tff(func_def_47,type,(
    constB44: state_type )).

tff(func_def_48,type,(
    constB45: state_type )).

tff(func_def_49,type,(
    constB46: state_type )).

tff(func_def_50,type,(
    constB47: state_type )).

tff(func_def_51,type,(
    constB48: state_type )).

tff(func_def_52,type,(
    constB49: state_type )).

tff(func_def_53,type,(
    constB50: state_type )).

tff(func_def_54,type,(
    constB51: state_type )).

tff(func_def_55,type,(
    constB52: state_type )).

tff(func_def_56,type,(
    constB53: state_type )).

tff(func_def_57,type,(
    constB54: state_type )).

tff(func_def_58,type,(
    constB55: state_type )).

tff(func_def_59,type,(
    constB56: state_type )).

tff(func_def_60,type,(
    constB57: state_type )).

tff(func_def_61,type,(
    constB58: state_type )).

tff(func_def_62,type,(
    constB59: state_type )).

tff(func_def_63,type,(
    constB60: state_type )).

tff(func_def_64,type,(
    constB61: state_type )).

tff(func_def_65,type,(
    constB62: state_type )).

tff(func_def_66,type,(
    constB63: state_type )).

tff(func_def_67,type,(
    constB64: state_type )).

tff(func_def_68,type,(
    constB65: state_type )).

tff(func_def_69,type,(
    constB66: state_type )).

tff(func_def_70,type,(
    constB67: state_type )).

tff(func_def_71,type,(
    constB68: state_type )).

tff(func_def_72,type,(
    constB69: state_type )).

tff(func_def_73,type,(
    constB70: state_type )).

tff(func_def_74,type,(
    constB71: state_type )).

tff(func_def_75,type,(
    constB72: state_type )).

tff(func_def_76,type,(
    constB73: state_type )).

tff(func_def_77,type,(
    constB74: state_type )).

tff(func_def_78,type,(
    constB75: state_type )).

tff(func_def_79,type,(
    constB76: state_type )).

tff(func_def_80,type,(
    constB77: state_type )).

tff(func_def_81,type,(
    constB78: state_type )).

tff(func_def_82,type,(
    constB79: state_type )).

tff(func_def_83,type,(
    constB80: state_type )).

tff(func_def_84,type,(
    constB81: state_type )).

tff(func_def_85,type,(
    constB82: state_type )).

tff(func_def_86,type,(
    constB83: state_type )).

tff(func_def_87,type,(
    constB84: state_type )).

tff(func_def_88,type,(
    constB85: state_type )).

tff(func_def_89,type,(
    constB86: state_type )).

tff(func_def_90,type,(
    constB87: state_type )).

tff(func_def_91,type,(
    constB88: state_type )).

tff(func_def_92,type,(
    constB89: state_type )).

tff(func_def_93,type,(
    constB90: state_type )).

tff(func_def_94,type,(
    constB91: state_type )).

tff(func_def_95,type,(
    constB92: state_type )).

tff(func_def_96,type,(
    constB93: state_type )).

tff(func_def_97,type,(
    constB94: state_type )).

tff(func_def_98,type,(
    constB95: state_type )).

tff(func_def_99,type,(
    constB96: state_type )).

tff(func_def_100,type,(
    constB97: state_type )).

tff(func_def_101,type,(
    constB98: state_type )).

tff(func_def_102,type,(
    constB99: state_type )).

tff(func_def_103,type,(
    constB100: state_type )).

tff(func_def_104,type,(
    constB101: state_type )).

tff(func_def_105,type,(
    constB102: state_type )).

tff(func_def_106,type,(
    constB103: state_type )).

tff(func_def_107,type,(
    constB104: state_type )).

tff(func_def_108,type,(
    constB105: state_type )).

tff(func_def_109,type,(
    constB106: state_type )).

tff(func_def_110,type,(
    constB107: state_type )).

tff(func_def_111,type,(
    constB108: state_type )).

tff(func_def_112,type,(
    constB109: state_type )).

tff(func_def_113,type,(
    constB110: state_type )).

tff(func_def_114,type,(
    constB111: state_type )).

tff(func_def_115,type,(
    constB112: state_type )).

tff(func_def_116,type,(
    constB113: state_type )).

tff(func_def_117,type,(
    constB114: state_type )).

tff(func_def_118,type,(
    constB115: state_type )).

tff(func_def_119,type,(
    constB116: state_type )).

tff(func_def_120,type,(
    constB117: state_type )).

tff(func_def_121,type,(
    constB118: state_type )).

tff(func_def_122,type,(
    constB119: state_type )).

tff(func_def_123,type,(
    constB120: state_type )).

tff(func_def_124,type,(
    constB121: state_type )).

tff(func_def_125,type,(
    constB122: state_type )).

tff(func_def_126,type,(
    constB123: state_type )).

tff(func_def_127,type,(
    constB124: state_type )).

tff(func_def_128,type,(
    constB125: state_type )).

tff(func_def_129,type,(
    constB126: state_type )).

tff(func_def_130,type,(
    constB127: state_type )).

tff(func_def_131,type,(
    constB128: state_type )).

tff(func_def_132,type,(
    constB129: state_type )).

tff(func_def_133,type,(
    constB130: state_type )).

tff(func_def_134,type,(
    constB131: state_type )).

tff(func_def_135,type,(
    constB132: state_type )).

tff(func_def_136,type,(
    constB133: state_type )).

tff(func_def_137,type,(
    constB134: state_type )).

tff(func_def_138,type,(
    constB135: state_type )).

tff(func_def_139,type,(
    constB136: state_type )).

tff(func_def_140,type,(
    constB137: state_type )).

tff(func_def_141,type,(
    constB138: state_type )).

tff(func_def_142,type,(
    constB139: state_type )).

tff(func_def_143,type,(
    constB140: state_type )).

tff(func_def_144,type,(
    constB141: state_type )).

tff(func_def_145,type,(
    constB142: state_type )).

tff(func_def_146,type,(
    constB143: state_type )).

tff(func_def_147,type,(
    constB144: state_type )).

tff(func_def_148,type,(
    constB145: state_type )).

tff(func_def_149,type,(
    constB146: state_type )).

tff(func_def_150,type,(
    constB147: state_type )).

tff(func_def_151,type,(
    constB148: state_type )).

tff(func_def_152,type,(
    constB149: state_type )).

tff(func_def_153,type,(
    constB150: state_type )).

tff(func_def_154,type,(
    constB151: state_type )).

tff(func_def_155,type,(
    constB152: state_type )).

tff(func_def_156,type,(
    constB153: state_type )).

tff(func_def_157,type,(
    constB154: state_type )).

tff(func_def_158,type,(
    constB155: state_type )).

tff(func_def_159,type,(
    constB156: state_type )).

tff(func_def_160,type,(
    constB157: state_type )).

tff(func_def_161,type,(
    constB158: state_type )).

tff(func_def_162,type,(
    constB159: state_type )).

tff(func_def_163,type,(
    constB160: state_type )).

tff(func_def_164,type,(
    constB161: state_type )).

tff(func_def_165,type,(
    constB162: state_type )).

tff(func_def_166,type,(
    constB163: state_type )).

tff(func_def_167,type,(
    constB164: state_type )).

tff(func_def_168,type,(
    constB165: state_type )).

tff(func_def_169,type,(
    constB166: state_type )).

tff(func_def_170,type,(
    constB167: state_type )).

tff(func_def_171,type,(
    constB168: state_type )).

tff(func_def_172,type,(
    constB169: state_type )).

tff(func_def_173,type,(
    constB170: state_type )).

tff(func_def_174,type,(
    constB171: state_type )).

tff(func_def_175,type,(
    constB172: state_type )).

tff(func_def_176,type,(
    constB173: state_type )).

tff(func_def_177,type,(
    constB174: state_type )).

tff(func_def_178,type,(
    constB175: state_type )).

tff(func_def_179,type,(
    constB176: state_type )).

tff(func_def_180,type,(
    constB177: state_type )).

tff(func_def_181,type,(
    constB178: state_type )).

tff(func_def_182,type,(
    constB179: state_type )).

tff(func_def_183,type,(
    constB180: state_type )).

tff(func_def_184,type,(
    constB181: state_type )).

tff(func_def_185,type,(
    constB182: state_type )).

tff(func_def_186,type,(
    constB183: state_type )).

tff(func_def_187,type,(
    constB184: state_type )).

tff(func_def_188,type,(
    constB185: state_type )).

tff(func_def_189,type,(
    constB186: state_type )).

tff(func_def_190,type,(
    constB187: state_type )).

tff(func_def_191,type,(
    constB188: state_type )).

tff(func_def_192,type,(
    constB189: state_type )).

tff(func_def_193,type,(
    constB190: state_type )).

tff(func_def_194,type,(
    constB191: state_type )).

tff(func_def_195,type,(
    constB192: state_type )).

tff(func_def_196,type,(
    constB193: state_type )).

tff(func_def_197,type,(
    constB194: state_type )).

tff(func_def_198,type,(
    constB195: state_type )).

tff(func_def_199,type,(
    constB196: state_type )).

tff(func_def_200,type,(
    constB197: state_type )).

tff(func_def_201,type,(
    constB198: state_type )).

tff(func_def_202,type,(
    constB199: state_type )).

tff(func_def_203,type,(
    constB200: state_type )).

tff(func_def_204,type,(
    sK0_VarCurr: state_type )).

tff(pred_def_1,type,(
    v9: state_type > $o )).

tff(pred_def_2,type,(
    v13: state_type > $o )).

tff(pred_def_3,type,(
    nextState: ( state_type * state_type ) > $o )).

tff(pred_def_4,type,(
    v6: state_type > $o )).

tff(pred_def_5,type,(
    v1: state_type > $o )).

tff(pred_def_6,type,(
    v20: state_type > $o )).

tff(pred_def_7,type,(
    v21: state_type > $o )).

tff(pred_def_8,type,(
    v19: state_type > $o )).

tff(pred_def_9,type,(
    v22: state_type > $o )).

tff(pred_def_10,type,(
    v18: state_type > $o )).

tff(pred_def_11,type,(
    v4: state_type > $o )).

tff(pred_def_12,type,(
    v26: state_type > $o )).

tff(pred_def_13,type,(
    v31: state_type > $o )).

tff(pred_def_14,type,(
    v39: state_type > $o )).

tff(pred_def_15,type,(
    v38: state_type > $o )).

tff(pred_def_16,type,(
    v37: state_type > $o )).

tff(pred_def_17,type,(
    v34: state_type > $o )).

tff(pred_def_18,type,(
    v36: state_type > $o )).

tff(pred_def_19,type,(
    v29: state_type > $o )).

tff(pred_def_20,type,(
    v47: state_type > $o )).

tff(pred_def_21,type,(
    v49: state_type > $o )).

tff(pred_def_22,type,(
    v48: state_type > $o )).

tff(pred_def_23,type,(
    v44: state_type > $o )).

tff(pred_def_24,type,(
    v46: state_type > $o )).

tff(pred_def_25,type,(
    v56: state_type > $o )).

tff(pred_def_26,type,(
    v55: state_type > $o )).

tff(pred_def_27,type,(
    v54: state_type > $o )).

tff(pred_def_28,type,(
    v53: state_type > $o )).

tff(pred_def_29,type,(
    v24: state_type > $o )).

tff(pred_def_30,type,(
    v64: state_type > $o )).

tff(pred_def_31,type,(
    v63: state_type > $o )).

tff(pred_def_32,type,(
    v66: state_type > $o )).

tff(pred_def_33,type,(
    v60: state_type > $o )).

tff(pred_def_34,type,(
    v74: state_type > $o )).

tff(pred_def_35,type,(
    v73: state_type > $o )).

tff(pred_def_36,type,(
    v75: state_type > $o )).

tff(pred_def_37,type,(
    v72: state_type > $o )).

tff(pred_def_38,type,(
    v79: state_type > $o )).

tff(pred_def_39,type,(
    v78: state_type > $o )).

tff(pred_def_40,type,(
    v80: state_type > $o )).

tff(pred_def_41,type,(
    v76: state_type > $o )).

tff(pred_def_42,type,(
    v71: state_type > $o )).

tff(pred_def_43,type,(
    v58: state_type > $o )).

tff(pred_def_44,type,(
    v84: ( state_type * bitindex_type ) > $o )).

tff(pred_def_45,type,(
    v98: state_type > $o )).

tff(pred_def_46,type,(
    v96: state_type > $o )).

tff(pred_def_47,type,(
    v95: state_type > $o )).

tff(pred_def_48,type,(
    v105: state_type > $o )).

tff(pred_def_49,type,(
    v113: state_type > $o )).

tff(pred_def_50,type,(
    v114: state_type > $o )).

tff(pred_def_51,type,(
    v112: state_type > $o )).

tff(pred_def_52,type,(
    v115: state_type > $o )).

tff(pred_def_53,type,(
    v111: state_type > $o )).

tff(pred_def_54,type,(
    v116: state_type > $o )).

tff(pred_def_55,type,(
    v110: state_type > $o )).

tff(pred_def_56,type,(
    v117: state_type > $o )).

tff(pred_def_57,type,(
    v109: state_type > $o )).

tff(pred_def_58,type,(
    v118: state_type > $o )).

tff(pred_def_59,type,(
    v108: state_type > $o )).

tff(pred_def_60,type,(
    v119: state_type > $o )).

tff(pred_def_61,type,(
    v107: state_type > $o )).

tff(pred_def_62,type,(
    v120: state_type > $o )).

tff(pred_def_63,type,(
    v106: state_type > $o )).

tff(pred_def_64,type,(
    v102: state_type > $o )).

tff(pred_def_65,type,(
    v104: state_type > $o )).

tff(pred_def_66,type,(
    v94: state_type > $o )).

tff(pred_def_67,type,(
    v124: ( state_type * bitindex_type ) > $o )).

tff(pred_def_68,type,(
    v121: ( state_type * bitindex_type ) > $o )).

tff(pred_def_69,type,(
    v123: ( state_type * bitindex_type ) > $o )).

tff(pred_def_70,type,(
    undeclared: $o )).

tff(pred_def_71,type,(
    v132: state_type > $o )).

tff(pred_def_72,type,(
    v141: state_type > $o )).

tff(pred_def_73,type,(
    v140: state_type > $o )).

tff(pred_def_74,type,(
    v139: state_type > $o )).

tff(pred_def_75,type,(
    v138: state_type > $o )).

tff(pred_def_76,type,(
    v143: state_type > $o )).

tff(pred_def_77,type,(
    v145: state_type > $o )).

tff(pred_def_78,type,(
    v144: state_type > $o )).

tff(pred_def_79,type,(
    v142: state_type > $o )).

tff(pred_def_80,type,(
    v135: state_type > $o )).

tff(pred_def_81,type,(
    v137: state_type > $o )).

tff(pred_def_82,type,(
    v130: state_type > $o )).

tff(pred_def_83,type,(
    v154: state_type > $o )).

tff(pred_def_84,type,(
    v153: state_type > $o )).

tff(pred_def_85,type,(
    v155: state_type > $o )).

tff(pred_def_86,type,(
    v150: state_type > $o )).

tff(pred_def_87,type,(
    v152: state_type > $o )).

tff(pred_def_88,type,(
    v128: state_type > $o )).

tff(pred_def_89,type,(
    v163: state_type > $o )).

tff(pred_def_90,type,(
    v164: state_type > $o )).

tff(pred_def_91,type,(
    v160: state_type > $o )).

tff(pred_def_92,type,(
    v162: state_type > $o )).

tff(pred_def_93,type,(
    v172: state_type > $o )).

tff(pred_def_94,type,(
    v171: state_type > $o )).

tff(pred_def_95,type,(
    v170: state_type > $o )).

tff(pred_def_96,type,(
    v169: state_type > $o )).

tff(pred_def_97,type,(
    v168: state_type > $o )).

tff(pred_def_98,type,(
    v82: state_type > $o )).

tff(pred_def_99,type,(
    reachableState: state_type > $o )).

tff(addAssignment_11_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v13(VarNext)
      | v9(VarCurr) ) )).

tff(addAssignment_11_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v9(VarCurr)
      | v13(VarNext) ) )).

tff(writeBinaryOperatorShiftedRanges_44_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( v13(VarCurr)
      | v19(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_44_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v1(VarCurr)
      | v19(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_1_1_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v13(VarCurr)
      | v22(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_1_1_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1(VarCurr)
      | v22(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_2_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v19(VarCurr)
      | ~ v22(VarCurr)
      | v18(VarCurr) ) )).

tff(addAssignment_1_1_1,axiom,(
    ! [VarCurr: state_type] : ~ v26(VarCurr) )).

tff(addAssignmentInitValue_8_1,axiom,(
    v36(constB0) )).

tff(writeBinaryOperatorShiftedRanges_4_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v34(VarCurr)
      | v36(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_4_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v34(VarCurr)
      | ~ v141(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_4_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v36(VarCurr)
      | v141(VarCurr)
      | v34(VarCurr) ) )).

tff(addAssignment_2_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v36(VarNext)
      | v34(VarCurr) ) )).

tff(addAssignment_2_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v34(VarCurr)
      | v36(VarNext) ) )).

tff(addAssignmentInitValue_1_1_1_1,axiom,(
    ~ v46(constB0) )).

tff(writeBinaryOperatorShiftedRanges_5_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v47(VarCurr)
      | v46(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_5_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v47(VarCurr)
      | ~ v141(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_5_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v46(VarCurr)
      | v141(VarCurr)
      | v47(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_7_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v48(VarCurr)
      | v36(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_7_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v48(VarCurr)
      | v54(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_7_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v36(VarCurr)
      | ~ v54(VarCurr)
      | v48(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_8_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v44(VarCurr)
      | v47(VarCurr)
      | v48(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_8_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v47(VarCurr)
      | v44(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_8_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v48(VarCurr)
      | v44(VarCurr) ) )).

tff(addAssignment_3_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v46(VarNext)
      | v44(VarCurr) ) )).

tff(addAssignment_3_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v44(VarCurr)
      | v46(VarNext) ) )).

tff(writeBinaryOperatorShiftedRanges_9_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v54(VarCurr)
      | v26(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_9_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v54(VarCurr)
      | v141(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_9_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v26(VarCurr)
      | ~ v141(VarCurr)
      | v54(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_10_1_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v46(VarCurr)
      | ~ v54(VarCurr)
      | v53(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_11_1_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v64(VarCurr)
      | v9(VarCurr)
      | v1(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_11_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v64(VarCurr)
      | ~ v9(VarCurr)
      | ~ v1(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_11_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v9(VarCurr)
      | v1(VarCurr)
      | v64(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_11_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1(VarCurr)
      | v9(VarCurr)
      | v64(VarCurr) ) )).

tff(addAssignment_4_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v66(VarNext)
      | v26(VarCurr) ) )).

tff(addAssignment_4_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v26(VarCurr)
      | v66(VarNext) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_3_1_2,axiom,(
    ! [VarNext: state_type] :
      ( ~ v64(VarNext)
      | ~ v60(VarNext)
      | v66(VarNext) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_3_1_1,axiom,(
    ! [VarNext: state_type] :
      ( ~ v64(VarNext)
      | ~ v66(VarNext)
      | v60(VarNext) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_3_1_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | v64(VarNext)
      | ~ v60(VarNext)
      | v60(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_3_1_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | v64(VarNext)
      | ~ v60(VarCurr)
      | v60(VarNext) ) )).

tff(addBitVectorEqualityBitBlasted_11_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v74(VarCurr)
      | ~ v26(VarCurr)
      | v60(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_11_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v74(VarCurr)
      | ~ v60(VarCurr)
      | v26(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_13_1_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v1(VarCurr)
      | v74(VarCurr)
      | v72(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_14_1_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( v9(VarCurr)
      | v78(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_14_1_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v1(VarCurr)
      | v78(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_15_1_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v9(VarCurr)
      | v80(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_15_1_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1(VarCurr)
      | v80(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_16_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v78(VarCurr)
      | ~ v80(VarCurr)
      | v76(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_17_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v72(VarCurr)
      | ~ v76(VarCurr)
      | v71(VarCurr) ) )).

tff(addAssignmentInitValue_2_1_1,axiom,(
    ~ v84(constB0,bitIndex0) )).

tff(addAssignmentInitValue_3_1_1,axiom,(
    ~ v84(constB0,bitIndex1) )).

tff(addAssignmentInitValue_4_1_1,axiom,(
    ~ v84(constB0,bitIndex2) )).

tff(addAssignment_5_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v98(VarNext)
      | v1(VarCurr) ) )).

tff(addAssignment_5_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v1(VarCurr)
      | v98(VarNext) ) )).

tff(writeUnaryOperator_12_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | v96(VarNext)
      | v98(VarNext) ) )).

tff(writeUnaryOperator_12_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v98(VarNext)
      | ~ v96(VarNext) ) )).

tff(writeBinaryOperatorShiftedRanges_18_1_3,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v95(VarNext)
      | v96(VarNext) ) )).

tff(writeBinaryOperatorShiftedRanges_18_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v95(VarNext)
      | v1(VarNext) ) )).

tff(writeBinaryOperatorShiftedRanges_18_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v96(VarNext)
      | ~ v1(VarNext)
      | v95(VarNext) ) )).

tff(addBitVectorEqualityBitBlasted_2_1_1_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v113(VarCurr)
      | ~ v84(VarCurr,bitIndex0) ) )).

tff(addBitVectorEqualityBitBlasted_2_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v113(VarCurr)
      | ~ v84(VarCurr,bitIndex1) ) )).

tff(addBitVectorEqualityBitBlasted_2_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v113(VarCurr)
      | ~ v84(VarCurr,bitIndex2) ) )).

tff(addBitVectorEqualityBitBlasted_2_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v84(VarCurr,bitIndex0)
      | v84(VarCurr,bitIndex1)
      | v84(VarCurr,bitIndex2)
      | v113(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_19_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v112(VarCurr)
      | v113(VarCurr)
      | v145(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_19_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v113(VarCurr)
      | v112(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_19_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v145(VarCurr)
      | v112(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_4_1_1_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v115(VarCurr)
      | ~ v84(VarCurr,bitIndex0) ) )).

tff(addBitVectorEqualityBitBlasted_4_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v115(VarCurr)
      | ~ v84(VarCurr,bitIndex2) ) )).

tff(addBitVectorEqualityBitBlasted_4_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v115(VarCurr)
      | v84(VarCurr,bitIndex1) ) )).

tff(addBitVectorEqualityBitBlasted_4_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v84(VarCurr,bitIndex0)
      | v84(VarCurr,bitIndex2)
      | ~ v84(VarCurr,bitIndex1)
      | v115(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_20_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v111(VarCurr)
      | v112(VarCurr)
      | v115(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_20_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v112(VarCurr)
      | v111(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_20_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v115(VarCurr)
      | v111(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_5_1_1_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v116(VarCurr)
      | ~ v84(VarCurr,bitIndex2) ) )).

tff(addBitVectorEqualityBitBlasted_5_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v116(VarCurr)
      | v84(VarCurr,bitIndex0) ) )).

tff(addBitVectorEqualityBitBlasted_5_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v116(VarCurr)
      | v84(VarCurr,bitIndex1) ) )).

tff(addBitVectorEqualityBitBlasted_5_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v84(VarCurr,bitIndex2)
      | ~ v84(VarCurr,bitIndex0)
      | ~ v84(VarCurr,bitIndex1)
      | v116(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_21_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v110(VarCurr)
      | v111(VarCurr)
      | v116(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_21_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v111(VarCurr)
      | v110(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_21_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v116(VarCurr)
      | v110(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_22_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v109(VarCurr)
      | v110(VarCurr)
      | v172(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_22_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v110(VarCurr)
      | v109(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_22_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v172(VarCurr)
      | v109(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_7_1_1_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v118(VarCurr)
      | ~ v84(VarCurr,bitIndex1) ) )).

tff(addBitVectorEqualityBitBlasted_7_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v118(VarCurr)
      | v84(VarCurr,bitIndex0) ) )).

tff(addBitVectorEqualityBitBlasted_7_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v118(VarCurr)
      | v84(VarCurr,bitIndex2) ) )).

tff(addBitVectorEqualityBitBlasted_7_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v84(VarCurr,bitIndex1)
      | ~ v84(VarCurr,bitIndex0)
      | ~ v84(VarCurr,bitIndex2)
      | v118(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_23_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v108(VarCurr)
      | v109(VarCurr)
      | v118(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_23_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v109(VarCurr)
      | v108(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_23_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v118(VarCurr)
      | v108(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_8_1_1_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v119(VarCurr)
      | ~ v84(VarCurr,bitIndex0) ) )).

tff(addBitVectorEqualityBitBlasted_8_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v119(VarCurr)
      | v84(VarCurr,bitIndex1) ) )).

tff(addBitVectorEqualityBitBlasted_8_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v119(VarCurr)
      | v84(VarCurr,bitIndex2) ) )).

tff(addBitVectorEqualityBitBlasted_8_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v84(VarCurr,bitIndex0)
      | ~ v84(VarCurr,bitIndex1)
      | ~ v84(VarCurr,bitIndex2)
      | v119(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_24_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v107(VarCurr)
      | v108(VarCurr)
      | v119(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_24_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v108(VarCurr)
      | v107(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_24_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v119(VarCurr)
      | v107(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_25_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v106(VarCurr)
      | ~ v26(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_25_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v106(VarCurr)
      | v107(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_25_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v26(VarCurr)
      | ~ v107(VarCurr)
      | v106(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_26_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v102(VarCurr)
      | v26(VarCurr)
      | v106(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_26_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v26(VarCurr)
      | v102(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_26_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v106(VarCurr)
      | v102(VarCurr) ) )).

tff(addAssignment_6_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v104(VarNext)
      | v102(VarCurr) ) )).

tff(addAssignment_6_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v102(VarCurr)
      | v104(VarNext) ) )).

tff(writeBinaryOperatorShiftedRanges_27_1_3,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v94(VarNext)
      | v95(VarNext) ) )).

tff(writeBinaryOperatorShiftedRanges_27_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v94(VarNext)
      | v104(VarNext) ) )).

tff(writeBinaryOperatorShiftedRanges_27_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v95(VarNext)
      | ~ v104(VarNext)
      | v94(VarNext) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_2_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v113(VarCurr)
      | ~ v124(VarCurr,bitIndex2) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_2_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v113(VarCurr)
      | ~ v124(VarCurr,bitIndex1) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_2_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v113(VarCurr)
      | v124(VarCurr,bitIndex0) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_2_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v145(VarCurr)
      | ~ v124(VarCurr,bitIndex2) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_2_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v145(VarCurr)
      | v124(VarCurr,bitIndex1) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_2_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v145(VarCurr)
      | ~ v124(VarCurr,bitIndex0) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v115(VarCurr)
      | ~ v124(VarCurr,bitIndex2) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v115(VarCurr)
      | v124(VarCurr,bitIndex1) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v115(VarCurr)
      | v124(VarCurr,bitIndex0) ) )).

tff(addParallelCaseBooleanConditionEqualRanges3_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v116(VarCurr)
      | v124(VarCurr,bitIndex2) ) )).

tff(addParallelCaseBooleanConditionEqualRanges3_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v116(VarCurr)
      | ~ v124(VarCurr,bitIndex1) ) )).

tff(addParallelCaseBooleanConditionEqualRanges3_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v116(VarCurr)
      | ~ v124(VarCurr,bitIndex0) ) )).

tff(addParallelCaseBooleanConditionEqualRanges4_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v172(VarCurr)
      | v124(VarCurr,bitIndex2) ) )).

tff(addParallelCaseBooleanConditionEqualRanges4_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v172(VarCurr)
      | ~ v124(VarCurr,bitIndex1) ) )).

tff(addParallelCaseBooleanConditionEqualRanges4_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v172(VarCurr)
      | v124(VarCurr,bitIndex0) ) )).

tff(addParallelCaseBooleanConditionEqualRanges5_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v118(VarCurr)
      | v124(VarCurr,bitIndex2) ) )).

tff(addParallelCaseBooleanConditionEqualRanges5_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v118(VarCurr)
      | v124(VarCurr,bitIndex1) ) )).

tff(addParallelCaseBooleanConditionEqualRanges5_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v118(VarCurr)
      | ~ v124(VarCurr,bitIndex0) ) )).

tff(addParallelCaseBooleanConditionEqualRanges6_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( v113(VarCurr)
      | v145(VarCurr)
      | v115(VarCurr)
      | v116(VarCurr)
      | v172(VarCurr)
      | v118(VarCurr)
      | ~ v124(VarCurr,bitIndex2) ) )).

tff(addParallelCaseBooleanConditionEqualRanges6_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( v113(VarCurr)
      | v145(VarCurr)
      | v115(VarCurr)
      | v116(VarCurr)
      | v172(VarCurr)
      | v118(VarCurr)
      | ~ v124(VarCurr,bitIndex1) ) )).

tff(addParallelCaseBooleanConditionEqualRanges6_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v113(VarCurr)
      | v145(VarCurr)
      | v115(VarCurr)
      | v116(VarCurr)
      | v172(VarCurr)
      | v118(VarCurr)
      | v124(VarCurr,bitIndex0) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_1_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v26(VarCurr)
      | ~ v121(VarCurr,bitIndex2) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_1_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v26(VarCurr)
      | ~ v121(VarCurr,bitIndex1) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_1_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v26(VarCurr)
      | ~ v121(VarCurr,bitIndex0) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_1_1_1_6,axiom,(
    ! [VarCurr: state_type] :
      ( v26(VarCurr)
      | ~ v121(VarCurr,bitIndex2)
      | v124(VarCurr,bitIndex2) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_1_1_1_5,axiom,(
    ! [VarCurr: state_type] :
      ( v26(VarCurr)
      | ~ v124(VarCurr,bitIndex2)
      | v121(VarCurr,bitIndex2) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_1_1_1_4,axiom,(
    ! [VarCurr: state_type] :
      ( v26(VarCurr)
      | ~ v121(VarCurr,bitIndex1)
      | v124(VarCurr,bitIndex1) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_1_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( v26(VarCurr)
      | ~ v124(VarCurr,bitIndex1)
      | v121(VarCurr,bitIndex1) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_1_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( v26(VarCurr)
      | ~ v121(VarCurr,bitIndex0)
      | v124(VarCurr,bitIndex0) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_1_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v26(VarCurr)
      | ~ v124(VarCurr,bitIndex0)
      | v121(VarCurr,bitIndex0) ) )).

tff(addAssignment_7_1_6,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v123(VarNext,bitIndex2)
      | v121(VarCurr,bitIndex2) ) )).

tff(addAssignment_7_1_5,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v121(VarCurr,bitIndex2)
      | v123(VarNext,bitIndex2) ) )).

tff(addAssignment_7_1_4,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v123(VarNext,bitIndex1)
      | v121(VarCurr,bitIndex1) ) )).

tff(addAssignment_7_1_3,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v121(VarCurr,bitIndex1)
      | v123(VarNext,bitIndex1) ) )).

tff(addAssignment_7_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v123(VarNext,bitIndex0)
      | v121(VarCurr,bitIndex0) ) )).

tff(addAssignment_7_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v121(VarCurr,bitIndex0)
      | v123(VarNext,bitIndex0) ) )).

tff(addCaseBooleanConditionEqualRanges0_1_6,axiom,(
    ! [VarNext: state_type] :
      ( ~ v94(VarNext)
      | ~ v84(VarNext,bitIndex2)
      | v123(VarNext,bitIndex2) ) )).

tff(addCaseBooleanConditionEqualRanges0_1_5,axiom,(
    ! [VarNext: state_type] :
      ( ~ v94(VarNext)
      | ~ v123(VarNext,bitIndex2)
      | v84(VarNext,bitIndex2) ) )).

tff(addCaseBooleanConditionEqualRanges0_1_4,axiom,(
    ! [VarNext: state_type] :
      ( ~ v94(VarNext)
      | ~ v84(VarNext,bitIndex1)
      | v123(VarNext,bitIndex1) ) )).

tff(addCaseBooleanConditionEqualRanges0_1_3,axiom,(
    ! [VarNext: state_type] :
      ( ~ v94(VarNext)
      | ~ v123(VarNext,bitIndex1)
      | v84(VarNext,bitIndex1) ) )).

tff(addCaseBooleanConditionEqualRanges0_1_2,axiom,(
    ! [VarNext: state_type] :
      ( ~ v94(VarNext)
      | ~ v84(VarNext,bitIndex0)
      | v123(VarNext,bitIndex0) ) )).

tff(addCaseBooleanConditionEqualRanges0_1_1,axiom,(
    ! [VarNext: state_type] :
      ( ~ v94(VarNext)
      | ~ v123(VarNext,bitIndex0)
      | v84(VarNext,bitIndex0) ) )).

tff(addCaseBooleanConditionEqualRanges1_1_6,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | v94(VarNext)
      | ~ v84(VarNext,bitIndex2)
      | v84(VarCurr,bitIndex2) ) )).

tff(addCaseBooleanConditionEqualRanges1_1_5,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | v94(VarNext)
      | ~ v84(VarCurr,bitIndex2)
      | v84(VarNext,bitIndex2) ) )).

tff(addCaseBooleanConditionEqualRanges1_1_4,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | v94(VarNext)
      | ~ v84(VarNext,bitIndex1)
      | v84(VarCurr,bitIndex1) ) )).

tff(addCaseBooleanConditionEqualRanges1_1_3,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | v94(VarNext)
      | ~ v84(VarCurr,bitIndex1)
      | v84(VarNext,bitIndex1) ) )).

tff(addCaseBooleanConditionEqualRanges1_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | v94(VarNext)
      | ~ v84(VarNext,bitIndex0)
      | v84(VarCurr,bitIndex0) ) )).

tff(addCaseBooleanConditionEqualRanges1_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | v94(VarNext)
      | ~ v84(VarCurr,bitIndex0)
      | v84(VarNext,bitIndex0) ) )).

tff(addAssignmentInitValue_5_1_1_1,axiom,(
    ~ v137(constB0) )).

tff(writeBinaryOperatorShiftedRanges_28_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v141(VarCurr)
      | v9(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_28_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v141(VarCurr)
      | ~ v1(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_28_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v9(VarCurr)
      | v1(VarCurr)
      | v141(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_29_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v140(VarCurr)
      | v26(VarCurr)
      | v141(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_29_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v26(VarCurr)
      | v140(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_29_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v141(VarCurr)
      | v140(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_30_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v138(VarCurr)
      | ~ v140(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_30_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v138(VarCurr)
      | v137(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_30_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v140(VarCurr)
      | ~ v137(VarCurr)
      | v138(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_9_1_1_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v145(VarCurr)
      | ~ v84(VarCurr,bitIndex1) ) )).

tff(addBitVectorEqualityBitBlasted_9_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v145(VarCurr)
      | ~ v84(VarCurr,bitIndex2) ) )).

tff(addBitVectorEqualityBitBlasted_9_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v145(VarCurr)
      | v84(VarCurr,bitIndex0) ) )).

tff(addBitVectorEqualityBitBlasted_9_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v84(VarCurr,bitIndex1)
      | v84(VarCurr,bitIndex2)
      | ~ v84(VarCurr,bitIndex0)
      | v145(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_31_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v144(VarCurr)
      | v141(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_31_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v144(VarCurr)
      | v145(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_31_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v141(VarCurr)
      | ~ v145(VarCurr)
      | v144(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_32_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v142(VarCurr)
      | ~ v26(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_32_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v142(VarCurr)
      | v144(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_32_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v26(VarCurr)
      | ~ v144(VarCurr)
      | v142(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_33_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v135(VarCurr)
      | v138(VarCurr)
      | v142(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_33_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v138(VarCurr)
      | v135(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_33_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v142(VarCurr)
      | v135(VarCurr) ) )).

tff(addAssignment_8_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v137(VarNext)
      | v135(VarCurr) ) )).

tff(addAssignment_8_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v135(VarCurr)
      | v137(VarNext) ) )).

tff(addAssignmentInitValue_6_1_1_1,axiom,(
    ~ v152(constB0) )).

tff(writeBinaryOperatorShiftedRanges_34_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v154(VarCurr)
      | ~ v26(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_34_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v154(VarCurr)
      | v141(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_34_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v26(VarCurr)
      | ~ v141(VarCurr)
      | v154(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_35_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v153(VarCurr)
      | v137(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_35_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v153(VarCurr)
      | v154(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_35_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v137(VarCurr)
      | ~ v154(VarCurr)
      | v153(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_36_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v155(VarCurr)
      | ~ v140(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_36_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v155(VarCurr)
      | v152(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_36_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v140(VarCurr)
      | ~ v152(VarCurr)
      | v155(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_37_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v150(VarCurr)
      | v153(VarCurr)
      | v155(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_37_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v153(VarCurr)
      | v150(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_37_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v155(VarCurr)
      | v150(VarCurr) ) )).

tff(addAssignment_9_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v152(VarNext)
      | v150(VarCurr) ) )).

tff(addAssignment_9_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v150(VarCurr)
      | v152(VarNext) ) )).

tff(addAssignmentInitValue_7_1_1_1,axiom,(
    ~ v162(constB0) )).

tff(writeBinaryOperatorShiftedRanges_38_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v163(VarCurr)
      | ~ v140(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_38_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v163(VarCurr)
      | v162(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_38_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v140(VarCurr)
      | ~ v162(VarCurr)
      | v163(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_39_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v164(VarCurr)
      | v154(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_39_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v164(VarCurr)
      | v152(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_39_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v154(VarCurr)
      | ~ v152(VarCurr)
      | v164(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_40_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v160(VarCurr)
      | v163(VarCurr)
      | v164(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_40_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v163(VarCurr)
      | v160(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_40_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v164(VarCurr)
      | v160(VarCurr) ) )).

tff(addAssignment_10_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v162(VarNext)
      | v160(VarCurr) ) )).

tff(addAssignment_10_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | ~ v160(VarCurr)
      | v162(VarNext) ) )).

tff(addBitVectorEqualityBitBlasted_10_1_1_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v172(VarCurr)
      | ~ v84(VarCurr,bitIndex0) ) )).

tff(addBitVectorEqualityBitBlasted_10_1_1_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v172(VarCurr)
      | ~ v84(VarCurr,bitIndex1) ) )).

tff(addBitVectorEqualityBitBlasted_10_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v172(VarCurr)
      | v84(VarCurr,bitIndex2) ) )).

tff(addBitVectorEqualityBitBlasted_10_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v84(VarCurr,bitIndex0)
      | v84(VarCurr,bitIndex1)
      | ~ v84(VarCurr,bitIndex2)
      | v172(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_41_1_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v170(VarCurr)
      | v141(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_41_1_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v170(VarCurr)
      | ~ v172(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_42_1_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v169(VarCurr)
      | ~ v26(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_42_1_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v169(VarCurr)
      | v170(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_43_1_1_1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v168(VarCurr)
      | v162(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_43_1_1_1_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v168(VarCurr)
      | v169(VarCurr) ) )).

tff(addGlobalAssumption_3_1_1,axiom,(
    ! [VarCurr: state_type] : ~ v18(VarCurr) )).

tff(addGlobalAssumption_1_1_1_1,axiom,(
    ! [VarCurr: state_type] : ~ v71(VarCurr) )).

tff(addGlobalAssumption_2_1_1_1,axiom,(
    ! [VarCurr: state_type] : ~ v53(VarCurr) )).

tff(addAssertion_1_1_2,negated_conjecture,(
    reachableState(sK0_VarCurr) )).

tff(addAssertion_1_1_1,negated_conjecture,(
    v168(sK0_VarCurr) )).

tff(reachableStateAxiom_203,axiom,(
    reachableState(constB0) )).

tff(reachableStateAxiom_1_1,axiom,(
    reachableState(constB1) )).

tff(reachableStateAxiom_2_1,axiom,(
    reachableState(constB2) )).

tff(reachableStateAxiom_3_1,axiom,(
    reachableState(constB3) )).

tff(reachableStateAxiom_4_1,axiom,(
    reachableState(constB4) )).

tff(reachableStateAxiom_5_1,axiom,(
    reachableState(constB5) )).

tff(reachableStateAxiom_6_1,axiom,(
    reachableState(constB6) )).

tff(reachableStateAxiom_7_1,axiom,(
    reachableState(constB7) )).

tff(reachableStateAxiom_8_1,axiom,(
    reachableState(constB8) )).

tff(reachableStateAxiom_9_1,axiom,(
    reachableState(constB9) )).

tff(reachableStateAxiom_10_1,axiom,(
    reachableState(constB10) )).

tff(reachableStateAxiom_11_1,axiom,(
    reachableState(constB11) )).

tff(reachableStateAxiom_12_1,axiom,(
    reachableState(constB12) )).

tff(reachableStateAxiom_13_1,axiom,(
    reachableState(constB13) )).

tff(reachableStateAxiom_14_1,axiom,(
    reachableState(constB14) )).

tff(reachableStateAxiom_15_1,axiom,(
    reachableState(constB15) )).

tff(reachableStateAxiom_16_1,axiom,(
    reachableState(constB16) )).

tff(reachableStateAxiom_17_1,axiom,(
    reachableState(constB17) )).

tff(reachableStateAxiom_18_1,axiom,(
    reachableState(constB18) )).

tff(reachableStateAxiom_19_1,axiom,(
    reachableState(constB19) )).

tff(reachableStateAxiom_20_1,axiom,(
    reachableState(constB20) )).

tff(reachableStateAxiom_21_1,axiom,(
    reachableState(constB21) )).

tff(reachableStateAxiom_22_1,axiom,(
    reachableState(constB22) )).

tff(reachableStateAxiom_23_1,axiom,(
    reachableState(constB23) )).

tff(reachableStateAxiom_24_1,axiom,(
    reachableState(constB24) )).

tff(reachableStateAxiom_25_1,axiom,(
    reachableState(constB25) )).

tff(reachableStateAxiom_26_1,axiom,(
    reachableState(constB26) )).

tff(reachableStateAxiom_27_1,axiom,(
    reachableState(constB27) )).

tff(reachableStateAxiom_28_1,axiom,(
    reachableState(constB28) )).

tff(reachableStateAxiom_29_1,axiom,(
    reachableState(constB29) )).

tff(reachableStateAxiom_30_1,axiom,(
    reachableState(constB30) )).

tff(reachableStateAxiom_31_1,axiom,(
    reachableState(constB31) )).

tff(reachableStateAxiom_32_1,axiom,(
    reachableState(constB32) )).

tff(reachableStateAxiom_33_1,axiom,(
    reachableState(constB33) )).

tff(reachableStateAxiom_34_1,axiom,(
    reachableState(constB34) )).

tff(reachableStateAxiom_35_1,axiom,(
    reachableState(constB35) )).

tff(reachableStateAxiom_36_1,axiom,(
    reachableState(constB36) )).

tff(reachableStateAxiom_37_1,axiom,(
    reachableState(constB37) )).

tff(reachableStateAxiom_38_1,axiom,(
    reachableState(constB38) )).

tff(reachableStateAxiom_39_1,axiom,(
    reachableState(constB39) )).

tff(reachableStateAxiom_40_1,axiom,(
    reachableState(constB40) )).

tff(reachableStateAxiom_41_1,axiom,(
    reachableState(constB41) )).

tff(reachableStateAxiom_42_1,axiom,(
    reachableState(constB42) )).

tff(reachableStateAxiom_43_1,axiom,(
    reachableState(constB43) )).

tff(reachableStateAxiom_44_1,axiom,(
    reachableState(constB44) )).

tff(reachableStateAxiom_45_1,axiom,(
    reachableState(constB45) )).

tff(reachableStateAxiom_46_1,axiom,(
    reachableState(constB46) )).

tff(reachableStateAxiom_47_1,axiom,(
    reachableState(constB47) )).

tff(reachableStateAxiom_48_1,axiom,(
    reachableState(constB48) )).

tff(reachableStateAxiom_49_1,axiom,(
    reachableState(constB49) )).

tff(reachableStateAxiom_50_1,axiom,(
    reachableState(constB50) )).

tff(reachableStateAxiom_51_1,axiom,(
    reachableState(constB51) )).

tff(reachableStateAxiom_52_1,axiom,(
    reachableState(constB52) )).

tff(reachableStateAxiom_53_1,axiom,(
    reachableState(constB53) )).

tff(reachableStateAxiom_54_1,axiom,(
    reachableState(constB54) )).

tff(reachableStateAxiom_55_1,axiom,(
    reachableState(constB55) )).

tff(reachableStateAxiom_56_1,axiom,(
    reachableState(constB56) )).

tff(reachableStateAxiom_57_1,axiom,(
    reachableState(constB57) )).

tff(reachableStateAxiom_58_1,axiom,(
    reachableState(constB58) )).

tff(reachableStateAxiom_59_1,axiom,(
    reachableState(constB59) )).

tff(reachableStateAxiom_60_1,axiom,(
    reachableState(constB60) )).

tff(reachableStateAxiom_61_1,axiom,(
    reachableState(constB61) )).

tff(reachableStateAxiom_62_1,axiom,(
    reachableState(constB62) )).

tff(reachableStateAxiom_63_1,axiom,(
    reachableState(constB63) )).

tff(reachableStateAxiom_64_1,axiom,(
    reachableState(constB64) )).

tff(reachableStateAxiom_65_1,axiom,(
    reachableState(constB65) )).

tff(reachableStateAxiom_66_1,axiom,(
    reachableState(constB66) )).

tff(reachableStateAxiom_67_1,axiom,(
    reachableState(constB67) )).

tff(reachableStateAxiom_68_1,axiom,(
    reachableState(constB68) )).

tff(reachableStateAxiom_69_1,axiom,(
    reachableState(constB69) )).

tff(reachableStateAxiom_70_1,axiom,(
    reachableState(constB70) )).

tff(reachableStateAxiom_71_1,axiom,(
    reachableState(constB71) )).

tff(reachableStateAxiom_72_1,axiom,(
    reachableState(constB72) )).

tff(reachableStateAxiom_73_1,axiom,(
    reachableState(constB73) )).

tff(reachableStateAxiom_74_1,axiom,(
    reachableState(constB74) )).

tff(reachableStateAxiom_75_1,axiom,(
    reachableState(constB75) )).

tff(reachableStateAxiom_76_1,axiom,(
    reachableState(constB76) )).

tff(reachableStateAxiom_77_1,axiom,(
    reachableState(constB77) )).

tff(reachableStateAxiom_78_1,axiom,(
    reachableState(constB78) )).

tff(reachableStateAxiom_79_1,axiom,(
    reachableState(constB79) )).

tff(reachableStateAxiom_80_1,axiom,(
    reachableState(constB80) )).

tff(reachableStateAxiom_81_1,axiom,(
    reachableState(constB81) )).

tff(reachableStateAxiom_82_1,axiom,(
    reachableState(constB82) )).

tff(reachableStateAxiom_83_1,axiom,(
    reachableState(constB83) )).

tff(reachableStateAxiom_84_1,axiom,(
    reachableState(constB84) )).

tff(reachableStateAxiom_85_1,axiom,(
    reachableState(constB85) )).

tff(reachableStateAxiom_86_1,axiom,(
    reachableState(constB86) )).

tff(reachableStateAxiom_87_1,axiom,(
    reachableState(constB87) )).

tff(reachableStateAxiom_88_1,axiom,(
    reachableState(constB88) )).

tff(reachableStateAxiom_89_1,axiom,(
    reachableState(constB89) )).

tff(reachableStateAxiom_90_1,axiom,(
    reachableState(constB90) )).

tff(reachableStateAxiom_91_1,axiom,(
    reachableState(constB91) )).

tff(reachableStateAxiom_92_1,axiom,(
    reachableState(constB92) )).

tff(reachableStateAxiom_93_1,axiom,(
    reachableState(constB93) )).

tff(reachableStateAxiom_94_1,axiom,(
    reachableState(constB94) )).

tff(reachableStateAxiom_95_1,axiom,(
    reachableState(constB95) )).

tff(reachableStateAxiom_96_1,axiom,(
    reachableState(constB96) )).

tff(reachableStateAxiom_97_1,axiom,(
    reachableState(constB97) )).

tff(reachableStateAxiom_98_1,axiom,(
    reachableState(constB98) )).

tff(reachableStateAxiom_99_1,axiom,(
    reachableState(constB99) )).

tff(reachableStateAxiom_100_1,axiom,(
    reachableState(constB100) )).

tff(reachableStateAxiom_101_1,axiom,(
    reachableState(constB101) )).

tff(reachableStateAxiom_102_1,axiom,(
    reachableState(constB102) )).

tff(reachableStateAxiom_103_1,axiom,(
    reachableState(constB103) )).

tff(reachableStateAxiom_104_1,axiom,(
    reachableState(constB104) )).

tff(reachableStateAxiom_105_1,axiom,(
    reachableState(constB105) )).

tff(reachableStateAxiom_106_1,axiom,(
    reachableState(constB106) )).

tff(reachableStateAxiom_107_1,axiom,(
    reachableState(constB107) )).

tff(reachableStateAxiom_108_1,axiom,(
    reachableState(constB108) )).

tff(reachableStateAxiom_109_1,axiom,(
    reachableState(constB109) )).

tff(reachableStateAxiom_110_1,axiom,(
    reachableState(constB110) )).

tff(reachableStateAxiom_111_1,axiom,(
    reachableState(constB111) )).

tff(reachableStateAxiom_112_1,axiom,(
    reachableState(constB112) )).

tff(reachableStateAxiom_113_1,axiom,(
    reachableState(constB113) )).

tff(reachableStateAxiom_114_1,axiom,(
    reachableState(constB114) )).

tff(reachableStateAxiom_115_1,axiom,(
    reachableState(constB115) )).

tff(reachableStateAxiom_116_1,axiom,(
    reachableState(constB116) )).

tff(reachableStateAxiom_117_1,axiom,(
    reachableState(constB117) )).

tff(reachableStateAxiom_118_1,axiom,(
    reachableState(constB118) )).

tff(reachableStateAxiom_119_1,axiom,(
    reachableState(constB119) )).

tff(reachableStateAxiom_120_1,axiom,(
    reachableState(constB120) )).

tff(reachableStateAxiom_121_1,axiom,(
    reachableState(constB121) )).

tff(reachableStateAxiom_122_1,axiom,(
    reachableState(constB122) )).

tff(reachableStateAxiom_123_1,axiom,(
    reachableState(constB123) )).

tff(reachableStateAxiom_124_1,axiom,(
    reachableState(constB124) )).

tff(reachableStateAxiom_125_1,axiom,(
    reachableState(constB125) )).

tff(reachableStateAxiom_126_1,axiom,(
    reachableState(constB126) )).

tff(reachableStateAxiom_127_1,axiom,(
    reachableState(constB127) )).

tff(reachableStateAxiom_128_1,axiom,(
    reachableState(constB128) )).

tff(reachableStateAxiom_129_1,axiom,(
    reachableState(constB129) )).

tff(reachableStateAxiom_130_1,axiom,(
    reachableState(constB130) )).

tff(reachableStateAxiom_131_1,axiom,(
    reachableState(constB131) )).

tff(reachableStateAxiom_132_1,axiom,(
    reachableState(constB132) )).

tff(reachableStateAxiom_133_1,axiom,(
    reachableState(constB133) )).

tff(reachableStateAxiom_134_1,axiom,(
    reachableState(constB134) )).

tff(reachableStateAxiom_135_1,axiom,(
    reachableState(constB135) )).

tff(reachableStateAxiom_136_1,axiom,(
    reachableState(constB136) )).

tff(reachableStateAxiom_137_1,axiom,(
    reachableState(constB137) )).

tff(reachableStateAxiom_138_1,axiom,(
    reachableState(constB138) )).

tff(reachableStateAxiom_139_1,axiom,(
    reachableState(constB139) )).

tff(reachableStateAxiom_140_1,axiom,(
    reachableState(constB140) )).

tff(reachableStateAxiom_141_1,axiom,(
    reachableState(constB141) )).

tff(reachableStateAxiom_142_1,axiom,(
    reachableState(constB142) )).

tff(reachableStateAxiom_143_1,axiom,(
    reachableState(constB143) )).

tff(reachableStateAxiom_144_1,axiom,(
    reachableState(constB144) )).

tff(reachableStateAxiom_145_1,axiom,(
    reachableState(constB145) )).

tff(reachableStateAxiom_146_1,axiom,(
    reachableState(constB146) )).

tff(reachableStateAxiom_147_1,axiom,(
    reachableState(constB147) )).

tff(reachableStateAxiom_148_1,axiom,(
    reachableState(constB148) )).

tff(reachableStateAxiom_149_1,axiom,(
    reachableState(constB149) )).

tff(reachableStateAxiom_150_1,axiom,(
    reachableState(constB150) )).

tff(reachableStateAxiom_151_1,axiom,(
    reachableState(constB151) )).

tff(reachableStateAxiom_152_1,axiom,(
    reachableState(constB152) )).

tff(reachableStateAxiom_153_1,axiom,(
    reachableState(constB153) )).

tff(reachableStateAxiom_154_1,axiom,(
    reachableState(constB154) )).

tff(reachableStateAxiom_155_1,axiom,(
    reachableState(constB155) )).

tff(reachableStateAxiom_156_1,axiom,(
    reachableState(constB156) )).

tff(reachableStateAxiom_157_1,axiom,(
    reachableState(constB157) )).

tff(reachableStateAxiom_158_1,axiom,(
    reachableState(constB158) )).

tff(reachableStateAxiom_159_1,axiom,(
    reachableState(constB159) )).

tff(reachableStateAxiom_160_1,axiom,(
    reachableState(constB160) )).

tff(reachableStateAxiom_161_1,axiom,(
    reachableState(constB161) )).

tff(reachableStateAxiom_162_1,axiom,(
    reachableState(constB162) )).

tff(reachableStateAxiom_163_1,axiom,(
    reachableState(constB163) )).

tff(reachableStateAxiom_164_1,axiom,(
    reachableState(constB164) )).

tff(reachableStateAxiom_165_1,axiom,(
    reachableState(constB165) )).

tff(reachableStateAxiom_166_1,axiom,(
    reachableState(constB166) )).

tff(reachableStateAxiom_167_1,axiom,(
    reachableState(constB167) )).

tff(reachableStateAxiom_168_1,axiom,(
    reachableState(constB168) )).

tff(reachableStateAxiom_169_1,axiom,(
    reachableState(constB169) )).

tff(reachableStateAxiom_170_1,axiom,(
    reachableState(constB170) )).

tff(reachableStateAxiom_171_1,axiom,(
    reachableState(constB171) )).

tff(reachableStateAxiom_172_1,axiom,(
    reachableState(constB172) )).

tff(reachableStateAxiom_173_1,axiom,(
    reachableState(constB173) )).

tff(reachableStateAxiom_174_1,axiom,(
    reachableState(constB174) )).

tff(reachableStateAxiom_175_1,axiom,(
    reachableState(constB175) )).

tff(reachableStateAxiom_176_1,axiom,(
    reachableState(constB176) )).

tff(reachableStateAxiom_177_1,axiom,(
    reachableState(constB177) )).

tff(reachableStateAxiom_178_1,axiom,(
    reachableState(constB178) )).

tff(reachableStateAxiom_179_1,axiom,(
    reachableState(constB179) )).

tff(reachableStateAxiom_180_1,axiom,(
    reachableState(constB180) )).

tff(reachableStateAxiom_181_1,axiom,(
    reachableState(constB181) )).

tff(reachableStateAxiom_182_1,axiom,(
    reachableState(constB182) )).

tff(reachableStateAxiom_183_1,axiom,(
    reachableState(constB183) )).

tff(reachableStateAxiom_184_1,axiom,(
    reachableState(constB184) )).

tff(reachableStateAxiom_185_1,axiom,(
    reachableState(constB185) )).

tff(reachableStateAxiom_186_1,axiom,(
    reachableState(constB186) )).

tff(reachableStateAxiom_187_1,axiom,(
    reachableState(constB187) )).

tff(reachableStateAxiom_188_1,axiom,(
    reachableState(constB188) )).

tff(reachableStateAxiom_189_1,axiom,(
    reachableState(constB189) )).

tff(reachableStateAxiom_190_1,axiom,(
    reachableState(constB190) )).

tff(reachableStateAxiom_191_1,axiom,(
    reachableState(constB191) )).

tff(reachableStateAxiom_192_1,axiom,(
    reachableState(constB192) )).

tff(reachableStateAxiom_193_1,axiom,(
    reachableState(constB193) )).

tff(reachableStateAxiom_194_1,axiom,(
    reachableState(constB194) )).

tff(reachableStateAxiom_195_1,axiom,(
    reachableState(constB195) )).

tff(reachableStateAxiom_196_1,axiom,(
    reachableState(constB196) )).

tff(reachableStateAxiom_197_1,axiom,(
    reachableState(constB197) )).

tff(reachableStateAxiom_198_1,axiom,(
    reachableState(constB198) )).

tff(reachableStateAxiom_199_1,axiom,(
    reachableState(constB199) )).

tff(reachableStateAxiom_200_1,axiom,(
    reachableState(constB200) )).

tff(reachableStateAxiom_201_1_1,axiom,(
    ! [VarState: state_type] :
      ( ~ reachableState(VarState)
      | constB0 = VarState
      | constB1 = VarState
      | constB2 = VarState
      | constB3 = VarState
      | constB4 = VarState
      | constB5 = VarState
      | constB6 = VarState
      | constB7 = VarState
      | constB8 = VarState
      | constB9 = VarState
      | constB10 = VarState
      | constB11 = VarState
      | constB12 = VarState
      | constB13 = VarState
      | constB14 = VarState
      | constB15 = VarState
      | constB16 = VarState
      | constB17 = VarState
      | constB18 = VarState
      | constB19 = VarState
      | constB20 = VarState
      | constB21 = VarState
      | constB22 = VarState
      | constB23 = VarState
      | constB24 = VarState
      | constB25 = VarState
      | constB26 = VarState
      | constB27 = VarState
      | constB28 = VarState
      | constB29 = VarState
      | constB30 = VarState
      | constB31 = VarState
      | constB32 = VarState
      | constB33 = VarState
      | constB34 = VarState
      | constB35 = VarState
      | constB36 = VarState
      | constB37 = VarState
      | constB38 = VarState
      | constB39 = VarState
      | constB40 = VarState
      | constB41 = VarState
      | constB42 = VarState
      | constB43 = VarState
      | constB44 = VarState
      | constB45 = VarState
      | constB46 = VarState
      | constB47 = VarState
      | constB48 = VarState
      | constB49 = VarState
      | constB50 = VarState
      | constB51 = VarState
      | constB52 = VarState
      | constB53 = VarState
      | constB54 = VarState
      | constB55 = VarState
      | constB56 = VarState
      | constB57 = VarState
      | constB58 = VarState
      | constB59 = VarState
      | constB60 = VarState
      | constB61 = VarState
      | constB62 = VarState
      | constB63 = VarState
      | constB64 = VarState
      | constB65 = VarState
      | constB66 = VarState
      | constB67 = VarState
      | constB68 = VarState
      | constB69 = VarState
      | constB70 = VarState
      | constB71 = VarState
      | constB72 = VarState
      | constB73 = VarState
      | constB74 = VarState
      | constB75 = VarState
      | constB76 = VarState
      | constB77 = VarState
      | constB78 = VarState
      | constB79 = VarState
      | constB80 = VarState
      | constB81 = VarState
      | constB82 = VarState
      | constB83 = VarState
      | constB84 = VarState
      | constB85 = VarState
      | constB86 = VarState
      | constB87 = VarState
      | constB88 = VarState
      | constB89 = VarState
      | constB90 = VarState
      | constB91 = VarState
      | constB92 = VarState
      | constB93 = VarState
      | constB94 = VarState
      | constB95 = VarState
      | constB96 = VarState
      | constB97 = VarState
      | constB98 = VarState
      | constB99 = VarState
      | constB100 = VarState
      | constB101 = VarState
      | constB102 = VarState
      | constB103 = VarState
      | constB104 = VarState
      | constB105 = VarState
      | constB106 = VarState
      | constB107 = VarState
      | constB108 = VarState
      | constB109 = VarState
      | constB110 = VarState
      | constB111 = VarState
      | constB112 = VarState
      | constB113 = VarState
      | constB114 = VarState
      | constB115 = VarState
      | constB116 = VarState
      | constB117 = VarState
      | constB118 = VarState
      | constB119 = VarState
      | constB120 = VarState
      | constB121 = VarState
      | constB122 = VarState
      | constB123 = VarState
      | constB124 = VarState
      | constB125 = VarState
      | constB126 = VarState
      | constB127 = VarState
      | constB128 = VarState
      | constB129 = VarState
      | constB130 = VarState
      | constB131 = VarState
      | constB132 = VarState
      | constB133 = VarState
      | constB134 = VarState
      | constB135 = VarState
      | constB136 = VarState
      | constB137 = VarState
      | constB138 = VarState
      | constB139 = VarState
      | constB140 = VarState
      | constB141 = VarState
      | constB142 = VarState
      | constB143 = VarState
      | constB144 = VarState
      | constB145 = VarState
      | constB146 = VarState
      | constB147 = VarState
      | constB148 = VarState
      | constB149 = VarState
      | constB150 = VarState
      | constB151 = VarState
      | constB152 = VarState
      | constB153 = VarState
      | constB154 = VarState
      | constB155 = VarState
      | constB156 = VarState
      | constB157 = VarState
      | constB158 = VarState
      | constB159 = VarState
      | constB160 = VarState
      | constB161 = VarState
      | constB162 = VarState
      | constB163 = VarState
      | constB164 = VarState
      | constB165 = VarState
      | constB166 = VarState
      | constB167 = VarState
      | constB168 = VarState
      | constB169 = VarState
      | constB170 = VarState
      | constB171 = VarState
      | constB172 = VarState
      | constB173 = VarState
      | constB174 = VarState
      | constB175 = VarState
      | constB176 = VarState
      | constB177 = VarState
      | constB178 = VarState
      | constB179 = VarState
      | constB180 = VarState
      | constB181 = VarState
      | constB182 = VarState
      | constB183 = VarState
      | constB184 = VarState
      | constB185 = VarState
      | constB186 = VarState
      | constB187 = VarState
      | constB188 = VarState
      | constB189 = VarState
      | constB190 = VarState
      | constB191 = VarState
      | constB192 = VarState
      | constB193 = VarState
      | constB194 = VarState
      | constB195 = VarState
      | constB196 = VarState
      | constB197 = VarState
      | constB198 = VarState
      | constB199 = VarState
      | constB200 = VarState ) )).

tff(reachableStateAxiom_202_1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | reachableState(VarCurr) ) )).

tff(reachableStateAxiom_202_1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( ~ nextState(VarCurr,VarNext)
      | reachableState(VarNext) ) )).

tff(pathAxiom_200,axiom,(
    nextState(constB0,constB1) )).

tff(pathAxiom_1_1,axiom,(
    nextState(constB1,constB2) )).

tff(pathAxiom_2_1,axiom,(
    nextState(constB2,constB3) )).

tff(pathAxiom_3_1,axiom,(
    nextState(constB3,constB4) )).

tff(pathAxiom_4_1,axiom,(
    nextState(constB4,constB5) )).

tff(pathAxiom_5_1,axiom,(
    nextState(constB5,constB6) )).

tff(pathAxiom_6_1,axiom,(
    nextState(constB6,constB7) )).

tff(pathAxiom_7_1,axiom,(
    nextState(constB7,constB8) )).

tff(pathAxiom_8_1,axiom,(
    nextState(constB8,constB9) )).

tff(pathAxiom_9_1,axiom,(
    nextState(constB9,constB10) )).

tff(pathAxiom_10_1,axiom,(
    nextState(constB10,constB11) )).

tff(pathAxiom_11_1,axiom,(
    nextState(constB11,constB12) )).

tff(pathAxiom_12_1,axiom,(
    nextState(constB12,constB13) )).

tff(pathAxiom_13_1,axiom,(
    nextState(constB13,constB14) )).

tff(pathAxiom_14_1,axiom,(
    nextState(constB14,constB15) )).

tff(pathAxiom_15_1,axiom,(
    nextState(constB15,constB16) )).

tff(pathAxiom_16_1,axiom,(
    nextState(constB16,constB17) )).

tff(pathAxiom_17_1,axiom,(
    nextState(constB17,constB18) )).

tff(pathAxiom_18_1,axiom,(
    nextState(constB18,constB19) )).

tff(pathAxiom_19_1,axiom,(
    nextState(constB19,constB20) )).

tff(pathAxiom_20_1,axiom,(
    nextState(constB20,constB21) )).

tff(pathAxiom_21_1,axiom,(
    nextState(constB21,constB22) )).

tff(pathAxiom_22_1,axiom,(
    nextState(constB22,constB23) )).

tff(pathAxiom_23_1,axiom,(
    nextState(constB23,constB24) )).

tff(pathAxiom_24_1,axiom,(
    nextState(constB24,constB25) )).

tff(pathAxiom_25_1,axiom,(
    nextState(constB25,constB26) )).

tff(pathAxiom_26_1,axiom,(
    nextState(constB26,constB27) )).

tff(pathAxiom_27_1,axiom,(
    nextState(constB27,constB28) )).

tff(pathAxiom_28_1,axiom,(
    nextState(constB28,constB29) )).

tff(pathAxiom_29_1,axiom,(
    nextState(constB29,constB30) )).

tff(pathAxiom_30_1,axiom,(
    nextState(constB30,constB31) )).

tff(pathAxiom_31_1,axiom,(
    nextState(constB31,constB32) )).

tff(pathAxiom_32_1,axiom,(
    nextState(constB32,constB33) )).

tff(pathAxiom_33_1,axiom,(
    nextState(constB33,constB34) )).

tff(pathAxiom_34_1,axiom,(
    nextState(constB34,constB35) )).

tff(pathAxiom_35_1,axiom,(
    nextState(constB35,constB36) )).

tff(pathAxiom_36_1,axiom,(
    nextState(constB36,constB37) )).

tff(pathAxiom_37_1,axiom,(
    nextState(constB37,constB38) )).

tff(pathAxiom_38_1,axiom,(
    nextState(constB38,constB39) )).

tff(pathAxiom_39_1,axiom,(
    nextState(constB39,constB40) )).

tff(pathAxiom_40_1,axiom,(
    nextState(constB40,constB41) )).

tff(pathAxiom_41_1,axiom,(
    nextState(constB41,constB42) )).

tff(pathAxiom_42_1,axiom,(
    nextState(constB42,constB43) )).

tff(pathAxiom_43_1,axiom,(
    nextState(constB43,constB44) )).

tff(pathAxiom_44_1,axiom,(
    nextState(constB44,constB45) )).

tff(pathAxiom_45_1,axiom,(
    nextState(constB45,constB46) )).

tff(pathAxiom_46_1,axiom,(
    nextState(constB46,constB47) )).

tff(pathAxiom_47_1,axiom,(
    nextState(constB47,constB48) )).

tff(pathAxiom_48_1,axiom,(
    nextState(constB48,constB49) )).

tff(pathAxiom_49_1,axiom,(
    nextState(constB49,constB50) )).

tff(pathAxiom_50_1,axiom,(
    nextState(constB50,constB51) )).

tff(pathAxiom_51_1,axiom,(
    nextState(constB51,constB52) )).

tff(pathAxiom_52_1,axiom,(
    nextState(constB52,constB53) )).

tff(pathAxiom_53_1,axiom,(
    nextState(constB53,constB54) )).

tff(pathAxiom_54_1,axiom,(
    nextState(constB54,constB55) )).

tff(pathAxiom_55_1,axiom,(
    nextState(constB55,constB56) )).

tff(pathAxiom_56_1,axiom,(
    nextState(constB56,constB57) )).

tff(pathAxiom_57_1,axiom,(
    nextState(constB57,constB58) )).

tff(pathAxiom_58_1,axiom,(
    nextState(constB58,constB59) )).

tff(pathAxiom_59_1,axiom,(
    nextState(constB59,constB60) )).

tff(pathAxiom_60_1,axiom,(
    nextState(constB60,constB61) )).

tff(pathAxiom_61_1,axiom,(
    nextState(constB61,constB62) )).

tff(pathAxiom_62_1,axiom,(
    nextState(constB62,constB63) )).

tff(pathAxiom_63_1,axiom,(
    nextState(constB63,constB64) )).

tff(pathAxiom_64_1,axiom,(
    nextState(constB64,constB65) )).

tff(pathAxiom_65_1,axiom,(
    nextState(constB65,constB66) )).

tff(pathAxiom_66_1,axiom,(
    nextState(constB66,constB67) )).

tff(pathAxiom_67_1,axiom,(
    nextState(constB67,constB68) )).

tff(pathAxiom_68_1,axiom,(
    nextState(constB68,constB69) )).

tff(pathAxiom_69_1,axiom,(
    nextState(constB69,constB70) )).

tff(pathAxiom_70_1,axiom,(
    nextState(constB70,constB71) )).

tff(pathAxiom_71_1,axiom,(
    nextState(constB71,constB72) )).

tff(pathAxiom_72_1,axiom,(
    nextState(constB72,constB73) )).

tff(pathAxiom_73_1,axiom,(
    nextState(constB73,constB74) )).

tff(pathAxiom_74_1,axiom,(
    nextState(constB74,constB75) )).

tff(pathAxiom_75_1,axiom,(
    nextState(constB75,constB76) )).

tff(pathAxiom_76_1,axiom,(
    nextState(constB76,constB77) )).

tff(pathAxiom_77_1,axiom,(
    nextState(constB77,constB78) )).

tff(pathAxiom_78_1,axiom,(
    nextState(constB78,constB79) )).

tff(pathAxiom_79_1,axiom,(
    nextState(constB79,constB80) )).

tff(pathAxiom_80_1,axiom,(
    nextState(constB80,constB81) )).

tff(pathAxiom_81_1,axiom,(
    nextState(constB81,constB82) )).

tff(pathAxiom_82_1,axiom,(
    nextState(constB82,constB83) )).

tff(pathAxiom_83_1,axiom,(
    nextState(constB83,constB84) )).

tff(pathAxiom_84_1,axiom,(
    nextState(constB84,constB85) )).

tff(pathAxiom_85_1,axiom,(
    nextState(constB85,constB86) )).

tff(pathAxiom_86_1,axiom,(
    nextState(constB86,constB87) )).

tff(pathAxiom_87_1,axiom,(
    nextState(constB87,constB88) )).

tff(pathAxiom_88_1,axiom,(
    nextState(constB88,constB89) )).

tff(pathAxiom_89_1,axiom,(
    nextState(constB89,constB90) )).

tff(pathAxiom_90_1,axiom,(
    nextState(constB90,constB91) )).

tff(pathAxiom_91_1,axiom,(
    nextState(constB91,constB92) )).

tff(pathAxiom_92_1,axiom,(
    nextState(constB92,constB93) )).

tff(pathAxiom_93_1,axiom,(
    nextState(constB93,constB94) )).

tff(pathAxiom_94_1,axiom,(
    nextState(constB94,constB95) )).

tff(pathAxiom_95_1,axiom,(
    nextState(constB95,constB96) )).

tff(pathAxiom_96_1,axiom,(
    nextState(constB96,constB97) )).

tff(pathAxiom_97_1,axiom,(
    nextState(constB97,constB98) )).

tff(pathAxiom_98_1,axiom,(
    nextState(constB98,constB99) )).

tff(pathAxiom_99_1,axiom,(
    nextState(constB99,constB100) )).

tff(pathAxiom_100_1,axiom,(
    nextState(constB100,constB101) )).

tff(pathAxiom_101_1,axiom,(
    nextState(constB101,constB102) )).

tff(pathAxiom_102_1,axiom,(
    nextState(constB102,constB103) )).

tff(pathAxiom_103_1,axiom,(
    nextState(constB103,constB104) )).

tff(pathAxiom_104_1,axiom,(
    nextState(constB104,constB105) )).

tff(pathAxiom_105_1,axiom,(
    nextState(constB105,constB106) )).

tff(pathAxiom_106_1,axiom,(
    nextState(constB106,constB107) )).

tff(pathAxiom_107_1,axiom,(
    nextState(constB107,constB108) )).

tff(pathAxiom_108_1,axiom,(
    nextState(constB108,constB109) )).

tff(pathAxiom_109_1,axiom,(
    nextState(constB109,constB110) )).

tff(pathAxiom_110_1,axiom,(
    nextState(constB110,constB111) )).

tff(pathAxiom_111_1,axiom,(
    nextState(constB111,constB112) )).

tff(pathAxiom_112_1,axiom,(
    nextState(constB112,constB113) )).

tff(pathAxiom_113_1,axiom,(
    nextState(constB113,constB114) )).

tff(pathAxiom_114_1,axiom,(
    nextState(constB114,constB115) )).

tff(pathAxiom_115_1,axiom,(
    nextState(constB115,constB116) )).

tff(pathAxiom_116_1,axiom,(
    nextState(constB116,constB117) )).

tff(pathAxiom_117_1,axiom,(
    nextState(constB117,constB118) )).

tff(pathAxiom_118_1,axiom,(
    nextState(constB118,constB119) )).

tff(pathAxiom_119_1,axiom,(
    nextState(constB119,constB120) )).

tff(pathAxiom_120_1,axiom,(
    nextState(constB120,constB121) )).

tff(pathAxiom_121_1,axiom,(
    nextState(constB121,constB122) )).

tff(pathAxiom_122_1,axiom,(
    nextState(constB122,constB123) )).

tff(pathAxiom_123_1,axiom,(
    nextState(constB123,constB124) )).

tff(pathAxiom_124_1,axiom,(
    nextState(constB124,constB125) )).

tff(pathAxiom_125_1,axiom,(
    nextState(constB125,constB126) )).

tff(pathAxiom_126_1,axiom,(
    nextState(constB126,constB127) )).

tff(pathAxiom_127_1,axiom,(
    nextState(constB127,constB128) )).

tff(pathAxiom_128_1,axiom,(
    nextState(constB128,constB129) )).

tff(pathAxiom_129_1,axiom,(
    nextState(constB129,constB130) )).

tff(pathAxiom_130_1,axiom,(
    nextState(constB130,constB131) )).

tff(pathAxiom_131_1,axiom,(
    nextState(constB131,constB132) )).

tff(pathAxiom_132_1,axiom,(
    nextState(constB132,constB133) )).

tff(pathAxiom_133_1,axiom,(
    nextState(constB133,constB134) )).

tff(pathAxiom_134_1,axiom,(
    nextState(constB134,constB135) )).

tff(pathAxiom_135_1,axiom,(
    nextState(constB135,constB136) )).

tff(pathAxiom_136_1,axiom,(
    nextState(constB136,constB137) )).

tff(pathAxiom_137_1,axiom,(
    nextState(constB137,constB138) )).

tff(pathAxiom_138_1,axiom,(
    nextState(constB138,constB139) )).

tff(pathAxiom_139_1,axiom,(
    nextState(constB139,constB140) )).

tff(pathAxiom_140_1,axiom,(
    nextState(constB140,constB141) )).

tff(pathAxiom_141_1,axiom,(
    nextState(constB141,constB142) )).

tff(pathAxiom_142_1,axiom,(
    nextState(constB142,constB143) )).

tff(pathAxiom_143_1,axiom,(
    nextState(constB143,constB144) )).

tff(pathAxiom_144_1,axiom,(
    nextState(constB144,constB145) )).

tff(pathAxiom_145_1,axiom,(
    nextState(constB145,constB146) )).

tff(pathAxiom_146_1,axiom,(
    nextState(constB146,constB147) )).

tff(pathAxiom_147_1,axiom,(
    nextState(constB147,constB148) )).

tff(pathAxiom_148_1,axiom,(
    nextState(constB148,constB149) )).

tff(pathAxiom_149_1,axiom,(
    nextState(constB149,constB150) )).

tff(pathAxiom_150_1,axiom,(
    nextState(constB150,constB151) )).

tff(pathAxiom_151_1,axiom,(
    nextState(constB151,constB152) )).

tff(pathAxiom_152_1,axiom,(
    nextState(constB152,constB153) )).

tff(pathAxiom_153_1,axiom,(
    nextState(constB153,constB154) )).

tff(pathAxiom_154_1,axiom,(
    nextState(constB154,constB155) )).

tff(pathAxiom_155_1,axiom,(
    nextState(constB155,constB156) )).

tff(pathAxiom_156_1,axiom,(
    nextState(constB156,constB157) )).

tff(pathAxiom_157_1,axiom,(
    nextState(constB157,constB158) )).

tff(pathAxiom_158_1,axiom,(
    nextState(constB158,constB159) )).

tff(pathAxiom_159_1,axiom,(
    nextState(constB159,constB160) )).

tff(pathAxiom_160_1,axiom,(
    nextState(constB160,constB161) )).

tff(pathAxiom_161_1,axiom,(
    nextState(constB161,constB162) )).

tff(pathAxiom_162_1,axiom,(
    nextState(constB162,constB163) )).

tff(pathAxiom_163_1,axiom,(
    nextState(constB163,constB164) )).

tff(pathAxiom_164_1,axiom,(
    nextState(constB164,constB165) )).

tff(pathAxiom_165_1,axiom,(
    nextState(constB165,constB166) )).

tff(pathAxiom_166_1,axiom,(
    nextState(constB166,constB167) )).

tff(pathAxiom_167_1,axiom,(
    nextState(constB167,constB168) )).

tff(pathAxiom_168_1,axiom,(
    nextState(constB168,constB169) )).

tff(pathAxiom_169_1,axiom,(
    nextState(constB169,constB170) )).

tff(pathAxiom_170_1,axiom,(
    nextState(constB170,constB171) )).

tff(pathAxiom_171_1,axiom,(
    nextState(constB171,constB172) )).

tff(pathAxiom_172_1,axiom,(
    nextState(constB172,constB173) )).

tff(pathAxiom_173_1,axiom,(
    nextState(constB173,constB174) )).

tff(pathAxiom_174_1,axiom,(
    nextState(constB174,constB175) )).

tff(pathAxiom_175_1,axiom,(
    nextState(constB175,constB176) )).

tff(pathAxiom_176_1,axiom,(
    nextState(constB176,constB177) )).

tff(pathAxiom_177_1,axiom,(
    nextState(constB177,constB178) )).

tff(pathAxiom_178_1,axiom,(
    nextState(constB178,constB179) )).

tff(pathAxiom_179_1,axiom,(
    nextState(constB179,constB180) )).

tff(pathAxiom_180_1,axiom,(
    nextState(constB180,constB181) )).

tff(pathAxiom_181_1,axiom,(
    nextState(constB181,constB182) )).

tff(pathAxiom_182_1,axiom,(
    nextState(constB182,constB183) )).

tff(pathAxiom_183_1,axiom,(
    nextState(constB183,constB184) )).

tff(pathAxiom_184_1,axiom,(
    nextState(constB184,constB185) )).

tff(pathAxiom_185_1,axiom,(
    nextState(constB185,constB186) )).

tff(pathAxiom_186_1,axiom,(
    nextState(constB186,constB187) )).

tff(pathAxiom_187_1,axiom,(
    nextState(constB187,constB188) )).

tff(pathAxiom_188_1,axiom,(
    nextState(constB188,constB189) )).

tff(pathAxiom_189_1,axiom,(
    nextState(constB189,constB190) )).

tff(pathAxiom_190_1,axiom,(
    nextState(constB190,constB191) )).

tff(pathAxiom_191_1,axiom,(
    nextState(constB191,constB192) )).

tff(pathAxiom_192_1,axiom,(
    nextState(constB192,constB193) )).

tff(pathAxiom_193_1,axiom,(
    nextState(constB193,constB194) )).

tff(pathAxiom_194_1,axiom,(
    nextState(constB194,constB195) )).

tff(pathAxiom_195_1,axiom,(
    nextState(constB195,constB196) )).

tff(pathAxiom_196_1,axiom,(
    nextState(constB196,constB197) )).

tff(pathAxiom_197_1,axiom,(
    nextState(constB197,constB198) )).

tff(pathAxiom_198_1,axiom,(
    nextState(constB198,constB199) )).

tff(pathAxiom_199_1,axiom,(
    nextState(constB199,constB200) )).

tff(clock_pattern_201,axiom,(
    v1(constB0) )).

tff(clock_pattern_1_1_1,axiom,(
    ~ v1(constB1) )).

tff(clock_pattern_2_1,axiom,(
    v1(constB2) )).

tff(clock_pattern_3_1_1,axiom,(
    ~ v1(constB3) )).

tff(clock_pattern_4_1,axiom,(
    v1(constB4) )).

tff(clock_pattern_5_1_1,axiom,(
    ~ v1(constB5) )).

tff(clock_pattern_6_1,axiom,(
    v1(constB6) )).

tff(clock_pattern_7_1_1,axiom,(
    ~ v1(constB7) )).

tff(clock_pattern_8_1,axiom,(
    v1(constB8) )).

tff(clock_pattern_9_1_1,axiom,(
    ~ v1(constB9) )).

tff(clock_pattern_10_1,axiom,(
    v1(constB10) )).

tff(clock_pattern_11_1_1,axiom,(
    ~ v1(constB11) )).

tff(clock_pattern_12_1,axiom,(
    v1(constB12) )).

tff(clock_pattern_13_1_1,axiom,(
    ~ v1(constB13) )).

tff(clock_pattern_14_1,axiom,(
    v1(constB14) )).

tff(clock_pattern_15_1_1,axiom,(
    ~ v1(constB15) )).

tff(clock_pattern_16_1,axiom,(
    v1(constB16) )).

tff(clock_pattern_17_1_1,axiom,(
    ~ v1(constB17) )).

tff(clock_pattern_18_1,axiom,(
    v1(constB18) )).

tff(clock_pattern_19_1_1,axiom,(
    ~ v1(constB19) )).

tff(clock_pattern_20_1,axiom,(
    v1(constB20) )).

tff(clock_pattern_21_1_1,axiom,(
    ~ v1(constB21) )).

tff(clock_pattern_22_1,axiom,(
    v1(constB22) )).

tff(clock_pattern_23_1_1,axiom,(
    ~ v1(constB23) )).

tff(clock_pattern_24_1,axiom,(
    v1(constB24) )).

tff(clock_pattern_25_1_1,axiom,(
    ~ v1(constB25) )).

tff(clock_pattern_26_1,axiom,(
    v1(constB26) )).

tff(clock_pattern_27_1_1,axiom,(
    ~ v1(constB27) )).

tff(clock_pattern_28_1,axiom,(
    v1(constB28) )).

tff(clock_pattern_29_1_1,axiom,(
    ~ v1(constB29) )).

tff(clock_pattern_30_1,axiom,(
    v1(constB30) )).

tff(clock_pattern_31_1_1,axiom,(
    ~ v1(constB31) )).

tff(clock_pattern_32_1,axiom,(
    v1(constB32) )).

tff(clock_pattern_33_1_1,axiom,(
    ~ v1(constB33) )).

tff(clock_pattern_34_1,axiom,(
    v1(constB34) )).

tff(clock_pattern_35_1_1,axiom,(
    ~ v1(constB35) )).

tff(clock_pattern_36_1,axiom,(
    v1(constB36) )).

tff(clock_pattern_37_1_1,axiom,(
    ~ v1(constB37) )).

tff(clock_pattern_38_1,axiom,(
    v1(constB38) )).

tff(clock_pattern_39_1_1,axiom,(
    ~ v1(constB39) )).

tff(clock_pattern_40_1,axiom,(
    v1(constB40) )).

tff(clock_pattern_41_1_1,axiom,(
    ~ v1(constB41) )).

tff(clock_pattern_42_1,axiom,(
    v1(constB42) )).

tff(clock_pattern_43_1_1,axiom,(
    ~ v1(constB43) )).

tff(clock_pattern_44_1,axiom,(
    v1(constB44) )).

tff(clock_pattern_45_1_1,axiom,(
    ~ v1(constB45) )).

tff(clock_pattern_46_1,axiom,(
    v1(constB46) )).

tff(clock_pattern_47_1_1,axiom,(
    ~ v1(constB47) )).

tff(clock_pattern_48_1,axiom,(
    v1(constB48) )).

tff(clock_pattern_49_1_1,axiom,(
    ~ v1(constB49) )).

tff(clock_pattern_50_1,axiom,(
    v1(constB50) )).

tff(clock_pattern_51_1_1,axiom,(
    ~ v1(constB51) )).

tff(clock_pattern_52_1,axiom,(
    v1(constB52) )).

tff(clock_pattern_53_1_1,axiom,(
    ~ v1(constB53) )).

tff(clock_pattern_54_1,axiom,(
    v1(constB54) )).

tff(clock_pattern_55_1_1,axiom,(
    ~ v1(constB55) )).

tff(clock_pattern_56_1,axiom,(
    v1(constB56) )).

tff(clock_pattern_57_1_1,axiom,(
    ~ v1(constB57) )).

tff(clock_pattern_58_1,axiom,(
    v1(constB58) )).

tff(clock_pattern_59_1_1,axiom,(
    ~ v1(constB59) )).

tff(clock_pattern_60_1,axiom,(
    v1(constB60) )).

tff(clock_pattern_61_1_1,axiom,(
    ~ v1(constB61) )).

tff(clock_pattern_62_1,axiom,(
    v1(constB62) )).

tff(clock_pattern_63_1_1,axiom,(
    ~ v1(constB63) )).

tff(clock_pattern_64_1,axiom,(
    v1(constB64) )).

tff(clock_pattern_65_1_1,axiom,(
    ~ v1(constB65) )).

tff(clock_pattern_66_1,axiom,(
    v1(constB66) )).

tff(clock_pattern_67_1_1,axiom,(
    ~ v1(constB67) )).

tff(clock_pattern_68_1,axiom,(
    v1(constB68) )).

tff(clock_pattern_69_1_1,axiom,(
    ~ v1(constB69) )).

tff(clock_pattern_70_1,axiom,(
    v1(constB70) )).

tff(clock_pattern_71_1_1,axiom,(
    ~ v1(constB71) )).

tff(clock_pattern_72_1,axiom,(
    v1(constB72) )).

tff(clock_pattern_73_1_1,axiom,(
    ~ v1(constB73) )).

tff(clock_pattern_74_1,axiom,(
    v1(constB74) )).

tff(clock_pattern_75_1_1,axiom,(
    ~ v1(constB75) )).

tff(clock_pattern_76_1,axiom,(
    v1(constB76) )).

tff(clock_pattern_77_1_1,axiom,(
    ~ v1(constB77) )).

tff(clock_pattern_78_1,axiom,(
    v1(constB78) )).

tff(clock_pattern_79_1_1,axiom,(
    ~ v1(constB79) )).

tff(clock_pattern_80_1,axiom,(
    v1(constB80) )).

tff(clock_pattern_81_1_1,axiom,(
    ~ v1(constB81) )).

tff(clock_pattern_82_1,axiom,(
    v1(constB82) )).

tff(clock_pattern_83_1_1,axiom,(
    ~ v1(constB83) )).

tff(clock_pattern_84_1,axiom,(
    v1(constB84) )).

tff(clock_pattern_85_1_1,axiom,(
    ~ v1(constB85) )).

tff(clock_pattern_86_1,axiom,(
    v1(constB86) )).

tff(clock_pattern_87_1_1,axiom,(
    ~ v1(constB87) )).

tff(clock_pattern_88_1,axiom,(
    v1(constB88) )).

tff(clock_pattern_89_1_1,axiom,(
    ~ v1(constB89) )).

tff(clock_pattern_90_1,axiom,(
    v1(constB90) )).

tff(clock_pattern_91_1_1,axiom,(
    ~ v1(constB91) )).

tff(clock_pattern_92_1,axiom,(
    v1(constB92) )).

tff(clock_pattern_93_1_1,axiom,(
    ~ v1(constB93) )).

tff(clock_pattern_94_1,axiom,(
    v1(constB94) )).

tff(clock_pattern_95_1_1,axiom,(
    ~ v1(constB95) )).

tff(clock_pattern_96_1,axiom,(
    v1(constB96) )).

tff(clock_pattern_97_1_1,axiom,(
    ~ v1(constB97) )).

tff(clock_pattern_98_1,axiom,(
    v1(constB98) )).

tff(clock_pattern_99_1_1,axiom,(
    ~ v1(constB99) )).

tff(clock_pattern_100_1,axiom,(
    v1(constB100) )).

tff(clock_pattern_101_1_1,axiom,(
    ~ v1(constB101) )).

tff(clock_pattern_102_1,axiom,(
    v1(constB102) )).

tff(clock_pattern_103_1_1,axiom,(
    ~ v1(constB103) )).

tff(clock_pattern_104_1,axiom,(
    v1(constB104) )).

tff(clock_pattern_105_1_1,axiom,(
    ~ v1(constB105) )).

tff(clock_pattern_106_1,axiom,(
    v1(constB106) )).

tff(clock_pattern_107_1_1,axiom,(
    ~ v1(constB107) )).

tff(clock_pattern_108_1,axiom,(
    v1(constB108) )).

tff(clock_pattern_109_1_1,axiom,(
    ~ v1(constB109) )).

tff(clock_pattern_110_1,axiom,(
    v1(constB110) )).

tff(clock_pattern_111_1_1,axiom,(
    ~ v1(constB111) )).

tff(clock_pattern_112_1,axiom,(
    v1(constB112) )).

tff(clock_pattern_113_1_1,axiom,(
    ~ v1(constB113) )).

tff(clock_pattern_114_1,axiom,(
    v1(constB114) )).

tff(clock_pattern_115_1_1,axiom,(
    ~ v1(constB115) )).

tff(clock_pattern_116_1,axiom,(
    v1(constB116) )).

tff(clock_pattern_117_1_1,axiom,(
    ~ v1(constB117) )).

tff(clock_pattern_118_1,axiom,(
    v1(constB118) )).

tff(clock_pattern_119_1_1,axiom,(
    ~ v1(constB119) )).

tff(clock_pattern_120_1,axiom,(
    v1(constB120) )).

tff(clock_pattern_121_1_1,axiom,(
    ~ v1(constB121) )).

tff(clock_pattern_122_1,axiom,(
    v1(constB122) )).

tff(clock_pattern_123_1_1,axiom,(
    ~ v1(constB123) )).

tff(clock_pattern_124_1,axiom,(
    v1(constB124) )).

tff(clock_pattern_125_1_1,axiom,(
    ~ v1(constB125) )).

tff(clock_pattern_126_1,axiom,(
    v1(constB126) )).

tff(clock_pattern_127_1_1,axiom,(
    ~ v1(constB127) )).

tff(clock_pattern_128_1,axiom,(
    v1(constB128) )).

tff(clock_pattern_129_1_1,axiom,(
    ~ v1(constB129) )).

tff(clock_pattern_130_1,axiom,(
    v1(constB130) )).

tff(clock_pattern_131_1_1,axiom,(
    ~ v1(constB131) )).

tff(clock_pattern_132_1,axiom,(
    v1(constB132) )).

tff(clock_pattern_133_1_1,axiom,(
    ~ v1(constB133) )).

tff(clock_pattern_134_1,axiom,(
    v1(constB134) )).

tff(clock_pattern_135_1_1,axiom,(
    ~ v1(constB135) )).

tff(clock_pattern_136_1,axiom,(
    v1(constB136) )).

tff(clock_pattern_137_1_1,axiom,(
    ~ v1(constB137) )).

tff(clock_pattern_138_1,axiom,(
    v1(constB138) )).

tff(clock_pattern_139_1_1,axiom,(
    ~ v1(constB139) )).

tff(clock_pattern_140_1,axiom,(
    v1(constB140) )).

tff(clock_pattern_141_1_1,axiom,(
    ~ v1(constB141) )).

tff(clock_pattern_142_1,axiom,(
    v1(constB142) )).

tff(clock_pattern_143_1_1,axiom,(
    ~ v1(constB143) )).

tff(clock_pattern_144_1,axiom,(
    v1(constB144) )).

tff(clock_pattern_145_1_1,axiom,(
    ~ v1(constB145) )).

tff(clock_pattern_146_1,axiom,(
    v1(constB146) )).

tff(clock_pattern_147_1_1,axiom,(
    ~ v1(constB147) )).

tff(clock_pattern_148_1,axiom,(
    v1(constB148) )).

tff(clock_pattern_149_1_1,axiom,(
    ~ v1(constB149) )).

tff(clock_pattern_150_1,axiom,(
    v1(constB150) )).

tff(clock_pattern_151_1_1,axiom,(
    ~ v1(constB151) )).

tff(clock_pattern_152_1,axiom,(
    v1(constB152) )).

tff(clock_pattern_153_1_1,axiom,(
    ~ v1(constB153) )).

tff(clock_pattern_154_1,axiom,(
    v1(constB154) )).

tff(clock_pattern_155_1_1,axiom,(
    ~ v1(constB155) )).

tff(clock_pattern_156_1,axiom,(
    v1(constB156) )).

tff(clock_pattern_157_1_1,axiom,(
    ~ v1(constB157) )).

tff(clock_pattern_158_1,axiom,(
    v1(constB158) )).

tff(clock_pattern_159_1_1,axiom,(
    ~ v1(constB159) )).

tff(clock_pattern_160_1,axiom,(
    v1(constB160) )).

tff(clock_pattern_161_1_1,axiom,(
    ~ v1(constB161) )).

tff(clock_pattern_162_1,axiom,(
    v1(constB162) )).

tff(clock_pattern_163_1_1,axiom,(
    ~ v1(constB163) )).

tff(clock_pattern_164_1,axiom,(
    v1(constB164) )).

tff(clock_pattern_165_1_1,axiom,(
    ~ v1(constB165) )).

tff(clock_pattern_166_1,axiom,(
    v1(constB166) )).

tff(clock_pattern_167_1_1,axiom,(
    ~ v1(constB167) )).

tff(clock_pattern_168_1,axiom,(
    v1(constB168) )).

tff(clock_pattern_169_1_1,axiom,(
    ~ v1(constB169) )).

tff(clock_pattern_170_1,axiom,(
    v1(constB170) )).

tff(clock_pattern_171_1_1,axiom,(
    ~ v1(constB171) )).

tff(clock_pattern_172_1,axiom,(
    v1(constB172) )).

tff(clock_pattern_173_1_1,axiom,(
    ~ v1(constB173) )).

tff(clock_pattern_174_1,axiom,(
    v1(constB174) )).

tff(clock_pattern_175_1_1,axiom,(
    ~ v1(constB175) )).

tff(clock_pattern_176_1,axiom,(
    v1(constB176) )).

tff(clock_pattern_177_1_1,axiom,(
    ~ v1(constB177) )).

tff(clock_pattern_178_1,axiom,(
    v1(constB178) )).

tff(clock_pattern_179_1_1,axiom,(
    ~ v1(constB179) )).

tff(clock_pattern_180_1,axiom,(
    v1(constB180) )).

tff(clock_pattern_181_1_1,axiom,(
    ~ v1(constB181) )).

tff(clock_pattern_182_1,axiom,(
    v1(constB182) )).

tff(clock_pattern_183_1_1,axiom,(
    ~ v1(constB183) )).

tff(clock_pattern_184_1,axiom,(
    v1(constB184) )).

tff(clock_pattern_185_1_1,axiom,(
    ~ v1(constB185) )).

tff(clock_pattern_186_1,axiom,(
    v1(constB186) )).

tff(clock_pattern_187_1_1,axiom,(
    ~ v1(constB187) )).

tff(clock_pattern_188_1,axiom,(
    v1(constB188) )).

tff(clock_pattern_189_1_1,axiom,(
    ~ v1(constB189) )).

tff(clock_pattern_190_1,axiom,(
    v1(constB190) )).

tff(clock_pattern_191_1_1,axiom,(
    ~ v1(constB191) )).

tff(clock_pattern_192_1,axiom,(
    v1(constB192) )).

tff(clock_pattern_193_1_1,axiom,(
    ~ v1(constB193) )).

tff(clock_pattern_194_1,axiom,(
    v1(constB194) )).

tff(clock_pattern_195_1_1,axiom,(
    ~ v1(constB195) )).

tff(clock_pattern_196_1,axiom,(
    v1(constB196) )).

tff(clock_pattern_197_1_1,axiom,(
    ~ v1(constB197) )).

tff(clock_pattern_198_1,axiom,(
    v1(constB198) )).

tff(clock_pattern_199_1_1,axiom,(
    ~ v1(constB199) )).

tff(clock_pattern_200_1,axiom,(
    v1(constB200) )).

%------------------------------------------------------------------------------
