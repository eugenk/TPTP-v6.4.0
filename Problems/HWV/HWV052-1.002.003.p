%------------------------------------------------------------------------------
% File     : HWV052-1.002.003 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Hardware Verification
% Problem  : Faulty channel 2 3
% Version  : Especial.
% English  : The problem of sending N bits over a faulty channel that can
%            mutilate any one bit. We can use K extra bits to help us do this.
%            Satisfiable means that it is possible, unsatisfiable means that
%            it is not possible.

% Refs     : [Cla10] Claessen (2010), Email to Geoff Sutcliffe
% Source   : [Cla10]
% Names    : fault_2_3 [Cla10]

% Status   : Satisfiable
% Rating   : 0.57 v6.4.0, 0.29 v6.3.0, 0.25 v6.2.0, 0.10 v6.1.0, 0.33 v6.0.0, 0.29 v5.5.0, 0.38 v5.4.0, 0.70 v5.3.0, 0.67 v5.2.0, 0.70 v5.0.0, 0.78 v4.1.0
% Syntax   : Number of clauses     :   14 (   1 non-Horn;  13 unit;   1 RR)
%            Number of atoms       :   15 (  15 equality)
%            Maximal clause size   :    2 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    8 (   2 constant; 0-5 arity)
%            Number of variables   :   26 (   0 singleton)
%            Maximal term depth    :    4 (   2 average)
% SPC      : CNF_SAT_RFO_EQU_NUE

% Comments :
%------------------------------------------------------------------------------
cnf(bit_domain,axiom,
    ( X = o
    | X = i )).

cnf(bit_inverse,axiom,
    ( inv(X) != X )).

cnf(unpack1,axiom,
    ( unpack1(X1,X2,pack1(X1,X2),pack2(X1,X2),pack3(X1,X2)) = X1 )).

cnf(unpack1_01,axiom,
    ( unpack1(inv(X1),X2,pack1(X1,X2),pack2(X1,X2),pack3(X1,X2)) = X1 )).

cnf(unpack1_02,axiom,
    ( unpack1(X1,inv(X2),pack1(X1,X2),pack2(X1,X2),pack3(X1,X2)) = X1 )).

cnf(unpack1_03,axiom,
    ( unpack1(X1,X2,inv(pack1(X1,X2)),pack2(X1,X2),pack3(X1,X2)) = X1 )).

cnf(unpack1_04,axiom,
    ( unpack1(X1,X2,pack1(X1,X2),inv(pack2(X1,X2)),pack3(X1,X2)) = X1 )).

cnf(unpack1_05,axiom,
    ( unpack1(X1,X2,pack1(X1,X2),pack2(X1,X2),inv(pack3(X1,X2))) = X1 )).

cnf(unpack2,axiom,
    ( unpack2(X1,X2,pack1(X1,X2),pack2(X1,X2),pack3(X1,X2)) = X2 )).

cnf(unpack2_06,axiom,
    ( unpack2(inv(X1),X2,pack1(X1,X2),pack2(X1,X2),pack3(X1,X2)) = X2 )).

cnf(unpack2_07,axiom,
    ( unpack2(X1,inv(X2),pack1(X1,X2),pack2(X1,X2),pack3(X1,X2)) = X2 )).

cnf(unpack2_08,axiom,
    ( unpack2(X1,X2,inv(pack1(X1,X2)),pack2(X1,X2),pack3(X1,X2)) = X2 )).

cnf(unpack2_09,axiom,
    ( unpack2(X1,X2,pack1(X1,X2),inv(pack2(X1,X2)),pack3(X1,X2)) = X2 )).

cnf(unpack2_10,axiom,
    ( unpack2(X1,X2,pack1(X1,X2),pack2(X1,X2),inv(pack3(X1,X2))) = X2 )).

%------------------------------------------------------------------------------
