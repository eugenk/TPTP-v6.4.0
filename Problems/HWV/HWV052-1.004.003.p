%------------------------------------------------------------------------------
% File     : HWV052-1.004.003 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Hardware Verification
% Problem  : Faulty channel 4 3
% Version  : Especial.
% English  : The problem of sending N bits over a faulty channel that can
%            mutilate any one bit. We can use K extra bits to help us do this.
%            Satisfiable means that it is possible, unsatisfiable means that
%            it is not possible.

% Refs     : [Cla10] Claessen (2010), Email to Geoff Sutcliffe
% Source   : [Cla10]
% Names    : fault_4_3 [Cla10]

% Status   : Satisfiable
% Rating   : 0.71 v6.4.0, 0.43 v6.3.0, 0.38 v6.2.0, 0.40 v6.1.0, 0.67 v6.0.0, 0.71 v5.5.0, 0.75 v5.4.0, 0.80 v5.3.0, 0.78 v5.2.0, 0.80 v5.0.0, 0.89 v4.1.0
% Syntax   : Number of clauses     :   34 (   1 non-Horn;  33 unit;   1 RR)
%            Number of atoms       :   35 (  35 equality)
%            Maximal clause size   :    2 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :   10 (   2 constant; 0-7 arity)
%            Number of variables   :  130 (   0 singleton)
%            Maximal term depth    :    4 (   2 average)
% SPC      : CNF_SAT_RFO_EQU_NUE

% Comments :
%------------------------------------------------------------------------------
cnf(bit_domain,axiom,
    ( X = o
    | X = i )).

cnf(bit_inverse,axiom,
    ( inv(X) != X )).

cnf(unpack1,axiom,
    ( unpack1(X1,X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X1 )).

cnf(unpack1_01,axiom,
    ( unpack1(inv(X1),X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X1 )).

cnf(unpack1_02,axiom,
    ( unpack1(X1,inv(X2),X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X1 )).

cnf(unpack1_03,axiom,
    ( unpack1(X1,X2,inv(X3),X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X1 )).

cnf(unpack1_04,axiom,
    ( unpack1(X1,X2,X3,inv(X4),pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X1 )).

cnf(unpack1_05,axiom,
    ( unpack1(X1,X2,X3,X4,inv(pack1(X1,X2,X3,X4)),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X1 )).

cnf(unpack1_06,axiom,
    ( unpack1(X1,X2,X3,X4,pack1(X1,X2,X3,X4),inv(pack2(X1,X2,X3,X4)),pack3(X1,X2,X3,X4)) = X1 )).

cnf(unpack1_07,axiom,
    ( unpack1(X1,X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),inv(pack3(X1,X2,X3,X4))) = X1 )).

cnf(unpack2,axiom,
    ( unpack2(X1,X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X2 )).

cnf(unpack2_08,axiom,
    ( unpack2(inv(X1),X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X2 )).

cnf(unpack2_09,axiom,
    ( unpack2(X1,inv(X2),X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X2 )).

cnf(unpack2_10,axiom,
    ( unpack2(X1,X2,inv(X3),X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X2 )).

cnf(unpack2_11,axiom,
    ( unpack2(X1,X2,X3,inv(X4),pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X2 )).

cnf(unpack2_12,axiom,
    ( unpack2(X1,X2,X3,X4,inv(pack1(X1,X2,X3,X4)),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X2 )).

cnf(unpack2_13,axiom,
    ( unpack2(X1,X2,X3,X4,pack1(X1,X2,X3,X4),inv(pack2(X1,X2,X3,X4)),pack3(X1,X2,X3,X4)) = X2 )).

cnf(unpack2_14,axiom,
    ( unpack2(X1,X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),inv(pack3(X1,X2,X3,X4))) = X2 )).

cnf(unpack3,axiom,
    ( unpack3(X1,X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X3 )).

cnf(unpack3_15,axiom,
    ( unpack3(inv(X1),X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X3 )).

cnf(unpack3_16,axiom,
    ( unpack3(X1,inv(X2),X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X3 )).

cnf(unpack3_17,axiom,
    ( unpack3(X1,X2,inv(X3),X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X3 )).

cnf(unpack3_18,axiom,
    ( unpack3(X1,X2,X3,inv(X4),pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X3 )).

cnf(unpack3_19,axiom,
    ( unpack3(X1,X2,X3,X4,inv(pack1(X1,X2,X3,X4)),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X3 )).

cnf(unpack3_20,axiom,
    ( unpack3(X1,X2,X3,X4,pack1(X1,X2,X3,X4),inv(pack2(X1,X2,X3,X4)),pack3(X1,X2,X3,X4)) = X3 )).

cnf(unpack3_21,axiom,
    ( unpack3(X1,X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),inv(pack3(X1,X2,X3,X4))) = X3 )).

cnf(unpack4,axiom,
    ( unpack4(X1,X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X4 )).

cnf(unpack4_22,axiom,
    ( unpack4(inv(X1),X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X4 )).

cnf(unpack4_23,axiom,
    ( unpack4(X1,inv(X2),X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X4 )).

cnf(unpack4_24,axiom,
    ( unpack4(X1,X2,inv(X3),X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X4 )).

cnf(unpack4_25,axiom,
    ( unpack4(X1,X2,X3,inv(X4),pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X4 )).

cnf(unpack4_26,axiom,
    ( unpack4(X1,X2,X3,X4,inv(pack1(X1,X2,X3,X4)),pack2(X1,X2,X3,X4),pack3(X1,X2,X3,X4)) = X4 )).

cnf(unpack4_27,axiom,
    ( unpack4(X1,X2,X3,X4,pack1(X1,X2,X3,X4),inv(pack2(X1,X2,X3,X4)),pack3(X1,X2,X3,X4)) = X4 )).

cnf(unpack4_28,axiom,
    ( unpack4(X1,X2,X3,X4,pack1(X1,X2,X3,X4),pack2(X1,X2,X3,X4),inv(pack3(X1,X2,X3,X4))) = X4 )).

%------------------------------------------------------------------------------
