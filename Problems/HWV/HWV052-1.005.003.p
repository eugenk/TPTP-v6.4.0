%------------------------------------------------------------------------------
% File     : HWV052-1.005.003 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Hardware Verification
% Problem  : Faulty channel 5 3
% Version  : Especial.
% English  : The problem of sending N bits over a faulty channel that can
%            mutilate any one bit. We can use K extra bits to help us do this.
%            Satisfiable means that it is possible, unsatisfiable means that
%            it is not possible.

% Refs     : [Cla10] Claessen (2010), Email to Geoff Sutcliffe
% Source   : [Cla10]
% Names    : fault_5_3 [Cla10]

% Status   : Unsatisfiable
% Rating   : 0.85 v6.4.0, 0.86 v6.3.0, 0.90 v6.1.0, 0.91 v6.0.0, 0.86 v5.5.0, 0.88 v5.4.0, 0.89 v5.3.0, 0.90 v5.2.0, 0.88 v5.1.0, 0.89 v5.0.0, 0.90 v4.1.0
% Syntax   : Number of clauses     :   47 (   1 non-Horn;  46 unit;   1 RR)
%            Number of atoms       :   48 (  48 equality)
%            Maximal clause size   :    2 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :   11 (   2 constant; 0-8 arity)
%            Number of variables   :  227 (   0 singleton)
%            Maximal term depth    :    4 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
%------------------------------------------------------------------------------
cnf(bit_domain,axiom,
    ( X = o
    | X = i )).

cnf(bit_inverse,axiom,
    ( inv(X) != X )).

cnf(unpack1,axiom,
    ( unpack1(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X1 )).

cnf(unpack1_01,axiom,
    ( unpack1(inv(X1),X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X1 )).

cnf(unpack1_02,axiom,
    ( unpack1(X1,inv(X2),X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X1 )).

cnf(unpack1_03,axiom,
    ( unpack1(X1,X2,inv(X3),X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X1 )).

cnf(unpack1_04,axiom,
    ( unpack1(X1,X2,X3,inv(X4),X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X1 )).

cnf(unpack1_05,axiom,
    ( unpack1(X1,X2,X3,X4,inv(X5),pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X1 )).

cnf(unpack1_06,axiom,
    ( unpack1(X1,X2,X3,X4,X5,inv(pack1(X1,X2,X3,X4,X5)),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X1 )).

cnf(unpack1_07,axiom,
    ( unpack1(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),inv(pack2(X1,X2,X3,X4,X5)),pack3(X1,X2,X3,X4,X5)) = X1 )).

cnf(unpack1_08,axiom,
    ( unpack1(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),inv(pack3(X1,X2,X3,X4,X5))) = X1 )).

cnf(unpack2,axiom,
    ( unpack2(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X2 )).

cnf(unpack2_09,axiom,
    ( unpack2(inv(X1),X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X2 )).

cnf(unpack2_10,axiom,
    ( unpack2(X1,inv(X2),X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X2 )).

cnf(unpack2_11,axiom,
    ( unpack2(X1,X2,inv(X3),X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X2 )).

cnf(unpack2_12,axiom,
    ( unpack2(X1,X2,X3,inv(X4),X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X2 )).

cnf(unpack2_13,axiom,
    ( unpack2(X1,X2,X3,X4,inv(X5),pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X2 )).

cnf(unpack2_14,axiom,
    ( unpack2(X1,X2,X3,X4,X5,inv(pack1(X1,X2,X3,X4,X5)),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X2 )).

cnf(unpack2_15,axiom,
    ( unpack2(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),inv(pack2(X1,X2,X3,X4,X5)),pack3(X1,X2,X3,X4,X5)) = X2 )).

cnf(unpack2_16,axiom,
    ( unpack2(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),inv(pack3(X1,X2,X3,X4,X5))) = X2 )).

cnf(unpack3,axiom,
    ( unpack3(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X3 )).

cnf(unpack3_17,axiom,
    ( unpack3(inv(X1),X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X3 )).

cnf(unpack3_18,axiom,
    ( unpack3(X1,inv(X2),X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X3 )).

cnf(unpack3_19,axiom,
    ( unpack3(X1,X2,inv(X3),X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X3 )).

cnf(unpack3_20,axiom,
    ( unpack3(X1,X2,X3,inv(X4),X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X3 )).

cnf(unpack3_21,axiom,
    ( unpack3(X1,X2,X3,X4,inv(X5),pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X3 )).

cnf(unpack3_22,axiom,
    ( unpack3(X1,X2,X3,X4,X5,inv(pack1(X1,X2,X3,X4,X5)),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X3 )).

cnf(unpack3_23,axiom,
    ( unpack3(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),inv(pack2(X1,X2,X3,X4,X5)),pack3(X1,X2,X3,X4,X5)) = X3 )).

cnf(unpack3_24,axiom,
    ( unpack3(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),inv(pack3(X1,X2,X3,X4,X5))) = X3 )).

cnf(unpack4,axiom,
    ( unpack4(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X4 )).

cnf(unpack4_25,axiom,
    ( unpack4(inv(X1),X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X4 )).

cnf(unpack4_26,axiom,
    ( unpack4(X1,inv(X2),X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X4 )).

cnf(unpack4_27,axiom,
    ( unpack4(X1,X2,inv(X3),X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X4 )).

cnf(unpack4_28,axiom,
    ( unpack4(X1,X2,X3,inv(X4),X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X4 )).

cnf(unpack4_29,axiom,
    ( unpack4(X1,X2,X3,X4,inv(X5),pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X4 )).

cnf(unpack4_30,axiom,
    ( unpack4(X1,X2,X3,X4,X5,inv(pack1(X1,X2,X3,X4,X5)),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X4 )).

cnf(unpack4_31,axiom,
    ( unpack4(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),inv(pack2(X1,X2,X3,X4,X5)),pack3(X1,X2,X3,X4,X5)) = X4 )).

cnf(unpack4_32,axiom,
    ( unpack4(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),inv(pack3(X1,X2,X3,X4,X5))) = X4 )).

cnf(unpack5,axiom,
    ( unpack5(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X5 )).

cnf(unpack5_33,axiom,
    ( unpack5(inv(X1),X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X5 )).

cnf(unpack5_34,axiom,
    ( unpack5(X1,inv(X2),X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X5 )).

cnf(unpack5_35,axiom,
    ( unpack5(X1,X2,inv(X3),X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X5 )).

cnf(unpack5_36,axiom,
    ( unpack5(X1,X2,X3,inv(X4),X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X5 )).

cnf(unpack5_37,axiom,
    ( unpack5(X1,X2,X3,X4,inv(X5),pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X5 )).

cnf(unpack5_38,axiom,
    ( unpack5(X1,X2,X3,X4,X5,inv(pack1(X1,X2,X3,X4,X5)),pack2(X1,X2,X3,X4,X5),pack3(X1,X2,X3,X4,X5)) = X5 )).

cnf(unpack5_39,axiom,
    ( unpack5(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),inv(pack2(X1,X2,X3,X4,X5)),pack3(X1,X2,X3,X4,X5)) = X5 )).

cnf(unpack5_40,axiom,
    ( unpack5(X1,X2,X3,X4,X5,pack1(X1,X2,X3,X4,X5),pack2(X1,X2,X3,X4,X5),inv(pack3(X1,X2,X3,X4,X5))) = X5 )).

%------------------------------------------------------------------------------
