%------------------------------------------------------------------------------
% File     : HWV052-1.007.005 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Hardware Verification
% Problem  : Faulty channel 7 5
% Version  : Especial.
% English  : The problem of sending N bits over a faulty channel that can
%            mutilate any one bit. We can use K extra bits to help us do this.
%            Satisfiable means that it is possible, unsatisfiable means that
%            it is not possible.

% Refs     : [Cla10] Claessen (2010), Email to Geoff Sutcliffe
% Source   : [Cla10]
% Names    : fault_7_5 [Cla10]

% Status   : Satisfiable
% Rating   : 0.86 v6.4.0, 0.57 v6.3.0, 0.62 v6.2.0, 0.90 v6.1.0, 0.89 v6.0.0, 1.00 v4.1.0
% Syntax   : Number of clauses     :   93 (   1 non-Horn;  92 unit;   1 RR)
%            Number of atoms       :   94 (  94 equality)
%            Maximal clause size   :    2 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :   15 (   2 constant; 0-12 arity)
%            Number of variables   :  639 (   0 singleton)
%            Maximal term depth    :    4 (   2 average)
% SPC      : CNF_SAT_RFO_EQU_NUE

% Comments :
%------------------------------------------------------------------------------
cnf(bit_domain,axiom,
    ( X = o
    | X = i )).

cnf(bit_inverse,axiom,
    ( inv(X) != X )).

cnf(unpack1,axiom,
    ( unpack1(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_01,axiom,
    ( unpack1(inv(X1),X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_02,axiom,
    ( unpack1(X1,inv(X2),X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_03,axiom,
    ( unpack1(X1,X2,inv(X3),X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_04,axiom,
    ( unpack1(X1,X2,X3,inv(X4),X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_05,axiom,
    ( unpack1(X1,X2,X3,X4,inv(X5),X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_06,axiom,
    ( unpack1(X1,X2,X3,X4,X5,inv(X6),X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_07,axiom,
    ( unpack1(X1,X2,X3,X4,X5,X6,inv(X7),pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_08,axiom,
    ( unpack1(X1,X2,X3,X4,X5,X6,X7,inv(pack1(X1,X2,X3,X4,X5,X6,X7)),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_09,axiom,
    ( unpack1(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),inv(pack2(X1,X2,X3,X4,X5,X6,X7)),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_10,axiom,
    ( unpack1(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),inv(pack3(X1,X2,X3,X4,X5,X6,X7)),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_11,axiom,
    ( unpack1(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),inv(pack4(X1,X2,X3,X4,X5,X6,X7)),pack5(X1,X2,X3,X4,X5,X6,X7)) = X1 )).

cnf(unpack1_12,axiom,
    ( unpack1(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),inv(pack5(X1,X2,X3,X4,X5,X6,X7))) = X1 )).

cnf(unpack2,axiom,
    ( unpack2(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_13,axiom,
    ( unpack2(inv(X1),X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_14,axiom,
    ( unpack2(X1,inv(X2),X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_15,axiom,
    ( unpack2(X1,X2,inv(X3),X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_16,axiom,
    ( unpack2(X1,X2,X3,inv(X4),X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_17,axiom,
    ( unpack2(X1,X2,X3,X4,inv(X5),X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_18,axiom,
    ( unpack2(X1,X2,X3,X4,X5,inv(X6),X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_19,axiom,
    ( unpack2(X1,X2,X3,X4,X5,X6,inv(X7),pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_20,axiom,
    ( unpack2(X1,X2,X3,X4,X5,X6,X7,inv(pack1(X1,X2,X3,X4,X5,X6,X7)),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_21,axiom,
    ( unpack2(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),inv(pack2(X1,X2,X3,X4,X5,X6,X7)),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_22,axiom,
    ( unpack2(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),inv(pack3(X1,X2,X3,X4,X5,X6,X7)),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_23,axiom,
    ( unpack2(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),inv(pack4(X1,X2,X3,X4,X5,X6,X7)),pack5(X1,X2,X3,X4,X5,X6,X7)) = X2 )).

cnf(unpack2_24,axiom,
    ( unpack2(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),inv(pack5(X1,X2,X3,X4,X5,X6,X7))) = X2 )).

cnf(unpack3,axiom,
    ( unpack3(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_25,axiom,
    ( unpack3(inv(X1),X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_26,axiom,
    ( unpack3(X1,inv(X2),X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_27,axiom,
    ( unpack3(X1,X2,inv(X3),X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_28,axiom,
    ( unpack3(X1,X2,X3,inv(X4),X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_29,axiom,
    ( unpack3(X1,X2,X3,X4,inv(X5),X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_30,axiom,
    ( unpack3(X1,X2,X3,X4,X5,inv(X6),X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_31,axiom,
    ( unpack3(X1,X2,X3,X4,X5,X6,inv(X7),pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_32,axiom,
    ( unpack3(X1,X2,X3,X4,X5,X6,X7,inv(pack1(X1,X2,X3,X4,X5,X6,X7)),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_33,axiom,
    ( unpack3(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),inv(pack2(X1,X2,X3,X4,X5,X6,X7)),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_34,axiom,
    ( unpack3(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),inv(pack3(X1,X2,X3,X4,X5,X6,X7)),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_35,axiom,
    ( unpack3(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),inv(pack4(X1,X2,X3,X4,X5,X6,X7)),pack5(X1,X2,X3,X4,X5,X6,X7)) = X3 )).

cnf(unpack3_36,axiom,
    ( unpack3(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),inv(pack5(X1,X2,X3,X4,X5,X6,X7))) = X3 )).

cnf(unpack4,axiom,
    ( unpack4(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_37,axiom,
    ( unpack4(inv(X1),X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_38,axiom,
    ( unpack4(X1,inv(X2),X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_39,axiom,
    ( unpack4(X1,X2,inv(X3),X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_40,axiom,
    ( unpack4(X1,X2,X3,inv(X4),X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_41,axiom,
    ( unpack4(X1,X2,X3,X4,inv(X5),X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_42,axiom,
    ( unpack4(X1,X2,X3,X4,X5,inv(X6),X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_43,axiom,
    ( unpack4(X1,X2,X3,X4,X5,X6,inv(X7),pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_44,axiom,
    ( unpack4(X1,X2,X3,X4,X5,X6,X7,inv(pack1(X1,X2,X3,X4,X5,X6,X7)),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_45,axiom,
    ( unpack4(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),inv(pack2(X1,X2,X3,X4,X5,X6,X7)),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_46,axiom,
    ( unpack4(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),inv(pack3(X1,X2,X3,X4,X5,X6,X7)),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_47,axiom,
    ( unpack4(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),inv(pack4(X1,X2,X3,X4,X5,X6,X7)),pack5(X1,X2,X3,X4,X5,X6,X7)) = X4 )).

cnf(unpack4_48,axiom,
    ( unpack4(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),inv(pack5(X1,X2,X3,X4,X5,X6,X7))) = X4 )).

cnf(unpack5,axiom,
    ( unpack5(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_49,axiom,
    ( unpack5(inv(X1),X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_50,axiom,
    ( unpack5(X1,inv(X2),X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_51,axiom,
    ( unpack5(X1,X2,inv(X3),X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_52,axiom,
    ( unpack5(X1,X2,X3,inv(X4),X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_53,axiom,
    ( unpack5(X1,X2,X3,X4,inv(X5),X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_54,axiom,
    ( unpack5(X1,X2,X3,X4,X5,inv(X6),X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_55,axiom,
    ( unpack5(X1,X2,X3,X4,X5,X6,inv(X7),pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_56,axiom,
    ( unpack5(X1,X2,X3,X4,X5,X6,X7,inv(pack1(X1,X2,X3,X4,X5,X6,X7)),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_57,axiom,
    ( unpack5(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),inv(pack2(X1,X2,X3,X4,X5,X6,X7)),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_58,axiom,
    ( unpack5(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),inv(pack3(X1,X2,X3,X4,X5,X6,X7)),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_59,axiom,
    ( unpack5(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),inv(pack4(X1,X2,X3,X4,X5,X6,X7)),pack5(X1,X2,X3,X4,X5,X6,X7)) = X5 )).

cnf(unpack5_60,axiom,
    ( unpack5(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),inv(pack5(X1,X2,X3,X4,X5,X6,X7))) = X5 )).

cnf(unpack6,axiom,
    ( unpack6(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_61,axiom,
    ( unpack6(inv(X1),X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_62,axiom,
    ( unpack6(X1,inv(X2),X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_63,axiom,
    ( unpack6(X1,X2,inv(X3),X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_64,axiom,
    ( unpack6(X1,X2,X3,inv(X4),X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_65,axiom,
    ( unpack6(X1,X2,X3,X4,inv(X5),X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_66,axiom,
    ( unpack6(X1,X2,X3,X4,X5,inv(X6),X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_67,axiom,
    ( unpack6(X1,X2,X3,X4,X5,X6,inv(X7),pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_68,axiom,
    ( unpack6(X1,X2,X3,X4,X5,X6,X7,inv(pack1(X1,X2,X3,X4,X5,X6,X7)),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_69,axiom,
    ( unpack6(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),inv(pack2(X1,X2,X3,X4,X5,X6,X7)),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_70,axiom,
    ( unpack6(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),inv(pack3(X1,X2,X3,X4,X5,X6,X7)),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_71,axiom,
    ( unpack6(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),inv(pack4(X1,X2,X3,X4,X5,X6,X7)),pack5(X1,X2,X3,X4,X5,X6,X7)) = X6 )).

cnf(unpack6_72,axiom,
    ( unpack6(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),inv(pack5(X1,X2,X3,X4,X5,X6,X7))) = X6 )).

cnf(unpack7,axiom,
    ( unpack7(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_73,axiom,
    ( unpack7(inv(X1),X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_74,axiom,
    ( unpack7(X1,inv(X2),X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_75,axiom,
    ( unpack7(X1,X2,inv(X3),X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_76,axiom,
    ( unpack7(X1,X2,X3,inv(X4),X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_77,axiom,
    ( unpack7(X1,X2,X3,X4,inv(X5),X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_78,axiom,
    ( unpack7(X1,X2,X3,X4,X5,inv(X6),X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_79,axiom,
    ( unpack7(X1,X2,X3,X4,X5,X6,inv(X7),pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_80,axiom,
    ( unpack7(X1,X2,X3,X4,X5,X6,X7,inv(pack1(X1,X2,X3,X4,X5,X6,X7)),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_81,axiom,
    ( unpack7(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),inv(pack2(X1,X2,X3,X4,X5,X6,X7)),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_82,axiom,
    ( unpack7(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),inv(pack3(X1,X2,X3,X4,X5,X6,X7)),pack4(X1,X2,X3,X4,X5,X6,X7),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_83,axiom,
    ( unpack7(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),inv(pack4(X1,X2,X3,X4,X5,X6,X7)),pack5(X1,X2,X3,X4,X5,X6,X7)) = X7 )).

cnf(unpack7_84,axiom,
    ( unpack7(X1,X2,X3,X4,X5,X6,X7,pack1(X1,X2,X3,X4,X5,X6,X7),pack2(X1,X2,X3,X4,X5,X6,X7),pack3(X1,X2,X3,X4,X5,X6,X7),pack4(X1,X2,X3,X4,X5,X6,X7),inv(pack5(X1,X2,X3,X4,X5,X6,X7))) = X7 )).

%------------------------------------------------------------------------------
