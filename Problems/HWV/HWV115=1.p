%------------------------------------------------------------------------------
% File     : HWV115=1 : TPTP v6.4.0. Released v6.1.0.
% Domain   : Hardware Verification
% Problem  : dmu_rmu_rrm property 1 cone of influence 10_b50
% Version  : Especial.
% English  : Verification of a property of the SPARCT2 RTL hardware design.

% Refs     : [Kha14] Khasidashvili (2014), Email to Geoff Sutcliffe
% Source   : [Kha14]
% Names    : dmu_rmu_rrm_prop1_cone10_b50 [Kha14]

% Status   : Theorem
% Rating   : 1.00 v6.1.0
% Syntax   : Number of formulae    : 2130 ( 459 unit; 734 type)
%            Number of atoms       : 6856 (  51 equality)
%            Maximal formula depth :   75 (   4 average)
%            Number of connectives : 6180 ( 720   ~; 125   |;2021   &)
%                                         (2700 <=>; 614  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  884 ( 681   >; 203   *;   0   +;   0  <<)
%            Number of predicates  : 1422 ( 739 propositional; 0-2 arity)
%            Number of functors    :  611 ( 611 constant; 0-0 arity)
%            Number of variables   : 1318 (   0 sgn;1318   !;   0   ?)
%                                         (1318   :;   0  !>;   0  ?*)
%            Maximal term depth    :    1 (   1 average)
%            Arithmetic symbols    :  752 (   1 prd;   0 fun; 560 num; 191 var)
% SPC      : TF0_THM_EQU_ARI

% Comments : Copyright 2013 Moshe Emmer and Zurab Khasidashvili
%            Licensed under the Apache License, Version 2.0 (the "License");
%            you may not use this file except in compliance with the License.
%            You may obtain a copy of the License at
%                http://www.apache.org/licenses/LICENSE-2.0
%            Unless required by applicable law or agreed to in writing,
%            software distributed under the License is distributed on an "AS
%            IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
%            express or implied. See the License for the specific language
%            governing permissions and limitations under the License.
%------------------------------------------------------------------------------
tff(state_type,type,(
    state_type: $tType )).

tff(constB0_type,type,(
    constB0: state_type )).

tff(constB1_type,type,(
    constB1: state_type )).

tff(constB2_type,type,(
    constB2: state_type )).

tff(constB3_type,type,(
    constB3: state_type )).

tff(constB4_type,type,(
    constB4: state_type )).

tff(constB5_type,type,(
    constB5: state_type )).

tff(constB6_type,type,(
    constB6: state_type )).

tff(constB7_type,type,(
    constB7: state_type )).

tff(constB8_type,type,(
    constB8: state_type )).

tff(constB9_type,type,(
    constB9: state_type )).

tff(constB10_type,type,(
    constB10: state_type )).

tff(constB11_type,type,(
    constB11: state_type )).

tff(constB12_type,type,(
    constB12: state_type )).

tff(constB13_type,type,(
    constB13: state_type )).

tff(constB14_type,type,(
    constB14: state_type )).

tff(constB15_type,type,(
    constB15: state_type )).

tff(constB16_type,type,(
    constB16: state_type )).

tff(constB17_type,type,(
    constB17: state_type )).

tff(constB18_type,type,(
    constB18: state_type )).

tff(constB19_type,type,(
    constB19: state_type )).

tff(constB20_type,type,(
    constB20: state_type )).

tff(constB21_type,type,(
    constB21: state_type )).

tff(constB22_type,type,(
    constB22: state_type )).

tff(constB23_type,type,(
    constB23: state_type )).

tff(constB24_type,type,(
    constB24: state_type )).

tff(constB25_type,type,(
    constB25: state_type )).

tff(constB26_type,type,(
    constB26: state_type )).

tff(constB27_type,type,(
    constB27: state_type )).

tff(constB28_type,type,(
    constB28: state_type )).

tff(func_def_589,type,(
    constB29: state_type )).

tff(func_def_590,type,(
    constB30: state_type )).

tff(func_def_591,type,(
    constB31: state_type )).

tff(func_def_592,type,(
    constB32: state_type )).

tff(func_def_593,type,(
    constB33: state_type )).

tff(func_def_594,type,(
    constB34: state_type )).

tff(func_def_595,type,(
    constB35: state_type )).

tff(func_def_596,type,(
    constB36: state_type )).

tff(func_def_597,type,(
    constB37: state_type )).

tff(func_def_598,type,(
    constB38: state_type )).

tff(func_def_599,type,(
    constB39: state_type )).

tff(func_def_600,type,(
    constB40: state_type )).

tff(func_def_601,type,(
    constB41: state_type )).

tff(func_def_602,type,(
    constB42: state_type )).

tff(func_def_603,type,(
    constB43: state_type )).

tff(func_def_604,type,(
    constB44: state_type )).

tff(func_def_605,type,(
    constB45: state_type )).

tff(func_def_606,type,(
    constB46: state_type )).

tff(func_def_607,type,(
    constB47: state_type )).

tff(func_def_608,type,(
    constB48: state_type )).

tff(func_def_609,type,(
    constB49: state_type )).

tff(func_def_610,type,(
    constB50: state_type )).

tff(pred_def_1,type,(
    v9: state_type > $o )).

tff(pred_def_2,type,(
    v11: state_type > $o )).

tff(pred_def_3,type,(
    v22: state_type > $o )).

tff(pred_def_4,type,(
    v24: state_type > $o )).

tff(pred_def_5,type,(
    v34: state_type > $o )).

tff(pred_def_6,type,(
    v38: state_type > $o )).

tff(pred_def_7,type,(
    v40: state_type > $o )).

tff(pred_def_8,type,(
    v36: state_type > $o )).

tff(pred_def_9,type,(
    b00: $int > $o )).

tff(pred_def_10,type,(
    v28: ( state_type * $int ) > $o )).

tff(pred_def_11,type,(
    v52: state_type > $o )).

tff(pred_def_12,type,(
    v30: state_type > $o )).

tff(pred_def_13,type,(
    v53: state_type > $o )).

tff(pred_def_14,type,(
    v51: state_type > $o )).

tff(pred_def_15,type,(
    v7: ( state_type * $int ) > $o )).

tff(pred_def_16,type,(
    v54: state_type > $o )).

tff(pred_def_17,type,(
    v50: state_type > $o )).

tff(pred_def_18,type,(
    v47: state_type > $o )).

tff(pred_def_19,type,(
    v64: state_type > $o )).

tff(pred_def_20,type,(
    v112: state_type > $o )).

tff(pred_def_21,type,(
    v1: state_type > $o )).

tff(pred_def_22,type,(
    v110: state_type > $o )).

tff(pred_def_23,type,(
    b00000000001: $int > $o )).

tff(pred_def_24,type,(
    v107: ( state_type * $int ) > $o )).

tff(pred_def_25,type,(
    v119: state_type > $o )).

tff(pred_def_26,type,(
    nextState: ( state_type * state_type ) > $o )).

tff(pred_def_27,type,(
    v117: state_type > $o )).

tff(pred_def_28,type,(
    v116: state_type > $o )).

tff(pred_def_29,type,(
    v115: state_type > $o )).

tff(pred_def_30,type,(
    v126: state_type > $o )).

tff(pred_def_31,type,(
    v123: ( state_type * $int ) > $o )).

tff(pred_def_33,type,(
    v103: ( state_type * $int ) > $o )).

tff(pred_def_34,type,(
    v125: ( state_type * $int ) > $o )).

tff(pred_def_35,type,(
    v114: ( state_type * $int ) > $o )).

tff(pred_def_36,type,(
    undeclared: $o )).

tff(pred_def_37,type,(
    v129: ( state_type * $int ) > $o )).

tff(pred_def_38,type,(
    v105: ( state_type * $int ) > $o )).

tff(pred_def_39,type,(
    v132: state_type > $o )).

tff(pred_def_40,type,(
    v133: ( state_type * $int ) > $o )).

tff(pred_def_41,type,(
    v136: ( state_type * $int ) > $o )).

tff(pred_def_42,type,(
    v43: state_type > $o )).

tff(pred_def_43,type,(
    v135: state_type > $o )).

tff(pred_def_44,type,(
    v138: ( state_type * $int ) > $o )).

tff(pred_def_45,type,(
    b01: $int > $o )).

tff(pred_def_46,type,(
    v137: state_type > $o )).

tff(pred_def_47,type,(
    v139: ( state_type * $int ) > $o )).

tff(pred_def_48,type,(
    v142: ( state_type * $int ) > $o )).

tff(pred_def_49,type,(
    b10: $int > $o )).

tff(pred_def_50,type,(
    v141: state_type > $o )).

tff(pred_def_51,type,(
    v143: ( state_type * $int ) > $o )).

tff(pred_def_52,type,(
    v146: ( state_type * $int ) > $o )).

tff(pred_def_53,type,(
    b11: $int > $o )).

tff(pred_def_54,type,(
    v145: state_type > $o )).

tff(pred_def_55,type,(
    v134: ( state_type * $int ) > $o )).

tff(pred_def_56,type,(
    v131: ( state_type * $int ) > $o )).

tff(pred_def_57,type,(
    v152: state_type > $o )).

tff(pred_def_58,type,(
    v150: state_type > $o )).

tff(pred_def_59,type,(
    v149: state_type > $o )).

tff(pred_def_60,type,(
    v148: ( state_type * $int ) > $o )).

tff(pred_def_61,type,(
    v160: state_type > $o )).

tff(pred_def_62,type,(
    v158: state_type > $o )).

tff(pred_def_63,type,(
    v157: state_type > $o )).

tff(pred_def_64,type,(
    v156: ( state_type * $int ) > $o )).

tff(pred_def_65,type,(
    v168: state_type > $o )).

tff(pred_def_66,type,(
    v166: state_type > $o )).

tff(pred_def_67,type,(
    v165: state_type > $o )).

tff(pred_def_68,type,(
    v164: ( state_type * $int ) > $o )).

tff(pred_def_69,type,(
    v176: state_type > $o )).

tff(pred_def_70,type,(
    v174: state_type > $o )).

tff(pred_def_71,type,(
    v173: state_type > $o )).

tff(pred_def_72,type,(
    v172: ( state_type * $int ) > $o )).

tff(pred_def_73,type,(
    v184: state_type > $o )).

tff(pred_def_74,type,(
    v182: state_type > $o )).

tff(pred_def_75,type,(
    v181: state_type > $o )).

tff(pred_def_76,type,(
    v180: ( state_type * $int ) > $o )).

tff(pred_def_77,type,(
    v192: state_type > $o )).

tff(pred_def_78,type,(
    v190: state_type > $o )).

tff(pred_def_79,type,(
    v189: state_type > $o )).

tff(pred_def_80,type,(
    v188: ( state_type * $int ) > $o )).

tff(pred_def_81,type,(
    v200: state_type > $o )).

tff(pred_def_82,type,(
    v198: state_type > $o )).

tff(pred_def_83,type,(
    v197: state_type > $o )).

tff(pred_def_84,type,(
    v196: ( state_type * $int ) > $o )).

tff(pred_def_85,type,(
    v208: state_type > $o )).

tff(pred_def_86,type,(
    v206: state_type > $o )).

tff(pred_def_87,type,(
    v205: state_type > $o )).

tff(pred_def_88,type,(
    v204: ( state_type * $int ) > $o )).

tff(pred_def_89,type,(
    v101: state_type > $o )).

tff(pred_def_90,type,(
    v214: ( state_type * $int ) > $o )).

tff(pred_def_91,type,(
    v216: ( state_type * $int ) > $o )).

tff(pred_def_92,type,(
    v212: ( state_type * $int ) > $o )).

tff(pred_def_93,type,(
    b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx: $int > $o )).

tff(pred_def_94,type,(
    v94: ( state_type * $int ) > $o )).

tff(pred_def_95,type,(
    v218: ( state_type * $int ) > $o )).

tff(pred_def_96,type,(
    v235: ( state_type * $int ) > $o )).

tff(pred_def_97,type,(
    v99: ( state_type * $int ) > $o )).

tff(pred_def_98,type,(
    v239: state_type > $o )).

tff(pred_def_99,type,(
    v241: ( state_type * $int ) > $o )).

tff(pred_def_100,type,(
    v242: ( state_type * $int ) > $o )).

tff(pred_def_101,type,(
    v237: ( state_type * $int ) > $o )).

tff(pred_def_102,type,(
    v247: state_type > $o )).

tff(pred_def_103,type,(
    v246: state_type > $o )).

tff(pred_def_104,type,(
    v255: state_type > $o )).

tff(pred_def_105,type,(
    v260: ( state_type * $int ) > $o )).

tff(pred_def_106,type,(
    v259: state_type > $o )).

tff(pred_def_107,type,(
    v262: ( state_type * $int ) > $o )).

tff(pred_def_108,type,(
    v261: state_type > $o )).

tff(pred_def_109,type,(
    v258: state_type > $o )).

tff(pred_def_110,type,(
    v264: ( state_type * $int ) > $o )).

tff(pred_def_111,type,(
    v263: state_type > $o )).

tff(pred_def_112,type,(
    v257: state_type > $o )).

tff(pred_def_113,type,(
    v265: state_type > $o )).

tff(pred_def_114,type,(
    v256: state_type > $o )).

tff(pred_def_115,type,(
    v254: state_type > $o )).

tff(pred_def_116,type,(
    v269: ( state_type * $int ) > $o )).

tff(pred_def_117,type,(
    v268: state_type > $o )).

tff(pred_def_118,type,(
    v267: state_type > $o )).

tff(pred_def_119,type,(
    v266: state_type > $o )).

tff(pred_def_120,type,(
    v251: state_type > $o )).

tff(pred_def_121,type,(
    v253: state_type > $o )).

tff(pred_def_122,type,(
    v245: state_type > $o )).

tff(pred_def_123,type,(
    v273: ( state_type * $int ) > $o )).

tff(pred_def_124,type,(
    b0000000000000000000000000000000000000000000000000000000000000000000000: $int > $o )).

tff(pred_def_125,type,(
    v270: ( state_type * $int ) > $o )).

tff(pred_def_126,type,(
    v272: ( state_type * $int ) > $o )).

tff(pred_def_127,type,(
    v244: ( state_type * $int ) > $o )).

tff(pred_def_128,type,(
    v279: state_type > $o )).

tff(pred_def_129,type,(
    v281: ( state_type * $int ) > $o )).

tff(pred_def_130,type,(
    v282: ( state_type * $int ) > $o )).

tff(pred_def_131,type,(
    v277: ( state_type * $int ) > $o )).

tff(pred_def_132,type,(
    v286: state_type > $o )).

tff(pred_def_133,type,(
    v288: ( state_type * $int ) > $o )).

tff(pred_def_134,type,(
    v289: ( state_type * $int ) > $o )).

tff(pred_def_135,type,(
    v284: ( state_type * $int ) > $o )).

tff(pred_def_136,type,(
    v295: state_type > $o )).

tff(pred_def_137,type,(
    v293: state_type > $o )).

tff(pred_def_138,type,(
    v306: ( state_type * $int ) > $o )).

tff(pred_def_139,type,(
    v305: state_type > $o )).

tff(pred_def_140,type,(
    v308: ( state_type * $int ) > $o )).

tff(pred_def_141,type,(
    v307: state_type > $o )).

tff(pred_def_142,type,(
    v304: state_type > $o )).

tff(pred_def_143,type,(
    v310: ( state_type * $int ) > $o )).

tff(pred_def_144,type,(
    v309: state_type > $o )).

tff(pred_def_145,type,(
    v303: state_type > $o )).

tff(pred_def_146,type,(
    v311: state_type > $o )).

tff(pred_def_147,type,(
    v302: state_type > $o )).

tff(pred_def_148,type,(
    v301: state_type > $o )).

tff(pred_def_149,type,(
    v315: ( state_type * $int ) > $o )).

tff(pred_def_150,type,(
    v314: state_type > $o )).

tff(pred_def_151,type,(
    v313: state_type > $o )).

tff(pred_def_152,type,(
    v312: state_type > $o )).

tff(pred_def_153,type,(
    v298: state_type > $o )).

tff(pred_def_154,type,(
    v300: state_type > $o )).

tff(pred_def_155,type,(
    v292: state_type > $o )).

tff(pred_def_156,type,(
    v319: ( state_type * $int ) > $o )).

tff(pred_def_157,type,(
    v316: ( state_type * $int ) > $o )).

tff(pred_def_158,type,(
    v318: ( state_type * $int ) > $o )).

tff(pred_def_159,type,(
    v291: ( state_type * $int ) > $o )).

tff(pred_def_160,type,(
    v325: state_type > $o )).

tff(pred_def_161,type,(
    v327: ( state_type * $int ) > $o )).

tff(pred_def_162,type,(
    v328: ( state_type * $int ) > $o )).

tff(pred_def_163,type,(
    v323: ( state_type * $int ) > $o )).

tff(pred_def_164,type,(
    v332: state_type > $o )).

tff(pred_def_165,type,(
    v334: ( state_type * $int ) > $o )).

tff(pred_def_166,type,(
    v335: ( state_type * $int ) > $o )).

tff(pred_def_167,type,(
    v330: ( state_type * $int ) > $o )).

tff(pred_def_168,type,(
    v341: state_type > $o )).

tff(pred_def_169,type,(
    v339: state_type > $o )).

tff(pred_def_170,type,(
    v352: ( state_type * $int ) > $o )).

tff(pred_def_171,type,(
    v351: state_type > $o )).

tff(pred_def_172,type,(
    v354: ( state_type * $int ) > $o )).

tff(pred_def_173,type,(
    v353: state_type > $o )).

tff(pred_def_174,type,(
    v350: state_type > $o )).

tff(pred_def_175,type,(
    v356: ( state_type * $int ) > $o )).

tff(pred_def_176,type,(
    v355: state_type > $o )).

tff(pred_def_177,type,(
    v349: state_type > $o )).

tff(pred_def_178,type,(
    v357: state_type > $o )).

tff(pred_def_179,type,(
    v348: state_type > $o )).

tff(pred_def_180,type,(
    v347: state_type > $o )).

tff(pred_def_181,type,(
    v361: ( state_type * $int ) > $o )).

tff(pred_def_182,type,(
    v360: state_type > $o )).

tff(pred_def_183,type,(
    v359: state_type > $o )).

tff(pred_def_184,type,(
    v358: state_type > $o )).

tff(pred_def_185,type,(
    v344: state_type > $o )).

tff(pred_def_186,type,(
    v346: state_type > $o )).

tff(pred_def_187,type,(
    v338: state_type > $o )).

tff(pred_def_188,type,(
    v365: ( state_type * $int ) > $o )).

tff(pred_def_189,type,(
    v362: ( state_type * $int ) > $o )).

tff(pred_def_190,type,(
    v364: ( state_type * $int ) > $o )).

tff(pred_def_191,type,(
    v337: ( state_type * $int ) > $o )).

tff(pred_def_192,type,(
    v371: state_type > $o )).

tff(pred_def_193,type,(
    v373: ( state_type * $int ) > $o )).

tff(pred_def_194,type,(
    v374: ( state_type * $int ) > $o )).

tff(pred_def_195,type,(
    v369: ( state_type * $int ) > $o )).

tff(pred_def_196,type,(
    v378: state_type > $o )).

tff(pred_def_197,type,(
    v380: ( state_type * $int ) > $o )).

tff(pred_def_198,type,(
    v381: ( state_type * $int ) > $o )).

tff(pred_def_199,type,(
    v376: ( state_type * $int ) > $o )).

tff(pred_def_200,type,(
    v387: state_type > $o )).

tff(pred_def_201,type,(
    v385: state_type > $o )).

tff(pred_def_202,type,(
    v398: ( state_type * $int ) > $o )).

tff(pred_def_203,type,(
    v397: state_type > $o )).

tff(pred_def_204,type,(
    v400: ( state_type * $int ) > $o )).

tff(pred_def_205,type,(
    v399: state_type > $o )).

tff(pred_def_206,type,(
    v396: state_type > $o )).

tff(pred_def_207,type,(
    v402: ( state_type * $int ) > $o )).

tff(pred_def_208,type,(
    v401: state_type > $o )).

tff(pred_def_209,type,(
    v395: state_type > $o )).

tff(pred_def_210,type,(
    v403: state_type > $o )).

tff(pred_def_211,type,(
    v394: state_type > $o )).

tff(pred_def_212,type,(
    v393: state_type > $o )).

tff(pred_def_213,type,(
    v407: ( state_type * $int ) > $o )).

tff(pred_def_214,type,(
    v406: state_type > $o )).

tff(pred_def_215,type,(
    v405: state_type > $o )).

tff(pred_def_216,type,(
    v404: state_type > $o )).

tff(pred_def_217,type,(
    v390: state_type > $o )).

tff(pred_def_218,type,(
    v392: state_type > $o )).

tff(pred_def_219,type,(
    v384: state_type > $o )).

tff(pred_def_220,type,(
    v411: ( state_type * $int ) > $o )).

tff(pred_def_221,type,(
    v408: ( state_type * $int ) > $o )).

tff(pred_def_222,type,(
    v410: ( state_type * $int ) > $o )).

tff(pred_def_223,type,(
    v383: ( state_type * $int ) > $o )).

tff(pred_def_224,type,(
    v417: state_type > $o )).

tff(pred_def_225,type,(
    v419: ( state_type * $int ) > $o )).

tff(pred_def_226,type,(
    v420: ( state_type * $int ) > $o )).

tff(pred_def_227,type,(
    v415: ( state_type * $int ) > $o )).

tff(pred_def_228,type,(
    v424: state_type > $o )).

tff(pred_def_229,type,(
    v426: ( state_type * $int ) > $o )).

tff(pred_def_230,type,(
    v427: ( state_type * $int ) > $o )).

tff(pred_def_231,type,(
    v422: ( state_type * $int ) > $o )).

tff(pred_def_232,type,(
    v433: state_type > $o )).

tff(pred_def_233,type,(
    v431: state_type > $o )).

tff(pred_def_234,type,(
    v444: ( state_type * $int ) > $o )).

tff(pred_def_235,type,(
    v443: state_type > $o )).

tff(pred_def_236,type,(
    v446: ( state_type * $int ) > $o )).

tff(pred_def_237,type,(
    v445: state_type > $o )).

tff(pred_def_238,type,(
    v442: state_type > $o )).

tff(pred_def_239,type,(
    v448: ( state_type * $int ) > $o )).

tff(pred_def_240,type,(
    v447: state_type > $o )).

tff(pred_def_241,type,(
    v441: state_type > $o )).

tff(pred_def_242,type,(
    v449: state_type > $o )).

tff(pred_def_243,type,(
    v440: state_type > $o )).

tff(pred_def_244,type,(
    v439: state_type > $o )).

tff(pred_def_245,type,(
    v453: ( state_type * $int ) > $o )).

tff(pred_def_246,type,(
    v452: state_type > $o )).

tff(pred_def_247,type,(
    v451: state_type > $o )).

tff(pred_def_248,type,(
    v450: state_type > $o )).

tff(pred_def_249,type,(
    v436: state_type > $o )).

tff(pred_def_250,type,(
    v438: state_type > $o )).

tff(pred_def_251,type,(
    v430: state_type > $o )).

tff(pred_def_252,type,(
    v457: ( state_type * $int ) > $o )).

tff(pred_def_253,type,(
    v454: ( state_type * $int ) > $o )).

tff(pred_def_254,type,(
    v456: ( state_type * $int ) > $o )).

tff(pred_def_255,type,(
    v429: ( state_type * $int ) > $o )).

tff(pred_def_256,type,(
    v463: state_type > $o )).

tff(pred_def_257,type,(
    v465: ( state_type * $int ) > $o )).

tff(pred_def_258,type,(
    v466: ( state_type * $int ) > $o )).

tff(pred_def_259,type,(
    v461: ( state_type * $int ) > $o )).

tff(pred_def_260,type,(
    v470: state_type > $o )).

tff(pred_def_261,type,(
    v472: ( state_type * $int ) > $o )).

tff(pred_def_262,type,(
    v473: ( state_type * $int ) > $o )).

tff(pred_def_263,type,(
    v468: ( state_type * $int ) > $o )).

tff(pred_def_264,type,(
    v479: state_type > $o )).

tff(pred_def_265,type,(
    v477: state_type > $o )).

tff(pred_def_266,type,(
    v490: ( state_type * $int ) > $o )).

tff(pred_def_267,type,(
    v489: state_type > $o )).

tff(pred_def_268,type,(
    v492: ( state_type * $int ) > $o )).

tff(pred_def_269,type,(
    v491: state_type > $o )).

tff(pred_def_270,type,(
    v488: state_type > $o )).

tff(pred_def_271,type,(
    v494: ( state_type * $int ) > $o )).

tff(pred_def_272,type,(
    v493: state_type > $o )).

tff(pred_def_273,type,(
    v487: state_type > $o )).

tff(pred_def_274,type,(
    v495: state_type > $o )).

tff(pred_def_275,type,(
    v486: state_type > $o )).

tff(pred_def_276,type,(
    v485: state_type > $o )).

tff(pred_def_277,type,(
    v499: ( state_type * $int ) > $o )).

tff(pred_def_278,type,(
    v498: state_type > $o )).

tff(pred_def_279,type,(
    v497: state_type > $o )).

tff(pred_def_280,type,(
    v496: state_type > $o )).

tff(pred_def_281,type,(
    v482: state_type > $o )).

tff(pred_def_282,type,(
    v484: state_type > $o )).

tff(pred_def_283,type,(
    v476: state_type > $o )).

tff(pred_def_284,type,(
    v503: ( state_type * $int ) > $o )).

tff(pred_def_285,type,(
    v500: ( state_type * $int ) > $o )).

tff(pred_def_286,type,(
    v502: ( state_type * $int ) > $o )).

tff(pred_def_287,type,(
    v475: ( state_type * $int ) > $o )).

tff(pred_def_288,type,(
    v509: state_type > $o )).

tff(pred_def_289,type,(
    v511: ( state_type * $int ) > $o )).

tff(pred_def_290,type,(
    v512: ( state_type * $int ) > $o )).

tff(pred_def_291,type,(
    v507: ( state_type * $int ) > $o )).

tff(pred_def_292,type,(
    v516: state_type > $o )).

tff(pred_def_293,type,(
    v518: ( state_type * $int ) > $o )).

tff(pred_def_294,type,(
    v519: ( state_type * $int ) > $o )).

tff(pred_def_295,type,(
    v514: ( state_type * $int ) > $o )).

tff(pred_def_296,type,(
    v525: state_type > $o )).

tff(pred_def_297,type,(
    v523: state_type > $o )).

tff(pred_def_298,type,(
    v536: ( state_type * $int ) > $o )).

tff(pred_def_299,type,(
    v535: state_type > $o )).

tff(pred_def_300,type,(
    v538: ( state_type * $int ) > $o )).

tff(pred_def_301,type,(
    v537: state_type > $o )).

tff(pred_def_302,type,(
    v534: state_type > $o )).

tff(pred_def_303,type,(
    v540: ( state_type * $int ) > $o )).

tff(pred_def_304,type,(
    v539: state_type > $o )).

tff(pred_def_305,type,(
    v533: state_type > $o )).

tff(pred_def_306,type,(
    v541: state_type > $o )).

tff(pred_def_307,type,(
    v532: state_type > $o )).

tff(pred_def_308,type,(
    v531: state_type > $o )).

tff(pred_def_309,type,(
    v545: ( state_type * $int ) > $o )).

tff(pred_def_310,type,(
    v544: state_type > $o )).

tff(pred_def_311,type,(
    v543: state_type > $o )).

tff(pred_def_312,type,(
    v542: state_type > $o )).

tff(pred_def_313,type,(
    v528: state_type > $o )).

tff(pred_def_314,type,(
    v530: state_type > $o )).

tff(pred_def_315,type,(
    v522: state_type > $o )).

tff(pred_def_316,type,(
    v549: ( state_type * $int ) > $o )).

tff(pred_def_317,type,(
    v546: ( state_type * $int ) > $o )).

tff(pred_def_318,type,(
    v548: ( state_type * $int ) > $o )).

tff(pred_def_319,type,(
    v521: ( state_type * $int ) > $o )).

tff(pred_def_320,type,(
    v92: ( state_type * $int ) > $o )).

tff(pred_def_321,type,(
    v90: ( state_type * $int ) > $o )).

tff(pred_def_322,type,(
    v88: ( state_type * $int ) > $o )).

tff(pred_def_323,type,(
    v86: state_type > $o )).

tff(pred_def_324,type,(
    v84: state_type > $o )).

tff(pred_def_325,type,(
    v82: state_type > $o )).

tff(pred_def_326,type,(
    v554: state_type > $o )).

tff(pred_def_327,type,(
    v556: state_type > $o )).

tff(pred_def_328,type,(
    v62: ( state_type * $int ) > $o )).

tff(pred_def_329,type,(
    b000: $int > $o )).

tff(pred_def_330,type,(
    v560: state_type > $o )).

tff(pred_def_331,type,(
    v559: state_type > $o )).

tff(pred_def_332,type,(
    v562: state_type > $o )).

tff(pred_def_333,type,(
    v563: state_type > $o )).

tff(pred_def_334,type,(
    v561: state_type > $o )).

tff(pred_def_335,type,(
    v558: state_type > $o )).

tff(pred_def_336,type,(
    v67: ( state_type * $int ) > $o )).

tff(pred_def_337,type,(
    v565: state_type > $o )).

tff(pred_def_338,type,(
    v572: state_type > $o )).

tff(pred_def_339,type,(
    v570: state_type > $o )).

tff(pred_def_340,type,(
    v569: state_type > $o )).

tff(pred_def_341,type,(
    v568: state_type > $o )).

tff(pred_def_342,type,(
    v579: state_type > $o )).

tff(pred_def_343,type,(
    v576: ( state_type * $int ) > $o )).

tff(pred_def_344,type,(
    v578: ( state_type * $int ) > $o )).

tff(pred_def_345,type,(
    v567: ( state_type * $int ) > $o )).

tff(pred_def_346,type,(
    v69: state_type > $o )).

tff(pred_def_347,type,(
    v585: state_type > $o )).

tff(pred_def_348,type,(
    v587: state_type > $o )).

tff(pred_def_349,type,(
    v584: state_type > $o )).

tff(pred_def_350,type,(
    v589: state_type > $o )).

tff(pred_def_351,type,(
    v590: state_type > $o )).

tff(pred_def_352,type,(
    v588: state_type > $o )).

tff(pred_def_353,type,(
    v583: state_type > $o )).

tff(pred_def_354,type,(
    v596: state_type > $o )).

tff(pred_def_355,type,(
    v594: state_type > $o )).

tff(pred_def_356,type,(
    v593: state_type > $o )).

tff(pred_def_357,type,(
    v592: ( state_type * $int ) > $o )).

tff(pred_def_358,type,(
    v603: state_type > $o )).

tff(pred_def_359,type,(
    v602: state_type > $o )).

tff(pred_def_360,type,(
    v605: state_type > $o )).

tff(pred_def_361,type,(
    v601: state_type > $o )).

tff(pred_def_362,type,(
    v606: state_type > $o )).

tff(pred_def_363,type,(
    v600: state_type > $o )).

tff(pred_def_364,type,(
    v612: state_type > $o )).

tff(pred_def_365,type,(
    v610: state_type > $o )).

tff(pred_def_366,type,(
    v609: state_type > $o )).

tff(pred_def_367,type,(
    v615: state_type > $o )).

tff(pred_def_368,type,(
    v617: state_type > $o )).

tff(pred_def_369,type,(
    v80: state_type > $o )).

tff(pred_def_370,type,(
    v78: state_type > $o )).

tff(pred_def_371,type,(
    v76: state_type > $o )).

tff(pred_def_372,type,(
    v623: state_type > $o )).

tff(pred_def_373,type,(
    v622: state_type > $o )).

tff(pred_def_374,type,(
    v621: state_type > $o )).

tff(pred_def_375,type,(
    v624: state_type > $o )).

tff(pred_def_376,type,(
    v73: state_type > $o )).

tff(pred_def_377,type,(
    v71: state_type > $o )).

tff(pred_def_378,type,(
    v630: state_type > $o )).

tff(pred_def_379,type,(
    v628: state_type > $o )).

tff(pred_def_380,type,(
    v627: state_type > $o )).

tff(pred_def_381,type,(
    v632: state_type > $o )).

tff(pred_def_382,type,(
    v631: state_type > $o )).

tff(pred_def_383,type,(
    v626: state_type > $o )).

tff(pred_def_384,type,(
    v638: state_type > $o )).

tff(pred_def_385,type,(
    v636: state_type > $o )).

tff(pred_def_386,type,(
    v635: state_type > $o )).

tff(pred_def_387,type,(
    v634: ( state_type * $int ) > $o )).

tff(pred_def_388,type,(
    v642: state_type > $o )).

tff(pred_def_389,type,(
    v60: state_type > $o )).

tff(pred_def_390,type,(
    v58: state_type > $o )).

tff(pred_def_391,type,(
    v56: state_type > $o )).

tff(pred_def_392,type,(
    v648: state_type > $o )).

tff(pred_def_393,type,(
    v647: state_type > $o )).

tff(pred_def_394,type,(
    v646: state_type > $o )).

tff(pred_def_395,type,(
    v644: state_type > $o )).

tff(pred_def_396,type,(
    v650: state_type > $o )).

tff(pred_def_397,type,(
    v45: state_type > $o )).

tff(pred_def_398,type,(
    bx0000000: $int > $o )).

tff(pred_def_399,type,(
    v652: ( state_type * $int ) > $o )).

tff(pred_def_400,type,(
    v658: ( state_type * $int ) > $o )).

tff(pred_def_401,type,(
    v657: ( state_type * $int ) > $o )).

tff(pred_def_402,type,(
    v663: state_type > $o )).

tff(pred_def_403,type,(
    v666: ( state_type * $int ) > $o )).

tff(pred_def_404,type,(
    v665: state_type > $o )).

tff(pred_def_405,type,(
    v668: ( state_type * $int ) > $o )).

tff(pred_def_406,type,(
    v667: state_type > $o )).

tff(pred_def_407,type,(
    v669: ( state_type * $int ) > $o )).

tff(pred_def_408,type,(
    v672: ( state_type * $int ) > $o )).

tff(pred_def_409,type,(
    v671: state_type > $o )).

tff(pred_def_410,type,(
    v673: ( state_type * $int ) > $o )).

tff(pred_def_411,type,(
    v675: ( state_type * $int ) > $o )).

tff(pred_def_412,type,(
    v674: state_type > $o )).

tff(pred_def_413,type,(
    v664: ( state_type * $int ) > $o )).

tff(pred_def_414,type,(
    b00000000: $int > $o )).

tff(pred_def_415,type,(
    v662: ( state_type * $int ) > $o )).

tff(pred_def_416,type,(
    v655: ( state_type * $int ) > $o )).

tff(pred_def_417,type,(
    v681: state_type > $o )).

tff(pred_def_418,type,(
    v679: state_type > $o )).

tff(pred_def_419,type,(
    v678: state_type > $o )).

tff(pred_def_420,type,(
    v684: ( state_type * $int ) > $o )).

tff(pred_def_421,type,(
    v686: ( state_type * $int ) > $o )).

tff(pred_def_422,type,(
    v677: ( state_type * $int ) > $o )).

tff(pred_def_423,type,(
    v694: state_type > $o )).

tff(pred_def_424,type,(
    v692: state_type > $o )).

tff(pred_def_425,type,(
    v691: state_type > $o )).

tff(pred_def_426,type,(
    v690: ( state_type * $int ) > $o )).

tff(pred_def_427,type,(
    v702: state_type > $o )).

tff(pred_def_428,type,(
    v700: state_type > $o )).

tff(pred_def_429,type,(
    v699: state_type > $o )).

tff(pred_def_430,type,(
    v698: ( state_type * $int ) > $o )).

tff(pred_def_431,type,(
    v710: state_type > $o )).

tff(pred_def_432,type,(
    v708: state_type > $o )).

tff(pred_def_433,type,(
    v707: state_type > $o )).

tff(pred_def_434,type,(
    v706: ( state_type * $int ) > $o )).

tff(pred_def_435,type,(
    v718: state_type > $o )).

tff(pred_def_436,type,(
    v716: state_type > $o )).

tff(pred_def_437,type,(
    v715: state_type > $o )).

tff(pred_def_438,type,(
    v714: ( state_type * $int ) > $o )).

tff(pred_def_439,type,(
    v726: state_type > $o )).

tff(pred_def_440,type,(
    v724: state_type > $o )).

tff(pred_def_441,type,(
    v723: state_type > $o )).

tff(pred_def_442,type,(
    v722: ( state_type * $int ) > $o )).

tff(pred_def_443,type,(
    v734: state_type > $o )).

tff(pred_def_444,type,(
    v732: state_type > $o )).

tff(pred_def_445,type,(
    v731: state_type > $o )).

tff(pred_def_446,type,(
    v730: ( state_type * $int ) > $o )).

tff(pred_def_447,type,(
    v742: state_type > $o )).

tff(pred_def_448,type,(
    v740: state_type > $o )).

tff(pred_def_449,type,(
    v739: state_type > $o )).

tff(pred_def_450,type,(
    v738: ( state_type * $int ) > $o )).

tff(pred_def_451,type,(
    v749: state_type > $o )).

tff(pred_def_452,type,(
    v748: state_type > $o )).

tff(pred_def_453,type,(
    v759: state_type > $o )).

tff(pred_def_454,type,(
    v760: state_type > $o )).

tff(pred_def_455,type,(
    v758: state_type > $o )).

tff(pred_def_456,type,(
    v761: state_type > $o )).

tff(pred_def_457,type,(
    v757: state_type > $o )).

tff(pred_def_458,type,(
    v756: state_type > $o )).

tff(pred_def_459,type,(
    v762: state_type > $o )).

tff(pred_def_460,type,(
    v753: state_type > $o )).

tff(pred_def_461,type,(
    v755: state_type > $o )).

tff(pred_def_462,type,(
    v747: state_type > $o )).

tff(pred_def_463,type,(
    v766: state_type > $o )).

tff(pred_def_464,type,(
    v763: state_type > $o )).

tff(pred_def_465,type,(
    v765: state_type > $o )).

tff(pred_def_466,type,(
    v32: state_type > $o )).

tff(pred_def_467,type,(
    v774: state_type > $o )).

tff(pred_def_468,type,(
    v776: ( state_type * $int ) > $o )).

tff(pred_def_469,type,(
    v777: ( state_type * $int ) > $o )).

tff(pred_def_470,type,(
    v772: ( state_type * $int ) > $o )).

tff(pred_def_471,type,(
    v781: state_type > $o )).

tff(pred_def_472,type,(
    v783: ( state_type * $int ) > $o )).

tff(pred_def_473,type,(
    v784: ( state_type * $int ) > $o )).

tff(pred_def_474,type,(
    v779: ( state_type * $int ) > $o )).

tff(pred_def_475,type,(
    v790: state_type > $o )).

tff(pred_def_476,type,(
    v788: state_type > $o )).

tff(pred_def_477,type,(
    v801: ( state_type * $int ) > $o )).

tff(pred_def_478,type,(
    v800: state_type > $o )).

tff(pred_def_479,type,(
    v803: ( state_type * $int ) > $o )).

tff(pred_def_480,type,(
    v802: state_type > $o )).

tff(pred_def_481,type,(
    v799: state_type > $o )).

tff(pred_def_482,type,(
    v805: ( state_type * $int ) > $o )).

tff(pred_def_483,type,(
    v804: state_type > $o )).

tff(pred_def_484,type,(
    v798: state_type > $o )).

tff(pred_def_485,type,(
    v806: state_type > $o )).

tff(pred_def_486,type,(
    v797: state_type > $o )).

tff(pred_def_487,type,(
    v796: state_type > $o )).

tff(pred_def_488,type,(
    v810: ( state_type * $int ) > $o )).

tff(pred_def_489,type,(
    v809: state_type > $o )).

tff(pred_def_490,type,(
    v808: state_type > $o )).

tff(pred_def_491,type,(
    v807: state_type > $o )).

tff(pred_def_492,type,(
    v793: state_type > $o )).

tff(pred_def_493,type,(
    v795: state_type > $o )).

tff(pred_def_494,type,(
    v787: state_type > $o )).

tff(pred_def_495,type,(
    v814: ( state_type * $int ) > $o )).

tff(pred_def_496,type,(
    v811: ( state_type * $int ) > $o )).

tff(pred_def_497,type,(
    v813: ( state_type * $int ) > $o )).

tff(pred_def_498,type,(
    v786: ( state_type * $int ) > $o )).

tff(pred_def_499,type,(
    v823: state_type > $o )).

tff(pred_def_500,type,(
    v821: state_type > $o )).

tff(pred_def_501,type,(
    v820: state_type > $o )).

tff(pred_def_502,type,(
    v818: ( state_type * $int ) > $o )).

tff(pred_def_503,type,(
    v831: state_type > $o )).

tff(pred_def_504,type,(
    v829: state_type > $o )).

tff(pred_def_505,type,(
    v828: state_type > $o )).

tff(pred_def_506,type,(
    v826: ( state_type * $int ) > $o )).

tff(pred_def_507,type,(
    v839: state_type > $o )).

tff(pred_def_508,type,(
    v837: state_type > $o )).

tff(pred_def_509,type,(
    v836: state_type > $o )).

tff(pred_def_510,type,(
    v834: ( state_type * $int ) > $o )).

tff(pred_def_511,type,(
    v847: state_type > $o )).

tff(pred_def_512,type,(
    v845: state_type > $o )).

tff(pred_def_513,type,(
    v844: state_type > $o )).

tff(pred_def_514,type,(
    v842: ( state_type * $int ) > $o )).

tff(pred_def_515,type,(
    v855: state_type > $o )).

tff(pred_def_516,type,(
    v853: state_type > $o )).

tff(pred_def_517,type,(
    v852: state_type > $o )).

tff(pred_def_518,type,(
    v850: ( state_type * $int ) > $o )).

tff(pred_def_519,type,(
    v863: state_type > $o )).

tff(pred_def_520,type,(
    v861: state_type > $o )).

tff(pred_def_521,type,(
    v860: state_type > $o )).

tff(pred_def_522,type,(
    v858: ( state_type * $int ) > $o )).

tff(pred_def_523,type,(
    v871: state_type > $o )).

tff(pred_def_524,type,(
    v869: state_type > $o )).

tff(pred_def_525,type,(
    v868: state_type > $o )).

tff(pred_def_526,type,(
    v866: ( state_type * $int ) > $o )).

tff(pred_def_527,type,(
    v770: ( state_type * $int ) > $o )).

tff(pred_def_528,type,(
    b0000000: $int > $o )).

tff(pred_def_529,type,(
    v899: state_type > $o )).

tff(pred_def_530,type,(
    b0100000: $int > $o )).

tff(pred_def_531,type,(
    v900: state_type > $o )).

tff(pred_def_532,type,(
    v898: state_type > $o )).

tff(pred_def_533,type,(
    b0000010: $int > $o )).

tff(pred_def_534,type,(
    v901: state_type > $o )).

tff(pred_def_535,type,(
    v897: state_type > $o )).

tff(pred_def_536,type,(
    b0000100: $int > $o )).

tff(pred_def_537,type,(
    v902: state_type > $o )).

tff(pred_def_538,type,(
    v896: state_type > $o )).

tff(pred_def_539,type,(
    b0000101: $int > $o )).

tff(pred_def_540,type,(
    v903: state_type > $o )).

tff(pred_def_541,type,(
    v895: state_type > $o )).

tff(pred_def_542,type,(
    b1000010: $int > $o )).

tff(pred_def_543,type,(
    v904: state_type > $o )).

tff(pred_def_544,type,(
    v894: state_type > $o )).

tff(pred_def_545,type,(
    b1000000: $int > $o )).

tff(pred_def_546,type,(
    v905: state_type > $o )).

tff(pred_def_547,type,(
    v893: state_type > $o )).

tff(pred_def_548,type,(
    b1100000: $int > $o )).

tff(pred_def_549,type,(
    v906: state_type > $o )).

tff(pred_def_550,type,(
    v892: state_type > $o )).

tff(pred_def_551,type,(
    b1000100: $int > $o )).

tff(pred_def_552,type,(
    v907: state_type > $o )).

tff(pred_def_553,type,(
    v891: state_type > $o )).

tff(pred_def_554,type,(
    b1000101: $int > $o )).

tff(pred_def_555,type,(
    v908: state_type > $o )).

tff(pred_def_556,type,(
    v889: state_type > $o )).

tff(pred_def_557,type,(
    b1111010: $int > $o )).

tff(pred_def_558,type,(
    v909: state_type > $o )).

tff(pred_def_559,type,(
    b0001010: $int > $o )).

tff(pred_def_560,type,(
    v913: state_type > $o )).

tff(pred_def_561,type,(
    b0001011: $int > $o )).

tff(pred_def_562,type,(
    v914: state_type > $o )).

tff(pred_def_563,type,(
    v912: state_type > $o )).

tff(pred_def_564,type,(
    b1001010: $int > $o )).

tff(pred_def_565,type,(
    v915: state_type > $o )).

tff(pred_def_566,type,(
    v910: state_type > $o )).

tff(pred_def_567,type,(
    v888: ( state_type * $int ) > $o )).

tff(pred_def_568,type,(
    v920: state_type > $o )).

tff(pred_def_569,type,(
    v919: state_type > $o )).

tff(pred_def_570,type,(
    v918: state_type > $o )).

tff(pred_def_571,type,(
    v924: ( state_type * $int ) > $o )).

tff(pred_def_572,type,(
    v15: state_type > $o )).

tff(pred_def_573,type,(
    v923: state_type > $o )).

tff(pred_def_574,type,(
    v926: ( state_type * $int ) > $o )).

tff(pred_def_575,type,(
    v925: state_type > $o )).

tff(pred_def_576,type,(
    v922: state_type > $o )).

tff(pred_def_577,type,(
    v927: state_type > $o )).

tff(pred_def_578,type,(
    v921: state_type > $o )).

tff(pred_def_579,type,(
    v917: state_type > $o )).

tff(pred_def_580,type,(
    v13: ( state_type * $int ) > $o )).

tff(pred_def_581,type,(
    v934: state_type > $o )).

tff(pred_def_582,type,(
    v932: state_type > $o )).

tff(pred_def_583,type,(
    v931: state_type > $o )).

tff(pred_def_584,type,(
    v930: state_type > $o )).

tff(pred_def_585,type,(
    v941: state_type > $o )).

tff(pred_def_586,type,(
    v938: ( state_type * $int ) > $o )).

tff(pred_def_587,type,(
    v940: ( state_type * $int ) > $o )).

tff(pred_def_588,type,(
    v929: ( state_type * $int ) > $o )).

tff(pred_def_589,type,(
    v949: state_type > $o )).

tff(pred_def_590,type,(
    v948: state_type > $o )).

tff(pred_def_591,type,(
    v947: state_type > $o )).

tff(pred_def_592,type,(
    v952: ( state_type * $int ) > $o )).

tff(pred_def_593,type,(
    v951: state_type > $o )).

tff(pred_def_594,type,(
    v950: state_type > $o )).

tff(pred_def_595,type,(
    v946: state_type > $o )).

tff(pred_def_596,type,(
    v954: state_type > $o )).

tff(pred_def_597,type,(
    v953: state_type > $o )).

tff(pred_def_598,type,(
    v945: state_type > $o )).

tff(pred_def_599,type,(
    v960: state_type > $o )).

tff(pred_def_600,type,(
    v958: state_type > $o )).

tff(pred_def_601,type,(
    v957: state_type > $o )).

tff(pred_def_602,type,(
    v956: ( state_type * $int ) > $o )).

tff(pred_def_603,type,(
    v967: state_type > $o )).

tff(pred_def_604,type,(
    v966: state_type > $o )).

tff(pred_def_605,type,(
    v965: state_type > $o )).

tff(pred_def_606,type,(
    v964: state_type > $o )).

tff(pred_def_607,type,(
    v969: state_type > $o )).

tff(pred_def_608,type,(
    v972: ( state_type * $int ) > $o )).

tff(pred_def_609,type,(
    v971: state_type > $o )).

tff(pred_def_610,type,(
    v970: state_type > $o )).

tff(pred_def_611,type,(
    v973: state_type > $o )).

tff(pred_def_612,type,(
    v968: state_type > $o )).

tff(pred_def_613,type,(
    v26: state_type > $o )).

tff(pred_def_614,type,(
    v977: ( state_type * $int ) > $o )).

tff(pred_def_615,type,(
    v976: state_type > $o )).

tff(pred_def_616,type,(
    b100: $int > $o )).

tff(pred_def_617,type,(
    v17: ( state_type * $int ) > $o )).

tff(pred_def_618,type,(
    v985: state_type > $o )).

tff(pred_def_619,type,(
    v984: state_type > $o )).

tff(pred_def_620,type,(
    v983: state_type > $o )).

tff(pred_def_621,type,(
    v982: state_type > $o )).

tff(pred_def_622,type,(
    v981: state_type > $o )).

tff(pred_def_623,type,(
    v987: state_type > $o )).

tff(pred_def_624,type,(
    v986: state_type > $o )).

tff(pred_def_625,type,(
    v980: state_type > $o )).

tff(pred_def_626,type,(
    v990: state_type > $o )).

tff(pred_def_627,type,(
    v989: state_type > $o )).

tff(pred_def_628,type,(
    v991: state_type > $o )).

tff(pred_def_629,type,(
    v988: state_type > $o )).

tff(pred_def_630,type,(
    v978: ( state_type * $int ) > $o )).

tff(pred_def_631,type,(
    v993: ( state_type * $int ) > $o )).

tff(pred_def_632,type,(
    v992: state_type > $o )).

tff(pred_def_633,type,(
    v999: state_type > $o )).

tff(pred_def_634,type,(
    v998: state_type > $o )).

tff(pred_def_635,type,(
    v997: state_type > $o )).

tff(pred_def_636,type,(
    v1000: state_type > $o )).

tff(pred_def_637,type,(
    v996: state_type > $o )).

tff(pred_def_638,type,(
    v1002: state_type > $o )).

tff(pred_def_639,type,(
    v1003: state_type > $o )).

tff(pred_def_640,type,(
    v1001: state_type > $o )).

tff(pred_def_641,type,(
    v994: ( state_type * $int ) > $o )).

tff(pred_def_642,type,(
    v1006: ( state_type * $int ) > $o )).

tff(pred_def_643,type,(
    v1005: state_type > $o )).

tff(pred_def_644,type,(
    v1008: ( state_type * $int ) > $o )).

tff(pred_def_645,type,(
    v1007: state_type > $o )).

tff(pred_def_646,type,(
    v1004: state_type > $o )).

tff(pred_def_647,type,(
    v20: ( state_type * $int ) > $o )).

tff(pred_def_648,type,(
    v1013: state_type > $o )).

tff(pred_def_649,type,(
    v1012: state_type > $o )).

tff(pred_def_650,type,(
    v1011: state_type > $o )).

tff(pred_def_651,type,(
    v1020: state_type > $o )).

tff(pred_def_652,type,(
    v1017: ( state_type * $int ) > $o )).

tff(pred_def_653,type,(
    v1019: ( state_type * $int ) > $o )).

tff(pred_def_654,type,(
    v1025: state_type > $o )).

tff(pred_def_655,type,(
    v1035: state_type > $o )).

tff(pred_def_656,type,(
    v1034: state_type > $o )).

tff(pred_def_657,type,(
    v1033: state_type > $o )).

tff(pred_def_658,type,(
    v1037: state_type > $o )).

tff(pred_def_659,type,(
    v1036: state_type > $o )).

tff(pred_def_660,type,(
    v1032: state_type > $o )).

tff(pred_def_661,type,(
    v1031: state_type > $o )).

tff(pred_def_662,type,(
    v1038: state_type > $o )).

tff(pred_def_663,type,(
    v1030: state_type > $o )).

tff(pred_def_664,type,(
    v1029: state_type > $o )).

tff(pred_def_665,type,(
    v1039: state_type > $o )).

tff(pred_def_666,type,(
    v1028: state_type > $o )).

tff(pred_def_667,type,(
    v1041: state_type > $o )).

tff(pred_def_668,type,(
    v1040: state_type > $o )).

tff(pred_def_669,type,(
    v1027: state_type > $o )).

tff(pred_def_670,type,(
    v1047: state_type > $o )).

tff(pred_def_671,type,(
    v1045: state_type > $o )).

tff(pred_def_672,type,(
    v1044: state_type > $o )).

tff(pred_def_673,type,(
    v1050: state_type > $o )).

tff(pred_def_674,type,(
    v1052: state_type > $o )).

tff(pred_def_675,type,(
    v1059: state_type > $o )).

tff(pred_def_676,type,(
    v1058: state_type > $o )).

tff(pred_def_677,type,(
    v1061: state_type > $o )).

tff(pred_def_678,type,(
    v1060: state_type > $o )).

tff(pred_def_679,type,(
    v1057: state_type > $o )).

tff(pred_def_680,type,(
    v1063: state_type > $o )).

tff(pred_def_681,type,(
    v1062: state_type > $o )).

tff(pred_def_682,type,(
    v4: state_type > $o )).

tff(pred_def_683,type,(
    reachableState: state_type > $o )).

tff(pathAxiom_49,axiom,(
    nextState(constB49,constB50) )).

tff(pathAxiom_48,axiom,(
    nextState(constB48,constB49) )).

tff(pathAxiom_47,axiom,(
    nextState(constB47,constB48) )).

tff(pathAxiom_46,axiom,(
    nextState(constB46,constB47) )).

tff(pathAxiom_45,axiom,(
    nextState(constB45,constB46) )).

tff(pathAxiom_44,axiom,(
    nextState(constB44,constB45) )).

tff(pathAxiom_43,axiom,(
    nextState(constB43,constB44) )).

tff(pathAxiom_42,axiom,(
    nextState(constB42,constB43) )).

tff(pathAxiom_41,axiom,(
    nextState(constB41,constB42) )).

tff(pathAxiom_40,axiom,(
    nextState(constB40,constB41) )).

tff(pathAxiom_39,axiom,(
    nextState(constB39,constB40) )).

tff(pathAxiom_38,axiom,(
    nextState(constB38,constB39) )).

tff(pathAxiom_37,axiom,(
    nextState(constB37,constB38) )).

tff(pathAxiom_36,axiom,(
    nextState(constB36,constB37) )).

tff(pathAxiom_35,axiom,(
    nextState(constB35,constB36) )).

tff(pathAxiom_34,axiom,(
    nextState(constB34,constB35) )).

tff(pathAxiom_33,axiom,(
    nextState(constB33,constB34) )).

tff(pathAxiom_32,axiom,(
    nextState(constB32,constB33) )).

tff(pathAxiom_31,axiom,(
    nextState(constB31,constB32) )).

tff(pathAxiom_30,axiom,(
    nextState(constB30,constB31) )).

tff(pathAxiom_29,axiom,(
    nextState(constB29,constB30) )).

tff(pathAxiom_28,axiom,(
    nextState(constB28,constB29) )).

tff(pathAxiom_27,axiom,(
    nextState(constB27,constB28) )).

tff(pathAxiom_26,axiom,(
    nextState(constB26,constB27) )).

tff(pathAxiom_25,axiom,(
    nextState(constB25,constB26) )).

tff(pathAxiom_24,axiom,(
    nextState(constB24,constB25) )).

tff(pathAxiom_23,axiom,(
    nextState(constB23,constB24) )).

tff(pathAxiom_22,axiom,(
    nextState(constB22,constB23) )).

tff(pathAxiom_21,axiom,(
    nextState(constB21,constB22) )).

tff(pathAxiom_20,axiom,(
    nextState(constB20,constB21) )).

tff(pathAxiom_19,axiom,(
    nextState(constB19,constB20) )).

tff(pathAxiom_18,axiom,(
    nextState(constB18,constB19) )).

tff(pathAxiom_17,axiom,(
    nextState(constB17,constB18) )).

tff(pathAxiom_16,axiom,(
    nextState(constB16,constB17) )).

tff(pathAxiom_15,axiom,(
    nextState(constB15,constB16) )).

tff(pathAxiom_14,axiom,(
    nextState(constB14,constB15) )).

tff(pathAxiom_13,axiom,(
    nextState(constB13,constB14) )).

tff(pathAxiom_12,axiom,(
    nextState(constB12,constB13) )).

tff(pathAxiom_11,axiom,(
    nextState(constB11,constB12) )).

tff(pathAxiom_10,axiom,(
    nextState(constB10,constB11) )).

tff(pathAxiom_9,axiom,(
    nextState(constB9,constB10) )).

tff(pathAxiom_8,axiom,(
    nextState(constB8,constB9) )).

tff(pathAxiom_7,axiom,(
    nextState(constB7,constB8) )).

tff(pathAxiom_6,axiom,(
    nextState(constB6,constB7) )).

tff(pathAxiom_5,axiom,(
    nextState(constB5,constB6) )).

tff(pathAxiom_4,axiom,(
    nextState(constB4,constB5) )).

tff(pathAxiom_3,axiom,(
    nextState(constB3,constB4) )).

tff(pathAxiom_2,axiom,(
    nextState(constB2,constB3) )).

tff(pathAxiom_1,axiom,(
    nextState(constB1,constB2) )).

tff(pathAxiom,axiom,(
    nextState(constB0,constB1) )).

tff(reachableStateAxiom_52,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( reachableState(VarCurr)
        & reachableState(VarNext) ) ) )).

tff(reachableStateAxiom_51,axiom,(
    ! [VarState: state_type] :
      ( reachableState(VarState)
     => ( constB0 = VarState
        | constB1 = VarState
        | constB2 = VarState
        | constB3 = VarState
        | constB4 = VarState
        | constB5 = VarState
        | constB6 = VarState
        | constB7 = VarState
        | constB8 = VarState
        | constB9 = VarState
        | constB10 = VarState
        | constB11 = VarState
        | constB12 = VarState
        | constB13 = VarState
        | constB14 = VarState
        | constB15 = VarState
        | constB16 = VarState
        | constB17 = VarState
        | constB18 = VarState
        | constB19 = VarState
        | constB20 = VarState
        | constB21 = VarState
        | constB22 = VarState
        | constB23 = VarState
        | constB24 = VarState
        | constB25 = VarState
        | constB26 = VarState
        | constB27 = VarState
        | constB28 = VarState
        | constB29 = VarState
        | constB30 = VarState
        | constB31 = VarState
        | constB32 = VarState
        | constB33 = VarState
        | constB34 = VarState
        | constB35 = VarState
        | constB36 = VarState
        | constB37 = VarState
        | constB38 = VarState
        | constB39 = VarState
        | constB40 = VarState
        | constB41 = VarState
        | constB42 = VarState
        | constB43 = VarState
        | constB44 = VarState
        | constB45 = VarState
        | constB46 = VarState
        | constB47 = VarState
        | constB48 = VarState
        | constB49 = VarState
        | constB50 = VarState ) ) )).

tff(reachableStateAxiom_50,axiom,(
    reachableState(constB50) )).

tff(reachableStateAxiom_49,axiom,(
    reachableState(constB49) )).

tff(reachableStateAxiom_48,axiom,(
    reachableState(constB48) )).

tff(reachableStateAxiom_47,axiom,(
    reachableState(constB47) )).

tff(reachableStateAxiom_46,axiom,(
    reachableState(constB46) )).

tff(reachableStateAxiom_45,axiom,(
    reachableState(constB45) )).

tff(reachableStateAxiom_44,axiom,(
    reachableState(constB44) )).

tff(reachableStateAxiom_43,axiom,(
    reachableState(constB43) )).

tff(reachableStateAxiom_42,axiom,(
    reachableState(constB42) )).

tff(reachableStateAxiom_41,axiom,(
    reachableState(constB41) )).

tff(reachableStateAxiom_40,axiom,(
    reachableState(constB40) )).

tff(reachableStateAxiom_39,axiom,(
    reachableState(constB39) )).

tff(reachableStateAxiom_38,axiom,(
    reachableState(constB38) )).

tff(reachableStateAxiom_37,axiom,(
    reachableState(constB37) )).

tff(reachableStateAxiom_36,axiom,(
    reachableState(constB36) )).

tff(reachableStateAxiom_35,axiom,(
    reachableState(constB35) )).

tff(reachableStateAxiom_34,axiom,(
    reachableState(constB34) )).

tff(reachableStateAxiom_33,axiom,(
    reachableState(constB33) )).

tff(reachableStateAxiom_32,axiom,(
    reachableState(constB32) )).

tff(reachableStateAxiom_31,axiom,(
    reachableState(constB31) )).

tff(reachableStateAxiom_30,axiom,(
    reachableState(constB30) )).

tff(reachableStateAxiom_29,axiom,(
    reachableState(constB29) )).

tff(reachableStateAxiom_28,axiom,(
    reachableState(constB28) )).

tff(reachableStateAxiom_27,axiom,(
    reachableState(constB27) )).

tff(reachableStateAxiom_26,axiom,(
    reachableState(constB26) )).

tff(reachableStateAxiom_25,axiom,(
    reachableState(constB25) )).

tff(reachableStateAxiom_24,axiom,(
    reachableState(constB24) )).

tff(reachableStateAxiom_23,axiom,(
    reachableState(constB23) )).

tff(reachableStateAxiom_22,axiom,(
    reachableState(constB22) )).

tff(reachableStateAxiom_21,axiom,(
    reachableState(constB21) )).

tff(reachableStateAxiom_20,axiom,(
    reachableState(constB20) )).

tff(reachableStateAxiom_19,axiom,(
    reachableState(constB19) )).

tff(reachableStateAxiom_18,axiom,(
    reachableState(constB18) )).

tff(reachableStateAxiom_17,axiom,(
    reachableState(constB17) )).

tff(reachableStateAxiom_16,axiom,(
    reachableState(constB16) )).

tff(reachableStateAxiom_15,axiom,(
    reachableState(constB15) )).

tff(reachableStateAxiom_14,axiom,(
    reachableState(constB14) )).

tff(reachableStateAxiom_13,axiom,(
    reachableState(constB13) )).

tff(reachableStateAxiom_12,axiom,(
    reachableState(constB12) )).

tff(reachableStateAxiom_11,axiom,(
    reachableState(constB11) )).

tff(reachableStateAxiom_10,axiom,(
    reachableState(constB10) )).

tff(reachableStateAxiom_9,axiom,(
    reachableState(constB9) )).

tff(reachableStateAxiom_8,axiom,(
    reachableState(constB8) )).

tff(reachableStateAxiom_7,axiom,(
    reachableState(constB7) )).

tff(reachableStateAxiom_6,axiom,(
    reachableState(constB6) )).

tff(reachableStateAxiom_5,axiom,(
    reachableState(constB5) )).

tff(reachableStateAxiom_4,axiom,(
    reachableState(constB4) )).

tff(reachableStateAxiom_3,axiom,(
    reachableState(constB3) )).

tff(reachableStateAxiom_2,axiom,(
    reachableState(constB2) )).

tff(reachableStateAxiom_1,axiom,(
    reachableState(constB1) )).

tff(reachableStateAxiom,axiom,(
    reachableState(constB0) )).

tff(clock_toggling,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v1(VarCurr)
      <=> ~ v1(VarNext) ) ) )).

tff(clock_pattern,axiom,(
    ~ v1(constB0) )).

tff(addAssertion,conjecture,(
    ! [VarCurr: state_type] :
      ( reachableState(VarCurr)
     => v4(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_212,axiom,(
    ! [VarCurr: state_type] :
      ( v4(VarCurr)
    <=> ( v1057(VarCurr)
        & v1062(VarCurr) ) ) )).

tff(writeUnaryOperator_83,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1062(VarCurr)
    <=> v1063(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_211,axiom,(
    ! [VarCurr: state_type] :
      ( v1063(VarCurr)
    <=> ( v927(VarCurr)
        & v954(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_210,axiom,(
    ! [VarCurr: state_type] :
      ( v1057(VarCurr)
    <=> ( v1058(VarCurr)
        & v1060(VarCurr) ) ) )).

tff(writeUnaryOperator_82,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1060(VarCurr)
    <=> v1061(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_209,axiom,(
    ! [VarCurr: state_type] :
      ( v1061(VarCurr)
    <=> ( v54(VarCurr)
        & v954(VarCurr) ) ) )).

tff(writeUnaryOperator_81,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1058(VarCurr)
    <=> v1059(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_208,axiom,(
    ! [VarCurr: state_type] :
      ( v1059(VarCurr)
    <=> ( v54(VarCurr)
        & v927(VarCurr) ) ) )).

tff(addCaseBooleanConditionEqualRanges1_21,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v1044(VarNext)
       => ( v7(VarNext,0)
        <=> v7(VarCurr,0) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_40,axiom,(
    ! [VarNext: state_type] :
      ( v1044(VarNext)
     => ( v7(VarNext,0)
      <=> v1052(VarNext) ) ) )).

tff(addAssignment_321,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v1052(VarNext)
      <=> v1050(VarCurr) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_34,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v941(VarCurr)
     => ( v1050(VarCurr)
      <=> v13(VarCurr,0) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_36,axiom,(
    ! [VarCurr: state_type] :
      ( v941(VarCurr)
     => ( v1050(VarCurr)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_207,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v1044(VarNext)
      <=> v1045(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_206,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v1045(VarNext)
      <=> ( v1047(VarNext)
          & v112(VarNext) ) ) ) )).

tff(writeUnaryOperator_80,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v1047(VarNext)
      <=> v934(VarNext) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_33,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1027(VarCurr)
     => ( v13(VarCurr,0)
      <=> $false ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_35,axiom,(
    ! [VarCurr: state_type] :
      ( v1027(VarCurr)
     => ( v13(VarCurr,0)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_205,axiom,(
    ! [VarCurr: state_type] :
      ( v1027(VarCurr)
    <=> ( v1028(VarCurr)
        | v1040(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_204,axiom,(
    ! [VarCurr: state_type] :
      ( v1040(VarCurr)
    <=> ( v1041(VarCurr)
        & v954(VarCurr) ) ) )).

tff(writeUnaryOperator_79,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1041(VarCurr)
    <=> v15(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_203,axiom,(
    ! [VarCurr: state_type] :
      ( v1028(VarCurr)
    <=> ( v1029(VarCurr)
        | v1039(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_202,axiom,(
    ! [VarCurr: state_type] :
      ( v1039(VarCurr)
    <=> ( v971(VarCurr)
        & v927(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_201,axiom,(
    ! [VarCurr: state_type] :
      ( v1029(VarCurr)
    <=> ( v1030(VarCurr)
        & v54(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_200,axiom,(
    ! [VarCurr: state_type] :
      ( v1030(VarCurr)
    <=> ( v1031(VarCurr)
        | v1038(VarCurr) ) ) )).

tff(writeUnaryOperator_78,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1038(VarCurr)
    <=> v53(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_199,axiom,(
    ! [VarCurr: state_type] :
      ( v1031(VarCurr)
    <=> ( v1032(VarCurr)
        & v53(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_198,axiom,(
    ! [VarCurr: state_type] :
      ( v1032(VarCurr)
    <=> ( v1033(VarCurr)
        | v1036(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_197,axiom,(
    ! [VarCurr: state_type] :
      ( v1036(VarCurr)
    <=> ( v1037(VarCurr)
        & v623(VarCurr) ) ) )).

tff(writeUnaryOperator_77,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1037(VarCurr)
    <=> v76(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_196,axiom,(
    ! [VarCurr: state_type] :
      ( v1033(VarCurr)
    <=> ( v1034(VarCurr)
        | v648(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_195,axiom,(
    ! [VarCurr: state_type] :
      ( v1034(VarCurr)
    <=> ( v1035(VarCurr)
        & v52(VarCurr) ) ) )).

tff(writeUnaryOperator_76,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1035(VarCurr)
    <=> v15(VarCurr) ) )).

tff(writeUnaryOperator_75,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v15(VarCurr)
    <=> v1025(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_7,axiom,(
    ! [VarCurr: state_type] :
      ( v1025(VarCurr)
    <=> ( v1003(VarCurr)
        | v17(VarCurr,2) ) ) )).

tff(addCaseBooleanConditionEqualRanges1_20,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v1011(VarNext)
       => ! [B: $int] :
            ( ( $less(B,3)
              & ~ $less(B,0) )
           => ( v17(VarNext,B)
            <=> v17(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_39,axiom,(
    ! [VarNext: state_type] :
      ( v1011(VarNext)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v17(VarNext,B)
          <=> v1019(VarNext,B) ) ) ) )).

tff(addAssignment_320,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v1019(VarNext,B)
          <=> v1017(VarCurr,B) ) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_32,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1020(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v1017(VarCurr,B)
          <=> v20(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_34,axiom,(
    ! [VarCurr: state_type] :
      ( v1020(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v1017(VarCurr,B)
          <=> b100(B) ) ) ) )).

tff(writeUnaryOperator_74,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v1020(VarCurr)
    <=> v9(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_194,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v1011(VarNext)
      <=> v1012(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_193,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v1012(VarNext)
      <=> ( v1013(VarNext)
          & v112(VarNext) ) ) ) )).

tff(writeUnaryOperator_73,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v1013(VarNext)
      <=> v934(VarNext) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_13,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v976(VarCurr)
        & ~ v992(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v20(VarCurr,B)
          <=> v17(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_21,axiom,(
    ! [VarCurr: state_type] :
      ( v992(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v20(VarCurr,B)
          <=> v994(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_15,axiom,(
    ! [VarCurr: state_type] :
      ( v976(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v20(VarCurr,B)
          <=> v978(VarCurr,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_192,axiom,(
    ! [VarCurr: state_type] :
      ( v1004(VarCurr)
    <=> ( v1005(VarCurr)
        | v1007(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_74,axiom,(
    ! [VarCurr: state_type] :
      ( v1007(VarCurr)
    <=> ( ( v1008(VarCurr,1)
        <=> $true )
        & ( v1008(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_319,axiom,(
    ! [VarCurr: state_type] :
      ( v1008(VarCurr,0)
    <=> v26(VarCurr) ) )).

tff(addAssignment_318,axiom,(
    ! [VarCurr: state_type] :
      ( v1008(VarCurr,1)
    <=> v22(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_73,axiom,(
    ! [VarCurr: state_type] :
      ( v1005(VarCurr)
    <=> ( ( v1006(VarCurr,1)
        <=> $false )
        & ( v1006(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_317,axiom,(
    ! [VarCurr: state_type] :
      ( v1006(VarCurr,0)
    <=> v26(VarCurr) ) )).

tff(addAssignment_316,axiom,(
    ! [VarCurr: state_type] :
      ( v1006(VarCurr,1)
    <=> v22(VarCurr) ) )).

tff(addAssignment_315,axiom,(
    ! [VarCurr: state_type] :
      ( v994(VarCurr,0)
    <=> v990(VarCurr) ) )).

tff(addAssignment_314,axiom,(
    ! [VarCurr: state_type] :
      ( v994(VarCurr,1)
    <=> v1001(VarCurr) ) )).

tff(addAssignment_313,axiom,(
    ! [VarCurr: state_type] :
      ( v994(VarCurr,2)
    <=> v996(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_191,axiom,(
    ! [VarCurr: state_type] :
      ( v1001(VarCurr)
    <=> ( v1002(VarCurr)
        & v1003(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_6,axiom,(
    ! [VarCurr: state_type] :
      ( v1003(VarCurr)
    <=> ( v17(VarCurr,0)
        | v17(VarCurr,1) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_190,axiom,(
    ! [VarCurr: state_type] :
      ( v1002(VarCurr)
    <=> ( v990(VarCurr)
        | v985(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_189,axiom,(
    ! [VarCurr: state_type] :
      ( v996(VarCurr)
    <=> ( v997(VarCurr)
        & v1000(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_5,axiom,(
    ! [VarCurr: state_type] :
      ( v1000(VarCurr)
    <=> ( v17(VarCurr,2)
        | v999(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_188,axiom,(
    ! [VarCurr: state_type] :
      ( v997(VarCurr)
    <=> ( v987(VarCurr)
        | v998(VarCurr) ) ) )).

tff(writeUnaryOperator_72,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v998(VarCurr)
    <=> v999(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_4,axiom,(
    ! [VarCurr: state_type] :
      ( v999(VarCurr)
    <=> ( v17(VarCurr,0)
        & v17(VarCurr,1) ) ) )).

tff(addBitVectorEqualityBitBlasted_72,axiom,(
    ! [VarCurr: state_type] :
      ( v992(VarCurr)
    <=> ( ( v993(VarCurr,1)
        <=> $true )
        & ( v993(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_312,axiom,(
    ! [VarCurr: state_type] :
      ( v993(VarCurr,0)
    <=> v26(VarCurr) ) )).

tff(addAssignment_311,axiom,(
    ! [VarCurr: state_type] :
      ( v993(VarCurr,1)
    <=> v22(VarCurr) ) )).

tff(addAssignment_310,axiom,(
    ! [VarCurr: state_type] :
      ( v978(VarCurr,0)
    <=> v990(VarCurr) ) )).

tff(addAssignment_309,axiom,(
    ! [VarCurr: state_type] :
      ( v978(VarCurr,1)
    <=> v988(VarCurr) ) )).

tff(addAssignment_308,axiom,(
    ! [VarCurr: state_type] :
      ( v978(VarCurr,2)
    <=> v980(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_187,axiom,(
    ! [VarCurr: state_type] :
      ( v988(VarCurr)
    <=> ( v989(VarCurr)
        & v991(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_186,axiom,(
    ! [VarCurr: state_type] :
      ( v991(VarCurr)
    <=> ( v17(VarCurr,0)
        | v985(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges_3,axiom,(
    ! [VarCurr: state_type] :
      ( v989(VarCurr)
    <=> ( v990(VarCurr)
        | v17(VarCurr,1) ) ) )).

tff(writeUnaryOperator_71,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v990(VarCurr)
    <=> v17(VarCurr,0) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_185,axiom,(
    ! [VarCurr: state_type] :
      ( v980(VarCurr)
    <=> ( v981(VarCurr)
        & v986(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_184,axiom,(
    ! [VarCurr: state_type] :
      ( v986(VarCurr)
    <=> ( v983(VarCurr)
        | v987(VarCurr) ) ) )).

tff(writeUnaryOperator_70,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v987(VarCurr)
    <=> v17(VarCurr,2) ) )).

tff(writeBinaryOperatorShiftedRanges_2,axiom,(
    ! [VarCurr: state_type] :
      ( v981(VarCurr)
    <=> ( v982(VarCurr)
        | v17(VarCurr,2) ) ) )).

tff(writeUnaryOperator_69,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v982(VarCurr)
    <=> v983(VarCurr) ) )).

tff(writeBinaryOperatorShiftedRanges_1,axiom,(
    ! [VarCurr: state_type] :
      ( v983(VarCurr)
    <=> ( v17(VarCurr,1)
        | v984(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_183,axiom,(
    ! [VarCurr: state_type] :
      ( v984(VarCurr)
    <=> ( v17(VarCurr,0)
        & v985(VarCurr) ) ) )).

tff(writeUnaryOperator_68,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v985(VarCurr)
    <=> v17(VarCurr,1) ) )).

tff(addAssignmentInitValue_84,axiom,(
    v17(constB0,2) )).

tff(addAssignmentInitValue_83,axiom,(
    ~ v17(constB0,1) )).

tff(addAssignmentInitValue_82,axiom,(
    ~ v17(constB0,0) )).

tff(bitBlastConstant_271,axiom,(
    b100(2) )).

tff(bitBlastConstant_270,axiom,(
    ~ b100(1) )).

tff(bitBlastConstant_269,axiom,(
    ~ b100(0) )).

tff(addBitVectorEqualityBitBlasted_71,axiom,(
    ! [VarCurr: state_type] :
      ( v976(VarCurr)
    <=> ( ( v977(VarCurr,1)
        <=> $false )
        & ( v977(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_307,axiom,(
    ! [VarCurr: state_type] :
      ( v977(VarCurr,0)
    <=> v26(VarCurr) ) )).

tff(addAssignment_306,axiom,(
    ! [VarCurr: state_type] :
      ( v977(VarCurr,1)
    <=> v22(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_31,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v964(VarCurr)
     => ( v26(VarCurr)
      <=> $false ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_33,axiom,(
    ! [VarCurr: state_type] :
      ( v964(VarCurr)
     => ( v26(VarCurr)
      <=> v968(VarCurr) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_12,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v966(VarCurr)
        & ~ v927(VarCurr) )
     => ( v968(VarCurr)
      <=> v973(VarCurr) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_20,axiom,(
    ! [VarCurr: state_type] :
      ( v927(VarCurr)
     => ( v968(VarCurr)
      <=> v970(VarCurr) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_14,axiom,(
    ! [VarCurr: state_type] :
      ( v966(VarCurr)
     => ( v968(VarCurr)
      <=> v969(VarCurr) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_30,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v15(VarCurr)
     => ( v973(VarCurr)
      <=> $true ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_32,axiom,(
    ! [VarCurr: state_type] :
      ( v15(VarCurr)
     => ( v973(VarCurr)
      <=> $false ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_11,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v922(VarCurr)
        & ~ v971(VarCurr) )
     => ( v970(VarCurr)
      <=> $false ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_19,axiom,(
    ! [VarCurr: state_type] :
      ( v971(VarCurr)
     => ( v970(VarCurr)
      <=> $true ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_13,axiom,(
    ! [VarCurr: state_type] :
      ( v922(VarCurr)
     => ( v970(VarCurr)
      <=> $false ) ) )).

tff(addBitVectorEqualityBitBlasted_70,axiom,(
    ! [VarCurr: state_type] :
      ( v971(VarCurr)
    <=> ( ( v972(VarCurr,1)
        <=> $true )
        & ( v972(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_305,axiom,(
    ! [VarCurr: state_type] :
      ( v972(VarCurr,0)
    <=> v15(VarCurr) ) )).

tff(addAssignment_304,axiom,(
    ! [VarCurr: state_type] :
      ( v972(VarCurr,1)
    <=> v56(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_29,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v15(VarCurr)
     => ( v969(VarCurr)
      <=> $true ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_31,axiom,(
    ! [VarCurr: state_type] :
      ( v15(VarCurr)
     => ( v969(VarCurr)
      <=> $false ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_182,axiom,(
    ! [VarCurr: state_type] :
      ( v964(VarCurr)
    <=> ( v965(VarCurr)
        | v954(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_181,axiom,(
    ! [VarCurr: state_type] :
      ( v965(VarCurr)
    <=> ( v966(VarCurr)
        | v927(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_180,axiom,(
    ! [VarCurr: state_type] :
      ( v966(VarCurr)
    <=> ( v967(VarCurr)
        & v54(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_179,axiom,(
    ! [VarCurr: state_type] :
      ( v967(VarCurr)
    <=> ( v52(VarCurr)
        & v53(VarCurr) ) ) )).

tff(addAssignment_303,axiom,(
    ! [VarNext: state_type] :
      ( v7(VarNext,1)
    <=> v956(VarNext,0) ) )).

tff(addCaseBooleanConditionShiftedRanges1_18,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v957(VarNext)
       => ( ( v956(VarNext,1)
          <=> v7(VarCurr,2) )
          & ( v956(VarNext,0)
          <=> v7(VarCurr,1) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_38,axiom,(
    ! [VarNext: state_type] :
      ( v957(VarNext)
     => ! [B: $int] :
          ( ( $less(B,2)
            & ~ $less(B,0) )
         => ( v956(VarNext,B)
          <=> v940(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_178,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v957(VarNext)
      <=> v958(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_177,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v958(VarNext)
      <=> ( v960(VarNext)
          & v112(VarNext) ) ) ) )).

tff(writeUnaryOperator_67,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v960(VarNext)
      <=> v934(VarNext) ) ) )).

tff(addConditionBooleanCondShiftedRangesElseBranch_6,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v945(VarCurr)
     => ( v13(VarCurr,1)
      <=> $false ) ) )).

tff(addConditionBooleanCondShiftedRangesThenBranch_4,axiom,(
    ! [VarCurr: state_type] :
      ( v945(VarCurr)
     => ( v13(VarCurr,1)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_176,axiom,(
    ! [VarCurr: state_type] :
      ( v945(VarCurr)
    <=> ( v946(VarCurr)
        | v953(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_175,axiom,(
    ! [VarCurr: state_type] :
      ( v953(VarCurr)
    <=> ( v15(VarCurr)
        & v954(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_69,axiom,(
    ! [VarCurr: state_type] :
      ( v954(VarCurr)
    <=> ( $true
      <=> v7(VarCurr,1) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_174,axiom,(
    ! [VarCurr: state_type] :
      ( v946(VarCurr)
    <=> ( v947(VarCurr)
        | v950(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_173,axiom,(
    ! [VarCurr: state_type] :
      ( v950(VarCurr)
    <=> ( v951(VarCurr)
        & v927(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_68,axiom,(
    ! [VarCurr: state_type] :
      ( v951(VarCurr)
    <=> ( ( v952(VarCurr,1)
        <=> $true )
        & ( v952(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_302,axiom,(
    ! [VarCurr: state_type] :
      ( v952(VarCurr,0)
    <=> v15(VarCurr) ) )).

tff(addAssignment_301,axiom,(
    ! [VarCurr: state_type] :
      ( v952(VarCurr,1)
    <=> v56(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_172,axiom,(
    ! [VarCurr: state_type] :
      ( v947(VarCurr)
    <=> ( v948(VarCurr)
        & v54(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_171,axiom,(
    ! [VarCurr: state_type] :
      ( v948(VarCurr)
    <=> ( v949(VarCurr)
        & v53(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_170,axiom,(
    ! [VarCurr: state_type] :
      ( v949(VarCurr)
    <=> ( v15(VarCurr)
        & v52(VarCurr) ) ) )).

tff(addAssignment_300,axiom,(
    ! [VarNext: state_type] :
      ( v7(VarNext,2)
    <=> v929(VarNext,1) ) )).

tff(addCaseBooleanConditionShiftedRanges1_17,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v930(VarNext)
       => ( ( v929(VarNext,1)
          <=> v7(VarCurr,2) )
          & ( v929(VarNext,0)
          <=> v7(VarCurr,1) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_37,axiom,(
    ! [VarNext: state_type] :
      ( v930(VarNext)
     => ! [B: $int] :
          ( ( $less(B,2)
            & ~ $less(B,0) )
         => ( v929(VarNext,B)
          <=> v940(VarNext,B) ) ) ) )).

tff(addAssignment_299,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,2)
            & ~ $less(B,0) )
         => ( v940(VarNext,B)
          <=> v938(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondShiftedRangesElseBranch_5,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v941(VarCurr)
     => ( ( v938(VarCurr,1)
        <=> v13(VarCurr,2) )
        & ( v938(VarCurr,0)
        <=> v13(VarCurr,1) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_30,axiom,(
    ! [VarCurr: state_type] :
      ( v941(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,2)
            & ~ $less(B,0) )
         => ( v938(VarCurr,B)
          <=> $false ) ) ) )).

tff(writeUnaryOperator_66,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v941(VarCurr)
    <=> v9(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_169,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v930(VarNext)
      <=> v931(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_168,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v931(VarNext)
      <=> ( v932(VarNext)
          & v112(VarNext) ) ) ) )).

tff(writeUnaryOperator_65,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v932(VarNext)
      <=> v934(VarNext) ) ) )).

tff(addAssignment_298,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v934(VarNext)
      <=> v112(VarCurr) ) ) )).

tff(addConditionBooleanCondShiftedRangesElseBranch_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v917(VarCurr)
     => ( v13(VarCurr,2)
      <=> $false ) ) )).

tff(addConditionBooleanCondShiftedRangesThenBranch_3,axiom,(
    ! [VarCurr: state_type] :
      ( v917(VarCurr)
     => ( v13(VarCurr,2)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_167,axiom,(
    ! [VarCurr: state_type] :
      ( v917(VarCurr)
    <=> ( v918(VarCurr)
        | v921(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_166,axiom,(
    ! [VarCurr: state_type] :
      ( v921(VarCurr)
    <=> ( v922(VarCurr)
        & v927(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_67,axiom,(
    ! [VarCurr: state_type] :
      ( v927(VarCurr)
    <=> ( $true
      <=> v7(VarCurr,2) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_165,axiom,(
    ! [VarCurr: state_type] :
      ( v922(VarCurr)
    <=> ( v923(VarCurr)
        | v925(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_66,axiom,(
    ! [VarCurr: state_type] :
      ( v925(VarCurr)
    <=> ( ( v926(VarCurr,1)
        <=> $false )
        & ( v926(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_297,axiom,(
    ! [VarCurr: state_type] :
      ( v926(VarCurr,0)
    <=> v15(VarCurr) ) )).

tff(addAssignment_296,axiom,(
    ! [VarCurr: state_type] :
      ( v926(VarCurr,1)
    <=> v56(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_65,axiom,(
    ! [VarCurr: state_type] :
      ( v923(VarCurr)
    <=> ( ( v924(VarCurr,1)
        <=> $false )
        & ( v924(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_295,axiom,(
    ! [VarCurr: state_type] :
      ( v924(VarCurr,0)
    <=> v15(VarCurr) ) )).

tff(addAssignment_294,axiom,(
    ! [VarCurr: state_type] :
      ( v924(VarCurr,1)
    <=> v56(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_164,axiom,(
    ! [VarCurr: state_type] :
      ( v918(VarCurr)
    <=> ( v919(VarCurr)
        & v54(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_163,axiom,(
    ! [VarCurr: state_type] :
      ( v919(VarCurr)
    <=> ( v920(VarCurr)
        & v53(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_162,axiom,(
    ! [VarCurr: state_type] :
      ( v920(VarCurr)
    <=> ( v76(VarCurr)
        & v623(VarCurr) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_28,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v30(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,2)
            & ~ $less(B,0) )
         => ( v28(VarCurr,B)
          <=> v888(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_29,axiom,(
    ! [VarCurr: state_type] :
      ( v30(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,2)
            & ~ $less(B,0) )
         => ( v28(VarCurr,B)
          <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges3_2,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v889(VarCurr)
        & ~ v909(VarCurr)
        & ~ v910(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,2)
            & ~ $less(B,0) )
         => ( v888(VarCurr,B)
          <=> $true ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_10,axiom,(
    ! [VarCurr: state_type] :
      ( v910(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,2)
            & ~ $less(B,0) )
         => ( v888(VarCurr,B)
          <=> b10(B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_18,axiom,(
    ! [VarCurr: state_type] :
      ( v909(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,2)
            & ~ $less(B,0) )
         => ( v888(VarCurr,B)
          <=> b01(B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_12,axiom,(
    ! [VarCurr: state_type] :
      ( v889(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,2)
            & ~ $less(B,0) )
         => ( v888(VarCurr,B)
          <=> $false ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_161,axiom,(
    ! [VarCurr: state_type] :
      ( v910(VarCurr)
    <=> ( v912(VarCurr)
        | v915(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_64,axiom,(
    ! [VarCurr: state_type] :
      ( v915(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $true )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $true )
        & ( v770(VarCurr,2)
        <=> $false )
        & ( v770(VarCurr,1)
        <=> $true )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_268,axiom,(
    b1001010(6) )).

tff(bitBlastConstant_267,axiom,(
    ~ b1001010(5) )).

tff(bitBlastConstant_266,axiom,(
    ~ b1001010(4) )).

tff(bitBlastConstant_265,axiom,(
    b1001010(3) )).

tff(bitBlastConstant_264,axiom,(
    ~ b1001010(2) )).

tff(bitBlastConstant_263,axiom,(
    b1001010(1) )).

tff(bitBlastConstant_262,axiom,(
    ~ b1001010(0) )).

tff(writeBinaryOperatorEqualRangesSingleBits_160,axiom,(
    ! [VarCurr: state_type] :
      ( v912(VarCurr)
    <=> ( v913(VarCurr)
        | v914(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_63,axiom,(
    ! [VarCurr: state_type] :
      ( v914(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $false )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $true )
        & ( v770(VarCurr,2)
        <=> $false )
        & ( v770(VarCurr,1)
        <=> $true )
        & ( v770(VarCurr,0)
        <=> $true ) ) ) )).

tff(bitBlastConstant_261,axiom,(
    ~ b0001011(6) )).

tff(bitBlastConstant_260,axiom,(
    ~ b0001011(5) )).

tff(bitBlastConstant_259,axiom,(
    ~ b0001011(4) )).

tff(bitBlastConstant_258,axiom,(
    b0001011(3) )).

tff(bitBlastConstant_257,axiom,(
    ~ b0001011(2) )).

tff(bitBlastConstant_256,axiom,(
    b0001011(1) )).

tff(bitBlastConstant_255,axiom,(
    b0001011(0) )).

tff(addBitVectorEqualityBitBlasted_62,axiom,(
    ! [VarCurr: state_type] :
      ( v913(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $false )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $true )
        & ( v770(VarCurr,2)
        <=> $false )
        & ( v770(VarCurr,1)
        <=> $true )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_254,axiom,(
    ~ b0001010(6) )).

tff(bitBlastConstant_253,axiom,(
    ~ b0001010(5) )).

tff(bitBlastConstant_252,axiom,(
    ~ b0001010(4) )).

tff(bitBlastConstant_251,axiom,(
    b0001010(3) )).

tff(bitBlastConstant_250,axiom,(
    ~ b0001010(2) )).

tff(bitBlastConstant_249,axiom,(
    b0001010(1) )).

tff(bitBlastConstant_248,axiom,(
    ~ b0001010(0) )).

tff(addBitVectorEqualityBitBlasted_61,axiom,(
    ! [VarCurr: state_type] :
      ( v909(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $true )
        & ( v770(VarCurr,5)
        <=> $true )
        & ( v770(VarCurr,4)
        <=> $true )
        & ( v770(VarCurr,3)
        <=> $true )
        & ( v770(VarCurr,2)
        <=> $false )
        & ( v770(VarCurr,1)
        <=> $true )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_247,axiom,(
    b1111010(6) )).

tff(bitBlastConstant_246,axiom,(
    b1111010(5) )).

tff(bitBlastConstant_245,axiom,(
    b1111010(4) )).

tff(bitBlastConstant_244,axiom,(
    b1111010(3) )).

tff(bitBlastConstant_243,axiom,(
    ~ b1111010(2) )).

tff(bitBlastConstant_242,axiom,(
    b1111010(1) )).

tff(bitBlastConstant_241,axiom,(
    ~ b1111010(0) )).

tff(writeBinaryOperatorEqualRangesSingleBits_159,axiom,(
    ! [VarCurr: state_type] :
      ( v889(VarCurr)
    <=> ( v891(VarCurr)
        | v908(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_60,axiom,(
    ! [VarCurr: state_type] :
      ( v908(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $true )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $false )
        & ( v770(VarCurr,2)
        <=> $true )
        & ( v770(VarCurr,1)
        <=> $false )
        & ( v770(VarCurr,0)
        <=> $true ) ) ) )).

tff(bitBlastConstant_240,axiom,(
    b1000101(6) )).

tff(bitBlastConstant_239,axiom,(
    ~ b1000101(5) )).

tff(bitBlastConstant_238,axiom,(
    ~ b1000101(4) )).

tff(bitBlastConstant_237,axiom,(
    ~ b1000101(3) )).

tff(bitBlastConstant_236,axiom,(
    b1000101(2) )).

tff(bitBlastConstant_235,axiom,(
    ~ b1000101(1) )).

tff(bitBlastConstant_234,axiom,(
    b1000101(0) )).

tff(writeBinaryOperatorEqualRangesSingleBits_158,axiom,(
    ! [VarCurr: state_type] :
      ( v891(VarCurr)
    <=> ( v892(VarCurr)
        | v907(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_59,axiom,(
    ! [VarCurr: state_type] :
      ( v907(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $true )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $false )
        & ( v770(VarCurr,2)
        <=> $true )
        & ( v770(VarCurr,1)
        <=> $false )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_233,axiom,(
    b1000100(6) )).

tff(bitBlastConstant_232,axiom,(
    ~ b1000100(5) )).

tff(bitBlastConstant_231,axiom,(
    ~ b1000100(4) )).

tff(bitBlastConstant_230,axiom,(
    ~ b1000100(3) )).

tff(bitBlastConstant_229,axiom,(
    b1000100(2) )).

tff(bitBlastConstant_228,axiom,(
    ~ b1000100(1) )).

tff(bitBlastConstant_227,axiom,(
    ~ b1000100(0) )).

tff(writeBinaryOperatorEqualRangesSingleBits_157,axiom,(
    ! [VarCurr: state_type] :
      ( v892(VarCurr)
    <=> ( v893(VarCurr)
        | v906(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_58,axiom,(
    ! [VarCurr: state_type] :
      ( v906(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $true )
        & ( v770(VarCurr,5)
        <=> $true )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $false )
        & ( v770(VarCurr,2)
        <=> $false )
        & ( v770(VarCurr,1)
        <=> $false )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_226,axiom,(
    b1100000(6) )).

tff(bitBlastConstant_225,axiom,(
    b1100000(5) )).

tff(bitBlastConstant_224,axiom,(
    ~ b1100000(4) )).

tff(bitBlastConstant_223,axiom,(
    ~ b1100000(3) )).

tff(bitBlastConstant_222,axiom,(
    ~ b1100000(2) )).

tff(bitBlastConstant_221,axiom,(
    ~ b1100000(1) )).

tff(bitBlastConstant_220,axiom,(
    ~ b1100000(0) )).

tff(writeBinaryOperatorEqualRangesSingleBits_156,axiom,(
    ! [VarCurr: state_type] :
      ( v893(VarCurr)
    <=> ( v894(VarCurr)
        | v905(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_57,axiom,(
    ! [VarCurr: state_type] :
      ( v905(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $true )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $false )
        & ( v770(VarCurr,2)
        <=> $false )
        & ( v770(VarCurr,1)
        <=> $false )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_219,axiom,(
    b1000000(6) )).

tff(bitBlastConstant_218,axiom,(
    ~ b1000000(5) )).

tff(bitBlastConstant_217,axiom,(
    ~ b1000000(4) )).

tff(bitBlastConstant_216,axiom,(
    ~ b1000000(3) )).

tff(bitBlastConstant_215,axiom,(
    ~ b1000000(2) )).

tff(bitBlastConstant_214,axiom,(
    ~ b1000000(1) )).

tff(bitBlastConstant_213,axiom,(
    ~ b1000000(0) )).

tff(writeBinaryOperatorEqualRangesSingleBits_155,axiom,(
    ! [VarCurr: state_type] :
      ( v894(VarCurr)
    <=> ( v895(VarCurr)
        | v904(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_56,axiom,(
    ! [VarCurr: state_type] :
      ( v904(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $true )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $false )
        & ( v770(VarCurr,2)
        <=> $false )
        & ( v770(VarCurr,1)
        <=> $true )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_212,axiom,(
    b1000010(6) )).

tff(bitBlastConstant_211,axiom,(
    ~ b1000010(5) )).

tff(bitBlastConstant_210,axiom,(
    ~ b1000010(4) )).

tff(bitBlastConstant_209,axiom,(
    ~ b1000010(3) )).

tff(bitBlastConstant_208,axiom,(
    ~ b1000010(2) )).

tff(bitBlastConstant_207,axiom,(
    b1000010(1) )).

tff(bitBlastConstant_206,axiom,(
    ~ b1000010(0) )).

tff(writeBinaryOperatorEqualRangesSingleBits_154,axiom,(
    ! [VarCurr: state_type] :
      ( v895(VarCurr)
    <=> ( v896(VarCurr)
        | v903(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_55,axiom,(
    ! [VarCurr: state_type] :
      ( v903(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $false )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $false )
        & ( v770(VarCurr,2)
        <=> $true )
        & ( v770(VarCurr,1)
        <=> $false )
        & ( v770(VarCurr,0)
        <=> $true ) ) ) )).

tff(bitBlastConstant_205,axiom,(
    ~ b0000101(6) )).

tff(bitBlastConstant_204,axiom,(
    ~ b0000101(5) )).

tff(bitBlastConstant_203,axiom,(
    ~ b0000101(4) )).

tff(bitBlastConstant_202,axiom,(
    ~ b0000101(3) )).

tff(bitBlastConstant_201,axiom,(
    b0000101(2) )).

tff(bitBlastConstant_200,axiom,(
    ~ b0000101(1) )).

tff(bitBlastConstant_199,axiom,(
    b0000101(0) )).

tff(writeBinaryOperatorEqualRangesSingleBits_153,axiom,(
    ! [VarCurr: state_type] :
      ( v896(VarCurr)
    <=> ( v897(VarCurr)
        | v902(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_54,axiom,(
    ! [VarCurr: state_type] :
      ( v902(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $false )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $false )
        & ( v770(VarCurr,2)
        <=> $true )
        & ( v770(VarCurr,1)
        <=> $false )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_198,axiom,(
    ~ b0000100(6) )).

tff(bitBlastConstant_197,axiom,(
    ~ b0000100(5) )).

tff(bitBlastConstant_196,axiom,(
    ~ b0000100(4) )).

tff(bitBlastConstant_195,axiom,(
    ~ b0000100(3) )).

tff(bitBlastConstant_194,axiom,(
    b0000100(2) )).

tff(bitBlastConstant_193,axiom,(
    ~ b0000100(1) )).

tff(bitBlastConstant_192,axiom,(
    ~ b0000100(0) )).

tff(writeBinaryOperatorEqualRangesSingleBits_152,axiom,(
    ! [VarCurr: state_type] :
      ( v897(VarCurr)
    <=> ( v898(VarCurr)
        | v901(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_53,axiom,(
    ! [VarCurr: state_type] :
      ( v901(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $false )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $false )
        & ( v770(VarCurr,2)
        <=> $false )
        & ( v770(VarCurr,1)
        <=> $true )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_191,axiom,(
    ~ b0000010(6) )).

tff(bitBlastConstant_190,axiom,(
    ~ b0000010(5) )).

tff(bitBlastConstant_189,axiom,(
    ~ b0000010(4) )).

tff(bitBlastConstant_188,axiom,(
    ~ b0000010(3) )).

tff(bitBlastConstant_187,axiom,(
    ~ b0000010(2) )).

tff(bitBlastConstant_186,axiom,(
    b0000010(1) )).

tff(bitBlastConstant_185,axiom,(
    ~ b0000010(0) )).

tff(writeBinaryOperatorEqualRangesSingleBits_151,axiom,(
    ! [VarCurr: state_type] :
      ( v898(VarCurr)
    <=> ( v899(VarCurr)
        | v900(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_52,axiom,(
    ! [VarCurr: state_type] :
      ( v900(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $false )
        & ( v770(VarCurr,5)
        <=> $true )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $false )
        & ( v770(VarCurr,2)
        <=> $false )
        & ( v770(VarCurr,1)
        <=> $false )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_184,axiom,(
    ~ b0100000(6) )).

tff(bitBlastConstant_183,axiom,(
    b0100000(5) )).

tff(bitBlastConstant_182,axiom,(
    ~ b0100000(4) )).

tff(bitBlastConstant_181,axiom,(
    ~ b0100000(3) )).

tff(bitBlastConstant_180,axiom,(
    ~ b0100000(2) )).

tff(bitBlastConstant_179,axiom,(
    ~ b0100000(1) )).

tff(bitBlastConstant_178,axiom,(
    ~ b0100000(0) )).

tff(addBitVectorEqualityBitBlasted_51,axiom,(
    ! [VarCurr: state_type] :
      ( v899(VarCurr)
    <=> ( ( v770(VarCurr,6)
        <=> $false )
        & ( v770(VarCurr,5)
        <=> $false )
        & ( v770(VarCurr,4)
        <=> $false )
        & ( v770(VarCurr,3)
        <=> $false )
        & ( v770(VarCurr,2)
        <=> $false )
        & ( v770(VarCurr,1)
        <=> $false )
        & ( v770(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_177,axiom,(
    ~ b0000000(6) )).

tff(bitBlastConstant_176,axiom,(
    ~ b0000000(5) )).

tff(bitBlastConstant_175,axiom,(
    ~ b0000000(4) )).

tff(bitBlastConstant_174,axiom,(
    ~ b0000000(3) )).

tff(bitBlastConstant_173,axiom,(
    ~ b0000000(2) )).

tff(bitBlastConstant_172,axiom,(
    ~ b0000000(1) )).

tff(bitBlastConstant_171,axiom,(
    ~ b0000000(0) )).

tff(addAssignment_293,axiom,(
    ! [VarCurr: state_type] :
      ( ( v770(VarCurr,6)
      <=> v90(VarCurr,69) )
      & ( v770(VarCurr,5)
      <=> v90(VarCurr,68) )
      & ( v770(VarCurr,4)
      <=> v90(VarCurr,67) )
      & ( v770(VarCurr,3)
      <=> v90(VarCurr,66) )
      & ( v770(VarCurr,2)
      <=> v90(VarCurr,65) )
      & ( v770(VarCurr,1)
      <=> v90(VarCurr,64) )
      & ( v770(VarCurr,0)
      <=> v90(VarCurr,63) ) ) )).

tff(addAssignment_292,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v90(VarCurr,B)
      <=> v92(VarCurr,B) ) ) )).

tff(addAssignment_291,axiom,(
    ! [VarCurr: state_type] :
      ( ( v92(VarCurr,69)
      <=> v94(VarCurr,559) )
      & ( v92(VarCurr,68)
      <=> v94(VarCurr,558) )
      & ( v92(VarCurr,67)
      <=> v94(VarCurr,557) )
      & ( v92(VarCurr,66)
      <=> v94(VarCurr,556) )
      & ( v92(VarCurr,65)
      <=> v94(VarCurr,555) )
      & ( v92(VarCurr,64)
      <=> v94(VarCurr,554) )
      & ( v92(VarCurr,63)
      <=> v94(VarCurr,553) ) ) )).

tff(addAssignment_290,axiom,(
    ! [VarNext: state_type] :
      ( ( v94(VarNext,559)
      <=> v866(VarNext,69) )
      & ( v94(VarNext,558)
      <=> v866(VarNext,68) )
      & ( v94(VarNext,557)
      <=> v866(VarNext,67) )
      & ( v94(VarNext,556)
      <=> v866(VarNext,66) )
      & ( v94(VarNext,555)
      <=> v866(VarNext,65) )
      & ( v94(VarNext,554)
      <=> v866(VarNext,64) )
      & ( v94(VarNext,553)
      <=> v866(VarNext,63) ) ) )).

tff(addCaseBooleanConditionShiftedRanges1_16,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v868(VarNext)
       => ( ( v866(VarNext,69)
          <=> v94(VarCurr,559) )
          & ( v866(VarNext,68)
          <=> v94(VarCurr,558) )
          & ( v866(VarNext,67)
          <=> v94(VarCurr,557) )
          & ( v866(VarNext,66)
          <=> v94(VarCurr,556) )
          & ( v866(VarNext,65)
          <=> v94(VarCurr,555) )
          & ( v866(VarNext,64)
          <=> v94(VarCurr,554) )
          & ( v866(VarNext,63)
          <=> v94(VarCurr,553) )
          & ( v866(VarNext,62)
          <=> v94(VarCurr,552) )
          & ( v866(VarNext,61)
          <=> v94(VarCurr,551) )
          & ( v866(VarNext,60)
          <=> v94(VarCurr,550) )
          & ( v866(VarNext,59)
          <=> v94(VarCurr,549) )
          & ( v866(VarNext,58)
          <=> v94(VarCurr,548) )
          & ( v866(VarNext,57)
          <=> v94(VarCurr,547) )
          & ( v866(VarNext,56)
          <=> v94(VarCurr,546) )
          & ( v866(VarNext,55)
          <=> v94(VarCurr,545) )
          & ( v866(VarNext,54)
          <=> v94(VarCurr,544) )
          & ( v866(VarNext,53)
          <=> v94(VarCurr,543) )
          & ( v866(VarNext,52)
          <=> v94(VarCurr,542) )
          & ( v866(VarNext,51)
          <=> v94(VarCurr,541) )
          & ( v866(VarNext,50)
          <=> v94(VarCurr,540) )
          & ( v866(VarNext,49)
          <=> v94(VarCurr,539) )
          & ( v866(VarNext,48)
          <=> v94(VarCurr,538) )
          & ( v866(VarNext,47)
          <=> v94(VarCurr,537) )
          & ( v866(VarNext,46)
          <=> v94(VarCurr,536) )
          & ( v866(VarNext,45)
          <=> v94(VarCurr,535) )
          & ( v866(VarNext,44)
          <=> v94(VarCurr,534) )
          & ( v866(VarNext,43)
          <=> v94(VarCurr,533) )
          & ( v866(VarNext,42)
          <=> v94(VarCurr,532) )
          & ( v866(VarNext,41)
          <=> v94(VarCurr,531) )
          & ( v866(VarNext,40)
          <=> v94(VarCurr,530) )
          & ( v866(VarNext,39)
          <=> v94(VarCurr,529) )
          & ( v866(VarNext,38)
          <=> v94(VarCurr,528) )
          & ( v866(VarNext,37)
          <=> v94(VarCurr,527) )
          & ( v866(VarNext,36)
          <=> v94(VarCurr,526) )
          & ( v866(VarNext,35)
          <=> v94(VarCurr,525) )
          & ( v866(VarNext,34)
          <=> v94(VarCurr,524) )
          & ( v866(VarNext,33)
          <=> v94(VarCurr,523) )
          & ( v866(VarNext,32)
          <=> v94(VarCurr,522) )
          & ( v866(VarNext,31)
          <=> v94(VarCurr,521) )
          & ( v866(VarNext,30)
          <=> v94(VarCurr,520) )
          & ( v866(VarNext,29)
          <=> v94(VarCurr,519) )
          & ( v866(VarNext,28)
          <=> v94(VarCurr,518) )
          & ( v866(VarNext,27)
          <=> v94(VarCurr,517) )
          & ( v866(VarNext,26)
          <=> v94(VarCurr,516) )
          & ( v866(VarNext,25)
          <=> v94(VarCurr,515) )
          & ( v866(VarNext,24)
          <=> v94(VarCurr,514) )
          & ( v866(VarNext,23)
          <=> v94(VarCurr,513) )
          & ( v866(VarNext,22)
          <=> v94(VarCurr,512) )
          & ( v866(VarNext,21)
          <=> v94(VarCurr,511) )
          & ( v866(VarNext,20)
          <=> v94(VarCurr,510) )
          & ( v866(VarNext,19)
          <=> v94(VarCurr,509) )
          & ( v866(VarNext,18)
          <=> v94(VarCurr,508) )
          & ( v866(VarNext,17)
          <=> v94(VarCurr,507) )
          & ( v866(VarNext,16)
          <=> v94(VarCurr,506) )
          & ( v866(VarNext,15)
          <=> v94(VarCurr,505) )
          & ( v866(VarNext,14)
          <=> v94(VarCurr,504) )
          & ( v866(VarNext,13)
          <=> v94(VarCurr,503) )
          & ( v866(VarNext,12)
          <=> v94(VarCurr,502) )
          & ( v866(VarNext,11)
          <=> v94(VarCurr,501) )
          & ( v866(VarNext,10)
          <=> v94(VarCurr,500) )
          & ( v866(VarNext,9)
          <=> v94(VarCurr,499) )
          & ( v866(VarNext,8)
          <=> v94(VarCurr,498) )
          & ( v866(VarNext,7)
          <=> v94(VarCurr,497) )
          & ( v866(VarNext,6)
          <=> v94(VarCurr,496) )
          & ( v866(VarNext,5)
          <=> v94(VarCurr,495) )
          & ( v866(VarNext,4)
          <=> v94(VarCurr,494) )
          & ( v866(VarNext,3)
          <=> v94(VarCurr,493) )
          & ( v866(VarNext,2)
          <=> v94(VarCurr,492) )
          & ( v866(VarNext,1)
          <=> v94(VarCurr,491) )
          & ( v866(VarNext,0)
          <=> v94(VarCurr,490) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_36,axiom,(
    ! [VarNext: state_type] :
      ( v868(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v866(VarNext,B)
          <=> v548(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_150,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v868(VarNext)
      <=> ( v869(VarNext)
          & v530(VarNext) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_149,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v869(VarNext)
      <=> ( v871(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_64,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v871(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_289,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v514(VarCurr,B)
      <=> v519(VarCurr,B) ) ) )).

tff(addAssignment_288,axiom,(
    ! [VarCurr: state_type] :
      ( ( v518(VarCurr,69)
      <=> v94(VarCurr,489) )
      & ( v518(VarCurr,68)
      <=> v94(VarCurr,488) )
      & ( v518(VarCurr,67)
      <=> v94(VarCurr,487) )
      & ( v518(VarCurr,66)
      <=> v94(VarCurr,486) )
      & ( v518(VarCurr,65)
      <=> v94(VarCurr,485) )
      & ( v518(VarCurr,64)
      <=> v94(VarCurr,484) )
      & ( v518(VarCurr,63)
      <=> v94(VarCurr,483) ) ) )).

tff(addAssignment_287,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v507(VarCurr,B)
      <=> v512(VarCurr,B) ) ) )).

tff(addAssignment_286,axiom,(
    ! [VarCurr: state_type] :
      ( ( v511(VarCurr,69)
      <=> v94(VarCurr,559) )
      & ( v511(VarCurr,68)
      <=> v94(VarCurr,558) )
      & ( v511(VarCurr,67)
      <=> v94(VarCurr,557) )
      & ( v511(VarCurr,66)
      <=> v94(VarCurr,556) )
      & ( v511(VarCurr,65)
      <=> v94(VarCurr,555) )
      & ( v511(VarCurr,64)
      <=> v94(VarCurr,554) )
      & ( v511(VarCurr,63)
      <=> v94(VarCurr,553) ) ) )).

tff(addAssignment_285,axiom,(
    ! [VarNext: state_type] :
      ( ( v94(VarNext,489)
      <=> v858(VarNext,69) )
      & ( v94(VarNext,488)
      <=> v858(VarNext,68) )
      & ( v94(VarNext,487)
      <=> v858(VarNext,67) )
      & ( v94(VarNext,486)
      <=> v858(VarNext,66) )
      & ( v94(VarNext,485)
      <=> v858(VarNext,65) )
      & ( v94(VarNext,484)
      <=> v858(VarNext,64) )
      & ( v94(VarNext,483)
      <=> v858(VarNext,63) ) ) )).

tff(addCaseBooleanConditionShiftedRanges1_15,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v860(VarNext)
       => ( ( v858(VarNext,69)
          <=> v94(VarCurr,489) )
          & ( v858(VarNext,68)
          <=> v94(VarCurr,488) )
          & ( v858(VarNext,67)
          <=> v94(VarCurr,487) )
          & ( v858(VarNext,66)
          <=> v94(VarCurr,486) )
          & ( v858(VarNext,65)
          <=> v94(VarCurr,485) )
          & ( v858(VarNext,64)
          <=> v94(VarCurr,484) )
          & ( v858(VarNext,63)
          <=> v94(VarCurr,483) )
          & ( v858(VarNext,62)
          <=> v94(VarCurr,482) )
          & ( v858(VarNext,61)
          <=> v94(VarCurr,481) )
          & ( v858(VarNext,60)
          <=> v94(VarCurr,480) )
          & ( v858(VarNext,59)
          <=> v94(VarCurr,479) )
          & ( v858(VarNext,58)
          <=> v94(VarCurr,478) )
          & ( v858(VarNext,57)
          <=> v94(VarCurr,477) )
          & ( v858(VarNext,56)
          <=> v94(VarCurr,476) )
          & ( v858(VarNext,55)
          <=> v94(VarCurr,475) )
          & ( v858(VarNext,54)
          <=> v94(VarCurr,474) )
          & ( v858(VarNext,53)
          <=> v94(VarCurr,473) )
          & ( v858(VarNext,52)
          <=> v94(VarCurr,472) )
          & ( v858(VarNext,51)
          <=> v94(VarCurr,471) )
          & ( v858(VarNext,50)
          <=> v94(VarCurr,470) )
          & ( v858(VarNext,49)
          <=> v94(VarCurr,469) )
          & ( v858(VarNext,48)
          <=> v94(VarCurr,468) )
          & ( v858(VarNext,47)
          <=> v94(VarCurr,467) )
          & ( v858(VarNext,46)
          <=> v94(VarCurr,466) )
          & ( v858(VarNext,45)
          <=> v94(VarCurr,465) )
          & ( v858(VarNext,44)
          <=> v94(VarCurr,464) )
          & ( v858(VarNext,43)
          <=> v94(VarCurr,463) )
          & ( v858(VarNext,42)
          <=> v94(VarCurr,462) )
          & ( v858(VarNext,41)
          <=> v94(VarCurr,461) )
          & ( v858(VarNext,40)
          <=> v94(VarCurr,460) )
          & ( v858(VarNext,39)
          <=> v94(VarCurr,459) )
          & ( v858(VarNext,38)
          <=> v94(VarCurr,458) )
          & ( v858(VarNext,37)
          <=> v94(VarCurr,457) )
          & ( v858(VarNext,36)
          <=> v94(VarCurr,456) )
          & ( v858(VarNext,35)
          <=> v94(VarCurr,455) )
          & ( v858(VarNext,34)
          <=> v94(VarCurr,454) )
          & ( v858(VarNext,33)
          <=> v94(VarCurr,453) )
          & ( v858(VarNext,32)
          <=> v94(VarCurr,452) )
          & ( v858(VarNext,31)
          <=> v94(VarCurr,451) )
          & ( v858(VarNext,30)
          <=> v94(VarCurr,450) )
          & ( v858(VarNext,29)
          <=> v94(VarCurr,449) )
          & ( v858(VarNext,28)
          <=> v94(VarCurr,448) )
          & ( v858(VarNext,27)
          <=> v94(VarCurr,447) )
          & ( v858(VarNext,26)
          <=> v94(VarCurr,446) )
          & ( v858(VarNext,25)
          <=> v94(VarCurr,445) )
          & ( v858(VarNext,24)
          <=> v94(VarCurr,444) )
          & ( v858(VarNext,23)
          <=> v94(VarCurr,443) )
          & ( v858(VarNext,22)
          <=> v94(VarCurr,442) )
          & ( v858(VarNext,21)
          <=> v94(VarCurr,441) )
          & ( v858(VarNext,20)
          <=> v94(VarCurr,440) )
          & ( v858(VarNext,19)
          <=> v94(VarCurr,439) )
          & ( v858(VarNext,18)
          <=> v94(VarCurr,438) )
          & ( v858(VarNext,17)
          <=> v94(VarCurr,437) )
          & ( v858(VarNext,16)
          <=> v94(VarCurr,436) )
          & ( v858(VarNext,15)
          <=> v94(VarCurr,435) )
          & ( v858(VarNext,14)
          <=> v94(VarCurr,434) )
          & ( v858(VarNext,13)
          <=> v94(VarCurr,433) )
          & ( v858(VarNext,12)
          <=> v94(VarCurr,432) )
          & ( v858(VarNext,11)
          <=> v94(VarCurr,431) )
          & ( v858(VarNext,10)
          <=> v94(VarCurr,430) )
          & ( v858(VarNext,9)
          <=> v94(VarCurr,429) )
          & ( v858(VarNext,8)
          <=> v94(VarCurr,428) )
          & ( v858(VarNext,7)
          <=> v94(VarCurr,427) )
          & ( v858(VarNext,6)
          <=> v94(VarCurr,426) )
          & ( v858(VarNext,5)
          <=> v94(VarCurr,425) )
          & ( v858(VarNext,4)
          <=> v94(VarCurr,424) )
          & ( v858(VarNext,3)
          <=> v94(VarCurr,423) )
          & ( v858(VarNext,2)
          <=> v94(VarCurr,422) )
          & ( v858(VarNext,1)
          <=> v94(VarCurr,421) )
          & ( v858(VarNext,0)
          <=> v94(VarCurr,420) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_35,axiom,(
    ! [VarNext: state_type] :
      ( v860(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v858(VarNext,B)
          <=> v502(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_148,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v860(VarNext)
      <=> ( v861(VarNext)
          & v484(VarNext) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_147,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v861(VarNext)
      <=> ( v863(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_63,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v863(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_284,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v468(VarCurr,B)
      <=> v473(VarCurr,B) ) ) )).

tff(addAssignment_283,axiom,(
    ! [VarCurr: state_type] :
      ( ( v472(VarCurr,69)
      <=> v94(VarCurr,419) )
      & ( v472(VarCurr,68)
      <=> v94(VarCurr,418) )
      & ( v472(VarCurr,67)
      <=> v94(VarCurr,417) )
      & ( v472(VarCurr,66)
      <=> v94(VarCurr,416) )
      & ( v472(VarCurr,65)
      <=> v94(VarCurr,415) )
      & ( v472(VarCurr,64)
      <=> v94(VarCurr,414) )
      & ( v472(VarCurr,63)
      <=> v94(VarCurr,413) ) ) )).

tff(addAssignment_282,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v461(VarCurr,B)
      <=> v466(VarCurr,B) ) ) )).

tff(addAssignment_281,axiom,(
    ! [VarCurr: state_type] :
      ( ( v465(VarCurr,69)
      <=> v94(VarCurr,489) )
      & ( v465(VarCurr,68)
      <=> v94(VarCurr,488) )
      & ( v465(VarCurr,67)
      <=> v94(VarCurr,487) )
      & ( v465(VarCurr,66)
      <=> v94(VarCurr,486) )
      & ( v465(VarCurr,65)
      <=> v94(VarCurr,485) )
      & ( v465(VarCurr,64)
      <=> v94(VarCurr,484) )
      & ( v465(VarCurr,63)
      <=> v94(VarCurr,483) ) ) )).

tff(addAssignment_280,axiom,(
    ! [VarNext: state_type] :
      ( ( v94(VarNext,419)
      <=> v850(VarNext,69) )
      & ( v94(VarNext,418)
      <=> v850(VarNext,68) )
      & ( v94(VarNext,417)
      <=> v850(VarNext,67) )
      & ( v94(VarNext,416)
      <=> v850(VarNext,66) )
      & ( v94(VarNext,415)
      <=> v850(VarNext,65) )
      & ( v94(VarNext,414)
      <=> v850(VarNext,64) )
      & ( v94(VarNext,413)
      <=> v850(VarNext,63) ) ) )).

tff(addCaseBooleanConditionShiftedRanges1_14,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v852(VarNext)
       => ( ( v850(VarNext,69)
          <=> v94(VarCurr,419) )
          & ( v850(VarNext,68)
          <=> v94(VarCurr,418) )
          & ( v850(VarNext,67)
          <=> v94(VarCurr,417) )
          & ( v850(VarNext,66)
          <=> v94(VarCurr,416) )
          & ( v850(VarNext,65)
          <=> v94(VarCurr,415) )
          & ( v850(VarNext,64)
          <=> v94(VarCurr,414) )
          & ( v850(VarNext,63)
          <=> v94(VarCurr,413) )
          & ( v850(VarNext,62)
          <=> v94(VarCurr,412) )
          & ( v850(VarNext,61)
          <=> v94(VarCurr,411) )
          & ( v850(VarNext,60)
          <=> v94(VarCurr,410) )
          & ( v850(VarNext,59)
          <=> v94(VarCurr,409) )
          & ( v850(VarNext,58)
          <=> v94(VarCurr,408) )
          & ( v850(VarNext,57)
          <=> v94(VarCurr,407) )
          & ( v850(VarNext,56)
          <=> v94(VarCurr,406) )
          & ( v850(VarNext,55)
          <=> v94(VarCurr,405) )
          & ( v850(VarNext,54)
          <=> v94(VarCurr,404) )
          & ( v850(VarNext,53)
          <=> v94(VarCurr,403) )
          & ( v850(VarNext,52)
          <=> v94(VarCurr,402) )
          & ( v850(VarNext,51)
          <=> v94(VarCurr,401) )
          & ( v850(VarNext,50)
          <=> v94(VarCurr,400) )
          & ( v850(VarNext,49)
          <=> v94(VarCurr,399) )
          & ( v850(VarNext,48)
          <=> v94(VarCurr,398) )
          & ( v850(VarNext,47)
          <=> v94(VarCurr,397) )
          & ( v850(VarNext,46)
          <=> v94(VarCurr,396) )
          & ( v850(VarNext,45)
          <=> v94(VarCurr,395) )
          & ( v850(VarNext,44)
          <=> v94(VarCurr,394) )
          & ( v850(VarNext,43)
          <=> v94(VarCurr,393) )
          & ( v850(VarNext,42)
          <=> v94(VarCurr,392) )
          & ( v850(VarNext,41)
          <=> v94(VarCurr,391) )
          & ( v850(VarNext,40)
          <=> v94(VarCurr,390) )
          & ( v850(VarNext,39)
          <=> v94(VarCurr,389) )
          & ( v850(VarNext,38)
          <=> v94(VarCurr,388) )
          & ( v850(VarNext,37)
          <=> v94(VarCurr,387) )
          & ( v850(VarNext,36)
          <=> v94(VarCurr,386) )
          & ( v850(VarNext,35)
          <=> v94(VarCurr,385) )
          & ( v850(VarNext,34)
          <=> v94(VarCurr,384) )
          & ( v850(VarNext,33)
          <=> v94(VarCurr,383) )
          & ( v850(VarNext,32)
          <=> v94(VarCurr,382) )
          & ( v850(VarNext,31)
          <=> v94(VarCurr,381) )
          & ( v850(VarNext,30)
          <=> v94(VarCurr,380) )
          & ( v850(VarNext,29)
          <=> v94(VarCurr,379) )
          & ( v850(VarNext,28)
          <=> v94(VarCurr,378) )
          & ( v850(VarNext,27)
          <=> v94(VarCurr,377) )
          & ( v850(VarNext,26)
          <=> v94(VarCurr,376) )
          & ( v850(VarNext,25)
          <=> v94(VarCurr,375) )
          & ( v850(VarNext,24)
          <=> v94(VarCurr,374) )
          & ( v850(VarNext,23)
          <=> v94(VarCurr,373) )
          & ( v850(VarNext,22)
          <=> v94(VarCurr,372) )
          & ( v850(VarNext,21)
          <=> v94(VarCurr,371) )
          & ( v850(VarNext,20)
          <=> v94(VarCurr,370) )
          & ( v850(VarNext,19)
          <=> v94(VarCurr,369) )
          & ( v850(VarNext,18)
          <=> v94(VarCurr,368) )
          & ( v850(VarNext,17)
          <=> v94(VarCurr,367) )
          & ( v850(VarNext,16)
          <=> v94(VarCurr,366) )
          & ( v850(VarNext,15)
          <=> v94(VarCurr,365) )
          & ( v850(VarNext,14)
          <=> v94(VarCurr,364) )
          & ( v850(VarNext,13)
          <=> v94(VarCurr,363) )
          & ( v850(VarNext,12)
          <=> v94(VarCurr,362) )
          & ( v850(VarNext,11)
          <=> v94(VarCurr,361) )
          & ( v850(VarNext,10)
          <=> v94(VarCurr,360) )
          & ( v850(VarNext,9)
          <=> v94(VarCurr,359) )
          & ( v850(VarNext,8)
          <=> v94(VarCurr,358) )
          & ( v850(VarNext,7)
          <=> v94(VarCurr,357) )
          & ( v850(VarNext,6)
          <=> v94(VarCurr,356) )
          & ( v850(VarNext,5)
          <=> v94(VarCurr,355) )
          & ( v850(VarNext,4)
          <=> v94(VarCurr,354) )
          & ( v850(VarNext,3)
          <=> v94(VarCurr,353) )
          & ( v850(VarNext,2)
          <=> v94(VarCurr,352) )
          & ( v850(VarNext,1)
          <=> v94(VarCurr,351) )
          & ( v850(VarNext,0)
          <=> v94(VarCurr,350) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_34,axiom,(
    ! [VarNext: state_type] :
      ( v852(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v850(VarNext,B)
          <=> v456(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_146,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v852(VarNext)
      <=> ( v853(VarNext)
          & v438(VarNext) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_145,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v853(VarNext)
      <=> ( v855(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_62,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v855(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_279,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v422(VarCurr,B)
      <=> v427(VarCurr,B) ) ) )).

tff(addAssignment_278,axiom,(
    ! [VarCurr: state_type] :
      ( ( v426(VarCurr,69)
      <=> v94(VarCurr,349) )
      & ( v426(VarCurr,68)
      <=> v94(VarCurr,348) )
      & ( v426(VarCurr,67)
      <=> v94(VarCurr,347) )
      & ( v426(VarCurr,66)
      <=> v94(VarCurr,346) )
      & ( v426(VarCurr,65)
      <=> v94(VarCurr,345) )
      & ( v426(VarCurr,64)
      <=> v94(VarCurr,344) )
      & ( v426(VarCurr,63)
      <=> v94(VarCurr,343) ) ) )).

tff(addAssignment_277,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v415(VarCurr,B)
      <=> v420(VarCurr,B) ) ) )).

tff(addAssignment_276,axiom,(
    ! [VarCurr: state_type] :
      ( ( v419(VarCurr,69)
      <=> v94(VarCurr,419) )
      & ( v419(VarCurr,68)
      <=> v94(VarCurr,418) )
      & ( v419(VarCurr,67)
      <=> v94(VarCurr,417) )
      & ( v419(VarCurr,66)
      <=> v94(VarCurr,416) )
      & ( v419(VarCurr,65)
      <=> v94(VarCurr,415) )
      & ( v419(VarCurr,64)
      <=> v94(VarCurr,414) )
      & ( v419(VarCurr,63)
      <=> v94(VarCurr,413) ) ) )).

tff(addAssignment_275,axiom,(
    ! [VarNext: state_type] :
      ( ( v94(VarNext,349)
      <=> v842(VarNext,69) )
      & ( v94(VarNext,348)
      <=> v842(VarNext,68) )
      & ( v94(VarNext,347)
      <=> v842(VarNext,67) )
      & ( v94(VarNext,346)
      <=> v842(VarNext,66) )
      & ( v94(VarNext,345)
      <=> v842(VarNext,65) )
      & ( v94(VarNext,344)
      <=> v842(VarNext,64) )
      & ( v94(VarNext,343)
      <=> v842(VarNext,63) ) ) )).

tff(addCaseBooleanConditionShiftedRanges1_13,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v844(VarNext)
       => ( ( v842(VarNext,69)
          <=> v94(VarCurr,349) )
          & ( v842(VarNext,68)
          <=> v94(VarCurr,348) )
          & ( v842(VarNext,67)
          <=> v94(VarCurr,347) )
          & ( v842(VarNext,66)
          <=> v94(VarCurr,346) )
          & ( v842(VarNext,65)
          <=> v94(VarCurr,345) )
          & ( v842(VarNext,64)
          <=> v94(VarCurr,344) )
          & ( v842(VarNext,63)
          <=> v94(VarCurr,343) )
          & ( v842(VarNext,62)
          <=> v94(VarCurr,342) )
          & ( v842(VarNext,61)
          <=> v94(VarCurr,341) )
          & ( v842(VarNext,60)
          <=> v94(VarCurr,340) )
          & ( v842(VarNext,59)
          <=> v94(VarCurr,339) )
          & ( v842(VarNext,58)
          <=> v94(VarCurr,338) )
          & ( v842(VarNext,57)
          <=> v94(VarCurr,337) )
          & ( v842(VarNext,56)
          <=> v94(VarCurr,336) )
          & ( v842(VarNext,55)
          <=> v94(VarCurr,335) )
          & ( v842(VarNext,54)
          <=> v94(VarCurr,334) )
          & ( v842(VarNext,53)
          <=> v94(VarCurr,333) )
          & ( v842(VarNext,52)
          <=> v94(VarCurr,332) )
          & ( v842(VarNext,51)
          <=> v94(VarCurr,331) )
          & ( v842(VarNext,50)
          <=> v94(VarCurr,330) )
          & ( v842(VarNext,49)
          <=> v94(VarCurr,329) )
          & ( v842(VarNext,48)
          <=> v94(VarCurr,328) )
          & ( v842(VarNext,47)
          <=> v94(VarCurr,327) )
          & ( v842(VarNext,46)
          <=> v94(VarCurr,326) )
          & ( v842(VarNext,45)
          <=> v94(VarCurr,325) )
          & ( v842(VarNext,44)
          <=> v94(VarCurr,324) )
          & ( v842(VarNext,43)
          <=> v94(VarCurr,323) )
          & ( v842(VarNext,42)
          <=> v94(VarCurr,322) )
          & ( v842(VarNext,41)
          <=> v94(VarCurr,321) )
          & ( v842(VarNext,40)
          <=> v94(VarCurr,320) )
          & ( v842(VarNext,39)
          <=> v94(VarCurr,319) )
          & ( v842(VarNext,38)
          <=> v94(VarCurr,318) )
          & ( v842(VarNext,37)
          <=> v94(VarCurr,317) )
          & ( v842(VarNext,36)
          <=> v94(VarCurr,316) )
          & ( v842(VarNext,35)
          <=> v94(VarCurr,315) )
          & ( v842(VarNext,34)
          <=> v94(VarCurr,314) )
          & ( v842(VarNext,33)
          <=> v94(VarCurr,313) )
          & ( v842(VarNext,32)
          <=> v94(VarCurr,312) )
          & ( v842(VarNext,31)
          <=> v94(VarCurr,311) )
          & ( v842(VarNext,30)
          <=> v94(VarCurr,310) )
          & ( v842(VarNext,29)
          <=> v94(VarCurr,309) )
          & ( v842(VarNext,28)
          <=> v94(VarCurr,308) )
          & ( v842(VarNext,27)
          <=> v94(VarCurr,307) )
          & ( v842(VarNext,26)
          <=> v94(VarCurr,306) )
          & ( v842(VarNext,25)
          <=> v94(VarCurr,305) )
          & ( v842(VarNext,24)
          <=> v94(VarCurr,304) )
          & ( v842(VarNext,23)
          <=> v94(VarCurr,303) )
          & ( v842(VarNext,22)
          <=> v94(VarCurr,302) )
          & ( v842(VarNext,21)
          <=> v94(VarCurr,301) )
          & ( v842(VarNext,20)
          <=> v94(VarCurr,300) )
          & ( v842(VarNext,19)
          <=> v94(VarCurr,299) )
          & ( v842(VarNext,18)
          <=> v94(VarCurr,298) )
          & ( v842(VarNext,17)
          <=> v94(VarCurr,297) )
          & ( v842(VarNext,16)
          <=> v94(VarCurr,296) )
          & ( v842(VarNext,15)
          <=> v94(VarCurr,295) )
          & ( v842(VarNext,14)
          <=> v94(VarCurr,294) )
          & ( v842(VarNext,13)
          <=> v94(VarCurr,293) )
          & ( v842(VarNext,12)
          <=> v94(VarCurr,292) )
          & ( v842(VarNext,11)
          <=> v94(VarCurr,291) )
          & ( v842(VarNext,10)
          <=> v94(VarCurr,290) )
          & ( v842(VarNext,9)
          <=> v94(VarCurr,289) )
          & ( v842(VarNext,8)
          <=> v94(VarCurr,288) )
          & ( v842(VarNext,7)
          <=> v94(VarCurr,287) )
          & ( v842(VarNext,6)
          <=> v94(VarCurr,286) )
          & ( v842(VarNext,5)
          <=> v94(VarCurr,285) )
          & ( v842(VarNext,4)
          <=> v94(VarCurr,284) )
          & ( v842(VarNext,3)
          <=> v94(VarCurr,283) )
          & ( v842(VarNext,2)
          <=> v94(VarCurr,282) )
          & ( v842(VarNext,1)
          <=> v94(VarCurr,281) )
          & ( v842(VarNext,0)
          <=> v94(VarCurr,280) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_33,axiom,(
    ! [VarNext: state_type] :
      ( v844(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v842(VarNext,B)
          <=> v410(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_144,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v844(VarNext)
      <=> ( v845(VarNext)
          & v392(VarNext) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_143,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v845(VarNext)
      <=> ( v847(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_61,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v847(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_274,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v376(VarCurr,B)
      <=> v381(VarCurr,B) ) ) )).

tff(addAssignment_273,axiom,(
    ! [VarCurr: state_type] :
      ( ( v380(VarCurr,69)
      <=> v94(VarCurr,279) )
      & ( v380(VarCurr,68)
      <=> v94(VarCurr,278) )
      & ( v380(VarCurr,67)
      <=> v94(VarCurr,277) )
      & ( v380(VarCurr,66)
      <=> v94(VarCurr,276) )
      & ( v380(VarCurr,65)
      <=> v94(VarCurr,275) )
      & ( v380(VarCurr,64)
      <=> v94(VarCurr,274) )
      & ( v380(VarCurr,63)
      <=> v94(VarCurr,273) ) ) )).

tff(addAssignment_272,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v369(VarCurr,B)
      <=> v374(VarCurr,B) ) ) )).

tff(addAssignment_271,axiom,(
    ! [VarCurr: state_type] :
      ( ( v373(VarCurr,69)
      <=> v94(VarCurr,349) )
      & ( v373(VarCurr,68)
      <=> v94(VarCurr,348) )
      & ( v373(VarCurr,67)
      <=> v94(VarCurr,347) )
      & ( v373(VarCurr,66)
      <=> v94(VarCurr,346) )
      & ( v373(VarCurr,65)
      <=> v94(VarCurr,345) )
      & ( v373(VarCurr,64)
      <=> v94(VarCurr,344) )
      & ( v373(VarCurr,63)
      <=> v94(VarCurr,343) ) ) )).

tff(addAssignment_270,axiom,(
    ! [VarNext: state_type] :
      ( ( v94(VarNext,279)
      <=> v834(VarNext,69) )
      & ( v94(VarNext,278)
      <=> v834(VarNext,68) )
      & ( v94(VarNext,277)
      <=> v834(VarNext,67) )
      & ( v94(VarNext,276)
      <=> v834(VarNext,66) )
      & ( v94(VarNext,275)
      <=> v834(VarNext,65) )
      & ( v94(VarNext,274)
      <=> v834(VarNext,64) )
      & ( v94(VarNext,273)
      <=> v834(VarNext,63) ) ) )).

tff(addCaseBooleanConditionShiftedRanges1_12,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v836(VarNext)
       => ( ( v834(VarNext,69)
          <=> v94(VarCurr,279) )
          & ( v834(VarNext,68)
          <=> v94(VarCurr,278) )
          & ( v834(VarNext,67)
          <=> v94(VarCurr,277) )
          & ( v834(VarNext,66)
          <=> v94(VarCurr,276) )
          & ( v834(VarNext,65)
          <=> v94(VarCurr,275) )
          & ( v834(VarNext,64)
          <=> v94(VarCurr,274) )
          & ( v834(VarNext,63)
          <=> v94(VarCurr,273) )
          & ( v834(VarNext,62)
          <=> v94(VarCurr,272) )
          & ( v834(VarNext,61)
          <=> v94(VarCurr,271) )
          & ( v834(VarNext,60)
          <=> v94(VarCurr,270) )
          & ( v834(VarNext,59)
          <=> v94(VarCurr,269) )
          & ( v834(VarNext,58)
          <=> v94(VarCurr,268) )
          & ( v834(VarNext,57)
          <=> v94(VarCurr,267) )
          & ( v834(VarNext,56)
          <=> v94(VarCurr,266) )
          & ( v834(VarNext,55)
          <=> v94(VarCurr,265) )
          & ( v834(VarNext,54)
          <=> v94(VarCurr,264) )
          & ( v834(VarNext,53)
          <=> v94(VarCurr,263) )
          & ( v834(VarNext,52)
          <=> v94(VarCurr,262) )
          & ( v834(VarNext,51)
          <=> v94(VarCurr,261) )
          & ( v834(VarNext,50)
          <=> v94(VarCurr,260) )
          & ( v834(VarNext,49)
          <=> v94(VarCurr,259) )
          & ( v834(VarNext,48)
          <=> v94(VarCurr,258) )
          & ( v834(VarNext,47)
          <=> v94(VarCurr,257) )
          & ( v834(VarNext,46)
          <=> v94(VarCurr,256) )
          & ( v834(VarNext,45)
          <=> v94(VarCurr,255) )
          & ( v834(VarNext,44)
          <=> v94(VarCurr,254) )
          & ( v834(VarNext,43)
          <=> v94(VarCurr,253) )
          & ( v834(VarNext,42)
          <=> v94(VarCurr,252) )
          & ( v834(VarNext,41)
          <=> v94(VarCurr,251) )
          & ( v834(VarNext,40)
          <=> v94(VarCurr,250) )
          & ( v834(VarNext,39)
          <=> v94(VarCurr,249) )
          & ( v834(VarNext,38)
          <=> v94(VarCurr,248) )
          & ( v834(VarNext,37)
          <=> v94(VarCurr,247) )
          & ( v834(VarNext,36)
          <=> v94(VarCurr,246) )
          & ( v834(VarNext,35)
          <=> v94(VarCurr,245) )
          & ( v834(VarNext,34)
          <=> v94(VarCurr,244) )
          & ( v834(VarNext,33)
          <=> v94(VarCurr,243) )
          & ( v834(VarNext,32)
          <=> v94(VarCurr,242) )
          & ( v834(VarNext,31)
          <=> v94(VarCurr,241) )
          & ( v834(VarNext,30)
          <=> v94(VarCurr,240) )
          & ( v834(VarNext,29)
          <=> v94(VarCurr,239) )
          & ( v834(VarNext,28)
          <=> v94(VarCurr,238) )
          & ( v834(VarNext,27)
          <=> v94(VarCurr,237) )
          & ( v834(VarNext,26)
          <=> v94(VarCurr,236) )
          & ( v834(VarNext,25)
          <=> v94(VarCurr,235) )
          & ( v834(VarNext,24)
          <=> v94(VarCurr,234) )
          & ( v834(VarNext,23)
          <=> v94(VarCurr,233) )
          & ( v834(VarNext,22)
          <=> v94(VarCurr,232) )
          & ( v834(VarNext,21)
          <=> v94(VarCurr,231) )
          & ( v834(VarNext,20)
          <=> v94(VarCurr,230) )
          & ( v834(VarNext,19)
          <=> v94(VarCurr,229) )
          & ( v834(VarNext,18)
          <=> v94(VarCurr,228) )
          & ( v834(VarNext,17)
          <=> v94(VarCurr,227) )
          & ( v834(VarNext,16)
          <=> v94(VarCurr,226) )
          & ( v834(VarNext,15)
          <=> v94(VarCurr,225) )
          & ( v834(VarNext,14)
          <=> v94(VarCurr,224) )
          & ( v834(VarNext,13)
          <=> v94(VarCurr,223) )
          & ( v834(VarNext,12)
          <=> v94(VarCurr,222) )
          & ( v834(VarNext,11)
          <=> v94(VarCurr,221) )
          & ( v834(VarNext,10)
          <=> v94(VarCurr,220) )
          & ( v834(VarNext,9)
          <=> v94(VarCurr,219) )
          & ( v834(VarNext,8)
          <=> v94(VarCurr,218) )
          & ( v834(VarNext,7)
          <=> v94(VarCurr,217) )
          & ( v834(VarNext,6)
          <=> v94(VarCurr,216) )
          & ( v834(VarNext,5)
          <=> v94(VarCurr,215) )
          & ( v834(VarNext,4)
          <=> v94(VarCurr,214) )
          & ( v834(VarNext,3)
          <=> v94(VarCurr,213) )
          & ( v834(VarNext,2)
          <=> v94(VarCurr,212) )
          & ( v834(VarNext,1)
          <=> v94(VarCurr,211) )
          & ( v834(VarNext,0)
          <=> v94(VarCurr,210) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_32,axiom,(
    ! [VarNext: state_type] :
      ( v836(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v834(VarNext,B)
          <=> v364(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_142,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v836(VarNext)
      <=> ( v837(VarNext)
          & v346(VarNext) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_141,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v837(VarNext)
      <=> ( v839(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_60,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v839(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_269,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v330(VarCurr,B)
      <=> v335(VarCurr,B) ) ) )).

tff(addAssignment_268,axiom,(
    ! [VarCurr: state_type] :
      ( ( v334(VarCurr,69)
      <=> v94(VarCurr,209) )
      & ( v334(VarCurr,68)
      <=> v94(VarCurr,208) )
      & ( v334(VarCurr,67)
      <=> v94(VarCurr,207) )
      & ( v334(VarCurr,66)
      <=> v94(VarCurr,206) )
      & ( v334(VarCurr,65)
      <=> v94(VarCurr,205) )
      & ( v334(VarCurr,64)
      <=> v94(VarCurr,204) )
      & ( v334(VarCurr,63)
      <=> v94(VarCurr,203) ) ) )).

tff(addAssignment_267,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v323(VarCurr,B)
      <=> v328(VarCurr,B) ) ) )).

tff(addAssignment_266,axiom,(
    ! [VarCurr: state_type] :
      ( ( v327(VarCurr,69)
      <=> v94(VarCurr,279) )
      & ( v327(VarCurr,68)
      <=> v94(VarCurr,278) )
      & ( v327(VarCurr,67)
      <=> v94(VarCurr,277) )
      & ( v327(VarCurr,66)
      <=> v94(VarCurr,276) )
      & ( v327(VarCurr,65)
      <=> v94(VarCurr,275) )
      & ( v327(VarCurr,64)
      <=> v94(VarCurr,274) )
      & ( v327(VarCurr,63)
      <=> v94(VarCurr,273) ) ) )).

tff(addAssignment_265,axiom,(
    ! [VarNext: state_type] :
      ( ( v94(VarNext,209)
      <=> v826(VarNext,69) )
      & ( v94(VarNext,208)
      <=> v826(VarNext,68) )
      & ( v94(VarNext,207)
      <=> v826(VarNext,67) )
      & ( v94(VarNext,206)
      <=> v826(VarNext,66) )
      & ( v94(VarNext,205)
      <=> v826(VarNext,65) )
      & ( v94(VarNext,204)
      <=> v826(VarNext,64) )
      & ( v94(VarNext,203)
      <=> v826(VarNext,63) ) ) )).

tff(addCaseBooleanConditionShiftedRanges1_11,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v828(VarNext)
       => ( ( v826(VarNext,69)
          <=> v94(VarCurr,209) )
          & ( v826(VarNext,68)
          <=> v94(VarCurr,208) )
          & ( v826(VarNext,67)
          <=> v94(VarCurr,207) )
          & ( v826(VarNext,66)
          <=> v94(VarCurr,206) )
          & ( v826(VarNext,65)
          <=> v94(VarCurr,205) )
          & ( v826(VarNext,64)
          <=> v94(VarCurr,204) )
          & ( v826(VarNext,63)
          <=> v94(VarCurr,203) )
          & ( v826(VarNext,62)
          <=> v94(VarCurr,202) )
          & ( v826(VarNext,61)
          <=> v94(VarCurr,201) )
          & ( v826(VarNext,60)
          <=> v94(VarCurr,200) )
          & ( v826(VarNext,59)
          <=> v94(VarCurr,199) )
          & ( v826(VarNext,58)
          <=> v94(VarCurr,198) )
          & ( v826(VarNext,57)
          <=> v94(VarCurr,197) )
          & ( v826(VarNext,56)
          <=> v94(VarCurr,196) )
          & ( v826(VarNext,55)
          <=> v94(VarCurr,195) )
          & ( v826(VarNext,54)
          <=> v94(VarCurr,194) )
          & ( v826(VarNext,53)
          <=> v94(VarCurr,193) )
          & ( v826(VarNext,52)
          <=> v94(VarCurr,192) )
          & ( v826(VarNext,51)
          <=> v94(VarCurr,191) )
          & ( v826(VarNext,50)
          <=> v94(VarCurr,190) )
          & ( v826(VarNext,49)
          <=> v94(VarCurr,189) )
          & ( v826(VarNext,48)
          <=> v94(VarCurr,188) )
          & ( v826(VarNext,47)
          <=> v94(VarCurr,187) )
          & ( v826(VarNext,46)
          <=> v94(VarCurr,186) )
          & ( v826(VarNext,45)
          <=> v94(VarCurr,185) )
          & ( v826(VarNext,44)
          <=> v94(VarCurr,184) )
          & ( v826(VarNext,43)
          <=> v94(VarCurr,183) )
          & ( v826(VarNext,42)
          <=> v94(VarCurr,182) )
          & ( v826(VarNext,41)
          <=> v94(VarCurr,181) )
          & ( v826(VarNext,40)
          <=> v94(VarCurr,180) )
          & ( v826(VarNext,39)
          <=> v94(VarCurr,179) )
          & ( v826(VarNext,38)
          <=> v94(VarCurr,178) )
          & ( v826(VarNext,37)
          <=> v94(VarCurr,177) )
          & ( v826(VarNext,36)
          <=> v94(VarCurr,176) )
          & ( v826(VarNext,35)
          <=> v94(VarCurr,175) )
          & ( v826(VarNext,34)
          <=> v94(VarCurr,174) )
          & ( v826(VarNext,33)
          <=> v94(VarCurr,173) )
          & ( v826(VarNext,32)
          <=> v94(VarCurr,172) )
          & ( v826(VarNext,31)
          <=> v94(VarCurr,171) )
          & ( v826(VarNext,30)
          <=> v94(VarCurr,170) )
          & ( v826(VarNext,29)
          <=> v94(VarCurr,169) )
          & ( v826(VarNext,28)
          <=> v94(VarCurr,168) )
          & ( v826(VarNext,27)
          <=> v94(VarCurr,167) )
          & ( v826(VarNext,26)
          <=> v94(VarCurr,166) )
          & ( v826(VarNext,25)
          <=> v94(VarCurr,165) )
          & ( v826(VarNext,24)
          <=> v94(VarCurr,164) )
          & ( v826(VarNext,23)
          <=> v94(VarCurr,163) )
          & ( v826(VarNext,22)
          <=> v94(VarCurr,162) )
          & ( v826(VarNext,21)
          <=> v94(VarCurr,161) )
          & ( v826(VarNext,20)
          <=> v94(VarCurr,160) )
          & ( v826(VarNext,19)
          <=> v94(VarCurr,159) )
          & ( v826(VarNext,18)
          <=> v94(VarCurr,158) )
          & ( v826(VarNext,17)
          <=> v94(VarCurr,157) )
          & ( v826(VarNext,16)
          <=> v94(VarCurr,156) )
          & ( v826(VarNext,15)
          <=> v94(VarCurr,155) )
          & ( v826(VarNext,14)
          <=> v94(VarCurr,154) )
          & ( v826(VarNext,13)
          <=> v94(VarCurr,153) )
          & ( v826(VarNext,12)
          <=> v94(VarCurr,152) )
          & ( v826(VarNext,11)
          <=> v94(VarCurr,151) )
          & ( v826(VarNext,10)
          <=> v94(VarCurr,150) )
          & ( v826(VarNext,9)
          <=> v94(VarCurr,149) )
          & ( v826(VarNext,8)
          <=> v94(VarCurr,148) )
          & ( v826(VarNext,7)
          <=> v94(VarCurr,147) )
          & ( v826(VarNext,6)
          <=> v94(VarCurr,146) )
          & ( v826(VarNext,5)
          <=> v94(VarCurr,145) )
          & ( v826(VarNext,4)
          <=> v94(VarCurr,144) )
          & ( v826(VarNext,3)
          <=> v94(VarCurr,143) )
          & ( v826(VarNext,2)
          <=> v94(VarCurr,142) )
          & ( v826(VarNext,1)
          <=> v94(VarCurr,141) )
          & ( v826(VarNext,0)
          <=> v94(VarCurr,140) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_31,axiom,(
    ! [VarNext: state_type] :
      ( v828(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v826(VarNext,B)
          <=> v318(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_140,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v828(VarNext)
      <=> ( v829(VarNext)
          & v300(VarNext) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_139,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v829(VarNext)
      <=> ( v831(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_59,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v831(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_264,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v284(VarCurr,B)
      <=> v289(VarCurr,B) ) ) )).

tff(addAssignment_263,axiom,(
    ! [VarCurr: state_type] :
      ( ( v288(VarCurr,69)
      <=> v94(VarCurr,139) )
      & ( v288(VarCurr,68)
      <=> v94(VarCurr,138) )
      & ( v288(VarCurr,67)
      <=> v94(VarCurr,137) )
      & ( v288(VarCurr,66)
      <=> v94(VarCurr,136) )
      & ( v288(VarCurr,65)
      <=> v94(VarCurr,135) )
      & ( v288(VarCurr,64)
      <=> v94(VarCurr,134) )
      & ( v288(VarCurr,63)
      <=> v94(VarCurr,133) ) ) )).

tff(addAssignment_262,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v277(VarCurr,B)
      <=> v282(VarCurr,B) ) ) )).

tff(addAssignment_261,axiom,(
    ! [VarCurr: state_type] :
      ( ( v281(VarCurr,69)
      <=> v94(VarCurr,209) )
      & ( v281(VarCurr,68)
      <=> v94(VarCurr,208) )
      & ( v281(VarCurr,67)
      <=> v94(VarCurr,207) )
      & ( v281(VarCurr,66)
      <=> v94(VarCurr,206) )
      & ( v281(VarCurr,65)
      <=> v94(VarCurr,205) )
      & ( v281(VarCurr,64)
      <=> v94(VarCurr,204) )
      & ( v281(VarCurr,63)
      <=> v94(VarCurr,203) ) ) )).

tff(addAssignment_260,axiom,(
    ! [VarNext: state_type] :
      ( ( v94(VarNext,139)
      <=> v818(VarNext,69) )
      & ( v94(VarNext,138)
      <=> v818(VarNext,68) )
      & ( v94(VarNext,137)
      <=> v818(VarNext,67) )
      & ( v94(VarNext,136)
      <=> v818(VarNext,66) )
      & ( v94(VarNext,135)
      <=> v818(VarNext,65) )
      & ( v94(VarNext,134)
      <=> v818(VarNext,64) )
      & ( v94(VarNext,133)
      <=> v818(VarNext,63) ) ) )).

tff(addCaseBooleanConditionShiftedRanges1_10,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v820(VarNext)
       => ( ( v818(VarNext,69)
          <=> v94(VarCurr,139) )
          & ( v818(VarNext,68)
          <=> v94(VarCurr,138) )
          & ( v818(VarNext,67)
          <=> v94(VarCurr,137) )
          & ( v818(VarNext,66)
          <=> v94(VarCurr,136) )
          & ( v818(VarNext,65)
          <=> v94(VarCurr,135) )
          & ( v818(VarNext,64)
          <=> v94(VarCurr,134) )
          & ( v818(VarNext,63)
          <=> v94(VarCurr,133) )
          & ( v818(VarNext,62)
          <=> v94(VarCurr,132) )
          & ( v818(VarNext,61)
          <=> v94(VarCurr,131) )
          & ( v818(VarNext,60)
          <=> v94(VarCurr,130) )
          & ( v818(VarNext,59)
          <=> v94(VarCurr,129) )
          & ( v818(VarNext,58)
          <=> v94(VarCurr,128) )
          & ( v818(VarNext,57)
          <=> v94(VarCurr,127) )
          & ( v818(VarNext,56)
          <=> v94(VarCurr,126) )
          & ( v818(VarNext,55)
          <=> v94(VarCurr,125) )
          & ( v818(VarNext,54)
          <=> v94(VarCurr,124) )
          & ( v818(VarNext,53)
          <=> v94(VarCurr,123) )
          & ( v818(VarNext,52)
          <=> v94(VarCurr,122) )
          & ( v818(VarNext,51)
          <=> v94(VarCurr,121) )
          & ( v818(VarNext,50)
          <=> v94(VarCurr,120) )
          & ( v818(VarNext,49)
          <=> v94(VarCurr,119) )
          & ( v818(VarNext,48)
          <=> v94(VarCurr,118) )
          & ( v818(VarNext,47)
          <=> v94(VarCurr,117) )
          & ( v818(VarNext,46)
          <=> v94(VarCurr,116) )
          & ( v818(VarNext,45)
          <=> v94(VarCurr,115) )
          & ( v818(VarNext,44)
          <=> v94(VarCurr,114) )
          & ( v818(VarNext,43)
          <=> v94(VarCurr,113) )
          & ( v818(VarNext,42)
          <=> v94(VarCurr,112) )
          & ( v818(VarNext,41)
          <=> v94(VarCurr,111) )
          & ( v818(VarNext,40)
          <=> v94(VarCurr,110) )
          & ( v818(VarNext,39)
          <=> v94(VarCurr,109) )
          & ( v818(VarNext,38)
          <=> v94(VarCurr,108) )
          & ( v818(VarNext,37)
          <=> v94(VarCurr,107) )
          & ( v818(VarNext,36)
          <=> v94(VarCurr,106) )
          & ( v818(VarNext,35)
          <=> v94(VarCurr,105) )
          & ( v818(VarNext,34)
          <=> v94(VarCurr,104) )
          & ( v818(VarNext,33)
          <=> v94(VarCurr,103) )
          & ( v818(VarNext,32)
          <=> v94(VarCurr,102) )
          & ( v818(VarNext,31)
          <=> v94(VarCurr,101) )
          & ( v818(VarNext,30)
          <=> v94(VarCurr,100) )
          & ( v818(VarNext,29)
          <=> v94(VarCurr,99) )
          & ( v818(VarNext,28)
          <=> v94(VarCurr,98) )
          & ( v818(VarNext,27)
          <=> v94(VarCurr,97) )
          & ( v818(VarNext,26)
          <=> v94(VarCurr,96) )
          & ( v818(VarNext,25)
          <=> v94(VarCurr,95) )
          & ( v818(VarNext,24)
          <=> v94(VarCurr,94) )
          & ( v818(VarNext,23)
          <=> v94(VarCurr,93) )
          & ( v818(VarNext,22)
          <=> v94(VarCurr,92) )
          & ( v818(VarNext,21)
          <=> v94(VarCurr,91) )
          & ( v818(VarNext,20)
          <=> v94(VarCurr,90) )
          & ( v818(VarNext,19)
          <=> v94(VarCurr,89) )
          & ( v818(VarNext,18)
          <=> v94(VarCurr,88) )
          & ( v818(VarNext,17)
          <=> v94(VarCurr,87) )
          & ( v818(VarNext,16)
          <=> v94(VarCurr,86) )
          & ( v818(VarNext,15)
          <=> v94(VarCurr,85) )
          & ( v818(VarNext,14)
          <=> v94(VarCurr,84) )
          & ( v818(VarNext,13)
          <=> v94(VarCurr,83) )
          & ( v818(VarNext,12)
          <=> v94(VarCurr,82) )
          & ( v818(VarNext,11)
          <=> v94(VarCurr,81) )
          & ( v818(VarNext,10)
          <=> v94(VarCurr,80) )
          & ( v818(VarNext,9)
          <=> v94(VarCurr,79) )
          & ( v818(VarNext,8)
          <=> v94(VarCurr,78) )
          & ( v818(VarNext,7)
          <=> v94(VarCurr,77) )
          & ( v818(VarNext,6)
          <=> v94(VarCurr,76) )
          & ( v818(VarNext,5)
          <=> v94(VarCurr,75) )
          & ( v818(VarNext,4)
          <=> v94(VarCurr,74) )
          & ( v818(VarNext,3)
          <=> v94(VarCurr,73) )
          & ( v818(VarNext,2)
          <=> v94(VarCurr,72) )
          & ( v818(VarNext,1)
          <=> v94(VarCurr,71) )
          & ( v818(VarNext,0)
          <=> v94(VarCurr,70) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_30,axiom,(
    ! [VarNext: state_type] :
      ( v820(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v818(VarNext,B)
          <=> v272(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_138,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v820(VarNext)
      <=> ( v821(VarNext)
          & v253(VarNext) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_137,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v821(VarNext)
      <=> ( v823(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_58,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v823(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_259,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v237(VarCurr,B)
      <=> v242(VarCurr,B) ) ) )).

tff(addAssignment_258,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v241(VarCurr,B)
      <=> v94(VarCurr,B) ) ) )).

tff(addAssignment_257,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v99(VarCurr,B)
      <=> v235(VarCurr,B) ) ) )).

tff(addAssignment_256,axiom,(
    ! [VarCurr: state_type] :
      ( ( v218(VarCurr,69)
      <=> v94(VarCurr,139) )
      & ( v218(VarCurr,68)
      <=> v94(VarCurr,138) )
      & ( v218(VarCurr,67)
      <=> v94(VarCurr,137) )
      & ( v218(VarCurr,66)
      <=> v94(VarCurr,136) )
      & ( v218(VarCurr,65)
      <=> v94(VarCurr,135) )
      & ( v218(VarCurr,64)
      <=> v94(VarCurr,134) )
      & ( v218(VarCurr,63)
      <=> v94(VarCurr,133) ) ) )).

tff(addAssignment_255,axiom,(
    ! [VarNext: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v94(VarNext,B)
      <=> v786(VarNext,B) ) ) )).

tff(addCaseBooleanConditionEqualRanges1_19,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v787(VarNext)
       => ! [B: $int] :
            ( ( $less(B,70)
              & ~ $less(B,0) )
           => ( v786(VarNext,B)
            <=> v94(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_29,axiom,(
    ! [VarNext: state_type] :
      ( v787(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v786(VarNext,B)
          <=> v813(VarNext,B) ) ) ) )).

tff(addAssignment_254,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v813(VarNext,B)
          <=> v811(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_17,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v811(VarCurr,B)
          <=> v814(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_11,axiom,(
    ! [VarCurr: state_type] :
      ( v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v811(VarCurr,B)
          <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_9,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v800(VarCurr)
        & ~ v802(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v814(VarCurr,B)
          <=> v779(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_16,axiom,(
    ! [VarCurr: state_type] :
      ( v802(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v814(VarCurr,B)
          <=> v772(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_10,axiom,(
    ! [VarCurr: state_type] :
      ( v800(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v814(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_136,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v787(VarNext)
      <=> ( v788(VarNext)
          & v795(VarNext) ) ) ) )).

tff(addAssignment_253,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v795(VarNext)
      <=> v793(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_135,axiom,(
    ! [VarCurr: state_type] :
      ( v793(VarCurr)
    <=> ( v796(VarCurr)
        & v807(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_134,axiom,(
    ! [VarCurr: state_type] :
      ( v807(VarCurr)
    <=> ( v808(VarCurr)
        | v255(VarCurr) ) ) )).

tff(writeUnaryOperator_57,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v808(VarCurr)
    <=> v809(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_50,axiom,(
    ! [VarCurr: state_type] :
      ( v809(VarCurr)
    <=> ( ( v810(VarCurr,1)
        <=> $false )
        & ( v810(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_252,axiom,(
    ! [VarCurr: state_type] :
      ( v810(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_251,axiom,(
    ! [VarCurr: state_type] :
      ( v810(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_133,axiom,(
    ! [VarCurr: state_type] :
      ( v796(VarCurr)
    <=> ( v255(VarCurr)
        | v797(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_132,axiom,(
    ! [VarCurr: state_type] :
      ( v797(VarCurr)
    <=> ( v798(VarCurr)
        & v806(VarCurr) ) ) )).

tff(writeUnaryOperator_56,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v806(VarCurr)
    <=> v255(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_131,axiom,(
    ! [VarCurr: state_type] :
      ( v798(VarCurr)
    <=> ( v799(VarCurr)
        | v804(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_49,axiom,(
    ! [VarCurr: state_type] :
      ( v804(VarCurr)
    <=> ( ( v805(VarCurr,1)
        <=> $true )
        & ( v805(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_250,axiom,(
    ! [VarCurr: state_type] :
      ( v805(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_249,axiom,(
    ! [VarCurr: state_type] :
      ( v805(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_130,axiom,(
    ! [VarCurr: state_type] :
      ( v799(VarCurr)
    <=> ( v800(VarCurr)
        | v802(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_48,axiom,(
    ! [VarCurr: state_type] :
      ( v802(VarCurr)
    <=> ( ( v803(VarCurr,1)
        <=> $true )
        & ( v803(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_248,axiom,(
    ! [VarCurr: state_type] :
      ( v803(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_247,axiom,(
    ! [VarCurr: state_type] :
      ( v803(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_47,axiom,(
    ! [VarCurr: state_type] :
      ( v800(VarCurr)
    <=> ( ( v801(VarCurr,1)
        <=> $false )
        & ( v801(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_246,axiom,(
    ! [VarCurr: state_type] :
      ( v801(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_245,axiom,(
    ! [VarCurr: state_type] :
      ( v801(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_129,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v788(VarNext)
      <=> ( v790(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_55,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v790(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_244,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v779(VarCurr,B)
      <=> v784(VarCurr,B) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_27,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v781(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v784(VarCurr,B)
          <=> v783(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_28,axiom,(
    ! [VarCurr: state_type] :
      ( v781(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v784(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_243,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v783(VarCurr,B)
      <=> v94(VarCurr,B) ) ) )).

tff(addAssignment_242,axiom,(
    ! [VarCurr: state_type] :
      ( v781(VarCurr)
    <=> v103(VarCurr,8) ) )).

tff(addAssignment_241,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v772(VarCurr,B)
      <=> v777(VarCurr,B) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_26,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v774(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v777(VarCurr,B)
          <=> v776(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_27,axiom,(
    ! [VarCurr: state_type] :
      ( v774(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v777(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_240,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v776(VarCurr,B)
      <=> v94(VarCurr,B) ) ) )).

tff(addAssignment_239,axiom,(
    ! [VarCurr: state_type] :
      ( v774(VarCurr)
    <=> v103(VarCurr,8) ) )).

tff(addAssignment_238,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v212(VarCurr,B)
      <=> v214(VarCurr,B) ) ) )).

tff(addAssignment_237,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,70)
        & ~ $less(B,63) )
     => ( v214(VarCurr,B)
      <=> v216(VarCurr,B) ) ) )).

tff(addAssignment_236,axiom,(
    ! [VarCurr: state_type] :
      ( v30(VarCurr)
    <=> v32(VarCurr) ) )).

tff(addCaseBooleanConditionEqualRanges1_18,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v747(VarNext)
       => ( v32(VarNext)
        <=> v32(VarCurr) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_28,axiom,(
    ! [VarNext: state_type] :
      ( v747(VarNext)
     => ( v32(VarNext)
      <=> v765(VarNext) ) ) )).

tff(addAssignment_235,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v765(VarNext)
      <=> v763(VarCurr) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_25,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v762(VarCurr)
     => ( v763(VarCurr)
      <=> v766(VarCurr) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_26,axiom,(
    ! [VarCurr: state_type] :
      ( v762(VarCurr)
     => ( v763(VarCurr)
      <=> $true ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_24,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v36(VarCurr)
     => ( v766(VarCurr)
      <=> $true ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_25,axiom,(
    ! [VarCurr: state_type] :
      ( v36(VarCurr)
     => ( v766(VarCurr)
      <=> $false ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_128,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v747(VarNext)
      <=> ( v748(VarNext)
          & v755(VarNext) ) ) ) )).

tff(addAssignment_234,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v755(VarNext)
      <=> v753(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_127,axiom,(
    ! [VarCurr: state_type] :
      ( v753(VarCurr)
    <=> ( v756(VarCurr)
        | v762(VarCurr) ) ) )).

tff(writeUnaryOperator_54,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v762(VarCurr)
    <=> v34(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_126,axiom,(
    ! [VarCurr: state_type] :
      ( v756(VarCurr)
    <=> ( v757(VarCurr)
        | v36(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_125,axiom,(
    ! [VarCurr: state_type] :
      ( v757(VarCurr)
    <=> ( v758(VarCurr)
        & v761(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_46,axiom,(
    ! [VarCurr: state_type] :
      ( v761(VarCurr)
    <=> ( v105(VarCurr,0)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_124,axiom,(
    ! [VarCurr: state_type] :
      ( v758(VarCurr)
    <=> ( v759(VarCurr)
        & v760(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_45,axiom,(
    ! [VarCurr: state_type] :
      ( v760(VarCurr)
    <=> ( v652(VarCurr,1)
      <=> $false ) ) )).

tff(addBitVectorEqualityBitBlasted_44,axiom,(
    ! [VarCurr: state_type] :
      ( v759(VarCurr)
    <=> ( v43(VarCurr)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_123,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v748(VarNext)
      <=> ( v749(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_53,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v749(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_233,axiom,(
    ! [VarCurr: state_type] :
      ( v105(VarCurr,0)
    <=> v129(VarCurr,0) ) )).

tff(addAssignment_232,axiom,(
    ! [VarNext: state_type] :
      ( v652(VarNext,1)
    <=> v738(VarNext,1) ) )).

tff(addCaseBooleanConditionEqualRanges1_17,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v739(VarNext)
       => ! [B: $int] :
            ( ( $less(B,8)
              & ~ $less(B,0) )
           => ( v738(VarNext,B)
            <=> v652(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_27,axiom,(
    ! [VarNext: state_type] :
      ( v739(VarNext)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v738(VarNext,B)
          <=> v686(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_122,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v739(VarNext)
      <=> v740(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_121,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v740(VarNext)
      <=> ( v742(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_52,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v742(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_231,axiom,(
    ! [VarCurr: state_type] :
      ( v655(VarCurr,1)
    <=> v662(VarCurr,1) ) )).

tff(addAssignment_230,axiom,(
    ! [VarCurr: state_type] :
      ( v657(VarCurr,1)
    <=> v658(VarCurr,1) ) )).

tff(addAssignment_229,axiom,(
    ! [VarNext: state_type] :
      ( v652(VarNext,0)
    <=> v730(VarNext,0) ) )).

tff(addCaseBooleanConditionEqualRanges1_16,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v731(VarNext)
       => ! [B: $int] :
            ( ( $less(B,8)
              & ~ $less(B,0) )
           => ( v730(VarNext,B)
            <=> v652(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_26,axiom,(
    ! [VarNext: state_type] :
      ( v731(VarNext)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v730(VarNext,B)
          <=> v686(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_120,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v731(VarNext)
      <=> v732(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_119,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v732(VarNext)
      <=> ( v734(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_51,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v734(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_228,axiom,(
    ! [VarCurr: state_type] :
      ( v655(VarCurr,0)
    <=> v662(VarCurr,0) ) )).

tff(addAssignment_227,axiom,(
    ! [VarNext: state_type] :
      ( v652(VarNext,2)
    <=> v722(VarNext,2) ) )).

tff(addCaseBooleanConditionEqualRanges1_15,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v723(VarNext)
       => ! [B: $int] :
            ( ( $less(B,8)
              & ~ $less(B,0) )
           => ( v722(VarNext,B)
            <=> v652(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_25,axiom,(
    ! [VarNext: state_type] :
      ( v723(VarNext)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v722(VarNext,B)
          <=> v686(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_118,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v723(VarNext)
      <=> v724(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_117,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v724(VarNext)
      <=> ( v726(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_50,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v726(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_226,axiom,(
    ! [VarCurr: state_type] :
      ( v655(VarCurr,2)
    <=> v662(VarCurr,2) ) )).

tff(addAssignment_225,axiom,(
    ! [VarCurr: state_type] :
      ( v657(VarCurr,2)
    <=> v658(VarCurr,2) ) )).

tff(addAssignment_224,axiom,(
    ! [VarNext: state_type] :
      ( v652(VarNext,3)
    <=> v714(VarNext,3) ) )).

tff(addCaseBooleanConditionEqualRanges1_14,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v715(VarNext)
       => ! [B: $int] :
            ( ( $less(B,8)
              & ~ $less(B,0) )
           => ( v714(VarNext,B)
            <=> v652(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_24,axiom,(
    ! [VarNext: state_type] :
      ( v715(VarNext)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v714(VarNext,B)
          <=> v686(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_116,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v715(VarNext)
      <=> v716(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_115,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v716(VarNext)
      <=> ( v718(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_49,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v718(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_223,axiom,(
    ! [VarCurr: state_type] :
      ( v655(VarCurr,3)
    <=> v662(VarCurr,3) ) )).

tff(addAssignment_222,axiom,(
    ! [VarCurr: state_type] :
      ( v657(VarCurr,3)
    <=> v658(VarCurr,3) ) )).

tff(addAssignment_221,axiom,(
    ! [VarNext: state_type] :
      ( v652(VarNext,4)
    <=> v706(VarNext,4) ) )).

tff(addCaseBooleanConditionEqualRanges1_13,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v707(VarNext)
       => ! [B: $int] :
            ( ( $less(B,8)
              & ~ $less(B,0) )
           => ( v706(VarNext,B)
            <=> v652(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_23,axiom,(
    ! [VarNext: state_type] :
      ( v707(VarNext)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v706(VarNext,B)
          <=> v686(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_114,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v707(VarNext)
      <=> v708(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_113,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v708(VarNext)
      <=> ( v710(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_48,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v710(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_220,axiom,(
    ! [VarCurr: state_type] :
      ( v655(VarCurr,4)
    <=> v662(VarCurr,4) ) )).

tff(addAssignment_219,axiom,(
    ! [VarCurr: state_type] :
      ( v657(VarCurr,4)
    <=> v658(VarCurr,4) ) )).

tff(addAssignment_218,axiom,(
    ! [VarNext: state_type] :
      ( v652(VarNext,5)
    <=> v698(VarNext,5) ) )).

tff(addCaseBooleanConditionEqualRanges1_12,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v699(VarNext)
       => ! [B: $int] :
            ( ( $less(B,8)
              & ~ $less(B,0) )
           => ( v698(VarNext,B)
            <=> v652(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_22,axiom,(
    ! [VarNext: state_type] :
      ( v699(VarNext)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v698(VarNext,B)
          <=> v686(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_112,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v699(VarNext)
      <=> v700(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_111,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v700(VarNext)
      <=> ( v702(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_47,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v702(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_217,axiom,(
    ! [VarCurr: state_type] :
      ( v655(VarCurr,5)
    <=> v662(VarCurr,5) ) )).

tff(addAssignment_216,axiom,(
    ! [VarCurr: state_type] :
      ( v657(VarCurr,5)
    <=> v658(VarCurr,5) ) )).

tff(addAssignment_215,axiom,(
    ! [VarNext: state_type] :
      ( v652(VarNext,6)
    <=> v690(VarNext,6) ) )).

tff(addCaseBooleanConditionEqualRanges1_11,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v691(VarNext)
       => ! [B: $int] :
            ( ( $less(B,8)
              & ~ $less(B,0) )
           => ( v690(VarNext,B)
            <=> v652(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_21,axiom,(
    ! [VarNext: state_type] :
      ( v691(VarNext)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v690(VarNext,B)
          <=> v686(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_110,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v691(VarNext)
      <=> v692(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_109,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v692(VarNext)
      <=> ( v694(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_46,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v694(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_214,axiom,(
    ! [VarCurr: state_type] :
      ( v655(VarCurr,6)
    <=> v662(VarCurr,6) ) )).

tff(addAssignment_213,axiom,(
    ! [VarCurr: state_type] :
      ( v657(VarCurr,6)
    <=> v658(VarCurr,6) ) )).

tff(addAssignment_212,axiom,(
    ! [VarNext: state_type] :
      ( v652(VarNext,7)
    <=> v677(VarNext,7) ) )).

tff(addCaseBooleanConditionEqualRanges1_10,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v678(VarNext)
       => ! [B: $int] :
            ( ( $less(B,8)
              & ~ $less(B,0) )
           => ( v677(VarNext,B)
            <=> v652(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_20,axiom,(
    ! [VarNext: state_type] :
      ( v678(VarNext)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v677(VarNext,B)
          <=> v686(VarNext,B) ) ) ) )).

tff(addAssignment_211,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v686(VarNext,B)
          <=> v684(VarCurr,B) ) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_23,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v126(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v684(VarCurr,B)
          <=> v655(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_24,axiom,(
    ! [VarCurr: state_type] :
      ( v126(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v684(VarCurr,B)
          <=> $false ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_108,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v678(VarNext)
      <=> v679(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_107,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v679(VarNext)
      <=> ( v681(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_45,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v681(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_210,axiom,(
    ! [VarCurr: state_type] :
      ( v655(VarCurr,7)
    <=> v662(VarCurr,7) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_22,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v663(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v662(VarCurr,B)
          <=> v664(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_23,axiom,(
    ! [VarCurr: state_type] :
      ( v663(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v662(VarCurr,B)
          <=> $false ) ) ) )).

tff(bitBlastConstant_170,axiom,(
    ~ b00000000(7) )).

tff(bitBlastConstant_169,axiom,(
    ~ b00000000(6) )).

tff(bitBlastConstant_168,axiom,(
    ~ b00000000(5) )).

tff(bitBlastConstant_167,axiom,(
    ~ b00000000(4) )).

tff(bitBlastConstant_166,axiom,(
    ~ b00000000(3) )).

tff(bitBlastConstant_165,axiom,(
    ~ b00000000(2) )).

tff(bitBlastConstant_164,axiom,(
    ~ b00000000(1) )).

tff(bitBlastConstant_163,axiom,(
    ~ b00000000(0) )).

tff(addParallelCaseBooleanConditionEqualRanges3_1,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v665(VarCurr)
        & ~ v667(VarCurr)
        & ~ v671(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v664(VarCurr,B)
          <=> v652(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_8,axiom,(
    ! [VarCurr: state_type] :
      ( v671(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v664(VarCurr,B)
          <=> v673(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_15,axiom,(
    ! [VarCurr: state_type] :
      ( v667(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v664(VarCurr,B)
          <=> v669(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_9,axiom,(
    ! [VarCurr: state_type] :
      ( v665(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,8)
            & ~ $less(B,0) )
         => ( v664(VarCurr,B)
          <=> v652(VarCurr,B) ) ) ) )).

tff(addBitVectorEqualityBitBlasted_43,axiom,(
    ! [VarCurr: state_type] :
      ( v674(VarCurr)
    <=> ( ( v675(VarCurr,1)
        <=> $true )
        & ( v675(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_209,axiom,(
    ! [VarCurr: state_type] :
      ( v675(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_208,axiom,(
    ! [VarCurr: state_type] :
      ( v675(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addAssignment_207,axiom,(
    ! [VarCurr: state_type] :
      ( v673(VarCurr,0)
    <=> $true ) )).

tff(addAssignment_206,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,8)
        & ~ $less(B,1) )
     => ( v673(VarCurr,B)
      <=> v657(VarCurr,B) ) ) )).

tff(addBitVectorEqualityBitBlasted_42,axiom,(
    ! [VarCurr: state_type] :
      ( v671(VarCurr)
    <=> ( ( v672(VarCurr,1)
        <=> $true )
        & ( v672(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_205,axiom,(
    ! [VarCurr: state_type] :
      ( v672(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_204,axiom,(
    ! [VarCurr: state_type] :
      ( v672(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addAssignment_203,axiom,(
    ! [VarCurr: state_type] :
      ( ( v669(VarCurr,6)
      <=> v652(VarCurr,7) )
      & ( v669(VarCurr,5)
      <=> v652(VarCurr,6) )
      & ( v669(VarCurr,4)
      <=> v652(VarCurr,5) )
      & ( v669(VarCurr,3)
      <=> v652(VarCurr,4) )
      & ( v669(VarCurr,2)
      <=> v652(VarCurr,3) )
      & ( v669(VarCurr,1)
      <=> v652(VarCurr,2) )
      & ( v669(VarCurr,0)
      <=> v652(VarCurr,1) ) ) )).

tff(addAssignment_202,axiom,(
    ! [VarCurr: state_type] :
      ( v669(VarCurr,7)
    <=> $false ) )).

tff(addBitVectorEqualityBitBlasted_41,axiom,(
    ! [VarCurr: state_type] :
      ( v667(VarCurr)
    <=> ( ( v668(VarCurr,1)
        <=> $false )
        & ( v668(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_201,axiom,(
    ! [VarCurr: state_type] :
      ( v668(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_200,axiom,(
    ! [VarCurr: state_type] :
      ( v668(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_40,axiom,(
    ! [VarCurr: state_type] :
      ( v665(VarCurr)
    <=> ( ( v666(VarCurr,1)
        <=> $false )
        & ( v666(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_199,axiom,(
    ! [VarCurr: state_type] :
      ( v666(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_198,axiom,(
    ! [VarCurr: state_type] :
      ( v666(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeUnaryOperator_44,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v663(VarCurr)
    <=> v34(VarCurr) ) )).

tff(addAssignment_197,axiom,(
    ! [VarCurr: state_type] :
      ( v657(VarCurr,7)
    <=> v658(VarCurr,7) ) )).

tff(addAssignment_196,axiom,(
    ! [VarCurr: state_type] :
      ( v658(VarCurr,0)
    <=> $false ) )).

tff(addAssignment_195,axiom,(
    ! [VarCurr: state_type] :
      ( ( v658(VarCurr,7)
      <=> v652(VarCurr,6) )
      & ( v658(VarCurr,6)
      <=> v652(VarCurr,5) )
      & ( v658(VarCurr,5)
      <=> v652(VarCurr,4) )
      & ( v658(VarCurr,4)
      <=> v652(VarCurr,3) )
      & ( v658(VarCurr,3)
      <=> v652(VarCurr,2) )
      & ( v658(VarCurr,2)
      <=> v652(VarCurr,1) )
      & ( v658(VarCurr,1)
      <=> v652(VarCurr,0) ) ) )).

tff(addAssignmentInitValue_81,axiom,(
    ~ v652(constB0,6) )).

tff(addAssignmentInitValue_80,axiom,(
    ~ v652(constB0,5) )).

tff(addAssignmentInitValue_79,axiom,(
    ~ v652(constB0,4) )).

tff(addAssignmentInitValue_78,axiom,(
    ~ v652(constB0,3) )).

tff(addAssignmentInitValue_77,axiom,(
    ~ v652(constB0,2) )).

tff(addAssignmentInitValue_76,axiom,(
    ~ v652(constB0,1) )).

tff(addAssignmentInitValue_75,axiom,(
    ~ v652(constB0,0) )).

tff(bitBlastConstant_162,axiom,(
    ~ bx0000000(6) )).

tff(bitBlastConstant_161,axiom,(
    ~ bx0000000(5) )).

tff(bitBlastConstant_160,axiom,(
    ~ bx0000000(4) )).

tff(bitBlastConstant_159,axiom,(
    ~ bx0000000(3) )).

tff(bitBlastConstant_158,axiom,(
    ~ bx0000000(2) )).

tff(bitBlastConstant_157,axiom,(
    ~ bx0000000(1) )).

tff(bitBlastConstant_156,axiom,(
    ~ bx0000000(0) )).

tff(addAssignment_194,axiom,(
    ! [VarCurr: state_type] :
      ( v43(VarCurr)
    <=> v45(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_106,axiom,(
    ! [VarCurr: state_type] :
      ( v45(VarCurr)
    <=> ( v650(VarCurr)
        | v644(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_105,axiom,(
    ! [VarCurr: state_type] :
      ( v650(VarCurr)
    <=> ( v47(VarCurr)
        | v56(VarCurr) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_21,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v646(VarCurr)
     => ( v644(VarCurr)
      <=> $false ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_22,axiom,(
    ! [VarCurr: state_type] :
      ( v646(VarCurr)
     => ( v644(VarCurr)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_104,axiom,(
    ! [VarCurr: state_type] :
      ( v646(VarCurr)
    <=> ( v647(VarCurr)
        & v54(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_103,axiom,(
    ! [VarCurr: state_type] :
      ( v647(VarCurr)
    <=> ( v648(VarCurr)
        & v53(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_39,axiom,(
    ! [VarCurr: state_type] :
      ( v648(VarCurr)
    <=> ( ( v28(VarCurr,1)
        <=> $false )
        & ( v28(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_193,axiom,(
    ! [VarCurr: state_type] :
      ( v56(VarCurr)
    <=> v58(VarCurr) ) )).

tff(addAssignment_192,axiom,(
    ! [VarCurr: state_type] :
      ( v58(VarCurr)
    <=> v60(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_102,axiom,(
    ! [VarCurr: state_type] :
      ( v60(VarCurr)
    <=> ( v642(VarCurr)
        & v554(VarCurr) ) ) )).

tff(writeBinaryOperatorShiftedRanges,axiom,(
    ! [VarCurr: state_type] :
      ( v642(VarCurr)
    <=> ( v62(VarCurr,2)
        | v62(VarCurr,1) ) ) )).

tff(addAssignment_191,axiom,(
    ! [VarNext: state_type] :
      ( v62(VarNext,2)
    <=> v634(VarNext,1) ) )).

tff(addCaseBooleanConditionShiftedRanges1_9,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v635(VarNext)
       => ( ( v634(VarNext,2)
          <=> v62(VarCurr,3) )
          & ( v634(VarNext,1)
          <=> v62(VarCurr,2) )
          & ( v634(VarNext,0)
          <=> v62(VarCurr,1) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_19,axiom,(
    ! [VarNext: state_type] :
      ( v635(VarNext)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v634(VarNext,B)
          <=> v578(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_101,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v635(VarNext)
      <=> v636(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_100,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v636(VarNext)
      <=> ( v638(VarNext)
          & v565(VarNext) ) ) ) )).

tff(writeUnaryOperator_43,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v638(VarNext)
      <=> v572(VarNext) ) ) )).

tff(addConditionBooleanCondShiftedRangesElseBranch_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v626(VarCurr)
     => ( v67(VarCurr,2)
      <=> $false ) ) )).

tff(addConditionBooleanCondShiftedRangesThenBranch_2,axiom,(
    ! [VarCurr: state_type] :
      ( v626(VarCurr)
     => ( v67(VarCurr,2)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_99,axiom,(
    ! [VarCurr: state_type] :
      ( v626(VarCurr)
    <=> ( v627(VarCurr)
        | v631(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_98,axiom,(
    ! [VarCurr: state_type] :
      ( v631(VarCurr)
    <=> ( v632(VarCurr)
        & v560(VarCurr) ) ) )).

tff(writeUnaryOperator_42,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v632(VarCurr)
    <=> v554(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_97,axiom,(
    ! [VarCurr: state_type] :
      ( v627(VarCurr)
    <=> ( v628(VarCurr)
        & v587(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_96,axiom,(
    ! [VarCurr: state_type] :
      ( v628(VarCurr)
    <=> ( v69(VarCurr)
        & v630(VarCurr) ) ) )).

tff(writeUnaryOperator_41,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v630(VarCurr)
    <=> v82(VarCurr) ) )).

tff(addAssignment_190,axiom,(
    ! [VarCurr: state_type] :
      ( v69(VarCurr)
    <=> v71(VarCurr) ) )).

tff(addAssignment_189,axiom,(
    ! [VarCurr: state_type] :
      ( v71(VarCurr)
    <=> v73(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_20,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v621(VarCurr)
     => ( v73(VarCurr)
      <=> $false ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_21,axiom,(
    ! [VarCurr: state_type] :
      ( v621(VarCurr)
     => ( v73(VarCurr)
      <=> v624(VarCurr) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_19,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v76(VarCurr)
     => ( v624(VarCurr)
      <=> $false ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_20,axiom,(
    ! [VarCurr: state_type] :
      ( v76(VarCurr)
     => ( v624(VarCurr)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_95,axiom,(
    ! [VarCurr: state_type] :
      ( v621(VarCurr)
    <=> ( v622(VarCurr)
        & v54(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_94,axiom,(
    ! [VarCurr: state_type] :
      ( v622(VarCurr)
    <=> ( v623(VarCurr)
        & v53(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_38,axiom,(
    ! [VarCurr: state_type] :
      ( v623(VarCurr)
    <=> ( ( v28(VarCurr,1)
        <=> $true )
        & ( v28(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_188,axiom,(
    ! [VarCurr: state_type] :
      ( v76(VarCurr)
    <=> v78(VarCurr) ) )).

tff(addAssignment_187,axiom,(
    ! [VarCurr: state_type] :
      ( v78(VarCurr)
    <=> v80(VarCurr) ) )).

tff(addAssignment_186,axiom,(
    ! [VarCurr: state_type] :
      ( v80(VarCurr)
    <=> v62(VarCurr,0) ) )).

tff(addCaseBooleanConditionEqualRanges1_9,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v609(VarNext)
       => ( v62(VarNext,0)
        <=> v62(VarCurr,0) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_18,axiom,(
    ! [VarNext: state_type] :
      ( v609(VarNext)
     => ( v62(VarNext,0)
      <=> v617(VarNext) ) ) )).

tff(addAssignment_185,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v617(VarNext)
      <=> v615(VarCurr) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_18,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v579(VarCurr)
     => ( v615(VarCurr)
      <=> v67(VarCurr,0) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_19,axiom,(
    ! [VarCurr: state_type] :
      ( v579(VarCurr)
     => ( v615(VarCurr)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_93,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v609(VarNext)
      <=> v610(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_92,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v610(VarNext)
      <=> ( v612(VarNext)
          & v565(VarNext) ) ) ) )).

tff(writeUnaryOperator_40,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v612(VarNext)
      <=> v572(VarNext) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_17,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v600(VarCurr)
     => ( v67(VarCurr,0)
      <=> $false ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_18,axiom,(
    ! [VarCurr: state_type] :
      ( v600(VarCurr)
     => ( v67(VarCurr,0)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_91,axiom,(
    ! [VarCurr: state_type] :
      ( v600(VarCurr)
    <=> ( v601(VarCurr)
        | v606(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_90,axiom,(
    ! [VarCurr: state_type] :
      ( v606(VarCurr)
    <=> ( v554(VarCurr)
        & v590(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_89,axiom,(
    ! [VarCurr: state_type] :
      ( v601(VarCurr)
    <=> ( v602(VarCurr)
        | v605(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_88,axiom,(
    ! [VarCurr: state_type] :
      ( v605(VarCurr)
    <=> ( v554(VarCurr)
        & v563(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_87,axiom,(
    ! [VarCurr: state_type] :
      ( v602(VarCurr)
    <=> ( v603(VarCurr)
        & v587(VarCurr) ) ) )).

tff(writeUnaryOperator_39,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v603(VarCurr)
    <=> v69(VarCurr) ) )).

tff(addAssignment_184,axiom,(
    ! [VarNext: state_type] :
      ( v62(VarNext,1)
    <=> v592(VarNext,0) ) )).

tff(addCaseBooleanConditionShiftedRanges1_8,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v593(VarNext)
       => ( ( v592(VarNext,2)
          <=> v62(VarCurr,3) )
          & ( v592(VarNext,1)
          <=> v62(VarCurr,2) )
          & ( v592(VarNext,0)
          <=> v62(VarCurr,1) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_17,axiom,(
    ! [VarNext: state_type] :
      ( v593(VarNext)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v592(VarNext,B)
          <=> v578(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_86,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v593(VarNext)
      <=> v594(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_85,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v594(VarNext)
      <=> ( v596(VarNext)
          & v565(VarNext) ) ) ) )).

tff(writeUnaryOperator_38,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v596(VarNext)
      <=> v572(VarNext) ) ) )).

tff(addConditionBooleanCondShiftedRangesElseBranch_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v583(VarCurr)
     => ( v67(VarCurr,1)
      <=> $false ) ) )).

tff(addConditionBooleanCondShiftedRangesThenBranch_1,axiom,(
    ! [VarCurr: state_type] :
      ( v583(VarCurr)
     => ( v67(VarCurr,1)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_84,axiom,(
    ! [VarCurr: state_type] :
      ( v583(VarCurr)
    <=> ( v584(VarCurr)
        | v588(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_83,axiom,(
    ! [VarCurr: state_type] :
      ( v588(VarCurr)
    <=> ( v589(VarCurr)
        & v590(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_37,axiom,(
    ! [VarCurr: state_type] :
      ( v590(VarCurr)
    <=> ( $true
      <=> v62(VarCurr,1) ) ) )).

tff(writeUnaryOperator_37,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v589(VarCurr)
    <=> v554(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_82,axiom,(
    ! [VarCurr: state_type] :
      ( v584(VarCurr)
    <=> ( v585(VarCurr)
        & v587(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_36,axiom,(
    ! [VarCurr: state_type] :
      ( v587(VarCurr)
    <=> ( $true
      <=> v62(VarCurr,0) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_81,axiom,(
    ! [VarCurr: state_type] :
      ( v585(VarCurr)
    <=> ( v69(VarCurr)
        & v82(VarCurr) ) ) )).

tff(addAssignment_183,axiom,(
    ! [VarNext: state_type] :
      ( v62(VarNext,3)
    <=> v567(VarNext,2) ) )).

tff(addCaseBooleanConditionShiftedRanges1_7,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v568(VarNext)
       => ( ( v567(VarNext,2)
          <=> v62(VarCurr,3) )
          & ( v567(VarNext,1)
          <=> v62(VarCurr,2) )
          & ( v567(VarNext,0)
          <=> v62(VarCurr,1) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_16,axiom,(
    ! [VarNext: state_type] :
      ( v568(VarNext)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v567(VarNext,B)
          <=> v578(VarNext,B) ) ) ) )).

tff(addAssignment_182,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v578(VarNext,B)
          <=> v576(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondShiftedRangesElseBranch_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v579(VarCurr)
     => ( ( v576(VarCurr,2)
        <=> v67(VarCurr,3) )
        & ( v576(VarCurr,1)
        <=> v67(VarCurr,2) )
        & ( v576(VarCurr,0)
        <=> v67(VarCurr,1) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_17,axiom,(
    ! [VarCurr: state_type] :
      ( v579(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,3)
            & ~ $less(B,0) )
         => ( v576(VarCurr,B)
          <=> $false ) ) ) )).

tff(writeUnaryOperator_36,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v579(VarCurr)
    <=> v64(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_80,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v568(VarNext)
      <=> v569(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_79,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v569(VarNext)
      <=> ( v570(VarNext)
          & v565(VarNext) ) ) ) )).

tff(writeUnaryOperator_35,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v570(VarNext)
      <=> v572(VarNext) ) ) )).

tff(addAssignment_181,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v572(VarNext)
      <=> v565(VarCurr) ) ) )).

tff(addAssignment_180,axiom,(
    ! [VarCurr: state_type] :
      ( v565(VarCurr)
    <=> v1(VarCurr) ) )).

tff(addConditionBooleanCondShiftedRangesElseBranch,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v558(VarCurr)
     => ( v67(VarCurr,3)
      <=> $false ) ) )).

tff(addConditionBooleanCondShiftedRangesThenBranch,axiom,(
    ! [VarCurr: state_type] :
      ( v558(VarCurr)
     => ( v67(VarCurr,3)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_78,axiom,(
    ! [VarCurr: state_type] :
      ( v558(VarCurr)
    <=> ( v559(VarCurr)
        | v561(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_77,axiom,(
    ! [VarCurr: state_type] :
      ( v561(VarCurr)
    <=> ( v562(VarCurr)
        & v563(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_35,axiom,(
    ! [VarCurr: state_type] :
      ( v563(VarCurr)
    <=> ( $true
      <=> v62(VarCurr,3) ) ) )).

tff(writeUnaryOperator_34,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v562(VarCurr)
    <=> v554(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_76,axiom,(
    ! [VarCurr: state_type] :
      ( v559(VarCurr)
    <=> ( v554(VarCurr)
        & v560(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_34,axiom,(
    ! [VarCurr: state_type] :
      ( v560(VarCurr)
    <=> ( $true
      <=> v62(VarCurr,2) ) ) )).

tff(addAssignmentInitValueVector_3,axiom,
    ( ( v62(constB0,3)
    <=> $false )
    & ( v62(constB0,2)
    <=> $false )
    & ( v62(constB0,1)
    <=> $false ) )).

tff(bitBlastConstant_155,axiom,(
    ~ b000(2) )).

tff(bitBlastConstant_154,axiom,(
    ~ b000(1) )).

tff(bitBlastConstant_153,axiom,(
    ~ b000(0) )).

tff(addAssignmentInitValueVector_2,axiom,
    ( v62(constB0,0)
  <=> $true )).

tff(addAssignment_179,axiom,(
    ! [VarCurr: state_type] :
      ( v554(VarCurr)
    <=> v556(VarCurr) ) )).

tff(addAssignment_178,axiom,(
    ! [VarCurr: state_type] :
      ( v82(VarCurr)
    <=> v84(VarCurr) ) )).

tff(addAssignment_177,axiom,(
    ! [VarCurr: state_type] :
      ( v84(VarCurr)
    <=> v86(VarCurr) ) )).

tff(addAssignment_176,axiom,(
    ! [VarCurr: state_type] :
      ( v86(VarCurr)
    <=> v88(VarCurr,0) ) )).

tff(addAssignment_175,axiom,(
    ! [VarCurr: state_type] :
      ( v88(VarCurr,0)
    <=> v90(VarCurr,49) ) )).

tff(addAssignment_174,axiom,(
    ! [VarCurr: state_type] :
      ( v90(VarCurr,49)
    <=> v92(VarCurr,49) ) )).

tff(addAssignment_173,axiom,(
    ! [VarCurr: state_type] :
      ( v92(VarCurr,49)
    <=> v94(VarCurr,539) ) )).

tff(addAssignment_172,axiom,(
    ! [VarNext: state_type] :
      ( v94(VarNext,539)
    <=> v521(VarNext,49) ) )).

tff(addCaseBooleanConditionShiftedRanges1_6,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v522(VarNext)
       => ( ( v521(VarNext,69)
          <=> v94(VarCurr,559) )
          & ( v521(VarNext,68)
          <=> v94(VarCurr,558) )
          & ( v521(VarNext,67)
          <=> v94(VarCurr,557) )
          & ( v521(VarNext,66)
          <=> v94(VarCurr,556) )
          & ( v521(VarNext,65)
          <=> v94(VarCurr,555) )
          & ( v521(VarNext,64)
          <=> v94(VarCurr,554) )
          & ( v521(VarNext,63)
          <=> v94(VarCurr,553) )
          & ( v521(VarNext,62)
          <=> v94(VarCurr,552) )
          & ( v521(VarNext,61)
          <=> v94(VarCurr,551) )
          & ( v521(VarNext,60)
          <=> v94(VarCurr,550) )
          & ( v521(VarNext,59)
          <=> v94(VarCurr,549) )
          & ( v521(VarNext,58)
          <=> v94(VarCurr,548) )
          & ( v521(VarNext,57)
          <=> v94(VarCurr,547) )
          & ( v521(VarNext,56)
          <=> v94(VarCurr,546) )
          & ( v521(VarNext,55)
          <=> v94(VarCurr,545) )
          & ( v521(VarNext,54)
          <=> v94(VarCurr,544) )
          & ( v521(VarNext,53)
          <=> v94(VarCurr,543) )
          & ( v521(VarNext,52)
          <=> v94(VarCurr,542) )
          & ( v521(VarNext,51)
          <=> v94(VarCurr,541) )
          & ( v521(VarNext,50)
          <=> v94(VarCurr,540) )
          & ( v521(VarNext,49)
          <=> v94(VarCurr,539) )
          & ( v521(VarNext,48)
          <=> v94(VarCurr,538) )
          & ( v521(VarNext,47)
          <=> v94(VarCurr,537) )
          & ( v521(VarNext,46)
          <=> v94(VarCurr,536) )
          & ( v521(VarNext,45)
          <=> v94(VarCurr,535) )
          & ( v521(VarNext,44)
          <=> v94(VarCurr,534) )
          & ( v521(VarNext,43)
          <=> v94(VarCurr,533) )
          & ( v521(VarNext,42)
          <=> v94(VarCurr,532) )
          & ( v521(VarNext,41)
          <=> v94(VarCurr,531) )
          & ( v521(VarNext,40)
          <=> v94(VarCurr,530) )
          & ( v521(VarNext,39)
          <=> v94(VarCurr,529) )
          & ( v521(VarNext,38)
          <=> v94(VarCurr,528) )
          & ( v521(VarNext,37)
          <=> v94(VarCurr,527) )
          & ( v521(VarNext,36)
          <=> v94(VarCurr,526) )
          & ( v521(VarNext,35)
          <=> v94(VarCurr,525) )
          & ( v521(VarNext,34)
          <=> v94(VarCurr,524) )
          & ( v521(VarNext,33)
          <=> v94(VarCurr,523) )
          & ( v521(VarNext,32)
          <=> v94(VarCurr,522) )
          & ( v521(VarNext,31)
          <=> v94(VarCurr,521) )
          & ( v521(VarNext,30)
          <=> v94(VarCurr,520) )
          & ( v521(VarNext,29)
          <=> v94(VarCurr,519) )
          & ( v521(VarNext,28)
          <=> v94(VarCurr,518) )
          & ( v521(VarNext,27)
          <=> v94(VarCurr,517) )
          & ( v521(VarNext,26)
          <=> v94(VarCurr,516) )
          & ( v521(VarNext,25)
          <=> v94(VarCurr,515) )
          & ( v521(VarNext,24)
          <=> v94(VarCurr,514) )
          & ( v521(VarNext,23)
          <=> v94(VarCurr,513) )
          & ( v521(VarNext,22)
          <=> v94(VarCurr,512) )
          & ( v521(VarNext,21)
          <=> v94(VarCurr,511) )
          & ( v521(VarNext,20)
          <=> v94(VarCurr,510) )
          & ( v521(VarNext,19)
          <=> v94(VarCurr,509) )
          & ( v521(VarNext,18)
          <=> v94(VarCurr,508) )
          & ( v521(VarNext,17)
          <=> v94(VarCurr,507) )
          & ( v521(VarNext,16)
          <=> v94(VarCurr,506) )
          & ( v521(VarNext,15)
          <=> v94(VarCurr,505) )
          & ( v521(VarNext,14)
          <=> v94(VarCurr,504) )
          & ( v521(VarNext,13)
          <=> v94(VarCurr,503) )
          & ( v521(VarNext,12)
          <=> v94(VarCurr,502) )
          & ( v521(VarNext,11)
          <=> v94(VarCurr,501) )
          & ( v521(VarNext,10)
          <=> v94(VarCurr,500) )
          & ( v521(VarNext,9)
          <=> v94(VarCurr,499) )
          & ( v521(VarNext,8)
          <=> v94(VarCurr,498) )
          & ( v521(VarNext,7)
          <=> v94(VarCurr,497) )
          & ( v521(VarNext,6)
          <=> v94(VarCurr,496) )
          & ( v521(VarNext,5)
          <=> v94(VarCurr,495) )
          & ( v521(VarNext,4)
          <=> v94(VarCurr,494) )
          & ( v521(VarNext,3)
          <=> v94(VarCurr,493) )
          & ( v521(VarNext,2)
          <=> v94(VarCurr,492) )
          & ( v521(VarNext,1)
          <=> v94(VarCurr,491) )
          & ( v521(VarNext,0)
          <=> v94(VarCurr,490) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_15,axiom,(
    ! [VarNext: state_type] :
      ( v522(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v521(VarNext,B)
          <=> v548(VarNext,B) ) ) ) )).

tff(addAssignment_171,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v548(VarNext,B)
          <=> v546(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_14,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v546(VarCurr,B)
          <=> v549(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_8,axiom,(
    ! [VarCurr: state_type] :
      ( v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v546(VarCurr,B)
          <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_7,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v535(VarCurr)
        & ~ v537(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v549(VarCurr,B)
          <=> v514(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_13,axiom,(
    ! [VarCurr: state_type] :
      ( v537(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v549(VarCurr,B)
          <=> v507(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionShiftedRanges0_5,axiom,(
    ! [VarCurr: state_type] :
      ( v535(VarCurr)
     => ( ( v549(VarCurr,69)
        <=> v94(VarCurr,489) )
        & ( v549(VarCurr,68)
        <=> v94(VarCurr,488) )
        & ( v549(VarCurr,67)
        <=> v94(VarCurr,487) )
        & ( v549(VarCurr,66)
        <=> v94(VarCurr,486) )
        & ( v549(VarCurr,65)
        <=> v94(VarCurr,485) )
        & ( v549(VarCurr,64)
        <=> v94(VarCurr,484) )
        & ( v549(VarCurr,63)
        <=> v94(VarCurr,483) )
        & ( v549(VarCurr,62)
        <=> v94(VarCurr,482) )
        & ( v549(VarCurr,61)
        <=> v94(VarCurr,481) )
        & ( v549(VarCurr,60)
        <=> v94(VarCurr,480) )
        & ( v549(VarCurr,59)
        <=> v94(VarCurr,479) )
        & ( v549(VarCurr,58)
        <=> v94(VarCurr,478) )
        & ( v549(VarCurr,57)
        <=> v94(VarCurr,477) )
        & ( v549(VarCurr,56)
        <=> v94(VarCurr,476) )
        & ( v549(VarCurr,55)
        <=> v94(VarCurr,475) )
        & ( v549(VarCurr,54)
        <=> v94(VarCurr,474) )
        & ( v549(VarCurr,53)
        <=> v94(VarCurr,473) )
        & ( v549(VarCurr,52)
        <=> v94(VarCurr,472) )
        & ( v549(VarCurr,51)
        <=> v94(VarCurr,471) )
        & ( v549(VarCurr,50)
        <=> v94(VarCurr,470) )
        & ( v549(VarCurr,49)
        <=> v94(VarCurr,469) )
        & ( v549(VarCurr,48)
        <=> v94(VarCurr,468) )
        & ( v549(VarCurr,47)
        <=> v94(VarCurr,467) )
        & ( v549(VarCurr,46)
        <=> v94(VarCurr,466) )
        & ( v549(VarCurr,45)
        <=> v94(VarCurr,465) )
        & ( v549(VarCurr,44)
        <=> v94(VarCurr,464) )
        & ( v549(VarCurr,43)
        <=> v94(VarCurr,463) )
        & ( v549(VarCurr,42)
        <=> v94(VarCurr,462) )
        & ( v549(VarCurr,41)
        <=> v94(VarCurr,461) )
        & ( v549(VarCurr,40)
        <=> v94(VarCurr,460) )
        & ( v549(VarCurr,39)
        <=> v94(VarCurr,459) )
        & ( v549(VarCurr,38)
        <=> v94(VarCurr,458) )
        & ( v549(VarCurr,37)
        <=> v94(VarCurr,457) )
        & ( v549(VarCurr,36)
        <=> v94(VarCurr,456) )
        & ( v549(VarCurr,35)
        <=> v94(VarCurr,455) )
        & ( v549(VarCurr,34)
        <=> v94(VarCurr,454) )
        & ( v549(VarCurr,33)
        <=> v94(VarCurr,453) )
        & ( v549(VarCurr,32)
        <=> v94(VarCurr,452) )
        & ( v549(VarCurr,31)
        <=> v94(VarCurr,451) )
        & ( v549(VarCurr,30)
        <=> v94(VarCurr,450) )
        & ( v549(VarCurr,29)
        <=> v94(VarCurr,449) )
        & ( v549(VarCurr,28)
        <=> v94(VarCurr,448) )
        & ( v549(VarCurr,27)
        <=> v94(VarCurr,447) )
        & ( v549(VarCurr,26)
        <=> v94(VarCurr,446) )
        & ( v549(VarCurr,25)
        <=> v94(VarCurr,445) )
        & ( v549(VarCurr,24)
        <=> v94(VarCurr,444) )
        & ( v549(VarCurr,23)
        <=> v94(VarCurr,443) )
        & ( v549(VarCurr,22)
        <=> v94(VarCurr,442) )
        & ( v549(VarCurr,21)
        <=> v94(VarCurr,441) )
        & ( v549(VarCurr,20)
        <=> v94(VarCurr,440) )
        & ( v549(VarCurr,19)
        <=> v94(VarCurr,439) )
        & ( v549(VarCurr,18)
        <=> v94(VarCurr,438) )
        & ( v549(VarCurr,17)
        <=> v94(VarCurr,437) )
        & ( v549(VarCurr,16)
        <=> v94(VarCurr,436) )
        & ( v549(VarCurr,15)
        <=> v94(VarCurr,435) )
        & ( v549(VarCurr,14)
        <=> v94(VarCurr,434) )
        & ( v549(VarCurr,13)
        <=> v94(VarCurr,433) )
        & ( v549(VarCurr,12)
        <=> v94(VarCurr,432) )
        & ( v549(VarCurr,11)
        <=> v94(VarCurr,431) )
        & ( v549(VarCurr,10)
        <=> v94(VarCurr,430) )
        & ( v549(VarCurr,9)
        <=> v94(VarCurr,429) )
        & ( v549(VarCurr,8)
        <=> v94(VarCurr,428) )
        & ( v549(VarCurr,7)
        <=> v94(VarCurr,427) )
        & ( v549(VarCurr,6)
        <=> v94(VarCurr,426) )
        & ( v549(VarCurr,5)
        <=> v94(VarCurr,425) )
        & ( v549(VarCurr,4)
        <=> v94(VarCurr,424) )
        & ( v549(VarCurr,3)
        <=> v94(VarCurr,423) )
        & ( v549(VarCurr,2)
        <=> v94(VarCurr,422) )
        & ( v549(VarCurr,1)
        <=> v94(VarCurr,421) )
        & ( v549(VarCurr,0)
        <=> v94(VarCurr,420) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_75,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v522(VarNext)
      <=> ( v523(VarNext)
          & v530(VarNext) ) ) ) )).

tff(addAssignment_170,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v530(VarNext)
      <=> v528(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_74,axiom,(
    ! [VarCurr: state_type] :
      ( v528(VarCurr)
    <=> ( v531(VarCurr)
        & v542(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_73,axiom,(
    ! [VarCurr: state_type] :
      ( v542(VarCurr)
    <=> ( v543(VarCurr)
        | v255(VarCurr) ) ) )).

tff(writeUnaryOperator_33,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v543(VarCurr)
    <=> v544(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_33,axiom,(
    ! [VarCurr: state_type] :
      ( v544(VarCurr)
    <=> ( ( v545(VarCurr,1)
        <=> $false )
        & ( v545(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_169,axiom,(
    ! [VarCurr: state_type] :
      ( v545(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_168,axiom,(
    ! [VarCurr: state_type] :
      ( v545(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_72,axiom,(
    ! [VarCurr: state_type] :
      ( v531(VarCurr)
    <=> ( v255(VarCurr)
        | v532(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_71,axiom,(
    ! [VarCurr: state_type] :
      ( v532(VarCurr)
    <=> ( v533(VarCurr)
        & v541(VarCurr) ) ) )).

tff(writeUnaryOperator_32,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v541(VarCurr)
    <=> v255(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_70,axiom,(
    ! [VarCurr: state_type] :
      ( v533(VarCurr)
    <=> ( v534(VarCurr)
        | v539(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_32,axiom,(
    ! [VarCurr: state_type] :
      ( v539(VarCurr)
    <=> ( ( v540(VarCurr,1)
        <=> $true )
        & ( v540(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_167,axiom,(
    ! [VarCurr: state_type] :
      ( v540(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_166,axiom,(
    ! [VarCurr: state_type] :
      ( v540(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_69,axiom,(
    ! [VarCurr: state_type] :
      ( v534(VarCurr)
    <=> ( v535(VarCurr)
        | v537(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_31,axiom,(
    ! [VarCurr: state_type] :
      ( v537(VarCurr)
    <=> ( ( v538(VarCurr,1)
        <=> $true )
        & ( v538(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_165,axiom,(
    ! [VarCurr: state_type] :
      ( v538(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_164,axiom,(
    ! [VarCurr: state_type] :
      ( v538(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_30,axiom,(
    ! [VarCurr: state_type] :
      ( v535(VarCurr)
    <=> ( ( v536(VarCurr,1)
        <=> $false )
        & ( v536(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_163,axiom,(
    ! [VarCurr: state_type] :
      ( v536(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_162,axiom,(
    ! [VarCurr: state_type] :
      ( v536(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_68,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v523(VarNext)
      <=> ( v525(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_31,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v525(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_161,axiom,(
    ! [VarCurr: state_type] :
      ( v514(VarCurr,49)
    <=> v519(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_16,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v516(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v519(VarCurr,B)
          <=> v518(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_16,axiom,(
    ! [VarCurr: state_type] :
      ( v516(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v519(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_160,axiom,(
    ! [VarCurr: state_type] :
      ( v518(VarCurr,49)
    <=> v94(VarCurr,469) ) )).

tff(addAssignment_159,axiom,(
    ! [VarCurr: state_type] :
      ( v516(VarCurr)
    <=> v103(VarCurr,1) ) )).

tff(addAssignment_158,axiom,(
    ! [VarCurr: state_type] :
      ( v507(VarCurr,49)
    <=> v512(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_15,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v509(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v512(VarCurr,B)
          <=> v511(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_15,axiom,(
    ! [VarCurr: state_type] :
      ( v509(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v512(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_157,axiom,(
    ! [VarCurr: state_type] :
      ( v511(VarCurr,49)
    <=> v94(VarCurr,539) ) )).

tff(addAssignment_156,axiom,(
    ! [VarCurr: state_type] :
      ( v509(VarCurr)
    <=> v103(VarCurr,1) ) )).

tff(addAssignment_155,axiom,(
    ! [VarNext: state_type] :
      ( v94(VarNext,469)
    <=> v475(VarNext,49) ) )).

tff(addCaseBooleanConditionShiftedRanges1_5,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v476(VarNext)
       => ( ( v475(VarNext,69)
          <=> v94(VarCurr,489) )
          & ( v475(VarNext,68)
          <=> v94(VarCurr,488) )
          & ( v475(VarNext,67)
          <=> v94(VarCurr,487) )
          & ( v475(VarNext,66)
          <=> v94(VarCurr,486) )
          & ( v475(VarNext,65)
          <=> v94(VarCurr,485) )
          & ( v475(VarNext,64)
          <=> v94(VarCurr,484) )
          & ( v475(VarNext,63)
          <=> v94(VarCurr,483) )
          & ( v475(VarNext,62)
          <=> v94(VarCurr,482) )
          & ( v475(VarNext,61)
          <=> v94(VarCurr,481) )
          & ( v475(VarNext,60)
          <=> v94(VarCurr,480) )
          & ( v475(VarNext,59)
          <=> v94(VarCurr,479) )
          & ( v475(VarNext,58)
          <=> v94(VarCurr,478) )
          & ( v475(VarNext,57)
          <=> v94(VarCurr,477) )
          & ( v475(VarNext,56)
          <=> v94(VarCurr,476) )
          & ( v475(VarNext,55)
          <=> v94(VarCurr,475) )
          & ( v475(VarNext,54)
          <=> v94(VarCurr,474) )
          & ( v475(VarNext,53)
          <=> v94(VarCurr,473) )
          & ( v475(VarNext,52)
          <=> v94(VarCurr,472) )
          & ( v475(VarNext,51)
          <=> v94(VarCurr,471) )
          & ( v475(VarNext,50)
          <=> v94(VarCurr,470) )
          & ( v475(VarNext,49)
          <=> v94(VarCurr,469) )
          & ( v475(VarNext,48)
          <=> v94(VarCurr,468) )
          & ( v475(VarNext,47)
          <=> v94(VarCurr,467) )
          & ( v475(VarNext,46)
          <=> v94(VarCurr,466) )
          & ( v475(VarNext,45)
          <=> v94(VarCurr,465) )
          & ( v475(VarNext,44)
          <=> v94(VarCurr,464) )
          & ( v475(VarNext,43)
          <=> v94(VarCurr,463) )
          & ( v475(VarNext,42)
          <=> v94(VarCurr,462) )
          & ( v475(VarNext,41)
          <=> v94(VarCurr,461) )
          & ( v475(VarNext,40)
          <=> v94(VarCurr,460) )
          & ( v475(VarNext,39)
          <=> v94(VarCurr,459) )
          & ( v475(VarNext,38)
          <=> v94(VarCurr,458) )
          & ( v475(VarNext,37)
          <=> v94(VarCurr,457) )
          & ( v475(VarNext,36)
          <=> v94(VarCurr,456) )
          & ( v475(VarNext,35)
          <=> v94(VarCurr,455) )
          & ( v475(VarNext,34)
          <=> v94(VarCurr,454) )
          & ( v475(VarNext,33)
          <=> v94(VarCurr,453) )
          & ( v475(VarNext,32)
          <=> v94(VarCurr,452) )
          & ( v475(VarNext,31)
          <=> v94(VarCurr,451) )
          & ( v475(VarNext,30)
          <=> v94(VarCurr,450) )
          & ( v475(VarNext,29)
          <=> v94(VarCurr,449) )
          & ( v475(VarNext,28)
          <=> v94(VarCurr,448) )
          & ( v475(VarNext,27)
          <=> v94(VarCurr,447) )
          & ( v475(VarNext,26)
          <=> v94(VarCurr,446) )
          & ( v475(VarNext,25)
          <=> v94(VarCurr,445) )
          & ( v475(VarNext,24)
          <=> v94(VarCurr,444) )
          & ( v475(VarNext,23)
          <=> v94(VarCurr,443) )
          & ( v475(VarNext,22)
          <=> v94(VarCurr,442) )
          & ( v475(VarNext,21)
          <=> v94(VarCurr,441) )
          & ( v475(VarNext,20)
          <=> v94(VarCurr,440) )
          & ( v475(VarNext,19)
          <=> v94(VarCurr,439) )
          & ( v475(VarNext,18)
          <=> v94(VarCurr,438) )
          & ( v475(VarNext,17)
          <=> v94(VarCurr,437) )
          & ( v475(VarNext,16)
          <=> v94(VarCurr,436) )
          & ( v475(VarNext,15)
          <=> v94(VarCurr,435) )
          & ( v475(VarNext,14)
          <=> v94(VarCurr,434) )
          & ( v475(VarNext,13)
          <=> v94(VarCurr,433) )
          & ( v475(VarNext,12)
          <=> v94(VarCurr,432) )
          & ( v475(VarNext,11)
          <=> v94(VarCurr,431) )
          & ( v475(VarNext,10)
          <=> v94(VarCurr,430) )
          & ( v475(VarNext,9)
          <=> v94(VarCurr,429) )
          & ( v475(VarNext,8)
          <=> v94(VarCurr,428) )
          & ( v475(VarNext,7)
          <=> v94(VarCurr,427) )
          & ( v475(VarNext,6)
          <=> v94(VarCurr,426) )
          & ( v475(VarNext,5)
          <=> v94(VarCurr,425) )
          & ( v475(VarNext,4)
          <=> v94(VarCurr,424) )
          & ( v475(VarNext,3)
          <=> v94(VarCurr,423) )
          & ( v475(VarNext,2)
          <=> v94(VarCurr,422) )
          & ( v475(VarNext,1)
          <=> v94(VarCurr,421) )
          & ( v475(VarNext,0)
          <=> v94(VarCurr,420) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_14,axiom,(
    ! [VarNext: state_type] :
      ( v476(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v475(VarNext,B)
          <=> v502(VarNext,B) ) ) ) )).

tff(addAssignment_154,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v502(VarNext,B)
          <=> v500(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_12,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v500(VarCurr,B)
          <=> v503(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_7,axiom,(
    ! [VarCurr: state_type] :
      ( v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v500(VarCurr,B)
          <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_6,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v489(VarCurr)
        & ~ v491(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v503(VarCurr,B)
          <=> v468(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_11,axiom,(
    ! [VarCurr: state_type] :
      ( v491(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v503(VarCurr,B)
          <=> v461(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionShiftedRanges0_4,axiom,(
    ! [VarCurr: state_type] :
      ( v489(VarCurr)
     => ( ( v503(VarCurr,69)
        <=> v94(VarCurr,419) )
        & ( v503(VarCurr,68)
        <=> v94(VarCurr,418) )
        & ( v503(VarCurr,67)
        <=> v94(VarCurr,417) )
        & ( v503(VarCurr,66)
        <=> v94(VarCurr,416) )
        & ( v503(VarCurr,65)
        <=> v94(VarCurr,415) )
        & ( v503(VarCurr,64)
        <=> v94(VarCurr,414) )
        & ( v503(VarCurr,63)
        <=> v94(VarCurr,413) )
        & ( v503(VarCurr,62)
        <=> v94(VarCurr,412) )
        & ( v503(VarCurr,61)
        <=> v94(VarCurr,411) )
        & ( v503(VarCurr,60)
        <=> v94(VarCurr,410) )
        & ( v503(VarCurr,59)
        <=> v94(VarCurr,409) )
        & ( v503(VarCurr,58)
        <=> v94(VarCurr,408) )
        & ( v503(VarCurr,57)
        <=> v94(VarCurr,407) )
        & ( v503(VarCurr,56)
        <=> v94(VarCurr,406) )
        & ( v503(VarCurr,55)
        <=> v94(VarCurr,405) )
        & ( v503(VarCurr,54)
        <=> v94(VarCurr,404) )
        & ( v503(VarCurr,53)
        <=> v94(VarCurr,403) )
        & ( v503(VarCurr,52)
        <=> v94(VarCurr,402) )
        & ( v503(VarCurr,51)
        <=> v94(VarCurr,401) )
        & ( v503(VarCurr,50)
        <=> v94(VarCurr,400) )
        & ( v503(VarCurr,49)
        <=> v94(VarCurr,399) )
        & ( v503(VarCurr,48)
        <=> v94(VarCurr,398) )
        & ( v503(VarCurr,47)
        <=> v94(VarCurr,397) )
        & ( v503(VarCurr,46)
        <=> v94(VarCurr,396) )
        & ( v503(VarCurr,45)
        <=> v94(VarCurr,395) )
        & ( v503(VarCurr,44)
        <=> v94(VarCurr,394) )
        & ( v503(VarCurr,43)
        <=> v94(VarCurr,393) )
        & ( v503(VarCurr,42)
        <=> v94(VarCurr,392) )
        & ( v503(VarCurr,41)
        <=> v94(VarCurr,391) )
        & ( v503(VarCurr,40)
        <=> v94(VarCurr,390) )
        & ( v503(VarCurr,39)
        <=> v94(VarCurr,389) )
        & ( v503(VarCurr,38)
        <=> v94(VarCurr,388) )
        & ( v503(VarCurr,37)
        <=> v94(VarCurr,387) )
        & ( v503(VarCurr,36)
        <=> v94(VarCurr,386) )
        & ( v503(VarCurr,35)
        <=> v94(VarCurr,385) )
        & ( v503(VarCurr,34)
        <=> v94(VarCurr,384) )
        & ( v503(VarCurr,33)
        <=> v94(VarCurr,383) )
        & ( v503(VarCurr,32)
        <=> v94(VarCurr,382) )
        & ( v503(VarCurr,31)
        <=> v94(VarCurr,381) )
        & ( v503(VarCurr,30)
        <=> v94(VarCurr,380) )
        & ( v503(VarCurr,29)
        <=> v94(VarCurr,379) )
        & ( v503(VarCurr,28)
        <=> v94(VarCurr,378) )
        & ( v503(VarCurr,27)
        <=> v94(VarCurr,377) )
        & ( v503(VarCurr,26)
        <=> v94(VarCurr,376) )
        & ( v503(VarCurr,25)
        <=> v94(VarCurr,375) )
        & ( v503(VarCurr,24)
        <=> v94(VarCurr,374) )
        & ( v503(VarCurr,23)
        <=> v94(VarCurr,373) )
        & ( v503(VarCurr,22)
        <=> v94(VarCurr,372) )
        & ( v503(VarCurr,21)
        <=> v94(VarCurr,371) )
        & ( v503(VarCurr,20)
        <=> v94(VarCurr,370) )
        & ( v503(VarCurr,19)
        <=> v94(VarCurr,369) )
        & ( v503(VarCurr,18)
        <=> v94(VarCurr,368) )
        & ( v503(VarCurr,17)
        <=> v94(VarCurr,367) )
        & ( v503(VarCurr,16)
        <=> v94(VarCurr,366) )
        & ( v503(VarCurr,15)
        <=> v94(VarCurr,365) )
        & ( v503(VarCurr,14)
        <=> v94(VarCurr,364) )
        & ( v503(VarCurr,13)
        <=> v94(VarCurr,363) )
        & ( v503(VarCurr,12)
        <=> v94(VarCurr,362) )
        & ( v503(VarCurr,11)
        <=> v94(VarCurr,361) )
        & ( v503(VarCurr,10)
        <=> v94(VarCurr,360) )
        & ( v503(VarCurr,9)
        <=> v94(VarCurr,359) )
        & ( v503(VarCurr,8)
        <=> v94(VarCurr,358) )
        & ( v503(VarCurr,7)
        <=> v94(VarCurr,357) )
        & ( v503(VarCurr,6)
        <=> v94(VarCurr,356) )
        & ( v503(VarCurr,5)
        <=> v94(VarCurr,355) )
        & ( v503(VarCurr,4)
        <=> v94(VarCurr,354) )
        & ( v503(VarCurr,3)
        <=> v94(VarCurr,353) )
        & ( v503(VarCurr,2)
        <=> v94(VarCurr,352) )
        & ( v503(VarCurr,1)
        <=> v94(VarCurr,351) )
        & ( v503(VarCurr,0)
        <=> v94(VarCurr,350) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_67,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v476(VarNext)
      <=> ( v477(VarNext)
          & v484(VarNext) ) ) ) )).

tff(addAssignment_153,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v484(VarNext)
      <=> v482(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_66,axiom,(
    ! [VarCurr: state_type] :
      ( v482(VarCurr)
    <=> ( v485(VarCurr)
        & v496(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_65,axiom,(
    ! [VarCurr: state_type] :
      ( v496(VarCurr)
    <=> ( v497(VarCurr)
        | v255(VarCurr) ) ) )).

tff(writeUnaryOperator_30,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v497(VarCurr)
    <=> v498(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_29,axiom,(
    ! [VarCurr: state_type] :
      ( v498(VarCurr)
    <=> ( ( v499(VarCurr,1)
        <=> $false )
        & ( v499(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_152,axiom,(
    ! [VarCurr: state_type] :
      ( v499(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_151,axiom,(
    ! [VarCurr: state_type] :
      ( v499(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_64,axiom,(
    ! [VarCurr: state_type] :
      ( v485(VarCurr)
    <=> ( v255(VarCurr)
        | v486(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_63,axiom,(
    ! [VarCurr: state_type] :
      ( v486(VarCurr)
    <=> ( v487(VarCurr)
        & v495(VarCurr) ) ) )).

tff(writeUnaryOperator_29,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v495(VarCurr)
    <=> v255(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_62,axiom,(
    ! [VarCurr: state_type] :
      ( v487(VarCurr)
    <=> ( v488(VarCurr)
        | v493(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_28,axiom,(
    ! [VarCurr: state_type] :
      ( v493(VarCurr)
    <=> ( ( v494(VarCurr,1)
        <=> $true )
        & ( v494(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_150,axiom,(
    ! [VarCurr: state_type] :
      ( v494(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_149,axiom,(
    ! [VarCurr: state_type] :
      ( v494(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_61,axiom,(
    ! [VarCurr: state_type] :
      ( v488(VarCurr)
    <=> ( v489(VarCurr)
        | v491(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_27,axiom,(
    ! [VarCurr: state_type] :
      ( v491(VarCurr)
    <=> ( ( v492(VarCurr,1)
        <=> $true )
        & ( v492(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_148,axiom,(
    ! [VarCurr: state_type] :
      ( v492(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_147,axiom,(
    ! [VarCurr: state_type] :
      ( v492(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_26,axiom,(
    ! [VarCurr: state_type] :
      ( v489(VarCurr)
    <=> ( ( v490(VarCurr,1)
        <=> $false )
        & ( v490(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_146,axiom,(
    ! [VarCurr: state_type] :
      ( v490(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_145,axiom,(
    ! [VarCurr: state_type] :
      ( v490(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_60,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v477(VarNext)
      <=> ( v479(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_28,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v479(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_144,axiom,(
    ! [VarCurr: state_type] :
      ( v468(VarCurr,49)
    <=> v473(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_14,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v470(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v473(VarCurr,B)
          <=> v472(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_14,axiom,(
    ! [VarCurr: state_type] :
      ( v470(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v473(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_143,axiom,(
    ! [VarCurr: state_type] :
      ( v472(VarCurr,49)
    <=> v94(VarCurr,399) ) )).

tff(addAssignment_142,axiom,(
    ! [VarCurr: state_type] :
      ( v470(VarCurr)
    <=> v103(VarCurr,2) ) )).

tff(addAssignment_141,axiom,(
    ! [VarCurr: state_type] :
      ( v461(VarCurr,49)
    <=> v466(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_13,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v463(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v466(VarCurr,B)
          <=> v465(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_13,axiom,(
    ! [VarCurr: state_type] :
      ( v463(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v466(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_140,axiom,(
    ! [VarCurr: state_type] :
      ( v465(VarCurr,49)
    <=> v94(VarCurr,469) ) )).

tff(addAssignment_139,axiom,(
    ! [VarCurr: state_type] :
      ( v463(VarCurr)
    <=> v103(VarCurr,2) ) )).

tff(addAssignment_138,axiom,(
    ! [VarNext: state_type] :
      ( v94(VarNext,399)
    <=> v429(VarNext,49) ) )).

tff(addCaseBooleanConditionShiftedRanges1_4,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v430(VarNext)
       => ( ( v429(VarNext,69)
          <=> v94(VarCurr,419) )
          & ( v429(VarNext,68)
          <=> v94(VarCurr,418) )
          & ( v429(VarNext,67)
          <=> v94(VarCurr,417) )
          & ( v429(VarNext,66)
          <=> v94(VarCurr,416) )
          & ( v429(VarNext,65)
          <=> v94(VarCurr,415) )
          & ( v429(VarNext,64)
          <=> v94(VarCurr,414) )
          & ( v429(VarNext,63)
          <=> v94(VarCurr,413) )
          & ( v429(VarNext,62)
          <=> v94(VarCurr,412) )
          & ( v429(VarNext,61)
          <=> v94(VarCurr,411) )
          & ( v429(VarNext,60)
          <=> v94(VarCurr,410) )
          & ( v429(VarNext,59)
          <=> v94(VarCurr,409) )
          & ( v429(VarNext,58)
          <=> v94(VarCurr,408) )
          & ( v429(VarNext,57)
          <=> v94(VarCurr,407) )
          & ( v429(VarNext,56)
          <=> v94(VarCurr,406) )
          & ( v429(VarNext,55)
          <=> v94(VarCurr,405) )
          & ( v429(VarNext,54)
          <=> v94(VarCurr,404) )
          & ( v429(VarNext,53)
          <=> v94(VarCurr,403) )
          & ( v429(VarNext,52)
          <=> v94(VarCurr,402) )
          & ( v429(VarNext,51)
          <=> v94(VarCurr,401) )
          & ( v429(VarNext,50)
          <=> v94(VarCurr,400) )
          & ( v429(VarNext,49)
          <=> v94(VarCurr,399) )
          & ( v429(VarNext,48)
          <=> v94(VarCurr,398) )
          & ( v429(VarNext,47)
          <=> v94(VarCurr,397) )
          & ( v429(VarNext,46)
          <=> v94(VarCurr,396) )
          & ( v429(VarNext,45)
          <=> v94(VarCurr,395) )
          & ( v429(VarNext,44)
          <=> v94(VarCurr,394) )
          & ( v429(VarNext,43)
          <=> v94(VarCurr,393) )
          & ( v429(VarNext,42)
          <=> v94(VarCurr,392) )
          & ( v429(VarNext,41)
          <=> v94(VarCurr,391) )
          & ( v429(VarNext,40)
          <=> v94(VarCurr,390) )
          & ( v429(VarNext,39)
          <=> v94(VarCurr,389) )
          & ( v429(VarNext,38)
          <=> v94(VarCurr,388) )
          & ( v429(VarNext,37)
          <=> v94(VarCurr,387) )
          & ( v429(VarNext,36)
          <=> v94(VarCurr,386) )
          & ( v429(VarNext,35)
          <=> v94(VarCurr,385) )
          & ( v429(VarNext,34)
          <=> v94(VarCurr,384) )
          & ( v429(VarNext,33)
          <=> v94(VarCurr,383) )
          & ( v429(VarNext,32)
          <=> v94(VarCurr,382) )
          & ( v429(VarNext,31)
          <=> v94(VarCurr,381) )
          & ( v429(VarNext,30)
          <=> v94(VarCurr,380) )
          & ( v429(VarNext,29)
          <=> v94(VarCurr,379) )
          & ( v429(VarNext,28)
          <=> v94(VarCurr,378) )
          & ( v429(VarNext,27)
          <=> v94(VarCurr,377) )
          & ( v429(VarNext,26)
          <=> v94(VarCurr,376) )
          & ( v429(VarNext,25)
          <=> v94(VarCurr,375) )
          & ( v429(VarNext,24)
          <=> v94(VarCurr,374) )
          & ( v429(VarNext,23)
          <=> v94(VarCurr,373) )
          & ( v429(VarNext,22)
          <=> v94(VarCurr,372) )
          & ( v429(VarNext,21)
          <=> v94(VarCurr,371) )
          & ( v429(VarNext,20)
          <=> v94(VarCurr,370) )
          & ( v429(VarNext,19)
          <=> v94(VarCurr,369) )
          & ( v429(VarNext,18)
          <=> v94(VarCurr,368) )
          & ( v429(VarNext,17)
          <=> v94(VarCurr,367) )
          & ( v429(VarNext,16)
          <=> v94(VarCurr,366) )
          & ( v429(VarNext,15)
          <=> v94(VarCurr,365) )
          & ( v429(VarNext,14)
          <=> v94(VarCurr,364) )
          & ( v429(VarNext,13)
          <=> v94(VarCurr,363) )
          & ( v429(VarNext,12)
          <=> v94(VarCurr,362) )
          & ( v429(VarNext,11)
          <=> v94(VarCurr,361) )
          & ( v429(VarNext,10)
          <=> v94(VarCurr,360) )
          & ( v429(VarNext,9)
          <=> v94(VarCurr,359) )
          & ( v429(VarNext,8)
          <=> v94(VarCurr,358) )
          & ( v429(VarNext,7)
          <=> v94(VarCurr,357) )
          & ( v429(VarNext,6)
          <=> v94(VarCurr,356) )
          & ( v429(VarNext,5)
          <=> v94(VarCurr,355) )
          & ( v429(VarNext,4)
          <=> v94(VarCurr,354) )
          & ( v429(VarNext,3)
          <=> v94(VarCurr,353) )
          & ( v429(VarNext,2)
          <=> v94(VarCurr,352) )
          & ( v429(VarNext,1)
          <=> v94(VarCurr,351) )
          & ( v429(VarNext,0)
          <=> v94(VarCurr,350) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_13,axiom,(
    ! [VarNext: state_type] :
      ( v430(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v429(VarNext,B)
          <=> v456(VarNext,B) ) ) ) )).

tff(addAssignment_137,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v456(VarNext,B)
          <=> v454(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_10,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v454(VarCurr,B)
          <=> v457(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_6,axiom,(
    ! [VarCurr: state_type] :
      ( v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v454(VarCurr,B)
          <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_5,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v443(VarCurr)
        & ~ v445(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v457(VarCurr,B)
          <=> v422(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_9,axiom,(
    ! [VarCurr: state_type] :
      ( v445(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v457(VarCurr,B)
          <=> v415(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionShiftedRanges0_3,axiom,(
    ! [VarCurr: state_type] :
      ( v443(VarCurr)
     => ( ( v457(VarCurr,69)
        <=> v94(VarCurr,349) )
        & ( v457(VarCurr,68)
        <=> v94(VarCurr,348) )
        & ( v457(VarCurr,67)
        <=> v94(VarCurr,347) )
        & ( v457(VarCurr,66)
        <=> v94(VarCurr,346) )
        & ( v457(VarCurr,65)
        <=> v94(VarCurr,345) )
        & ( v457(VarCurr,64)
        <=> v94(VarCurr,344) )
        & ( v457(VarCurr,63)
        <=> v94(VarCurr,343) )
        & ( v457(VarCurr,62)
        <=> v94(VarCurr,342) )
        & ( v457(VarCurr,61)
        <=> v94(VarCurr,341) )
        & ( v457(VarCurr,60)
        <=> v94(VarCurr,340) )
        & ( v457(VarCurr,59)
        <=> v94(VarCurr,339) )
        & ( v457(VarCurr,58)
        <=> v94(VarCurr,338) )
        & ( v457(VarCurr,57)
        <=> v94(VarCurr,337) )
        & ( v457(VarCurr,56)
        <=> v94(VarCurr,336) )
        & ( v457(VarCurr,55)
        <=> v94(VarCurr,335) )
        & ( v457(VarCurr,54)
        <=> v94(VarCurr,334) )
        & ( v457(VarCurr,53)
        <=> v94(VarCurr,333) )
        & ( v457(VarCurr,52)
        <=> v94(VarCurr,332) )
        & ( v457(VarCurr,51)
        <=> v94(VarCurr,331) )
        & ( v457(VarCurr,50)
        <=> v94(VarCurr,330) )
        & ( v457(VarCurr,49)
        <=> v94(VarCurr,329) )
        & ( v457(VarCurr,48)
        <=> v94(VarCurr,328) )
        & ( v457(VarCurr,47)
        <=> v94(VarCurr,327) )
        & ( v457(VarCurr,46)
        <=> v94(VarCurr,326) )
        & ( v457(VarCurr,45)
        <=> v94(VarCurr,325) )
        & ( v457(VarCurr,44)
        <=> v94(VarCurr,324) )
        & ( v457(VarCurr,43)
        <=> v94(VarCurr,323) )
        & ( v457(VarCurr,42)
        <=> v94(VarCurr,322) )
        & ( v457(VarCurr,41)
        <=> v94(VarCurr,321) )
        & ( v457(VarCurr,40)
        <=> v94(VarCurr,320) )
        & ( v457(VarCurr,39)
        <=> v94(VarCurr,319) )
        & ( v457(VarCurr,38)
        <=> v94(VarCurr,318) )
        & ( v457(VarCurr,37)
        <=> v94(VarCurr,317) )
        & ( v457(VarCurr,36)
        <=> v94(VarCurr,316) )
        & ( v457(VarCurr,35)
        <=> v94(VarCurr,315) )
        & ( v457(VarCurr,34)
        <=> v94(VarCurr,314) )
        & ( v457(VarCurr,33)
        <=> v94(VarCurr,313) )
        & ( v457(VarCurr,32)
        <=> v94(VarCurr,312) )
        & ( v457(VarCurr,31)
        <=> v94(VarCurr,311) )
        & ( v457(VarCurr,30)
        <=> v94(VarCurr,310) )
        & ( v457(VarCurr,29)
        <=> v94(VarCurr,309) )
        & ( v457(VarCurr,28)
        <=> v94(VarCurr,308) )
        & ( v457(VarCurr,27)
        <=> v94(VarCurr,307) )
        & ( v457(VarCurr,26)
        <=> v94(VarCurr,306) )
        & ( v457(VarCurr,25)
        <=> v94(VarCurr,305) )
        & ( v457(VarCurr,24)
        <=> v94(VarCurr,304) )
        & ( v457(VarCurr,23)
        <=> v94(VarCurr,303) )
        & ( v457(VarCurr,22)
        <=> v94(VarCurr,302) )
        & ( v457(VarCurr,21)
        <=> v94(VarCurr,301) )
        & ( v457(VarCurr,20)
        <=> v94(VarCurr,300) )
        & ( v457(VarCurr,19)
        <=> v94(VarCurr,299) )
        & ( v457(VarCurr,18)
        <=> v94(VarCurr,298) )
        & ( v457(VarCurr,17)
        <=> v94(VarCurr,297) )
        & ( v457(VarCurr,16)
        <=> v94(VarCurr,296) )
        & ( v457(VarCurr,15)
        <=> v94(VarCurr,295) )
        & ( v457(VarCurr,14)
        <=> v94(VarCurr,294) )
        & ( v457(VarCurr,13)
        <=> v94(VarCurr,293) )
        & ( v457(VarCurr,12)
        <=> v94(VarCurr,292) )
        & ( v457(VarCurr,11)
        <=> v94(VarCurr,291) )
        & ( v457(VarCurr,10)
        <=> v94(VarCurr,290) )
        & ( v457(VarCurr,9)
        <=> v94(VarCurr,289) )
        & ( v457(VarCurr,8)
        <=> v94(VarCurr,288) )
        & ( v457(VarCurr,7)
        <=> v94(VarCurr,287) )
        & ( v457(VarCurr,6)
        <=> v94(VarCurr,286) )
        & ( v457(VarCurr,5)
        <=> v94(VarCurr,285) )
        & ( v457(VarCurr,4)
        <=> v94(VarCurr,284) )
        & ( v457(VarCurr,3)
        <=> v94(VarCurr,283) )
        & ( v457(VarCurr,2)
        <=> v94(VarCurr,282) )
        & ( v457(VarCurr,1)
        <=> v94(VarCurr,281) )
        & ( v457(VarCurr,0)
        <=> v94(VarCurr,280) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_59,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v430(VarNext)
      <=> ( v431(VarNext)
          & v438(VarNext) ) ) ) )).

tff(addAssignment_136,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v438(VarNext)
      <=> v436(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_58,axiom,(
    ! [VarCurr: state_type] :
      ( v436(VarCurr)
    <=> ( v439(VarCurr)
        & v450(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_57,axiom,(
    ! [VarCurr: state_type] :
      ( v450(VarCurr)
    <=> ( v451(VarCurr)
        | v255(VarCurr) ) ) )).

tff(writeUnaryOperator_27,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v451(VarCurr)
    <=> v452(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_25,axiom,(
    ! [VarCurr: state_type] :
      ( v452(VarCurr)
    <=> ( ( v453(VarCurr,1)
        <=> $false )
        & ( v453(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_135,axiom,(
    ! [VarCurr: state_type] :
      ( v453(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_134,axiom,(
    ! [VarCurr: state_type] :
      ( v453(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_56,axiom,(
    ! [VarCurr: state_type] :
      ( v439(VarCurr)
    <=> ( v255(VarCurr)
        | v440(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_55,axiom,(
    ! [VarCurr: state_type] :
      ( v440(VarCurr)
    <=> ( v441(VarCurr)
        & v449(VarCurr) ) ) )).

tff(writeUnaryOperator_26,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v449(VarCurr)
    <=> v255(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_54,axiom,(
    ! [VarCurr: state_type] :
      ( v441(VarCurr)
    <=> ( v442(VarCurr)
        | v447(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_24,axiom,(
    ! [VarCurr: state_type] :
      ( v447(VarCurr)
    <=> ( ( v448(VarCurr,1)
        <=> $true )
        & ( v448(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_133,axiom,(
    ! [VarCurr: state_type] :
      ( v448(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_132,axiom,(
    ! [VarCurr: state_type] :
      ( v448(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_53,axiom,(
    ! [VarCurr: state_type] :
      ( v442(VarCurr)
    <=> ( v443(VarCurr)
        | v445(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_23,axiom,(
    ! [VarCurr: state_type] :
      ( v445(VarCurr)
    <=> ( ( v446(VarCurr,1)
        <=> $true )
        & ( v446(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_131,axiom,(
    ! [VarCurr: state_type] :
      ( v446(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_130,axiom,(
    ! [VarCurr: state_type] :
      ( v446(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_22,axiom,(
    ! [VarCurr: state_type] :
      ( v443(VarCurr)
    <=> ( ( v444(VarCurr,1)
        <=> $false )
        & ( v444(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_129,axiom,(
    ! [VarCurr: state_type] :
      ( v444(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_128,axiom,(
    ! [VarCurr: state_type] :
      ( v444(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_52,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v431(VarNext)
      <=> ( v433(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_25,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v433(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_127,axiom,(
    ! [VarCurr: state_type] :
      ( v422(VarCurr,49)
    <=> v427(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_12,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v424(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v427(VarCurr,B)
          <=> v426(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_12,axiom,(
    ! [VarCurr: state_type] :
      ( v424(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v427(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_126,axiom,(
    ! [VarCurr: state_type] :
      ( v426(VarCurr,49)
    <=> v94(VarCurr,329) ) )).

tff(addAssignment_125,axiom,(
    ! [VarCurr: state_type] :
      ( v424(VarCurr)
    <=> v103(VarCurr,3) ) )).

tff(addAssignment_124,axiom,(
    ! [VarCurr: state_type] :
      ( v415(VarCurr,49)
    <=> v420(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_11,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v417(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v420(VarCurr,B)
          <=> v419(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_11,axiom,(
    ! [VarCurr: state_type] :
      ( v417(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v420(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_123,axiom,(
    ! [VarCurr: state_type] :
      ( v419(VarCurr,49)
    <=> v94(VarCurr,399) ) )).

tff(addAssignment_122,axiom,(
    ! [VarCurr: state_type] :
      ( v417(VarCurr)
    <=> v103(VarCurr,3) ) )).

tff(addAssignment_121,axiom,(
    ! [VarNext: state_type] :
      ( v94(VarNext,329)
    <=> v383(VarNext,49) ) )).

tff(addCaseBooleanConditionShiftedRanges1_3,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v384(VarNext)
       => ( ( v383(VarNext,69)
          <=> v94(VarCurr,349) )
          & ( v383(VarNext,68)
          <=> v94(VarCurr,348) )
          & ( v383(VarNext,67)
          <=> v94(VarCurr,347) )
          & ( v383(VarNext,66)
          <=> v94(VarCurr,346) )
          & ( v383(VarNext,65)
          <=> v94(VarCurr,345) )
          & ( v383(VarNext,64)
          <=> v94(VarCurr,344) )
          & ( v383(VarNext,63)
          <=> v94(VarCurr,343) )
          & ( v383(VarNext,62)
          <=> v94(VarCurr,342) )
          & ( v383(VarNext,61)
          <=> v94(VarCurr,341) )
          & ( v383(VarNext,60)
          <=> v94(VarCurr,340) )
          & ( v383(VarNext,59)
          <=> v94(VarCurr,339) )
          & ( v383(VarNext,58)
          <=> v94(VarCurr,338) )
          & ( v383(VarNext,57)
          <=> v94(VarCurr,337) )
          & ( v383(VarNext,56)
          <=> v94(VarCurr,336) )
          & ( v383(VarNext,55)
          <=> v94(VarCurr,335) )
          & ( v383(VarNext,54)
          <=> v94(VarCurr,334) )
          & ( v383(VarNext,53)
          <=> v94(VarCurr,333) )
          & ( v383(VarNext,52)
          <=> v94(VarCurr,332) )
          & ( v383(VarNext,51)
          <=> v94(VarCurr,331) )
          & ( v383(VarNext,50)
          <=> v94(VarCurr,330) )
          & ( v383(VarNext,49)
          <=> v94(VarCurr,329) )
          & ( v383(VarNext,48)
          <=> v94(VarCurr,328) )
          & ( v383(VarNext,47)
          <=> v94(VarCurr,327) )
          & ( v383(VarNext,46)
          <=> v94(VarCurr,326) )
          & ( v383(VarNext,45)
          <=> v94(VarCurr,325) )
          & ( v383(VarNext,44)
          <=> v94(VarCurr,324) )
          & ( v383(VarNext,43)
          <=> v94(VarCurr,323) )
          & ( v383(VarNext,42)
          <=> v94(VarCurr,322) )
          & ( v383(VarNext,41)
          <=> v94(VarCurr,321) )
          & ( v383(VarNext,40)
          <=> v94(VarCurr,320) )
          & ( v383(VarNext,39)
          <=> v94(VarCurr,319) )
          & ( v383(VarNext,38)
          <=> v94(VarCurr,318) )
          & ( v383(VarNext,37)
          <=> v94(VarCurr,317) )
          & ( v383(VarNext,36)
          <=> v94(VarCurr,316) )
          & ( v383(VarNext,35)
          <=> v94(VarCurr,315) )
          & ( v383(VarNext,34)
          <=> v94(VarCurr,314) )
          & ( v383(VarNext,33)
          <=> v94(VarCurr,313) )
          & ( v383(VarNext,32)
          <=> v94(VarCurr,312) )
          & ( v383(VarNext,31)
          <=> v94(VarCurr,311) )
          & ( v383(VarNext,30)
          <=> v94(VarCurr,310) )
          & ( v383(VarNext,29)
          <=> v94(VarCurr,309) )
          & ( v383(VarNext,28)
          <=> v94(VarCurr,308) )
          & ( v383(VarNext,27)
          <=> v94(VarCurr,307) )
          & ( v383(VarNext,26)
          <=> v94(VarCurr,306) )
          & ( v383(VarNext,25)
          <=> v94(VarCurr,305) )
          & ( v383(VarNext,24)
          <=> v94(VarCurr,304) )
          & ( v383(VarNext,23)
          <=> v94(VarCurr,303) )
          & ( v383(VarNext,22)
          <=> v94(VarCurr,302) )
          & ( v383(VarNext,21)
          <=> v94(VarCurr,301) )
          & ( v383(VarNext,20)
          <=> v94(VarCurr,300) )
          & ( v383(VarNext,19)
          <=> v94(VarCurr,299) )
          & ( v383(VarNext,18)
          <=> v94(VarCurr,298) )
          & ( v383(VarNext,17)
          <=> v94(VarCurr,297) )
          & ( v383(VarNext,16)
          <=> v94(VarCurr,296) )
          & ( v383(VarNext,15)
          <=> v94(VarCurr,295) )
          & ( v383(VarNext,14)
          <=> v94(VarCurr,294) )
          & ( v383(VarNext,13)
          <=> v94(VarCurr,293) )
          & ( v383(VarNext,12)
          <=> v94(VarCurr,292) )
          & ( v383(VarNext,11)
          <=> v94(VarCurr,291) )
          & ( v383(VarNext,10)
          <=> v94(VarCurr,290) )
          & ( v383(VarNext,9)
          <=> v94(VarCurr,289) )
          & ( v383(VarNext,8)
          <=> v94(VarCurr,288) )
          & ( v383(VarNext,7)
          <=> v94(VarCurr,287) )
          & ( v383(VarNext,6)
          <=> v94(VarCurr,286) )
          & ( v383(VarNext,5)
          <=> v94(VarCurr,285) )
          & ( v383(VarNext,4)
          <=> v94(VarCurr,284) )
          & ( v383(VarNext,3)
          <=> v94(VarCurr,283) )
          & ( v383(VarNext,2)
          <=> v94(VarCurr,282) )
          & ( v383(VarNext,1)
          <=> v94(VarCurr,281) )
          & ( v383(VarNext,0)
          <=> v94(VarCurr,280) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_12,axiom,(
    ! [VarNext: state_type] :
      ( v384(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v383(VarNext,B)
          <=> v410(VarNext,B) ) ) ) )).

tff(addAssignment_120,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v410(VarNext,B)
          <=> v408(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_8,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v408(VarCurr,B)
          <=> v411(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_5,axiom,(
    ! [VarCurr: state_type] :
      ( v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v408(VarCurr,B)
          <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_4,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v397(VarCurr)
        & ~ v399(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v411(VarCurr,B)
          <=> v376(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_7,axiom,(
    ! [VarCurr: state_type] :
      ( v399(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v411(VarCurr,B)
          <=> v369(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionShiftedRanges0_2,axiom,(
    ! [VarCurr: state_type] :
      ( v397(VarCurr)
     => ( ( v411(VarCurr,69)
        <=> v94(VarCurr,279) )
        & ( v411(VarCurr,68)
        <=> v94(VarCurr,278) )
        & ( v411(VarCurr,67)
        <=> v94(VarCurr,277) )
        & ( v411(VarCurr,66)
        <=> v94(VarCurr,276) )
        & ( v411(VarCurr,65)
        <=> v94(VarCurr,275) )
        & ( v411(VarCurr,64)
        <=> v94(VarCurr,274) )
        & ( v411(VarCurr,63)
        <=> v94(VarCurr,273) )
        & ( v411(VarCurr,62)
        <=> v94(VarCurr,272) )
        & ( v411(VarCurr,61)
        <=> v94(VarCurr,271) )
        & ( v411(VarCurr,60)
        <=> v94(VarCurr,270) )
        & ( v411(VarCurr,59)
        <=> v94(VarCurr,269) )
        & ( v411(VarCurr,58)
        <=> v94(VarCurr,268) )
        & ( v411(VarCurr,57)
        <=> v94(VarCurr,267) )
        & ( v411(VarCurr,56)
        <=> v94(VarCurr,266) )
        & ( v411(VarCurr,55)
        <=> v94(VarCurr,265) )
        & ( v411(VarCurr,54)
        <=> v94(VarCurr,264) )
        & ( v411(VarCurr,53)
        <=> v94(VarCurr,263) )
        & ( v411(VarCurr,52)
        <=> v94(VarCurr,262) )
        & ( v411(VarCurr,51)
        <=> v94(VarCurr,261) )
        & ( v411(VarCurr,50)
        <=> v94(VarCurr,260) )
        & ( v411(VarCurr,49)
        <=> v94(VarCurr,259) )
        & ( v411(VarCurr,48)
        <=> v94(VarCurr,258) )
        & ( v411(VarCurr,47)
        <=> v94(VarCurr,257) )
        & ( v411(VarCurr,46)
        <=> v94(VarCurr,256) )
        & ( v411(VarCurr,45)
        <=> v94(VarCurr,255) )
        & ( v411(VarCurr,44)
        <=> v94(VarCurr,254) )
        & ( v411(VarCurr,43)
        <=> v94(VarCurr,253) )
        & ( v411(VarCurr,42)
        <=> v94(VarCurr,252) )
        & ( v411(VarCurr,41)
        <=> v94(VarCurr,251) )
        & ( v411(VarCurr,40)
        <=> v94(VarCurr,250) )
        & ( v411(VarCurr,39)
        <=> v94(VarCurr,249) )
        & ( v411(VarCurr,38)
        <=> v94(VarCurr,248) )
        & ( v411(VarCurr,37)
        <=> v94(VarCurr,247) )
        & ( v411(VarCurr,36)
        <=> v94(VarCurr,246) )
        & ( v411(VarCurr,35)
        <=> v94(VarCurr,245) )
        & ( v411(VarCurr,34)
        <=> v94(VarCurr,244) )
        & ( v411(VarCurr,33)
        <=> v94(VarCurr,243) )
        & ( v411(VarCurr,32)
        <=> v94(VarCurr,242) )
        & ( v411(VarCurr,31)
        <=> v94(VarCurr,241) )
        & ( v411(VarCurr,30)
        <=> v94(VarCurr,240) )
        & ( v411(VarCurr,29)
        <=> v94(VarCurr,239) )
        & ( v411(VarCurr,28)
        <=> v94(VarCurr,238) )
        & ( v411(VarCurr,27)
        <=> v94(VarCurr,237) )
        & ( v411(VarCurr,26)
        <=> v94(VarCurr,236) )
        & ( v411(VarCurr,25)
        <=> v94(VarCurr,235) )
        & ( v411(VarCurr,24)
        <=> v94(VarCurr,234) )
        & ( v411(VarCurr,23)
        <=> v94(VarCurr,233) )
        & ( v411(VarCurr,22)
        <=> v94(VarCurr,232) )
        & ( v411(VarCurr,21)
        <=> v94(VarCurr,231) )
        & ( v411(VarCurr,20)
        <=> v94(VarCurr,230) )
        & ( v411(VarCurr,19)
        <=> v94(VarCurr,229) )
        & ( v411(VarCurr,18)
        <=> v94(VarCurr,228) )
        & ( v411(VarCurr,17)
        <=> v94(VarCurr,227) )
        & ( v411(VarCurr,16)
        <=> v94(VarCurr,226) )
        & ( v411(VarCurr,15)
        <=> v94(VarCurr,225) )
        & ( v411(VarCurr,14)
        <=> v94(VarCurr,224) )
        & ( v411(VarCurr,13)
        <=> v94(VarCurr,223) )
        & ( v411(VarCurr,12)
        <=> v94(VarCurr,222) )
        & ( v411(VarCurr,11)
        <=> v94(VarCurr,221) )
        & ( v411(VarCurr,10)
        <=> v94(VarCurr,220) )
        & ( v411(VarCurr,9)
        <=> v94(VarCurr,219) )
        & ( v411(VarCurr,8)
        <=> v94(VarCurr,218) )
        & ( v411(VarCurr,7)
        <=> v94(VarCurr,217) )
        & ( v411(VarCurr,6)
        <=> v94(VarCurr,216) )
        & ( v411(VarCurr,5)
        <=> v94(VarCurr,215) )
        & ( v411(VarCurr,4)
        <=> v94(VarCurr,214) )
        & ( v411(VarCurr,3)
        <=> v94(VarCurr,213) )
        & ( v411(VarCurr,2)
        <=> v94(VarCurr,212) )
        & ( v411(VarCurr,1)
        <=> v94(VarCurr,211) )
        & ( v411(VarCurr,0)
        <=> v94(VarCurr,210) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_51,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v384(VarNext)
      <=> ( v385(VarNext)
          & v392(VarNext) ) ) ) )).

tff(addAssignment_119,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v392(VarNext)
      <=> v390(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_50,axiom,(
    ! [VarCurr: state_type] :
      ( v390(VarCurr)
    <=> ( v393(VarCurr)
        & v404(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_49,axiom,(
    ! [VarCurr: state_type] :
      ( v404(VarCurr)
    <=> ( v405(VarCurr)
        | v255(VarCurr) ) ) )).

tff(writeUnaryOperator_24,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v405(VarCurr)
    <=> v406(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_21,axiom,(
    ! [VarCurr: state_type] :
      ( v406(VarCurr)
    <=> ( ( v407(VarCurr,1)
        <=> $false )
        & ( v407(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_118,axiom,(
    ! [VarCurr: state_type] :
      ( v407(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_117,axiom,(
    ! [VarCurr: state_type] :
      ( v407(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_48,axiom,(
    ! [VarCurr: state_type] :
      ( v393(VarCurr)
    <=> ( v255(VarCurr)
        | v394(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_47,axiom,(
    ! [VarCurr: state_type] :
      ( v394(VarCurr)
    <=> ( v395(VarCurr)
        & v403(VarCurr) ) ) )).

tff(writeUnaryOperator_23,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v403(VarCurr)
    <=> v255(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_46,axiom,(
    ! [VarCurr: state_type] :
      ( v395(VarCurr)
    <=> ( v396(VarCurr)
        | v401(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_20,axiom,(
    ! [VarCurr: state_type] :
      ( v401(VarCurr)
    <=> ( ( v402(VarCurr,1)
        <=> $true )
        & ( v402(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_116,axiom,(
    ! [VarCurr: state_type] :
      ( v402(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_115,axiom,(
    ! [VarCurr: state_type] :
      ( v402(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_45,axiom,(
    ! [VarCurr: state_type] :
      ( v396(VarCurr)
    <=> ( v397(VarCurr)
        | v399(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_19,axiom,(
    ! [VarCurr: state_type] :
      ( v399(VarCurr)
    <=> ( ( v400(VarCurr,1)
        <=> $true )
        & ( v400(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_114,axiom,(
    ! [VarCurr: state_type] :
      ( v400(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_113,axiom,(
    ! [VarCurr: state_type] :
      ( v400(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_18,axiom,(
    ! [VarCurr: state_type] :
      ( v397(VarCurr)
    <=> ( ( v398(VarCurr,1)
        <=> $false )
        & ( v398(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_112,axiom,(
    ! [VarCurr: state_type] :
      ( v398(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_111,axiom,(
    ! [VarCurr: state_type] :
      ( v398(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_44,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v385(VarNext)
      <=> ( v387(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_22,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v387(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_110,axiom,(
    ! [VarCurr: state_type] :
      ( v376(VarCurr,49)
    <=> v381(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_10,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v378(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v381(VarCurr,B)
          <=> v380(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_10,axiom,(
    ! [VarCurr: state_type] :
      ( v378(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v381(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_109,axiom,(
    ! [VarCurr: state_type] :
      ( v380(VarCurr,49)
    <=> v94(VarCurr,259) ) )).

tff(addAssignment_108,axiom,(
    ! [VarCurr: state_type] :
      ( v378(VarCurr)
    <=> v103(VarCurr,4) ) )).

tff(addAssignment_107,axiom,(
    ! [VarCurr: state_type] :
      ( v369(VarCurr,49)
    <=> v374(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_9,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v371(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v374(VarCurr,B)
          <=> v373(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_9,axiom,(
    ! [VarCurr: state_type] :
      ( v371(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v374(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_106,axiom,(
    ! [VarCurr: state_type] :
      ( v373(VarCurr,49)
    <=> v94(VarCurr,329) ) )).

tff(addAssignment_105,axiom,(
    ! [VarCurr: state_type] :
      ( v371(VarCurr)
    <=> v103(VarCurr,4) ) )).

tff(addAssignment_104,axiom,(
    ! [VarNext: state_type] :
      ( v94(VarNext,259)
    <=> v337(VarNext,49) ) )).

tff(addCaseBooleanConditionShiftedRanges1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v338(VarNext)
       => ( ( v337(VarNext,69)
          <=> v94(VarCurr,279) )
          & ( v337(VarNext,68)
          <=> v94(VarCurr,278) )
          & ( v337(VarNext,67)
          <=> v94(VarCurr,277) )
          & ( v337(VarNext,66)
          <=> v94(VarCurr,276) )
          & ( v337(VarNext,65)
          <=> v94(VarCurr,275) )
          & ( v337(VarNext,64)
          <=> v94(VarCurr,274) )
          & ( v337(VarNext,63)
          <=> v94(VarCurr,273) )
          & ( v337(VarNext,62)
          <=> v94(VarCurr,272) )
          & ( v337(VarNext,61)
          <=> v94(VarCurr,271) )
          & ( v337(VarNext,60)
          <=> v94(VarCurr,270) )
          & ( v337(VarNext,59)
          <=> v94(VarCurr,269) )
          & ( v337(VarNext,58)
          <=> v94(VarCurr,268) )
          & ( v337(VarNext,57)
          <=> v94(VarCurr,267) )
          & ( v337(VarNext,56)
          <=> v94(VarCurr,266) )
          & ( v337(VarNext,55)
          <=> v94(VarCurr,265) )
          & ( v337(VarNext,54)
          <=> v94(VarCurr,264) )
          & ( v337(VarNext,53)
          <=> v94(VarCurr,263) )
          & ( v337(VarNext,52)
          <=> v94(VarCurr,262) )
          & ( v337(VarNext,51)
          <=> v94(VarCurr,261) )
          & ( v337(VarNext,50)
          <=> v94(VarCurr,260) )
          & ( v337(VarNext,49)
          <=> v94(VarCurr,259) )
          & ( v337(VarNext,48)
          <=> v94(VarCurr,258) )
          & ( v337(VarNext,47)
          <=> v94(VarCurr,257) )
          & ( v337(VarNext,46)
          <=> v94(VarCurr,256) )
          & ( v337(VarNext,45)
          <=> v94(VarCurr,255) )
          & ( v337(VarNext,44)
          <=> v94(VarCurr,254) )
          & ( v337(VarNext,43)
          <=> v94(VarCurr,253) )
          & ( v337(VarNext,42)
          <=> v94(VarCurr,252) )
          & ( v337(VarNext,41)
          <=> v94(VarCurr,251) )
          & ( v337(VarNext,40)
          <=> v94(VarCurr,250) )
          & ( v337(VarNext,39)
          <=> v94(VarCurr,249) )
          & ( v337(VarNext,38)
          <=> v94(VarCurr,248) )
          & ( v337(VarNext,37)
          <=> v94(VarCurr,247) )
          & ( v337(VarNext,36)
          <=> v94(VarCurr,246) )
          & ( v337(VarNext,35)
          <=> v94(VarCurr,245) )
          & ( v337(VarNext,34)
          <=> v94(VarCurr,244) )
          & ( v337(VarNext,33)
          <=> v94(VarCurr,243) )
          & ( v337(VarNext,32)
          <=> v94(VarCurr,242) )
          & ( v337(VarNext,31)
          <=> v94(VarCurr,241) )
          & ( v337(VarNext,30)
          <=> v94(VarCurr,240) )
          & ( v337(VarNext,29)
          <=> v94(VarCurr,239) )
          & ( v337(VarNext,28)
          <=> v94(VarCurr,238) )
          & ( v337(VarNext,27)
          <=> v94(VarCurr,237) )
          & ( v337(VarNext,26)
          <=> v94(VarCurr,236) )
          & ( v337(VarNext,25)
          <=> v94(VarCurr,235) )
          & ( v337(VarNext,24)
          <=> v94(VarCurr,234) )
          & ( v337(VarNext,23)
          <=> v94(VarCurr,233) )
          & ( v337(VarNext,22)
          <=> v94(VarCurr,232) )
          & ( v337(VarNext,21)
          <=> v94(VarCurr,231) )
          & ( v337(VarNext,20)
          <=> v94(VarCurr,230) )
          & ( v337(VarNext,19)
          <=> v94(VarCurr,229) )
          & ( v337(VarNext,18)
          <=> v94(VarCurr,228) )
          & ( v337(VarNext,17)
          <=> v94(VarCurr,227) )
          & ( v337(VarNext,16)
          <=> v94(VarCurr,226) )
          & ( v337(VarNext,15)
          <=> v94(VarCurr,225) )
          & ( v337(VarNext,14)
          <=> v94(VarCurr,224) )
          & ( v337(VarNext,13)
          <=> v94(VarCurr,223) )
          & ( v337(VarNext,12)
          <=> v94(VarCurr,222) )
          & ( v337(VarNext,11)
          <=> v94(VarCurr,221) )
          & ( v337(VarNext,10)
          <=> v94(VarCurr,220) )
          & ( v337(VarNext,9)
          <=> v94(VarCurr,219) )
          & ( v337(VarNext,8)
          <=> v94(VarCurr,218) )
          & ( v337(VarNext,7)
          <=> v94(VarCurr,217) )
          & ( v337(VarNext,6)
          <=> v94(VarCurr,216) )
          & ( v337(VarNext,5)
          <=> v94(VarCurr,215) )
          & ( v337(VarNext,4)
          <=> v94(VarCurr,214) )
          & ( v337(VarNext,3)
          <=> v94(VarCurr,213) )
          & ( v337(VarNext,2)
          <=> v94(VarCurr,212) )
          & ( v337(VarNext,1)
          <=> v94(VarCurr,211) )
          & ( v337(VarNext,0)
          <=> v94(VarCurr,210) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_11,axiom,(
    ! [VarNext: state_type] :
      ( v338(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v337(VarNext,B)
          <=> v364(VarNext,B) ) ) ) )).

tff(addAssignment_103,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v364(VarNext,B)
          <=> v362(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_6,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v362(VarCurr,B)
          <=> v365(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_4,axiom,(
    ! [VarCurr: state_type] :
      ( v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v362(VarCurr,B)
          <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_3,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v351(VarCurr)
        & ~ v353(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v365(VarCurr,B)
          <=> v330(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_5,axiom,(
    ! [VarCurr: state_type] :
      ( v353(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v365(VarCurr,B)
          <=> v323(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionShiftedRanges0_1,axiom,(
    ! [VarCurr: state_type] :
      ( v351(VarCurr)
     => ( ( v365(VarCurr,69)
        <=> v94(VarCurr,209) )
        & ( v365(VarCurr,68)
        <=> v94(VarCurr,208) )
        & ( v365(VarCurr,67)
        <=> v94(VarCurr,207) )
        & ( v365(VarCurr,66)
        <=> v94(VarCurr,206) )
        & ( v365(VarCurr,65)
        <=> v94(VarCurr,205) )
        & ( v365(VarCurr,64)
        <=> v94(VarCurr,204) )
        & ( v365(VarCurr,63)
        <=> v94(VarCurr,203) )
        & ( v365(VarCurr,62)
        <=> v94(VarCurr,202) )
        & ( v365(VarCurr,61)
        <=> v94(VarCurr,201) )
        & ( v365(VarCurr,60)
        <=> v94(VarCurr,200) )
        & ( v365(VarCurr,59)
        <=> v94(VarCurr,199) )
        & ( v365(VarCurr,58)
        <=> v94(VarCurr,198) )
        & ( v365(VarCurr,57)
        <=> v94(VarCurr,197) )
        & ( v365(VarCurr,56)
        <=> v94(VarCurr,196) )
        & ( v365(VarCurr,55)
        <=> v94(VarCurr,195) )
        & ( v365(VarCurr,54)
        <=> v94(VarCurr,194) )
        & ( v365(VarCurr,53)
        <=> v94(VarCurr,193) )
        & ( v365(VarCurr,52)
        <=> v94(VarCurr,192) )
        & ( v365(VarCurr,51)
        <=> v94(VarCurr,191) )
        & ( v365(VarCurr,50)
        <=> v94(VarCurr,190) )
        & ( v365(VarCurr,49)
        <=> v94(VarCurr,189) )
        & ( v365(VarCurr,48)
        <=> v94(VarCurr,188) )
        & ( v365(VarCurr,47)
        <=> v94(VarCurr,187) )
        & ( v365(VarCurr,46)
        <=> v94(VarCurr,186) )
        & ( v365(VarCurr,45)
        <=> v94(VarCurr,185) )
        & ( v365(VarCurr,44)
        <=> v94(VarCurr,184) )
        & ( v365(VarCurr,43)
        <=> v94(VarCurr,183) )
        & ( v365(VarCurr,42)
        <=> v94(VarCurr,182) )
        & ( v365(VarCurr,41)
        <=> v94(VarCurr,181) )
        & ( v365(VarCurr,40)
        <=> v94(VarCurr,180) )
        & ( v365(VarCurr,39)
        <=> v94(VarCurr,179) )
        & ( v365(VarCurr,38)
        <=> v94(VarCurr,178) )
        & ( v365(VarCurr,37)
        <=> v94(VarCurr,177) )
        & ( v365(VarCurr,36)
        <=> v94(VarCurr,176) )
        & ( v365(VarCurr,35)
        <=> v94(VarCurr,175) )
        & ( v365(VarCurr,34)
        <=> v94(VarCurr,174) )
        & ( v365(VarCurr,33)
        <=> v94(VarCurr,173) )
        & ( v365(VarCurr,32)
        <=> v94(VarCurr,172) )
        & ( v365(VarCurr,31)
        <=> v94(VarCurr,171) )
        & ( v365(VarCurr,30)
        <=> v94(VarCurr,170) )
        & ( v365(VarCurr,29)
        <=> v94(VarCurr,169) )
        & ( v365(VarCurr,28)
        <=> v94(VarCurr,168) )
        & ( v365(VarCurr,27)
        <=> v94(VarCurr,167) )
        & ( v365(VarCurr,26)
        <=> v94(VarCurr,166) )
        & ( v365(VarCurr,25)
        <=> v94(VarCurr,165) )
        & ( v365(VarCurr,24)
        <=> v94(VarCurr,164) )
        & ( v365(VarCurr,23)
        <=> v94(VarCurr,163) )
        & ( v365(VarCurr,22)
        <=> v94(VarCurr,162) )
        & ( v365(VarCurr,21)
        <=> v94(VarCurr,161) )
        & ( v365(VarCurr,20)
        <=> v94(VarCurr,160) )
        & ( v365(VarCurr,19)
        <=> v94(VarCurr,159) )
        & ( v365(VarCurr,18)
        <=> v94(VarCurr,158) )
        & ( v365(VarCurr,17)
        <=> v94(VarCurr,157) )
        & ( v365(VarCurr,16)
        <=> v94(VarCurr,156) )
        & ( v365(VarCurr,15)
        <=> v94(VarCurr,155) )
        & ( v365(VarCurr,14)
        <=> v94(VarCurr,154) )
        & ( v365(VarCurr,13)
        <=> v94(VarCurr,153) )
        & ( v365(VarCurr,12)
        <=> v94(VarCurr,152) )
        & ( v365(VarCurr,11)
        <=> v94(VarCurr,151) )
        & ( v365(VarCurr,10)
        <=> v94(VarCurr,150) )
        & ( v365(VarCurr,9)
        <=> v94(VarCurr,149) )
        & ( v365(VarCurr,8)
        <=> v94(VarCurr,148) )
        & ( v365(VarCurr,7)
        <=> v94(VarCurr,147) )
        & ( v365(VarCurr,6)
        <=> v94(VarCurr,146) )
        & ( v365(VarCurr,5)
        <=> v94(VarCurr,145) )
        & ( v365(VarCurr,4)
        <=> v94(VarCurr,144) )
        & ( v365(VarCurr,3)
        <=> v94(VarCurr,143) )
        & ( v365(VarCurr,2)
        <=> v94(VarCurr,142) )
        & ( v365(VarCurr,1)
        <=> v94(VarCurr,141) )
        & ( v365(VarCurr,0)
        <=> v94(VarCurr,140) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_43,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v338(VarNext)
      <=> ( v339(VarNext)
          & v346(VarNext) ) ) ) )).

tff(addAssignment_102,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v346(VarNext)
      <=> v344(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_42,axiom,(
    ! [VarCurr: state_type] :
      ( v344(VarCurr)
    <=> ( v347(VarCurr)
        & v358(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_41,axiom,(
    ! [VarCurr: state_type] :
      ( v358(VarCurr)
    <=> ( v359(VarCurr)
        | v255(VarCurr) ) ) )).

tff(writeUnaryOperator_21,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v359(VarCurr)
    <=> v360(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_17,axiom,(
    ! [VarCurr: state_type] :
      ( v360(VarCurr)
    <=> ( ( v361(VarCurr,1)
        <=> $false )
        & ( v361(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_101,axiom,(
    ! [VarCurr: state_type] :
      ( v361(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_100,axiom,(
    ! [VarCurr: state_type] :
      ( v361(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_40,axiom,(
    ! [VarCurr: state_type] :
      ( v347(VarCurr)
    <=> ( v255(VarCurr)
        | v348(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_39,axiom,(
    ! [VarCurr: state_type] :
      ( v348(VarCurr)
    <=> ( v349(VarCurr)
        & v357(VarCurr) ) ) )).

tff(writeUnaryOperator_20,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v357(VarCurr)
    <=> v255(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_38,axiom,(
    ! [VarCurr: state_type] :
      ( v349(VarCurr)
    <=> ( v350(VarCurr)
        | v355(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_16,axiom,(
    ! [VarCurr: state_type] :
      ( v355(VarCurr)
    <=> ( ( v356(VarCurr,1)
        <=> $true )
        & ( v356(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_99,axiom,(
    ! [VarCurr: state_type] :
      ( v356(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_98,axiom,(
    ! [VarCurr: state_type] :
      ( v356(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_37,axiom,(
    ! [VarCurr: state_type] :
      ( v350(VarCurr)
    <=> ( v351(VarCurr)
        | v353(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_15,axiom,(
    ! [VarCurr: state_type] :
      ( v353(VarCurr)
    <=> ( ( v354(VarCurr,1)
        <=> $true )
        & ( v354(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_97,axiom,(
    ! [VarCurr: state_type] :
      ( v354(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_96,axiom,(
    ! [VarCurr: state_type] :
      ( v354(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_14,axiom,(
    ! [VarCurr: state_type] :
      ( v351(VarCurr)
    <=> ( ( v352(VarCurr,1)
        <=> $false )
        & ( v352(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_95,axiom,(
    ! [VarCurr: state_type] :
      ( v352(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_94,axiom,(
    ! [VarCurr: state_type] :
      ( v352(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_36,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v339(VarNext)
      <=> ( v341(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_19,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v341(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_93,axiom,(
    ! [VarCurr: state_type] :
      ( v330(VarCurr,49)
    <=> v335(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_8,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v332(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v335(VarCurr,B)
          <=> v334(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_8,axiom,(
    ! [VarCurr: state_type] :
      ( v332(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v335(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_92,axiom,(
    ! [VarCurr: state_type] :
      ( v334(VarCurr,49)
    <=> v94(VarCurr,189) ) )).

tff(addAssignment_91,axiom,(
    ! [VarCurr: state_type] :
      ( v332(VarCurr)
    <=> v103(VarCurr,5) ) )).

tff(addAssignment_90,axiom,(
    ! [VarCurr: state_type] :
      ( v323(VarCurr,49)
    <=> v328(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_7,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v325(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v328(VarCurr,B)
          <=> v327(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_7,axiom,(
    ! [VarCurr: state_type] :
      ( v325(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v328(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_89,axiom,(
    ! [VarCurr: state_type] :
      ( v327(VarCurr,49)
    <=> v94(VarCurr,259) ) )).

tff(addAssignment_88,axiom,(
    ! [VarCurr: state_type] :
      ( v325(VarCurr)
    <=> v103(VarCurr,5) ) )).

tff(addAssignment_87,axiom,(
    ! [VarNext: state_type] :
      ( v94(VarNext,189)
    <=> v291(VarNext,49) ) )).

tff(addCaseBooleanConditionShiftedRanges1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v292(VarNext)
       => ( ( v291(VarNext,69)
          <=> v94(VarCurr,209) )
          & ( v291(VarNext,68)
          <=> v94(VarCurr,208) )
          & ( v291(VarNext,67)
          <=> v94(VarCurr,207) )
          & ( v291(VarNext,66)
          <=> v94(VarCurr,206) )
          & ( v291(VarNext,65)
          <=> v94(VarCurr,205) )
          & ( v291(VarNext,64)
          <=> v94(VarCurr,204) )
          & ( v291(VarNext,63)
          <=> v94(VarCurr,203) )
          & ( v291(VarNext,62)
          <=> v94(VarCurr,202) )
          & ( v291(VarNext,61)
          <=> v94(VarCurr,201) )
          & ( v291(VarNext,60)
          <=> v94(VarCurr,200) )
          & ( v291(VarNext,59)
          <=> v94(VarCurr,199) )
          & ( v291(VarNext,58)
          <=> v94(VarCurr,198) )
          & ( v291(VarNext,57)
          <=> v94(VarCurr,197) )
          & ( v291(VarNext,56)
          <=> v94(VarCurr,196) )
          & ( v291(VarNext,55)
          <=> v94(VarCurr,195) )
          & ( v291(VarNext,54)
          <=> v94(VarCurr,194) )
          & ( v291(VarNext,53)
          <=> v94(VarCurr,193) )
          & ( v291(VarNext,52)
          <=> v94(VarCurr,192) )
          & ( v291(VarNext,51)
          <=> v94(VarCurr,191) )
          & ( v291(VarNext,50)
          <=> v94(VarCurr,190) )
          & ( v291(VarNext,49)
          <=> v94(VarCurr,189) )
          & ( v291(VarNext,48)
          <=> v94(VarCurr,188) )
          & ( v291(VarNext,47)
          <=> v94(VarCurr,187) )
          & ( v291(VarNext,46)
          <=> v94(VarCurr,186) )
          & ( v291(VarNext,45)
          <=> v94(VarCurr,185) )
          & ( v291(VarNext,44)
          <=> v94(VarCurr,184) )
          & ( v291(VarNext,43)
          <=> v94(VarCurr,183) )
          & ( v291(VarNext,42)
          <=> v94(VarCurr,182) )
          & ( v291(VarNext,41)
          <=> v94(VarCurr,181) )
          & ( v291(VarNext,40)
          <=> v94(VarCurr,180) )
          & ( v291(VarNext,39)
          <=> v94(VarCurr,179) )
          & ( v291(VarNext,38)
          <=> v94(VarCurr,178) )
          & ( v291(VarNext,37)
          <=> v94(VarCurr,177) )
          & ( v291(VarNext,36)
          <=> v94(VarCurr,176) )
          & ( v291(VarNext,35)
          <=> v94(VarCurr,175) )
          & ( v291(VarNext,34)
          <=> v94(VarCurr,174) )
          & ( v291(VarNext,33)
          <=> v94(VarCurr,173) )
          & ( v291(VarNext,32)
          <=> v94(VarCurr,172) )
          & ( v291(VarNext,31)
          <=> v94(VarCurr,171) )
          & ( v291(VarNext,30)
          <=> v94(VarCurr,170) )
          & ( v291(VarNext,29)
          <=> v94(VarCurr,169) )
          & ( v291(VarNext,28)
          <=> v94(VarCurr,168) )
          & ( v291(VarNext,27)
          <=> v94(VarCurr,167) )
          & ( v291(VarNext,26)
          <=> v94(VarCurr,166) )
          & ( v291(VarNext,25)
          <=> v94(VarCurr,165) )
          & ( v291(VarNext,24)
          <=> v94(VarCurr,164) )
          & ( v291(VarNext,23)
          <=> v94(VarCurr,163) )
          & ( v291(VarNext,22)
          <=> v94(VarCurr,162) )
          & ( v291(VarNext,21)
          <=> v94(VarCurr,161) )
          & ( v291(VarNext,20)
          <=> v94(VarCurr,160) )
          & ( v291(VarNext,19)
          <=> v94(VarCurr,159) )
          & ( v291(VarNext,18)
          <=> v94(VarCurr,158) )
          & ( v291(VarNext,17)
          <=> v94(VarCurr,157) )
          & ( v291(VarNext,16)
          <=> v94(VarCurr,156) )
          & ( v291(VarNext,15)
          <=> v94(VarCurr,155) )
          & ( v291(VarNext,14)
          <=> v94(VarCurr,154) )
          & ( v291(VarNext,13)
          <=> v94(VarCurr,153) )
          & ( v291(VarNext,12)
          <=> v94(VarCurr,152) )
          & ( v291(VarNext,11)
          <=> v94(VarCurr,151) )
          & ( v291(VarNext,10)
          <=> v94(VarCurr,150) )
          & ( v291(VarNext,9)
          <=> v94(VarCurr,149) )
          & ( v291(VarNext,8)
          <=> v94(VarCurr,148) )
          & ( v291(VarNext,7)
          <=> v94(VarCurr,147) )
          & ( v291(VarNext,6)
          <=> v94(VarCurr,146) )
          & ( v291(VarNext,5)
          <=> v94(VarCurr,145) )
          & ( v291(VarNext,4)
          <=> v94(VarCurr,144) )
          & ( v291(VarNext,3)
          <=> v94(VarCurr,143) )
          & ( v291(VarNext,2)
          <=> v94(VarCurr,142) )
          & ( v291(VarNext,1)
          <=> v94(VarCurr,141) )
          & ( v291(VarNext,0)
          <=> v94(VarCurr,140) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_10,axiom,(
    ! [VarNext: state_type] :
      ( v292(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v291(VarNext,B)
          <=> v318(VarNext,B) ) ) ) )).

tff(addAssignment_86,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v318(VarNext,B)
          <=> v316(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v316(VarCurr,B)
          <=> v319(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_3,axiom,(
    ! [VarCurr: state_type] :
      ( v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v316(VarCurr,B)
          <=> $false ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2_2,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v305(VarCurr)
        & ~ v307(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v319(VarCurr,B)
          <=> v284(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_3,axiom,(
    ! [VarCurr: state_type] :
      ( v307(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v319(VarCurr,B)
          <=> v277(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionShiftedRanges0,axiom,(
    ! [VarCurr: state_type] :
      ( v305(VarCurr)
     => ( ( v319(VarCurr,69)
        <=> v94(VarCurr,139) )
        & ( v319(VarCurr,68)
        <=> v94(VarCurr,138) )
        & ( v319(VarCurr,67)
        <=> v94(VarCurr,137) )
        & ( v319(VarCurr,66)
        <=> v94(VarCurr,136) )
        & ( v319(VarCurr,65)
        <=> v94(VarCurr,135) )
        & ( v319(VarCurr,64)
        <=> v94(VarCurr,134) )
        & ( v319(VarCurr,63)
        <=> v94(VarCurr,133) )
        & ( v319(VarCurr,62)
        <=> v94(VarCurr,132) )
        & ( v319(VarCurr,61)
        <=> v94(VarCurr,131) )
        & ( v319(VarCurr,60)
        <=> v94(VarCurr,130) )
        & ( v319(VarCurr,59)
        <=> v94(VarCurr,129) )
        & ( v319(VarCurr,58)
        <=> v94(VarCurr,128) )
        & ( v319(VarCurr,57)
        <=> v94(VarCurr,127) )
        & ( v319(VarCurr,56)
        <=> v94(VarCurr,126) )
        & ( v319(VarCurr,55)
        <=> v94(VarCurr,125) )
        & ( v319(VarCurr,54)
        <=> v94(VarCurr,124) )
        & ( v319(VarCurr,53)
        <=> v94(VarCurr,123) )
        & ( v319(VarCurr,52)
        <=> v94(VarCurr,122) )
        & ( v319(VarCurr,51)
        <=> v94(VarCurr,121) )
        & ( v319(VarCurr,50)
        <=> v94(VarCurr,120) )
        & ( v319(VarCurr,49)
        <=> v94(VarCurr,119) )
        & ( v319(VarCurr,48)
        <=> v94(VarCurr,118) )
        & ( v319(VarCurr,47)
        <=> v94(VarCurr,117) )
        & ( v319(VarCurr,46)
        <=> v94(VarCurr,116) )
        & ( v319(VarCurr,45)
        <=> v94(VarCurr,115) )
        & ( v319(VarCurr,44)
        <=> v94(VarCurr,114) )
        & ( v319(VarCurr,43)
        <=> v94(VarCurr,113) )
        & ( v319(VarCurr,42)
        <=> v94(VarCurr,112) )
        & ( v319(VarCurr,41)
        <=> v94(VarCurr,111) )
        & ( v319(VarCurr,40)
        <=> v94(VarCurr,110) )
        & ( v319(VarCurr,39)
        <=> v94(VarCurr,109) )
        & ( v319(VarCurr,38)
        <=> v94(VarCurr,108) )
        & ( v319(VarCurr,37)
        <=> v94(VarCurr,107) )
        & ( v319(VarCurr,36)
        <=> v94(VarCurr,106) )
        & ( v319(VarCurr,35)
        <=> v94(VarCurr,105) )
        & ( v319(VarCurr,34)
        <=> v94(VarCurr,104) )
        & ( v319(VarCurr,33)
        <=> v94(VarCurr,103) )
        & ( v319(VarCurr,32)
        <=> v94(VarCurr,102) )
        & ( v319(VarCurr,31)
        <=> v94(VarCurr,101) )
        & ( v319(VarCurr,30)
        <=> v94(VarCurr,100) )
        & ( v319(VarCurr,29)
        <=> v94(VarCurr,99) )
        & ( v319(VarCurr,28)
        <=> v94(VarCurr,98) )
        & ( v319(VarCurr,27)
        <=> v94(VarCurr,97) )
        & ( v319(VarCurr,26)
        <=> v94(VarCurr,96) )
        & ( v319(VarCurr,25)
        <=> v94(VarCurr,95) )
        & ( v319(VarCurr,24)
        <=> v94(VarCurr,94) )
        & ( v319(VarCurr,23)
        <=> v94(VarCurr,93) )
        & ( v319(VarCurr,22)
        <=> v94(VarCurr,92) )
        & ( v319(VarCurr,21)
        <=> v94(VarCurr,91) )
        & ( v319(VarCurr,20)
        <=> v94(VarCurr,90) )
        & ( v319(VarCurr,19)
        <=> v94(VarCurr,89) )
        & ( v319(VarCurr,18)
        <=> v94(VarCurr,88) )
        & ( v319(VarCurr,17)
        <=> v94(VarCurr,87) )
        & ( v319(VarCurr,16)
        <=> v94(VarCurr,86) )
        & ( v319(VarCurr,15)
        <=> v94(VarCurr,85) )
        & ( v319(VarCurr,14)
        <=> v94(VarCurr,84) )
        & ( v319(VarCurr,13)
        <=> v94(VarCurr,83) )
        & ( v319(VarCurr,12)
        <=> v94(VarCurr,82) )
        & ( v319(VarCurr,11)
        <=> v94(VarCurr,81) )
        & ( v319(VarCurr,10)
        <=> v94(VarCurr,80) )
        & ( v319(VarCurr,9)
        <=> v94(VarCurr,79) )
        & ( v319(VarCurr,8)
        <=> v94(VarCurr,78) )
        & ( v319(VarCurr,7)
        <=> v94(VarCurr,77) )
        & ( v319(VarCurr,6)
        <=> v94(VarCurr,76) )
        & ( v319(VarCurr,5)
        <=> v94(VarCurr,75) )
        & ( v319(VarCurr,4)
        <=> v94(VarCurr,74) )
        & ( v319(VarCurr,3)
        <=> v94(VarCurr,73) )
        & ( v319(VarCurr,2)
        <=> v94(VarCurr,72) )
        & ( v319(VarCurr,1)
        <=> v94(VarCurr,71) )
        & ( v319(VarCurr,0)
        <=> v94(VarCurr,70) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_35,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v292(VarNext)
      <=> ( v293(VarNext)
          & v300(VarNext) ) ) ) )).

tff(addAssignment_85,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v300(VarNext)
      <=> v298(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_34,axiom,(
    ! [VarCurr: state_type] :
      ( v298(VarCurr)
    <=> ( v301(VarCurr)
        & v312(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_33,axiom,(
    ! [VarCurr: state_type] :
      ( v312(VarCurr)
    <=> ( v313(VarCurr)
        | v255(VarCurr) ) ) )).

tff(writeUnaryOperator_18,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v313(VarCurr)
    <=> v314(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_13,axiom,(
    ! [VarCurr: state_type] :
      ( v314(VarCurr)
    <=> ( ( v315(VarCurr,1)
        <=> $false )
        & ( v315(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_84,axiom,(
    ! [VarCurr: state_type] :
      ( v315(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_83,axiom,(
    ! [VarCurr: state_type] :
      ( v315(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_32,axiom,(
    ! [VarCurr: state_type] :
      ( v301(VarCurr)
    <=> ( v255(VarCurr)
        | v302(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_31,axiom,(
    ! [VarCurr: state_type] :
      ( v302(VarCurr)
    <=> ( v303(VarCurr)
        & v311(VarCurr) ) ) )).

tff(writeUnaryOperator_17,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v311(VarCurr)
    <=> v255(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_30,axiom,(
    ! [VarCurr: state_type] :
      ( v303(VarCurr)
    <=> ( v304(VarCurr)
        | v309(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_12,axiom,(
    ! [VarCurr: state_type] :
      ( v309(VarCurr)
    <=> ( ( v310(VarCurr,1)
        <=> $true )
        & ( v310(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_82,axiom,(
    ! [VarCurr: state_type] :
      ( v310(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_81,axiom,(
    ! [VarCurr: state_type] :
      ( v310(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_29,axiom,(
    ! [VarCurr: state_type] :
      ( v304(VarCurr)
    <=> ( v305(VarCurr)
        | v307(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_11,axiom,(
    ! [VarCurr: state_type] :
      ( v307(VarCurr)
    <=> ( ( v308(VarCurr,1)
        <=> $true )
        & ( v308(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_80,axiom,(
    ! [VarCurr: state_type] :
      ( v308(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_79,axiom,(
    ! [VarCurr: state_type] :
      ( v308(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_10,axiom,(
    ! [VarCurr: state_type] :
      ( v305(VarCurr)
    <=> ( ( v306(VarCurr,1)
        <=> $false )
        & ( v306(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_78,axiom,(
    ! [VarCurr: state_type] :
      ( v306(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_77,axiom,(
    ! [VarCurr: state_type] :
      ( v306(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_28,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v293(VarNext)
      <=> ( v295(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_16,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v295(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_76,axiom,(
    ! [VarCurr: state_type] :
      ( v284(VarCurr,49)
    <=> v289(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_6,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v286(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v289(VarCurr,B)
          <=> v288(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_6,axiom,(
    ! [VarCurr: state_type] :
      ( v286(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v289(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_75,axiom,(
    ! [VarCurr: state_type] :
      ( v288(VarCurr,49)
    <=> v94(VarCurr,119) ) )).

tff(addAssignment_74,axiom,(
    ! [VarCurr: state_type] :
      ( v286(VarCurr)
    <=> v103(VarCurr,6) ) )).

tff(addAssignment_73,axiom,(
    ! [VarCurr: state_type] :
      ( v277(VarCurr,49)
    <=> v282(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_5,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v279(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v282(VarCurr,B)
          <=> v281(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_5,axiom,(
    ! [VarCurr: state_type] :
      ( v279(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v282(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_72,axiom,(
    ! [VarCurr: state_type] :
      ( v281(VarCurr,49)
    <=> v94(VarCurr,189) ) )).

tff(addAssignment_71,axiom,(
    ! [VarCurr: state_type] :
      ( v279(VarCurr)
    <=> v103(VarCurr,6) ) )).

tff(addAssignment_70,axiom,(
    ! [VarNext: state_type] :
      ( v94(VarNext,119)
    <=> v244(VarNext,49) ) )).

tff(addCaseBooleanConditionShiftedRanges1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v245(VarNext)
       => ( ( v244(VarNext,69)
          <=> v94(VarCurr,139) )
          & ( v244(VarNext,68)
          <=> v94(VarCurr,138) )
          & ( v244(VarNext,67)
          <=> v94(VarCurr,137) )
          & ( v244(VarNext,66)
          <=> v94(VarCurr,136) )
          & ( v244(VarNext,65)
          <=> v94(VarCurr,135) )
          & ( v244(VarNext,64)
          <=> v94(VarCurr,134) )
          & ( v244(VarNext,63)
          <=> v94(VarCurr,133) )
          & ( v244(VarNext,62)
          <=> v94(VarCurr,132) )
          & ( v244(VarNext,61)
          <=> v94(VarCurr,131) )
          & ( v244(VarNext,60)
          <=> v94(VarCurr,130) )
          & ( v244(VarNext,59)
          <=> v94(VarCurr,129) )
          & ( v244(VarNext,58)
          <=> v94(VarCurr,128) )
          & ( v244(VarNext,57)
          <=> v94(VarCurr,127) )
          & ( v244(VarNext,56)
          <=> v94(VarCurr,126) )
          & ( v244(VarNext,55)
          <=> v94(VarCurr,125) )
          & ( v244(VarNext,54)
          <=> v94(VarCurr,124) )
          & ( v244(VarNext,53)
          <=> v94(VarCurr,123) )
          & ( v244(VarNext,52)
          <=> v94(VarCurr,122) )
          & ( v244(VarNext,51)
          <=> v94(VarCurr,121) )
          & ( v244(VarNext,50)
          <=> v94(VarCurr,120) )
          & ( v244(VarNext,49)
          <=> v94(VarCurr,119) )
          & ( v244(VarNext,48)
          <=> v94(VarCurr,118) )
          & ( v244(VarNext,47)
          <=> v94(VarCurr,117) )
          & ( v244(VarNext,46)
          <=> v94(VarCurr,116) )
          & ( v244(VarNext,45)
          <=> v94(VarCurr,115) )
          & ( v244(VarNext,44)
          <=> v94(VarCurr,114) )
          & ( v244(VarNext,43)
          <=> v94(VarCurr,113) )
          & ( v244(VarNext,42)
          <=> v94(VarCurr,112) )
          & ( v244(VarNext,41)
          <=> v94(VarCurr,111) )
          & ( v244(VarNext,40)
          <=> v94(VarCurr,110) )
          & ( v244(VarNext,39)
          <=> v94(VarCurr,109) )
          & ( v244(VarNext,38)
          <=> v94(VarCurr,108) )
          & ( v244(VarNext,37)
          <=> v94(VarCurr,107) )
          & ( v244(VarNext,36)
          <=> v94(VarCurr,106) )
          & ( v244(VarNext,35)
          <=> v94(VarCurr,105) )
          & ( v244(VarNext,34)
          <=> v94(VarCurr,104) )
          & ( v244(VarNext,33)
          <=> v94(VarCurr,103) )
          & ( v244(VarNext,32)
          <=> v94(VarCurr,102) )
          & ( v244(VarNext,31)
          <=> v94(VarCurr,101) )
          & ( v244(VarNext,30)
          <=> v94(VarCurr,100) )
          & ( v244(VarNext,29)
          <=> v94(VarCurr,99) )
          & ( v244(VarNext,28)
          <=> v94(VarCurr,98) )
          & ( v244(VarNext,27)
          <=> v94(VarCurr,97) )
          & ( v244(VarNext,26)
          <=> v94(VarCurr,96) )
          & ( v244(VarNext,25)
          <=> v94(VarCurr,95) )
          & ( v244(VarNext,24)
          <=> v94(VarCurr,94) )
          & ( v244(VarNext,23)
          <=> v94(VarCurr,93) )
          & ( v244(VarNext,22)
          <=> v94(VarCurr,92) )
          & ( v244(VarNext,21)
          <=> v94(VarCurr,91) )
          & ( v244(VarNext,20)
          <=> v94(VarCurr,90) )
          & ( v244(VarNext,19)
          <=> v94(VarCurr,89) )
          & ( v244(VarNext,18)
          <=> v94(VarCurr,88) )
          & ( v244(VarNext,17)
          <=> v94(VarCurr,87) )
          & ( v244(VarNext,16)
          <=> v94(VarCurr,86) )
          & ( v244(VarNext,15)
          <=> v94(VarCurr,85) )
          & ( v244(VarNext,14)
          <=> v94(VarCurr,84) )
          & ( v244(VarNext,13)
          <=> v94(VarCurr,83) )
          & ( v244(VarNext,12)
          <=> v94(VarCurr,82) )
          & ( v244(VarNext,11)
          <=> v94(VarCurr,81) )
          & ( v244(VarNext,10)
          <=> v94(VarCurr,80) )
          & ( v244(VarNext,9)
          <=> v94(VarCurr,79) )
          & ( v244(VarNext,8)
          <=> v94(VarCurr,78) )
          & ( v244(VarNext,7)
          <=> v94(VarCurr,77) )
          & ( v244(VarNext,6)
          <=> v94(VarCurr,76) )
          & ( v244(VarNext,5)
          <=> v94(VarCurr,75) )
          & ( v244(VarNext,4)
          <=> v94(VarCurr,74) )
          & ( v244(VarNext,3)
          <=> v94(VarCurr,73) )
          & ( v244(VarNext,2)
          <=> v94(VarCurr,72) )
          & ( v244(VarNext,1)
          <=> v94(VarCurr,71) )
          & ( v244(VarNext,0)
          <=> v94(VarCurr,70) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_9,axiom,(
    ! [VarNext: state_type] :
      ( v245(VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v244(VarNext,B)
          <=> v272(VarNext,B) ) ) ) )).

tff(addAssignment_69,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v272(VarNext,B)
          <=> v270(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v270(VarCurr,B)
          <=> v273(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_2,axiom,(
    ! [VarCurr: state_type] :
      ( v255(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v270(VarCurr,B)
          <=> $false ) ) ) )).

tff(bitBlastConstant_152,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(69) )).

tff(bitBlastConstant_151,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(68) )).

tff(bitBlastConstant_150,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(67) )).

tff(bitBlastConstant_149,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(66) )).

tff(bitBlastConstant_148,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(65) )).

tff(bitBlastConstant_147,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(64) )).

tff(bitBlastConstant_146,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(63) )).

tff(bitBlastConstant_145,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(62) )).

tff(bitBlastConstant_144,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(61) )).

tff(bitBlastConstant_143,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(60) )).

tff(bitBlastConstant_142,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(59) )).

tff(bitBlastConstant_141,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(58) )).

tff(bitBlastConstant_140,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(57) )).

tff(bitBlastConstant_139,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(56) )).

tff(bitBlastConstant_138,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(55) )).

tff(bitBlastConstant_137,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(54) )).

tff(bitBlastConstant_136,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(53) )).

tff(bitBlastConstant_135,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(52) )).

tff(bitBlastConstant_134,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(51) )).

tff(bitBlastConstant_133,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(50) )).

tff(bitBlastConstant_132,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(49) )).

tff(bitBlastConstant_131,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(48) )).

tff(bitBlastConstant_130,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(47) )).

tff(bitBlastConstant_129,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(46) )).

tff(bitBlastConstant_128,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(45) )).

tff(bitBlastConstant_127,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(44) )).

tff(bitBlastConstant_126,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(43) )).

tff(bitBlastConstant_125,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(42) )).

tff(bitBlastConstant_124,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(41) )).

tff(bitBlastConstant_123,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(40) )).

tff(bitBlastConstant_122,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(39) )).

tff(bitBlastConstant_121,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(38) )).

tff(bitBlastConstant_120,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(37) )).

tff(bitBlastConstant_119,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(36) )).

tff(bitBlastConstant_118,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(35) )).

tff(bitBlastConstant_117,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(34) )).

tff(bitBlastConstant_116,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(33) )).

tff(bitBlastConstant_115,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(32) )).

tff(bitBlastConstant_114,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(31) )).

tff(bitBlastConstant_113,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(30) )).

tff(bitBlastConstant_112,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(29) )).

tff(bitBlastConstant_111,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(28) )).

tff(bitBlastConstant_110,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(27) )).

tff(bitBlastConstant_109,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(26) )).

tff(bitBlastConstant_108,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(25) )).

tff(bitBlastConstant_107,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(24) )).

tff(bitBlastConstant_106,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(23) )).

tff(bitBlastConstant_105,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(22) )).

tff(bitBlastConstant_104,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(21) )).

tff(bitBlastConstant_103,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(20) )).

tff(bitBlastConstant_102,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(19) )).

tff(bitBlastConstant_101,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(18) )).

tff(bitBlastConstant_100,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(17) )).

tff(bitBlastConstant_99,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(16) )).

tff(bitBlastConstant_98,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(15) )).

tff(bitBlastConstant_97,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(14) )).

tff(bitBlastConstant_96,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(13) )).

tff(bitBlastConstant_95,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(12) )).

tff(bitBlastConstant_94,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(11) )).

tff(bitBlastConstant_93,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(10) )).

tff(bitBlastConstant_92,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(9) )).

tff(bitBlastConstant_91,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(8) )).

tff(bitBlastConstant_90,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(7) )).

tff(bitBlastConstant_89,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(6) )).

tff(bitBlastConstant_88,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(5) )).

tff(bitBlastConstant_87,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(4) )).

tff(bitBlastConstant_86,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(3) )).

tff(bitBlastConstant_85,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(2) )).

tff(bitBlastConstant_84,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(1) )).

tff(bitBlastConstant_83,axiom,(
    ~ b0000000000000000000000000000000000000000000000000000000000000000000000(0) )).

tff(addParallelCaseBooleanConditionEqualRanges2_1,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v259(VarCurr)
        & ~ v261(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v273(VarCurr,B)
          <=> v237(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1_1,axiom,(
    ! [VarCurr: state_type] :
      ( v261(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v273(VarCurr,B)
          <=> v99(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0_1,axiom,(
    ! [VarCurr: state_type] :
      ( v259(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v273(VarCurr,B)
          <=> v94(VarCurr,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_27,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v245(VarNext)
      <=> ( v246(VarNext)
          & v253(VarNext) ) ) ) )).

tff(addAssignment_68,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v253(VarNext)
      <=> v251(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_26,axiom,(
    ! [VarCurr: state_type] :
      ( v251(VarCurr)
    <=> ( v254(VarCurr)
        & v266(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_25,axiom,(
    ! [VarCurr: state_type] :
      ( v266(VarCurr)
    <=> ( v267(VarCurr)
        | v255(VarCurr) ) ) )).

tff(writeUnaryOperator_15,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v267(VarCurr)
    <=> v268(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_9,axiom,(
    ! [VarCurr: state_type] :
      ( v268(VarCurr)
    <=> ( ( v269(VarCurr,1)
        <=> $false )
        & ( v269(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_67,axiom,(
    ! [VarCurr: state_type] :
      ( v269(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_66,axiom,(
    ! [VarCurr: state_type] :
      ( v269(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_24,axiom,(
    ! [VarCurr: state_type] :
      ( v254(VarCurr)
    <=> ( v255(VarCurr)
        | v256(VarCurr) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_23,axiom,(
    ! [VarCurr: state_type] :
      ( v256(VarCurr)
    <=> ( v257(VarCurr)
        & v265(VarCurr) ) ) )).

tff(writeUnaryOperator_14,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v265(VarCurr)
    <=> v255(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_22,axiom,(
    ! [VarCurr: state_type] :
      ( v257(VarCurr)
    <=> ( v258(VarCurr)
        | v263(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_8,axiom,(
    ! [VarCurr: state_type] :
      ( v263(VarCurr)
    <=> ( ( v264(VarCurr,1)
        <=> $true )
        & ( v264(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_65,axiom,(
    ! [VarCurr: state_type] :
      ( v264(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_64,axiom,(
    ! [VarCurr: state_type] :
      ( v264(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_21,axiom,(
    ! [VarCurr: state_type] :
      ( v258(VarCurr)
    <=> ( v259(VarCurr)
        | v261(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_7,axiom,(
    ! [VarCurr: state_type] :
      ( v261(VarCurr)
    <=> ( ( v262(VarCurr,1)
        <=> $true )
        & ( v262(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_63,axiom,(
    ! [VarCurr: state_type] :
      ( v262(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_62,axiom,(
    ! [VarCurr: state_type] :
      ( v262(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_6,axiom,(
    ! [VarCurr: state_type] :
      ( v259(VarCurr)
    <=> ( ( v260(VarCurr,1)
        <=> $false )
        & ( v260(VarCurr,0)
        <=> $true ) ) ) )).

tff(addAssignment_61,axiom,(
    ! [VarCurr: state_type] :
      ( v260(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_60,axiom,(
    ! [VarCurr: state_type] :
      ( v260(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(writeUnaryOperator_13,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v255(VarCurr)
    <=> v34(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_20,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v246(VarNext)
      <=> ( v247(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_12,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v247(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_59,axiom,(
    ! [VarCurr: state_type] :
      ( v237(VarCurr,49)
    <=> v242(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_4,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v239(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v242(VarCurr,B)
          <=> v241(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_4,axiom,(
    ! [VarCurr: state_type] :
      ( v239(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v242(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_58,axiom,(
    ! [VarCurr: state_type] :
      ( v241(VarCurr,49)
    <=> v94(VarCurr,49) ) )).

tff(addAssignment_57,axiom,(
    ! [VarCurr: state_type] :
      ( v239(VarCurr)
    <=> v103(VarCurr,7) ) )).

tff(addAssignment_56,axiom,(
    ! [VarCurr: state_type] :
      ( v99(VarCurr,49)
    <=> v235(VarCurr,49) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v101(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v235(VarCurr,B)
          <=> v218(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_3,axiom,(
    ! [VarCurr: state_type] :
      ( v101(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,70)
            & ~ $less(B,0) )
         => ( v235(VarCurr,B)
          <=> v212(VarCurr,B) ) ) ) )).

tff(addAssignment_55,axiom,(
    ! [VarCurr: state_type] :
      ( v218(VarCurr,49)
    <=> v94(VarCurr,119) ) )).

tff(addAssignmentInitValue_74,axiom,(
    ~ v94(constB0,559) )).

tff(addAssignmentInitValue_73,axiom,(
    ~ v94(constB0,558) )).

tff(addAssignmentInitValue_72,axiom,(
    ~ v94(constB0,557) )).

tff(addAssignmentInitValue_71,axiom,(
    ~ v94(constB0,556) )).

tff(addAssignmentInitValue_70,axiom,(
    ~ v94(constB0,555) )).

tff(addAssignmentInitValue_69,axiom,(
    ~ v94(constB0,554) )).

tff(addAssignmentInitValue_68,axiom,(
    ~ v94(constB0,553) )).

tff(addAssignmentInitValue_67,axiom,(
    ~ v94(constB0,539) )).

tff(bitBlastConstant_82,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(69) )).

tff(bitBlastConstant_81,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(68) )).

tff(bitBlastConstant_80,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(67) )).

tff(bitBlastConstant_79,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(66) )).

tff(bitBlastConstant_78,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(65) )).

tff(bitBlastConstant_77,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(64) )).

tff(bitBlastConstant_76,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(63) )).

tff(bitBlastConstant_75,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(49) )).

tff(addAssignmentInitValue_66,axiom,(
    ~ v94(constB0,489) )).

tff(addAssignmentInitValue_65,axiom,(
    ~ v94(constB0,488) )).

tff(addAssignmentInitValue_64,axiom,(
    ~ v94(constB0,487) )).

tff(addAssignmentInitValue_63,axiom,(
    ~ v94(constB0,486) )).

tff(addAssignmentInitValue_62,axiom,(
    ~ v94(constB0,485) )).

tff(addAssignmentInitValue_61,axiom,(
    ~ v94(constB0,484) )).

tff(addAssignmentInitValue_60,axiom,(
    ~ v94(constB0,483) )).

tff(addAssignmentInitValue_59,axiom,(
    ~ v94(constB0,469) )).

tff(bitBlastConstant_74,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(69) )).

tff(bitBlastConstant_73,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(68) )).

tff(bitBlastConstant_72,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(67) )).

tff(bitBlastConstant_71,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(66) )).

tff(bitBlastConstant_70,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(65) )).

tff(bitBlastConstant_69,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(64) )).

tff(bitBlastConstant_68,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(63) )).

tff(bitBlastConstant_67,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(49) )).

tff(addAssignmentInitValue_58,axiom,(
    ~ v94(constB0,419) )).

tff(addAssignmentInitValue_57,axiom,(
    ~ v94(constB0,418) )).

tff(addAssignmentInitValue_56,axiom,(
    ~ v94(constB0,417) )).

tff(addAssignmentInitValue_55,axiom,(
    ~ v94(constB0,416) )).

tff(addAssignmentInitValue_54,axiom,(
    ~ v94(constB0,415) )).

tff(addAssignmentInitValue_53,axiom,(
    ~ v94(constB0,414) )).

tff(addAssignmentInitValue_52,axiom,(
    ~ v94(constB0,413) )).

tff(addAssignmentInitValue_51,axiom,(
    ~ v94(constB0,399) )).

tff(bitBlastConstant_66,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(69) )).

tff(bitBlastConstant_65,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(68) )).

tff(bitBlastConstant_64,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(67) )).

tff(bitBlastConstant_63,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(66) )).

tff(bitBlastConstant_62,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(65) )).

tff(bitBlastConstant_61,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(64) )).

tff(bitBlastConstant_60,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(63) )).

tff(bitBlastConstant_59,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(49) )).

tff(addAssignmentInitValue_50,axiom,(
    ~ v94(constB0,349) )).

tff(addAssignmentInitValue_49,axiom,(
    ~ v94(constB0,348) )).

tff(addAssignmentInitValue_48,axiom,(
    ~ v94(constB0,347) )).

tff(addAssignmentInitValue_47,axiom,(
    ~ v94(constB0,346) )).

tff(addAssignmentInitValue_46,axiom,(
    ~ v94(constB0,345) )).

tff(addAssignmentInitValue_45,axiom,(
    ~ v94(constB0,344) )).

tff(addAssignmentInitValue_44,axiom,(
    ~ v94(constB0,343) )).

tff(addAssignmentInitValue_43,axiom,(
    ~ v94(constB0,329) )).

tff(bitBlastConstant_58,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(69) )).

tff(bitBlastConstant_57,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(68) )).

tff(bitBlastConstant_56,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(67) )).

tff(bitBlastConstant_55,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(66) )).

tff(bitBlastConstant_54,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(65) )).

tff(bitBlastConstant_53,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(64) )).

tff(bitBlastConstant_52,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(63) )).

tff(bitBlastConstant_51,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(49) )).

tff(addAssignmentInitValue_42,axiom,(
    ~ v94(constB0,279) )).

tff(addAssignmentInitValue_41,axiom,(
    ~ v94(constB0,278) )).

tff(addAssignmentInitValue_40,axiom,(
    ~ v94(constB0,277) )).

tff(addAssignmentInitValue_39,axiom,(
    ~ v94(constB0,276) )).

tff(addAssignmentInitValue_38,axiom,(
    ~ v94(constB0,275) )).

tff(addAssignmentInitValue_37,axiom,(
    ~ v94(constB0,274) )).

tff(addAssignmentInitValue_36,axiom,(
    ~ v94(constB0,273) )).

tff(addAssignmentInitValue_35,axiom,(
    ~ v94(constB0,259) )).

tff(bitBlastConstant_50,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(69) )).

tff(bitBlastConstant_49,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(68) )).

tff(bitBlastConstant_48,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(67) )).

tff(bitBlastConstant_47,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(66) )).

tff(bitBlastConstant_46,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(65) )).

tff(bitBlastConstant_45,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(64) )).

tff(bitBlastConstant_44,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(63) )).

tff(bitBlastConstant_43,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(49) )).

tff(addAssignmentInitValue_34,axiom,(
    ~ v94(constB0,209) )).

tff(addAssignmentInitValue_33,axiom,(
    ~ v94(constB0,208) )).

tff(addAssignmentInitValue_32,axiom,(
    ~ v94(constB0,207) )).

tff(addAssignmentInitValue_31,axiom,(
    ~ v94(constB0,206) )).

tff(addAssignmentInitValue_30,axiom,(
    ~ v94(constB0,205) )).

tff(addAssignmentInitValue_29,axiom,(
    ~ v94(constB0,204) )).

tff(addAssignmentInitValue_28,axiom,(
    ~ v94(constB0,203) )).

tff(addAssignmentInitValue_27,axiom,(
    ~ v94(constB0,189) )).

tff(bitBlastConstant_42,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(69) )).

tff(bitBlastConstant_41,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(68) )).

tff(bitBlastConstant_40,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(67) )).

tff(bitBlastConstant_39,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(66) )).

tff(bitBlastConstant_38,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(65) )).

tff(bitBlastConstant_37,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(64) )).

tff(bitBlastConstant_36,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(63) )).

tff(bitBlastConstant_35,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(49) )).

tff(addAssignmentInitValue_26,axiom,(
    ~ v94(constB0,139) )).

tff(addAssignmentInitValue_25,axiom,(
    ~ v94(constB0,138) )).

tff(addAssignmentInitValue_24,axiom,(
    ~ v94(constB0,137) )).

tff(addAssignmentInitValue_23,axiom,(
    ~ v94(constB0,136) )).

tff(addAssignmentInitValue_22,axiom,(
    ~ v94(constB0,135) )).

tff(addAssignmentInitValue_21,axiom,(
    ~ v94(constB0,134) )).

tff(addAssignmentInitValue_20,axiom,(
    ~ v94(constB0,133) )).

tff(addAssignmentInitValue_19,axiom,(
    ~ v94(constB0,119) )).

tff(bitBlastConstant_34,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(69) )).

tff(bitBlastConstant_33,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(68) )).

tff(bitBlastConstant_32,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(67) )).

tff(bitBlastConstant_31,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(66) )).

tff(bitBlastConstant_30,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(65) )).

tff(bitBlastConstant_29,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(64) )).

tff(bitBlastConstant_28,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(63) )).

tff(bitBlastConstant_27,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(49) )).

tff(addAssignmentInitValue_18,axiom,(
    ~ v94(constB0,69) )).

tff(addAssignmentInitValue_17,axiom,(
    ~ v94(constB0,68) )).

tff(addAssignmentInitValue_16,axiom,(
    ~ v94(constB0,67) )).

tff(addAssignmentInitValue_15,axiom,(
    ~ v94(constB0,66) )).

tff(addAssignmentInitValue_14,axiom,(
    ~ v94(constB0,65) )).

tff(addAssignmentInitValue_13,axiom,(
    ~ v94(constB0,64) )).

tff(addAssignmentInitValue_12,axiom,(
    ~ v94(constB0,63) )).

tff(addAssignmentInitValue_11,axiom,(
    ~ v94(constB0,49) )).

tff(bitBlastConstant_26,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(69) )).

tff(bitBlastConstant_25,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(68) )).

tff(bitBlastConstant_24,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(67) )).

tff(bitBlastConstant_23,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(66) )).

tff(bitBlastConstant_22,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(65) )).

tff(bitBlastConstant_21,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(64) )).

tff(bitBlastConstant_20,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(63) )).

tff(bitBlastConstant_19,axiom,(
    ~ b0000000xxxxxxxxxxxxx0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx(49) )).

tff(addAssignment_54,axiom,(
    ! [VarCurr: state_type] :
      ( v212(VarCurr,49)
    <=> v214(VarCurr,49) ) )).

tff(addAssignment_53,axiom,(
    ! [VarCurr: state_type] :
      ( v214(VarCurr,49)
    <=> v216(VarCurr,49) ) )).

tff(addAssignment_52,axiom,(
    ! [VarCurr: state_type] :
      ( v101(VarCurr)
    <=> v103(VarCurr,7) ) )).

tff(addAssignment_51,axiom,(
    ! [VarCurr: state_type] :
      ( v103(VarCurr,7)
    <=> v131(VarCurr,7) ) )).

tff(addAssignment_50,axiom,(
    ! [VarNext: state_type] :
      ( v107(VarNext,6)
    <=> v204(VarNext,6) ) )).

tff(addCaseBooleanConditionEqualRanges1_8,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v205(VarNext)
       => ! [B: $int] :
            ( ( $less(B,11)
              & ~ $less(B,0) )
           => ( v204(VarNext,B)
            <=> v107(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_8,axiom,(
    ! [VarNext: state_type] :
      ( v205(VarNext)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v204(VarNext,B)
          <=> v125(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_19,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v205(VarNext)
      <=> v206(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_18,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v206(VarNext)
      <=> ( v208(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_11,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v208(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_49,axiom,(
    ! [VarCurr: state_type] :
      ( v103(VarCurr,6)
    <=> v131(VarCurr,6) ) )).

tff(addAssignment_48,axiom,(
    ! [VarNext: state_type] :
      ( v107(VarNext,5)
    <=> v196(VarNext,5) ) )).

tff(addCaseBooleanConditionEqualRanges1_7,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v197(VarNext)
       => ! [B: $int] :
            ( ( $less(B,11)
              & ~ $less(B,0) )
           => ( v196(VarNext,B)
            <=> v107(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_7,axiom,(
    ! [VarNext: state_type] :
      ( v197(VarNext)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v196(VarNext,B)
          <=> v125(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_17,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v197(VarNext)
      <=> v198(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_16,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v198(VarNext)
      <=> ( v200(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_10,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v200(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_47,axiom,(
    ! [VarCurr: state_type] :
      ( v103(VarCurr,5)
    <=> v131(VarCurr,5) ) )).

tff(addAssignment_46,axiom,(
    ! [VarNext: state_type] :
      ( v107(VarNext,4)
    <=> v188(VarNext,4) ) )).

tff(addCaseBooleanConditionEqualRanges1_6,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v189(VarNext)
       => ! [B: $int] :
            ( ( $less(B,11)
              & ~ $less(B,0) )
           => ( v188(VarNext,B)
            <=> v107(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_6,axiom,(
    ! [VarNext: state_type] :
      ( v189(VarNext)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v188(VarNext,B)
          <=> v125(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_15,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v189(VarNext)
      <=> v190(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_14,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v190(VarNext)
      <=> ( v192(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_9,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v192(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_45,axiom,(
    ! [VarCurr: state_type] :
      ( v103(VarCurr,4)
    <=> v131(VarCurr,4) ) )).

tff(addAssignment_44,axiom,(
    ! [VarNext: state_type] :
      ( v107(VarNext,3)
    <=> v180(VarNext,3) ) )).

tff(addCaseBooleanConditionEqualRanges1_5,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v181(VarNext)
       => ! [B: $int] :
            ( ( $less(B,11)
              & ~ $less(B,0) )
           => ( v180(VarNext,B)
            <=> v107(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_5,axiom,(
    ! [VarNext: state_type] :
      ( v181(VarNext)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v180(VarNext,B)
          <=> v125(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_13,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v181(VarNext)
      <=> v182(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_12,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v182(VarNext)
      <=> ( v184(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_8,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v184(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_43,axiom,(
    ! [VarCurr: state_type] :
      ( v103(VarCurr,3)
    <=> v131(VarCurr,3) ) )).

tff(addAssignment_42,axiom,(
    ! [VarNext: state_type] :
      ( v107(VarNext,2)
    <=> v172(VarNext,2) ) )).

tff(addCaseBooleanConditionEqualRanges1_4,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v173(VarNext)
       => ! [B: $int] :
            ( ( $less(B,11)
              & ~ $less(B,0) )
           => ( v172(VarNext,B)
            <=> v107(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_4,axiom,(
    ! [VarNext: state_type] :
      ( v173(VarNext)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v172(VarNext,B)
          <=> v125(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_11,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v173(VarNext)
      <=> v174(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_10,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v174(VarNext)
      <=> ( v176(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_7,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v176(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_41,axiom,(
    ! [VarCurr: state_type] :
      ( v103(VarCurr,2)
    <=> v131(VarCurr,2) ) )).

tff(addAssignment_40,axiom,(
    ! [VarNext: state_type] :
      ( v107(VarNext,1)
    <=> v164(VarNext,1) ) )).

tff(addCaseBooleanConditionEqualRanges1_3,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v165(VarNext)
       => ! [B: $int] :
            ( ( $less(B,11)
              & ~ $less(B,0) )
           => ( v164(VarNext,B)
            <=> v107(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_3,axiom,(
    ! [VarNext: state_type] :
      ( v165(VarNext)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v164(VarNext,B)
          <=> v125(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_9,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v165(VarNext)
      <=> v166(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_8,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v166(VarNext)
      <=> ( v168(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_6,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v168(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_39,axiom,(
    ! [VarCurr: state_type] :
      ( v103(VarCurr,1)
    <=> v131(VarCurr,1) ) )).

tff(addAssignment_38,axiom,(
    ! [VarNext: state_type] :
      ( v107(VarNext,0)
    <=> v156(VarNext,0) ) )).

tff(addCaseBooleanConditionEqualRanges1_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v157(VarNext)
       => ! [B: $int] :
            ( ( $less(B,11)
              & ~ $less(B,0) )
           => ( v156(VarNext,B)
            <=> v107(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_2,axiom,(
    ! [VarNext: state_type] :
      ( v157(VarNext)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v156(VarNext,B)
          <=> v125(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_7,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v157(VarNext)
      <=> v158(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_6,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v158(VarNext)
      <=> ( v160(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_5,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v160(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_37,axiom,(
    ! [VarCurr: state_type] :
      ( v103(VarCurr,0)
    <=> v131(VarCurr,0) ) )).

tff(addAssignment_36,axiom,(
    ! [VarCurr: state_type] :
      ( v105(VarCurr,1)
    <=> v129(VarCurr,1) ) )).

tff(addAssignment_35,axiom,(
    ! [VarCurr: state_type] :
      ( v105(VarCurr,2)
    <=> v129(VarCurr,2) ) )).

tff(addAssignment_34,axiom,(
    ! [VarCurr: state_type] :
      ( v105(VarCurr,3)
    <=> v129(VarCurr,3) ) )).

tff(addAssignment_33,axiom,(
    ! [VarCurr: state_type] :
      ( v105(VarCurr,4)
    <=> v129(VarCurr,4) ) )).

tff(addAssignment_32,axiom,(
    ! [VarCurr: state_type] :
      ( v105(VarCurr,5)
    <=> v129(VarCurr,5) ) )).

tff(addAssignment_31,axiom,(
    ! [VarCurr: state_type] :
      ( v105(VarCurr,6)
    <=> v129(VarCurr,6) ) )).

tff(addAssignment_30,axiom,(
    ! [VarNext: state_type] :
      ( v107(VarNext,8)
    <=> v148(VarNext,8) ) )).

tff(addCaseBooleanConditionEqualRanges1_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v149(VarNext)
       => ! [B: $int] :
            ( ( $less(B,11)
              & ~ $less(B,0) )
           => ( v148(VarNext,B)
            <=> v107(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0_1,axiom,(
    ! [VarNext: state_type] :
      ( v149(VarNext)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v148(VarNext,B)
          <=> v125(VarNext,B) ) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_5,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v149(VarNext)
      <=> v150(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_4,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v150(VarNext)
      <=> ( v152(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_4,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v152(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_29,axiom,(
    ! [VarCurr: state_type] :
      ( v103(VarCurr,8)
    <=> v131(VarCurr,8) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v132(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v131(VarCurr,B)
          <=> v134(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_2,axiom,(
    ! [VarCurr: state_type] :
      ( v132(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v131(VarCurr,B)
          <=> v133(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges3,axiom,(
    ! [VarCurr: state_type] :
      ( ( ~ v135(VarCurr)
        & ~ v137(VarCurr)
        & ~ v141(VarCurr) )
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v134(VarCurr,B)
          <=> v107(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges2,axiom,(
    ! [VarCurr: state_type] :
      ( v141(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v134(VarCurr,B)
          <=> v143(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges1,axiom,(
    ! [VarCurr: state_type] :
      ( v137(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v134(VarCurr,B)
          <=> v139(VarCurr,B) ) ) ) )).

tff(addParallelCaseBooleanConditionEqualRanges0,axiom,(
    ! [VarCurr: state_type] :
      ( v135(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v134(VarCurr,B)
          <=> v107(VarCurr,B) ) ) ) )).

tff(addBitVectorEqualityBitBlasted_5,axiom,(
    ! [VarCurr: state_type] :
      ( v145(VarCurr)
    <=> ( ( v146(VarCurr,1)
        <=> $true )
        & ( v146(VarCurr,0)
        <=> $true ) ) ) )).

tff(bitBlastConstant_18,axiom,(
    b11(1) )).

tff(bitBlastConstant_17,axiom,(
    b11(0) )).

tff(addAssignment_28,axiom,(
    ! [VarCurr: state_type] :
      ( v146(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_27,axiom,(
    ! [VarCurr: state_type] :
      ( v146(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addAssignment_26,axiom,(
    ! [VarCurr: state_type] :
      ( v143(VarCurr,0)
    <=> $false ) )).

tff(addAssignment_25,axiom,(
    ! [VarCurr: state_type] :
      ( ( v143(VarCurr,10)
      <=> v107(VarCurr,9) )
      & ( v143(VarCurr,9)
      <=> v107(VarCurr,8) )
      & ( v143(VarCurr,8)
      <=> v107(VarCurr,7) )
      & ( v143(VarCurr,7)
      <=> v107(VarCurr,6) )
      & ( v143(VarCurr,6)
      <=> v107(VarCurr,5) )
      & ( v143(VarCurr,5)
      <=> v107(VarCurr,4) )
      & ( v143(VarCurr,4)
      <=> v107(VarCurr,3) )
      & ( v143(VarCurr,3)
      <=> v107(VarCurr,2) )
      & ( v143(VarCurr,2)
      <=> v107(VarCurr,1) )
      & ( v143(VarCurr,1)
      <=> v107(VarCurr,0) ) ) )).

tff(addBitVectorEqualityBitBlasted_4,axiom,(
    ! [VarCurr: state_type] :
      ( v141(VarCurr)
    <=> ( ( v142(VarCurr,1)
        <=> $true )
        & ( v142(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_16,axiom,(
    b10(1) )).

tff(bitBlastConstant_15,axiom,(
    ~ b10(0) )).

tff(addAssignment_24,axiom,(
    ! [VarCurr: state_type] :
      ( v142(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_23,axiom,(
    ! [VarCurr: state_type] :
      ( v142(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addAssignment_22,axiom,(
    ! [VarCurr: state_type] :
      ( ( v139(VarCurr,9)
      <=> v107(VarCurr,10) )
      & ( v139(VarCurr,8)
      <=> v107(VarCurr,9) )
      & ( v139(VarCurr,7)
      <=> v107(VarCurr,8) )
      & ( v139(VarCurr,6)
      <=> v107(VarCurr,7) )
      & ( v139(VarCurr,5)
      <=> v107(VarCurr,6) )
      & ( v139(VarCurr,4)
      <=> v107(VarCurr,5) )
      & ( v139(VarCurr,3)
      <=> v107(VarCurr,4) )
      & ( v139(VarCurr,2)
      <=> v107(VarCurr,3) )
      & ( v139(VarCurr,1)
      <=> v107(VarCurr,2) )
      & ( v139(VarCurr,0)
      <=> v107(VarCurr,1) ) ) )).

tff(addAssignment_21,axiom,(
    ! [VarCurr: state_type] :
      ( v139(VarCurr,10)
    <=> $false ) )).

tff(addBitVectorEqualityBitBlasted_3,axiom,(
    ! [VarCurr: state_type] :
      ( v137(VarCurr)
    <=> ( ( v138(VarCurr,1)
        <=> $false )
        & ( v138(VarCurr,0)
        <=> $true ) ) ) )).

tff(bitBlastConstant_14,axiom,(
    ~ b01(1) )).

tff(bitBlastConstant_13,axiom,(
    b01(0) )).

tff(addAssignment_20,axiom,(
    ! [VarCurr: state_type] :
      ( v138(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_19,axiom,(
    ! [VarCurr: state_type] :
      ( v138(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted_2,axiom,(
    ! [VarCurr: state_type] :
      ( v135(VarCurr)
    <=> ( ( v136(VarCurr,1)
        <=> $false )
        & ( v136(VarCurr,0)
        <=> $false ) ) ) )).

tff(addAssignment_18,axiom,(
    ! [VarCurr: state_type] :
      ( v136(VarCurr,0)
    <=> v43(VarCurr) ) )).

tff(addAssignment_17,axiom,(
    ! [VarCurr: state_type] :
      ( v136(VarCurr,1)
    <=> v36(VarCurr) ) )).

tff(addAssignment_16,axiom,(
    ! [VarCurr: state_type] :
      ( v133(VarCurr,0)
    <=> $true ) )).

tff(addAssignment_15,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,11)
        & ~ $less(B,1) )
     => ( v133(VarCurr,B)
      <=> v105(VarCurr,B) ) ) )).

tff(writeUnaryOperator_3,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v132(VarCurr)
    <=> v34(VarCurr) ) )).

tff(addAssignment_14,axiom,(
    ! [VarCurr: state_type] :
      ( v105(VarCurr,8)
    <=> v129(VarCurr,8) ) )).

tff(addAssignment_13,axiom,(
    ! [VarCurr: state_type] :
      ( v105(VarCurr,7)
    <=> v129(VarCurr,7) ) )).

tff(addAssignment_12,axiom,(
    ! [VarCurr: state_type] :
      ( v129(VarCurr,0)
    <=> $true ) )).

tff(addAssignment_11,axiom,(
    ! [VarCurr: state_type,B: $int] :
      ( ( $less(B,11)
        & ~ $less(B,1) )
     => ( v129(VarCurr,B)
      <=> v107(VarCurr,B) ) ) )).

tff(addAssignment_10,axiom,(
    ! [VarNext: state_type] :
      ( v107(VarNext,7)
    <=> v114(VarNext,7) ) )).

tff(addCaseBooleanConditionEqualRanges1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v115(VarNext)
       => ! [B: $int] :
            ( ( $less(B,11)
              & ~ $less(B,0) )
           => ( v114(VarNext,B)
            <=> v107(VarCurr,B) ) ) ) ) )).

tff(addCaseBooleanConditionEqualRanges0,axiom,(
    ! [VarNext: state_type] :
      ( v115(VarNext)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v114(VarNext,B)
          <=> v125(VarNext,B) ) ) ) )).

tff(addAssignment_9,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v125(VarNext,B)
          <=> v123(VarCurr,B) ) ) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch_1,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v126(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v123(VarCurr,B)
          <=> v103(VarCurr,B) ) ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch_1,axiom,(
    ! [VarCurr: state_type] :
      ( v126(VarCurr)
     => ! [B: $int] :
          ( ( $less(B,11)
            & ~ $less(B,0) )
         => ( v123(VarCurr,B)
          <=> b00000000001(B) ) ) ) )).

tff(writeUnaryOperator_2,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v126(VarCurr)
    <=> v34(VarCurr) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_3,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v115(VarNext)
      <=> v116(VarNext) ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_2,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v116(VarNext)
      <=> ( v117(VarNext)
          & v110(VarNext) ) ) ) )).

tff(writeUnaryOperator_1,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( ~ v117(VarNext)
      <=> v119(VarNext) ) ) )).

tff(addAssignment_8,axiom,(
    ! [VarNext: state_type,VarCurr: state_type] :
      ( nextState(VarCurr,VarNext)
     => ( v119(VarNext)
      <=> v110(VarCurr) ) ) )).

tff(addAssignmentInitValue_10,axiom,(
    ~ v107(constB0,10) )).

tff(addAssignmentInitValue_9,axiom,(
    ~ v107(constB0,9) )).

tff(addAssignmentInitValue_8,axiom,(
    ~ v107(constB0,8) )).

tff(addAssignmentInitValue_7,axiom,(
    ~ v107(constB0,7) )).

tff(addAssignmentInitValue_6,axiom,(
    ~ v107(constB0,6) )).

tff(addAssignmentInitValue_5,axiom,(
    ~ v107(constB0,5) )).

tff(addAssignmentInitValue_4,axiom,(
    ~ v107(constB0,4) )).

tff(addAssignmentInitValue_3,axiom,(
    ~ v107(constB0,3) )).

tff(addAssignmentInitValue_2,axiom,(
    ~ v107(constB0,2) )).

tff(addAssignmentInitValue_1,axiom,(
    ~ v107(constB0,1) )).

tff(addAssignmentInitValue,axiom,(
    v107(constB0,0) )).

tff(bitBlastConstant_12,axiom,(
    ~ b00000000001(10) )).

tff(bitBlastConstant_11,axiom,(
    ~ b00000000001(9) )).

tff(bitBlastConstant_10,axiom,(
    ~ b00000000001(8) )).

tff(bitBlastConstant_9,axiom,(
    ~ b00000000001(7) )).

tff(bitBlastConstant_8,axiom,(
    ~ b00000000001(6) )).

tff(bitBlastConstant_7,axiom,(
    ~ b00000000001(5) )).

tff(bitBlastConstant_6,axiom,(
    ~ b00000000001(4) )).

tff(bitBlastConstant_5,axiom,(
    ~ b00000000001(3) )).

tff(bitBlastConstant_4,axiom,(
    ~ b00000000001(2) )).

tff(bitBlastConstant_3,axiom,(
    ~ b00000000001(1) )).

tff(bitBlastConstant_2,axiom,(
    b00000000001(0) )).

tff(addAssignment_7,axiom,(
    ! [VarCurr: state_type] :
      ( v110(VarCurr)
    <=> v112(VarCurr) ) )).

tff(addAssignment_6,axiom,(
    ! [VarCurr: state_type] :
      ( v112(VarCurr)
    <=> v1(VarCurr) ) )).

tff(addAssignment_5,axiom,(
    ! [VarCurr: state_type] :
      ( v64(VarCurr)
    <=> v11(VarCurr) ) )).

tff(aaddConditionBooleanCondEqualRangesElseBranch,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v50(VarCurr)
     => ( v47(VarCurr)
      <=> $false ) ) )).

tff(addConditionBooleanCondEqualRangesThenBranch,axiom,(
    ! [VarCurr: state_type] :
      ( v50(VarCurr)
     => ( v47(VarCurr)
      <=> $true ) ) )).

tff(writeBinaryOperatorEqualRangesSingleBits_1,axiom,(
    ! [VarCurr: state_type] :
      ( v50(VarCurr)
    <=> ( v51(VarCurr)
        & v54(VarCurr) ) ) )).

tff(addBitVectorEqualityBitBlasted_1,axiom,(
    ! [VarCurr: state_type] :
      ( v54(VarCurr)
    <=> ( $true
      <=> v7(VarCurr,0) ) ) )).

tff(addAssignmentInitValueVector_1,axiom,
    ( ( v7(constB0,2)
    <=> $false )
    & ( v7(constB0,1)
    <=> $false ) )).

tff(addAssignmentInitValueVector,axiom,
    ( v7(constB0,0)
  <=> $true )).

tff(writeBinaryOperatorEqualRangesSingleBits,axiom,(
    ! [VarCurr: state_type] :
      ( v51(VarCurr)
    <=> ( v52(VarCurr)
        & v53(VarCurr) ) ) )).

tff(writeUnaryOperator,axiom,(
    ! [VarCurr: state_type] :
      ( ~ v53(VarCurr)
    <=> v30(VarCurr) ) )).

tff(addBitVectorEqualityBitBlasted,axiom,(
    ! [VarCurr: state_type] :
      ( v52(VarCurr)
    <=> ( ( v28(VarCurr,1)
        <=> $false )
        & ( v28(VarCurr,0)
        <=> $false ) ) ) )).

tff(bitBlastConstant_1,axiom,(
    ~ b00(1) )).

tff(bitBlastConstant,axiom,(
    ~ b00(0) )).

tff(addAssignment_4,axiom,(
    ! [VarCurr: state_type] :
      ( v36(VarCurr)
    <=> v38(VarCurr) ) )).

tff(addAssignment_3,axiom,(
    ! [VarCurr: state_type] :
      ( v38(VarCurr)
    <=> v40(VarCurr) ) )).

tff(addAssignment_2,axiom,(
    ! [VarCurr: state_type] :
      ( v34(VarCurr)
    <=> v9(VarCurr) ) )).

tff(addAssignment_1,axiom,(
    ! [VarCurr: state_type] :
      ( v22(VarCurr)
    <=> v24(VarCurr) ) )).

tff(addAssignment,axiom,(
    ! [VarCurr: state_type] :
      ( v9(VarCurr)
    <=> v11(VarCurr) ) )).

%------------------------------------------------------------------------------
