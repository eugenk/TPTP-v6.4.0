%------------------------------------------------------------------------------
% File     : KLE020+2 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (Idempotent Test Semirings)
% Problem  : On tests addition distributes over multiplication
% Version  : [Hoe08] axioms.
% English  :

% Refs     : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.70 v6.4.0, 0.69 v6.3.0, 0.62 v6.2.0, 0.72 v6.1.0, 0.83 v5.5.0, 0.89 v5.3.0, 0.85 v5.2.0, 0.80 v5.1.0, 0.86 v5.0.0, 0.96 v4.0.1, 0.91 v4.0.0
% Syntax   : Number of formulae    :   17 (  11 unit)
%            Number of atoms       :   29 (  17 equality)
%            Maximal formula depth :    7 (   4 average)
%            Number of connectives :   13 (   1 ~  ;   0  |;   5  &)
%                                         (   4 <=>;   3 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    4 (   0 propositional; 1-2 arity)
%            Number of functors    :    5 (   2 constant; 0-2 arity)
%            Number of variables   :   32 (   0 singleton;  31 !;   1 ?)
%            Maximal term depth    :    3 (   2 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Inequational encoding : proof goal is split into 2 inequations
%------------------------------------------------------------------------------
%---Include axioms for idempotent test semiring
include('Axioms/KLE001+0.ax').
%---Include test axioms
include('Axioms/KLE001+1.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1,X2] :
      ( ( test(X0)
        & test(X1)
        & test(X2) )
     => ( leq(addition(X0,multiplication(X1,X2)),multiplication(addition(X0,X1),addition(X0,X2)))
        & leq(multiplication(addition(X0,X1),addition(X0,X2)),addition(X0,multiplication(X1,X2))) ) ) )).

%------------------------------------------------------------------------------
