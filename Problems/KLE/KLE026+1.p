%------------------------------------------------------------------------------
% File     : KLE026+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (Idempotent Test Semirings)
% Problem  : Two ways of expressing the Hoare triple {p}x{q}
% Version  : [Hoe08] axioms.
% English  :

% Refs     : [Koz00] Kozen (2000), On Hoare Logic and Kleene Algebra with T
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.30 v6.4.0, 0.35 v6.3.0, 0.29 v6.2.0, 0.40 v6.1.0, 0.47 v6.0.0, 0.52 v5.4.0, 0.54 v5.3.0, 0.56 v5.2.0, 0.40 v5.1.0, 0.43 v5.0.0, 0.33 v4.1.0, 0.35 v4.0.0
% Syntax   : Number of formulae    :   17 (  11 unit)
%            Number of atoms       :   28 (  18 equality)
%            Maximal formula depth :    6 (   4 average)
%            Number of connectives :   12 (   1 ~  ;   0  |;   3  &)
%                                         (   4 <=>;   4 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    4 (   0 propositional; 1-2 arity)
%            Number of functors    :    5 (   2 constant; 0-2 arity)
%            Number of variables   :   32 (   0 singleton;  31 !;   1 ?)
%            Maximal term depth    :    3 (   2 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Equational encoding
%------------------------------------------------------------------------------
%---Include axioms for idempotent test semiring
include('Axioms/KLE001+0.ax').
%---Include test axioms
include('Axioms/KLE001+1.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1,X2] :
      ( ( test(X1)
        & test(X2) )
     => ( multiplication(X1,X0) = multiplication(multiplication(X1,X0),X2)
       => leq(multiplication(X1,X0),multiplication(X0,X2)) ) ) )).

%------------------------------------------------------------------------------
