%------------------------------------------------------------------------------
% File     : KLE034+2 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (Idempotent Test Semirings)
% Problem  : Hoare rule product
% Version  : [Hoe08] axioms : Augmented.
% English  : Encoding of Hoare rule {p}x{q} <description> {q}y{r} -> {p}x;y{r}.

% Refs     : [Koz00] Kozen (2000), On Hoare Logic and Kleene Algebra with T
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.60 v6.4.0, 0.58 v6.3.0, 0.54 v6.2.0, 0.64 v6.1.0, 0.73 v6.0.0, 0.70 v5.5.0, 0.78 v5.4.0, 0.82 v5.3.0, 0.85 v5.2.0, 0.75 v5.1.0, 0.71 v5.0.0, 0.75 v4.1.0, 0.74 v4.0.1, 0.65 v4.0.0
% Syntax   : Number of formulae    :   19 (  11 unit)
%            Number of atoms       :   36 (  19 equality)
%            Maximal formula depth :   11 (   4 average)
%            Number of connectives :   18 (   1 ~  ;   0  |;   8  &)
%                                         (   4 <=>;   5 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    4 (   0 propositional; 1-2 arity)
%            Number of functors    :    5 (   2 constant; 0-2 arity)
%            Number of variables   :   38 (   0 singleton;  37 !;   1 ?)
%            Maximal term depth    :    4 (   2 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Inequational encoding
%------------------------------------------------------------------------------
%---Include axioms for idempotent test semiring
include('Axioms/KLE001+0.ax').
%---Include test axioms
include('Axioms/KLE001+1.ax').
%---Include additionally axioms
include('Axioms/KLE001+2.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1,X2,X3,X4] :
      ( ( test(X3)
        & test(X2)
        & test(X4)
        & leq(multiplication(multiplication(X2,X0),c(X3)),zero)
        & leq(multiplication(multiplication(X3,X1),c(X4)),zero) )
     => leq(multiplication(multiplication(multiplication(X2,X0),X1),c(X4)),zero) ) )).

%------------------------------------------------------------------------------
