%------------------------------------------------------------------------------
% File     : KLE035+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (Idempotent Test Semirings)
% Problem  : Hoare rule sum
% Version  : [Hoe08] axioms.
% English  : Encoding of Hoare rule {p}x{q} <description> {q}y{r} -> {p}x;y{r}.

% Refs     : [Koz00] Kozen (2000), On Hoare Logic and Kleene Algebra with T
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.37 v6.4.0, 0.38 v6.3.0, 0.29 v6.2.0, 0.32 v6.1.0, 0.43 v6.0.0, 0.39 v5.5.0, 0.41 v5.4.0, 0.46 v5.3.0, 0.52 v5.2.0, 0.35 v5.1.0, 0.33 v5.0.0, 0.46 v4.1.0, 0.35 v4.0.0
% Syntax   : Number of formulae    :   17 (  11 unit)
%            Number of atoms       :   29 (  17 equality)
%            Maximal formula depth :    9 (   4 average)
%            Number of connectives :   13 (   1 ~  ;   0  |;   5  &)
%                                         (   4 <=>;   3 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    4 (   0 propositional; 1-2 arity)
%            Number of functors    :    5 (   2 constant; 0-2 arity)
%            Number of variables   :   33 (   0 singleton;  32 !;   1 ?)
%            Maximal term depth    :    4 (   2 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Inequational encoding
%------------------------------------------------------------------------------
%---Include axioms for idempotent test semiring
include('Axioms/KLE001+0.ax').
%---Include test axioms
include('Axioms/KLE001+1.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1,X2,X3] :
      ( ( test(X3)
        & test(X2)
        & leq(multiplication(multiplication(X2,X0),c(X3)),zero)
        & leq(multiplication(multiplication(X2,X1),c(X3)),zero) )
     => leq(multiplication(multiplication(X2,addition(X0,X1)),c(X3)),zero) ) )).

%------------------------------------------------------------------------------
