%------------------------------------------------------------------------------
% File     : KLE038+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra
% Problem  : Star extensivity
% Version  : [Hoe08] axioms.
% English  : Star is extensive.

% Refs     : [Koz94] Kozen (1994), A Completeness Theorem for Kleene Algebr
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.33 v6.4.0, 0.35 v6.3.0, 0.33 v6.2.0, 0.44 v6.1.0, 0.43 v6.0.0, 0.39 v5.5.0, 0.52 v5.4.0, 0.54 v5.3.0, 0.59 v5.2.0, 0.50 v5.1.0, 0.52 v5.0.0, 0.58 v4.1.0, 0.52 v4.0.1, 0.48 v4.0.0
% Syntax   : Number of formulae    :   17 (  14 unit)
%            Number of atoms       :   20 (  12 equality)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    3 (   0 ~  ;   0  |;   0  &)
%                                         (   1 <=>;   2 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :    5 (   2 constant; 0-2 arity)
%            Number of variables   :   31 (   0 singleton;  31 !;   0 ?)
%            Maximal term depth    :    4 (   2 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Inequational encoding
%------------------------------------------------------------------------------
%---Include axioms for Kleene algebra
include('Axioms/KLE002+0.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0] : leq(X0,star(X0)) )).

%------------------------------------------------------------------------------
