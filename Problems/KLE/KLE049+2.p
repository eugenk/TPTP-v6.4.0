%------------------------------------------------------------------------------
% File     : KLE049+2 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (with Tests)
% Problem  : Hoare rule while
% Version  : [Hoe08] axioms : Augmented.
% English  : Encoding of Hoare rule {p;q}x{q} -> {q} while p  do x {c(p);q},
%            where while p do x = (p;x)*;c(p).

% Refs     : [Koz00] Kozen (2000), On Hoare Logic and Kleene Algebra with T
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :   23 (  13 unit)
%            Number of atoms       :   59 (  19 equality)
%            Maximal formula depth :    7 (   4 average)
%            Number of connectives :   18 (   1   ~;   0   |;   6   &)
%                                         (   4 <=>;   7  =>;   0  <=)
%                                         (   0 <~>;   0  ~|;   0  ~&)
%            Number of predicates  :    4 (   0 propositional; 1-2 arity)
%            Number of functors    :    6 (   2 constant; 0-2 arity)
%            Number of variables   :   44 (   0 sgn;  43   !;   1   ?)
%            Maximal term depth    :    6 (   2 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Inequational encoding
%------------------------------------------------------------------------------
%---Include axioms for Kleene algebra with tests
include('Axioms/KLE002+0.ax').
%---Include test axioms
include('Axioms/KLE001+1.ax').
%---Include additionally axioms
include('Axioms/KLE001+2.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1,X2] :
      ( ( test(X1)
        & test(X2)
        & leq(multiplication(multiplication(multiplication(X2,X1),X0),c(X2)),zero) )
     => leq(multiplication(multiplication(multiplication(X2,star(multiplication(X1,X0))),c(X1)),c(X2)),zero) ) )).

%------------------------------------------------------------------------------
