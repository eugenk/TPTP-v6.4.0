%------------------------------------------------------------------------------
% File     : KLE093+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (with Domain)
% Problem  : Domain of star
% Version  : [Hoe08] axioms.
% English  : In a Kleene algebra with domain, the domain of a star is always
%            one.

% Refs     : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.27 v6.4.0, 0.31 v6.3.0, 0.33 v6.2.0, 0.40 v6.1.0, 0.43 v5.5.0, 0.52 v5.4.0, 0.54 v5.3.0, 0.56 v5.2.0, 0.50 v5.1.0, 0.52 v5.0.0, 0.46 v4.1.0, 0.43 v4.0.0
% Syntax   : Number of formulae    :   22 (  19 unit)
%            Number of atoms       :   25 (  18 equality)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    3 (   0 ~  ;   0  |;   0  &)
%                                         (   1 <=>;   2 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :    6 (   2 constant; 0-2 arity)
%            Number of variables   :   37 (   0 singleton;  37 !;   0 ?)
%            Maximal term depth    :    4 (   2 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Equational encoding
%------------------------------------------------------------------------------
%---Include axioms for Kleene algebra with domain
include('Axioms/KLE002+0.ax').
%---Include axioms for domain
include('Axioms/KLE001+5.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0] : domain(star(X0)) = one )).

%------------------------------------------------------------------------------
