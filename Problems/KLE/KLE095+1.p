%------------------------------------------------------------------------------
% File     : KLE095+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (Modal)
% Problem  : Modal operators satisfy a star unfold law
% Version  : [Hoe08] axioms.
% English  :

% Refs     : [DMS06] Desharnais et al. (2006), Kleene Algebra with Domain
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :   31 (  28 unit)
%            Number of atoms       :   34 (  27 equality)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    3 (   0 ~  ;   0  |;   0  &)
%                                         (   1 <=>;   2 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :   15 (   2 constant; 0-2 arity)
%            Number of variables   :   53 (   0 singleton;  53 !;   0 ?)
%            Maximal term depth    :    6 (   3 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : A similar law holds in propositional dynamic logic.
%          : Equational encoding
%------------------------------------------------------------------------------
%---Include axioms for modal Kleene algebra
include('Axioms/KLE002+0.ax').
%---Include axioms for Boolean domain/codomain
include('Axioms/KLE001+4.ax').
%---Include axioms for diamond and boxes
include('Axioms/KLE001+6.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1] : addition(domain(X0),forward_diamond(X1,forward_diamond(star(X1),domain(X0)))) = forward_diamond(star(X1),domain(X0)) )).

%------------------------------------------------------------------------------
