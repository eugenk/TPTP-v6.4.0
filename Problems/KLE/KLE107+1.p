%------------------------------------------------------------------------------
% File     : KLE107+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (Modal Semirings)
% Problem  : Galois
% Version  : [Hoe08] axioms.
% English  : Backward diamonds and forward boxes are adjoints of a Galois
%            connection.

% Refs     : [DMS04] Desharnais et al. (2004), Termination in Modal Kleene
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.77 v6.4.0, 0.73 v6.3.0, 0.67 v6.2.0, 0.76 v6.1.0, 0.80 v6.0.0, 0.74 v5.5.0, 0.81 v5.4.0, 0.82 v5.3.0, 0.81 v5.2.0, 0.70 v5.1.0, 0.71 v5.0.0, 0.75 v4.1.0, 0.74 v4.0.1, 0.70 v4.0.0
% Syntax   : Number of formulae    :   27 (  25 unit)
%            Number of atoms       :   29 (  28 equality)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    2 (   0 ~  ;   0  |;   0  &)
%                                         (   1 <=>;   1 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :   14 (   2 constant; 0-2 arity)
%            Number of variables   :   46 (   0 singleton;  46 !;   0 ?)
%            Maximal term depth    :    6 (   3 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Equational encoding
%------------------------------------------------------------------------------
%---Include axioms for modal semiring
include('Axioms/KLE001+0.ax').
%---Include axioms for Boolean domain/codomain
include('Axioms/KLE001+4.ax').
%---Include axioms for diamond and boxes
include('Axioms/KLE001+6.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1,X2] :
      ( addition(backward_diamond(X0,domain(X1)),domain(X2)) = domain(X2)
     => addition(domain(X1),forward_box(X0,domain(X2))) = forward_box(X0,domain(X2)) ) )).

%------------------------------------------------------------------------------
