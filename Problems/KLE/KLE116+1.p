%------------------------------------------------------------------------------
% File     : KLE116+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (Modal Semirings)
% Problem  : Boxes are multiplicative
% Version  : [Hoe08] axioms.
% English  :

% Refs     : [DMS04] Desharnais et al. (2004), Termination in Modal Kleene
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.83 v6.4.0, 0.81 v6.3.0, 0.75 v6.2.0, 0.84 v6.1.0, 0.87 v6.0.0, 0.83 v5.5.0, 0.85 v5.4.0, 0.86 v5.3.0, 0.85 v5.2.0, 0.80 v5.1.0, 0.86 v5.0.0, 0.88 v4.1.0, 0.87 v4.0.1, 0.91 v4.0.0
% Syntax   : Number of formulae    :   27 (  26 unit)
%            Number of atoms       :   28 (  27 equality)
%            Maximal formula depth :    4 (   3 average)
%            Number of connectives :    1 (   0 ~  ;   0  |;   0  &)
%                                         (   1 <=>;   0 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :   14 (   2 constant; 0-2 arity)
%            Number of variables   :   46 (   0 singleton;  46 !;   0 ?)
%            Maximal term depth    :    6 (   3 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : There is a dual law for backward boxes.
%          : Equational encoding
%------------------------------------------------------------------------------
%---Include axioms for modal semiring
include('Axioms/KLE001+0.ax').
%---Include axioms for Boolean domain/codomain
include('Axioms/KLE001+4.ax').
%---Include axioms for diamond and boxes
include('Axioms/KLE001+6.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1,X2] : forward_box(X0,multiplication(domain(X1),domain(X2))) = multiplication(forward_box(X0,domain(X1)),forward_box(X0,domain(X2))) )).

%------------------------------------------------------------------------------
