%------------------------------------------------------------------------------
% File     : KLE145+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (Demonic Refinement Algebra)
% Problem  : Finite iteration after strong iteration is strong iteration
% Version  : [Hoe08] axioms.
% English  :

% Refs     : [vW02]  von Wright (2002), From Kleene Algebra to Refinement A
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.57 v6.4.0, 0.58 v6.3.0, 0.50 v6.2.0, 0.64 v6.1.0, 0.77 v6.0.0, 0.70 v5.5.0, 0.78 v5.4.0, 0.79 v5.3.0, 0.81 v5.2.0, 0.70 v5.1.0, 0.71 v4.1.0, 0.70 v4.0.0
% Syntax   : Number of formulae    :   19 (  15 unit)
%            Number of atoms       :   23 (  16 equality)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    4 (   0 ~  ;   0  |;   0  &)
%                                         (   1 <=>;   3 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :    6 (   2 constant; 0-2 arity)
%            Number of variables   :   35 (   0 singleton;  35 !;   0 ?)
%            Maximal term depth    :    4 (   2 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%------------------------------------------------------------------------------
%---Include axioms for demonic refinement algebra
include('Axioms/KLE004+0.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0] : star(strong_iteration(X0)) = strong_iteration(X0) )).

%------------------------------------------------------------------------------
