%------------------------------------------------------------------------------
% File     : KLE152+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Kleene Algebra (Demonic Refinement Algebra)
% Problem  : Sliding of strong iteration
% Version  : [Hoe08] axioms.
% English  : Two ways of grouping an alternation of x's and y's that starts
%            and ends with x.

% Refs     : [vW02]  von Wright (2002), From Kleene Algebra to Refinement A
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :   19 (  15 unit)
%            Number of atoms       :   23 (  15 equality)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    4 (   0 ~  ;   0  |;   0  &)
%                                         (   1 <=>;   3 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :    6 (   2 constant; 0-2 arity)
%            Number of variables   :   36 (   0 singleton;  36 !;   0 ?)
%            Maximal term depth    :    4 (   2 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : The converse inequation holds also (KLE151).
%------------------------------------------------------------------------------
%---Include axioms for demonic refinement algebra
include('Axioms/KLE004+0.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1] : leq(multiplication(strong_iteration(multiplication(X0,X1)),X0),multiplication(X0,strong_iteration(multiplication(X1,X0)))) )).

%------------------------------------------------------------------------------
