%------------------------------------------------------------------------------
% File     : KLE179+1 : TPTP v6.4.0. Released v6.4.0.
% Domain   : Kleene Algebra
% Problem  : Omega algebra
% Version  : [Hoe08] axioms.
% English  :

% Refs     : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [TPTP]
% Names    :

% Status   : Satisfiable
% Rating   : 0.00 v6.4.0
% Syntax   : Number of formulae    :   26 (  14 unit)
%            Number of atoms       :   49 (  20 equality)
%            Maximal formula depth :   10 (   4 average)
%            Number of connectives :   24 (   1   ~;   0   |;   8   &)
%                                         (   7 <=>;   8  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :    6 (   0 propositional; 1-3 arity)
%            Number of functors    :    7 (   2 constant; 0-2 arity)
%            Number of variables   :   53 (   0 sgn;  52   !;   1   ?)
%            Maximal term depth    :    4 (   2 average)
% SPC      : FOF_SAT_RFO_SEQ

% Comments :
%------------------------------------------------------------------------------
%----Omega algebra
include('Axioms/KLE003+0.ax').
%----Characterisation of tests by complement predicate
include('Axioms/KLE001+1.ax').
%----de Morgan's laws for tests
include('Axioms/KLE001+2.ax').
%----Universal characterisation of meet
include('Axioms/KLE001+3.ax').
%------------------------------------------------------------------------------
