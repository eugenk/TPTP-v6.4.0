%--------------------------------------------------------------------------
% File     : LAT039-1 : TPTP v6.4.0. Released v2.4.0.
% Domain   : Lattice Theory
% Problem  : Every distributive lattice is modular
% Version  : [McC88] (equality) axioms.
%            Theorem formulation : Modularity is expressed by:
%            x <= y -> x v (y & z) = y & (x v z)
% English  :

% Refs     : [DeN00] DeNivelle (2000), Email to G. Sutcliffe
%            [McC88] McCune (1988), Challenge Equality Problems in Lattice
% Source   : [DeN00]
% Names    : lattice-mod-2 [DeN00]

% Status   : Unsatisfiable
% Rating   : 0.05 v6.3.0, 0.06 v6.2.0, 0.07 v6.1.0, 0.06 v6.0.0, 0.10 v5.5.0, 0.05 v5.4.0, 0.00 v2.4.0
% Syntax   : Number of clauses     :   12 (   0 non-Horn;  12 unit;   2 RR)
%            Number of atoms       :   12 (  12 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    5 (   3 constant; 0-2 arity)
%            Number of variables   :   22 (   2 singleton)
%            Maximal term depth    :    3 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments :
%--------------------------------------------------------------------------
%----Include lattice theory axioms
include('Axioms/LAT001-0.ax').
%--------------------------------------------------------------------------
cnf(dist_join,hypothesis,
    ( join(X,meet(Y,Z)) = meet(join(X,Y),join(X,Z)) )).

cnf(dist_meet,hypothesis,
    ( meet(X,join(Y,Z)) = join(meet(X,Y),meet(X,Z)) )).

cnf(lhs,hypothesis,
    ( join(xx,yy) = yy )).

cnf(rhs,negated_conjecture,
    (  join(xx,meet(yy,zz)) != meet(yy,join(xx,zz)) )).

%--------------------------------------------------------------------------
