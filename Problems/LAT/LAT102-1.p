%------------------------------------------------------------------------------
% File     : LAT102-1 : TPTP v6.4.0. Released v3.1.0.
% Domain   : Lattice Theory
% Problem  : Huntington equation H4 is independent of H7
% Version  : [McC05] (equality) axioms : Especial.
% English  : Show that Huntington equation H7 does not imply Huntington
%            equation H4 in lattice theory.

% Refs     : [McC05] McCune (2005), Email to Geoff Sutcliffe
% Source   : [McC05]
% Names    :

% Status   : Satisfiable
% Rating   : 0.00 v6.4.0, 0.25 v6.3.0, 0.00 v6.2.0, 0.67 v6.1.0, 0.60 v5.5.0, 0.80 v5.4.0, 0.75 v5.3.0, 0.67 v5.2.0, 0.33 v3.2.0, 0.67 v3.1.0
% Syntax   : Number of clauses     :   10 (   0 non-Horn;  10 unit;   1 RR)
%            Number of atoms       :   10 (  10 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    6 (   4 constant; 0-2 arity)
%            Number of variables   :   19 (   2 singleton)
%            Maximal term depth    :    7 (   3 average)
% SPC      : CNF_SAT_RFO_PEQ_UEQ

% Comments :
%------------------------------------------------------------------------------
%----Include Lattice theory (equality) axioms
include('Axioms/LAT001-0.ax').
%------------------------------------------------------------------------------
cnf(equation_H7,axiom,
    ( meet(X,join(Y,meet(X,Z))) = meet(X,join(Y,meet(X,join(meet(X,Y),meet(Z,join(X,Y)))))) )).

cnf(prove_H4,negated_conjecture,
    (  meet(a,join(b,meet(a,join(c,d)))) != meet(a,join(b,meet(join(a,meet(b,d)),join(c,d)))) )).

%------------------------------------------------------------------------------
