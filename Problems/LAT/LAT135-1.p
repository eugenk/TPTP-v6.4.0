%------------------------------------------------------------------------------
% File     : LAT135-1 : TPTP v6.4.0. Released v3.1.0.
% Domain   : Lattice Theory
% Problem  : Huntington equation H39_dual is independent of H68
% Version  : [McC05] (equality) axioms : Especial.
% English  : Show that Huntington equation H68 does not imply Huntington
%            equation H39_dual in lattice theory.

% Refs     : [McC05] McCune (2005), Email to Geoff Sutcliffe
% Source   : [McC05]
% Names    :

% Status   : Satisfiable
% Rating   : 0.00 v6.2.0, 0.50 v6.1.0, 0.40 v6.0.0, 0.20 v5.5.0, 0.40 v5.4.0, 0.50 v5.3.0, 0.67 v5.2.0, 0.33 v4.1.0, 0.67 v4.0.1, 0.33 v3.2.0, 0.67 v3.1.0
% Syntax   : Number of clauses     :   10 (   0 non-Horn;  10 unit;   1 RR)
%            Number of atoms       :   10 (  10 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    6 (   4 constant; 0-2 arity)
%            Number of variables   :   19 (   2 singleton)
%            Maximal term depth    :    6 (   3 average)
% SPC      : CNF_SAT_RFO_PEQ_UEQ

% Comments :
%------------------------------------------------------------------------------
%----Include Lattice theory (equality) axioms
include('Axioms/LAT001-0.ax').
%------------------------------------------------------------------------------
cnf(equation_H68,axiom,
    ( meet(X,join(Y,Z)) = meet(X,join(Y,meet(X,join(Z,meet(X,Y))))) )).

cnf(prove_H39_dual,negated_conjecture,
    (  join(a,meet(b,join(c,meet(a,d)))) != join(a,meet(b,join(c,meet(d,join(a,c))))) )).

%------------------------------------------------------------------------------
