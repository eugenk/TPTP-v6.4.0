%------------------------------------------------------------------------------
% File     : LAT204-1 : TPTP v6.4.0. Released v3.1.0.
% Domain   : Lattice Theory
% Problem  : Equation H58 is Huntington by distributivity
% Version  : [McC05] (equality) axioms : Especial.
% English  : Show that H58 is Huntington by deriving distributivity in uniquely
%            complemented lattices.

% Refs     : [McC05] McCune (2005), Email to Geoff Sutcliffe
% Source   : [McC05]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.92 v6.4.0, 0.93 v6.3.0, 0.90 v6.1.0, 1.00 v5.4.0, 0.89 v5.3.0, 0.90 v5.2.0, 0.88 v5.1.0, 1.00 v3.7.0, 0.86 v3.4.0, 1.00 v3.1.0
% Syntax   : Number of clauses     :   13 (   0 non-Horn;  12 unit;   2 RR)
%            Number of atoms       :   15 (  15 equality)
%            Maximal clause size   :    3 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    8 (   5 constant; 0-2 arity)
%            Number of variables   :   23 (   2 singleton)
%            Maximal term depth    :    6 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
%------------------------------------------------------------------------------
%----Include Lattice theory (equality) axioms
include('Axioms/LAT001-0.ax').
%----Include Lattice theory unique complementation (equality) axioms
include('Axioms/LAT001-4.ax').
%------------------------------------------------------------------------------
cnf(equation_H58,axiom,
    ( meet(X,join(Y,Z)) = meet(X,join(Y,meet(join(X,Y),join(Z,meet(X,Y))))) )).

cnf(prove_distributivity,negated_conjecture,
    (  meet(a,join(b,c)) != join(meet(a,b),meet(a,c)) )).

%------------------------------------------------------------------------------
