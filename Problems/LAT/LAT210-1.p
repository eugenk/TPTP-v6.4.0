%------------------------------------------------------------------------------
% File     : LAT210-1 : TPTP v6.4.0. Released v3.1.0.
% Domain   : Lattice Theory
% Problem  : Equation H68 is Huntington by distributivity
% Version  : [McC05] (equality) axioms : Especial.
% English  : Show that H68 is Huntington by deriving distributivity in uniquely
%            complemented lattices.

% Refs     : [McC05] McCune (2005), Email to Geoff Sutcliffe
% Source   : [McC05]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.77 v6.4.0, 0.79 v6.3.0, 0.70 v6.1.0, 0.73 v6.0.0, 0.57 v5.5.0, 0.62 v5.4.0, 0.78 v5.3.0, 0.80 v5.2.0, 0.75 v5.1.0, 0.78 v5.0.0, 0.80 v4.1.0, 0.67 v4.0.1, 0.75 v4.0.0, 0.57 v3.7.0, 0.14 v3.4.0, 0.17 v3.3.0, 0.33 v3.2.0, 0.22 v3.1.0
% Syntax   : Number of clauses     :   13 (   0 non-Horn;  12 unit;   2 RR)
%            Number of atoms       :   15 (  15 equality)
%            Maximal clause size   :    3 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    8 (   5 constant; 0-2 arity)
%            Number of variables   :   23 (   2 singleton)
%            Maximal term depth    :    6 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
%------------------------------------------------------------------------------
%----Include Lattice theory (equality) axioms
include('Axioms/LAT001-0.ax').
%----Include Lattice theory unique complementation (equality) axioms
include('Axioms/LAT001-4.ax').
%------------------------------------------------------------------------------
cnf(equation_H68,axiom,
    ( meet(X,join(Y,Z)) = meet(X,join(Y,meet(X,join(Z,meet(X,Y))))) )).

cnf(prove_distributivity,negated_conjecture,
    (  meet(a,join(b,c)) != join(meet(a,b),meet(a,c)) )).

%------------------------------------------------------------------------------
