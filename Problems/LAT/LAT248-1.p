%------------------------------------------------------------------------------
% File     : LAT248-1 : TPTP v6.4.0. Released v3.1.0.
% Domain   : Lattice Theory
% Problem  : Equation H63 is Huntington by implication
% Version  : [McC05] (equality) axioms : Especial.
% English  : Show that H63 is Huntington by deriving the Huntington implication
%            X ^ Y = Y  ->  X' v Y' = Y' in uniquely complemented lattices.

% Refs     : [McC05] McCune (2005), Email to Geoff Sutcliffe
% Source   : [McC05]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.77 v6.4.0, 0.71 v6.3.0, 0.70 v6.2.0, 0.90 v6.1.0, 0.91 v6.0.0, 0.86 v5.5.0, 0.88 v5.4.0, 0.89 v5.3.0, 0.90 v5.2.0, 0.88 v5.1.0, 0.89 v5.0.0, 0.90 v4.1.0, 0.89 v4.0.1, 0.88 v4.0.0, 0.86 v3.4.0, 0.83 v3.3.0, 0.78 v3.2.0, 0.89 v3.1.0
% Syntax   : Number of clauses     :   14 (   0 non-Horn;  13 unit;   3 RR)
%            Number of atoms       :   16 (  16 equality)
%            Maximal clause size   :    3 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    7 (   4 constant; 0-2 arity)
%            Number of variables   :   24 (   2 singleton)
%            Maximal term depth    :    6 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
%------------------------------------------------------------------------------
%----Include Lattice theory (equality) axioms
include('Axioms/LAT001-0.ax').
%----Include Lattice theory unique complementation (equality) axioms
include('Axioms/LAT001-4.ax').
%------------------------------------------------------------------------------
cnf(equation_H63,axiom,
    ( meet(X,meet(join(Y,Z),join(Y,U))) = meet(X,join(Y,meet(join(Y,Z),join(U,meet(Y,Z))))) )).

cnf(prove_distributivity_hypothesis,hypothesis,
    ( meet(b,a) = a )).

cnf(prove_distributivity,negated_conjecture,
    (  join(complement(b),complement(a)) != complement(a) )).

%------------------------------------------------------------------------------
