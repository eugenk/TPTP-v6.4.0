%------------------------------------------------------------------------------
% File     : LAT249-1 : TPTP v6.4.0. Released v3.1.0.
% Domain   : Lattice Theory
% Problem  : Equation H64 is Huntington by implication
% Version  : [McC05] (equality) axioms : Especial.
% English  : Show that H64 is Huntington by deriving the Huntington implication
%            X ^ Y = Y  ->  X' v Y' = Y' in uniquely complemented lattices.

% Refs     : [McC05] McCune (2005), Email to Geoff Sutcliffe
% Source   : [McC05]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.15 v6.4.0, 0.21 v6.3.0, 0.30 v6.2.0, 0.50 v6.1.0, 0.55 v6.0.0, 0.43 v5.5.0, 0.50 v5.4.0, 0.56 v5.3.0, 0.70 v5.2.0, 0.50 v5.1.0, 0.44 v5.0.0, 0.40 v4.1.0, 0.33 v4.0.1, 0.38 v4.0.0, 0.14 v3.7.0, 0.00 v3.1.0
% Syntax   : Number of clauses     :   14 (   0 non-Horn;  13 unit;   3 RR)
%            Number of atoms       :   16 (  16 equality)
%            Maximal clause size   :    3 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    7 (   4 constant; 0-2 arity)
%            Number of variables   :   23 (   2 singleton)
%            Maximal term depth    :    8 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
%------------------------------------------------------------------------------
%----Include Lattice theory (equality) axioms
include('Axioms/LAT001-0.ax').
%----Include Lattice theory unique complementation (equality) axioms
include('Axioms/LAT001-4.ax').
%------------------------------------------------------------------------------
cnf(equation_H64,axiom,
    ( meet(X,join(Y,Z)) = meet(X,join(Y,meet(X,join(Z,meet(X,join(Y,meet(X,Z))))))) )).

cnf(prove_distributivity_hypothesis,hypothesis,
    ( meet(b,a) = a )).

cnf(prove_distributivity,negated_conjecture,
    (  join(complement(b),complement(a)) != complement(a) )).

%------------------------------------------------------------------------------
