%------------------------------------------------------------------------------
% File     : LAT264-2 : TPTP v6.4.0. Released v3.2.0.
% Domain   : Analysis
% Problem  : Problem about Tarski's fixed point theorem
% Version  : [Pau06] axioms : Reduced > Especial.
% English  :

% Refs     : [Pau06] Paulson (2006), Email to G. Sutcliffe
% Source   : [Pau06]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.00 v6.2.0, 0.12 v6.0.0, 0.00 v3.3.0, 0.33 v3.2.0
% Syntax   : Number of clauses     :    2 (   0 non-Horn;   2 unit;   2 RR)
%            Number of atoms       :    2 (   0 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 3-3 arity)
%            Number of functors    :    6 (   4 constant; 0-2 arity)
%            Number of variables   :    0 (   0 singleton)
%            Maximal term depth    :    2 (   2 average)
% SPC      : CNF_UNS_EPR

% Comments : The problems in the [Pau06] collection each have very many axioms,
%            of which only a small selection are required for the refutation.
%            The mission is to find those few axioms, after which a refutation
%            can be quite easily found. This version has only the necessary
%            axioms.
%------------------------------------------------------------------------------
cnf(cls_conjecture_2,negated_conjecture,
    ( ~ c_in(c_Tarski_Odual(v_cl,t_a),c_Tarski_OPartialOrder,tc_Tarski_Opotype_Opotype__ext__type(t_a,tc_Product__Type_Ounit)) )).

cnf(cls_Tarski_Odual_Acl_A_58_APartialOrder_0,axiom,
    ( c_in(c_Tarski_Odual(v_cl,t_a),c_Tarski_OPartialOrder,tc_Tarski_Opotype_Opotype__ext__type(t_a,tc_Product__Type_Ounit)) )).

%------------------------------------------------------------------------------
