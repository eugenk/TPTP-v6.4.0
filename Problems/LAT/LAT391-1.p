%------------------------------------------------------------------------------
% File     : LAT391-1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Lattice Theory
% Problem  : Short axiom for lattice theory, part 3
% Version  : Especial.
% English  :

% Refs     : [Sta08] Stanovsky (2008), Email to G. Sutcliffe
% Source   : [Sta08]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.42 v6.3.0, 0.41 v6.2.0, 0.43 v6.1.0, 0.56 v6.0.0, 0.71 v5.5.0, 0.63 v5.4.0, 0.60 v5.3.0, 0.58 v5.2.0, 0.57 v5.1.0, 0.60 v5.0.0, 0.57 v4.1.0, 0.55 v4.0.1, 0.57 v4.0.0
% Syntax   : Number of clauses     :    2 (   0 non-Horn;   2 unit;   1 RR)
%            Number of atoms       :    2 (   2 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    5 (   3 constant; 0-2 arity)
%            Number of variables   :    8 (   2 singleton)
%            Maximal term depth    :   11 (   4 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments :
%------------------------------------------------------------------------------
cnf(sos,axiom,
    ( mult(plus(mult(plus(A,B),plus(B,mult(A,B))),C),plus(mult(plus(A,mult(mult(plus(D,B),plus(B,E)),B)),plus(mult(plus(B,plus(plus(mult(D,mult(B,E)),mult(F,B)),B)),plus(V6,mult(B,plus(plus(mult(B,V7),mult(F,B)),B)))),mult(A,mult(mult(plus(D,B),plus(B,E)),B)))),mult(mult(plus(A,B),plus(B,mult(A,B))),C))) = B )).

cnf(goals,negated_conjecture,
    ( mult(mult(plus(a,b),plus(b,c)),b) != b )).

%------------------------------------------------------------------------------
