%------------------------------------------------------------------------------
% File     : LAT393-2 : TPTP v6.4.0. Released v5.4.0.
% Domain   : Lattice Theory
% Problem  : Ortholattices in terms of Sheffer stroke: associativity
% Version  : Especial
% English  :

% Refs     : [Sta11] Stanovsky (2011), Email to Geoff Sutcliffe
% Source   : [Sta11]
% Names    : lat2 [Sta11]

% Status   : Unsatisfiable
% Rating   : 0.84 v6.4.0, 0.89 v6.3.0, 0.88 v6.2.0, 0.86 v6.1.0, 0.88 v6.0.0, 0.90 v5.5.0, 0.89 v5.4.0
% Syntax   : Number of clauses     :    2 (   0 non-Horn;   2 unit;   1 RR)
%            Number of atoms       :    2 (   2 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    4 (   3 constant; 0-2 arity)
%            Number of variables   :    4 (   1 singleton)
%            Maximal term depth    :    7 (   4 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments : 
%------------------------------------------------------------------------------
cnf(sos,axiom,
    ( f(f(f(f(A,B),f(B,C)),D),f(B,f(f(B,f(f(A,A),A)),C))) = B )).

cnf(goals,negated_conjecture,
    ( f(x0,f(f(x1,x2),f(x1,x2))) != f(x1,f(f(x0,x2),f(x0,x2))) )).

%------------------------------------------------------------------------------
