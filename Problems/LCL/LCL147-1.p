%--------------------------------------------------------------------------
% File     : LCL147-1 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Logic Calculi (Wajsberg Algebra)
% Problem  : A theorem in the lattice structure of Wajsberg algebras
% Version  : [Bon91] (equality) axioms.
% English  :

% Refs     : [FRT84] Font et al. (1984), Wajsberg Algebras
%          : [Bon91] Bonacina (1991), Problems in Lukasiewicz Logic
% Source   : [Bon91]
% Names    : Lattice structure theorem 6 [Bon91]

% Status   : Unsatisfiable
% Rating   : 1.00 v6.2.0, 0.67 v6.1.0, 1.00 v6.0.0, 0.89 v5.5.0, 1.00 v4.0.1, 0.83 v3.3.0, 0.71 v3.1.0, 1.00 v2.0.0
% Syntax   : Number of clauses     :    9 (   0 non-Horn;   7 unit;   3 RR)
%            Number of atoms       :   11 (   9 equality)
%            Maximal clause size   :    2 (   1 average)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :    8 (   4 constant; 0-2 arity)
%            Number of variables   :   16 (   0 singleton)
%            Maximal term depth    :    4 (   2 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include Wajsberg algebra axioms
include('Axioms/LCL001-0.ax').
%----Include Wajsberg algebra lattice structure axioms
include('Axioms/LCL001-1.ax').
%--------------------------------------------------------------------------
cnf(prove_wajsberg_theorem,negated_conjecture,
    (  implies(big_V(x,y),z) != big_hat(implies(x,z),implies(y,z)) )).

%--------------------------------------------------------------------------
