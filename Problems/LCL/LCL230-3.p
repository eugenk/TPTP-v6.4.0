%--------------------------------------------------------------------------
% File     : LCL230-3 : TPTP v6.4.0. Released v2.3.0.
% Domain   : Logic Calculi (Propositional)
% Problem  : Principia Mathematica 2.85
% Version  : [WR27] axioms.
% English  :

% Refs     : [WR27]  Whitehead & Russell (1927), Principia Mathematica
% Source   : [WR27]
% Names    : Problem 2.85 [WR27]

% Status   : Unsatisfiable
% Rating   : 0.43 v6.3.0, 0.33 v6.2.0, 0.00 v6.1.0, 0.40 v6.0.0, 0.67 v5.5.0, 0.81 v5.4.0, 0.80 v5.3.0, 0.83 v5.2.0, 0.62 v5.1.0, 0.57 v4.1.0, 0.67 v4.0.1, 0.50 v4.0.0, 0.67 v3.5.0, 0.50 v3.3.0, 0.57 v3.1.0, 0.67 v2.7.0, 0.50 v2.6.0, 0.71 v2.5.0, 0.60 v2.4.0, 0.67 v2.3.0
% Syntax   : Number of clauses     :    9 (   0 non-Horn;   7 unit;   3 RR)
%            Number of atoms       :   12 (   1 equality)
%            Maximal clause size   :    3 (   1 average)
%            Number of predicates  :    3 (   0 propositional; 1-2 arity)
%            Number of functors    :    6 (   3 constant; 0-2 arity)
%            Number of variables   :   16 (   1 singleton)
%            Maximal term depth    :    4 (   2 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include axioms of propositional logic
include('Axioms/LCL004-0.ax').
%--------------------------------------------------------------------------
cnf(prove_this,negated_conjecture,
    ( ~ theorem(implies(implies(or(p,q),or(p,r)),or(p,implies(q,r)))) )).

%--------------------------------------------------------------------------
