%--------------------------------------------------------------------------
% File     : LCL275-3 : TPTP v6.4.0. Released v2.3.0.
% Domain   : Logic Calculi (Propositional)
% Problem  : Principia Mathematica 4.32
% Version  : [WR27] axioms.
% English  :

% Refs     : [WR27]  Whitehead & Russell (1927), Principia Mathematica
% Source   : [WR27]
% Names    : Problem 4.32 [WR27]

% Status   : Unsatisfiable
% Rating   : 0.86 v6.4.0, 0.71 v6.3.0, 0.67 v6.2.0, 0.33 v6.1.0, 0.80 v6.0.0, 1.00 v5.4.0, 0.93 v5.3.0, 0.92 v5.2.0, 0.88 v5.1.0, 0.86 v4.1.0, 0.89 v4.0.1, 0.83 v3.3.0, 0.86 v3.1.0, 0.89 v2.7.0, 1.00 v2.3.0
% Syntax   : Number of clauses     :   11 (   0 non-Horn;   9 unit;   3 RR)
%            Number of atoms       :   14 (   3 equality)
%            Maximal clause size   :    3 (   1 average)
%            Number of predicates  :    3 (   0 propositional; 1-2 arity)
%            Number of functors    :    8 (   3 constant; 0-2 arity)
%            Number of variables   :   20 (   1 singleton)
%            Maximal term depth    :    4 (   3 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include axioms of propositional logic
include('Axioms/LCL004-0.ax').
include('Axioms/LCL004-1.ax').
include('Axioms/LCL004-2.ax').
%--------------------------------------------------------------------------
cnf(prove_this,negated_conjecture,
    ( ~ theorem(equivalent(and(r,and(p,q)),and(p,and(q,r)))) )).

%--------------------------------------------------------------------------
