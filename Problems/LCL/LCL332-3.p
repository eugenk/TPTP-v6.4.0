%--------------------------------------------------------------------------
% File     : LCL332-3 : TPTP v6.4.0. Released v2.3.0.
% Domain   : Logic Calculi (Propositional)
% Problem  : Principia Mathematica 5.31
% Version  : [WR27] axioms.
% English  :

% Refs     : [WR27]  Whitehead & Russell (1927), Principia Mathematica
% Source   : [WR27]
% Names    : Problem 5.31 [WR27]

% Status   : Unsatisfiable
% Rating   : 0.71 v6.3.0, 0.67 v6.2.0, 0.33 v6.1.0, 0.60 v6.0.0, 0.78 v5.5.0, 0.94 v5.4.0, 0.87 v5.3.0, 1.00 v5.2.0, 0.75 v5.1.0, 0.71 v4.1.0, 0.78 v4.0.1, 0.67 v4.0.0, 0.83 v3.5.0, 0.67 v3.3.0, 0.71 v3.1.0, 0.78 v2.7.0, 0.83 v2.6.0, 1.00 v2.3.0
% Syntax   : Number of clauses     :   10 (   0 non-Horn;   8 unit;   3 RR)
%            Number of atoms       :   13 (   2 equality)
%            Maximal clause size   :    3 (   1 average)
%            Number of predicates  :    3 (   0 propositional; 1-2 arity)
%            Number of functors    :    7 (   3 constant; 0-2 arity)
%            Number of variables   :   18 (   1 singleton)
%            Maximal term depth    :    4 (   3 average)
% SPC      : CNF_UNS_RFO_SEQ_HRN

% Comments :
%--------------------------------------------------------------------------
%----Include axioms of propositional logic
include('Axioms/LCL004-0.ax').
include('Axioms/LCL004-1.ax').
%--------------------------------------------------------------------------
cnf(prove_this,negated_conjecture,
    ( ~ theorem(implies(and(r,implies(p,q)),implies(p,and(q,r)))) )).

%--------------------------------------------------------------------------
