%--------------------------------------------------------------------------
% File     : LCL411-1 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Logic Calculi (Propositional)
% Problem  : Propositional logic deduction axioms
% Version  : [WR27] axioms : Reduced & Augmented.
% English  :

% Refs     : [WR27]  Whitehead & Russell (1927), Principia Mathematica
%          : [ORo89] O'Rourke (1989), LT Revisited: Explanation-Based Learn
%          : [SE94]  Segre & Elkan (1994), A High-Performance Explanation-B
% Source   : [SE94]
% Names    :

% Status   : Satisfiable
% Rating   : 0.00 v5.4.0, 0.67 v5.3.0, 0.71 v5.0.0, 0.50 v4.1.0, 0.43 v4.0.0, 0.50 v3.5.0, 0.57 v3.4.0, 0.50 v3.2.0, 0.60 v3.1.0, 0.43 v2.7.0, 0.40 v2.6.0, 0.25 v2.5.0
% Syntax   : Number of clauses     :    8 (   0 non-Horn;   5 unit;   3 RR)
%            Number of atoms       :   13 (   0 equality)
%            Maximal clause size   :    3 (   2 average)
%            Number of predicates  :    2 (   0 propositional; 1-1 arity)
%            Number of functors    :    2 (   0 constant; 1-2 arity)
%            Number of variables   :   17 (   1 singleton)
%            Maximal term depth    :    5 (   3 average)
% SPC      : CNF_SAT_RFO_NEQ

% Comments :
%--------------------------------------------------------------------------
%----Include Propositional logic deduction axioms
include('Axioms/LCL003-0.ax').
%--------------------------------------------------------------------------
