%------------------------------------------------------------------------------
% File     : LCL456+1 : TPTP v6.4.0. Released v3.3.0.
% Domain   : Logic Calculi (Propositional)
% Problem  : Prove Principia's r3 axiom from Hilbert's axiomatization
% Version  : [HB34] axioms.
% English  :

% Refs     : [HB34]  Hilbert & Bernays (1934), Grundlagen der Mathematick
%          : [Hal]   Halleck (URL), John Halleck's Logic Systems
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.23 v6.4.0, 0.31 v6.3.0, 0.25 v6.2.0, 0.24 v6.1.0, 0.33 v6.0.0, 0.26 v5.5.0, 0.37 v5.4.0, 0.43 v5.3.0, 0.48 v5.2.0, 0.35 v5.1.0, 0.38 v5.0.0, 0.46 v4.1.0, 0.48 v4.0.0, 0.46 v3.7.0, 0.40 v3.5.0, 0.42 v3.3.0
% Syntax   : Number of formulae    :   53 (  22 unit)
%            Number of atoms       :   87 (   6 equality)
%            Maximal formula depth :    6 (   3 average)
%            Number of connectives :   34 (   0 ~  ;   0  |;   1  &)
%                                         (  26 <=>;   7 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   33 (  31 propositional; 0-2 arity)
%            Number of functors    :    5 (   0 constant; 1-2 arity)
%            Number of variables   :   65 (   0 singleton;  65 !;   0 ?)
%            Maximal term depth    :    5 (   3 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%------------------------------------------------------------------------------
%----Include axioms of propositional logic
include('Axioms/LCL006+0.ax').
include('Axioms/LCL006+1.ax').
%----Include Hilbert's axiomatization of propositional logic
include('Axioms/LCL006+2.ax').
%------------------------------------------------------------------------------
%----Operator definitions to reduce everything to and & not
fof(principia_op_implies_or,axiom,op_implies_or).

fof(principia_op_and,axiom,op_and).

fof(principia_op_equiv,axiom,op_equiv).

fof(principia_r3,conjecture,r3).

%------------------------------------------------------------------------------
