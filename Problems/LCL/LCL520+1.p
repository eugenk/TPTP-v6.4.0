%------------------------------------------------------------------------------
% File     : LCL520+1 : TPTP v6.4.0. Released v3.3.0.
% Domain   : Logic Calculi (Propositional)
% Problem  : Prove Principia's r3 axiom from Rosser's axiomatization
% Version  : [Zem73] axioms.
% English  :

% Refs     : [Zem73] Zeman (1973), Modal Logic, the Lewis-Modal systems
%          : [Hal]   Halleck (URL), John Halleck's Logic Systems
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.60 v6.4.0, 0.65 v6.3.0, 0.58 v6.2.0, 0.60 v6.1.0, 0.77 v6.0.0, 0.74 v5.5.0, 0.70 v5.4.0, 0.68 v5.3.0, 0.70 v5.2.0, 0.60 v5.1.0, 0.57 v5.0.0, 0.58 v4.1.0, 0.65 v4.0.0, 0.58 v3.7.0, 0.65 v3.5.0, 0.63 v3.3.0
% Syntax   : Number of formulae    :   43 (  12 unit)
%            Number of atoms       :   77 (   6 equality)
%            Maximal formula depth :    6 (   3 average)
%            Number of connectives :   34 (   0 ~  ;   0  |;   1  &)
%                                         (  26 <=>;   7 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   33 (  31 propositional; 0-2 arity)
%            Number of functors    :    5 (   0 constant; 1-2 arity)
%            Number of variables   :   65 (   0 singleton;  65 !;   0 ?)
%            Maximal term depth    :    5 (   3 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%------------------------------------------------------------------------------
%----Include axioms of propositional logic
include('Axioms/LCL006+0.ax').
include('Axioms/LCL006+1.ax').
%----Include Rosser's axiomatization of propositional logic
include('Axioms/LCL006+5.ax').
%------------------------------------------------------------------------------
%----Operator definitions to reduce everything to and & not
fof(principia_op_implies_or,axiom,op_implies_or).

fof(principia_op_and,axiom,op_and).

fof(principia_op_equiv,axiom,op_equiv).

fof(principia_r3,conjecture,r3).

%------------------------------------------------------------------------------
