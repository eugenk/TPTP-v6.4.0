%------------------------------------------------------------------------------
% File     : LCL552+1 : TPTP v6.4.0. Released v3.3.0.
% Domain   : Logic Calculi (Propositional modal)
% Problem  : Prove Hilbert's implies_1 axiom from the S1-0 system
% Version  : [Fey50] axioms.
% English  :

% Refs     : [Fey50] Feys (1950), Les systemes formalises de modalites aris
%          : [Hal]   Halleck (URL), John Halleck's Logic Systems
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.90 v6.4.0, 0.88 v6.3.0, 0.83 v6.2.0, 0.88 v6.1.0, 0.90 v6.0.0, 0.91 v5.5.0, 0.89 v5.4.0, 0.93 v5.2.0, 0.85 v5.1.0, 0.86 v5.0.0, 0.83 v4.0.0, 0.88 v3.7.0, 0.95 v3.3.0
% Syntax   : Number of formulae    :   77 (  19 unit)
%            Number of atoms       :  144 (  11 equality)
%            Maximal formula depth :    6 (   3 average)
%            Number of connectives :   67 (   0 ~  ;   0  |;   3  &)
%                                         (  49 <=>;  15 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   61 (  59 propositional; 0-2 arity)
%            Number of functors    :    9 (   0 constant; 1-2 arity)
%            Number of variables   :  110 (   0 singleton; 110 !;   0 ?)
%            Maximal term depth    :    5 (   3 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%------------------------------------------------------------------------------
%----Include axioms of propositional logic
include('Axioms/LCL006+0.ax').
include('Axioms/LCL006+1.ax').
%----Include axioms of modal logic
include('Axioms/LCL007+0.ax').
include('Axioms/LCL007+1.ax').
%----Include axioms for S1-0
include('Axioms/LCL007+4.ax').
%------------------------------------------------------------------------------
%----Operator definitions to reduce everything to and & not
fof(hilbert_op_or,axiom,op_or).

fof(hilbert_op_implies_and,axiom,op_implies_and).

fof(hilbert_op_equiv,axiom,op_equiv).

%----Admissible but not required for completeness. With it much more can
%----be done.
fof(substitution_of_equivalents,axiom,substitution_of_equivalents).

%----Conjecture
fof(hilbert_implies_1,conjecture,implies_1).

%------------------------------------------------------------------------------
