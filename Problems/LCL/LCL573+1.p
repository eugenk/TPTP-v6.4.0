%------------------------------------------------------------------------------
% File     : LCL573+1 : TPTP v6.4.0. Released v3.3.0.
% Domain   : Logic Calculi (Propositional modal)
% Problem  : Prove axiom 5 from the S1-0M10 axiomatization of S5
% Version  : [Zem73] axioms.
% English  :

% Refs     : [Zem73] Zeman (1973), Modal Logic, the Lewis-Modal systems
%          : [Hal]   Halleck (URL), John Halleck's Logic Systems
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.83 v6.4.0, 0.85 v6.3.0, 0.79 v6.2.0, 0.88 v6.1.0, 0.90 v6.0.0, 0.87 v5.5.0, 0.89 v5.3.0, 0.93 v5.2.0, 0.85 v5.1.0, 0.86 v5.0.0, 0.83 v3.7.0, 0.85 v3.5.0, 0.84 v3.3.0
% Syntax   : Number of formulae    :   52 (  20 unit)
%            Number of atoms       :   90 (  10 equality)
%            Maximal formula depth :    6 (   3 average)
%            Number of connectives :   38 (   0 ~  ;   0  |;   2  &)
%                                         (  23 <=>;  13 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   36 (  34 propositional; 0-2 arity)
%            Number of functors    :    9 (   0 constant; 1-2 arity)
%            Number of variables   :   55 (   0 singleton;  55 !;   0 ?)
%            Maximal term depth    :    5 (   3 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%------------------------------------------------------------------------------
%----Include axioms of propositional logic
include('Axioms/LCL006+1.ax').
%----Include axioms of modal logic
include('Axioms/LCL007+0.ax').
include('Axioms/LCL007+1.ax').
%----Include axioms for S1-0
include('Axioms/LCL007+4.ax').
%----Include axioms for M10
include('Axioms/LCL007+6.ax').
%------------------------------------------------------------------------------
%----Operator definitions to reduce everything to and & not
fof(hilbert_op_or,axiom,op_or).

fof(hilbert_op_implies_and,axiom,op_implies_and).

fof(hilbert_op_equiv,axiom,op_equiv).

%----Admissible but not required for completeness. With it much more can
%----be done.
fof(substitution_of_equivalents,axiom,substitution_of_equivalents).

%----Conjecture
fof(km5_axiom_5,conjecture,axiom_5).

%------------------------------------------------------------------------------
