%------------------------------------------------------------------------------
% File     : LCL579^1 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Logical Calculi
% Problem  : Leibniz-equality definition means it's an equivalence
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.17 v6.1.0, 0.00 v6.0.0, 0.17 v5.5.0, 0.20 v5.4.0, 0.25 v5.3.0, 0.50 v5.2.0, 0.25 v4.1.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :    8 (   0 equality;   8 variable)
%            Maximal formula depth :    7 (   7 average)
%            Number of connectives :    7 (   0   ~;   0   |;   0   &;   4   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    2 (   0   :;   0   =)
%            Number of variables   :    4 (   0 sgn;   4   !;   0   ?;   0   ^)
%                                         (   4   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : 
%------------------------------------------------------------------------------
thf(a,conjecture,(
    ! [X: $i,Y: $i] :
      ( ! [P: $i > $o] :
          ( ( P @ X )
         => ( P @ Y ) )
     => ! [P: $i > $o] :
          ( ( P @ Y )
         => ( P @ X ) ) ) )).

%------------------------------------------------------------------------------
