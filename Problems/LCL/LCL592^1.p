%------------------------------------------------------------------------------
% File     : LCL592^1 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Logical Calculi
% Problem  : Axiom D is valid
% Version  : [BP09] axioms.
% English  :

% Refs     : [BP09]  Benzmueller & Paulson (2009), Exploring Properties of
% Source   : [BP09]
% Names    : K-D [BP09]
%          : NL_2 [BP09]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v4.0.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   36 (   0 unit;  20 type;  15 defn)
%            Number of atoms       :   91 (  15 equality;  47 variable)
%            Maximal formula depth :   10 (   5 average)
%            Number of connectives :   48 (   3   ~;   1   |;   2   &;  41   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   83 (  83   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   25 (  20   :;   0   =)
%            Number of variables   :   39 (   2 sgn;   7   !;   4   ?;  28   ^)
%                                         (  39   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include simple maths definitions and axioms
include('Axioms/LCL008^0.ax').
%------------------------------------------------------------------------------
%----Conjecture
thf(thm,conjecture,(
    ! [R: $i > $i > $o,A: $i > $o,B: $i > $o] :
      ( mvalid @ ( mimpl @ ( mbox @ R @ ( mimpl @ A @ B ) ) @ ( mimpl @ ( mbox @ R @ A ) @ ( mbox @ R @ B ) ) ) ) )).

%------------------------------------------------------------------------------
