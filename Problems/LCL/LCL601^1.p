%------------------------------------------------------------------------------
% File     : LCL601^1 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Logical Calculi
% Problem  : Axiom 4 for all R means all R are valid
% Version  : [BP09] axioms.
% English  : If axiom 4 is valid for all relations R then all relations R are
%            transitive.

% Refs     : [BP09]  Benzmueller & Paulson (2009), Exploring Properties of
% Source   : [BP09]
% Names    : K-4-f [BP09]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.14 v6.0.0, 0.43 v5.5.0, 0.50 v5.4.0, 0.40 v5.3.0, 0.60 v5.2.0, 0.40 v5.1.0, 0.60 v5.0.0, 0.40 v4.1.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   78 (   0 unit;  41 type;  36 defn)
%            Number of atoms       :  245 (  40 equality; 151 variable)
%            Maximal formula depth :   12 (   6 average)
%            Number of connectives :  134 (   6   ~;   3   |;  14   &; 100   @)
%                                         (   2 <=>;   9  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  226 ( 226   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   46 (  41   :;   0   =)
%            Number of variables   :  111 (   2 sgn;  25   !;  10   ?;  76   ^)
%                                         ( 111   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include simple maths definitions and axioms
include('Axioms/LCL008^0.ax').
include('Axioms/SET008^2.ax').
%------------------------------------------------------------------------------
%----Conjecture
thf(thm,conjecture,
    ( ! [R: $i > $i > $o,A: $i > $o] :
        ( mvalid @ ( mimpl @ ( mbox @ R @ A ) @ ( mbox @ R @ ( mbox @ R @ A ) ) ) )
  <=> ! [R: $i > $i > $o] :
        ( transitive @ R ) )).

%------------------------------------------------------------------------------
