%------------------------------------------------------------------------------
% File     : LCL631^1 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Logical Calculi
% Problem  : The muddy forehead puzzle
% Version  : [Ben08] axioms.
% English  :

% Refs     : [Fit07] Fitting (2007), Modal Proof Theory
%          : [Ben08] Benzmueller (2008), Email to G. Sutcliffe
% Source   : [Ben08]
% Names    : Fitting-HB-Knowledge-2b [Ben08]

% Status   : Theorem
% Rating   : 0.43 v6.4.0, 0.50 v6.3.0, 0.40 v6.2.0, 0.43 v6.0.0, 0.71 v5.5.0, 0.83 v5.4.0, 0.80 v5.2.0, 1.00 v4.0.1, 0.67 v3.7.0
% Syntax   : Number of formulae    :   55 (   0 unit;  28 type;  17 defn)
%            Number of atoms       :  213 (  17 equality;  56 variable)
%            Maximal formula depth :   13 (   5 average)
%            Number of connectives :  155 (   3   ~;   1   |;   2   &; 148   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  104 ( 104   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   33 (  28   :;   0   =)
%            Number of variables   :   45 (   2 sgn;  11   !;   4   ?;  30   ^)
%                                         (  45   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include simple maths definitions and axioms
include('Axioms/LCL008^0.ax').
%------------------------------------------------------------------------------
%----Signature
thf(a,type,(
    a: $i > $i > $o )).

thf(b,type,(
    b: $i > $i > $o )).

thf(c,type,(
    c: $i > $i > $o )).

thf(mfa,type,(
    mfa: $i > $o )).

thf(mfb,type,(
    mfb: $i > $o )).

thf(mfc,type,(
    mfc: $i > $o )).

thf(ck,type,(
    ck: ( $i > $o ) > $i > $o )).

thf(s,type,(
    s: $i > $o )).

%----Axioms
thf(knowledge_implies_truth,axiom,(
    ! [X: $i > $o,R: $i > $i > $o] :
      ( mvalid @ ( mimpl @ ( mbox @ R @ X ) @ X ) ) )).

thf(positive_introspection,axiom,(
    ! [X: $i > $o,R: $i > $i > $o] :
      ( mvalid @ ( mimpl @ ( mbox @ R @ X ) @ ( mbox @ R @ ( mbox @ R @ X ) ) ) ) )).

thf(negitive_introspection,axiom,(
    ! [X: $i > $o,R: $i > $i > $o] :
      ( mvalid @ ( mimpl @ ( mnot @ ( mbox @ R @ X ) ) @ ( mbox @ R @ ( mnot @ ( mbox @ R @ X ) ) ) ) ) )).

thf(common_knowledge,definition,
    ( ck
    = ( ^ [X: $i > $o,W: $i] :
        ! [R: $i > $i > $o] :
          ( mbox @ R @ X @ W ) ) )).

thf(what_a_knows_about_b,axiom,
    ( mvalid @ ( ck @ ( mor @ ( mbox @ a @ mfb ) @ ( mbox @ a @ ( mnot @ mfb ) ) ) ) )).

thf(what_a_knows_about_c,axiom,
    ( mvalid @ ( ck @ ( mor @ ( mbox @ a @ mfc ) @ ( mbox @ a @ ( mnot @ mfc ) ) ) ) )).

thf(what_b_knows_about_a,axiom,
    ( mvalid @ ( ck @ ( mor @ ( mbox @ b @ mfa ) @ ( mbox @ b @ ( mnot @ mfa ) ) ) ) )).

thf(what_b_knows_about_c,axiom,
    ( mvalid @ ( ck @ ( mor @ ( mbox @ b @ mfc ) @ ( mbox @ b @ ( mnot @ mfc ) ) ) ) )).

thf(what_c_knows_about_a,axiom,
    ( mvalid @ ( ck @ ( mor @ ( mbox @ c @ mfa ) @ ( mbox @ c @ ( mnot @ mfa ) ) ) ) )).

thf(what_c_knows_about_b,axiom,
    ( mvalid @ ( ck @ ( mor @ ( mbox @ c @ mfb ) @ ( mbox @ c @ ( mnot @ mfb ) ) ) ) )).

thf(someone_knows_its_forehead,definition,
    ( s
    = ( mor @ ( mbox @ a @ mfa ) @ ( mor @ ( mbox @ a @ ( mnot @ mfa ) ) @ ( mor @ ( mbox @ b @ mfb ) @ ( mor @ ( mbox @ b @ ( mnot @ mfb ) ) @ ( mor @ ( mbox @ c @ mfc ) @ ( mbox @ c @ ( mnot @ mfc ) ) ) ) ) ) ) )).

%----Conjecture
thf(thm,conjecture,
    ( mvalid @ ( mnot @ ( mimpl @ ( ck @ ( mnot @ ( mimpl @ ( ck @ ( mor @ mfa @ ( mor @ mfb @ mfc ) ) ) @ s ) ) ) @ s ) ) )).

%------------------------------------------------------------------------------
