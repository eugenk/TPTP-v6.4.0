%------------------------------------------------------------------------------
% File     : LCL657+1.001 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Logic Calculi (Modal Logic)
% Problem  : In KT, the branching formula, size 1
% Version  : Especial.
% English  :

% Refs     : [BHS00] Balsiger et al. (2000), A Benchmark Method for the Pro
%          : [Kam08] Kaminski (2008), Email to G. Sutcliffe
% Source   : [Kam08]
% Names    : kt_branch_n [BHS00]

% Status   : CounterSatisfiable
% Rating   : 0.00 v5.4.0, 0.13 v5.3.0, 0.23 v5.2.0, 0.12 v5.0.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit)
%            Number of atoms       :   36 (   0 equality)
%            Maximal formula depth :   16 (   9 average)
%            Number of connectives :   64 (  30 ~  ;  20  |;  14  &)
%                                         (   0 <=>;   0 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    6 (   0 propositional; 1-2 arity)
%            Number of functors    :    0 (   0 constant; --- arity)
%            Number of variables   :    9 (   0 singleton;   8 !;   1 ?)
%            Maximal term depth    :    1 (   1 average)
% SPC      : FOF_CSA_RFO_NEQ

% Comments : A naive relational encoding of the modal logic problem into
%            first-order logic.
%------------------------------------------------------------------------------
fof(reflexivity,axiom,(
    ! [X] : r1(X,X) )).

fof(main,conjecture,(
    ~ ( ? [X] :
          ( ! [Y] :
              ( ~ r1(X,Y)
              | ( ( ( ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ~ ( ~ p2(X)
                                & ~ p102(X)
                                & p101(X) ) ) )
                    & ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ~ ( p2(X)
                                & ~ p102(X)
                                & p101(X) ) ) ) )
                  | ~ ( ~ p101(Y)
                      & p100(Y) ) )
                & ( ( ( ! [X] :
                          ( ~ r1(Y,X)
                          | ~ p2(X)
                          | ~ p101(X) )
                      | p2(Y) )
                    & ( ! [X] :
                          ( ~ r1(Y,X)
                          | p2(X)
                          | ~ p101(X) )
                      | ~ p2(Y) ) )
                  | ~ p101(Y) )
                & ( ( ( ! [X] :
                          ( ~ r1(Y,X)
                          | ~ p1(X)
                          | ~ p100(X) )
                      | p1(Y) )
                    & ( ! [X] :
                          ( ~ r1(Y,X)
                          | p1(X)
                          | ~ p100(X) )
                      | ~ p1(Y) ) )
                  | ~ p100(Y) )
                & ( p101(Y)
                  | ~ p102(Y) )
                & ( p100(Y)
                  | ~ p101(Y) ) ) )
          & ~ p101(X)
          & p100(X) ) ) )).

%------------------------------------------------------------------------------
