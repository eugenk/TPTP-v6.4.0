%------------------------------------------------------------------------------
% File     : LCL661+1.015 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Logic Calculi (Modal Logic)
% Problem  : In KT, A5 is not provable with instances of Grz1, size 15
% Version  : Especial.
% English  :

% Refs     : [BHS00] Balsiger et al. (2000), A Benchmark Method for the Pro
%          : [Kam08] Kaminski (2008), Email to G. Sutcliffe
% Source   : [Kam08]
% Names    : kt_grz_n [BHS00]

% Status   : CounterSatisfiable
% Rating   : 0.33 v6.4.0, 0.00 v6.2.0, 0.22 v6.1.0, 0.20 v6.0.0, 0.14 v5.4.0, 0.73 v5.3.0, 0.77 v5.2.0, 0.50 v5.0.0, 0.67 v4.1.0, 0.50 v4.0.1, 0.67 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit)
%            Number of atoms       :  761 (   0 equality)
%            Maximal formula depth :   49 (  26 average)
%            Number of connectives : 1079 ( 320 ~  ; 740  |;  19  &)
%                                         (   0 <=>;   0 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    5 (   0 propositional; 1-2 arity)
%            Number of functors    :    0 (   0 constant; --- arity)
%            Number of variables   :  240 (   0 singleton; 239 !;   1 ?)
%            Maximal term depth    :    1 (   1 average)
% SPC      : FOF_CSA_RFO_NEQ

% Comments : A naive relational encoding of the modal logic problem into
%            first-order logic.
%------------------------------------------------------------------------------
fof(reflexivity,axiom,(
    ! [X] : r1(X,X) )).

fof(main,conjecture,(
    ~ ( ? [X] :
          ~ ( ! [Y] :
                ( ~ r1(X,Y)
                | ~ ( ! [X] :
                        ( ~ r1(Y,X)
                        | p3(X) ) ) )
            | ! [Y] :
                ( ~ r1(X,Y)
                | p3(Y) )
            | ! [Y] :
                ( ~ r1(X,Y)
                | ~ ( ! [X] :
                        ( ~ r1(Y,X)
                        | p2(X) ) ) )
            | ! [Y] :
                ( ~ r1(X,Y)
                | p2(Y) )
            | ! [Y] :
                ( ~ r1(X,Y)
                | ~ ( ! [X] :
                        ( ~ r1(Y,X)
                        | p1(X) ) ) )
            | ! [Y] :
                ( ~ r1(X,Y)
                | p1(Y) )
            | ~ ( ( ! [Y] :
                      ( ~ r1(X,Y)
                      | ( ( ! [X] :
                              ( ~ r1(Y,X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y)
                                  | ~ ( ! [X] :
                                          ( ~ r1(Y,X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | p2(Y) )
                                          | ~ p2(X) ) ) ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | p2(X)
                                  | ~ ( ! [Y] :
                                          ( ~ r1(X,Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | p2(X) )
                                          | ~ p2(Y) ) ) ) ) )
                        & ( p2(Y)
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p2(Y) )
                                  | ~ p2(X) ) ) ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | ( ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p2(Y)
                                      | ~ ( ! [X] :
                                              ( ~ r1(Y,X)
                                              | ! [Y] :
                                                  ( ~ r1(X,Y)
                                                  | p2(Y) )
                                              | ~ p2(X) ) ) ) )
                              | ~ ( ! [X] :
                                      ( ~ r1(Y,X)
                                      | p2(X)
                                      | ~ ( ! [Y] :
                                              ( ~ r1(X,Y)
                                              | ! [X] :
                                                  ( ~ r1(Y,X)
                                                  | p2(X) )
                                              | ~ p2(Y) ) ) ) ) )
                            & ( p2(Y)
                              | ~ ( ! [X] :
                                      ( ~ r1(Y,X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p2(Y) )
                                      | ~ p2(X) ) ) ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | ( ( ! [X] :
                                              ( ~ r1(Y,X)
                                              | ! [Y] :
                                                  ( ~ r1(X,Y)
                                                  | p2(Y)
                                                  | ~ ( ! [X] :
                                                          ( ~ r1(Y,X)
                                                          | ! [Y] :
                                                              ( ~ r1(X,Y)
                                                              | p2(Y) )
                                                          | ~ p2(X) ) ) ) )
                                          | ~ ( ! [X] :
                                                  ( ~ r1(Y,X)
                                                  | p2(X)
                                                  | ~ ( ! [Y] :
                                                          ( ~ r1(X,Y)
                                                          | ! [X] :
                                                              ( ~ r1(Y,X)
                                                              | p2(X) )
                                                          | ~ p2(Y) ) ) ) ) )
                                        & ( p2(Y)
                                          | ~ ( ! [X] :
                                                  ( ~ r1(Y,X)
                                                  | ! [Y] :
                                                      ( ~ r1(X,Y)
                                                      | p2(Y) )
                                                  | ~ p2(X) ) ) ) ) )
                                  | ~ ( ( ! [Y] :
                                            ( ~ r1(X,Y)
                                            | ! [X] :
                                                ( ~ r1(Y,X)
                                                | p2(X)
                                                | ~ ( ! [Y] :
                                                        ( ~ r1(X,Y)
                                                        | ! [X] :
                                                            ( ~ r1(Y,X)
                                                            | p2(X) )
                                                        | ~ p2(Y) ) ) ) )
                                        | ~ ( ! [Y] :
                                                ( ~ r1(X,Y)
                                                | p2(Y)
                                                | ~ ( ! [X] :
                                                        ( ~ r1(Y,X)
                                                        | ! [Y] :
                                                            ( ~ r1(X,Y)
                                                            | p2(Y) )
                                                        | ~ p2(X) ) ) ) ) )
                                      & ( p2(X)
                                        | ~ ( ! [Y] :
                                                ( ~ r1(X,Y)
                                                | ! [X] :
                                                    ( ~ r1(Y,X)
                                                    | p2(X) )
                                                | ~ p2(Y) ) ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p2(Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | p4(X)
                          | p3(X)
                          | p2(X)
                          | p1(X)
                          | ! [Y] :
                              ( ~ r1(X,Y)
                              | p4(Y)
                              | p3(Y)
                              | p2(Y)
                              | p1(Y)
                              | ! [X] :
                                  ( ~ r1(Y,X)
                                  | p4(X)
                                  | p3(X)
                                  | p2(X)
                                  | p1(X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | $false ) ) ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p2(Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | p4(X)
                              | p3(X)
                              | p2(X)
                              | p1(X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y)
                                  | p3(Y)
                                  | p2(Y)
                                  | p1(Y)
                                  | ! [X] :
                                      ( ~ r1(Y,X)
                                      | p4(X)
                                      | p3(X)
                                      | p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | $false ) ) ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p2(Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | p4(X)
                                          | p3(X)
                                          | p2(X)
                                          | p1(X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | p4(Y)
                                              | p3(Y)
                                              | p2(Y)
                                              | p1(Y)
                                              | ! [X] :
                                                  ( ~ r1(Y,X)
                                                  | p4(X)
                                                  | p3(X)
                                                  | p2(X)
                                                  | p1(X)
                                                  | ! [Y] :
                                                      ( ~ r1(X,Y)
                                                      | $false ) ) ) ) )
                                  | ~ ( p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p4(Y)
                                          | p3(Y)
                                          | p2(Y)
                                          | p1(Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | p4(X)
                                              | p3(X)
                                              | p2(X)
                                              | p1(X)
                                              | ! [Y] :
                                                  ( ~ r1(X,Y)
                                                  | p4(Y)
                                                  | p3(Y)
                                                  | p2(Y)
                                                  | p1(Y)
                                                  | ! [X] :
                                                      ( ~ r1(Y,X)
                                                      | $false ) ) ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | p4(X)
                          | p3(X)
                          | p2(X)
                          | p1(X)
                          | ! [Y] :
                              ( ~ r1(X,Y)
                              | p4(Y)
                              | p3(Y)
                              | p2(Y)
                              | p1(Y)
                              | ! [X] :
                                  ( ~ r1(Y,X)
                                  | p4(X)
                                  | p3(X)
                                  | p2(X)
                                  | p1(X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | $false ) ) ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | p4(X)
                              | p3(X)
                              | p2(X)
                              | p1(X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y)
                                  | p3(Y)
                                  | p2(Y)
                                  | p1(Y)
                                  | ! [X] :
                                      ( ~ r1(Y,X)
                                      | p4(X)
                                      | p3(X)
                                      | p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | $false ) ) ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | p4(X)
                                          | p3(X)
                                          | p2(X)
                                          | p1(X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | p4(Y)
                                              | p3(Y)
                                              | p2(Y)
                                              | p1(Y)
                                              | ! [X] :
                                                  ( ~ r1(Y,X)
                                                  | p4(X)
                                                  | p3(X)
                                                  | p2(X)
                                                  | p1(X)
                                                  | ! [Y] :
                                                      ( ~ r1(X,Y)
                                                      | $false ) ) ) ) )
                                  | ~ ( p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p4(Y)
                                          | p3(Y)
                                          | p2(Y)
                                          | p1(Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | p4(X)
                                              | p3(X)
                                              | p2(X)
                                              | p1(X)
                                              | ! [Y] :
                                                  ( ~ r1(X,Y)
                                                  | p4(Y)
                                                  | p3(Y)
                                                  | p2(Y)
                                                  | p1(Y)
                                                  | ! [X] :
                                                      ( ~ r1(Y,X)
                                                      | $false ) ) ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p4(Y)
                      | p3(Y)
                      | p2(Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | p4(X)
                          | p3(X)
                          | p2(X)
                          | p1(X)
                          | ! [Y] :
                              ( ~ r1(X,Y)
                              | p4(Y)
                              | p3(Y)
                              | p2(Y)
                              | p1(Y)
                              | ! [X] :
                                  ( ~ r1(Y,X)
                                  | $false ) ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p4(Y)
                          | p3(Y)
                          | p2(Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | p4(X)
                              | p3(X)
                              | p2(X)
                              | p1(X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y)
                                  | p3(Y)
                                  | p2(Y)
                                  | p1(Y)
                                  | ! [X] :
                                      ( ~ r1(Y,X)
                                      | $false ) ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p4(Y)
                                      | p3(Y)
                                      | p2(Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | p4(X)
                                          | p3(X)
                                          | p2(X)
                                          | p1(X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | p4(Y)
                                              | p3(Y)
                                              | p2(Y)
                                              | p1(Y)
                                              | ! [X] :
                                                  ( ~ r1(Y,X)
                                                  | $false ) ) ) )
                                  | ~ ( p4(X)
                                      | p3(X)
                                      | p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p4(Y)
                                          | p3(Y)
                                          | p2(Y)
                                          | p1(Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | p4(X)
                                              | p3(X)
                                              | p2(X)
                                              | p1(X)
                                              | ! [Y] :
                                                  ( ~ r1(X,Y)
                                                  | $false ) ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p3(Y)
                      | p2(Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | p4(X)
                          | p3(X)
                          | p2(X)
                          | p1(X)
                          | ! [Y] :
                              ( ~ r1(X,Y)
                              | p4(Y)
                              | p3(Y)
                              | p2(Y)
                              | p1(Y)
                              | ! [X] :
                                  ( ~ r1(Y,X)
                                  | $false ) ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p3(Y)
                          | p2(Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | p4(X)
                              | p3(X)
                              | p2(X)
                              | p1(X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y)
                                  | p3(Y)
                                  | p2(Y)
                                  | p1(Y)
                                  | ! [X] :
                                      ( ~ r1(Y,X)
                                      | $false ) ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p3(Y)
                                      | p2(Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | p4(X)
                                          | p3(X)
                                          | p2(X)
                                          | p1(X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | p4(Y)
                                              | p3(Y)
                                              | p2(Y)
                                              | p1(Y)
                                              | ! [X] :
                                                  ( ~ r1(Y,X)
                                                  | $false ) ) ) )
                                  | ~ ( p3(X)
                                      | p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p4(Y)
                                          | p3(Y)
                                          | p2(Y)
                                          | p1(Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | p4(X)
                                              | p3(X)
                                              | p2(X)
                                              | p1(X)
                                              | ! [Y] :
                                                  ( ~ r1(X,Y)
                                                  | $false ) ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p2(Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | p4(X)
                          | p3(X)
                          | p2(X)
                          | p1(X)
                          | ! [Y] :
                              ( ~ r1(X,Y)
                              | p4(Y)
                              | p3(Y)
                              | p2(Y)
                              | p1(Y)
                              | ! [X] :
                                  ( ~ r1(Y,X)
                                  | $false ) ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p2(Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | p4(X)
                              | p3(X)
                              | p2(X)
                              | p1(X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y)
                                  | p3(Y)
                                  | p2(Y)
                                  | p1(Y)
                                  | ! [X] :
                                      ( ~ r1(Y,X)
                                      | $false ) ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p2(Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | p4(X)
                                          | p3(X)
                                          | p2(X)
                                          | p1(X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | p4(Y)
                                              | p3(Y)
                                              | p2(Y)
                                              | p1(Y)
                                              | ! [X] :
                                                  ( ~ r1(Y,X)
                                                  | $false ) ) ) )
                                  | ~ ( p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p4(Y)
                                          | p3(Y)
                                          | p2(Y)
                                          | p1(Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | p4(X)
                                              | p3(X)
                                              | p2(X)
                                              | p1(X)
                                              | ! [Y] :
                                                  ( ~ r1(X,Y)
                                                  | $false ) ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | p4(X)
                          | p3(X)
                          | p2(X)
                          | p1(X)
                          | ! [Y] :
                              ( ~ r1(X,Y)
                              | p4(Y)
                              | p3(Y)
                              | p2(Y)
                              | p1(Y)
                              | ! [X] :
                                  ( ~ r1(Y,X)
                                  | $false ) ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | p4(X)
                              | p3(X)
                              | p2(X)
                              | p1(X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y)
                                  | p3(Y)
                                  | p2(Y)
                                  | p1(Y)
                                  | ! [X] :
                                      ( ~ r1(Y,X)
                                      | $false ) ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | p4(X)
                                          | p3(X)
                                          | p2(X)
                                          | p1(X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | p4(Y)
                                              | p3(Y)
                                              | p2(Y)
                                              | p1(Y)
                                              | ! [X] :
                                                  ( ~ r1(Y,X)
                                                  | $false ) ) ) )
                                  | ~ ( p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p4(Y)
                                          | p3(Y)
                                          | p2(Y)
                                          | p1(Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | p4(X)
                                              | p3(X)
                                              | p2(X)
                                              | p1(X)
                                              | ! [Y] :
                                                  ( ~ r1(X,Y)
                                                  | $false ) ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p4(Y)
                      | p3(Y)
                      | p2(Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | p4(X)
                          | p3(X)
                          | p2(X)
                          | p1(X)
                          | ! [Y] :
                              ( ~ r1(X,Y)
                              | $false ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p4(Y)
                          | p3(Y)
                          | p2(Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | p4(X)
                              | p3(X)
                              | p2(X)
                              | p1(X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | $false ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p4(Y)
                                      | p3(Y)
                                      | p2(Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | p4(X)
                                          | p3(X)
                                          | p2(X)
                                          | p1(X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | $false ) ) )
                                  | ~ ( p4(X)
                                      | p3(X)
                                      | p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p4(Y)
                                          | p3(Y)
                                          | p2(Y)
                                          | p1(Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | $false ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p3(Y)
                      | p2(Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | p4(X)
                          | p3(X)
                          | p2(X)
                          | p1(X)
                          | ! [Y] :
                              ( ~ r1(X,Y)
                              | $false ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p3(Y)
                          | p2(Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | p4(X)
                              | p3(X)
                              | p2(X)
                              | p1(X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | $false ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p3(Y)
                                      | p2(Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | p4(X)
                                          | p3(X)
                                          | p2(X)
                                          | p1(X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | $false ) ) )
                                  | ~ ( p3(X)
                                      | p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p4(Y)
                                          | p3(Y)
                                          | p2(Y)
                                          | p1(Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | $false ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p2(Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | p4(X)
                          | p3(X)
                          | p2(X)
                          | p1(X)
                          | ! [Y] :
                              ( ~ r1(X,Y)
                              | $false ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p2(Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | p4(X)
                              | p3(X)
                              | p2(X)
                              | p1(X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | $false ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p2(Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | p4(X)
                                          | p3(X)
                                          | p2(X)
                                          | p1(X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | $false ) ) )
                                  | ~ ( p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p4(Y)
                                          | p3(Y)
                                          | p2(Y)
                                          | p1(Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | $false ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | p4(X)
                          | p3(X)
                          | p2(X)
                          | p1(X)
                          | ! [Y] :
                              ( ~ r1(X,Y)
                              | $false ) ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | p4(X)
                              | p3(X)
                              | p2(X)
                              | p1(X)
                              | ! [Y] :
                                  ( ~ r1(X,Y)
                                  | $false ) )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | p4(X)
                                          | p3(X)
                                          | p2(X)
                                          | p1(X)
                                          | ! [Y] :
                                              ( ~ r1(X,Y)
                                              | $false ) ) )
                                  | ~ ( p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | p4(Y)
                                          | p3(Y)
                                          | p2(Y)
                                          | p1(Y)
                                          | ! [X] :
                                              ( ~ r1(Y,X)
                                              | $false ) ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p4(Y)
                      | p3(Y)
                      | p2(Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | $false ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p4(Y)
                          | p3(Y)
                          | p2(Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | $false )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p4(Y)
                                      | p3(Y)
                                      | p2(Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | $false ) )
                                  | ~ ( p4(X)
                                      | p3(X)
                                      | p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | $false ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p3(Y)
                      | p2(Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | $false ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p3(Y)
                          | p2(Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | $false )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p3(Y)
                                      | p2(Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | $false ) )
                                  | ~ ( p3(X)
                                      | p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | $false ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p2(Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | $false ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p2(Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | $false )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p2(Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | $false ) )
                                  | ~ ( p2(X)
                                      | p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | $false ) ) ) ) ) ) )
                & ( ! [Y] :
                      ( ~ r1(X,Y)
                      | p1(Y)
                      | ! [X] :
                          ( ~ r1(Y,X)
                          | $false ) )
                  | ~ ( ! [Y] :
                          ( ~ r1(X,Y)
                          | p1(Y)
                          | ! [X] :
                              ( ~ r1(Y,X)
                              | $false )
                          | ~ ( ! [X] :
                                  ( ~ r1(Y,X)
                                  | ! [Y] :
                                      ( ~ r1(X,Y)
                                      | p1(Y)
                                      | ! [X] :
                                          ( ~ r1(Y,X)
                                          | $false ) )
                                  | ~ ( p1(X)
                                      | ! [Y] :
                                          ( ~ r1(X,Y)
                                          | $false ) ) ) ) ) ) )
                & ! [Y] :
                    ( ~ r1(X,Y)
                    | ! [X] :
                        ( ~ r1(Y,X)
                        | p2(X) )
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | p2(X)
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | ! [X] :
                                        ( ~ r1(Y,X)
                                        | p2(X) )
                                    | ~ p2(Y) ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
