%------------------------------------------------------------------------------
% File     : LCL678+1.010 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Logic Calculi (Modal Logic)
% Problem  : In S4, formula provable in intuitionistic logic, size 10
% Version  : Especial.
% English  :

% Refs     : [BHS00] Balsiger et al. (2000), A Benchmark Method for the Pro
%          : [Kam08] Kaminski (2008), Email to G. Sutcliffe
% Source   : [Kam08]
% Names    : s4_ipc_p [BHS00]

% Status   : Theorem
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :    3 (   1 unit)
%            Number of atoms       :  255 (   0 equality)
%            Maximal formula depth :   34 (  14 average)
%            Number of connectives :  405 ( 153 ~  ; 151  |; 100  &)
%                                         (   0 <=>;   1 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   11 (   0 propositional; 1-2 arity)
%            Number of functors    :    0 (   0 constant; --- arity)
%            Number of variables   :  135 (   0 singleton; 134 !;   1 ?)
%            Maximal term depth    :    1 (   1 average)
% SPC      : FOF_THM_RFO_NEQ

% Comments : A naive relational encoding of the modal logic problem into
%            first-order logic.
%------------------------------------------------------------------------------
fof(reflexivity,axiom,(
    ! [X] : r1(X,X) )).

fof(transitivity,axiom,(
    ! [X,Y,Z] :
      ( ( r1(X,Y)
        & r1(Y,Z) )
     => r1(X,Z) ) )).

fof(main,conjecture,(
    ~ ( ? [X] :
          ~ ( $false
            | ~ ( ! [Y] :
                    ( ~ r1(X,Y)
                    | $false
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ( ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p10(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p9(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p8(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p7(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p6(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p5(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p3(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p1(Y) ) )
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | p10(Y) ) ) ) ) )
                & ! [Y] :
                    ( ~ r1(X,Y)
                    | $false
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ( ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p10(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p9(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p8(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p7(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p6(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p5(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p3(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p1(Y) ) )
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | p9(Y) ) ) ) ) )
                & ! [Y] :
                    ( ~ r1(X,Y)
                    | $false
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ( ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p10(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p9(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p8(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p7(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p6(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p5(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p3(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p1(Y) ) )
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | p8(Y) ) ) ) ) )
                & ! [Y] :
                    ( ~ r1(X,Y)
                    | $false
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ( ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p10(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p9(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p8(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p7(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p6(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p5(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p3(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p1(Y) ) )
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | p7(Y) ) ) ) ) )
                & ! [Y] :
                    ( ~ r1(X,Y)
                    | $false
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ( ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p10(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p9(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p8(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p7(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p6(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p5(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p3(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p1(Y) ) )
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | p6(Y) ) ) ) ) )
                & ! [Y] :
                    ( ~ r1(X,Y)
                    | $false
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ( ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p10(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p9(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p8(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p7(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p6(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p5(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p3(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p1(Y) ) )
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | p5(Y) ) ) ) ) )
                & ! [Y] :
                    ( ~ r1(X,Y)
                    | $false
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ( ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p10(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p9(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p8(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p7(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p6(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p5(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p3(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p1(Y) ) )
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | p4(Y) ) ) ) ) )
                & ! [Y] :
                    ( ~ r1(X,Y)
                    | $false
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ( ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p10(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p9(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p8(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p7(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p6(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p5(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p3(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p1(Y) ) )
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | p3(Y) ) ) ) ) )
                & ! [Y] :
                    ( ~ r1(X,Y)
                    | $false
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ( ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p10(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p9(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p8(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p7(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p6(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p5(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p3(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p1(Y) ) )
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | p2(Y) ) ) ) ) )
                & ! [Y] :
                    ( ~ r1(X,Y)
                    | $false
                    | ~ ( ! [X] :
                            ( ~ r1(Y,X)
                            | ( ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p10(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p9(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p8(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p7(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p6(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p5(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p4(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p3(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p2(Y) )
                              & ! [Y] :
                                  ( ~ r1(X,Y)
                                  | p1(Y) ) )
                            | ~ ( ! [Y] :
                                    ( ~ r1(X,Y)
                                    | p1(Y) ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
