%------------------------------------------------------------------------------
% File     : LCL713^1 : TPTP v6.4.0. Bugfixed v5.0.0.
% Domain   : Logic Calculi (Quantified multimodal logic)
% Problem  : Axiom implies accessibility relation for Euclidianity
% Version  : [Ben09] axioms.
% English  :

% Refs     : [Gol92] Goldblatt (1992), Logics of Time and Computation
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
% Source   : [Ben09]
% Names    : ex29_19.p [Ben09]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.40 v6.2.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v5.0.0
% Syntax   : Number of formulae    :   64 (   0 unit;  32 type;  31 defn)
%            Number of atoms       :  235 (  36 equality; 136 variable)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :  135 (   4   ~;   4   |;   8   &; 110   @)
%                                         (   0 <=>;   9  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  171 ( 171   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   36 (  32   :;   0   =)
%            Number of variables   :   86 (   3 sgn;  30   !;   6   ?;  50   ^)
%                                         (  86   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
% Bugfixes : v5.0.0 - Bugfix to LCL013^0.ax
%------------------------------------------------------------------------------
%----Include embedding of quantified multimodal logic in simple type theory
include('Axioms/LCL013^0.ax').
%------------------------------------------------------------------------------
thf(conj,conjecture,(
    ! [R: $i > $i > $o] :
      ( ( mvalid
        @ ( mforall_prop
          @ ^ [A: $i > $o] :
              ( mimplies @ ( mdia @ R @ A ) @ ( mbox @ R @ ( mdia @ R @ A ) ) ) ) )
     => ( meuclidean @ R ) ) )).

%------------------------------------------------------------------------------
