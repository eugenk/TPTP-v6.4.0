%------------------------------------------------------------------------------
% File     : LCL719^1 : TPTP v6.4.0. Bugfixed v5.0.0.
% Domain   : Logic Calculi (Modal logic)
% Problem  : Necessitation rule holds in monomodal logic K
% Version  : [Ben09] axioms.
% English  :

% Refs     : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
% Source   : [Ben09]
% Names    : ML_K_ex1.p [Ben09]

% Status   : Theorem
% Rating   : 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v5.0.0
% Syntax   : Number of formulae    :   70 (   0 unit;  36 type;  33 defn)
%            Number of atoms       :  240 (  38 equality; 135 variable)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :  135 (   5   ~;   5   |;   8   &; 108   @)
%                                         (   0 <=>;   9  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  179 ( 179   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   40 (  36   :;   0   =)
%            Number of variables   :   88 (   3 sgn;  30   !;   6   ?;  52   ^)
%                                         (  88   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
% Bugfixes : v5.0.0 - Bugfix to LCL013^0.ax
%------------------------------------------------------------------------------
include('Axioms/LCL013^0.ax').
include('Axioms/LCL013^1.ax').
%------------------------------------------------------------------------------
thf(phi,type,(
    phi: $i > $o )).

thf(conj,conjecture,
    ( ( mvalid @ phi )
   => ( mvalid @ ( mbox_k @ phi ) ) )).

%------------------------------------------------------------------------------
