%------------------------------------------------------------------------------
% File     : LCL860^1 : TPTP v6.4.0. Bugfixed v5.0.0.
% Domain   : Logical Calculi (Modal logic)
% Problem  : Modal logic S5(=M5) coincides with M4B5
% Version  : [Ben10] axioms.
% English  : 

% Refs     : [Ben10a] Benzmueller (2010), Email to Geoff Sutcliffe
%          : [Ben10b] Benzmueller (2010), Simple Type Theory as a Framework
% Source   : [Ben10a]
% Names    : Problem 22 [Ben10b]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.14 v6.1.0, 0.29 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v5.0.0
% Syntax   : Number of formulae    :   64 (   0 unit;  32 type;  31 defn)
%            Number of atoms       :  234 (  36 equality; 136 variable)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :  134 (   4   ~;   4   |;  12   &; 105   @)
%                                         (   1 <=>;   8  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  170 ( 170   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   36 (  32   :;   0   =)
%            Number of variables   :   85 (   3 sgn;  30   !;   6   ?;  49   ^)
%                                         (  85   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
% Bugfixes : v5.0.0 - Bugfix to LCL013^0.ax
%------------------------------------------------------------------------------
%----Include the definitions for quantified multimodal logic
include('Axioms/LCL013^0.ax').
%------------------------------------------------------------------------------
thf(conj,conjecture,(
    ! [R: $i > $i > $o] :
      ( ( ( mreflexive @ R )
        & ( meuclidean @ R ) )
    <=> ( ( mreflexive @ R )
        & ( mtransitive @ R )
        & ( msymmetric @ R )
        & ( meuclidean @ R ) ) ) )).

%------------------------------------------------------------------------------
