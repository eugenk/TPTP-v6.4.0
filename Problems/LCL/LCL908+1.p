%------------------------------------------------------------------------------
% File     : LCL908+1 : TPTP v6.4.0. Released v6.4.0.
% Domain   : Logical Calculi (Propositional)
% Problem  : Lukasiewicz's axiomatization of propositional logic
% Version  : [Zem73] axioms.
% English  :

% Refs     : [Zem73] Zeman (1973), Modal Logic, the Lewis-Modal systems
%          : [Hal]   Halleck (URL), John Halleck's Logic Systems
%          : [She06] Shen (2006), Automated Proofs of Equivalence of Modal
% Source   : [TPTP]
% Names    :

% Status   : Satisfiable
% Rating   : 0.00 v6.4.0
% Syntax   : Number of formulae    :   39 (   8 unit)
%            Number of atoms       :   73 (   6 equality)
%            Maximal formula depth :    6 (   4 average)
%            Number of connectives :   34 (   0   ~;   0   |;   1   &)
%                                         (  26 <=>;   7  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   34 (  32 propositional; 0-2 arity)
%            Number of functors    :    5 (   0 constant; 1-2 arity)
%            Number of variables   :   65 (   0 sgn;  65   !;   0   ?)
%            Maximal term depth    :    5 (   3 average)
% SPC      : FOF_SAT_RFO_SEQ

% Comments : 
%------------------------------------------------------------------------------
%----Propositional logic rules and axioms
include('Axioms/LCL006+0.ax').
%----Propositional logic definitions
include('Axioms/LCL006+1.ax').
%----Lukasiewicz's axiomatization of propositional logic
include('Axioms/LCL006+3.ax').
%------------------------------------------------------------------------------
