%------------------------------------------------------------------------------
% File     : LCL909+1 : TPTP v6.4.0. Released v6.4.0.
% Domain   : Logical Calculi (Propositional)
% Problem  : Principia's axiomatization of propositional logic
% Version  : [RW10] axioms.
% English  :

% Refs     : [RW10]  Russell & Whitehead (1910), Principia Mathmatica
%          : [Hal]   Halleck (URL), John Halleck's Logic Systems
%          : [She06] Shen (2006), Automated Proofs of Equivalence of Modal
% Source   : [TPTP]
% Names    :

% Status   : Satisfiable
% Rating   : 0.00 v6.4.0
% Syntax   : Number of formulae    :   41 (  10 unit)
%            Number of atoms       :   75 (   6 equality)
%            Maximal formula depth :    6 (   3 average)
%            Number of connectives :   34 (   0   ~;   0   |;   1   &)
%                                         (  26 <=>;   7  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   33 (  31 propositional; 0-2 arity)
%            Number of functors    :    5 (   0 constant; 1-2 arity)
%            Number of variables   :   65 (   0 sgn;  65   !;   0   ?)
%            Maximal term depth    :    5 (   3 average)
% SPC      : FOF_SAT_RFO_SEQ

% Comments : 
%------------------------------------------------------------------------------
%----Propositional logic rules and axioms
include('Axioms/LCL006+0.ax').
%----Propositional logic definitions
include('Axioms/LCL006+1.ax').
%----Principia's axiomatization of propositional logic
include('Axioms/LCL006+4.ax').
%------------------------------------------------------------------------------
