%--------------------------------------------------------------------------
% File     : NLP127-1 : TPTP v6.4.0. Released v2.4.0.
% Domain   : Natural Language Processing
% Problem  : An old dirty white Chevy, problem 14
% Version  : [Bos00b] axioms.
% English  : Eliminating inconsistent interpretations in the statement
%            "An old dirty white chevy barrels down a lonely street in
%            hollywood."

% Refs     : [Bos00a] Bos (2000), DORIS: Discourse Oriented Representation a
%            [Bos00b] Bos (2000), Applied Theorem Proving - Natural Language
% Source   : [TPTP]
% Names    :

% Status   : Satisfiable
% Rating   : 0.00 v2.4.0
% Syntax   : Number of clauses     :   52 (   0 non-Horn;  17 unit;  52 RR)
%            Number of atoms       :   91 (   1 equality)
%            Maximal clause size   :    6 (   2 average)
%            Number of predicates  :   41 (   0 propositional; 1-3 arity)
%            Number of functors    :    6 (   6 constant; 0-0 arity)
%            Number of variables   :   72 (   0 singleton)
%            Maximal term depth    :    1 (   1 average)
% SPC      : CNF_SAT_EPR

% Comments : Created from NLP127+1.p using FLOTTER
%--------------------------------------------------------------------------
cnf(clause1,axiom,
    ( ~ barrel(U,V)
    | event(U,V) )).

cnf(clause2,axiom,
    ( ~ event(U,V)
    | eventuality(U,V) )).

cnf(clause3,axiom,
    ( ~ eventuality(U,V)
    | thing(U,V) )).

cnf(clause4,axiom,
    ( ~ thing(U,V)
    | singleton(U,V) )).

cnf(clause5,axiom,
    ( ~ eventuality(U,V)
    | specific(U,V) )).

cnf(clause6,axiom,
    ( ~ eventuality(U,V)
    | nonexistent(U,V) )).

cnf(clause7,axiom,
    ( ~ eventuality(U,V)
    | unisex(U,V) )).

cnf(clause8,axiom,
    ( ~ street(U,V)
    | way(U,V) )).

cnf(clause9,axiom,
    ( ~ way(U,V)
    | artifact(U,V) )).

cnf(clause10,axiom,
    ( ~ artifact(U,V)
    | object(U,V) )).

cnf(clause11,axiom,
    ( ~ object(U,V)
    | entity(U,V) )).

cnf(clause12,axiom,
    ( ~ entity(U,V)
    | thing(U,V) )).

cnf(clause13,axiom,
    ( ~ entity(U,V)
    | specific(U,V) )).

cnf(clause14,axiom,
    ( ~ entity(U,V)
    | existent(U,V) )).

cnf(clause15,axiom,
    ( ~ object(U,V)
    | nonliving(U,V) )).

cnf(clause16,axiom,
    ( ~ object(U,V)
    | impartial(U,V) )).

cnf(clause17,axiom,
    ( ~ object(U,V)
    | unisex(U,V) )).

cnf(clause18,axiom,
    ( ~ placename(U,V)
    | relname(U,V) )).

cnf(clause19,axiom,
    ( ~ relname(U,V)
    | relation(U,V) )).

cnf(clause20,axiom,
    ( ~ relation(U,V)
    | abstraction(U,V) )).

cnf(clause21,axiom,
    ( ~ abstraction(U,V)
    | thing(U,V) )).

cnf(clause22,axiom,
    ( ~ abstraction(U,V)
    | nonhuman(U,V) )).

cnf(clause23,axiom,
    ( ~ abstraction(U,V)
    | general(U,V) )).

cnf(clause24,axiom,
    ( ~ abstraction(U,V)
    | unisex(U,V) )).

cnf(clause25,axiom,
    ( ~ hollywood_placename(U,V)
    | placename(U,V) )).

cnf(clause26,axiom,
    ( ~ city(U,V)
    | location(U,V) )).

cnf(clause27,axiom,
    ( ~ location(U,V)
    | object(U,V) )).

cnf(clause28,axiom,
    ( ~ chevy(U,V)
    | car(U,V) )).

cnf(clause29,axiom,
    ( ~ car(U,V)
    | vehicle(U,V) )).

cnf(clause30,axiom,
    ( ~ vehicle(U,V)
    | transport(U,V) )).

cnf(clause31,axiom,
    ( ~ transport(U,V)
    | instrumentality(U,V) )).

cnf(clause32,axiom,
    ( ~ instrumentality(U,V)
    | artifact(U,V) )).

cnf(clause33,axiom,
    ( ~ general(U,V)
    | ~ specific(U,V) )).

cnf(clause34,axiom,
    ( ~ nonexistent(U,V)
    | ~ existent(U,V) )).

cnf(clause35,axiom,
    ( ~ placename(U,V)
    | ~ of(U,W,X)
    | ~ placename(U,W)
    | ~ of(U,V,X)
    | ~ entity(U,X)
    | W = V )).

cnf(clause36,negated_conjecture,
    ( actual_world(skc6) )).

cnf(clause37,negated_conjecture,
    ( chevy(skc6,skc11) )).

cnf(clause38,negated_conjecture,
    ( city(skc6,skc10) )).

cnf(clause39,negated_conjecture,
    ( lonely(skc6,skc8) )).

cnf(clause40,negated_conjecture,
    ( street(skc6,skc8) )).

cnf(clause41,negated_conjecture,
    ( placename(skc6,skc9) )).

cnf(clause42,negated_conjecture,
    ( hollywood_placename(skc6,skc9) )).

cnf(clause43,negated_conjecture,
    ( event(skc6,skc7) )).

cnf(clause44,negated_conjecture,
    ( present(skc6,skc7) )).

cnf(clause45,negated_conjecture,
    ( barrel(skc6,skc7) )).

cnf(clause46,negated_conjecture,
    ( white(skc6,skc11) )).

cnf(clause47,negated_conjecture,
    ( dirty(skc6,skc11) )).

cnf(clause48,negated_conjecture,
    ( old(skc6,skc11) )).

cnf(clause49,negated_conjecture,
    ( of(skc6,skc9,skc10) )).

cnf(clause50,negated_conjecture,
    ( in(skc6,skc7,skc10) )).

cnf(clause51,negated_conjecture,
    ( down(skc6,skc7,skc8) )).

cnf(clause52,negated_conjecture,
    ( agent(skc6,skc7,skc11) )).

%--------------------------------------------------------------------------
