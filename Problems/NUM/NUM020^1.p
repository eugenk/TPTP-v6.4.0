%------------------------------------------------------------------------------
% File     : NUM020^1 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Number Theory
% Problem  : Find N such that N * 3 = 6
% Version  : [Ben08] axioms : Especial.
% English  :

% Refs     : [Ben08] Benzmueller (2008), Email to G. Sutcliffe
% Source   : [Ben08]
% Names    : CHURCH_NUM_6 [Ben08]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.14 v6.1.0, 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :   29 (   0 unit;  14 type;  14 defn)
%            Number of atoms       :  112 (  15 equality;  80 variable)
%            Maximal formula depth :   14 (   7 average)
%            Number of connectives :   67 (   0   ~;   0   |;   0   &;  67   @)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   94 (  94   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   15 (  14   :;   0   =)
%            Number of variables   :   34 (   1 sgn;   0   !;   1   ?;  33   ^)
%                                         (  34   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include church numerals definitions
include('Axioms/NUM006^0.ax').
%------------------------------------------------------------------------------
thf(thm,conjecture,(
    ? [N: ( $i > $i ) > $i > $i] :
      ( ( mult @ N @ three )
      = six ) )).

%------------------------------------------------------------------------------
