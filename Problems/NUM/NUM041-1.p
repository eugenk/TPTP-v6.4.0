%--------------------------------------------------------------------------
% File     : NUM041-1 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Number Theory (Ordinals)
% Problem  : Irreflexive class property 3
% Version  : [Qua92] axioms.
% English  :

% Refs     : [Qua92] Quaife (1992), Automated Deduction in von Neumann-Bern
% Source   : [Quaife]
% Names    : IR3.1 [Quaife]

% Status   : Unknown
% Rating   : 1.00 v2.1.0
% Syntax   : Number of clauses     :  160 (  12 non-Horn;  48 unit; 121 RR)
%            Number of atoms       :  324 (  71 equality)
%            Maximal clause size   :    5 (   2 average)
%            Number of predicates  :   17 (   0 propositional; 1-3 arity)
%            Number of functors    :   63 (  19 constant; 0-3 arity)
%            Number of variables   :  303 (  40 singleton)
%            Maximal term depth    :    6 (   2 average)
% SPC      : CNF_UNK_RFO_SEQ_NHN

% Comments : Not in [Qua92]. Theorem IR3 in [Quaife].
%          : Quaife proves all these problems by augmenting the axioms with
%            all previously proved theorems. The user may create an augmented
%            version of this problem by adding all previously proved theorems.
%            These include all of [Qua92]'s set theory and Boolean algebra
%            theorems, available from the SET domain.
% Bugfixes : v1.0.1 - Bugfix in SET004-1.ax.
%          : v2.1.0 - Bugfix in SET004-0.ax.
%--------------------------------------------------------------------------
%----Include von Neuman-Bernays-Godel set theory axioms
include('Axioms/SET004-0.ax').
%----Include Set theory (Boolean algebra) axioms based on NBG set theory
include('Axioms/SET004-1.ax').
%----Include ordinal number theory axioms.
include('Axioms/NUM004-0.ax').
%--------------------------------------------------------------------------
cnf(prove_irreflexive_class_property3_1,negated_conjecture,
    ( irreflexive(x,domain_of(symmetrization_of(x))) )).

cnf(prove_irreflexive_class_property3_2,negated_conjecture,
    ( ~ subclass(x,complement(identity_relation)) )).

%--------------------------------------------------------------------------
