%--------------------------------------------------------------------------
% File     : NUM154-1 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Number Theory (Ordinals)
% Problem  : Corollary 5 to successor property 1
% Version  : [Qua92] axioms.
% English  :

% Refs     : [Qua92] Quaife (1992), Automated Deduction in von Neumann-Bern
% Source   : [Quaife]
% Names    : SUC6 cor. 5 [Quaife]

% Status   : Unsatisfiable
% Rating   : 0.60 v6.3.0, 0.55 v6.2.0, 0.60 v6.1.0, 0.71 v6.0.0, 0.80 v5.5.0, 0.90 v5.3.0, 0.94 v5.2.0, 0.88 v5.0.0, 0.86 v4.1.0, 0.85 v4.0.1, 0.82 v3.7.0, 0.80 v3.5.0, 0.82 v3.4.0, 0.83 v3.3.0, 0.79 v3.2.0, 0.69 v3.1.0, 0.73 v2.7.0, 0.75 v2.6.0, 0.67 v2.5.0, 0.73 v2.4.0, 0.88 v2.3.0, 1.00 v2.1.0
% Syntax   : Number of clauses     :  159 (  12 non-Horn;  47 unit; 120 RR)
%            Number of atoms       :  323 (  72 equality)
%            Maximal clause size   :    5 (   2 average)
%            Number of predicates  :   17 (   0 propositional; 1-3 arity)
%            Number of functors    :   63 (  19 constant; 0-3 arity)
%            Number of variables   :  303 (  40 singleton)
%            Maximal term depth    :    6 (   2 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments : Not in [Qua92]. Theorem SUC6 cor. 5 in [Quaife].
%          : Quaife proves all these problems by augmenting the axioms with
%            all previously proved theorems. The user may create an augmented
%            version of this problem by adding all previously proved theorems.
%            These include all of [Qua92]'s set theory and Boolean algebra
%            theorems, available from the SET domain.
% Bugfixes : v1.0.1 - Bugfix in SET004-1.ax.
%          : v2.1.0 - Bugfix in SET004-0.ax.
%--------------------------------------------------------------------------
%----Include von Neuman-Bernays-Godel set theory axioms
include('Axioms/SET004-0.ax').
%----Include Set theory (Boolean algebra) axioms based on NBG set theory
include('Axioms/SET004-1.ax').
%----Include ordinal number theory axioms.
include('Axioms/NUM004-0.ax').
%--------------------------------------------------------------------------
cnf(prove_corollary_5_to_successor_property1_1,negated_conjecture,
    ( successor(x) = null_class )).

%--------------------------------------------------------------------------
