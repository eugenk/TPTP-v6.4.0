%--------------------------------------------------------------------------
% File     : NUM231-1 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Number Theory (Ordinals)
% Problem  : Transfinite recursion lemma 2
% Version  : [Qua92] axioms.
% English  :

% Refs     : [Qua92] Quaife (1992), Automated Deduction in von Neumann-Bern
% Source   : [Quaife]
% Names    : TREC.LEMMA2 [Quaife]

% Status   : Unknown
% Rating   : 1.00 v2.1.0
% Syntax   : Number of clauses     :  164 (  12 non-Horn;  52 unit; 125 RR)
%            Number of atoms       :  328 (  71 equality)
%            Maximal clause size   :    5 (   2 average)
%            Number of predicates  :   17 (   0 propositional; 1-3 arity)
%            Number of functors    :   67 (  23 constant; 0-3 arity)
%            Number of variables   :  303 (  40 singleton)
%            Maximal term depth    :    6 (   2 average)
% SPC      : CNF_UNK_RFO_SEQ_NHN

% Comments : Not in [Qua92]. Theorem TREC.LEMMA2 in [Quaife].
%          : Quaife proves all these problems by augmenting the axioms with
%            all previously proved theorems. The user may create an augmented
%            version of this problem by adding all previously proved theorems.
%            These include all of [Qua92]'s set theory and Boolean algebra
%            theorems, available from the SET domain.
% Bugfixes : v1.0.1 - Bugfix in SET004-1.ax.
%          : v2.1.0 - Bugfix in SET004-0.ax.
%--------------------------------------------------------------------------
%----Include von Neuman-Bernays-Godel set theory axioms
include('Axioms/SET004-0.ax').
%----Include Set theory (Boolean algebra) axioms based on NBG set theory
include('Axioms/SET004-1.ax').
%----Include ordinal number theory axioms.
include('Axioms/NUM004-0.ax').
%--------------------------------------------------------------------------
cnf(prove_transfinite_recursion_lemma2_1,negated_conjecture,
    ( member(x,recursion_equation_functions(z)) )).

cnf(prove_transfinite_recursion_lemma2_2,negated_conjecture,
    ( member(y,recursion_equation_functions(z)) )).

cnf(prove_transfinite_recursion_lemma2_3,negated_conjecture,
    ( member(ordered_pair(u,v),y) )).

cnf(prove_transfinite_recursion_lemma2_4,negated_conjecture,
    ( member(u,least(element_relation,domain_of(intersection(complement(y),x)))) )).

cnf(prove_transfinite_recursion_lemma2_5,negated_conjecture,
    ( ~ subclass(x,y) )).

cnf(prove_transfinite_recursion_lemma2_6,negated_conjecture,
    ( ~ member(ordered_pair(u,v),x) )).

%--------------------------------------------------------------------------
