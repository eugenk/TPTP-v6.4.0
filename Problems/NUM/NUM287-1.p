%--------------------------------------------------------------------------
% File     : NUM287-1 : TPTP v6.4.0. Bugfixed v4.0.0.
% Domain   : Number Theory
% Problem  : Number theory less axioms
% Version  : [LS74] axioms.
% English  :

% Refs     : [LS74]  Lawrence & Starkey (1974), Experimental Tests of Resol
% Source   : [SPRFN]
% Names    :

% Status   : Satisfiable
% Rating   : 0.00 v5.4.0, 0.67 v5.3.0, 0.71 v5.0.0, 0.50 v4.1.0, 0.43 v4.0.0
% Syntax   : Number of clauses     :    9 (   0 non-Horn;   4 unit;   5 RR)
%            Number of atoms       :   15 (   0 equality)
%            Maximal clause size   :    3 (   2 average)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :    5 (   1 constant; 0-2 arity)
%            Number of variables   :   18 (   2 singleton)
%            Maximal term depth    :    4 (   2 average)
% SPC      : CNF_SAT_RFO_NEQ

% Comments :
% Bugfixes : v4.0.0 - Bugfix in NUM001-0.ax
%--------------------------------------------------------------------------
%----Include Number theory axioms
include('Axioms/NUM001-0.ax').
%----Include Number theory less axioms
include('Axioms/NUM001-1.ax').
%--------------------------------------------------------------------------
