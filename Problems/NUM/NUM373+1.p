%------------------------------------------------------------------------------
% File     : NUM373+1 : TPTP v6.4.0. Released v3.1.0.
% Domain   : Number Theory (RDN arithmetic)
% Problem  : ?XYZ, (X+Y) = (Y+X)
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : CounterSatisfiable
% Rating   : 1.00 v3.1.0
% Syntax   : Number of formulae    :  402 ( 374 unit)
%            Number of atoms       :  475 (   6 equality)
%            Maximal formula depth :   19 (   1 average)
%            Number of connectives :   75 (   2 ~  ;   1  |;  44  &)
%                                         (   3 <=>;  25 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   11 (   0 propositional; 1-4 arity)
%            Number of functors    :  260 ( 256 constant; 0-2 arity)
%            Number of variables   :  125 (   0 singleton; 125 !;   0 ?)
%            Maximal term depth    :    5 (   2 average)
% SPC      : FOF_CSA_RFO_SEQ

% Comments :
%------------------------------------------------------------------------------
%----Include axioms for RDN arithmetic
include('Axioms/NUM005+0.ax').
include('Axioms/NUM005+1.ax').
include('Axioms/NUM005+2.ax').
%------------------------------------------------------------------------------
fof(communative,conjecture,
    ( ! [X,Y,Z1,Z2] :
        ( ( sum(X,Y,Z1)
          & sum(Y,X,Z2) )
       => Z1 = Z2 ) )).

%------------------------------------------------------------------------------
