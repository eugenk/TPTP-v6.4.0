%------------------------------------------------------------------------------
% File     : NUM470+2 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Number Theory
% Problem  : Square root of a prime is irrational 10, 01 expansion
% Version  : Especial.
% English  :

% Refs     : [LPV06] Lyaletski et al. (2006), SAD as a Mathematical Assista
%          : [VLP07] Verchinine et al. (2007), System for Automated Deduction
%          : [Pas08] Paskevich (2008), Email to G. Sutcliffe
% Source   : [Pas08]
% Names    : primes_10.01 [Pas08]

% Status   : Theorem
% Rating   : 1.00 v6.2.0, 0.96 v6.1.0, 1.00 v6.0.0, 0.96 v5.5.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :   36 (   1 unit)
%            Number of atoms       :  164 (  50 equality)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :  142 (  14 ~  ;   7  |;  68  &)
%                                         (   4 <=>;  49 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    5 (   0 propositional; 1-2 arity)
%            Number of functors    :    9 (   5 constant; 0-2 arity)
%            Number of variables   :   73 (   0 singleton;  68 !;   5 ?)
%            Maximal term depth    :    3 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Problem generated by the SAD system [VLP07]
%------------------------------------------------------------------------------
fof(mNatSort,axiom,(
    ! [W0] :
      ( aNaturalNumber0(W0)
     => $true ) )).

fof(mSortsC,axiom,(
    aNaturalNumber0(sz00) )).

fof(mSortsC_01,axiom,
    ( aNaturalNumber0(sz10)
    & sz10 != sz00 )).

fof(mSortsB,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => aNaturalNumber0(sdtpldt0(W0,W1)) ) )).

fof(mSortsB_02,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => aNaturalNumber0(sdtasdt0(W0,W1)) ) )).

fof(mAddComm,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => sdtpldt0(W0,W1) = sdtpldt0(W1,W0) ) )).

fof(mAddAsso,axiom,(
    ! [W0,W1,W2] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1)
        & aNaturalNumber0(W2) )
     => sdtpldt0(sdtpldt0(W0,W1),W2) = sdtpldt0(W0,sdtpldt0(W1,W2)) ) )).

fof(m_AddZero,axiom,(
    ! [W0] :
      ( aNaturalNumber0(W0)
     => ( sdtpldt0(W0,sz00) = W0
        & W0 = sdtpldt0(sz00,W0) ) ) )).

fof(mMulComm,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => sdtasdt0(W0,W1) = sdtasdt0(W1,W0) ) )).

fof(mMulAsso,axiom,(
    ! [W0,W1,W2] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1)
        & aNaturalNumber0(W2) )
     => sdtasdt0(sdtasdt0(W0,W1),W2) = sdtasdt0(W0,sdtasdt0(W1,W2)) ) )).

fof(m_MulUnit,axiom,(
    ! [W0] :
      ( aNaturalNumber0(W0)
     => ( sdtasdt0(W0,sz10) = W0
        & W0 = sdtasdt0(sz10,W0) ) ) )).

fof(m_MulZero,axiom,(
    ! [W0] :
      ( aNaturalNumber0(W0)
     => ( sdtasdt0(W0,sz00) = sz00
        & sz00 = sdtasdt0(sz00,W0) ) ) )).

fof(mAMDistr,axiom,(
    ! [W0,W1,W2] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1)
        & aNaturalNumber0(W2) )
     => ( sdtasdt0(W0,sdtpldt0(W1,W2)) = sdtpldt0(sdtasdt0(W0,W1),sdtasdt0(W0,W2))
        & sdtasdt0(sdtpldt0(W1,W2),W0) = sdtpldt0(sdtasdt0(W1,W0),sdtasdt0(W2,W0)) ) ) )).

fof(mAddCanc,axiom,(
    ! [W0,W1,W2] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1)
        & aNaturalNumber0(W2) )
     => ( ( sdtpldt0(W0,W1) = sdtpldt0(W0,W2)
          | sdtpldt0(W1,W0) = sdtpldt0(W2,W0) )
       => W1 = W2 ) ) )).

fof(mMulCanc,axiom,(
    ! [W0] :
      ( aNaturalNumber0(W0)
     => ( W0 != sz00
       => ! [W1,W2] :
            ( ( aNaturalNumber0(W1)
              & aNaturalNumber0(W2) )
           => ( ( sdtasdt0(W0,W1) = sdtasdt0(W0,W2)
                | sdtasdt0(W1,W0) = sdtasdt0(W2,W0) )
             => W1 = W2 ) ) ) ) )).

fof(mZeroAdd,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( sdtpldt0(W0,W1) = sz00
       => ( W0 = sz00
          & W1 = sz00 ) ) ) )).

fof(mZeroMul,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( sdtasdt0(W0,W1) = sz00
       => ( W0 = sz00
          | W1 = sz00 ) ) ) )).

fof(mDefLE,definition,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( sdtlseqdt0(W0,W1)
      <=> ? [W2] :
            ( aNaturalNumber0(W2)
            & sdtpldt0(W0,W2) = W1 ) ) ) )).

fof(mDefDiff,definition,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( sdtlseqdt0(W0,W1)
       => ! [W2] :
            ( W2 = sdtmndt0(W1,W0)
          <=> ( aNaturalNumber0(W2)
              & sdtpldt0(W0,W2) = W1 ) ) ) ) )).

fof(mLERefl,axiom,(
    ! [W0] :
      ( aNaturalNumber0(W0)
     => sdtlseqdt0(W0,W0) ) )).

fof(mLEAsym,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( ( sdtlseqdt0(W0,W1)
          & sdtlseqdt0(W1,W0) )
       => W0 = W1 ) ) )).

fof(mLETran,axiom,(
    ! [W0,W1,W2] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1)
        & aNaturalNumber0(W2) )
     => ( ( sdtlseqdt0(W0,W1)
          & sdtlseqdt0(W1,W2) )
       => sdtlseqdt0(W0,W2) ) ) )).

fof(mLETotal,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( sdtlseqdt0(W0,W1)
        | ( W1 != W0
          & sdtlseqdt0(W1,W0) ) ) ) )).

fof(mMonAdd,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( ( W0 != W1
          & sdtlseqdt0(W0,W1) )
       => ! [W2] :
            ( aNaturalNumber0(W2)
           => ( sdtpldt0(W2,W0) != sdtpldt0(W2,W1)
              & sdtlseqdt0(sdtpldt0(W2,W0),sdtpldt0(W2,W1))
              & sdtpldt0(W0,W2) != sdtpldt0(W1,W2)
              & sdtlseqdt0(sdtpldt0(W0,W2),sdtpldt0(W1,W2)) ) ) ) ) )).

fof(mMonMul,axiom,(
    ! [W0,W1,W2] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1)
        & aNaturalNumber0(W2) )
     => ( ( W0 != sz00
          & W1 != W2
          & sdtlseqdt0(W1,W2) )
       => ( sdtasdt0(W0,W1) != sdtasdt0(W0,W2)
          & sdtlseqdt0(sdtasdt0(W0,W1),sdtasdt0(W0,W2))
          & sdtasdt0(W1,W0) != sdtasdt0(W2,W0)
          & sdtlseqdt0(sdtasdt0(W1,W0),sdtasdt0(W2,W0)) ) ) ) )).

fof(mLENTr,axiom,(
    ! [W0] :
      ( aNaturalNumber0(W0)
     => ( W0 = sz00
        | W0 = sz10
        | ( sz10 != W0
          & sdtlseqdt0(sz10,W0) ) ) ) )).

fof(mMonMul2,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( W0 != sz00
       => sdtlseqdt0(W1,sdtasdt0(W1,W0)) ) ) )).

fof(mIH,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( iLess0(W0,W1)
       => $true ) ) )).

fof(mIH_03,axiom,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( ( W0 != W1
          & sdtlseqdt0(W0,W1) )
       => iLess0(W0,W1) ) ) )).

fof(mDefDiv,definition,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( doDivides0(W0,W1)
      <=> ? [W2] :
            ( aNaturalNumber0(W2)
            & W1 = sdtasdt0(W0,W2) ) ) ) )).

fof(mDefQuot,definition,(
    ! [W0,W1] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1) )
     => ( ( W0 != sz00
          & doDivides0(W0,W1) )
       => ! [W2] :
            ( W2 = sdtsldt0(W1,W0)
          <=> ( aNaturalNumber0(W2)
              & W1 = sdtasdt0(W0,W2) ) ) ) ) )).

fof(mDivTrans,axiom,(
    ! [W0,W1,W2] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1)
        & aNaturalNumber0(W2) )
     => ( ( doDivides0(W0,W1)
          & doDivides0(W1,W2) )
       => doDivides0(W0,W2) ) ) )).

fof(mDivSum,axiom,(
    ! [W0,W1,W2] :
      ( ( aNaturalNumber0(W0)
        & aNaturalNumber0(W1)
        & aNaturalNumber0(W2) )
     => ( ( doDivides0(W0,W1)
          & doDivides0(W0,W2) )
       => doDivides0(W0,sdtpldt0(W1,W2)) ) ) )).

fof(m__1324,hypothesis,
    ( aNaturalNumber0(xl)
    & aNaturalNumber0(xm)
    & aNaturalNumber0(xn) )).

fof(m__1324_04,hypothesis,
    ( ? [W0] :
        ( aNaturalNumber0(W0)
        & xm = sdtasdt0(xl,W0) )
    & doDivides0(xl,xm)
    & ? [W0] :
        ( aNaturalNumber0(W0)
        & sdtpldt0(xm,xn) = sdtasdt0(xl,W0) )
    & doDivides0(xl,sdtpldt0(xm,xn)) )).

fof(m__,conjecture,
    ( ? [W0] :
        ( aNaturalNumber0(W0)
        & xn = sdtasdt0(xl,W0) )
    | doDivides0(xl,xn) )).

%------------------------------------------------------------------------------
