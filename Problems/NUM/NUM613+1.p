%------------------------------------------------------------------------------
% File     : NUM613+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Number Theory
% Problem  : Ramsey's Infinite Theorem 15_02_23_07_02, 00 expansion
% Version  : Especial.
% English  :

% Refs     : [VLP07] Verchinine et al. (2007), System for Automated Deduction
%          : [Pas08] Paskevich (2008), Email to G. Sutcliffe
% Source   : [Pas08]
% Names    : ramsey_15_02_23_07_02.00 [Pas08]

% Status   : Theorem
% Rating   : 0.23 v6.4.0, 0.27 v6.3.0, 0.25 v6.2.0, 0.28 v6.1.0, 0.40 v6.0.0, 0.26 v5.5.0, 0.41 v5.4.0, 0.50 v5.3.0, 0.56 v5.2.0, 0.40 v5.1.0, 0.48 v5.0.0, 0.54 v4.1.0, 0.61 v4.0.1, 0.83 v4.0.0
% Syntax   : Number of formulae    :  110 (  17 unit)
%            Number of atoms       :  402 (  74 equality)
%            Maximal formula depth :   15 (   5 average)
%            Number of connectives :  317 (  25 ~  ;   4  |; 131  &)
%                                         (  22 <=>; 135 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   10 (   0 propositional; 1-2 arity)
%            Number of functors    :   30 (  16 constant; 0-2 arity)
%            Number of variables   :  171 (   0 singleton; 159 !;  12 ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Problem generated by the SAD system [VLP07]
%------------------------------------------------------------------------------
fof(mSetSort,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => $true ) )).

fof(mElmSort,axiom,(
    ! [W0] :
      ( aElement0(W0)
     => $true ) )).

fof(mEOfElem,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => ! [W1] :
          ( aElementOf0(W1,W0)
         => aElement0(W1) ) ) )).

fof(mFinRel,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => ( isFinite0(W0)
       => $true ) ) )).

fof(mDefEmp,definition,(
    ! [W0] :
      ( W0 = slcrc0
    <=> ( aSet0(W0)
        & ~ ? [W1] : aElementOf0(W1,W0) ) ) )).

fof(mEmpFin,axiom,(
    isFinite0(slcrc0) )).

fof(mCntRel,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => ( isCountable0(W0)
       => $true ) ) )).

fof(mCountNFin,axiom,(
    ! [W0] :
      ( ( aSet0(W0)
        & isCountable0(W0) )
     => ~ isFinite0(W0) ) )).

fof(mCountNFin_01,axiom,(
    ! [W0] :
      ( ( aSet0(W0)
        & isCountable0(W0) )
     => W0 != slcrc0 ) )).

fof(mDefSub,definition,(
    ! [W0] :
      ( aSet0(W0)
     => ! [W1] :
          ( aSubsetOf0(W1,W0)
        <=> ( aSet0(W1)
            & ! [W2] :
                ( aElementOf0(W2,W1)
               => aElementOf0(W2,W0) ) ) ) ) )).

fof(mSubFSet,axiom,(
    ! [W0] :
      ( ( aSet0(W0)
        & isFinite0(W0) )
     => ! [W1] :
          ( aSubsetOf0(W1,W0)
         => isFinite0(W1) ) ) )).

fof(mSubRefl,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => aSubsetOf0(W0,W0) ) )).

fof(mSubASymm,axiom,(
    ! [W0,W1] :
      ( ( aSet0(W0)
        & aSet0(W1) )
     => ( ( aSubsetOf0(W0,W1)
          & aSubsetOf0(W1,W0) )
       => W0 = W1 ) ) )).

fof(mSubTrans,axiom,(
    ! [W0,W1,W2] :
      ( ( aSet0(W0)
        & aSet0(W1)
        & aSet0(W2) )
     => ( ( aSubsetOf0(W0,W1)
          & aSubsetOf0(W1,W2) )
       => aSubsetOf0(W0,W2) ) ) )).

fof(mDefCons,definition,(
    ! [W0,W1] :
      ( ( aSet0(W0)
        & aElement0(W1) )
     => ! [W2] :
          ( W2 = sdtpldt0(W0,W1)
        <=> ( aSet0(W2)
            & ! [W3] :
                ( aElementOf0(W3,W2)
              <=> ( aElement0(W3)
                  & ( aElementOf0(W3,W0)
                    | W3 = W1 ) ) ) ) ) ) )).

fof(mDefDiff,definition,(
    ! [W0,W1] :
      ( ( aSet0(W0)
        & aElement0(W1) )
     => ! [W2] :
          ( W2 = sdtmndt0(W0,W1)
        <=> ( aSet0(W2)
            & ! [W3] :
                ( aElementOf0(W3,W2)
              <=> ( aElement0(W3)
                  & aElementOf0(W3,W0)
                  & W3 != W1 ) ) ) ) ) )).

fof(mConsDiff,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => ! [W1] :
          ( aElementOf0(W1,W0)
         => sdtpldt0(sdtmndt0(W0,W1),W1) = W0 ) ) )).

fof(mDiffCons,axiom,(
    ! [W0,W1] :
      ( ( aElement0(W0)
        & aSet0(W1) )
     => ( ~ aElementOf0(W0,W1)
       => sdtmndt0(sdtpldt0(W1,W0),W0) = W1 ) ) )).

fof(mCConsSet,axiom,(
    ! [W0] :
      ( aElement0(W0)
     => ! [W1] :
          ( ( aSet0(W1)
            & isCountable0(W1) )
         => isCountable0(sdtpldt0(W1,W0)) ) ) )).

fof(mCDiffSet,axiom,(
    ! [W0] :
      ( aElement0(W0)
     => ! [W1] :
          ( ( aSet0(W1)
            & isCountable0(W1) )
         => isCountable0(sdtmndt0(W1,W0)) ) ) )).

fof(mFConsSet,axiom,(
    ! [W0] :
      ( aElement0(W0)
     => ! [W1] :
          ( ( aSet0(W1)
            & isFinite0(W1) )
         => isFinite0(sdtpldt0(W1,W0)) ) ) )).

fof(mFDiffSet,axiom,(
    ! [W0] :
      ( aElement0(W0)
     => ! [W1] :
          ( ( aSet0(W1)
            & isFinite0(W1) )
         => isFinite0(sdtmndt0(W1,W0)) ) ) )).

fof(mNATSet,axiom,
    ( aSet0(szNzAzT0)
    & isCountable0(szNzAzT0) )).

fof(mZeroNum,axiom,(
    aElementOf0(sz00,szNzAzT0) )).

fof(mSuccNum,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ( aElementOf0(szszuzczcdt0(W0),szNzAzT0)
        & szszuzczcdt0(W0) != sz00 ) ) )).

fof(mSuccEquSucc,axiom,(
    ! [W0,W1] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0) )
     => ( szszuzczcdt0(W0) = szszuzczcdt0(W1)
       => W0 = W1 ) ) )).

fof(mNatExtra,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ( W0 = sz00
        | ? [W1] :
            ( aElementOf0(W1,szNzAzT0)
            & W0 = szszuzczcdt0(W1) ) ) ) )).

fof(mNatNSucc,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => W0 != szszuzczcdt0(W0) ) )).

fof(mLessRel,axiom,(
    ! [W0,W1] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0) )
     => ( sdtlseqdt0(W0,W1)
       => $true ) ) )).

fof(mZeroLess,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => sdtlseqdt0(sz00,W0) ) )).

fof(mNoScLessZr,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ~ sdtlseqdt0(szszuzczcdt0(W0),sz00) ) )).

fof(mSuccLess,axiom,(
    ! [W0,W1] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0) )
     => ( sdtlseqdt0(W0,W1)
      <=> sdtlseqdt0(szszuzczcdt0(W0),szszuzczcdt0(W1)) ) ) )).

fof(mLessSucc,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => sdtlseqdt0(W0,szszuzczcdt0(W0)) ) )).

fof(mLessRefl,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => sdtlseqdt0(W0,W0) ) )).

fof(mLessASymm,axiom,(
    ! [W0,W1] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0) )
     => ( ( sdtlseqdt0(W0,W1)
          & sdtlseqdt0(W1,W0) )
       => W0 = W1 ) ) )).

fof(mLessTrans,axiom,(
    ! [W0,W1,W2] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0)
        & aElementOf0(W2,szNzAzT0) )
     => ( ( sdtlseqdt0(W0,W1)
          & sdtlseqdt0(W1,W2) )
       => sdtlseqdt0(W0,W2) ) ) )).

fof(mLessTotal,axiom,(
    ! [W0,W1] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0) )
     => ( sdtlseqdt0(W0,W1)
        | sdtlseqdt0(szszuzczcdt0(W1),W0) ) ) )).

fof(mIHSort,axiom,(
    ! [W0,W1] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0) )
     => ( iLess0(W0,W1)
       => $true ) ) )).

fof(mIH,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => iLess0(W0,szszuzczcdt0(W0)) ) )).

fof(mCardS,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => aElement0(sbrdtbr0(W0)) ) )).

fof(mCardNum,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => ( aElementOf0(sbrdtbr0(W0),szNzAzT0)
      <=> isFinite0(W0) ) ) )).

fof(mCardEmpty,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => ( sbrdtbr0(W0) = sz00
      <=> W0 = slcrc0 ) ) )).

fof(mCardCons,axiom,(
    ! [W0] :
      ( ( aSet0(W0)
        & isFinite0(W0) )
     => ! [W1] :
          ( aElement0(W1)
         => ( ~ aElementOf0(W1,W0)
           => sbrdtbr0(sdtpldt0(W0,W1)) = szszuzczcdt0(sbrdtbr0(W0)) ) ) ) )).

fof(mCardDiff,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => ! [W1] :
          ( ( isFinite0(W0)
            & aElementOf0(W1,W0) )
         => szszuzczcdt0(sbrdtbr0(sdtmndt0(W0,W1))) = sbrdtbr0(W0) ) ) )).

fof(mCardSub,axiom,(
    ! [W0] :
      ( aSet0(W0)
     => ! [W1] :
          ( ( isFinite0(W0)
            & aSubsetOf0(W1,W0) )
         => sdtlseqdt0(sbrdtbr0(W1),sbrdtbr0(W0)) ) ) )).

fof(mCardSubEx,axiom,(
    ! [W0,W1] :
      ( ( aSet0(W0)
        & aElementOf0(W1,szNzAzT0) )
     => ( ( isFinite0(W0)
          & sdtlseqdt0(W1,sbrdtbr0(W0)) )
       => ? [W2] :
            ( aSubsetOf0(W2,W0)
            & sbrdtbr0(W2) = W1 ) ) ) )).

fof(mDefMin,definition,(
    ! [W0] :
      ( ( aSubsetOf0(W0,szNzAzT0)
        & W0 != slcrc0 )
     => ! [W1] :
          ( W1 = szmzizndt0(W0)
        <=> ( aElementOf0(W1,W0)
            & ! [W2] :
                ( aElementOf0(W2,W0)
               => sdtlseqdt0(W1,W2) ) ) ) ) )).

fof(mDefMax,definition,(
    ! [W0] :
      ( ( aSubsetOf0(W0,szNzAzT0)
        & isFinite0(W0)
        & W0 != slcrc0 )
     => ! [W1] :
          ( W1 = szmzazxdt0(W0)
        <=> ( aElementOf0(W1,W0)
            & ! [W2] :
                ( aElementOf0(W2,W0)
               => sdtlseqdt0(W2,W1) ) ) ) ) )).

fof(mMinMin,axiom,(
    ! [W0,W1] :
      ( ( aSubsetOf0(W0,szNzAzT0)
        & aSubsetOf0(W1,szNzAzT0)
        & W0 != slcrc0
        & W1 != slcrc0 )
     => ( ( aElementOf0(szmzizndt0(W0),W1)
          & aElementOf0(szmzizndt0(W1),W0) )
       => szmzizndt0(W0) = szmzizndt0(W1) ) ) )).

fof(mDefSeg,definition,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ! [W1] :
          ( W1 = slbdtrb0(W0)
        <=> ( aSet0(W1)
            & ! [W2] :
                ( aElementOf0(W2,W1)
              <=> ( aElementOf0(W2,szNzAzT0)
                  & sdtlseqdt0(szszuzczcdt0(W2),W0) ) ) ) ) ) )).

fof(mSegFin,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => isFinite0(slbdtrb0(W0)) ) )).

fof(mSegZero,axiom,(
    slbdtrb0(sz00) = slcrc0 )).

fof(mSegSucc,axiom,(
    ! [W0,W1] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0) )
     => ( aElementOf0(W0,slbdtrb0(szszuzczcdt0(W1)))
      <=> ( aElementOf0(W0,slbdtrb0(W1))
          | W0 = W1 ) ) ) )).

fof(mSegLess,axiom,(
    ! [W0,W1] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0) )
     => ( sdtlseqdt0(W0,W1)
      <=> aSubsetOf0(slbdtrb0(W0),slbdtrb0(W1)) ) ) )).

fof(mFinSubSeg,axiom,(
    ! [W0] :
      ( ( aSubsetOf0(W0,szNzAzT0)
        & isFinite0(W0) )
     => ? [W1] :
          ( aElementOf0(W1,szNzAzT0)
          & aSubsetOf0(W0,slbdtrb0(W1)) ) ) )).

fof(mCardSeg,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => sbrdtbr0(slbdtrb0(W0)) = W0 ) )).

fof(mDefSel,definition,(
    ! [W0,W1] :
      ( ( aSet0(W0)
        & aElementOf0(W1,szNzAzT0) )
     => ! [W2] :
          ( W2 = slbdtsldtrb0(W0,W1)
        <=> ( aSet0(W2)
            & ! [W3] :
                ( aElementOf0(W3,W2)
              <=> ( aSubsetOf0(W3,W0)
                  & sbrdtbr0(W3) = W1 ) ) ) ) ) )).

fof(mSelFSet,axiom,(
    ! [W0] :
      ( ( aSet0(W0)
        & isFinite0(W0) )
     => ! [W1] :
          ( aElementOf0(W1,szNzAzT0)
         => isFinite0(slbdtsldtrb0(W0,W1)) ) ) )).

fof(mSelNSet,axiom,(
    ! [W0] :
      ( ( aSet0(W0)
        & ~ isFinite0(W0) )
     => ! [W1] :
          ( aElementOf0(W1,szNzAzT0)
         => slbdtsldtrb0(W0,W1) != slcrc0 ) ) )).

fof(mSelCSet,axiom,(
    ! [W0] :
      ( ( aSet0(W0)
        & isCountable0(W0) )
     => ! [W1] :
          ( ( aElementOf0(W1,szNzAzT0)
            & W1 != sz00 )
         => isCountable0(slbdtsldtrb0(W0,W1)) ) ) )).

fof(mSelSub,axiom,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ! [W1,W2] :
          ( ( aSet0(W1)
            & aSet0(W2)
            & W0 != sz00 )
         => ( ( aSubsetOf0(slbdtsldtrb0(W1,W0),slbdtsldtrb0(W2,W0))
              & slbdtsldtrb0(W1,W0) != slcrc0 )
           => aSubsetOf0(W1,W2) ) ) ) )).

fof(mSelExtra,axiom,(
    ! [W0,W1] :
      ( ( aSet0(W0)
        & aElementOf0(W1,szNzAzT0) )
     => ! [W2] :
          ( ( aSubsetOf0(W2,slbdtsldtrb0(W0,W1))
            & isFinite0(W2) )
         => ? [W3] :
              ( aSubsetOf0(W3,W0)
              & isFinite0(W3)
              & aSubsetOf0(W2,slbdtsldtrb0(W3,W1)) ) ) ) )).

fof(mFunSort,axiom,(
    ! [W0] :
      ( aFunction0(W0)
     => $true ) )).

fof(mDomSet,axiom,(
    ! [W0] :
      ( aFunction0(W0)
     => aSet0(szDzozmdt0(W0)) ) )).

fof(mImgElm,axiom,(
    ! [W0] :
      ( aFunction0(W0)
     => ! [W1] :
          ( aElementOf0(W1,szDzozmdt0(W0))
         => aElement0(sdtlpdtrp0(W0,W1)) ) ) )).

fof(mDefPtt,definition,(
    ! [W0,W1] :
      ( ( aFunction0(W0)
        & aElement0(W1) )
     => ! [W2] :
          ( W2 = sdtlbdtrb0(W0,W1)
        <=> ( aSet0(W2)
            & ! [W3] :
                ( aElementOf0(W3,W2)
              <=> ( aElementOf0(W3,szDzozmdt0(W0))
                  & sdtlpdtrp0(W0,W3) = W1 ) ) ) ) ) )).

fof(mPttSet,axiom,(
    ! [W0,W1] :
      ( ( aFunction0(W0)
        & aElement0(W1) )
     => aSubsetOf0(sdtlbdtrb0(W0,W1),szDzozmdt0(W0)) ) )).

fof(mDefSImg,definition,(
    ! [W0] :
      ( aFunction0(W0)
     => ! [W1] :
          ( aSubsetOf0(W1,szDzozmdt0(W0))
         => ! [W2] :
              ( W2 = sdtlcdtrc0(W0,W1)
            <=> ( aSet0(W2)
                & ! [W3] :
                    ( aElementOf0(W3,W2)
                  <=> ? [W4] :
                        ( aElementOf0(W4,W1)
                        & sdtlpdtrp0(W0,W4) = W3 ) ) ) ) ) ) )).

fof(mImgRng,axiom,(
    ! [W0] :
      ( aFunction0(W0)
     => ! [W1] :
          ( aElementOf0(W1,szDzozmdt0(W0))
         => aElementOf0(sdtlpdtrp0(W0,W1),sdtlcdtrc0(W0,szDzozmdt0(W0))) ) ) )).

fof(mDefRst,definition,(
    ! [W0] :
      ( aFunction0(W0)
     => ! [W1] :
          ( aSubsetOf0(W1,szDzozmdt0(W0))
         => ! [W2] :
              ( W2 = sdtexdt0(W0,W1)
            <=> ( aFunction0(W2)
                & szDzozmdt0(W2) = W1
                & ! [W3] :
                    ( aElementOf0(W3,W1)
                   => sdtlpdtrp0(W2,W3) = sdtlpdtrp0(W0,W3) ) ) ) ) ) )).

fof(mImgCount,axiom,(
    ! [W0] :
      ( aFunction0(W0)
     => ! [W1] :
          ( ( aSubsetOf0(W1,szDzozmdt0(W0))
            & isCountable0(W1) )
         => ( ! [W2,W3] :
                ( ( aElementOf0(W2,szDzozmdt0(W0))
                  & aElementOf0(W3,szDzozmdt0(W0))
                  & W2 != W3 )
               => sdtlpdtrp0(W0,W2) != sdtlpdtrp0(W0,W3) )
           => isCountable0(sdtlcdtrc0(W0,W1)) ) ) ) )).

fof(mDirichlet,axiom,(
    ! [W0] :
      ( aFunction0(W0)
     => ( ( isCountable0(szDzozmdt0(W0))
          & isFinite0(sdtlcdtrc0(W0,szDzozmdt0(W0))) )
       => ( aElement0(szDzizrdt0(W0))
          & isCountable0(sdtlbdtrb0(W0,szDzizrdt0(W0))) ) ) ) )).

fof(m__3291,hypothesis,
    ( aSet0(xT)
    & isFinite0(xT) )).

fof(m__3418,hypothesis,(
    aElementOf0(xK,szNzAzT0) )).

fof(m__3435,hypothesis,
    ( aSubsetOf0(xS,szNzAzT0)
    & isCountable0(xS) )).

fof(m__3453,hypothesis,
    ( aFunction0(xc)
    & szDzozmdt0(xc) = slbdtsldtrb0(xS,xK)
    & aSubsetOf0(sdtlcdtrc0(xc,szDzozmdt0(xc)),xT) )).

fof(m__3398,hypothesis,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ! [W1] :
          ( ( aSubsetOf0(W1,szNzAzT0)
            & isCountable0(W1) )
         => ! [W2] :
              ( ( aFunction0(W2)
                & szDzozmdt0(W2) = slbdtsldtrb0(W1,W0)
                & aSubsetOf0(sdtlcdtrc0(W2,szDzozmdt0(W2)),xT) )
             => ( iLess0(W0,xK)
               => ? [W3] :
                    ( aElementOf0(W3,xT)
                    & ? [W4] :
                        ( aSubsetOf0(W4,W1)
                        & isCountable0(W4)
                        & ! [W5] :
                            ( aElementOf0(W5,slbdtsldtrb0(W4,W0))
                           => sdtlpdtrp0(W2,W5) = W3 ) ) ) ) ) ) ) )).

fof(m__3462,hypothesis,(
    xK != sz00 )).

fof(m__3520,hypothesis,(
    xK != sz00 )).

fof(m__3533,hypothesis,
    ( aElementOf0(xk,szNzAzT0)
    & szszuzczcdt0(xk) = xK )).

fof(m__3623,hypothesis,
    ( aFunction0(xN)
    & szDzozmdt0(xN) = szNzAzT0
    & sdtlpdtrp0(xN,sz00) = xS
    & ! [W0] :
        ( aElementOf0(W0,szNzAzT0)
       => ( ( aSubsetOf0(sdtlpdtrp0(xN,W0),szNzAzT0)
            & isCountable0(sdtlpdtrp0(xN,W0)) )
         => ( aSubsetOf0(sdtlpdtrp0(xN,szszuzczcdt0(W0)),sdtmndt0(sdtlpdtrp0(xN,W0),szmzizndt0(sdtlpdtrp0(xN,W0))))
            & isCountable0(sdtlpdtrp0(xN,szszuzczcdt0(W0))) ) ) ) )).

fof(m__3671,hypothesis,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ( aSubsetOf0(sdtlpdtrp0(xN,W0),szNzAzT0)
        & isCountable0(sdtlpdtrp0(xN,W0)) ) ) )).

fof(m__3754,hypothesis,(
    ! [W0,W1] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0) )
     => ( sdtlseqdt0(W1,W0)
       => aSubsetOf0(sdtlpdtrp0(xN,W0),sdtlpdtrp0(xN,W1)) ) ) )).

fof(m__3821,hypothesis,(
    ! [W0,W1] :
      ( ( aElementOf0(W0,szNzAzT0)
        & aElementOf0(W1,szNzAzT0)
        & W0 != W1 )
     => szmzizndt0(sdtlpdtrp0(xN,W0)) != szmzizndt0(sdtlpdtrp0(xN,W1)) ) )).

fof(m__3965,hypothesis,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ! [W1] :
          ( ( aSet0(W1)
            & aElementOf0(W1,slbdtsldtrb0(sdtmndt0(sdtlpdtrp0(xN,W0),szmzizndt0(sdtlpdtrp0(xN,W0))),xk)) )
         => aElementOf0(sdtpldt0(W1,szmzizndt0(sdtlpdtrp0(xN,W0))),slbdtsldtrb0(xS,xK)) ) ) )).

fof(m__4151,hypothesis,
    ( aFunction0(xC)
    & szDzozmdt0(xC) = szNzAzT0
    & ! [W0] :
        ( aElementOf0(W0,szNzAzT0)
       => ( aFunction0(sdtlpdtrp0(xC,W0))
          & szDzozmdt0(sdtlpdtrp0(xC,W0)) = slbdtsldtrb0(sdtmndt0(sdtlpdtrp0(xN,W0),szmzizndt0(sdtlpdtrp0(xN,W0))),xk)
          & ! [W1] :
              ( ( aSet0(W1)
                & aElementOf0(W1,slbdtsldtrb0(sdtmndt0(sdtlpdtrp0(xN,W0),szmzizndt0(sdtlpdtrp0(xN,W0))),xk)) )
             => sdtlpdtrp0(sdtlpdtrp0(xC,W0),W1) = sdtlpdtrp0(xc,sdtpldt0(W1,szmzizndt0(sdtlpdtrp0(xN,W0)))) ) ) ) )).

fof(m__4182,hypothesis,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => aSubsetOf0(sdtlcdtrc0(sdtlpdtrp0(xC,W0),szDzozmdt0(sdtlpdtrp0(xC,W0))),xT) ) )).

fof(m__4331,hypothesis,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ! [W1] :
          ( ( aSubsetOf0(W1,sdtmndt0(sdtlpdtrp0(xN,W0),szmzizndt0(sdtlpdtrp0(xN,W0))))
            & isCountable0(W1) )
         => ! [W2] :
              ( ( aSet0(W2)
                & aElementOf0(W2,slbdtsldtrb0(W1,xk)) )
             => aElementOf0(W2,slbdtsldtrb0(sdtmndt0(sdtlpdtrp0(xN,W0),szmzizndt0(sdtlpdtrp0(xN,W0))),xk)) ) ) ) )).

fof(m__4411,hypothesis,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ? [W1] :
          ( aElementOf0(W1,xT)
          & ? [W2] :
              ( aSubsetOf0(W2,sdtmndt0(sdtlpdtrp0(xN,W0),szmzizndt0(sdtlpdtrp0(xN,W0))))
              & isCountable0(W2)
              & ! [W3] :
                  ( ( aSet0(W3)
                    & aElementOf0(W3,slbdtsldtrb0(W2,xk)) )
                 => sdtlpdtrp0(sdtlpdtrp0(xC,W0),W3) = W1 ) ) ) ) )).

fof(m__4618,hypothesis,(
    ! [W0] :
      ( aElementOf0(W0,szNzAzT0)
     => ? [W1] :
          ( aElementOf0(W1,xT)
          & ! [W2] :
              ( ( aSet0(W2)
                & aElementOf0(W2,slbdtsldtrb0(sdtlpdtrp0(xN,szszuzczcdt0(W0)),xk)) )
             => sdtlpdtrp0(sdtlpdtrp0(xC,W0),W2) = W1 ) ) ) )).

fof(m__4660,hypothesis,
    ( aFunction0(xe)
    & szDzozmdt0(xe) = szNzAzT0
    & ! [W0] :
        ( aElementOf0(W0,szNzAzT0)
       => sdtlpdtrp0(xe,W0) = szmzizndt0(sdtlpdtrp0(xN,W0)) ) )).

fof(m__4730,hypothesis,
    ( aFunction0(xd)
    & szDzozmdt0(xd) = szNzAzT0
    & ! [W0] :
        ( aElementOf0(W0,szNzAzT0)
       => ! [W1] :
            ( ( aSet0(W1)
              & aElementOf0(W1,slbdtsldtrb0(sdtlpdtrp0(xN,szszuzczcdt0(W0)),xk)) )
           => sdtlpdtrp0(xd,W0) = sdtlpdtrp0(sdtlpdtrp0(xC,W0),W1) ) ) )).

fof(m__4758,hypothesis,(
    aSubsetOf0(sdtlcdtrc0(xd,szDzozmdt0(xd)),xT) )).

fof(m__4854,hypothesis,
    ( aElementOf0(szDzizrdt0(xd),xT)
    & isCountable0(sdtlbdtrb0(xd,szDzizrdt0(xd))) )).

fof(m__4891,hypothesis,
    ( aSet0(xO)
    & xO = sdtlcdtrc0(xe,sdtlbdtrb0(xd,szDzizrdt0(xd))) )).

fof(m__4908,hypothesis,
    ( aSet0(xO)
    & isCountable0(xO) )).

fof(m__4982,hypothesis,(
    ! [W0] :
      ( aElementOf0(W0,xO)
     => ? [W1] :
          ( aElementOf0(W1,szNzAzT0)
          & aElementOf0(W1,sdtlbdtrb0(xd,szDzizrdt0(xd)))
          & sdtlpdtrp0(xe,W1) = W0 ) ) )).

fof(m__4998,hypothesis,(
    aSubsetOf0(xO,xS) )).

fof(m__5078,hypothesis,(
    aElementOf0(xQ,slbdtsldtrb0(xO,xK)) )).

fof(m__5093,hypothesis,
    ( aSubsetOf0(xQ,xO)
    & xQ != slcrc0 )).

fof(m__5106,hypothesis,(
    aSubsetOf0(xQ,szNzAzT0) )).

fof(m__5116,hypothesis,(
    aElementOf0(xQ,szDzozmdt0(xc)) )).

fof(m__5147,hypothesis,(
    xp = szmzizndt0(xQ) )).

fof(m__5164,hypothesis,
    ( aSet0(xP)
    & xP = sdtmndt0(xQ,szmzizndt0(xQ)) )).

fof(m__5173,hypothesis,(
    aElementOf0(xp,xQ) )).

fof(m__5182,hypothesis,(
    aElementOf0(xp,xO) )).

fof(m__5195,hypothesis,(
    aSubsetOf0(xP,xQ) )).

fof(m__5208,hypothesis,(
    aSubsetOf0(xP,xO) )).

fof(m__5255,hypothesis,
    ( sbrdtbr0(xQ) = szszuzczcdt0(xk)
    & szszuzczcdt0(sbrdtbr0(xP)) = sbrdtbr0(xQ)
    & aElementOf0(sbrdtbr0(xP),szNzAzT0) )).

fof(m__,conjecture,(
    sbrdtbr0(xP) = xk )).

%------------------------------------------------------------------------------
