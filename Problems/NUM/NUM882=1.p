%------------------------------------------------------------------------------
% File     : NUM882=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Number Theory
% Problem  : Product is not associative
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : CounterSatisfiable
% Rating   : 0.33 v6.3.0, 0.00 v6.0.0, 0.75 v5.4.0, 0.33 v5.2.0, 1.00 v5.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type)
%            Number of atoms       :    5 (   5 equality)
%            Maximal formula depth :   13 (  13 average)
%            Number of connectives :    5 (   1   ~;   0   |;   4   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    2 (   1 propositional; 0-2 arity)
%            Number of functors    :    1 (   0 constant; 2-2 arity)
%            Number of variables   :    7 (   0 sgn;   0   !;   7   ?)
%                                         (   7   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   1 average)
%            Arithmetic symbols    :    8 (   0 prd;   1 fun;   0 num;   7 var)
% SPC      : TF0_CSA_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(anti_associativity_product,conjecture,(
    ? [X: $int,Y: $int,Z: $int,Z1: $int,Z2: $int,Z3: $int,Z4: $int] : 
      ( $product(X,Y) = Z1
      & $product(Z1,Z) = Z2
      & $product(Z,X) = Z3
      & $product(Z3,Y) = Z4
      & Z2 != Z4 ) )).
%------------------------------------------------------------------------------
