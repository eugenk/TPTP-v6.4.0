%------------------------------------------------------------------------------
% File     : NUM886=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Number Theory
% Problem  : Product non-idempotence
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : CounterSatisfiable
% Rating   : 0.33 v6.4.0, 0.67 v6.3.0, 0.33 v6.2.0, 0.00 v6.0.0, 0.25 v5.4.0, 0.00 v5.2.0, 1.00 v5.0.0
% Syntax   : Number of formulae    :    1 (   1 unit;   0 type)
%            Number of atoms       :    1 (   1 equality)
%            Maximal formula depth :    3 (   3 average)
%            Number of connectives :    1 (   1   ~;   0   |;   0   &)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    2 (   1 propositional; 0-2 arity)
%            Number of functors    :    1 (   0 constant; 2-2 arity)
%            Number of variables   :    1 (   0 sgn;   1   !;   0   ?)
%                                         (   1   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   2 average)
%            Arithmetic symbols    :    2 (   0 prd;   1 fun;   0 num;   1 var)
% SPC      : TF0_CSA_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(anti_not_product_idempotence,conjecture,(
    ! [X: $int] : $product(X,X) != X )).
%------------------------------------------------------------------------------
