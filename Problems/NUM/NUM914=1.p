%------------------------------------------------------------------------------
% File     : NUM914=1 : TPTP v6.4.0. Released v5.0.0.
% Domain   : Number Theory
% Problem  : Sum and difference
% Version  : Especial.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.00 v6.3.0, 0.14 v6.2.0, 0.25 v6.1.0, 0.11 v6.0.0, 0.00 v5.5.0, 0.22 v5.4.0, 0.25 v5.3.0, 0.60 v5.2.0, 0.67 v5.1.0, 0.80 v5.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type)
%            Number of atoms       :    3 (   3 equality)
%            Maximal formula depth :    6 (   6 average)
%            Number of connectives :    2 (   0   ~;   0   |;   1   &)
%                                         (   1 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of predicates  :    2 (   1 propositional; 0-2 arity)
%            Number of functors    :    2 (   0 constant; 2-2 arity)
%            Number of variables   :    3 (   0 sgn;   0   !;   3   ?)
%                                         (   3   :;   0  !>;   0  ?*)
%            Maximal term depth    :    2 (   2 average)
%            Arithmetic symbols    :    5 (   0 prd;   2 fun;   0 num;   3 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(real_combined_problem_2,conjecture,(
    ? [X: $real,Y: $real,Z: $real] : 
      ( $sum(X,Y) = Z
    <=> ( $difference(Z,Y) = X
        & $difference(Z,X) = Y ) ) )).
%------------------------------------------------------------------------------
