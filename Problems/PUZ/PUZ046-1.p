%--------------------------------------------------------------------------
% File     : PUZ046-1 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Puzzles (Quo Vadis)
% Problem  : Quo vadis axioms
% Version  : [TPTP] axioms.
% English  :

% Refs     :
% Source   : [TPTP]
% Names    :

% Status   : Satisfiable
% Rating   : 0.00 v5.4.0, 0.11 v5.3.0, 0.14 v5.0.0, 0.12 v4.1.0, 0.00 v4.0.0, 0.12 v3.5.0, 0.14 v3.4.0, 0.17 v3.2.0, 0.40 v3.1.0, 0.14 v2.7.0, 0.20 v2.6.0, 0.25 v2.5.0
% Syntax   : Number of clauses     :   41 (   0 non-Horn;   0 unit;  41 RR)
%            Number of atoms       :   82 (   0 equality)
%            Maximal clause size   :    2 (   2 average)
%            Number of predicates  :    1 (   0 propositional; 12-12 arity)
%            Number of functors    :   13 (   0 constant; 1-2 arity)
%            Number of variables   :  480 (   0 singleton)
%            Maximal term depth    :    4 (   1 average)
% SPC      : CNF_SAT_RFO_NEQ

% Comments :
%--------------------------------------------------------------------------
%----Include Quo vadis axioms
include('Axioms/PUZ004-0.ax').
%--------------------------------------------------------------------------
