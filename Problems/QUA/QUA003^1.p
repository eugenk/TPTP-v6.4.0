%------------------------------------------------------------------------------
% File     : QUA003^1 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Quantales
% Problem  : Zero is neutral with respect to addition
% Version  : [Hoe09] axioms.
% English  :

% Refs     : [Con71] Conway (1971), Regular Algebra and Finite Machines
%          : [Hoe09] Hoefner (2009), Email to Geoff Sutcliffe
% Source   : [Hoe09]
% Names    : QUA03 [Hoe09] 

% Status   : Theorem
% Rating   : 1.00 v4.1.0
% Syntax   : Number of formulae    :   27 (   0 unit;  12 type;   7 defn)
%            Number of atoms       :   96 (  18 equality;  42 variable)
%            Maximal formula depth :   12 (   5 average)
%            Number of connectives :   45 (   0   ~;   1   |;   4   &;  39   @)
%                                         (   1 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   43 (  43   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   15 (  12   :;   0   =)
%            Number of variables   :   28 (   1 sgn;   9   !;   4   ?;  15   ^)
%                                         (  28   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include axioms for Quantales
include('Axioms/QUA001^0.ax').
%------------------------------------------------------------------------------
thf(addition_neutral,conjecture,(
    ! [X1: $i] :
      ( ( addition @ X1 @ zero )
      = X1 ) )).

%------------------------------------------------------------------------------
