%------------------------------------------------------------------------------
% File     : QUA008^1 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Quantales
% Problem  : Left-distributivity of multiplication over addition
% Version  : [Hoe09] axioms.
% English  :

% Refs     : [Con71] Conway (1971), Regular Algebra and Finite Machines
%          : [Hoe09] Hoefner (2009), Email to Geoff Sutcliffe
% Source   : [Hoe09]
% Names    : QUA08 [Hoe09] 

% Status   : Theorem
% Rating   : 1.00 v4.1.0
% Syntax   : Number of formulae    :   27 (   0 unit;  12 type;   7 defn)
%            Number of atoms       :  104 (  18 equality;  47 variable)
%            Maximal formula depth :   12 (   5 average)
%            Number of connectives :   53 (   0   ~;   1   |;   4   &;  47   @)
%                                         (   1 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   43 (  43   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   15 (  12   :;   0   =)
%            Number of variables   :   30 (   1 sgn;  11   !;   4   ?;  15   ^)
%                                         (  30   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include axioms for Quantales
include('Axioms/QUA001^0.ax').
%------------------------------------------------------------------------------
thf(multiplication_distrl,conjecture,(
    ! [X1: $i,X2: $i,X3: $i] :
      ( ( multiplication @ ( addition @ X1 @ X2 ) @ X3 )
      = ( addition @ ( multiplication @ X1 @ X3 ) @ ( multiplication @ X2 @ X3 ) ) ) )).

%------------------------------------------------------------------------------
