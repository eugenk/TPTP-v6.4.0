%------------------------------------------------------------------------------
% File     : REL011-1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Relation Algebra
% Problem  : Schroeder equivalence (second implication)
% Version  : [Mad95] (equational) axioms.
% English  : Describes the interplay between composition, converse and
%            complement, w.r.t. containment.

% Refs     : [Mad95] Maddux (1995), Relation-Algebraic Semantics
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.79 v6.4.0, 0.84 v6.3.0, 0.82 v6.2.0, 0.79 v6.1.0, 0.81 v6.0.0, 0.95 v5.4.0, 0.87 v5.3.0, 0.83 v5.2.0, 0.86 v5.1.0, 0.87 v5.0.0, 0.86 v4.1.0, 0.82 v4.0.1, 0.79 v4.0.0
% Syntax   : Number of clauses     :   15 (   0 non-Horn;  15 unit;   2 RR)
%            Number of atoms       :   15 (  15 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :   11 (   6 constant; 0-2 arity)
%            Number of variables   :   25 (   0 singleton)
%            Maximal term depth    :    5 (   3 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments : tptp2X -f tptp:short -t cnf:otter REL011+1.p
%------------------------------------------------------------------------------
%----Include axioms of relation algebra
include('Axioms/REL001-0.ax').
%------------------------------------------------------------------------------
cnf(goals_14,negated_conjecture,
    ( meet(sk1,composition(converse(sk2),sk3)) = zero )).

cnf(goals_15,negated_conjecture,
    ( meet(composition(sk2,sk1),sk3) != zero )).
%------------------------------------------------------------------------------
