%------------------------------------------------------------------------------
% File     : REL016+4 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Relation Algebra
% Problem  : A modular law
% Version  : [Mad95] (equational) axioms : Augmented.
% English  :

% Refs     : [Mad95] Maddux (1995), Relation-Algebraic Semantics
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.93 v6.2.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :   17 (  16 unit)
%            Number of atoms       :   18 (  18 equality)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    1 (   0 ~  ;   0  |;   1  &)
%                                         (   0 <=>;   0 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    8 (   3 constant; 0-2 arity)
%            Number of variables   :   37 (   0 singleton;  37 !;   0 ?)
%            Maximal term depth    :    7 (   4 average)
% SPC      : FOF_THM_RFO_PEQ

% Comments : Proof goal is split into 2 inequations (encoded again as
%            equations).
%------------------------------------------------------------------------------
%---Include axioms for relation algebra
include('Axioms/REL001+0.ax').
%---Include Dedekind and modular laws
include('Axioms/REL001+1.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1,X2] :
      ( join(meet(composition(X0,X1),complement(composition(X0,X2))),meet(composition(X0,meet(X1,complement(X2))),complement(composition(X0,X2)))) = meet(composition(X0,meet(X1,complement(X2))),complement(composition(X0,X2)))
      & join(meet(composition(X0,meet(X1,complement(X2))),complement(composition(X0,X2))),meet(composition(X0,X1),complement(composition(X0,X2)))) = meet(composition(X0,X1),complement(composition(X0,X2))) ) )).

%------------------------------------------------------------------------------
