%------------------------------------------------------------------------------
% File     : REL020+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Relation Algebra
% Problem  : Restriction of subidentities
% Version  : [Mad95] (equational) axioms.
% English  : For vectors restriction of subidientities equals intersection
%            with vectors.

% Refs     : [Mad95] Maddux (1995), Relation-Algebraic Semantics
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 1.00 v6.1.0, 0.92 v5.5.0, 0.88 v5.4.0, 0.89 v5.3.0, 0.83 v5.2.0, 0.86 v5.0.0, 0.88 v4.1.0, 0.91 v4.0.1, 0.90 v4.0.0
% Syntax   : Number of formulae    :   14 (  13 unit)
%            Number of atoms       :   15 (  15 equality)
%            Maximal formula depth :    4 (   3 average)
%            Number of connectives :    1 (   0 ~  ;   0  |;   0  &)
%                                         (   0 <=>;   1 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    8 (   3 constant; 0-2 arity)
%            Number of variables   :   27 (   0 singleton;  27 !;   0 ?)
%            Maximal term depth    :    5 (   3 average)
% SPC      : FOF_THM_RFO_PEQ

% Comments : The right hand side of the proof goal is also split into 2
%            inequations.
%------------------------------------------------------------------------------
%---Include axioms for relation algebra
include('Axioms/REL001+0.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1] :
      ( composition(X0,top) = X0
     => composition(meet(X0,one),X1) = meet(X0,X1) ) )).

%------------------------------------------------------------------------------
