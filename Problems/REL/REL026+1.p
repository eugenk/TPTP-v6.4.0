%------------------------------------------------------------------------------
% File     : REL026+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Relation Algebra
% Problem  : Splitting rule for x;y if x is a subidentity
% Version  : [Mad95] (equational) axioms.
% English  : If x is a subidentity then the composition of x and y can be
%            split into a meet.

% Refs     : [Mad95] Maddux (1995), Relation-Algebraic Semantics
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.53 v6.4.0, 0.50 v6.2.0, 0.45 v6.1.0, 0.58 v6.0.0, 0.50 v5.5.0, 0.38 v5.4.0, 0.44 v5.3.0, 0.17 v5.2.0, 0.29 v5.0.0, 0.25 v4.1.0, 0.45 v4.0.1, 0.40 v4.0.0
% Syntax   : Number of formulae    :   14 (  13 unit)
%            Number of atoms       :   15 (  15 equality)
%            Maximal formula depth :    4 (   3 average)
%            Number of connectives :    1 (   0 ~  ;   0  |;   0  &)
%                                         (   0 <=>;   1 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    8 (   3 constant; 0-2 arity)
%            Number of variables   :   27 (   0 singleton;  27 !;   0 ?)
%            Maximal term depth    :    5 (   3 average)
% SPC      : FOF_THM_RFO_PEQ

% Comments :
%------------------------------------------------------------------------------
%---Include axioms for relation algebra
include('Axioms/REL001+0.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1] :
      ( join(X0,one) = one
     => meet(composition(X0,top),X1) = composition(X0,X1) ) )).

%------------------------------------------------------------------------------
