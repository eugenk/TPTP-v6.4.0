%------------------------------------------------------------------------------
% File     : REL032-1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Relation Algebra
% Problem  : Subdistributivity
% Version  : [Mad95] (equational) axioms.
% English  : Sequential composition subdistributes over meet, i.e.
%            x;(y meet z) <= x;y meet x;z.

% Refs     : [Mad95] Maddux (1995), Relation-Algebraic Semantics
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Unsatisfiable
% Rating   : 1.00 v4.0.0
% Syntax   : Number of clauses     :   14 (   0 non-Horn;  14 unit;   1 RR)
%            Number of atoms       :   14 (  14 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :   11 (   6 constant; 0-2 arity)
%            Number of variables   :   25 (   0 singleton)
%            Maximal term depth    :    5 (   3 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments : tptp2X -f tptp:short -t cnf:otter REL032+1.p
%------------------------------------------------------------------------------
%----Include axioms of relation algebra
include('Axioms/REL001-0.ax').
%------------------------------------------------------------------------------
cnf(goals_14,negated_conjecture,
    ( join(composition(sk1,meet(sk2,sk3)),meet(composition(sk1,sk2),composition(sk1,sk3))) != meet(composition(sk1,sk2),composition(sk1,sk3)) )).
%------------------------------------------------------------------------------
