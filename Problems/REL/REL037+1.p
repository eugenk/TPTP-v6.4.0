%------------------------------------------------------------------------------
% File     : REL037+1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Relation Algebra
% Problem  : Propagation of vectors
% Version  : [Mad95] (equational) axioms.
% English  : Post-assertion x^ to y can be propagated as pre-assertion x to
%            the right cofactor z.

% Refs     : [Mad95] Maddux (1995), Relation-Algebraic Semantics
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Theorem
% Rating   : 0.93 v6.2.0, 0.91 v6.1.0, 0.92 v5.5.0, 0.88 v5.4.0, 0.89 v5.3.0, 0.83 v5.2.0, 0.86 v5.0.0, 0.88 v4.1.0, 0.91 v4.0.1, 0.90 v4.0.0
% Syntax   : Number of formulae    :   14 (  13 unit)
%            Number of atoms       :   15 (  15 equality)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    1 (   0 ~  ;   0  |;   0  &)
%                                         (   0 <=>;   1 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    8 (   3 constant; 0-2 arity)
%            Number of variables   :   28 (   0 singleton;  28 !;   0 ?)
%            Maximal term depth    :    5 (   3 average)
% SPC      : FOF_THM_RFO_PEQ

% Comments :
%------------------------------------------------------------------------------
%---Include axioms for relation algebra
include('Axioms/REL001+0.ax').
%------------------------------------------------------------------------------
fof(goals,conjecture,(
    ! [X0,X1,X2] :
      ( composition(X0,top) = X0
     => composition(meet(X1,converse(X0)),meet(X0,X2)) = composition(meet(X1,converse(X0)),X2) ) )).

%------------------------------------------------------------------------------
