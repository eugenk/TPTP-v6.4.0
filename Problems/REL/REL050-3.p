%------------------------------------------------------------------------------
% File     : REL050-3 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Relation Algebra
% Problem  : The complement of x;TOP is left ideal
% Version  : [Mad95] (equational) axioms : Augmented.
% English  :

% Refs     : [Mad95] Maddux (1995), Relation-Algebraic Semantics
%          : [Hoe08] Hoefner (2008), Email to G. Sutcliffe
% Source   : [Hoe08]
% Names    :

% Status   : Unsatisfiable
% Rating   : 0.42 v6.4.0, 0.47 v6.2.0, 0.50 v6.1.0, 0.56 v6.0.0, 0.67 v5.5.0, 0.63 v5.4.0, 0.53 v5.3.0, 0.50 v5.1.0, 0.53 v5.0.0, 0.50 v4.1.0, 0.55 v4.0.1, 0.50 v4.0.0
% Syntax   : Number of clauses     :   17 (   0 non-Horn;  17 unit;   1 RR)
%            Number of atoms       :   17 (  17 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    9 (   4 constant; 0-2 arity)
%            Number of variables   :   34 (   0 singleton)
%            Maximal term depth    :    7 (   3 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments : tptp2X -f tptp:short -t cnf:otter REL050+3.p
%------------------------------------------------------------------------------
%----Include axioms of relation algebra
include('Axioms/REL001-0.ax').
%----Include Dedekind and modular laws
include('Axioms/REL001-1.ax').
%------------------------------------------------------------------------------
cnf(goals_17,negated_conjecture,
    ( complement(composition(sk1,top)) != composition(complement(composition(sk1,top)),top) )).
%------------------------------------------------------------------------------
