%--------------------------------------------------------------------------
% File     : RNG028-1 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Ring Theory (Alternative)
% Problem  : Left Moufang identity
% Version  : [AH90] (equality) axioms.
% English  :

% Refs     : [AH90]  Anantharaman & Hsiang (1990), Automated Proofs of the
% Source   : [TPTP]
% Names    :

% Status   : Unsatisfiable
% Rating   : 1.00 v2.7.0, 0.88 v2.6.0, 1.00 v2.2.1, 1.00 v2.0.0
% Syntax   : Number of clauses     :   18 (   0 non-Horn;  16 unit;   4 RR)
%            Number of atoms       :   20 (  20 equality)
%            Maximal clause size   :    2 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    7 (   4 constant; 0-2 arity)
%            Number of variables   :   32 (   2 singleton)
%            Maximal term depth    :    4 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
%--------------------------------------------------------------------------
%----Include Ring theory (equality) axioms
include('Axioms/RNG004-0.ax').
%--------------------------------------------------------------------------
cnf(prove_left_moufang,negated_conjecture,
    (  multiply(multiply(cx,multiply(cy,cx)),cz) != multiply(cx,multiply(cy,multiply(cx,cz))) )).

%--------------------------------------------------------------------------
