%--------------------------------------------------------------------------
% File     : RNG034-1 : TPTP v6.4.0. Released v1.0.0.
% Domain   : Ring Theory (Alternative)
% Problem  : A skew symmetry relation of the associator
% Version  : [AH90] (equality) axioms.
% English  :

% Refs     : [AH90]  Anantharaman & Hsiang (1990), Automated Proofs of the
% Source   : [AH90]
% Names    : PROOF II [AH90]

% Status   : Unsatisfiable
% Rating   : 0.85 v6.4.0, 0.86 v6.3.0, 0.80 v6.1.0, 0.82 v6.0.0, 0.71 v5.5.0, 0.88 v5.4.0, 0.78 v5.3.0, 0.80 v5.2.0, 0.75 v5.1.0, 0.89 v5.0.0, 0.90 v4.1.0, 0.89 v4.0.1, 0.88 v4.0.0, 0.71 v3.7.0, 0.29 v3.4.0, 0.17 v3.3.0, 0.44 v3.1.0, 0.40 v2.7.0, 0.50 v2.6.0, 0.67 v2.5.0, 0.50 v2.4.0, 0.75 v2.3.0, 0.50 v2.2.1, 0.83 v2.2.0, 0.75 v2.1.0, 1.00 v2.0.0
% Syntax   : Number of clauses     :   19 (   0 non-Horn;  17 unit;   4 RR)
%            Number of atoms       :   21 (  21 equality)
%            Maximal clause size   :    2 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    8 (   4 constant; 0-3 arity)
%            Number of variables   :   35 (   2 singleton)
%            Maximal term depth    :    5 (   2 average)
% SPC      : CNF_UNS_RFO_PEQ_NUE

% Comments :
%--------------------------------------------------------------------------
%----Include Ring theory (equality) axioms
include('Axioms/RNG004-0.ax').
%--------------------------------------------------------------------------
%----Associator
cnf(associator,axiom,
    ( associator(X,Y,Z) = add(multiply(multiply(X,Y),Z),additive_inverse(multiply(X,multiply(Y,Z)))) )).

cnf(prove_skew_symmetry,negated_conjecture,
    (  associator(cy,cx,cz) != additive_inverse(associator(cx,cy,cz)) )).

%--------------------------------------------------------------------------
