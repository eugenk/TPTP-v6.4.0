%------------------------------------------------------------------------------
% File     : ROB032-2 : TPTP v6.4.0. Released v4.1.0.
% Domain   : Robbins Algebra
% Problem  : Robbins => Exists absorbed element, with auxilliary definitions
% Version  : Especial.
% English  :

% Refs     : [Sta09] Stanovsky (2009), Email to Geoff Sutcliffe
% Source   : [Sta09]
% Names    : robbins [Sta09]

% Status   : Unsatisfiable
% Rating   : 1.00 v4.1.0
% Syntax   : Number of clauses     :    6 (   0 non-Horn;   6 unit;   1 RR)
%            Number of atoms       :    6 (   6 equality)
%            Maximal clause size   :    1 (   1 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    4 (   0 constant; 1-2 arity)
%            Number of variables   :   11 (   1 singleton)
%            Maximal term depth    :    6 (   3 average)
% SPC      : CNF_UNS_RFO_PEQ_UEQ

% Comments :
%------------------------------------------------------------------------------
%----Include axioms for Robbins algebra
include('Axioms/ROB001-0.ax').
%------------------------------------------------------------------------------
%----Definition of g
cnf(sos04,axiom,(
    g(A) = negate(add(A,negate(A))) )).

%----Definition of h
cnf(sos05,axiom,(
    h(A) = add(A,add(A,add(A,g(A)))) )).

%----Winker1b
cnf(goals,negated_conjecture,(
    add(X4,X5) != X5 )).

%------------------------------------------------------------------------------
