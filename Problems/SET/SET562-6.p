%--------------------------------------------------------------------------
% File     : SET562-6 : TPTP v6.4.0. Bugfixed v2.1.0.
% Domain   : Set Theory
% Problem  : Compatible function property 2
% Version  : [Qua92] axioms.
% English  :

% Refs     : [BL+86] Boyer et al. (1986), Set Theory in First-Order Logic:
%          : [Qua92] Quaife (1992), Automated Deduction in von Neumann-Bern
%          : [Bel97] Belinfante (1997), On Quaife's Development of Class Th
% Source   : [Quaife]
% Names    : CF5 [Qua92]

% Status   : Unsatisfiable
% Rating   : 1.00 v2.1.0
% Syntax   : Number of clauses     :  116 (   8 non-Horn;  41 unit;  83 RR)
%            Number of atoms       :  222 (  49 equality)
%            Maximal clause size   :    5 (   2 average)
%            Number of predicates  :   11 (   0 propositional; 1-3 arity)
%            Number of functors    :   51 (  17 constant; 0-3 arity)
%            Number of variables   :  214 (  32 singleton)
%            Maximal term depth    :    6 (   2 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments : Quaife proves all these problems by augmenting the axioms with
%            all previously proved theorems. With a few exceptions (the
%            problems that correspond to [BL+86] problems), the TPTP has
%            retained the order in which Quaife presents the problems. The
%            user may create an augmented version of this problem by adding
%            all previously proved theorems (the ones that correspond to
%            [BL+86] are easily identified and positioned using Quaife's
%            naming scheme).
% Bugfixes : v1.0.1 - Bugfix in SET004-1.ax.
%          : v2.1.0 - Added prove_compatible_function_property2_0 [Bel97].
%          : v2.1.0 - Bugfix in SET004-0.ax.
%--------------------------------------------------------------------------
%----Include von Neuman-Bernays-Godel set theory axioms
include('Axioms/SET004-0.ax').
%----Include von Neuman-Bernays-Godel Boolean Algebra definitions
include('Axioms/SET004-1.ax').
%--------------------------------------------------------------------------
cnf(prove_compatible_function_property2_0,negated_conjecture,
    ( operation(xf2) )).

cnf(prove_compatible_function_property2_1,negated_conjecture,
    ( compatible(xh,xf1,xf2) )).

cnf(prove_compatible_function_property2_2,negated_conjecture,
    ( member(ordered_pair(x,y),cross_product(domain_of(xh),domain_of(xh))) )).

cnf(prove_compatible_function_property2_3,negated_conjecture,
    ( ~ member(ordered_pair(apply(xh,x),apply(xh,y)),domain_of(xf2)) )).

%--------------------------------------------------------------------------
