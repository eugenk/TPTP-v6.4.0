%------------------------------------------------------------------------------
% File     : SEU447+4 : TPTP v6.4.0. Released v3.4.0.
% Domain   : Set Theory
% Problem  : First and Second Order Cutting of Binary Relations T59
% Version  : [Urb08] axioms : Especial.
% English  :

% Refs     : [Ret05] Retel (2005), Properties of First and Second Order Cut
%          : [Urb07] Urban (2007), MPTP 0.2: Design, Implementation, and In
%          : [Urb08] Urban (2006), Email to G. Sutcliffe
% Source   : [Urb08]
% Names    : t59_relset_2 [Urb08]

% Status   : Theorem
% Rating   : 1.00 v3.4.0
% Syntax   : Number of formulae    : 71141 (7567 unit)
%            Number of atoms       : 578896 (63174 equality)
%            Maximal formula depth :  150 (   9 average)
%            Number of connectives : 585021 (77266 ~  ;6490  |;300556  &)
%                                         (11669 <=>;189040 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  : 3601 (   3 propositional; 0-8 arity)
%            Number of functors    : 9763 (2144 constant; 0-10 arity)
%            Number of variables   : 207633 (  10 singleton;198121 !;9512 ?)
%            Maximal term depth    :   16 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments : Chainy large version: includes all preceding MML articles.
%          : Translated by MPTP from the Mizar Mathematical Library 4.48.930.
%          : The problem encoding is based on set theory.
%------------------------------------------------------------------------------
include('Axioms/SET007/SET007+0.ax').
include('Axioms/SET007/SET007+1.ax').
include('Axioms/SET007/SET007+2.ax').
include('Axioms/SET007/SET007+3.ax').
include('Axioms/SET007/SET007+4.ax').
include('Axioms/SET007/SET007+5.ax').
include('Axioms/SET007/SET007+6.ax').
include('Axioms/SET007/SET007+7.ax').
include('Axioms/SET007/SET007+8.ax').
include('Axioms/SET007/SET007+9.ax').
include('Axioms/SET007/SET007+10.ax').
include('Axioms/SET007/SET007+11.ax').
include('Axioms/SET007/SET007+12.ax').
include('Axioms/SET007/SET007+13.ax').
include('Axioms/SET007/SET007+14.ax').
include('Axioms/SET007/SET007+15.ax').
include('Axioms/SET007/SET007+16.ax').
include('Axioms/SET007/SET007+17.ax').
include('Axioms/SET007/SET007+18.ax').
include('Axioms/SET007/SET007+19.ax').
include('Axioms/SET007/SET007+20.ax').
include('Axioms/SET007/SET007+21.ax').
include('Axioms/SET007/SET007+22.ax').
include('Axioms/SET007/SET007+23.ax').
include('Axioms/SET007/SET007+24.ax').
include('Axioms/SET007/SET007+25.ax').
include('Axioms/SET007/SET007+26.ax').
include('Axioms/SET007/SET007+27.ax').
include('Axioms/SET007/SET007+28.ax').
include('Axioms/SET007/SET007+29.ax').
include('Axioms/SET007/SET007+30.ax').
include('Axioms/SET007/SET007+31.ax').
include('Axioms/SET007/SET007+32.ax').
include('Axioms/SET007/SET007+33.ax').
include('Axioms/SET007/SET007+34.ax').
include('Axioms/SET007/SET007+35.ax').
include('Axioms/SET007/SET007+36.ax').
include('Axioms/SET007/SET007+37.ax').
include('Axioms/SET007/SET007+38.ax').
include('Axioms/SET007/SET007+39.ax').
include('Axioms/SET007/SET007+40.ax').
include('Axioms/SET007/SET007+41.ax').
include('Axioms/SET007/SET007+42.ax').
include('Axioms/SET007/SET007+43.ax').
include('Axioms/SET007/SET007+44.ax').
include('Axioms/SET007/SET007+45.ax').
include('Axioms/SET007/SET007+46.ax').
include('Axioms/SET007/SET007+47.ax').
include('Axioms/SET007/SET007+48.ax').
include('Axioms/SET007/SET007+49.ax').
include('Axioms/SET007/SET007+50.ax').
include('Axioms/SET007/SET007+51.ax').
include('Axioms/SET007/SET007+52.ax').
include('Axioms/SET007/SET007+53.ax').
include('Axioms/SET007/SET007+54.ax').
include('Axioms/SET007/SET007+55.ax').
include('Axioms/SET007/SET007+56.ax').
include('Axioms/SET007/SET007+57.ax').
include('Axioms/SET007/SET007+58.ax').
include('Axioms/SET007/SET007+59.ax').
include('Axioms/SET007/SET007+60.ax').
include('Axioms/SET007/SET007+61.ax').
include('Axioms/SET007/SET007+62.ax').
include('Axioms/SET007/SET007+63.ax').
include('Axioms/SET007/SET007+64.ax').
include('Axioms/SET007/SET007+65.ax').
include('Axioms/SET007/SET007+66.ax').
include('Axioms/SET007/SET007+67.ax').
include('Axioms/SET007/SET007+68.ax').
include('Axioms/SET007/SET007+69.ax').
include('Axioms/SET007/SET007+70.ax').
include('Axioms/SET007/SET007+71.ax').
include('Axioms/SET007/SET007+72.ax').
include('Axioms/SET007/SET007+73.ax').
include('Axioms/SET007/SET007+74.ax').
include('Axioms/SET007/SET007+75.ax').
include('Axioms/SET007/SET007+76.ax').
include('Axioms/SET007/SET007+77.ax').
include('Axioms/SET007/SET007+78.ax').
include('Axioms/SET007/SET007+79.ax').
include('Axioms/SET007/SET007+80.ax').
include('Axioms/SET007/SET007+81.ax').
include('Axioms/SET007/SET007+82.ax').
include('Axioms/SET007/SET007+83.ax').
include('Axioms/SET007/SET007+84.ax').
include('Axioms/SET007/SET007+85.ax').
include('Axioms/SET007/SET007+86.ax').
include('Axioms/SET007/SET007+87.ax').
include('Axioms/SET007/SET007+88.ax').
include('Axioms/SET007/SET007+89.ax').
include('Axioms/SET007/SET007+90.ax').
include('Axioms/SET007/SET007+91.ax').
include('Axioms/SET007/SET007+92.ax').
include('Axioms/SET007/SET007+93.ax').
include('Axioms/SET007/SET007+94.ax').
include('Axioms/SET007/SET007+95.ax').
include('Axioms/SET007/SET007+96.ax').
include('Axioms/SET007/SET007+97.ax').
include('Axioms/SET007/SET007+98.ax').
include('Axioms/SET007/SET007+99.ax').
include('Axioms/SET007/SET007+100.ax').
include('Axioms/SET007/SET007+101.ax').
include('Axioms/SET007/SET007+102.ax').
include('Axioms/SET007/SET007+103.ax').
include('Axioms/SET007/SET007+104.ax').
include('Axioms/SET007/SET007+105.ax').
include('Axioms/SET007/SET007+106.ax').
include('Axioms/SET007/SET007+107.ax').
include('Axioms/SET007/SET007+108.ax').
include('Axioms/SET007/SET007+109.ax').
include('Axioms/SET007/SET007+110.ax').
include('Axioms/SET007/SET007+111.ax').
include('Axioms/SET007/SET007+112.ax').
include('Axioms/SET007/SET007+113.ax').
include('Axioms/SET007/SET007+114.ax').
include('Axioms/SET007/SET007+115.ax').
include('Axioms/SET007/SET007+116.ax').
include('Axioms/SET007/SET007+117.ax').
include('Axioms/SET007/SET007+118.ax').
include('Axioms/SET007/SET007+119.ax').
include('Axioms/SET007/SET007+120.ax').
include('Axioms/SET007/SET007+121.ax').
include('Axioms/SET007/SET007+122.ax').
include('Axioms/SET007/SET007+123.ax').
include('Axioms/SET007/SET007+124.ax').
include('Axioms/SET007/SET007+125.ax').
include('Axioms/SET007/SET007+126.ax').
include('Axioms/SET007/SET007+127.ax').
include('Axioms/SET007/SET007+128.ax').
include('Axioms/SET007/SET007+129.ax').
include('Axioms/SET007/SET007+130.ax').
include('Axioms/SET007/SET007+131.ax').
include('Axioms/SET007/SET007+132.ax').
include('Axioms/SET007/SET007+133.ax').
include('Axioms/SET007/SET007+134.ax').
include('Axioms/SET007/SET007+135.ax').
include('Axioms/SET007/SET007+136.ax').
include('Axioms/SET007/SET007+137.ax').
include('Axioms/SET007/SET007+138.ax').
include('Axioms/SET007/SET007+139.ax').
include('Axioms/SET007/SET007+140.ax').
include('Axioms/SET007/SET007+141.ax').
include('Axioms/SET007/SET007+142.ax').
include('Axioms/SET007/SET007+143.ax').
include('Axioms/SET007/SET007+144.ax').
include('Axioms/SET007/SET007+145.ax').
include('Axioms/SET007/SET007+146.ax').
include('Axioms/SET007/SET007+147.ax').
include('Axioms/SET007/SET007+148.ax').
include('Axioms/SET007/SET007+149.ax').
include('Axioms/SET007/SET007+150.ax').
include('Axioms/SET007/SET007+151.ax').
include('Axioms/SET007/SET007+152.ax').
include('Axioms/SET007/SET007+153.ax').
include('Axioms/SET007/SET007+154.ax').
include('Axioms/SET007/SET007+155.ax').
include('Axioms/SET007/SET007+156.ax').
include('Axioms/SET007/SET007+157.ax').
include('Axioms/SET007/SET007+158.ax').
include('Axioms/SET007/SET007+159.ax').
include('Axioms/SET007/SET007+160.ax').
include('Axioms/SET007/SET007+161.ax').
include('Axioms/SET007/SET007+162.ax').
include('Axioms/SET007/SET007+163.ax').
include('Axioms/SET007/SET007+164.ax').
include('Axioms/SET007/SET007+165.ax').
include('Axioms/SET007/SET007+166.ax').
include('Axioms/SET007/SET007+167.ax').
include('Axioms/SET007/SET007+168.ax').
include('Axioms/SET007/SET007+169.ax').
include('Axioms/SET007/SET007+170.ax').
include('Axioms/SET007/SET007+171.ax').
include('Axioms/SET007/SET007+172.ax').
include('Axioms/SET007/SET007+173.ax').
include('Axioms/SET007/SET007+174.ax').
include('Axioms/SET007/SET007+175.ax').
include('Axioms/SET007/SET007+176.ax').
include('Axioms/SET007/SET007+177.ax').
include('Axioms/SET007/SET007+178.ax').
include('Axioms/SET007/SET007+179.ax').
include('Axioms/SET007/SET007+180.ax').
include('Axioms/SET007/SET007+181.ax').
include('Axioms/SET007/SET007+182.ax').
include('Axioms/SET007/SET007+183.ax').
include('Axioms/SET007/SET007+184.ax').
include('Axioms/SET007/SET007+185.ax').
include('Axioms/SET007/SET007+186.ax').
include('Axioms/SET007/SET007+187.ax').
include('Axioms/SET007/SET007+188.ax').
include('Axioms/SET007/SET007+189.ax').
include('Axioms/SET007/SET007+190.ax').
include('Axioms/SET007/SET007+191.ax').
include('Axioms/SET007/SET007+192.ax').
include('Axioms/SET007/SET007+193.ax').
include('Axioms/SET007/SET007+194.ax').
include('Axioms/SET007/SET007+195.ax').
include('Axioms/SET007/SET007+196.ax').
include('Axioms/SET007/SET007+197.ax').
include('Axioms/SET007/SET007+198.ax').
include('Axioms/SET007/SET007+199.ax').
include('Axioms/SET007/SET007+200.ax').
include('Axioms/SET007/SET007+201.ax').
include('Axioms/SET007/SET007+202.ax').
include('Axioms/SET007/SET007+203.ax').
include('Axioms/SET007/SET007+204.ax').
include('Axioms/SET007/SET007+205.ax').
include('Axioms/SET007/SET007+206.ax').
include('Axioms/SET007/SET007+207.ax').
include('Axioms/SET007/SET007+208.ax').
include('Axioms/SET007/SET007+209.ax').
include('Axioms/SET007/SET007+210.ax').
include('Axioms/SET007/SET007+211.ax').
include('Axioms/SET007/SET007+212.ax').
include('Axioms/SET007/SET007+213.ax').
include('Axioms/SET007/SET007+214.ax').
include('Axioms/SET007/SET007+215.ax').
include('Axioms/SET007/SET007+216.ax').
include('Axioms/SET007/SET007+217.ax').
include('Axioms/SET007/SET007+218.ax').
include('Axioms/SET007/SET007+219.ax').
include('Axioms/SET007/SET007+220.ax').
include('Axioms/SET007/SET007+221.ax').
include('Axioms/SET007/SET007+222.ax').
include('Axioms/SET007/SET007+223.ax').
include('Axioms/SET007/SET007+224.ax').
include('Axioms/SET007/SET007+225.ax').
include('Axioms/SET007/SET007+226.ax').
include('Axioms/SET007/SET007+227.ax').
include('Axioms/SET007/SET007+228.ax').
include('Axioms/SET007/SET007+229.ax').
include('Axioms/SET007/SET007+230.ax').
include('Axioms/SET007/SET007+231.ax').
include('Axioms/SET007/SET007+232.ax').
include('Axioms/SET007/SET007+233.ax').
include('Axioms/SET007/SET007+234.ax').
include('Axioms/SET007/SET007+235.ax').
include('Axioms/SET007/SET007+236.ax').
include('Axioms/SET007/SET007+237.ax').
include('Axioms/SET007/SET007+238.ax').
include('Axioms/SET007/SET007+239.ax').
include('Axioms/SET007/SET007+240.ax').
include('Axioms/SET007/SET007+241.ax').
include('Axioms/SET007/SET007+242.ax').
include('Axioms/SET007/SET007+243.ax').
include('Axioms/SET007/SET007+244.ax').
include('Axioms/SET007/SET007+245.ax').
include('Axioms/SET007/SET007+246.ax').
include('Axioms/SET007/SET007+247.ax').
include('Axioms/SET007/SET007+248.ax').
include('Axioms/SET007/SET007+249.ax').
include('Axioms/SET007/SET007+250.ax').
include('Axioms/SET007/SET007+251.ax').
include('Axioms/SET007/SET007+252.ax').
include('Axioms/SET007/SET007+253.ax').
include('Axioms/SET007/SET007+254.ax').
include('Axioms/SET007/SET007+255.ax').
include('Axioms/SET007/SET007+256.ax').
include('Axioms/SET007/SET007+257.ax').
include('Axioms/SET007/SET007+258.ax').
include('Axioms/SET007/SET007+259.ax').
include('Axioms/SET007/SET007+260.ax').
include('Axioms/SET007/SET007+261.ax').
include('Axioms/SET007/SET007+262.ax').
include('Axioms/SET007/SET007+263.ax').
include('Axioms/SET007/SET007+264.ax').
include('Axioms/SET007/SET007+265.ax').
include('Axioms/SET007/SET007+266.ax').
include('Axioms/SET007/SET007+267.ax').
include('Axioms/SET007/SET007+268.ax').
include('Axioms/SET007/SET007+269.ax').
include('Axioms/SET007/SET007+270.ax').
include('Axioms/SET007/SET007+271.ax').
include('Axioms/SET007/SET007+272.ax').
include('Axioms/SET007/SET007+273.ax').
include('Axioms/SET007/SET007+274.ax').
include('Axioms/SET007/SET007+275.ax').
include('Axioms/SET007/SET007+276.ax').
include('Axioms/SET007/SET007+277.ax').
include('Axioms/SET007/SET007+278.ax').
include('Axioms/SET007/SET007+279.ax').
include('Axioms/SET007/SET007+280.ax').
include('Axioms/SET007/SET007+281.ax').
include('Axioms/SET007/SET007+282.ax').
include('Axioms/SET007/SET007+283.ax').
include('Axioms/SET007/SET007+284.ax').
include('Axioms/SET007/SET007+285.ax').
include('Axioms/SET007/SET007+286.ax').
include('Axioms/SET007/SET007+287.ax').
include('Axioms/SET007/SET007+288.ax').
include('Axioms/SET007/SET007+289.ax').
include('Axioms/SET007/SET007+290.ax').
include('Axioms/SET007/SET007+291.ax').
include('Axioms/SET007/SET007+292.ax').
include('Axioms/SET007/SET007+293.ax').
include('Axioms/SET007/SET007+294.ax').
include('Axioms/SET007/SET007+295.ax').
include('Axioms/SET007/SET007+296.ax').
include('Axioms/SET007/SET007+297.ax').
include('Axioms/SET007/SET007+298.ax').
include('Axioms/SET007/SET007+299.ax').
include('Axioms/SET007/SET007+300.ax').
include('Axioms/SET007/SET007+301.ax').
include('Axioms/SET007/SET007+302.ax').
include('Axioms/SET007/SET007+303.ax').
include('Axioms/SET007/SET007+304.ax').
include('Axioms/SET007/SET007+305.ax').
include('Axioms/SET007/SET007+306.ax').
include('Axioms/SET007/SET007+307.ax').
include('Axioms/SET007/SET007+308.ax').
include('Axioms/SET007/SET007+309.ax').
include('Axioms/SET007/SET007+310.ax').
include('Axioms/SET007/SET007+311.ax').
include('Axioms/SET007/SET007+312.ax').
include('Axioms/SET007/SET007+313.ax').
include('Axioms/SET007/SET007+314.ax').
include('Axioms/SET007/SET007+315.ax').
include('Axioms/SET007/SET007+316.ax').
include('Axioms/SET007/SET007+317.ax').
include('Axioms/SET007/SET007+318.ax').
include('Axioms/SET007/SET007+319.ax').
include('Axioms/SET007/SET007+320.ax').
include('Axioms/SET007/SET007+321.ax').
include('Axioms/SET007/SET007+322.ax').
include('Axioms/SET007/SET007+323.ax').
include('Axioms/SET007/SET007+324.ax').
include('Axioms/SET007/SET007+325.ax').
include('Axioms/SET007/SET007+326.ax').
include('Axioms/SET007/SET007+327.ax').
include('Axioms/SET007/SET007+328.ax').
include('Axioms/SET007/SET007+329.ax').
include('Axioms/SET007/SET007+330.ax').
include('Axioms/SET007/SET007+331.ax').
include('Axioms/SET007/SET007+332.ax').
include('Axioms/SET007/SET007+333.ax').
include('Axioms/SET007/SET007+334.ax').
include('Axioms/SET007/SET007+335.ax').
include('Axioms/SET007/SET007+336.ax').
include('Axioms/SET007/SET007+337.ax').
include('Axioms/SET007/SET007+338.ax').
include('Axioms/SET007/SET007+339.ax').
include('Axioms/SET007/SET007+340.ax').
include('Axioms/SET007/SET007+341.ax').
include('Axioms/SET007/SET007+342.ax').
include('Axioms/SET007/SET007+343.ax').
include('Axioms/SET007/SET007+344.ax').
include('Axioms/SET007/SET007+345.ax').
include('Axioms/SET007/SET007+346.ax').
include('Axioms/SET007/SET007+347.ax').
include('Axioms/SET007/SET007+348.ax').
include('Axioms/SET007/SET007+349.ax').
include('Axioms/SET007/SET007+350.ax').
include('Axioms/SET007/SET007+351.ax').
include('Axioms/SET007/SET007+352.ax').
include('Axioms/SET007/SET007+353.ax').
include('Axioms/SET007/SET007+354.ax').
include('Axioms/SET007/SET007+355.ax').
include('Axioms/SET007/SET007+356.ax').
include('Axioms/SET007/SET007+357.ax').
include('Axioms/SET007/SET007+358.ax').
include('Axioms/SET007/SET007+359.ax').
include('Axioms/SET007/SET007+360.ax').
include('Axioms/SET007/SET007+361.ax').
include('Axioms/SET007/SET007+362.ax').
include('Axioms/SET007/SET007+363.ax').
include('Axioms/SET007/SET007+364.ax').
include('Axioms/SET007/SET007+365.ax').
include('Axioms/SET007/SET007+366.ax').
include('Axioms/SET007/SET007+367.ax').
include('Axioms/SET007/SET007+368.ax').
include('Axioms/SET007/SET007+369.ax').
include('Axioms/SET007/SET007+370.ax').
include('Axioms/SET007/SET007+371.ax').
include('Axioms/SET007/SET007+372.ax').
include('Axioms/SET007/SET007+373.ax').
include('Axioms/SET007/SET007+374.ax').
include('Axioms/SET007/SET007+375.ax').
include('Axioms/SET007/SET007+376.ax').
include('Axioms/SET007/SET007+377.ax').
include('Axioms/SET007/SET007+378.ax').
include('Axioms/SET007/SET007+379.ax').
include('Axioms/SET007/SET007+380.ax').
include('Axioms/SET007/SET007+381.ax').
include('Axioms/SET007/SET007+382.ax').
include('Axioms/SET007/SET007+383.ax').
include('Axioms/SET007/SET007+384.ax').
include('Axioms/SET007/SET007+385.ax').
include('Axioms/SET007/SET007+386.ax').
include('Axioms/SET007/SET007+387.ax').
include('Axioms/SET007/SET007+388.ax').
include('Axioms/SET007/SET007+389.ax').
include('Axioms/SET007/SET007+390.ax').
include('Axioms/SET007/SET007+391.ax').
include('Axioms/SET007/SET007+392.ax').
include('Axioms/SET007/SET007+393.ax').
include('Axioms/SET007/SET007+394.ax').
include('Axioms/SET007/SET007+395.ax').
include('Axioms/SET007/SET007+396.ax').
include('Axioms/SET007/SET007+397.ax').
include('Axioms/SET007/SET007+398.ax').
include('Axioms/SET007/SET007+399.ax').
include('Axioms/SET007/SET007+400.ax').
include('Axioms/SET007/SET007+401.ax').
include('Axioms/SET007/SET007+402.ax').
include('Axioms/SET007/SET007+403.ax').
include('Axioms/SET007/SET007+404.ax').
include('Axioms/SET007/SET007+405.ax').
include('Axioms/SET007/SET007+406.ax').
include('Axioms/SET007/SET007+407.ax').
include('Axioms/SET007/SET007+408.ax').
include('Axioms/SET007/SET007+409.ax').
include('Axioms/SET007/SET007+410.ax').
include('Axioms/SET007/SET007+411.ax').
include('Axioms/SET007/SET007+412.ax').
include('Axioms/SET007/SET007+413.ax').
include('Axioms/SET007/SET007+414.ax').
include('Axioms/SET007/SET007+415.ax').
include('Axioms/SET007/SET007+416.ax').
include('Axioms/SET007/SET007+417.ax').
include('Axioms/SET007/SET007+418.ax').
include('Axioms/SET007/SET007+419.ax').
include('Axioms/SET007/SET007+420.ax').
include('Axioms/SET007/SET007+421.ax').
include('Axioms/SET007/SET007+422.ax').
include('Axioms/SET007/SET007+423.ax').
include('Axioms/SET007/SET007+424.ax').
include('Axioms/SET007/SET007+425.ax').
include('Axioms/SET007/SET007+426.ax').
include('Axioms/SET007/SET007+427.ax').
include('Axioms/SET007/SET007+428.ax').
include('Axioms/SET007/SET007+429.ax').
include('Axioms/SET007/SET007+430.ax').
include('Axioms/SET007/SET007+431.ax').
include('Axioms/SET007/SET007+432.ax').
include('Axioms/SET007/SET007+433.ax').
include('Axioms/SET007/SET007+434.ax').
include('Axioms/SET007/SET007+435.ax').
include('Axioms/SET007/SET007+436.ax').
include('Axioms/SET007/SET007+437.ax').
include('Axioms/SET007/SET007+438.ax').
include('Axioms/SET007/SET007+439.ax').
include('Axioms/SET007/SET007+440.ax').
include('Axioms/SET007/SET007+441.ax').
include('Axioms/SET007/SET007+442.ax').
include('Axioms/SET007/SET007+443.ax').
include('Axioms/SET007/SET007+444.ax').
include('Axioms/SET007/SET007+445.ax').
include('Axioms/SET007/SET007+446.ax').
include('Axioms/SET007/SET007+447.ax').
include('Axioms/SET007/SET007+448.ax').
include('Axioms/SET007/SET007+449.ax').
include('Axioms/SET007/SET007+450.ax').
include('Axioms/SET007/SET007+451.ax').
include('Axioms/SET007/SET007+452.ax').
include('Axioms/SET007/SET007+453.ax').
include('Axioms/SET007/SET007+454.ax').
include('Axioms/SET007/SET007+455.ax').
include('Axioms/SET007/SET007+456.ax').
include('Axioms/SET007/SET007+457.ax').
include('Axioms/SET007/SET007+458.ax').
include('Axioms/SET007/SET007+459.ax').
include('Axioms/SET007/SET007+460.ax').
include('Axioms/SET007/SET007+461.ax').
include('Axioms/SET007/SET007+462.ax').
include('Axioms/SET007/SET007+463.ax').
include('Axioms/SET007/SET007+464.ax').
include('Axioms/SET007/SET007+465.ax').
include('Axioms/SET007/SET007+466.ax').
include('Axioms/SET007/SET007+467.ax').
include('Axioms/SET007/SET007+468.ax').
include('Axioms/SET007/SET007+469.ax').
include('Axioms/SET007/SET007+470.ax').
include('Axioms/SET007/SET007+471.ax').
include('Axioms/SET007/SET007+472.ax').
include('Axioms/SET007/SET007+473.ax').
include('Axioms/SET007/SET007+474.ax').
include('Axioms/SET007/SET007+475.ax').
include('Axioms/SET007/SET007+476.ax').
include('Axioms/SET007/SET007+477.ax').
include('Axioms/SET007/SET007+478.ax').
include('Axioms/SET007/SET007+479.ax').
include('Axioms/SET007/SET007+480.ax').
include('Axioms/SET007/SET007+481.ax').
include('Axioms/SET007/SET007+482.ax').
include('Axioms/SET007/SET007+483.ax').
include('Axioms/SET007/SET007+484.ax').
include('Axioms/SET007/SET007+485.ax').
include('Axioms/SET007/SET007+486.ax').
include('Axioms/SET007/SET007+487.ax').
include('Axioms/SET007/SET007+488.ax').
include('Axioms/SET007/SET007+489.ax').
include('Axioms/SET007/SET007+490.ax').
include('Axioms/SET007/SET007+491.ax').
include('Axioms/SET007/SET007+492.ax').
include('Axioms/SET007/SET007+493.ax').
include('Axioms/SET007/SET007+494.ax').
include('Axioms/SET007/SET007+495.ax').
include('Axioms/SET007/SET007+496.ax').
include('Axioms/SET007/SET007+497.ax').
include('Axioms/SET007/SET007+498.ax').
include('Axioms/SET007/SET007+499.ax').
include('Axioms/SET007/SET007+500.ax').
include('Axioms/SET007/SET007+501.ax').
include('Axioms/SET007/SET007+502.ax').
include('Axioms/SET007/SET007+503.ax').
include('Axioms/SET007/SET007+504.ax').
include('Axioms/SET007/SET007+505.ax').
include('Axioms/SET007/SET007+506.ax').
include('Axioms/SET007/SET007+507.ax').
include('Axioms/SET007/SET007+508.ax').
include('Axioms/SET007/SET007+509.ax').
include('Axioms/SET007/SET007+510.ax').
include('Axioms/SET007/SET007+511.ax').
include('Axioms/SET007/SET007+512.ax').
include('Axioms/SET007/SET007+513.ax').
include('Axioms/SET007/SET007+514.ax').
include('Axioms/SET007/SET007+515.ax').
include('Axioms/SET007/SET007+516.ax').
include('Axioms/SET007/SET007+517.ax').
include('Axioms/SET007/SET007+518.ax').
include('Axioms/SET007/SET007+519.ax').
include('Axioms/SET007/SET007+520.ax').
include('Axioms/SET007/SET007+521.ax').
include('Axioms/SET007/SET007+522.ax').
include('Axioms/SET007/SET007+523.ax').
include('Axioms/SET007/SET007+524.ax').
include('Axioms/SET007/SET007+525.ax').
include('Axioms/SET007/SET007+526.ax').
include('Axioms/SET007/SET007+527.ax').
include('Axioms/SET007/SET007+528.ax').
include('Axioms/SET007/SET007+529.ax').
include('Axioms/SET007/SET007+530.ax').
include('Axioms/SET007/SET007+531.ax').
include('Axioms/SET007/SET007+532.ax').
include('Axioms/SET007/SET007+533.ax').
include('Axioms/SET007/SET007+534.ax').
include('Axioms/SET007/SET007+535.ax').
include('Axioms/SET007/SET007+536.ax').
include('Axioms/SET007/SET007+537.ax').
include('Axioms/SET007/SET007+538.ax').
include('Axioms/SET007/SET007+539.ax').
include('Axioms/SET007/SET007+540.ax').
include('Axioms/SET007/SET007+541.ax').
include('Axioms/SET007/SET007+542.ax').
include('Axioms/SET007/SET007+543.ax').
include('Axioms/SET007/SET007+544.ax').
include('Axioms/SET007/SET007+545.ax').
include('Axioms/SET007/SET007+546.ax').
include('Axioms/SET007/SET007+547.ax').
include('Axioms/SET007/SET007+548.ax').
include('Axioms/SET007/SET007+549.ax').
include('Axioms/SET007/SET007+550.ax').
include('Axioms/SET007/SET007+551.ax').
include('Axioms/SET007/SET007+552.ax').
include('Axioms/SET007/SET007+553.ax').
include('Axioms/SET007/SET007+554.ax').
include('Axioms/SET007/SET007+555.ax').
include('Axioms/SET007/SET007+556.ax').
include('Axioms/SET007/SET007+557.ax').
include('Axioms/SET007/SET007+558.ax').
include('Axioms/SET007/SET007+559.ax').
include('Axioms/SET007/SET007+560.ax').
include('Axioms/SET007/SET007+561.ax').
include('Axioms/SET007/SET007+562.ax').
include('Axioms/SET007/SET007+563.ax').
include('Axioms/SET007/SET007+564.ax').
include('Axioms/SET007/SET007+565.ax').
include('Axioms/SET007/SET007+566.ax').
include('Axioms/SET007/SET007+567.ax').
include('Axioms/SET007/SET007+568.ax').
include('Axioms/SET007/SET007+569.ax').
include('Axioms/SET007/SET007+570.ax').
include('Axioms/SET007/SET007+571.ax').
include('Axioms/SET007/SET007+572.ax').
include('Axioms/SET007/SET007+573.ax').
include('Axioms/SET007/SET007+574.ax').
include('Axioms/SET007/SET007+575.ax').
include('Axioms/SET007/SET007+576.ax').
include('Axioms/SET007/SET007+577.ax').
include('Axioms/SET007/SET007+578.ax').
include('Axioms/SET007/SET007+579.ax').
include('Axioms/SET007/SET007+580.ax').
include('Axioms/SET007/SET007+581.ax').
include('Axioms/SET007/SET007+582.ax').
include('Axioms/SET007/SET007+583.ax').
include('Axioms/SET007/SET007+584.ax').
include('Axioms/SET007/SET007+585.ax').
include('Axioms/SET007/SET007+586.ax').
include('Axioms/SET007/SET007+587.ax').
include('Axioms/SET007/SET007+588.ax').
include('Axioms/SET007/SET007+589.ax').
include('Axioms/SET007/SET007+590.ax').
include('Axioms/SET007/SET007+591.ax').
include('Axioms/SET007/SET007+592.ax').
include('Axioms/SET007/SET007+593.ax').
include('Axioms/SET007/SET007+594.ax').
include('Axioms/SET007/SET007+595.ax').
include('Axioms/SET007/SET007+596.ax').
include('Axioms/SET007/SET007+597.ax').
include('Axioms/SET007/SET007+598.ax').
include('Axioms/SET007/SET007+599.ax').
include('Axioms/SET007/SET007+600.ax').
include('Axioms/SET007/SET007+601.ax').
include('Axioms/SET007/SET007+602.ax').
include('Axioms/SET007/SET007+603.ax').
include('Axioms/SET007/SET007+604.ax').
include('Axioms/SET007/SET007+605.ax').
include('Axioms/SET007/SET007+606.ax').
include('Axioms/SET007/SET007+607.ax').
include('Axioms/SET007/SET007+608.ax').
include('Axioms/SET007/SET007+609.ax').
include('Axioms/SET007/SET007+610.ax').
include('Axioms/SET007/SET007+611.ax').
include('Axioms/SET007/SET007+612.ax').
include('Axioms/SET007/SET007+613.ax').
include('Axioms/SET007/SET007+614.ax').
include('Axioms/SET007/SET007+615.ax').
include('Axioms/SET007/SET007+616.ax').
include('Axioms/SET007/SET007+617.ax').
include('Axioms/SET007/SET007+618.ax').
include('Axioms/SET007/SET007+619.ax').
include('Axioms/SET007/SET007+620.ax').
include('Axioms/SET007/SET007+621.ax').
include('Axioms/SET007/SET007+622.ax').
include('Axioms/SET007/SET007+623.ax').
include('Axioms/SET007/SET007+624.ax').
include('Axioms/SET007/SET007+625.ax').
include('Axioms/SET007/SET007+626.ax').
include('Axioms/SET007/SET007+627.ax').
include('Axioms/SET007/SET007+628.ax').
include('Axioms/SET007/SET007+629.ax').
include('Axioms/SET007/SET007+630.ax').
include('Axioms/SET007/SET007+631.ax').
include('Axioms/SET007/SET007+632.ax').
include('Axioms/SET007/SET007+633.ax').
include('Axioms/SET007/SET007+634.ax').
include('Axioms/SET007/SET007+635.ax').
include('Axioms/SET007/SET007+636.ax').
include('Axioms/SET007/SET007+637.ax').
include('Axioms/SET007/SET007+638.ax').
include('Axioms/SET007/SET007+639.ax').
include('Axioms/SET007/SET007+640.ax').
include('Axioms/SET007/SET007+641.ax').
include('Axioms/SET007/SET007+642.ax').
include('Axioms/SET007/SET007+643.ax').
include('Axioms/SET007/SET007+644.ax').
include('Axioms/SET007/SET007+645.ax').
include('Axioms/SET007/SET007+646.ax').
include('Axioms/SET007/SET007+647.ax').
include('Axioms/SET007/SET007+648.ax').
include('Axioms/SET007/SET007+649.ax').
include('Axioms/SET007/SET007+650.ax').
include('Axioms/SET007/SET007+651.ax').
include('Axioms/SET007/SET007+652.ax').
include('Axioms/SET007/SET007+653.ax').
include('Axioms/SET007/SET007+654.ax').
include('Axioms/SET007/SET007+655.ax').
include('Axioms/SET007/SET007+656.ax').
include('Axioms/SET007/SET007+657.ax').
include('Axioms/SET007/SET007+658.ax').
include('Axioms/SET007/SET007+659.ax').
include('Axioms/SET007/SET007+660.ax').
include('Axioms/SET007/SET007+661.ax').
include('Axioms/SET007/SET007+662.ax').
include('Axioms/SET007/SET007+663.ax').
include('Axioms/SET007/SET007+664.ax').
include('Axioms/SET007/SET007+665.ax').
include('Axioms/SET007/SET007+666.ax').
include('Axioms/SET007/SET007+667.ax').
include('Axioms/SET007/SET007+668.ax').
include('Axioms/SET007/SET007+669.ax').
include('Axioms/SET007/SET007+670.ax').
include('Axioms/SET007/SET007+671.ax').
include('Axioms/SET007/SET007+672.ax').
include('Axioms/SET007/SET007+673.ax').
include('Axioms/SET007/SET007+674.ax').
include('Axioms/SET007/SET007+675.ax').
include('Axioms/SET007/SET007+676.ax').
include('Axioms/SET007/SET007+677.ax').
include('Axioms/SET007/SET007+678.ax').
include('Axioms/SET007/SET007+679.ax').
include('Axioms/SET007/SET007+680.ax').
include('Axioms/SET007/SET007+681.ax').
include('Axioms/SET007/SET007+682.ax').
include('Axioms/SET007/SET007+683.ax').
include('Axioms/SET007/SET007+684.ax').
include('Axioms/SET007/SET007+685.ax').
include('Axioms/SET007/SET007+686.ax').
include('Axioms/SET007/SET007+687.ax').
include('Axioms/SET007/SET007+688.ax').
include('Axioms/SET007/SET007+689.ax').
include('Axioms/SET007/SET007+690.ax').
include('Axioms/SET007/SET007+691.ax').
include('Axioms/SET007/SET007+692.ax').
include('Axioms/SET007/SET007+693.ax').
include('Axioms/SET007/SET007+694.ax').
include('Axioms/SET007/SET007+695.ax').
include('Axioms/SET007/SET007+696.ax').
include('Axioms/SET007/SET007+697.ax').
include('Axioms/SET007/SET007+698.ax').
include('Axioms/SET007/SET007+699.ax').
include('Axioms/SET007/SET007+700.ax').
include('Axioms/SET007/SET007+701.ax').
include('Axioms/SET007/SET007+702.ax').
include('Axioms/SET007/SET007+703.ax').
include('Axioms/SET007/SET007+704.ax').
include('Axioms/SET007/SET007+705.ax').
include('Axioms/SET007/SET007+706.ax').
include('Axioms/SET007/SET007+707.ax').
include('Axioms/SET007/SET007+708.ax').
include('Axioms/SET007/SET007+709.ax').
include('Axioms/SET007/SET007+710.ax').
include('Axioms/SET007/SET007+711.ax').
include('Axioms/SET007/SET007+712.ax').
include('Axioms/SET007/SET007+713.ax').
include('Axioms/SET007/SET007+714.ax').
include('Axioms/SET007/SET007+715.ax').
include('Axioms/SET007/SET007+716.ax').
include('Axioms/SET007/SET007+717.ax').
include('Axioms/SET007/SET007+718.ax').
include('Axioms/SET007/SET007+719.ax').
include('Axioms/SET007/SET007+720.ax').
include('Axioms/SET007/SET007+721.ax').
include('Axioms/SET007/SET007+722.ax').
include('Axioms/SET007/SET007+723.ax').
include('Axioms/SET007/SET007+724.ax').
include('Axioms/SET007/SET007+725.ax').
include('Axioms/SET007/SET007+726.ax').
include('Axioms/SET007/SET007+727.ax').
include('Axioms/SET007/SET007+728.ax').
include('Axioms/SET007/SET007+729.ax').
include('Axioms/SET007/SET007+730.ax').
include('Axioms/SET007/SET007+731.ax').
include('Axioms/SET007/SET007+732.ax').
include('Axioms/SET007/SET007+733.ax').
include('Axioms/SET007/SET007+734.ax').
include('Axioms/SET007/SET007+735.ax').
include('Axioms/SET007/SET007+736.ax').
include('Axioms/SET007/SET007+737.ax').
include('Axioms/SET007/SET007+738.ax').
include('Axioms/SET007/SET007+739.ax').
include('Axioms/SET007/SET007+740.ax').
include('Axioms/SET007/SET007+741.ax').
include('Axioms/SET007/SET007+742.ax').
include('Axioms/SET007/SET007+743.ax').
include('Axioms/SET007/SET007+744.ax').
include('Axioms/SET007/SET007+745.ax').
include('Axioms/SET007/SET007+746.ax').
include('Axioms/SET007/SET007+747.ax').
include('Axioms/SET007/SET007+748.ax').
include('Axioms/SET007/SET007+749.ax').
include('Axioms/SET007/SET007+750.ax').
include('Axioms/SET007/SET007+751.ax').
include('Axioms/SET007/SET007+752.ax').
include('Axioms/SET007/SET007+753.ax').
include('Axioms/SET007/SET007+754.ax').
include('Axioms/SET007/SET007+755.ax').
include('Axioms/SET007/SET007+756.ax').
include('Axioms/SET007/SET007+757.ax').
include('Axioms/SET007/SET007+758.ax').
include('Axioms/SET007/SET007+759.ax').
include('Axioms/SET007/SET007+760.ax').
include('Axioms/SET007/SET007+761.ax').
include('Axioms/SET007/SET007+762.ax').
include('Axioms/SET007/SET007+763.ax').
include('Axioms/SET007/SET007+764.ax').
include('Axioms/SET007/SET007+765.ax').
include('Axioms/SET007/SET007+766.ax').
include('Axioms/SET007/SET007+767.ax').
include('Axioms/SET007/SET007+768.ax').
include('Axioms/SET007/SET007+769.ax').
include('Axioms/SET007/SET007+770.ax').
include('Axioms/SET007/SET007+771.ax').
include('Axioms/SET007/SET007+772.ax').
include('Axioms/SET007/SET007+773.ax').
include('Axioms/SET007/SET007+774.ax').
include('Axioms/SET007/SET007+775.ax').
include('Axioms/SET007/SET007+776.ax').
include('Axioms/SET007/SET007+777.ax').
include('Axioms/SET007/SET007+778.ax').
include('Axioms/SET007/SET007+779.ax').
include('Axioms/SET007/SET007+780.ax').
include('Axioms/SET007/SET007+781.ax').
include('Axioms/SET007/SET007+782.ax').
include('Axioms/SET007/SET007+783.ax').
include('Axioms/SET007/SET007+784.ax').
include('Axioms/SET007/SET007+785.ax').
include('Axioms/SET007/SET007+786.ax').
include('Axioms/SET007/SET007+787.ax').
include('Axioms/SET007/SET007+788.ax').
include('Axioms/SET007/SET007+789.ax').
include('Axioms/SET007/SET007+790.ax').
include('Axioms/SET007/SET007+791.ax').
include('Axioms/SET007/SET007+792.ax').
include('Axioms/SET007/SET007+793.ax').
include('Axioms/SET007/SET007+794.ax').
include('Axioms/SET007/SET007+795.ax').
include('Axioms/SET007/SET007+796.ax').
include('Axioms/SET007/SET007+797.ax').
include('Axioms/SET007/SET007+798.ax').
include('Axioms/SET007/SET007+799.ax').
include('Axioms/SET007/SET007+800.ax').
include('Axioms/SET007/SET007+801.ax').
include('Axioms/SET007/SET007+802.ax').
include('Axioms/SET007/SET007+803.ax').
include('Axioms/SET007/SET007+804.ax').
include('Axioms/SET007/SET007+805.ax').
include('Axioms/SET007/SET007+806.ax').
include('Axioms/SET007/SET007+807.ax').
include('Axioms/SET007/SET007+808.ax').
include('Axioms/SET007/SET007+809.ax').
include('Axioms/SET007/SET007+810.ax').
include('Axioms/SET007/SET007+811.ax').
include('Axioms/SET007/SET007+812.ax').
include('Axioms/SET007/SET007+813.ax').
include('Axioms/SET007/SET007+814.ax').
include('Axioms/SET007/SET007+815.ax').
include('Axioms/SET007/SET007+816.ax').
include('Axioms/SET007/SET007+817.ax').
include('Axioms/SET007/SET007+818.ax').
include('Axioms/SET007/SET007+819.ax').
include('Axioms/SET007/SET007+820.ax').
include('Axioms/SET007/SET007+821.ax').
include('Axioms/SET007/SET007+822.ax').
include('Axioms/SET007/SET007+823.ax').
include('Axioms/SET007/SET007+824.ax').
include('Axioms/SET007/SET007+825.ax').
include('Axioms/SET007/SET007+826.ax').
include('Axioms/SET007/SET007+827.ax').
include('Axioms/SET007/SET007+828.ax').
include('Axioms/SET007/SET007+829.ax').
include('Axioms/SET007/SET007+830.ax').
include('Axioms/SET007/SET007+831.ax').
include('Axioms/SET007/SET007+832.ax').
include('Axioms/SET007/SET007+833.ax').
include('Axioms/SET007/SET007+834.ax').
include('Axioms/SET007/SET007+835.ax').
include('Axioms/SET007/SET007+836.ax').
include('Axioms/SET007/SET007+837.ax').
include('Axioms/SET007/SET007+838.ax').
include('Axioms/SET007/SET007+839.ax').
include('Axioms/SET007/SET007+840.ax').
include('Axioms/SET007/SET007+841.ax').
include('Axioms/SET007/SET007+842.ax').
include('Axioms/SET007/SET007+843.ax').
include('Axioms/SET007/SET007+844.ax').
include('Axioms/SET007/SET007+845.ax').
include('Axioms/SET007/SET007+846.ax').
include('Axioms/SET007/SET007+847.ax').
include('Axioms/SET007/SET007+848.ax').
include('Axioms/SET007/SET007+849.ax').
include('Axioms/SET007/SET007+850.ax').
include('Axioms/SET007/SET007+851.ax').
include('Axioms/SET007/SET007+852.ax').
include('Axioms/SET007/SET007+853.ax').
include('Axioms/SET007/SET007+854.ax').
include('Axioms/SET007/SET007+855.ax').
include('Axioms/SET007/SET007+856.ax').
include('Axioms/SET007/SET007+857.ax').
include('Axioms/SET007/SET007+858.ax').
include('Axioms/SET007/SET007+859.ax').
include('Axioms/SET007/SET007+860.ax').
include('Axioms/SET007/SET007+861.ax').
include('Axioms/SET007/SET007+862.ax').
include('Axioms/SET007/SET007+863.ax').
include('Axioms/SET007/SET007+864.ax').
include('Axioms/SET007/SET007+865.ax').
include('Axioms/SET007/SET007+866.ax').
include('Axioms/SET007/SET007+867.ax').
include('Axioms/SET007/SET007+868.ax').
include('Axioms/SET007/SET007+869.ax').
include('Axioms/SET007/SET007+870.ax').
include('Axioms/SET007/SET007+871.ax').
include('Axioms/SET007/SET007+872.ax').
include('Axioms/SET007/SET007+873.ax').
include('Axioms/SET007/SET007+874.ax').
include('Axioms/SET007/SET007+875.ax').
include('Axioms/SET007/SET007+876.ax').
include('Axioms/SET007/SET007+877.ax').
include('Axioms/SET007/SET007+878.ax').
include('Axioms/SET007/SET007+879.ax').
include('Axioms/SET007/SET007+880.ax').
include('Axioms/SET007/SET007+881.ax').
include('Axioms/SET007/SET007+882.ax').
include('Axioms/SET007/SET007+883.ax').
include('Axioms/SET007/SET007+884.ax').
include('Axioms/SET007/SET007+885.ax').
include('Axioms/SET007/SET007+886.ax').
include('Axioms/SET007/SET007+887.ax').
include('Axioms/SET007/SET007+888.ax').
include('Axioms/SET007/SET007+889.ax').
include('Axioms/SET007/SET007+890.ax').
include('Axioms/SET007/SET007+891.ax').
include('Axioms/SET007/SET007+892.ax').
include('Axioms/SET007/SET007+893.ax').
include('Axioms/SET007/SET007+894.ax').
include('Axioms/SET007/SET007+895.ax').
include('Axioms/SET007/SET007+896.ax').
include('Axioms/SET007/SET007+897.ax').
include('Axioms/SET007/SET007+898.ax').
include('Axioms/SET007/SET007+899.ax').
include('Axioms/SET007/SET007+900.ax').
%------------------------------------------------------------------------------
fof(dt_k1_relset_2,axiom,(
    ! [A,B,C] :
      ( ( m1_subset_1(B,k1_zfmisc_1(k1_zfmisc_1(A)))
        & m1_subset_1(C,k1_zfmisc_1(k1_zfmisc_1(A))) )
     => m1_subset_1(k1_relset_2(A,B,C),k1_zfmisc_1(k1_zfmisc_1(A))) ) )).

fof(commutativity_k1_relset_2,axiom,(
    ! [A,B,C] :
      ( ( m1_subset_1(B,k1_zfmisc_1(k1_zfmisc_1(A)))
        & m1_subset_1(C,k1_zfmisc_1(k1_zfmisc_1(A))) )
     => k1_relset_2(A,B,C) = k1_relset_2(A,C,B) ) )).

fof(idempotence_k1_relset_2,axiom,(
    ! [A,B,C] :
      ( ( m1_subset_1(B,k1_zfmisc_1(k1_zfmisc_1(A)))
        & m1_subset_1(C,k1_zfmisc_1(k1_zfmisc_1(A))) )
     => k1_relset_2(A,B,B) = B ) )).

fof(redefinition_k1_relset_2,axiom,(
    ! [A,B,C] :
      ( ( m1_subset_1(B,k1_zfmisc_1(k1_zfmisc_1(A)))
        & m1_subset_1(C,k1_zfmisc_1(k1_zfmisc_1(A))) )
     => k1_relset_2(A,B,C) = k3_xboole_0(B,C) ) )).

fof(dt_k2_relset_2,axiom,(
    ! [A,B,C,D] :
      ( ( m1_relset_1(C,A,B)
        & m1_subset_1(D,A) )
     => m1_subset_1(k2_relset_2(A,B,C,D),k1_zfmisc_1(B)) ) )).

fof(dt_k3_relset_2,axiom,(
    ! [A,B,C,D] :
      ( ( m1_relset_1(C,A,B)
        & m1_subset_1(D,A) )
     => m1_subset_1(k3_relset_2(A,B,C,D),k1_zfmisc_1(A)) ) )).

fof(dt_k4_relset_2,axiom,(
    ! [A,B,C,D] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(A,k1_zfmisc_1(B))))
     => m1_subset_1(k4_relset_2(A,B,C,D),k1_zfmisc_1(k1_zfmisc_1(B))) ) )).

fof(redefinition_k4_relset_2,axiom,(
    ! [A,B,C,D] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(A,k1_zfmisc_1(B))))
     => k4_relset_2(A,B,C,D) = k9_relat_1(C,D) ) )).

fof(dt_k5_relset_2,axiom,(
    ! [A,B] :
      ( v1_relat_1(B)
     => ( v1_relat_1(k5_relset_2(A,B))
        & v1_funct_1(k5_relset_2(A,B)) ) ) )).

fof(dt_k6_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(B,A)))
     => ( v1_funct_1(k6_relset_2(A,B,C))
        & v1_funct_2(k6_relset_2(A,B,C),k1_zfmisc_1(B),k1_zfmisc_1(A))
        & m2_relset_1(k6_relset_2(A,B,C),k1_zfmisc_1(B),k1_zfmisc_1(A)) ) ) )).

fof(redefinition_k6_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(B,A)))
     => k6_relset_2(A,B,C) = k5_relset_2(B,C) ) )).

fof(dt_k7_relset_2,axiom,(
    $true )).

fof(dt_k8_relset_2,axiom,(
    ! [A,B,C,D] :
      ( ( m1_subset_1(C,k1_zfmisc_1(A))
        & m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B))) )
     => m1_subset_1(k8_relset_2(A,B,C,D),k1_zfmisc_1(B)) ) )).

fof(redefinition_k8_relset_2,axiom,(
    ! [A,B,C,D] :
      ( ( m1_subset_1(C,k1_zfmisc_1(A))
        & m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B))) )
     => k8_relset_2(A,B,C,D) = k7_relset_2(A,B,C,D) ) )).

fof(dt_k9_relset_2,axiom,(
    ! [A,B,C,D,E] :
      ( ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
        & m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(B,C))) )
     => m2_relset_1(k9_relset_2(A,B,C,D,E),A,C) ) )).

fof(redefinition_k9_relset_2,axiom,(
    ! [A,B,C,D,E] :
      ( ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
        & m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(B,C))) )
     => k9_relset_2(A,B,C,D,E) = k5_relat_1(D,E) ) )).

fof(t1_relset_2,axiom,(
    ! [A,B] :
      ( r2_hidden(A,k3_pua2mss1(B))
    <=> ? [C] :
          ( A = k1_tarski(C)
          & r2_hidden(C,B) ) ) )).

fof(t2_relset_2,axiom,(
    ! [A] :
      ( A = k1_xboole_0
    <=> k3_pua2mss1(A) = k1_xboole_0 ) )).

fof(t3_relset_2,axiom,(
    ! [A,B] : k3_pua2mss1(k2_xboole_0(A,B)) = k2_xboole_0(k3_pua2mss1(A),k3_pua2mss1(B)) )).

fof(t4_relset_2,axiom,(
    ! [A,B] : k3_pua2mss1(k3_xboole_0(A,B)) = k3_xboole_0(k3_pua2mss1(A),k3_pua2mss1(B)) )).

fof(t5_relset_2,axiom,(
    ! [A,B] : k3_pua2mss1(k4_xboole_0(A,B)) = k4_xboole_0(k3_pua2mss1(A),k3_pua2mss1(B)) )).

fof(t6_relset_2,axiom,(
    ! [A,B] :
      ( r1_tarski(A,B)
    <=> r1_tarski(k3_pua2mss1(A),k3_pua2mss1(B)) ) )).

fof(t7_relset_2,axiom,(
    ! [A,B] :
      ( m1_subset_1(B,k1_zfmisc_1(k1_zfmisc_1(A)))
     => ! [C] :
          ( m1_subset_1(C,k1_zfmisc_1(k1_zfmisc_1(A)))
         => r1_tarski(k5_subset_1(A,k8_setfam_1(A,B),k8_setfam_1(A,C)),k8_setfam_1(A,k1_relset_2(A,B,C))) ) ) )).

fof(t8_relset_2,axiom,(
    ! [A] :
      ( v1_relat_1(A)
     => ! [B] :
          ( v1_relat_1(B)
         => ! [C] :
              ( v1_relat_1(C)
             => r1_tarski(k5_relat_1(k3_xboole_0(A,B),C),k3_xboole_0(k5_relat_1(A,C),k5_relat_1(B,C))) ) ) ) )).

fof(d1_relset_2,axiom,(
    ! [A,B,C] :
      ( m2_relset_1(C,A,B)
     => ! [D] :
          ( m1_subset_1(D,A)
         => k2_relset_2(A,B,C,D) = k10_relset_1(A,B,C,k1_tarski(D)) ) ) )).

fof(t9_relset_2,axiom,(
    ! [A,B,C] :
      ( v1_relat_1(C)
     => ( r2_hidden(A,k9_relat_1(C,k1_tarski(B)))
      <=> r2_hidden(k4_tarski(B,A),C) ) ) )).

fof(t10_relset_2,axiom,(
    ! [A,B] :
      ( v1_relat_1(B)
     => ! [C] :
          ( v1_relat_1(C)
         => k9_relat_1(k2_xboole_0(B,C),k1_tarski(A)) = k2_xboole_0(k9_relat_1(B,k1_tarski(A)),k9_relat_1(C,k1_tarski(A))) ) ) )).

fof(t11_relset_2,axiom,(
    ! [A,B] :
      ( v1_relat_1(B)
     => ! [C] :
          ( v1_relat_1(C)
         => k9_relat_1(k3_xboole_0(B,C),k1_tarski(A)) = k3_xboole_0(k9_relat_1(B,k1_tarski(A)),k9_relat_1(C,k1_tarski(A))) ) ) )).

fof(t12_relset_2,axiom,(
    ! [A,B] :
      ( v1_relat_1(B)
     => ! [C] :
          ( v1_relat_1(C)
         => k9_relat_1(k4_xboole_0(B,C),k1_tarski(A)) = k4_xboole_0(k9_relat_1(B,k1_tarski(A)),k9_relat_1(C,k1_tarski(A))) ) ) )).

fof(t13_relset_2,axiom,(
    ! [A,B] :
      ( v1_relat_1(B)
     => ! [C] :
          ( v1_relat_1(C)
         => r1_tarski(k9_relat_1(k3_xboole_0(B,C),k3_pua2mss1(A)),k3_xboole_0(k9_relat_1(B,k3_pua2mss1(A)),k9_relat_1(C,k3_pua2mss1(A)))) ) ) )).

fof(d2_relset_2,axiom,(
    ! [A,B,C] :
      ( m2_relset_1(C,A,B)
     => ! [D] :
          ( m1_subset_1(D,A)
         => k3_relset_2(A,B,C,D) = k11_relset_1(A,B,C,k1_tarski(D)) ) ) )).

fof(t14_relset_2,axiom,(
    ! [A,B] :
      ( m1_subset_1(B,k1_zfmisc_1(k1_zfmisc_1(A)))
     => ! [C] :
          ( v1_relat_1(C)
         => k9_relat_1(C,k5_setfam_1(A,B)) = k3_tarski(a_3_0_relset_2(A,B,C)) ) ) )).

fof(t15_relset_2,axiom,(
    ! [A] :
      ( ~ v1_xboole_0(A)
     => ! [B] :
          ( m1_subset_1(B,k1_zfmisc_1(A))
         => B = k3_tarski(a_2_0_relset_2(A,B)) ) ) )).

fof(t16_relset_2,axiom,(
    ! [A] :
      ( ~ v1_xboole_0(A)
     => ! [B] :
          ( m1_subset_1(B,k1_zfmisc_1(A))
         => m1_subset_1(a_2_0_relset_2(A,B),k1_zfmisc_1(k1_zfmisc_1(A))) ) ) )).

fof(t17_relset_2,axiom,(
    ! [A] :
      ( ~ v1_xboole_0(A)
     => ! [B,C] :
          ( m1_subset_1(C,k1_zfmisc_1(A))
         => ! [D] :
              ( m2_relset_1(D,A,B)
             => k10_relset_1(A,B,D,C) = k3_tarski(a_4_0_relset_2(A,B,C,D)) ) ) ) )).

fof(t18_relset_2,axiom,(
    ! [A] :
      ( ~ v1_xboole_0(A)
     => ! [B,C] :
          ( m1_subset_1(C,k1_zfmisc_1(A))
         => ! [D] :
              ( m2_relset_1(D,A,B)
             => m1_subset_1(a_4_0_relset_2(A,B,C,D),k1_zfmisc_1(k1_zfmisc_1(B))) ) ) ) )).

fof(d3_relset_2,axiom,(
    ! [A,B] :
      ( v1_relat_1(B)
     => ! [C] :
          ( ( v1_relat_1(C)
            & v1_funct_1(C) )
         => ( C = k5_relset_2(A,B)
          <=> ( k1_relat_1(C) = k1_zfmisc_1(A)
              & ! [D] :
                  ( r1_tarski(D,A)
                 => k1_funct_1(C,D) = k9_relat_1(B,D) ) ) ) ) ) )).

fof(t19_relset_2,axiom,(
    ! [A,B,C,D] :
      ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(B,C)))
     => ( r2_hidden(A,k1_relat_1(k5_relset_2(B,D)))
       => k1_funct_1(k5_relset_2(B,D),A) = k9_relat_1(D,A) ) ) )).

fof(t20_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(A,B)))
     => r1_tarski(k2_relat_1(k5_relset_2(A,C)),k1_zfmisc_1(k2_relat_1(C))) ) )).

fof(t21_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(A,B)))
     => ( v1_funct_1(k5_relset_2(A,C))
        & v1_funct_2(k5_relset_2(A,C),k1_zfmisc_1(A),k1_zfmisc_1(k2_relat_1(C)))
        & m2_relset_1(k5_relset_2(A,C),k1_zfmisc_1(A),k1_zfmisc_1(k2_relat_1(C))) ) ) )).

fof(t22_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(A,B)))
     => r1_tarski(k5_setfam_1(B,k4_relset_2(k1_zfmisc_1(A),B,k6_relset_2(B,A,C),A)),k9_relat_1(C,k3_tarski(A))) ) )).

fof(d4_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => k7_relset_2(A,B,C,D) = k8_setfam_1(B,k4_relset_2(k1_zfmisc_1(A),B,k6_relset_2(B,A,D),k3_pua2mss1(C))) ) ) )).

fof(t23_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => ( k4_relset_2(k1_zfmisc_1(A),B,k6_relset_2(B,A,D),k3_pua2mss1(C)) = k1_xboole_0
          <=> C = k1_xboole_0 ) ) ) )).

fof(t24_relset_2,axiom,(
    ! [A,B,C,D] :
      ( m1_subset_1(D,k1_zfmisc_1(A))
     => ! [E] :
          ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => ( r2_hidden(C,k8_relset_2(A,B,D,E))
           => ! [F] :
                ( r2_hidden(F,D)
               => r2_hidden(C,k9_relat_1(E,k1_tarski(F))) ) ) ) ) )).

fof(t25_relset_2,axiom,(
    ! [A] :
      ( ~ v1_xboole_0(A)
     => ! [B,C] :
          ( m1_subset_1(C,k1_zfmisc_1(B))
         => ! [D] :
              ( m1_subset_1(D,A)
             => ! [E] :
                  ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(B,A)))
                 => ( r2_hidden(D,k8_relset_2(B,A,C,E))
                  <=> ! [F] :
                        ( r2_hidden(F,C)
                       => r2_hidden(D,k9_relat_1(E,k1_tarski(F))) ) ) ) ) ) ) )).

fof(t26_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(A))
         => ! [E] :
              ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(A,B)))
             => ( k4_relset_2(k1_zfmisc_1(A),B,k6_relset_2(B,A,E),k3_pua2mss1(C)) = k1_xboole_0
               => k8_relset_2(A,B,k4_subset_1(A,C,D),E) = k8_relset_2(A,B,D,E) ) ) ) ) )).

fof(t27_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(A))
         => ! [E] :
              ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(A,B)))
             => k8_relset_2(A,B,k4_subset_1(A,C,D),E) = k5_subset_1(B,k8_relset_2(A,B,C,E),k8_relset_2(A,B,D,E)) ) ) ) )).

fof(t28_relset_2,axiom,(
    ! [A] :
      ( ~ v1_xboole_0(A)
     => ! [B,C] :
          ( m1_subset_1(C,k1_zfmisc_1(k1_zfmisc_1(A)))
         => ! [D] :
              ( m2_relset_1(D,A,B)
             => m1_subset_1(a_4_1_relset_2(A,B,C,D),k1_zfmisc_1(k1_zfmisc_1(B))) ) ) ) )).

fof(t29_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => ( C = k1_xboole_0
           => k8_relset_2(A,B,C,D) = B ) ) ) )).

fof(t30_relset_2,axiom,(
    ! [A,B] :
      ( m1_subset_1(B,k1_zfmisc_1(k1_zfmisc_1(A)))
     => ( k5_setfam_1(A,B) = k1_xboole_0
      <=> ! [C] :
            ( r2_hidden(C,B)
           => C = k1_xboole_0 ) ) ) )).

fof(t31_relset_2,axiom,(
    ! [A,B] :
      ( ~ v1_xboole_0(B)
     => ! [C] :
          ( m2_relset_1(C,A,B)
         => ! [D] :
              ( m1_subset_1(D,k1_zfmisc_1(k1_zfmisc_1(A)))
             => ! [E] :
                  ( m1_subset_1(E,k1_zfmisc_1(k1_zfmisc_1(B)))
                 => ( E = a_4_2_relset_2(A,B,C,D)
                   => k8_relset_2(A,B,k5_setfam_1(A,D),C) = k8_setfam_1(B,E) ) ) ) ) ) )).

fof(t32_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(A))
         => ! [E] :
              ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(A,B)))
             => ( r1_tarski(C,D)
               => r1_tarski(k8_relset_2(A,B,D,E),k8_relset_2(A,B,C,E)) ) ) ) ) )).

fof(t33_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(A))
         => ! [E] :
              ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(A,B)))
             => r1_tarski(k4_subset_1(B,k8_relset_2(A,B,C,E),k8_relset_2(A,B,D,E)),k8_relset_2(A,B,k5_subset_1(A,C,D),E)) ) ) ) )).

fof(t34_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => ! [E] :
              ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(A,B)))
             => k8_relset_2(A,B,C,k5_subset_1(k2_zfmisc_1(A,B),D,E)) = k5_subset_1(B,k8_relset_2(A,B,C,D),k8_relset_2(A,B,C,E)) ) ) ) )).

fof(t35_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k1_zfmisc_1(k2_zfmisc_1(A,B))))
         => k9_relat_1(k5_setfam_1(k2_zfmisc_1(A,B),D),C) = k3_tarski(a_4_3_relset_2(A,B,C,D)) ) ) )).

fof(t36_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(k1_zfmisc_1(k2_zfmisc_1(A,B))))
     => ! [D,E,F] :
          ( m1_subset_1(F,k1_zfmisc_1(D))
         => m1_subset_1(a_6_0_relset_2(A,B,C,D,E,F),k1_zfmisc_1(k1_zfmisc_1(E))) ) ) )).

fof(t37_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => ( D = k1_xboole_0
           => ( C = k1_xboole_0
              | k8_relset_2(A,B,C,D) = k1_xboole_0 ) ) ) ) )).

fof(t38_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => ( D = k2_zfmisc_1(A,B)
           => k8_relset_2(A,B,C,D) = B ) ) ) )).

fof(t39_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(B))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k1_zfmisc_1(k2_zfmisc_1(B,A))))
         => ! [E] :
              ( m1_subset_1(E,k1_zfmisc_1(k1_zfmisc_1(A)))
             => ( E = a_4_4_relset_2(A,B,C,D)
               => k8_relset_2(B,A,C,k8_setfam_1(k2_zfmisc_1(B,A),D)) = k8_setfam_1(A,E) ) ) ) ) )).

fof(t40_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => ! [E] :
              ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(A,B)))
             => ( r1_tarski(D,E)
               => r1_tarski(k8_relset_2(A,B,C,D),k8_relset_2(A,B,C,E)) ) ) ) ) )).

fof(t41_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => ! [E] :
              ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(A,B)))
             => r1_tarski(k4_subset_1(B,k8_relset_2(A,B,C,D),k8_relset_2(A,B,C,E)),k8_relset_2(A,B,C,k4_subset_1(k2_zfmisc_1(A,B),D,E))) ) ) ) )).

fof(t42_relset_2,axiom,(
    ! [A,B,C,D,E] :
      ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(C,D)))
     => ( r2_hidden(A,k9_relat_1(k3_subset_1(k2_zfmisc_1(C,D),E),k1_tarski(B)))
      <=> ( ~ r2_hidden(k4_tarski(B,A),E)
          & r2_hidden(B,C)
          & r2_hidden(A,D) ) ) ) )).

fof(t43_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => ( C != k1_xboole_0
           => r1_tarski(k8_relset_2(A,B,C,D),k9_relat_1(D,C)) ) ) ) )).

fof(t44_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(A,B)))
     => ! [D,E] :
          ( ~ ( ~ r1_xboole_0(D,k9_relat_1(k4_relat_1(C),E))
              & ! [F,G] :
                  ~ ( r2_hidden(F,D)
                    & r2_hidden(G,E)
                    & r2_hidden(F,k9_relat_1(k4_relat_1(C),k1_tarski(G))) ) )
          & ~ ( ? [F,G] :
                  ( r2_hidden(F,D)
                  & r2_hidden(G,E)
                  & r2_hidden(F,k9_relat_1(k4_relat_1(C),k1_tarski(G))) )
              & r1_xboole_0(D,k9_relat_1(k4_relat_1(C),E)) ) ) ) )).

fof(t45_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(A,B)))
     => ! [D,E] :
          ( ~ ( ? [F,G] :
                  ( r2_hidden(F,D)
                  & r2_hidden(G,E)
                  & r2_hidden(F,k9_relat_1(k4_relat_1(C),k1_tarski(G))) )
              & r1_xboole_0(E,k9_relat_1(C,D)) )
          & ~ ( ~ r1_xboole_0(E,k9_relat_1(C,D))
              & ! [F,G] :
                  ~ ( r2_hidden(F,D)
                    & r2_hidden(G,E)
                    & r2_hidden(F,k9_relat_1(k4_relat_1(C),k1_tarski(G))) ) ) ) ) )).

fof(t46_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(B))
         => ! [E] :
              ( m1_subset_1(E,k1_zfmisc_1(k2_zfmisc_1(A,B)))
             => ( r1_xboole_0(C,k9_relat_1(k4_relat_1(E),D))
              <=> r1_xboole_0(D,k9_relat_1(E,C)) ) ) ) ) )).

fof(t47_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(A,B)))
     => ! [D] : k9_relat_1(C,D) = k9_relat_1(C,k3_xboole_0(D,k1_funct_5(C))) ) )).

fof(t48_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(k2_zfmisc_1(A,B)))
     => ! [D] : k9_relat_1(k4_relat_1(C),D) = k9_relat_1(k4_relat_1(C),k3_xboole_0(D,k2_funct_5(C))) ) )).

fof(t49_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m1_subset_1(D,k1_zfmisc_1(k2_zfmisc_1(A,B)))
         => k3_subset_1(B,k8_relset_2(A,B,C,D)) = k9_relat_1(k3_subset_1(k2_zfmisc_1(A,B),D),C) ) ) )).

fof(t50_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m2_relset_1(D,A,B)
         => k3_subset_1(B,k10_relset_1(A,B,D,C)) = k8_relset_2(A,B,C,k3_subset_1(k2_zfmisc_1(A,B),D)) ) ) )).

fof(t51_relset_2,axiom,(
    ! [A,B,C] :
      ( m2_relset_1(C,B,A)
     => ( k1_funct_5(C) = k10_relset_1(A,B,k6_relset_1(B,A,C),A)
        & k2_funct_5(C) = k10_relset_1(B,A,C,B) ) ) )).

fof(t52_relset_2,axiom,(
    ! [A,B,C,D] :
      ( m2_relset_1(D,A,B)
     => ! [E] :
          ( m2_relset_1(E,B,C)
         => ( k1_funct_5(k9_relset_2(A,B,C,D,E)) = k10_relset_1(B,A,k6_relset_1(A,B,D),k1_funct_5(E))
            & r1_tarski(k1_funct_5(k9_relset_2(A,B,C,D,E)),k1_funct_5(D)) ) ) ) )).

fof(t53_relset_2,axiom,(
    ! [A,B,C,D] :
      ( m2_relset_1(D,A,B)
     => ! [E] :
          ( m2_relset_1(E,B,C)
         => ( k2_funct_5(k9_relset_2(A,B,C,D,E)) = k10_relset_1(B,C,E,k2_funct_5(D))
            & r1_tarski(k2_funct_5(k9_relset_2(A,B,C,D,E)),k2_funct_5(E)) ) ) ) )).

fof(t54_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(A))
     => ! [D] :
          ( m2_relset_1(D,A,B)
         => ( r1_tarski(C,k1_funct_5(D))
          <=> r1_tarski(C,k10_relset_1(A,A,k9_relset_2(A,B,A,D,k6_relset_1(A,B,D)),C)) ) ) ) )).

fof(t55_relset_2,axiom,(
    ! [A,B,C] :
      ( m1_subset_1(C,k1_zfmisc_1(B))
     => ! [D] :
          ( m2_relset_1(D,A,B)
         => ( r1_tarski(C,k2_funct_5(D))
          <=> r1_tarski(C,k10_relset_1(B,B,k9_relset_2(B,A,B,k6_relset_1(A,B,D),D),C)) ) ) ) )).

fof(t56_relset_2,axiom,(
    ! [A,B,C] :
      ( m2_relset_1(C,B,A)
     => ( k1_funct_5(C) = k10_relset_1(A,B,k6_relset_1(B,A,C),A)
        & k10_relset_1(A,B,k6_relset_1(B,A,C),k10_relset_1(B,A,C,B)) = k10_relset_1(A,B,k6_relset_1(B,A,C),k2_funct_5(C)) ) ) )).

fof(t57_relset_2,axiom,(
    ! [A,B,C] :
      ( m2_relset_1(C,B,A)
     => k10_relset_1(A,B,k6_relset_1(B,A,C),A) = k10_relset_1(B,B,k9_relset_2(B,A,B,C,k6_relset_1(B,A,C)),B) ) )).

fof(t58_relset_2,axiom,(
    ! [A,B,C] :
      ( m2_relset_1(C,A,B)
     => k10_relset_1(A,B,C,A) = k10_relset_1(B,B,k9_relset_2(B,A,B,k6_relset_1(A,B,C),C),B) ) )).

fof(t59_relset_2,conjecture,(
    ! [A,B,C,D] :
      ( m1_subset_1(D,k1_zfmisc_1(A))
     => ! [E] :
          ( m2_relset_1(E,A,B)
         => ! [F] :
              ( m2_relset_1(F,B,C)
             => k8_relset_2(B,C,k10_relset_1(A,B,E,D),F) = k8_relset_2(A,C,D,k3_subset_1(k2_zfmisc_1(A,C),k9_relset_2(A,B,C,E,k3_subset_1(k2_zfmisc_1(B,C),F)))) ) ) ) )).
%------------------------------------------------------------------------------
