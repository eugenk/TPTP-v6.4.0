%------------------------------------------------------------------------------
% File     : SEU471^1 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Set Theory (Binary relations)
% Problem  : The transitive reflexive symmetric closure operator is idempotent
% Version  : [Nei08] axioms.
% English  :

% Refs     : [BN99]  Baader & Nipkow (1999), Term Rewriting and All That
%          : [Nei08] Neis (2008), Email to Geoff Sutcliffe
% Source   : [Nei08]
% Names    :

% Status   : CounterSatisfiable
% Rating   : 0.67 v6.4.0, 1.00 v3.7.0
% Syntax   : Number of formulae    :   59 (   0 unit;  29 type;  29 defn)
%            Number of atoms       :  251 (  33 equality; 158 variable)
%            Maximal formula depth :   12 (   7 average)
%            Number of connectives :  159 (   4   ~;   4   |;  12   &; 123   @)
%                                         (   0 <=>;  16  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  197 ( 197   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   31 (  29   :;   0   =)
%            Number of variables   :   86 (   0 sgn;  38   !;   5   ?;  43   ^)
%                                         (  86   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU_NAR

% Comments : Some proofs can be found in chapter 2 of [BN99]
%------------------------------------------------------------------------------
%----Include axioms of binary relations
include('Axioms/SET009^0.ax').
%------------------------------------------------------------------------------
thf(transitive_reflexive_symmetric_closure_op_is_idempotent,conjecture,
    ( idem @ trsc )).

%------------------------------------------------------------------------------
