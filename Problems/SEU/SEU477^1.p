%------------------------------------------------------------------------------
% File     : SEU477^1 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Set Theory (Binary relations)
% Problem  : Another definition of terminating
% Version  : [Nei08] axioms.
% English  : The definition of terminating is the same as saying there is no
%            non-empty subset A in which every element has an R successor
%            (in A).

% Refs     : [BN99]  Baader & Nipkow (1999), Term Rewriting and All That
%          : [Nei08] Neis (2008), Email to Geoff Sutcliffe
% Source   : [Nei08]
% Names    :

% Status   : Theorem
% Rating   : 0.29 v6.4.0, 0.33 v6.3.0, 0.40 v6.2.0, 0.14 v6.1.0, 0.29 v5.5.0, 0.33 v5.4.0, 0.60 v5.3.0, 0.80 v5.2.0, 0.60 v4.1.0, 0.67 v3.7.0
% Syntax   : Number of formulae    :   59 (   0 unit;  29 type;  29 defn)
%            Number of atoms       :  260 (  34 equality; 167 variable)
%            Maximal formula depth :   12 (   7 average)
%            Number of connectives :  167 (   5   ~;   4   |;  14   &; 127   @)
%                                         (   0 <=>;  17  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  200 ( 200   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   31 (  29   :;   0   =)
%            Number of variables   :   91 (   0 sgn;  39   !;   8   ?;  44   ^)
%                                         (  91   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : Some proofs can be found in chapter 2 of [BN99]
%          : 
%------------------------------------------------------------------------------
%----Include axioms of binary relations
include('Axioms/SET009^0.ax').
%------------------------------------------------------------------------------
thf(alternative_formulation_of_terminating,conjecture,
    ( term
    = ( ^ [R: $i > $i > $o] :
          ~ ( ? [A: $i > $o] :
                ( ? [X: $i] :
                    ( A @ X )
                & ! [X: $i] :
                    ( ( A @ X )
                   => ? [Y: $i] :
                        ( ( A @ Y )
                        & ( R @ X @ Y ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
