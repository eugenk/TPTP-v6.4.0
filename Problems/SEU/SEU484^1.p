%------------------------------------------------------------------------------
% File     : SEU484^1 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Set Theory (Binary relations)
% Problem  : A reflexive relation is non-terminating
% Version  : [Nei08] axioms.
% English  :

% Refs     : [BN99]  Baader & Nipkow (1999), Term Rewriting and All That
%          : [Nei08] Neis (2008), Email to Geoff Sutcliffe
% Source   : [Nei08]
% Names    :

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.00 v6.1.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v4.0.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :   59 (   0 unit;  29 type;  29 defn)
%            Number of atoms       :  253 (  33 equality; 160 variable)
%            Maximal formula depth :   12 (   7 average)
%            Number of connectives :  162 (   5   ~;   4   |;  12   &; 124   @)
%                                         (   0 <=>;  17  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  199 ( 199   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   31 (  29   :;   0   =)
%            Number of variables   :   87 (   0 sgn;  39   !;   5   ?;  43   ^)
%                                         (  87   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : Some proofs can be found in chapter 2 of [BN99]
%          : 
%------------------------------------------------------------------------------
%----Include axioms of binary relations
include('Axioms/SET009^0.ax').
%------------------------------------------------------------------------------
thf(reflexive_implies_non_terminating,conjecture,(
    ! [R: $i > $i > $o] :
      ( ( refl @ R )
     => ~ ( term @ R ) ) )).

%------------------------------------------------------------------------------
