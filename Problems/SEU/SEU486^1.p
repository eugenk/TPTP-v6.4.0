%------------------------------------------------------------------------------
% File     : SEU486^1 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Set Theory (Binary relations)
% Problem  : Confluence implies local confluence
% Version  : [Nei08] axioms.
% English  :

% Refs     : [BN99]  Baader & Nipkow (1999), Term Rewriting and All That
%          : [Nei08] Neis (2008), Email to Geoff Sutcliffe
% Source   : [Nei08]
% Names    :

% Status   : Theorem
% Rating   : 0.29 v6.4.0, 0.33 v6.3.0, 0.40 v6.2.0, 0.57 v6.1.0, 0.71 v6.0.0, 0.57 v5.5.0, 0.50 v5.4.0, 0.40 v5.3.0, 0.60 v4.1.0, 0.67 v4.0.0, 1.00 v3.7.0
% Syntax   : Number of formulae    :   59 (   0 unit;  29 type;  29 defn)
%            Number of atoms       :  253 (  33 equality; 160 variable)
%            Maximal formula depth :   12 (   7 average)
%            Number of connectives :  161 (   4   ~;   4   |;  12   &; 124   @)
%                                         (   0 <=>;  17  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  199 ( 199   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   31 (  29   :;   0   =)
%            Number of variables   :   87 (   0 sgn;  39   !;   5   ?;  43   ^)
%                                         (  87   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : Some proofs can be found in chapter 2 of [BN99]
%          : 
%------------------------------------------------------------------------------
%----Include axioms of binary relations
include('Axioms/SET009^0.ax').
%------------------------------------------------------------------------------
thf(confluence_implies_local_confluence,conjecture,(
    ! [R: $i > $i > $o] :
      ( ( confl @ R )
     => ( lconfl @ R ) ) )).

%------------------------------------------------------------------------------
