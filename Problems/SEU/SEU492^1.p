%------------------------------------------------------------------------------
% File     : SEU492^1 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Set Theory (Binary relations)
% Problem  : Alternative definition of a strict (partial) order: requiring
% Version  : [Nei08] axioms.
% English  : Alternative definition of a strict (partial) order: requiring
%            irreflexibility instead of asymmetry.

% Refs     : [BN99]  Baader & Nipkow (1999), Term Rewriting and All That
%          : [Nei08] Neis (2008), Email to Geoff Sutcliffe
% Source   : [Nei08]
% Names    :

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.00 v6.1.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.40 v5.3.0, 0.60 v5.2.0, 0.40 v5.1.0, 0.60 v5.0.0, 0.20 v4.1.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   59 (   0 unit;  29 type;  29 defn)
%            Number of atoms       :  255 (  34 equality; 160 variable)
%            Maximal formula depth :   12 (   7 average)
%            Number of connectives :  161 (   4   ~;   4   |;  13   &; 124   @)
%                                         (   0 <=>;  16  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  199 ( 199   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   31 (  29   :;   0   =)
%            Number of variables   :   87 (   0 sgn;  38   !;   5   ?;  44   ^)
%                                         (  87   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : Some proofs can be found in chapter 2 of [BN99]
%          : 
%------------------------------------------------------------------------------
%----Include axioms of binary relations
include('Axioms/SET009^0.ax').
%------------------------------------------------------------------------------
thf(alternative_definition_of_strict_order,conjecture,
    ( so
    = ( ^ [R: $i > $i > $o] :
          ( ( irrefl @ R )
          & ( trans @ R ) ) ) )).

%------------------------------------------------------------------------------
