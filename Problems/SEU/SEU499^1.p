%------------------------------------------------------------------------------
% File     : SEU499^1 : TPTP v6.4.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Foundation - Axioms - Logical Axioms
% Version  : Especial.
% English  : (!phi:i>o.exu(^x:i.phi x)->(?x:i.phi x & (!y:i.phi y -> x = y)))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC001g [Bro08]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.14 v6.0.0, 0.29 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.1.0, 0.60 v5.0.0, 0.40 v4.1.0, 0.33 v4.0.1, 0.67 v4.0.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :   41 (   0 unit;  24 type;  16 defn)
%            Number of atoms       :  279 (  21 equality; 150 variable)
%            Maximal formula depth :   23 (   6 average)
%            Number of connectives :  223 (   3   ~;   3   |;  18   &; 149   @)
%                                         (   6 <=>;  44  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   21 (  21   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   26 (  24   :;   0   =)
%            Number of variables   :   69 (   0 sgn;  48   !;  13   ?;   8   ^)
%                                         (  69   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : http://mathgate.info/detsetitem.php?id=404
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(exu_type,type,(
    exu: ( $i > $o ) > $o )).

thf(exu,definition,
    ( exu
    = ( ^ [Xphi: $i > $o] :
        ? [Xx: $i] :
          ( ( Xphi @ Xx )
          & ! [Xy: $i] :
              ( ( Xphi @ Xy )
             => ( Xx = Xy ) ) ) ) )).

thf(setextAx_type,type,(
    setextAx: $o )).

thf(setextAx,definition,
    ( setextAx
    = ( ! [A: $i,B: $i] :
          ( ! [Xx: $i] :
              ( ( in @ Xx @ A )
            <=> ( in @ Xx @ B ) )
         => ( A = B ) ) ) )).

thf(emptyset_type,type,(
    emptyset: $i )).

thf(emptysetAx_type,type,(
    emptysetAx: $o )).

thf(emptysetAx,definition,
    ( emptysetAx
    = ( ! [Xx: $i] :
          ~ ( in @ Xx @ emptyset ) ) )).

thf(setadjoin_type,type,(
    setadjoin: $i > $i > $i )).

thf(setadjoinAx_type,type,(
    setadjoinAx: $o )).

thf(setadjoinAx,definition,
    ( setadjoinAx
    = ( ! [Xx: $i,A: $i,Xy: $i] :
          ( ( in @ Xy @ ( setadjoin @ Xx @ A ) )
        <=> ( ( Xy = Xx )
            | ( in @ Xy @ A ) ) ) ) )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(powersetAx_type,type,(
    powersetAx: $o )).

thf(powersetAx,definition,
    ( powersetAx
    = ( ! [A: $i,B: $i] :
          ( ( in @ B @ ( powerset @ A ) )
        <=> ! [Xx: $i] :
              ( ( in @ Xx @ B )
             => ( in @ Xx @ A ) ) ) ) )).

thf(setunion_type,type,(
    setunion: $i > $i )).

thf(setunionAx_type,type,(
    setunionAx: $o )).

thf(setunionAx,definition,
    ( setunionAx
    = ( ! [A: $i,Xx: $i] :
          ( ( in @ Xx @ ( setunion @ A ) )
        <=> ? [B: $i] :
              ( ( in @ Xx @ B )
              & ( in @ B @ A ) ) ) ) )).

thf(omega_type,type,(
    omega: $i )).

thf(omega0Ax_type,type,(
    omega0Ax: $o )).

thf(omega0Ax,definition,
    ( omega0Ax
    = ( in @ emptyset @ omega ) )).

thf(omegaSAx_type,type,(
    omegaSAx: $o )).

thf(omegaSAx,definition,
    ( omegaSAx
    = ( ! [Xx: $i] :
          ( ( in @ Xx @ omega )
         => ( in @ ( setadjoin @ Xx @ Xx ) @ omega ) ) ) )).

thf(omegaIndAx_type,type,(
    omegaIndAx: $o )).

thf(omegaIndAx,definition,
    ( omegaIndAx
    = ( ! [A: $i] :
          ( ( ( in @ emptyset @ A )
            & ! [Xx: $i] :
                ( ( ( in @ Xx @ omega )
                  & ( in @ Xx @ A ) )
               => ( in @ ( setadjoin @ Xx @ Xx ) @ A ) ) )
         => ! [Xx: $i] :
              ( ( in @ Xx @ omega )
             => ( in @ Xx @ A ) ) ) ) )).

thf(replAx_type,type,(
    replAx: $o )).

thf(replAx,definition,
    ( replAx
    = ( ! [Xphi: $i > $i > $o,A: $i] :
          ( ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ( exu
                @ ^ [Xy: $i] :
                    ( Xphi @ Xx @ Xy ) ) )
         => ? [B: $i] :
            ! [Xx: $i] :
              ( ( in @ Xx @ B )
            <=> ? [Xy: $i] :
                  ( ( in @ Xy @ A )
                  & ( Xphi @ Xy @ Xx ) ) ) ) ) )).

thf(foundationAx_type,type,(
    foundationAx: $o )).

thf(foundationAx,definition,
    ( foundationAx
    = ( ! [A: $i] :
          ( ? [Xx: $i] :
              ( in @ Xx @ A )
         => ? [B: $i] :
              ( ( in @ B @ A )
              & ~ ( ? [Xx: $i] :
                      ( ( in @ Xx @ B )
                      & ( in @ Xx @ A ) ) ) ) ) ) )).

thf(wellorderingAx_type,type,(
    wellorderingAx: $o )).

thf(wellorderingAx,definition,
    ( wellorderingAx
    = ( ! [A: $i] :
        ? [B: $i] :
          ( ! [C: $i] :
              ( ( in @ C @ B )
             => ! [Xx: $i] :
                  ( ( in @ Xx @ C )
                 => ( in @ Xx @ A ) ) )
          & ! [Xx: $i,Xy: $i] :
              ( ( ( in @ Xx @ A )
                & ( in @ Xy @ A ) )
             => ( ! [C: $i] :
                    ( ( in @ C @ B )
                   => ( ( in @ Xx @ C )
                    <=> ( in @ Xy @ C ) ) )
               => ( Xx = Xy ) ) )
          & ! [C: $i,D: $i] :
              ( ( ( in @ C @ B )
                & ( in @ D @ B ) )
             => ( ! [Xx: $i] :
                    ( ( in @ Xx @ C )
                   => ( in @ Xx @ D ) )
                | ! [Xx: $i] :
                    ( ( in @ Xx @ D )
                   => ( in @ Xx @ C ) ) ) )
          & ! [C: $i] :
              ( ( ! [Xx: $i] :
                    ( ( in @ Xx @ C )
                   => ( in @ Xx @ A ) )
                & ? [Xx: $i] :
                    ( in @ Xx @ C ) )
             => ? [D: $i,Xx: $i] :
                  ( ( in @ D @ B )
                  & ( in @ Xx @ C )
                  & ~ ( ? [Xy: $i] :
                          ( ( in @ Xy @ D )
                          & ( in @ Xy @ C ) ) )
                  & ! [E: $i] :
                      ( ( in @ E @ B )
                     => ( ! [Xy: $i] :
                            ( ( in @ Xy @ E )
                           => ( in @ Xy @ D ) )
                        | ( in @ Xx @ E ) ) ) ) ) ) ) )).

thf(descr_type,type,(
    descr: ( $i > $o ) > $i )).

thf(descrp_type,type,(
    descrp: $o )).

thf(descrp,definition,
    ( descrp
    = ( ! [Xphi: $i > $o] :
          ( ( exu
            @ ^ [Xx: $i] :
                ( Xphi @ Xx ) )
         => ( Xphi
            @ ( descr
              @ ^ [Xx: $i] :
                  ( Xphi @ Xx ) ) ) ) ) )).

thf(dsetconstr_type,type,(
    dsetconstr: $i > ( $i > $o ) > $i )).

thf(dsetconstrI_type,type,(
    dsetconstrI: $o )).

thf(dsetconstrI,definition,
    ( dsetconstrI
    = ( ! [A: $i,Xphi: $i > $o,Xx: $i] :
          ( ( in @ Xx @ A )
         => ( ( Xphi @ Xx )
           => ( in @ Xx
              @ ( dsetconstr @ A
                @ ^ [Xy: $i] :
                    ( Xphi @ Xy ) ) ) ) ) ) )).

thf(dsetconstrEL_type,type,(
    dsetconstrEL: $o )).

thf(dsetconstrEL,definition,
    ( dsetconstrEL
    = ( ! [A: $i,Xphi: $i > $o,Xx: $i] :
          ( ( in @ Xx
            @ ( dsetconstr @ A
              @ ^ [Xy: $i] :
                  ( Xphi @ Xy ) ) )
         => ( in @ Xx @ A ) ) ) )).

thf(dsetconstrER_type,type,(
    dsetconstrER: $o )).

thf(dsetconstrER,definition,
    ( dsetconstrER
    = ( ! [A: $i,Xphi: $i > $o,Xx: $i] :
          ( ( in @ Xx
            @ ( dsetconstr @ A
              @ ^ [Xy: $i] :
                  ( Xphi @ Xy ) ) )
         => ( Xphi @ Xx ) ) ) )).

thf(exuE1,conjecture,
    ( setextAx
   => ( emptysetAx
     => ( setadjoinAx
       => ( powersetAx
         => ( setunionAx
           => ( omega0Ax
             => ( omegaSAx
               => ( omegaIndAx
                 => ( replAx
                   => ( foundationAx
                     => ( wellorderingAx
                       => ( descrp
                         => ( dsetconstrI
                           => ( dsetconstrEL
                             => ( dsetconstrER
                               => ! [Xphi: $i > $o] :
                                    ( ( exu
                                      @ ^ [Xx: $i] :
                                          ( Xphi @ Xx ) )
                                   => ? [Xx: $i] :
                                        ( ( Xphi @ Xx )
                                        & ! [Xy: $i] :
                                            ( ( Xphi @ Xy )
                                           => ( Xx = Xy ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
