%------------------------------------------------------------------------------
% File     : SEU538^2 : TPTP v6.4.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Preliminary Notions - Bounded Quantifier Laws
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! phi:i>o.~(? x:i.in x A & phi x) -> (! x:i.in x A ->
%            ~(phi x)))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC040l [Bro08]
%          : ZFC044l [Bro08]

% Status   : Theorem
% Rating   : 0.00 v5.3.0, 0.25 v5.2.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type;   0 defn)
%            Number of atoms       :   10 (   0 equality;   8 variable)
%            Maximal formula depth :    9 (   6 average)
%            Number of connectives :   11 (   2   ~;   0   |;   1   &;   6   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :;   0   =)
%            Number of variables   :    4 (   0 sgn;   3   !;   1   ?;   0   ^)
%                                         (   4   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : http://mathgate.info/detsetitem.php?id=451
%          : http://mathgate.info/detsetitem.php?id=118
%          : 
%------------------------------------------------------------------------------
thf(in,type,(
    in: $i > $i > $o )).

thf(quantDeMorgan3,conjecture,(
    ! [A: $i,Xphi: $i > $o] :
      ( ~ ( ? [Xx: $i] :
              ( ( in @ Xx @ A )
              & ( Xphi @ Xx ) ) )
     => ! [Xx: $i] :
          ( ( in @ Xx @ A )
         => ~ ( Xphi @ Xx ) ) ) )).

%------------------------------------------------------------------------------
