%------------------------------------------------------------------------------
% File     : SEU571^2 : TPTP v6.4.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Preliminary Notions - Relations on Sets - Subsets
% Version  : Especial > Reduced > Especial.
% English  : (! x:i.! A:i.subset A (setadjoin x A))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC073l [Bro08]

% Status   : Theorem
% Rating   : 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :    8 (   0 unit;   5 type;   2 defn)
%            Number of atoms       :   28 (   2 equality;  14 variable)
%            Maximal formula depth :    9 (   5 average)
%            Number of connectives :   21 (   0   ~;   0   |;   0   &;  16   @)
%                                         (   0 <=>;   5  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    6 (   6   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    7 (   5   :;   0   =)
%            Number of variables   :    8 (   0 sgn;   8   !;   0   ?;   0   ^)
%                                         (   8   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : http://mathgate.info/detsetitem.php?id=458
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(setadjoin_type,type,(
    setadjoin: $i > $i > $i )).

thf(setadjoinIR_type,type,(
    setadjoinIR: $o )).

thf(setadjoinIR,definition,
    ( setadjoinIR
    = ( ! [Xx: $i,A: $i,Xy: $i] :
          ( ( in @ Xy @ A )
         => ( in @ Xy @ ( setadjoin @ Xx @ A ) ) ) ) )).

thf(subset_type,type,(
    subset: $i > $i > $o )).

thf(subsetI1_type,type,(
    subsetI1: $o )).

thf(subsetI1,definition,
    ( subsetI1
    = ( ! [A: $i,B: $i] :
          ( ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ( in @ Xx @ B ) )
         => ( subset @ A @ B ) ) ) )).

thf(setadjoinSub,conjecture,
    ( setadjoinIR
   => ( subsetI1
     => ! [Xx: $i,A: $i] :
          ( subset @ A @ ( setadjoin @ Xx @ A ) ) ) )).

%------------------------------------------------------------------------------
