%------------------------------------------------------------------------------
% File     : SEU648^2 : TPTP v6.4.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Ordered Pairs - Properties of Pairs
% Version  : Especial > Reduced > Especial.
% English  : (! x:i.! y:i.! z:i.! u:i.kpair x y = kpair z u -> x = z)

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC150l [Bro08]

% Status   : Theorem
% Rating   : 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v4.0.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :    7 (   0 unit;   4 type;   2 defn)
%            Number of atoms       :   52 (   6 equality;  17 variable)
%            Maximal formula depth :   14 (   7 average)
%            Number of connectives :   37 (   0   ~;   0   |;   0   &;  34   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    6 (   4   :;   0   =)
%            Number of variables   :   10 (   0 sgn;   8   !;   0   ?;   2   ^)
%                                         (  10   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : http://mathgate.info/detsetitem.php?id=233
%          : 
%------------------------------------------------------------------------------
thf(emptyset_type,type,(
    emptyset: $i )).

thf(setadjoin_type,type,(
    setadjoin: $i > $i > $i )).

thf(kpair_type,type,(
    kpair: $i > $i > $i )).

thf(kpair,definition,
    ( kpair
    = ( ^ [Xx: $i,Xy: $i] :
          ( setadjoin @ ( setadjoin @ Xx @ emptyset ) @ ( setadjoin @ ( setadjoin @ Xx @ ( setadjoin @ Xy @ emptyset ) ) @ emptyset ) ) ) )).

thf(setukpairinjL2_type,type,(
    setukpairinjL2: $o )).

thf(setukpairinjL2,definition,
    ( setukpairinjL2
    = ( ! [Xx: $i,Xy: $i,Xz: $i,Xu: $i] :
          ( ( ( setadjoin @ ( setadjoin @ Xx @ emptyset ) @ ( setadjoin @ ( setadjoin @ Xx @ ( setadjoin @ Xy @ emptyset ) ) @ emptyset ) )
            = ( setadjoin @ ( setadjoin @ Xz @ emptyset ) @ ( setadjoin @ ( setadjoin @ Xz @ ( setadjoin @ Xu @ emptyset ) ) @ emptyset ) ) )
         => ( Xx = Xz ) ) ) )).

thf(setukpairinjL,conjecture,
    ( setukpairinjL2
   => ! [Xx: $i,Xy: $i,Xz: $i,Xu: $i] :
        ( ( ( kpair @ Xx @ Xy )
          = ( kpair @ Xz @ Xu ) )
       => ( Xx = Xz ) ) )).

%------------------------------------------------------------------------------
