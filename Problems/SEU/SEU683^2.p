%------------------------------------------------------------------------------
% File     : SEU683^2 : TPTP v6.4.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Functions - Extensionality and Beta Reduction
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! phi:i>o.ex1 A (^ x:i.phi x) -> (! x:i.in x A ->
%            (! y:i.in y A -> phi x -> phi y -> x = y)))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC185l [Bro08]

% Status   : Theorem
% Rating   : 0.29 v6.4.0, 0.33 v6.3.0, 0.40 v6.2.0, 0.14 v6.1.0, 0.29 v5.5.0, 0.33 v5.4.0, 0.60 v5.3.0, 0.80 v5.2.0, 0.60 v4.1.0, 0.67 v3.7.0
% Syntax   : Number of formulae    :   13 (   0 unit;   8 type;   4 defn)
%            Number of atoms       :   59 (   7 equality;  32 variable)
%            Maximal formula depth :   13 (   6 average)
%            Number of connectives :   40 (   0   ~;   0   |;   1   &;  29   @)
%                                         (   0 <=>;  10  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   14 (  14   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   10 (   8   :;   0   =)
%            Number of variables   :   16 (   0 sgn;   9   !;   1   ?;   6   ^)
%                                         (  16   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : http://mathgate.info/detsetitem.php?id=240
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(emptyset_type,type,(
    emptyset: $i )).

thf(setadjoin_type,type,(
    setadjoin: $i > $i > $i )).

thf(dsetconstr_type,type,(
    dsetconstr: $i > ( $i > $o ) > $i )).

thf(dsetconstrI_type,type,(
    dsetconstrI: $o )).

thf(dsetconstrI,definition,
    ( dsetconstrI
    = ( ! [A: $i,Xphi: $i > $o,Xx: $i] :
          ( ( in @ Xx @ A )
         => ( ( Xphi @ Xx )
           => ( in @ Xx
              @ ( dsetconstr @ A
                @ ^ [Xy: $i] :
                    ( Xphi @ Xy ) ) ) ) ) ) )).

thf(uniqinunit_type,type,(
    uniqinunit: $o )).

thf(uniqinunit,definition,
    ( uniqinunit
    = ( ! [Xx: $i,Xy: $i] :
          ( ( in @ Xx @ ( setadjoin @ Xy @ emptyset ) )
         => ( Xx = Xy ) ) ) )).

thf(singleton_type,type,(
    singleton: $i > $o )).

thf(singleton,definition,
    ( singleton
    = ( ^ [A: $i] :
        ? [Xx: $i] :
          ( ( in @ Xx @ A )
          & ( A
            = ( setadjoin @ Xx @ emptyset ) ) ) ) )).

thf(ex1_type,type,(
    ex1: $i > ( $i > $o ) > $o )).

thf(ex1,definition,
    ( ex1
    = ( ^ [A: $i,Xphi: $i > $o] :
          ( singleton
          @ ( dsetconstr @ A
            @ ^ [Xx: $i] :
                ( Xphi @ Xx ) ) ) ) )).

thf(ex1E2,conjecture,
    ( dsetconstrI
   => ( uniqinunit
     => ! [A: $i,Xphi: $i > $o] :
          ( ( ex1 @ A
            @ ^ [Xx: $i] :
                ( Xphi @ Xx ) )
         => ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ! [Xy: $i] :
                  ( ( in @ Xy @ A )
                 => ( ( Xphi @ Xx )
                   => ( ( Xphi @ Xy )
                     => ( Xx = Xy ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
