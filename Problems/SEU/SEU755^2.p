%------------------------------------------------------------------------------
% File     : SEU755^2 : TPTP v6.4.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Typed Set Theory - Laws for Typed Sets - DeMorgan Laws
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! X:i.in X (powerset A) -> (! Y:i.in Y (powerset A) ->
%            (! x:i.in x A -> in x (setminus A X) -> in x (setminus A Y) ->
%            in x (setminus A (binunion X Y)))))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC257l [Bro08]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.14 v6.1.0, 0.29 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   11 (   0 unit;   7 type;   3 defn)
%            Number of atoms       :   78 (   3 equality;  41 variable)
%            Maximal formula depth :   17 (   7 average)
%            Number of connectives :   73 (   5   ~;   0   |;   0   &;  52   @)
%                                         (   0 <=>;  16  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    7 (   7   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    9 (   7   :;   0   =)
%            Number of variables   :   14 (   0 sgn;  14   !;   0   ?;   0   ^)
%                                         (  14   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : http://mathgate.info/detsetitem.php?id=316
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(binunion_type,type,(
    binunion: $i > $i > $i )).

thf(setminus_type,type,(
    setminus: $i > $i > $i )).

thf(setminusI_type,type,(
    setminusI: $o )).

thf(setminusI,definition,
    ( setminusI
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( in @ Xx @ A )
         => ( ~ ( in @ Xx @ B )
           => ( in @ Xx @ ( setminus @ A @ B ) ) ) ) ) )).

thf(setminusER_type,type,(
    setminusER: $o )).

thf(setminusER,definition,
    ( setminusER
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( in @ Xx @ ( setminus @ A @ B ) )
         => ~ ( in @ Xx @ B ) ) ) )).

thf(binunionTEcontra_type,type,(
    binunionTEcontra: $o )).

thf(binunionTEcontra,definition,
    ( binunionTEcontra
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ! [Xx: $i] :
                  ( ( in @ Xx @ A )
                 => ( ~ ( in @ Xx @ X )
                   => ( ~ ( in @ Xx @ Y )
                     => ~ ( in @ Xx @ ( binunion @ X @ Y ) ) ) ) ) ) ) ) )).

thf(demorgan2b2,conjecture,
    ( setminusI
   => ( setminusER
     => ( binunionTEcontra
       => ! [A: $i,X: $i] :
            ( ( in @ X @ ( powerset @ A ) )
           => ! [Y: $i] :
                ( ( in @ Y @ ( powerset @ A ) )
               => ! [Xx: $i] :
                    ( ( in @ Xx @ A )
                   => ( ( in @ Xx @ ( setminus @ A @ X ) )
                     => ( ( in @ Xx @ ( setminus @ A @ Y ) )
                       => ( in @ Xx @ ( setminus @ A @ ( binunion @ X @ Y ) ) ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
