%------------------------------------------------------------------------------
% File     : SEU788^2 : TPTP v6.4.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Binary Relations on a Set
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! R:i.breln1 A R -> (! S:i.breln1 A S ->
%            binunion R S = binunion S R))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC290l [Bro08]

% Status   : Theorem
% Rating   : 0.43 v6.4.0, 0.50 v6.3.0, 0.60 v6.2.0, 0.43 v6.1.0, 0.57 v5.5.0, 0.67 v5.4.0, 0.60 v5.2.0, 0.80 v4.1.0, 1.00 v3.7.0
% Syntax   : Number of formulae    :   18 (   0 unit;  11 type;   6 defn)
%            Number of atoms       :  153 (   8 equality;  85 variable)
%            Maximal formula depth :   17 (   7 average)
%            Number of connectives :  130 (   0   ~;   1   |;   0   &;  96   @)
%                                         (   0 <=>;  33  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   10 (  10   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   13 (  11   :;   0   =)
%            Number of variables   :   28 (   0 sgn;  28   !;   0   ?;   0   ^)
%                                         (  28   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : http://mathgate.info/detsetitem.php?id=353
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(subset_type,type,(
    subset: $i > $i > $o )).

thf(setextsub_type,type,(
    setextsub: $o )).

thf(setextsub,definition,
    ( setextsub
    = ( ! [A: $i,B: $i] :
          ( ( subset @ A @ B )
         => ( ( subset @ B @ A )
           => ( A = B ) ) ) ) )).

thf(binunion_type,type,(
    binunion: $i > $i > $i )).

thf(kpair_type,type,(
    kpair: $i > $i > $i )).

thf(breln1_type,type,(
    breln1: $i > $i > $o )).

thf(subbreln1_type,type,(
    subbreln1: $o )).

thf(subbreln1,definition,
    ( subbreln1
    = ( ! [A: $i,R: $i] :
          ( ( breln1 @ A @ R )
         => ! [S: $i] :
              ( ( breln1 @ A @ S )
             => ( ! [Xx: $i] :
                    ( ( in @ Xx @ A )
                   => ! [Xy: $i] :
                        ( ( in @ Xy @ A )
                       => ( ( in @ ( kpair @ Xx @ Xy ) @ R )
                         => ( in @ ( kpair @ Xx @ Xy ) @ S ) ) ) )
               => ( subset @ R @ S ) ) ) ) ) )).

thf(breln1unionprop_type,type,(
    breln1unionprop: $o )).

thf(breln1unionprop,definition,
    ( breln1unionprop
    = ( ! [A: $i,R: $i] :
          ( ( breln1 @ A @ R )
         => ! [S: $i] :
              ( ( breln1 @ A @ S )
             => ( breln1 @ A @ ( binunion @ R @ S ) ) ) ) ) )).

thf(breln1unionIL_type,type,(
    breln1unionIL: $o )).

thf(breln1unionIL,definition,
    ( breln1unionIL
    = ( ! [A: $i,R: $i] :
          ( ( breln1 @ A @ R )
         => ! [S: $i] :
              ( ( breln1 @ A @ S )
             => ! [Xx: $i] :
                  ( ( in @ Xx @ A )
                 => ! [Xy: $i] :
                      ( ( in @ Xy @ A )
                     => ( ( in @ ( kpair @ Xx @ Xy ) @ R )
                       => ( in @ ( kpair @ Xx @ Xy ) @ ( binunion @ R @ S ) ) ) ) ) ) ) ) )).

thf(breln1unionIR_type,type,(
    breln1unionIR: $o )).

thf(breln1unionIR,definition,
    ( breln1unionIR
    = ( ! [A: $i,R: $i] :
          ( ( breln1 @ A @ R )
         => ! [S: $i] :
              ( ( breln1 @ A @ S )
             => ! [Xx: $i] :
                  ( ( in @ Xx @ A )
                 => ! [Xy: $i] :
                      ( ( in @ Xy @ A )
                     => ( ( in @ ( kpair @ Xx @ Xy ) @ S )
                       => ( in @ ( kpair @ Xx @ Xy ) @ ( binunion @ R @ S ) ) ) ) ) ) ) ) )).

thf(breln1unionE_type,type,(
    breln1unionE: $o )).

thf(breln1unionE,definition,
    ( breln1unionE
    = ( ! [A: $i,R: $i] :
          ( ( breln1 @ A @ R )
         => ! [S: $i] :
              ( ( breln1 @ A @ S )
             => ! [Xx: $i] :
                  ( ( in @ Xx @ A )
                 => ! [Xy: $i] :
                      ( ( in @ Xy @ A )
                     => ( ( in @ ( kpair @ Xx @ Xy ) @ ( binunion @ R @ S ) )
                       => ( ( in @ ( kpair @ Xx @ Xy ) @ R )
                          | ( in @ ( kpair @ Xx @ Xy ) @ S ) ) ) ) ) ) ) ) )).

thf(breln1unionCommutes,conjecture,
    ( setextsub
   => ( subbreln1
     => ( breln1unionprop
       => ( breln1unionIL
         => ( breln1unionIR
           => ( breln1unionE
             => ! [A: $i,R: $i] :
                  ( ( breln1 @ A @ R )
                 => ! [S: $i] :
                      ( ( breln1 @ A @ S )
                     => ( ( binunion @ R @ S )
                        = ( binunion @ S @ R ) ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
