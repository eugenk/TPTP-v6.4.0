%------------------------------------------------------------------------------
% File     : SEU818^2 : TPTP v6.4.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Ordinals
% Version  : Especial > Reduced > Especial.
% English  : (! X:i.ordinal X -> (! A:i.in A X -> subset A X))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC320l [Bro08]

% Status   : Theorem
% Rating   : 0.00 v6.0.0, 0.29 v5.5.0, 0.33 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v4.0.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :   19 (   0 unit;  11 type;   7 defn)
%            Number of atoms       :  116 (  10 equality;  63 variable)
%            Maximal formula depth :   14 (   6 average)
%            Number of connectives :   90 (   2   ~;   3   |;   6   &;  59   @)
%                                         (   0 <=>;  20  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   10 (  10   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   13 (  11   :;   0   =)
%            Number of variables   :   23 (   0 sgn;  17   !;   1   ?;   5   ^)
%                                         (  23   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : http://mathgate.info/detsetitem.php?id=514
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(emptyset_type,type,(
    emptyset: $i )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(nonempty_type,type,(
    nonempty: $i > $o )).

thf(nonempty,definition,
    ( nonempty
    = ( ^ [Xx: $i] : ( Xx != emptyset ) ) )).

thf(subset_type,type,(
    subset: $i > $i > $o )).

thf(subsetI1_type,type,(
    subsetI1: $o )).

thf(subsetI1,definition,
    ( subsetI1
    = ( ! [A: $i,B: $i] :
          ( ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ( in @ Xx @ B ) )
         => ( subset @ A @ B ) ) ) )).

thf(transitiveset_type,type,(
    transitiveset: $i > $o )).

thf(transitiveset,definition,
    ( transitiveset
    = ( ^ [A: $i] :
        ! [X: $i] :
          ( ( in @ X @ A )
         => ( subset @ X @ A ) ) ) )).

thf(stricttotalorderedByIn_type,type,(
    stricttotalorderedByIn: $i > $o )).

thf(stricttotalorderedByIn,definition,
    ( stricttotalorderedByIn
    = ( ^ [A: $i] :
          ( ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ! [X: $i] :
                  ( ( in @ X @ A )
                 => ! [Y: $i] :
                      ( ( in @ Y @ A )
                     => ( ( ( in @ Xx @ X )
                          & ( in @ X @ Y ) )
                       => ( in @ Xx @ Y ) ) ) ) )
          & ! [X: $i] :
              ( ( in @ X @ A )
             => ! [Y: $i] :
                  ( ( in @ Y @ A )
                 => ( ( X = Y )
                    | ( in @ X @ Y )
                    | ( in @ Y @ X ) ) ) )
          & ! [X: $i] :
              ( ( in @ X @ A )
             => ~ ( in @ X @ X ) ) ) ) )).

thf(wellorderedByIn_type,type,(
    wellorderedByIn: $i > $o )).

thf(wellorderedByIn,definition,
    ( wellorderedByIn
    = ( ^ [A: $i] :
          ( ( stricttotalorderedByIn @ A )
          & ! [X: $i] :
              ( ( in @ X @ ( powerset @ A ) )
             => ( ( nonempty @ X )
               => ? [Xx: $i] :
                    ( ( in @ Xx @ X )
                    & ! [Y: $i] :
                        ( ( in @ Y @ X )
                       => ( ( Xx = Y )
                          | ( in @ Xx @ Y ) ) ) ) ) ) ) ) )).

thf(ordinal_type,type,(
    ordinal: $i > $o )).

thf(ordinal,definition,
    ( ordinal
    = ( ^ [Xx: $i] :
          ( ( transitiveset @ Xx )
          & ( wellorderedByIn @ Xx ) ) ) )).

thf(ordinalTransSet_type,type,(
    ordinalTransSet: $o )).

thf(ordinalTransSet,definition,
    ( ordinalTransSet
    = ( ! [X: $i] :
          ( ( ordinal @ X )
         => ! [Xx: $i,A: $i] :
              ( ( in @ A @ X )
             => ( ( in @ Xx @ A )
               => ( in @ Xx @ X ) ) ) ) ) )).

thf(ordinalTransSet1,conjecture,
    ( subsetI1
   => ( ordinalTransSet
     => ! [X: $i] :
          ( ( ordinal @ X )
         => ! [A: $i] :
              ( ( in @ A @ X )
             => ( subset @ A @ X ) ) ) ) )).

%------------------------------------------------------------------------------
