%------------------------------------------------------------------------------
% File     : SEU846^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory
% Problem  : TPS problem GAZING-THM11
% Version  : Especial.
% English  :

% Refs     : [Bar92] Barker-Plummer D (1992), Gazing: An Approach to the Pr
%          : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0268 [Bro09]
%          : 11 [Bar92]
%          : GAZING-THM11 [TPS]

% Status   : Theorem
% Rating   : 0.00 v6.1.0, 0.17 v6.0.0, 0.00 v5.3.0, 0.25 v5.2.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type;   0 defn)
%            Number of atoms       :   20 (   0 equality;  20 variable)
%            Maximal formula depth :   10 (   6 average)
%            Number of connectives :   21 (   2   ~;   0   |;   4   &;  10   @)
%                                         (   0 <=>;   5  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :;   0   =)
%            Number of variables   :    7 (   0 sgn;   7   !;   0   ?;   0   ^)
%                                         (   7   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cGAZING_THM11_pme,conjecture,(
    ! [S: a > $o,T: a > $o,U: a > $o] :
      ( ( ! [Xx: a] :
            ( ( S @ Xx )
           => ( U @ Xx ) )
        & ! [Xx: a] :
            ( ( T @ Xx )
           => ( U @ Xx ) )
        & ! [Xx: a] :
            ( ( S @ Xx )
           => ( T @ Xx ) ) )
     => ! [Xx: a] :
          ( ( ( U @ Xx )
            & ~ ( T @ Xx ) )
         => ( ( U @ Xx )
            & ~ ( S @ Xx ) ) ) ) )).

%------------------------------------------------------------------------------
