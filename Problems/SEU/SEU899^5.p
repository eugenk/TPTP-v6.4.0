%------------------------------------------------------------------------------
% File     : SEU899^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory
% Problem  : TPS problem THM34
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0430 [Bro09]
%          : THM34 [TPS]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.00 v6.1.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    6 (   0 unit;   5 type;   0 defn)
%            Number of atoms       :   20 (   3 equality;  10 variable)
%            Maximal formula depth :    8 (   4 average)
%            Number of connectives :   13 (   0   ~;   2   |;   3   &;   7   @)
%                                         (   1 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    7 (   5   :;   0   =)
%            Number of variables   :    4 (   0 sgn;   1   !;   3   ?;   0   ^)
%                                         (   4   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(b_type,type,(
    b: $tType )).

thf(a_type,type,(
    a: $tType )).

thf(cF,type,(
    cF: b > a )).

thf(cS,type,(
    cS: b > $o )).

thf(cR,type,(
    cR: b > $o )).

thf(cTHM34_pme,conjecture,(
    ! [Xx: a] :
      ( ? [Xt: b] :
          ( ( ( cR @ Xt )
            | ( cS @ Xt ) )
          & ( Xx
            = ( cF @ Xt ) ) )
    <=> ( ? [Xt: b] :
            ( ( cR @ Xt )
            & ( Xx
              = ( cF @ Xt ) ) )
        | ? [Xt: b] :
            ( ( cS @ Xt )
            & ( Xx
              = ( cF @ Xt ) ) ) ) ) )).

%------------------------------------------------------------------------------
