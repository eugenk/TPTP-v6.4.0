%------------------------------------------------------------------------------
% File     : SEV007^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem from LATTICES-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_1246 [Bro09]

% Status   : Unknown
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type;   0 defn)
%            Number of atoms       :  346 (  70 equality; 276 variable)
%            Maximal formula depth :   44 (  23 average)
%            Number of connectives :  227 (  22   ~;   0   |;  67   &; 136   @)
%                                         (   1 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    2 (   1   :;   0   =)
%            Number of variables   :   34 (   0 sgn;  24   !;  10   ?;   0   ^)
%                                         (  34   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_UNK_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cDISTRIB_THM2_pme,conjecture,(
    ! [JOIN: a > a > a,MEET: a > a > a] :
      ( ( ! [Xx: a] :
            ( ( JOIN @ Xx @ Xx )
            = Xx )
        & ! [Xx: a] :
            ( ( MEET @ Xx @ Xx )
            = Xx )
        & ! [Xx: a,Xy: a,Xz: a] :
            ( ( JOIN @ ( JOIN @ Xx @ Xy ) @ Xz )
            = ( JOIN @ Xx @ ( JOIN @ Xy @ Xz ) ) )
        & ! [Xx: a,Xy: a,Xz: a] :
            ( ( MEET @ ( MEET @ Xx @ Xy ) @ Xz )
            = ( MEET @ Xx @ ( MEET @ Xy @ Xz ) ) )
        & ! [Xx: a,Xy: a] :
            ( ( JOIN @ Xx @ Xy )
            = ( JOIN @ Xy @ Xx ) )
        & ! [Xx: a,Xy: a] :
            ( ( MEET @ Xx @ Xy )
            = ( MEET @ Xy @ Xx ) )
        & ! [Xx: a,Xy: a] :
            ( ( JOIN @ ( MEET @ Xx @ Xy ) @ Xy )
            = Xy )
        & ! [Xx: a,Xy: a] :
            ( ( MEET @ ( JOIN @ Xx @ Xy ) @ Xy )
            = Xy ) )
     => ( ( ! [Xx: a,Xy: a,Xz: a] :
              ( ( MEET @ Xx @ ( JOIN @ Xy @ Xz ) )
              = ( JOIN @ ( MEET @ Xx @ Xy ) @ ( MEET @ Xx @ Xz ) ) )
          & ! [Xx: a,Xy: a,Xz: a] :
              ( ( JOIN @ Xx @ ( MEET @ Xy @ Xz ) )
              = ( MEET @ ( JOIN @ Xx @ Xy ) @ ( JOIN @ Xx @ Xz ) ) ) )
      <=> ( ~ ( ? [Xx: a,Xy: a,Xa: a,Xb: a,Xc: a] :
                  ( ( Xa != Xb )
                  & ( Xa != Xc )
                  & ( Xa != Xx )
                  & ( Xa != Xy )
                  & ( Xb != Xc )
                  & ( Xb != Xx )
                  & ( Xb != Xy )
                  & ( Xc != Xx )
                  & ( Xc != Xy )
                  & ( Xx != Xy )
                  & ( ( MEET @ Xx @ Xy )
                    = Xy )
                  & ( ( JOIN @ Xx @ Xy )
                    = Xx )
                  & ( ( MEET @ Xx @ Xa )
                    = Xa )
                  & ( ( JOIN @ Xx @ Xa )
                    = Xx )
                  & ( ( MEET @ Xx @ Xb )
                    = Xb )
                  & ( ( JOIN @ Xx @ Xb )
                    = Xx )
                  & ( ( MEET @ Xx @ Xc )
                    = Xc )
                  & ( ( JOIN @ Xx @ Xc )
                    = Xx )
                  & ( ( MEET @ Xa @ Xb )
                    = Xy )
                  & ( ( JOIN @ Xa @ Xb )
                    = Xx )
                  & ( ( MEET @ Xa @ Xc )
                    = Xa )
                  & ( ( JOIN @ Xa @ Xc )
                    = Xc )
                  & ( ( MEET @ Xa @ Xy )
                    = Xy )
                  & ( ( JOIN @ Xa @ Xy )
                    = Xa )
                  & ( ( MEET @ Xb @ Xc )
                    = Xy )
                  & ( ( JOIN @ Xb @ Xc )
                    = Xx )
                  & ( ( MEET @ Xb @ Xy )
                    = Xy )
                  & ( ( JOIN @ Xb @ Xy )
                    = Xb )
                  & ( ( MEET @ Xc @ Xy )
                    = Xy )
                  & ( ( JOIN @ Xc @ Xy )
                    = Xc ) ) )
          & ~ ( ? [Xx: a,Xy: a,Xa: a,Xb: a,Xc: a] :
                  ( ( Xa != Xb )
                  & ( Xa != Xc )
                  & ( Xa != Xx )
                  & ( Xa != Xy )
                  & ( Xb != Xc )
                  & ( Xb != Xx )
                  & ( Xb != Xy )
                  & ( Xc != Xx )
                  & ( Xc != Xy )
                  & ( Xx != Xy )
                  & ( ( MEET @ Xx @ Xy )
                    = Xy )
                  & ( ( JOIN @ Xx @ Xy )
                    = Xx )
                  & ( ( MEET @ Xx @ Xa )
                    = Xa )
                  & ( ( JOIN @ Xx @ Xa )
                    = Xx )
                  & ( ( MEET @ Xx @ Xb )
                    = Xb )
                  & ( ( JOIN @ Xx @ Xb )
                    = Xx )
                  & ( ( MEET @ Xx @ Xc )
                    = Xc )
                  & ( ( JOIN @ Xx @ Xc )
                    = Xx )
                  & ( ( MEET @ Xa @ Xb )
                    = Xy )
                  & ( ( JOIN @ Xa @ Xb )
                    = Xx )
                  & ( ( MEET @ Xa @ Xc )
                    = Xy )
                  & ( ( JOIN @ Xa @ Xc )
                    = Xx )
                  & ( ( MEET @ Xa @ Xy )
                    = Xy )
                  & ( ( JOIN @ Xa @ Xy )
                    = Xa )
                  & ( ( MEET @ Xb @ Xc )
                    = Xy )
                  & ( ( JOIN @ Xb @ Xc )
                    = Xx )
                  & ( ( MEET @ Xb @ Xy )
                    = Xy )
                  & ( ( JOIN @ Xb @ Xy )
                    = Xb )
                  & ( ( MEET @ Xc @ Xy )
                    = Xy )
                  & ( ( JOIN @ Xc @ Xy )
                    = Xc ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
