%------------------------------------------------------------------------------
% File     : SEV015^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem from EQUIVALENCE-RELATIONS-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0926 [Bro09]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.14 v6.1.0, 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v4.0.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type;   0 defn)
%            Number of atoms       :   24 (   1 equality;  23 variable)
%            Maximal formula depth :   12 (   7 average)
%            Number of connectives :   21 (   0   ~;   0   |;   3   &;  14   @)
%                                         (   0 <=>;   4  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :;   0   =)
%            Number of variables   :    8 (   0 sgn;   8   !;   0   ?;   0   ^)
%                                         (   8   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cTHM520_pme,conjecture,(
    ! [Xp: a > a > $o] :
      ( ( ! [Xx: a,Xy: a] :
            ( ( Xp @ Xx @ Xy )
           => ( Xp @ Xy @ Xx ) )
        & ! [Xx: a,Xy: a,Xz: a] :
            ( ( ( Xp @ Xx @ Xy )
              & ( Xp @ Xy @ Xz ) )
           => ( Xp @ Xx @ Xz ) )
        & ( Xp = Xp ) )
     => ! [Xx: a,Xy: a] :
          ( ( Xp @ Xx @ Xy )
         => ( Xp @ Xx @ Xx ) ) ) )).

%------------------------------------------------------------------------------
