%------------------------------------------------------------------------------
% File     : SEV033^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem from EQUIVALENCE-RELATIONS-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_1135 [Bro09]

% Status   : Unknown
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type;   0 defn)
%            Number of atoms       :   58 (   2 equality;  56 variable)
%            Maximal formula depth :   17 (  10 average)
%            Number of connectives :   53 (   0   ~;   0   |;  12   &;  31   @)
%                                         (   1 <=>;   9  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   13 (  13   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :;   0   =)
%            Number of variables   :   24 (   0 sgn;  19   !;   4   ?;   1   ^)
%                                         (  24   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_UNK_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cTHM262_C_EXT_pme,conjecture,
    ( ! [Xp: a > $o,Xa: a > $o] :
        ( ! [Xb: a] :
            ( ( Xp @ Xb )
            = ( Xa @ Xb ) )
       => ! [P: ( a > $o ) > $o] :
            ( ( P @ Xp )
           => ( P @ Xa ) ) )
   => ! [P: ( a > $o ) > $o] :
        ( ( ! [Xp: a > $o] :
              ( ( P @ Xp )
             => ? [Xz: a] :
                  ( Xp @ Xz ) )
          & ! [Xx: a] :
            ? [Xp: a > $o] :
              ( ( P @ Xp )
              & ( Xp @ Xx ) )
          & ! [Xx: a,Xy: a,Xp: a > $o,Xq: a > $o] :
              ( ( ( P @ Xp )
                & ( P @ Xq )
                & ( Xp @ Xx )
                & ( Xq @ Xx )
                & ( Xp @ Xy ) )
             => ( Xq @ Xy ) ) )
       => ? [Q: a > a > $o] :
            ( ! [Xx: a] :
                ( Q @ Xx @ Xx )
            & ! [Xx: a,Xy: a] :
                ( ( Q @ Xx @ Xy )
               => ( Q @ Xy @ Xx ) )
            & ! [Xx: a,Xy: a,Xz: a] :
                ( ( ( Q @ Xx @ Xy )
                  & ( Q @ Xy @ Xz ) )
               => ( Q @ Xx @ Xz ) )
            & ( ( ^ [Xs: a > $o] :
                    ( ? [Xz: a] :
                        ( Xs @ Xz )
                    & ! [Xx: a] :
                        ( ( Xs @ Xx )
                       => ! [Xy: a] :
                            ( ( Xs @ Xy )
                          <=> ( Q @ Xx @ Xy ) ) ) ) )
              = P ) ) ) )).

%------------------------------------------------------------------------------
