%------------------------------------------------------------------------------
% File     : SEV058^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem THM122
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0324 [Bro09]
%          : THM122 [TPS]

% Status   : Theorem
% Rating   : 0.12 v6.4.0, 0.14 v6.3.0, 0.17 v6.2.0, 0.33 v6.1.0, 0.17 v6.0.0, 0.00 v5.5.0, 0.20 v5.4.0, 0.25 v5.3.0, 0.50 v5.1.0, 0.75 v5.0.0, 0.50 v4.1.0, 0.33 v4.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :   19 (   0 equality;  19 variable)
%            Maximal formula depth :   11 (  11 average)
%            Number of connectives :   18 (   0   ~;   0   |;   3   &;  12   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   10 (  10   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    2 (   0   :;   0   =)
%            Number of variables   :    8 (   0 sgn;   7   !;   1   ?;   0   ^)
%                                         (   8   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(cTHM122_pme,conjecture,(
    ? [R: ( $i > $o ) > ( $i > $o ) > $o] :
      ( ! [Xu: $i > $o,Xv: $i > $o] :
          ( ( R @ Xu @ Xv )
         => ! [Xz: $i] :
              ( ( Xu @ Xz )
             => ( Xv @ Xz ) ) )
      & ! [Xx: $i > $o] :
          ( R @ Xx @ Xx )
      & ! [Xx: $i > $o,Xy: $i > $o,Xz: $i > $o] :
          ( ( ( R @ Xx @ Xy )
            & ( R @ Xy @ Xz ) )
         => ( R @ Xx @ Xz ) ) ) )).

%------------------------------------------------------------------------------
