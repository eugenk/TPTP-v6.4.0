%------------------------------------------------------------------------------
% File     : SEV081^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem from RELN-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0858 [Bro09]

% Status   : Theorem
% Rating   : 0.12 v6.4.0, 0.14 v6.3.0, 0.17 v6.2.0, 0.33 v6.1.0, 0.17 v6.0.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :   15 (   0 equality;  15 variable)
%            Maximal formula depth :   11 (  11 average)
%            Number of connectives :   14 (   0   ~;   0   |;   3   &;  10   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   10 (  10   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    2 (   0   :;   0   =)
%            Number of variables   :    7 (   0 sgn;   4   !;   3   ?;   0   ^)
%                                         (   7   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cTHM120_1_pme,conjecture,(
    ? [R: ( $i > $o ) > ( $i > $o ) > $o] :
      ( ? [X: $i > $o,Y: $i > $o] :
          ( R @ X @ Y )
      & ! [Xx: $i > $o] :
          ( R @ Xx @ Xx )
      & ! [Xx: $i > $o,Xy: $i > $o,Xz: $i > $o] :
          ( ( ( R @ Xx @ Xy )
            & ( R @ Xy @ Xz ) )
         => ( R @ Xx @ Xz ) ) ) )).

%------------------------------------------------------------------------------
