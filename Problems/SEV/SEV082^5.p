%------------------------------------------------------------------------------
% File     : SEV082^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem from RELN-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0872 [Bro09]

% Status   : Theorem
% Rating   : 0.38 v6.4.0, 0.43 v6.3.0, 0.33 v6.2.0, 0.50 v6.1.0, 0.33 v6.0.0, 0.17 v5.5.0, 0.00 v5.2.0, 0.25 v5.1.0, 0.50 v4.1.0, 0.33 v4.0.1, 0.67 v4.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :   16 (   0 equality;  13 variable)
%            Maximal formula depth :   12 (  12 average)
%            Number of connectives :   16 (   1   ~;   1   |;   3   &;  10   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    8 (   8   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   0   :;   0   =)
%            Number of variables   :    7 (   2 sgn;   4   !;   1   ?;   2   ^)
%                                         (   7   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cTHM120_4_pme,conjecture,
    ( ? [R: ( $i > $o ) > ( $i > $o ) > $o] :
        ( ~ ( R
            @ ^ [Xx: $i] : $true
            @ ^ [Xx: $i] : $false )
        & ! [Xx: $i > $o] :
            ( R @ Xx @ Xx )
        & ! [Xx: $i > $o,Xy: $i > $o,Xz: $i > $o] :
            ( ( ( R @ Xx @ Xy )
              & ( R @ Xy @ Xz ) )
           => ( R @ Xx @ Xz ) ) )
    | $false )).

%------------------------------------------------------------------------------
