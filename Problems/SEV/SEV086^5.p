%------------------------------------------------------------------------------
% File     : SEV086^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem from RELN-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0918 [Bro09]

% Status   : Theorem
% Rating   : 0.50 v6.4.0, 0.43 v6.3.0, 0.50 v5.5.0, 0.40 v5.4.0, 0.25 v5.2.0, 0.50 v5.1.0, 0.75 v4.1.0, 0.67 v4.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :   22 (   0 equality;  22 variable)
%            Maximal formula depth :   11 (  11 average)
%            Number of connectives :   21 (   0   ~;   0   |;   4   &;  14   @)
%                                         (   1 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   10 (  10   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    2 (   0   :;   0   =)
%            Number of variables   :    8 (   0 sgn;   7   !;   1   ?;   0   ^)
%                                         (   8   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cTHM120I_pme,conjecture,(
    ? [R: ( $i > $o ) > ( $i > $o ) > $o] :
      ( ! [Xx: $i > $o] :
          ( R @ Xx @ Xx )
      & ! [Xx: $i > $o,Xy: $i > $o,Xz: $i > $o] :
          ( ( ( R @ Xx @ Xy )
            & ( R @ Xy @ Xz ) )
         => ( R @ Xx @ Xz ) )
      & ! [X: $i > $o,Y: $i > $o] :
          ( ( ( R @ X @ Y )
            & ( R @ Y @ X ) )
         => ! [Xx: $i] :
              ( ( X @ Xx )
            <=> ( Y @ Xx ) ) ) ) )).

%------------------------------------------------------------------------------
