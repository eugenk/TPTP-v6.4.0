%------------------------------------------------------------------------------
% File     : SEV181^5 : TPTP v6.4.0. Bugfixed v5.2.0.
% Domain   : Set Theory (Sets of sets)
% Problem  : TPS problem from CANTOR-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0780 [Bro09]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.14 v6.1.0, 0.00 v6.0.0, 0.29 v5.5.0, 0.17 v5.4.0, 0.40 v5.2.0
% Syntax   : Number of formulae    :    3 (   0 unit;   1 type;   1 defn)
%            Number of atoms       :   22 (   4 equality;  15 variable)
%            Maximal formula depth :    9 (   7 average)
%            Number of connectives :   14 (   2   ~;   0   |;   1   &;   9   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   11 (  11   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :;   0   =)
%            Number of variables   :    6 (   0 sgn;   3   !;   1   ?;   2   ^)
%                                         (   6   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
% Bugfixes : v5.2.0 - Added missing type declarations.
%------------------------------------------------------------------------------
thf(cD_FOR_X5309_type,type,(
    cD_FOR_X5309: ( ( $i > $o ) > $i ) > $i > $o )).

thf(cD_FOR_X5309_def,definition,
    ( cD_FOR_X5309
    = ( ^ [Xh: ( $i > $o ) > $i,Xz: $i] :
        ? [Xt: $i > $o] :
          ( ~ ( Xt @ ( Xh @ Xt ) )
          & ( Xz
            = ( Xh @ Xt ) ) ) ) )).

thf(cTHM143C_pme,conjecture,(
    ! [Xh: ( $i > $o ) > $i] :
      ( ! [Xp: $i > $o,Xq: $i > $o] :
          ( ( ( Xh @ Xp )
            = ( Xh @ Xq ) )
         => ( Xp = Xq ) )
     => ~ ( cD_FOR_X5309 @ Xh @ ( Xh @ ( cD_FOR_X5309 @ Xh ) ) ) ) )).

%------------------------------------------------------------------------------
