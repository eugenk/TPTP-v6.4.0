%------------------------------------------------------------------------------
% File     : SEV212^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (Sets of sets)
% Problem  : TPS problem from S-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_1224 [Bro09]

% Status   : Theorem
% Rating   : 0.29 v6.4.0, 0.33 v6.3.0, 0.40 v6.2.0, 0.43 v5.5.0, 0.50 v5.4.0, 0.60 v4.1.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :  168 (  25 equality; 116 variable)
%            Maximal formula depth :   30 (  10 average)
%            Number of connectives :  118 (   1   ~;   6   |;  27   &;  73   @)
%                                         (   0 <=>;  11  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   12 (  12   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    6 (   3   :;   0   =)
%            Number of variables   :   44 (   0 sgn;  26   !;  18   ?;   0   ^)
%                                         (  44   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(iS_type,type,(
    iS: $tType )).

thf(cP,type,(
    cP: iS > iS > iS )).

thf(c0,type,(
    c0: iS )).

thf(cS_LEM2_pme,conjecture,
    ( ( ! [Xx: iS,Xy: iS] :
          ( ( cP @ Xx @ Xy )
         != c0 )
      & ! [Xx: iS,Xy: iS,Xu: iS,Xv: iS] :
          ( ( ( cP @ Xx @ Xu )
            = ( cP @ Xy @ Xv ) )
         => ( ( Xx = Xy )
            & ( Xu = Xv ) ) )
      & ! [X: iS > $o] :
          ( ( ( X @ c0 )
            & ! [Xx: iS,Xy: iS] :
                ( ( ( X @ Xx )
                  & ( X @ Xy ) )
               => ( X @ ( cP @ Xx @ Xy ) ) ) )
         => ! [Xx: iS] :
              ( X @ Xx ) ) )
   => ! [Xx: iS,Xy: iS,Xu: iS,Xv: iS] :
        ( ( ! [R: iS > iS > iS > $o] :
              ( ( $true
                & ! [Xa: iS,Xb: iS,Xc: iS] :
                    ( ( ( ( Xa = c0 )
                        & ( Xb = Xc ) )
                      | ( ( Xb = c0 )
                        & ( Xa = Xc ) )
                      | ? [Xx1: iS,Xx2: iS,Xy1: iS,Xy2: iS,Xz1: iS,Xz2: iS] :
                          ( ( Xa
                            = ( cP @ Xx1 @ Xx2 ) )
                          & ( Xb
                            = ( cP @ Xy1 @ Xy2 ) )
                          & ( Xc
                            = ( cP @ Xz1 @ Xz2 ) )
                          & ( R @ Xx1 @ Xy1 @ Xz1 )
                          & ( R @ Xx2 @ Xy2 @ Xz2 ) ) )
                   => ( R @ Xa @ Xb @ Xc ) ) )
             => ( R @ Xx @ Xy @ Xy ) )
          & ! [R: iS > iS > iS > $o] :
              ( ( $true
                & ! [Xa: iS,Xb: iS,Xc: iS] :
                    ( ( ( ( Xa = c0 )
                        & ( Xb = Xc ) )
                      | ( ( Xb = c0 )
                        & ( Xa = Xc ) )
                      | ? [Xx1: iS,Xx2: iS,Xy1: iS,Xy2: iS,Xz1: iS,Xz2: iS] :
                          ( ( Xa
                            = ( cP @ Xx1 @ Xx2 ) )
                          & ( Xb
                            = ( cP @ Xy1 @ Xy2 ) )
                          & ( Xc
                            = ( cP @ Xz1 @ Xz2 ) )
                          & ( R @ Xx1 @ Xy1 @ Xz1 )
                          & ( R @ Xx2 @ Xy2 @ Xz2 ) ) )
                   => ( R @ Xa @ Xb @ Xc ) ) )
             => ( R @ Xu @ Xv @ Xv ) ) )
       => ! [R: iS > iS > iS > $o] :
            ( ( $true
              & ! [Xa: iS,Xb: iS,Xc: iS] :
                  ( ( ( ( Xa = c0 )
                      & ( Xb = Xc ) )
                    | ( ( Xb = c0 )
                      & ( Xa = Xc ) )
                    | ? [Xx1: iS,Xx2: iS,Xy1: iS,Xy2: iS,Xz1: iS,Xz2: iS] :
                        ( ( Xa
                          = ( cP @ Xx1 @ Xx2 ) )
                        & ( Xb
                          = ( cP @ Xy1 @ Xy2 ) )
                        & ( Xc
                          = ( cP @ Xz1 @ Xz2 ) )
                        & ( R @ Xx1 @ Xy1 @ Xz1 )
                        & ( R @ Xx2 @ Xy2 @ Xz2 ) ) )
                 => ( R @ Xa @ Xb @ Xc ) ) )
           => ( R @ ( cP @ Xx @ Xu ) @ ( cP @ Xy @ Xv ) @ ( cP @ Xy @ Xv ) ) ) ) )).

%------------------------------------------------------------------------------
