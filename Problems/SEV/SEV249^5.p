%------------------------------------------------------------------------------
% File     : SEV249^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (Sets of sets)
% Problem  : TPS problem from SETS-OF-SETS-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0992 [Bro09]

% Status   : Theorem
% Rating   : 1.00 v6.2.0, 0.71 v6.0.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type;   0 defn)
%            Number of atoms       :   27 (   2 equality;  21 variable)
%            Maximal formula depth :   12 (   8 average)
%            Number of connectives :   22 (   0   ~;   2   |;   2   &;  12   @)
%                                         (   0 <=>;   6  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   13 (  13   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   1   :;   0   =)
%            Number of variables   :   12 (   2 sgn;   7   !;   0   ?;   5   ^)
%                                         (  12   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cX,type,(
    cX: $i > $o )).

thf(cTHM626_pme,conjecture,
    ( ! [Xw: ( $i > $o ) > $o] :
        ( ( ( Xw
            @ ^ [Xx: $i] : $false )
          & ! [Xr: $i > $o,Xx: $i] :
              ( ( Xw @ Xr )
             => ( Xw
                @ ^ [Xt: $i] :
                    ( ( Xr @ Xt )
                    | ( Xt = Xx ) ) ) ) )
       => ( Xw @ cX ) )
   => ! [Xw: ( ( $i > $o ) > $o ) > $o] :
        ( ( ( Xw
            @ ^ [Xx: $i > $o] : $false )
          & ! [Xr: ( $i > $o ) > $o,Xx: $i > $o] :
              ( ( Xw @ Xr )
             => ( Xw
                @ ^ [Xt: $i > $o] :
                    ( ( Xr @ Xt )
                    | ( Xt = Xx ) ) ) ) )
       => ( Xw
          @ ^ [R: $i > $o] :
            ! [Xx: $i] :
              ( ( R @ Xx )
             => ( cX @ Xx ) ) ) ) )).

%------------------------------------------------------------------------------
