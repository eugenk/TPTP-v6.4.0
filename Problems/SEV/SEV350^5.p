%------------------------------------------------------------------------------
% File     : SEV350^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (GvNB)
% Problem  : TPS problem from GVB-MB-AXIOMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0777 [Bro09]

% Status   : CounterSatisfiable
% Rating   : 0.00 v5.4.0, 0.67 v5.0.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    6 (   0 unit;   5 type;   0 defn)
%            Number of atoms       :   14 (   0 equality;   3 variable)
%            Maximal formula depth :    8 (   4 average)
%            Number of connectives :   13 (   0   ~;   0   |;   3   &;   9   @)
%                                         (   1 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    7 (   5   :;   0   =)
%            Number of variables   :    1 (   0 sgn;   0   !;   1   ?;   0   ^)
%                                         (   1   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(z,type,(
    z: $i )).

thf(x,type,(
    x: $i )).

thf(cGVB_IN,type,(
    cGVB_IN: $i > $i > $o )).

thf(cGVB_M,type,(
    cGVB_M: $i > $o )).

thf(cGVB_SIGMA,type,(
    cGVB_SIGMA: $i > $i )).

thf(cGVB_C2,conjecture,
    ( ( cGVB_IN @ z @ ( cGVB_SIGMA @ x ) )
  <=> ( ( cGVB_M @ z )
      & ? [Xy: $i] :
          ( ( cGVB_M @ Xy )
          & ( cGVB_IN @ Xy @ x )
          & ( cGVB_IN @ z @ Xy ) ) ) )).

%------------------------------------------------------------------------------
