%------------------------------------------------------------------------------
% File     : SEV372^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory (GvNB)
% Problem  : TPS problem from GVB-MB-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0719 [Bro09]

% Status   : CounterSatisfiable
% Rating   : 0.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :   10 (   1 equality;   5 variable)
%            Maximal formula depth :    8 (   4 average)
%            Number of connectives :    7 (   0   ~;   0   |;   1   &;   5   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :;   0   =)
%            Number of variables   :    2 (   0 sgn;   2   !;   0   ?;   0   ^)
%                                         (   2   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cGVB_OP,type,(
    cGVB_OP: $i > $i > $i )).

thf(cGVB_FIRST,type,(
    cGVB_FIRST: $i > $i )).

thf(cGVB_M,type,(
    cGVB_M: $i > $o )).

thf(cGVB_FST_PROP_1,conjecture,(
    ! [Xa: $i,Xb: $i] :
      ( ( ( cGVB_M @ Xa )
        & ( cGVB_M @ Xb ) )
     => ( ( cGVB_FIRST @ ( cGVB_OP @ Xa @ Xb ) )
        = Xa ) ) )).

%------------------------------------------------------------------------------
