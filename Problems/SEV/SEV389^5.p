%------------------------------------------------------------------------------
% File     : SEV389^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory
% Problem  : TPS problem THM37
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0183 [Bro09]
%          : THM37 [TPS]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.14 v6.1.0, 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v4.0.0
% Syntax   : Number of formulae    :    3 (   0 unit;   2 type;   0 defn)
%            Number of atoms       :   10 (   1 equality;   4 variable)
%            Maximal formula depth :    6 (   4 average)
%            Number of connectives :    7 (   0   ~;   0   |;   1   &;   4   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   2   :;   0   =)
%            Number of variables   :    2 (   0 sgn;   1   !;   0   ?;   1   ^)
%                                         (   2   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(cS,type,(
    cS: $i > $o )).

thf(cR,type,(
    cR: $i > $o )).

thf(cTHM37_pme,conjecture,
    ( ( cR
      = ( ^ [Xx: $i] :
            ( ( cR @ Xx )
            & ( cS @ Xx ) ) ) )
   => ! [Xx: $i] :
        ( ( cR @ Xx )
       => ( cS @ Xx ) ) )).

%------------------------------------------------------------------------------
