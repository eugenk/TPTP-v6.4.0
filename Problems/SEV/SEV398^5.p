%------------------------------------------------------------------------------
% File     : SEV398^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory
% Problem  : TPS problem THM67A
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0437 [Bro09]
%          : THM67A [TPS]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.33 v6.1.0, 0.17 v6.0.0, 0.00 v5.1.0, 0.25 v5.0.0, 0.00 v4.0.1, 0.33 v4.0.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :   30 (   0 equality;  20 variable)
%            Maximal formula depth :   10 (   5 average)
%            Number of connectives :   29 (   0   ~;   0   |;   2   &;  20   @)
%                                         (   1 <=>;   6  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   10 (  10   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :;   0   =)
%            Number of variables   :    9 (   0 sgn;   9   !;   0   ?;   0   ^)
%                                         (   9   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cF,type,(
    cF: ( a > $o ) > a > $o )).

thf(cG,type,(
    cG: ( a > $o ) > a > $o )).

thf(cTHM67A_pme,conjecture,
    ( ( ! [S: a > $o,T: a > $o] :
          ( ! [Xx: a] :
              ( ( S @ Xx )
             => ( T @ Xx ) )
         => ! [Xx: a] :
              ( ( cF @ T @ Xx )
             => ( cF @ S @ Xx ) ) )
      & ! [S: a > $o] :
          ( ! [Xx: a] :
              ( ( S @ Xx )
             => ( cF @ ( cG @ S ) @ Xx ) )
          & ! [Xx: a] :
              ( ( S @ Xx )
             => ( cG @ ( cF @ S ) @ Xx ) ) ) )
   => ! [S: a > $o,Xx: a] :
        ( ( cF @ ( cG @ ( cF @ S ) ) @ Xx )
      <=> ( cF @ S @ Xx ) ) )).

%------------------------------------------------------------------------------
