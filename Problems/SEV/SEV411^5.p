%------------------------------------------------------------------------------
% File     : SEV411^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Set Theory
% Problem  : TPS problem from SETS-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0799 [Bro09]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.17 v6.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    3 (   0 unit;   2 type;   0 defn)
%            Number of atoms       :   12 (   0 equality;   6 variable)
%            Maximal formula depth :    6 (   4 average)
%            Number of connectives :   11 (   0   ~;   1   |;   0   &;   6   @)
%                                         (   0 <=>;   4  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   2   :;   0   =)
%            Number of variables   :    3 (   0 sgn;   3   !;   0   ?;   0   ^)
%                                         (   3   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cB,type,(
    cB: $i > $o )).

thf(cA,type,(
    cA: $i > $o )).

thf(cDUAL_EG1_pme,conjecture,
    ( ( ! [Xx: $i] :
          ( ( cA @ Xx )
         => ( cB @ Xx ) )
      | ! [Xx: $i] :
          ( ( cA @ Xx )
         => ( cB @ Xx ) ) )
   => ! [Xx: $i] :
        ( ( cA @ Xx )
       => ( cB @ Xx ) ) )).

%------------------------------------------------------------------------------
