%------------------------------------------------------------------------------
% File     : SEV437+1 : TPTP v6.4.0. Released v6.4.0.
% Domain   : Set Theory
% Problem  : Naive set theory based on Goedel's set theory
% Version  : [Pas99] axioms.
% English  :

% Refs     : [Pas99] Pastre (1999), Email to G. Sutcliffe
% Source   : [TPTP]
% Names    :

% Status   : Satisfiable
% Rating   : 1.00 v6.4.0
% Syntax   : Number of formulae    :   33 (   1 unit)
%            Number of atoms       :  167 (   7 equality)
%            Maximal formula depth :   19 (   9 average)
%            Number of connectives :  137 (   3   ~;   2   |;  67   &)
%                                         (  35 <=>;  30  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   20 (   0 propositional; 2-6 arity)
%            Number of functors    :   16 (   1 constant; 0-5 arity)
%            Number of variables   :  162 (   0 sgn; 150   !;  12   ?)
%            Maximal term depth    :    2 (   1 average)
% SPC      : FOF_SAT_RFO_SEQ

% Comments : 
%------------------------------------------------------------------------------
%----Naive set theory based on Goedel's set theory
include('Axioms/SET006+0.ax').
%----Mapping axioms for the SET006+0 set theory axioms
include('Axioms/SET006+1.ax').
%----quivalence relation axioms for the SET006+0 set theory axioms
include('Axioms/SET006+2.ax').
%------------------------------------------------------------------------------
