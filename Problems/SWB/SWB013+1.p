%------------------------------------------------------------------------------
% File     : SWB013+1 : TPTP v6.4.0. Released v5.2.0.
% Domain   : Semantic Web
% Problem  : Cliques
% Version  : [Sch11] axioms.
% English  : 

% Refs     : [Sch11] Schneider, M. (2011), Email to G. Sutcliffe
% Source   : [Sch11]
% Names    : 013_Cliques [Sch11]

% Status   : Theorem
% Rating   : 0.87 v6.4.0, 0.88 v6.3.0, 0.83 v6.2.0, 0.84 v6.1.0, 0.90 v6.0.0, 0.96 v5.4.0, 1.00 v5.2.0
% Syntax   : Number of formulae    :  560 ( 197 unit)
%            Number of atoms       : 1792 (  90 equality)
%            Maximal formula depth :   27 (   5 average)
%            Number of connectives : 1368 ( 136   ~;  35   |; 776   &)
%                                         ( 126 <=>; 295  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   13 (   1 propositional; 0-3 arity)
%            Number of functors    :  163 ( 162 constant; 0-2 arity)
%            Number of variables   :  978 (   0 sgn; 911   !;  67   ?)
%            Maximal term depth    :    2 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%------------------------------------------------------------------------------
%----Include OWL 2 Full axioms
include('Axioms/SWB001+0.ax').
%------------------------------------------------------------------------------
fof(testcase_conclusion_fullish_013_Cliques,conjecture,(
    iext(uri_foaf_knows,uri_ex_alice,uri_ex_bob) )).

fof(testcase_premise_fullish_013_Cliques,axiom,(
    ? [BNODE_r,BNODE_i,BNODE_l1,BNODE_l2,BNODE_l3] :
      ( iext(uri_rdf_type,uri_ex_Clique,uri_owl_Class)
      & iext(uri_rdfs_subPropertyOf,uri_ex_sameCliqueAs,uri_owl_sameAs)
      & iext(uri_rdfs_range,uri_ex_sameCliqueAs,uri_ex_Clique)
      & iext(uri_rdfs_subClassOf,uri_ex_Clique,BNODE_r)
      & iext(uri_rdf_type,BNODE_r,uri_owl_Restriction)
      & iext(uri_owl_onProperty,BNODE_r,uri_ex_sameCliqueAs)
      & iext(uri_owl_someValuesFrom,BNODE_r,uri_ex_Clique)
      & iext(uri_rdf_type,uri_foaf_knows,uri_owl_ObjectProperty)
      & iext(uri_owl_propertyChainAxiom,uri_foaf_knows,BNODE_l1)
      & iext(uri_rdf_first,BNODE_l1,uri_rdf_type)
      & iext(uri_rdf_rest,BNODE_l1,BNODE_l2)
      & iext(uri_rdf_first,BNODE_l2,uri_ex_sameCliqueAs)
      & iext(uri_rdf_rest,BNODE_l2,BNODE_l3)
      & iext(uri_rdf_first,BNODE_l3,BNODE_i)
      & iext(uri_rdf_rest,BNODE_l3,uri_rdf_nil)
      & iext(uri_owl_inverseOf,BNODE_i,uri_rdf_type)
      & iext(uri_rdf_type,uri_ex_JoesGang,uri_ex_Clique)
      & iext(uri_rdf_type,uri_ex_alice,uri_ex_JoesGang)
      & iext(uri_rdf_type,uri_ex_bob,uri_ex_JoesGang) ) )).

%------------------------------------------------------------------------------
