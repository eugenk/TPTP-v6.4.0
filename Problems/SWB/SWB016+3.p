%------------------------------------------------------------------------------
% File     : SWB016+3 : TPTP v6.4.0. Released v5.2.0.
% Domain   : Semantic Web
% Problem  : Reflective Tautologies II
% Version  : [Sch11] axioms : Incomplete.
% English  : 

% Refs     : [Sch11] Schneider, M. (2011), Email to G. Sutcliffe
% Source   : [Sch11]
% Names    : 016_Reflective_Tautologies_II [Sch11]

% Status   : CounterSatisfiable
% Rating   : 0.33 v6.4.0, 0.00 v6.3.0, 0.50 v6.2.0, 0.78 v6.1.0, 0.70 v6.0.0, 0.86 v5.5.0, 0.71 v5.4.0, 0.93 v5.3.0, 0.92 v5.2.0
% Syntax   : Number of formulae    :  139 (  74 unit)
%            Number of atoms       :  311 (   0 equality)
%            Maximal formula depth :   18 (   3 average)
%            Number of connectives :  175 (   3   ~;   3   |;  74   &)
%                                         (  38 <=>;  57  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   11 (   0 propositional; 1-3 arity)
%            Number of functors    :   48 (  48 constant; 0-0 arity)
%            Number of variables   :  159 (   0 sgn; 157   !;   2   ?)
%            Maximal term depth    :    1 (   1 average)
% SPC      : FOF_CSA_RFO_NEQ

% Comments :
%------------------------------------------------------------------------------
%----Include ALCO Full Extensional axioms
include('Axioms/SWB002+0.ax').
%------------------------------------------------------------------------------
fof(testcase_conclusion_fullish_016_Reflective_Tautologies_II,conjecture,(
    iext(uri_rdfs_subPropertyOf,uri_owl_equivalentClass,uri_rdfs_subClassOf) )).

%------------------------------------------------------------------------------
