%------------------------------------------------------------------------------
% File     : SWB069+1 : TPTP v6.4.0. Released v5.2.0.
% Domain   : Semantic Web
% Problem  : N-Ary Class Disjointness Extensional
% Version  : [Sch11] axioms.
% English  : For a group of mutually disjoint classes, the corresponding 
%            owl:AllDisjointClasses construct exists.

% Refs     : [Sch11] Schneider, M. (2011), Email to G. Sutcliffe
% Source   : [Sch11]
% Names    : rdfbased-sem-ndis-alldisjointclasses-bw [Sch11]

% Status   : Theorem
% Rating   : 0.87 v6.4.0, 0.85 v6.3.0, 0.88 v6.2.0, 0.92 v6.1.0, 0.93 v6.0.0, 0.91 v5.5.0, 0.93 v5.4.0, 0.96 v5.2.0
% Syntax   : Number of formulae    :  560 ( 196 unit)
%            Number of atoms       : 1789 (  90 equality)
%            Maximal formula depth :   27 (   5 average)
%            Number of connectives : 1365 ( 136   ~;  35   |; 773   &)
%                                         ( 126 <=>; 295  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of predicates  :   13 (   1 propositional; 0-3 arity)
%            Number of functors    :  160 ( 159 constant; 0-2 arity)
%            Number of variables   :  980 (   0 sgn; 911   !;  69   ?)
%            Maximal term depth    :    2 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%------------------------------------------------------------------------------
%----Include OWL 2 Full axioms
include('Axioms/SWB001+0.ax').
%------------------------------------------------------------------------------
fof(conclusion_rdfbased_sem_ndis_alldisjointclasses_bw,conjecture,(
    ? [X2,X3,X0,X1] :
      ( iext(uri_rdf_first,X0,uri_ex_c2)
      & iext(uri_rdf_rest,X0,X1)
      & iext(uri_rdf_first,X1,uri_ex_c3)
      & iext(uri_rdf_rest,X1,uri_rdf_nil)
      & iext(uri_rdf_type,X2,uri_owl_AllDisjointClasses)
      & iext(uri_owl_members,X2,X3)
      & iext(uri_rdf_first,X3,uri_ex_c1)
      & iext(uri_rdf_rest,X3,X0) ) )).

fof(premise_rdfbased_sem_ndis_alldisjointclasses_bw,axiom,(
    ? [X1,X2,X0] :
      ( iext(uri_owl_disjointWith,uri_ex_c2,uri_ex_c3)
      & iext(uri_rdf_first,X0,uri_ex_c2)
      & iext(uri_rdf_rest,X0,X1)
      & iext(uri_owl_disjointWith,uri_ex_c1,uri_ex_c2)
      & iext(uri_owl_disjointWith,uri_ex_c1,uri_ex_c3)
      & iext(uri_rdf_first,X2,uri_ex_c1)
      & iext(uri_rdf_rest,X2,X0)
      & iext(uri_rdf_first,X1,uri_ex_c3)
      & iext(uri_rdf_rest,X1,uri_rdf_nil) ) )).

%------------------------------------------------------------------------------
