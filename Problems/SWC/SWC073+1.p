%--------------------------------------------------------------------------
% File     : SWC073+1 : TPTP v6.4.0. Released v2.4.0.
% Domain   : Software Creation
% Problem  : cond_id_segment_total2_x_rot_r_total1
% Version  : [Wei00] axioms.
% English  : Find components in a software library that match a given target
%            specification given in first-order logic. The components are
%            specified in first-order logic as well. The problem represents
%            a test of one library module specification against a target
%            specification.

% Refs     : [Wei00] Weidenbach (2000), Software Reuse of List Functions Ve
%          : [FSS98] Fischer et al. (1998), Deduction-Based Software Compon
% Source   : [Wei00]
% Names    : cond_id_segment_total2_x_rot_r_total1 [Wei00]

% Status   : Theorem
% Rating   : 0.80 v6.4.0, 0.81 v6.3.0, 0.83 v6.2.0, 0.96 v6.1.0, 0.97 v6.0.0, 0.96 v5.3.0, 1.00 v5.2.0, 0.95 v5.0.0, 0.96 v4.1.0, 1.00 v3.3.0, 0.93 v3.2.0, 1.00 v2.4.0
% Syntax   : Number of formulae    :   96 (   9 unit)
%            Number of atoms       :  421 (  82 equality)
%            Maximal formula depth :   30 (   7 average)
%            Number of connectives :  354 (  29 ~  ;  14  |;  54  &)
%                                         (  26 <=>; 231 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   20 (   0 propositional; 1-2 arity)
%            Number of functors    :    5 (   1 constant; 0-2 arity)
%            Number of variables   :  212 (   0 singleton; 194 !;  18 ?)
%            Maximal term depth    :    4 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%--------------------------------------------------------------------------
%----Include list specification axioms
include('Axioms/SWC001+0.ax').
%--------------------------------------------------------------------------
fof(co1,conjecture,
    ( ! [U] :
        ( ssList(U)
       => ! [V] :
            ( ssList(V)
           => ! [W] :
                ( ssList(W)
               => ! [X] :
                    ( ssList(X)
                   => ( V != X
                      | U != W
                      | ? [Y] :
                          ( ssList(Y)
                          & neq(Y,nil)
                          & segmentP(V,Y)
                          & segmentP(U,Y) )
                      | ( nil != W
                        & nil = X )
                      | ( nil = V
                        & nil = U )
                      | ( neq(X,nil)
                        & ( ~ neq(W,nil)
                          | ? [Z] :
                              ( ssList(Z)
                              & X != Z
                              & ? [X1] :
                                  ( ssList(X1)
                                  & ? [X2] :
                                      ( ssList(X2)
                                      & tl(W) = X1
                                      & app(X1,X2) = Z
                                      & ? [X3] :
                                          ( ssItem(X3)
                                          & cons(X3,nil) = X2
                                          & hd(W) = X3
                                          & neq(nil,W) )
                                      & neq(nil,W) ) ) ) ) ) ) ) ) ) ) )).

%--------------------------------------------------------------------------
