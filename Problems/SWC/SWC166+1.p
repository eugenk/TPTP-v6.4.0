%--------------------------------------------------------------------------
% File     : SWC166+1 : TPTP v6.4.0. Released v2.4.0.
% Domain   : Software Creation
% Problem  : cond_pst_diff_adj1_x_pst_singleton
% Version  : [Wei00] axioms.
% English  : Find components in a software library that match a given target
%            specification given in first-order logic. The components are
%            specified in first-order logic as well. The problem represents
%            a test of one library module specification against a target
%            specification.

% Refs     : [Wei00] Weidenbach (2000), Software Reuse of List Functions Ve
%          : [FSS98] Fischer et al. (1998), Deduction-Based Software Compon
% Source   : [Wei00]
% Names    : cond_pst_diff_adj1_x_pst_singleton [Wei00]

% Status   : Theorem
% Rating   : 0.63 v6.4.0, 0.65 v6.3.0, 0.62 v6.2.0, 0.76 v6.1.0, 0.83 v6.0.0, 0.70 v5.5.0, 0.78 v5.4.0, 0.79 v5.3.0, 0.85 v5.2.0, 0.80 v5.1.0, 0.86 v5.0.0, 0.88 v4.1.0, 0.91 v4.0.1, 0.96 v3.7.0, 0.90 v3.5.0, 0.95 v3.3.0, 0.93 v3.2.0, 1.00 v2.6.0, 0.83 v2.4.0
% Syntax   : Number of formulae    :   96 (   9 unit)
%            Number of atoms       :  407 (  74 equality)
%            Maximal formula depth :   22 (   7 average)
%            Number of connectives :  339 (  28 ~  ;  12  |;  38  &)
%                                         (  26 <=>; 235 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   20 (   0 propositional; 1-2 arity)
%            Number of functors    :    5 (   1 constant; 0-2 arity)
%            Number of variables   :  211 (   0 singleton; 198 !;  13 ?)
%            Maximal term depth    :    5 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%--------------------------------------------------------------------------
%----Include list specification axioms
include('Axioms/SWC001+0.ax').
%--------------------------------------------------------------------------
fof(co1,conjecture,
    ( ! [U] :
        ( ssList(U)
       => ! [V] :
            ( ssList(V)
           => ! [W] :
                ( ssList(W)
               => ! [X] :
                    ( ssList(X)
                   => ( V != X
                      | U != W
                      | ~ singletonP(W)
                      | ! [Y] :
                          ( ssItem(Y)
                         => ! [Z] :
                              ( ssItem(Z)
                             => ! [X1] :
                                  ( ssList(X1)
                                 => ! [X2] :
                                      ( ssList(X2)
                                     => ( app(app(app(X1,cons(Y,nil)),cons(Z,nil)),X2) != U
                                        | neq(Y,Z) ) ) ) ) ) ) ) ) ) ) )).

%--------------------------------------------------------------------------
