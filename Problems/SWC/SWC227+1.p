%--------------------------------------------------------------------------
% File     : SWC227+1 : TPTP v6.4.0. Released v2.4.0.
% Domain   : Software Creation
% Problem  : cond_pst_pivoted1_x_run_ord_max2
% Version  : [Wei00] axioms.
% English  : Find components in a software library that match a given target
%            specification given in first-order logic. The components are
%            specified in first-order logic as well. The problem represents
%            a test of one library module specification against a target
%            specification.

% Refs     : [Wei00] Weidenbach (2000), Software Reuse of List Functions Ve
%          : [FSS98] Fischer et al. (1998), Deduction-Based Software Compon
% Source   : [Wei00]
% Names    : cond_pst_pivoted1_x_run_ord_max2 [Wei00]

% Status   : Theorem
% Rating   : 0.80 v6.4.0, 0.73 v6.3.0, 0.75 v6.2.0, 0.84 v6.1.0, 0.87 v6.0.0, 0.78 v5.5.0, 0.81 v5.4.0, 0.82 v5.3.0, 0.89 v5.2.0, 0.80 v5.1.0, 0.81 v5.0.0, 0.83 v4.0.1, 0.87 v4.0.0, 0.88 v3.7.0, 0.80 v3.5.0, 0.79 v3.3.0, 0.71 v3.2.0, 0.73 v3.1.0, 0.78 v2.7.0, 0.67 v2.6.0, 0.83 v2.4.0
% Syntax   : Number of formulae    :   96 (   9 unit)
%            Number of atoms       :  430 (  82 equality)
%            Maximal formula depth :   31 (   7 average)
%            Number of connectives :  369 (  35 ~  ;  22  |;  55  &)
%                                         (  26 <=>; 231 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   20 (   0 propositional; 1-2 arity)
%            Number of functors    :    5 (   1 constant; 0-2 arity)
%            Number of variables   :  221 (   0 singleton; 197 !;  24 ?)
%            Maximal term depth    :    4 (   1 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%--------------------------------------------------------------------------
%----Include list specification axioms
include('Axioms/SWC001+0.ax').
%--------------------------------------------------------------------------
fof(co1,conjecture,
    ( ! [U] :
        ( ssList(U)
       => ! [V] :
            ( ssList(V)
           => ! [W] :
                ( ssList(W)
               => ! [X] :
                    ( ~ ssList(X)
                    | V != X
                    | U != W
                    | nil = U
                    | ? [Y] :
                        ( ssItem(Y)
                        & ? [Z] :
                            ( ssList(Z)
                            & ? [X1] :
                                ( ssList(X1)
                                & app(app(Z,cons(Y,nil)),X1) = U
                                & ! [X2] :
                                    ( ~ ssItem(X2)
                                    | ~ memberP(Z,X2)
                                    | ~ memberP(X1,X2)
                                    | ~ leq(Y,X2)
                                    | leq(X2,Y) ) ) ) )
                    | ! [X3] :
                        ( ssList(X3)
                       => ! [X4] :
                            ( ~ ssList(X4)
                            | app(app(X3,W),X4) != X
                            | ~ totalorderedP(W)
                            | ? [X5] :
                                ( ssItem(X5)
                                & ? [X6] :
                                    ( ssList(X6)
                                    & app(X6,cons(X5,nil)) = X3
                                    & ? [X7] :
                                        ( ssItem(X7)
                                        & ? [X8] :
                                            ( ssList(X8)
                                            & app(cons(X7,nil),X8) = W
                                            & leq(X5,X7) ) ) ) )
                            | ? [X9] :
                                ( ssItem(X9)
                                & ? [X10] :
                                    ( ssList(X10)
                                    & app(cons(X9,nil),X10) = X4
                                    & ? [X11] :
                                        ( ssItem(X11)
                                        & ? [X12] :
                                            ( ssList(X12)
                                            & app(X12,cons(X11,nil)) = W
                                            & leq(X11,X9) ) ) ) ) ) )
                    | ( nil != X
                      & nil = W ) ) ) ) ) )).

%--------------------------------------------------------------------------
