%------------------------------------------------------------------------------
% File     : SWV306-1 : TPTP v6.4.0. Released v3.2.0.
% Domain   : Software Verification (Security)
% Problem  : Cryptographic protocol problem for Otway Rees
% Version  : [Pau06] axioms : Especial.
% English  :

% Refs     : [Pau06] Paulson (2006), Email to G. Sutcliffe
% Source   : [Pau06]
% Names    : OtwayRees__no_nonce_OR1_OR2_2 [Pau06]

% Status   : Unsatisfiable
% Rating   : 0.33 v6.4.0, 0.27 v6.2.0, 0.40 v6.1.0, 0.43 v6.0.0, 0.50 v5.5.0, 0.75 v5.3.0, 0.78 v5.2.0, 0.69 v5.1.0, 0.71 v5.0.0, 0.64 v4.1.0, 0.62 v4.0.1, 0.55 v3.7.0, 0.40 v3.5.0, 0.45 v3.4.0, 0.58 v3.3.0, 0.71 v3.2.0
% Syntax   : Number of clauses     : 1480 (  34 non-Horn; 274 unit;1378 RR)
%            Number of atoms       : 2761 ( 281 equality)
%            Maximal clause size   :    4 (   2 average)
%            Number of predicates  :   82 (   0 propositional; 1-3 arity)
%            Number of functors    :  161 (  36 constant; 0-6 arity)
%            Number of variables   : 2223 ( 381 singleton)
%            Maximal term depth    :    6 (   1 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments : The problems in the [Pau06] collection each have very many axioms,
%            of which only a small selection are required for the refutation.
%            The mission is to find those few axioms, after which a refutation
%            can be quite easily found.
%------------------------------------------------------------------------------
include('Axioms/MSC001-0.ax').
include('Axioms/MSC001-2.ax').
include('Axioms/SWV006-0.ax').
include('Axioms/SWV006-2.ax').
%------------------------------------------------------------------------------
cnf(cls_OtwayRees_OSpy__see__shrK__D__dest_0,axiom,
    ( ~ c_in(V_evs,c_OtwayRees_Ootway,tc_List_Olist(tc_Event_Oevent))
    | ~ c_in(c_Message_Omsg_OKey(c_Public_OshrK(V_A)),c_Message_Oparts(c_Event_Oknows(c_Message_Oagent_OSpy,V_evs)),tc_Message_Omsg)
    | c_in(V_A,c_Event_Obad,tc_Message_Oagent) )).

cnf(cls_conjecture_0,negated_conjecture,
    ( ~ c_in(v_A,c_Event_Obad,tc_Message_Oagent) )).

cnf(cls_conjecture_1,negated_conjecture,
    ( c_in(v_evs1,c_OtwayRees_Ootway,tc_List_Olist(tc_Event_Oevent)) )).

cnf(cls_conjecture_2,negated_conjecture,
    ( ~ c_in(c_Message_Omsg_ONonce(v_NAa),c_Event_Oused(v_evs1),tc_Message_Omsg) )).

cnf(cls_conjecture_3,negated_conjecture,
    ( v_A = v_Aa )).

cnf(cls_conjecture_4,negated_conjecture,
    ( v_NA = c_Message_Omsg_ONonce(v_NAa) )).

cnf(cls_conjecture_5,negated_conjecture,
    ( v_A = v_Aa )).

cnf(cls_conjecture_6,negated_conjecture,
    ( v_B = v_Ba )).

cnf(cls_conjecture_7,negated_conjecture,
    ( c_in(c_Message_Omsg_OCrypt(c_Public_OshrK(v_Aa),c_Message_Omsg_OMPair(v_NA_H,c_Message_Omsg_OMPair(c_Message_Omsg_ONonce(v_NAa),c_Message_Omsg_OMPair(c_Message_Omsg_OAgent(v_A_H),c_Message_Omsg_OAgent(v_Aa))))),c_Message_Oparts(c_Event_Oknows(c_Message_Oagent_OSpy,v_evs1)),tc_Message_Omsg) )).

cnf(cls_conjecture_8,negated_conjecture,
    ( ~ c_in(c_Message_Omsg_OCrypt(c_Public_OshrK(v_A),c_Message_Omsg_OMPair(v_NA_H,c_Message_Omsg_OMPair(v_NA,c_Message_Omsg_OMPair(c_Message_Omsg_OAgent(v_A_H),c_Message_Omsg_OAgent(v_A))))),c_Message_Oparts(c_Event_Oknows(c_Message_Oagent_OSpy,v_evs1)),tc_Message_Omsg)
    | ~ c_in(c_Message_Omsg_OCrypt(c_Public_OshrK(v_A),c_Message_Omsg_OMPair(v_NA,c_Message_Omsg_OMPair(c_Message_Omsg_OAgent(v_A),c_Message_Omsg_OAgent(v_B)))),c_Message_Oparts(c_Event_Oknows(c_Message_Oagent_OSpy,v_evs1)),tc_Message_Omsg) )).

%------------------------------------------------------------------------------
