%------------------------------------------------------------------------------
% File     : SWV398+1 : TPTP v6.4.0. Released v3.3.0.
% Domain   : Software Verification
% Problem  : Priority queue checker: tmp_not_check_03
% Version  : [dNP05] axioms.
% English  :

% Refs     : [Pis06] Piskac (2006), Email to Geoff Sutcliffe
%          : [dNP05] de Nivelle & Piskac (2005), Verification of an Off-Lin
% Source   : [Pis06]
% Names    : cpq_l034 [Pis06]

% Status   : Theorem
% Rating   : 0.57 v6.4.0, 0.62 v6.2.0, 0.72 v6.1.0, 0.77 v6.0.0, 0.74 v5.5.0, 0.81 v5.4.0, 0.79 v5.3.0, 0.85 v5.2.0, 0.80 v5.1.0, 0.81 v5.0.0, 0.79 v4.1.0, 0.78 v4.0.1, 0.83 v4.0.0, 0.88 v3.7.0, 0.85 v3.5.0, 0.89 v3.4.0, 0.84 v3.3.0
% Syntax   : Number of formulae    :   44 (  16 unit)
%            Number of atoms       :   91 (  29 equality)
%            Maximal formula depth :    9 (   5 average)
%            Number of connectives :   62 (  15 ~  ;   3  |;  14  &)
%                                         (   8 <=>;  22 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :   11 (   1 propositional; 0-3 arity)
%            Number of functors    :   18 (   3 constant; 0-3 arity)
%            Number of variables   :  131 (   4 singleton; 127 !;   4 ?)
%            Maximal term depth    :    4 (   2 average)
% SPC      : FOF_THM_RFO_SEQ

% Comments :
%------------------------------------------------------------------------------
%----Include the axioms about priority queues and checked priority queues
include('Axioms/SWV007+0.ax').
include('Axioms/SWV007+2.ax').
include('Axioms/SWV007+3.ax').
%------------------------------------------------------------------------------
%----lemma_check_characterization (cpq_l041.p, cpq_l042.p)
fof(l26_li4142,lemma,(
    ! [U,V,W] :
      ( check_cpq(triple(U,V,W))
    <=> ! [X,Y] :
          ( pair_in_list(V,X,Y)
         => less_than(Y,X) ) ) )).

%----tmp_not_check_03_1 (cpq_l035.p)
fof(l34_l35,lemma,(
    ! [U] :
      ( ? [V,W] :
          ( pair_in_list(U,V,W)
          & strictly_less_than(V,W) )
     => ! [X] :
        ? [Y,Z] :
          ( pair_in_list(update_slb(U,X),Y,Z)
          & strictly_less_than(Y,Z) ) ) )).

%----tmp_not_check_03 (conjecture)
fof(l24_co,conjecture,(
    ! [U,V,W] :
      ( ~ check_cpq(triple(U,V,W))
     => ~ check_cpq(findmin_cpq_eff(triple(U,V,W))) ) )).
%------------------------------------------------------------------------------
