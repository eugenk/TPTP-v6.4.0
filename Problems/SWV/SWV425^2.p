%------------------------------------------------------------------------------
% File     : SWV425^2 : TPTP v6.4.0. Released v3.6.0.
% Domain   : Software Verification (Security)
% Problem  : ICL logic mapping to modal logic implies 'unit'
% Version  : [Ben08] axioms : Augmented.
% English  :

% Refs     : [GA08]  Garg & Abadi (2008), A Modal Deconstruction of Access
%          : [Ben08] Benzmueller (2008), Automating Access Control Logics i
%          : [BP09]  Benzmueller & Paulson (2009), Exploring Properties of
% Source   : [Ben08]
% Names    :

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.29 v6.1.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.1.0, 0.60 v5.0.0, 0.20 v4.1.0, 0.00 v4.0.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   59 (   0 unit;  32 type;  24 defn)
%            Number of atoms       :  144 (  24 equality;  55 variable)
%            Maximal formula depth :    9 (   5 average)
%            Number of connectives :   72 (   3   ~;   1   |;   2   &;  65   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  126 ( 126   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   37 (  32   :;   0   =)
%            Number of variables   :   49 (   2 sgn;   6   !;   4   ?;  39   ^)
%                                         (  49   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include axioms of multi modal logic
include('Axioms/LCL008^0.ax').
%----Include axioms of ICL logic
include('Axioms/SWV008^0.ax').
%----Include axioms for ICL notions of validity wrt S4
include('Axioms/SWV008^1.ax').
%------------------------------------------------------------------------------
%----We introduce an arbitrary atom s
thf(s,type,(
    s: $i > $o )).

%----We introduce an arbitrary principal a
thf(a,type,(
    a: $i > $o )).

%----Can we prove 'unit'?
thf(unit,conjecture,
    ( iclval @ ( icl_impl @ ( icl_atom @ s ) @ ( icl_says @ ( icl_princ @ a ) @ ( icl_atom @ s ) ) ) )).

%------------------------------------------------------------------------------
