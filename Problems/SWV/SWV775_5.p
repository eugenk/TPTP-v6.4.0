%------------------------------------------------------------------------------
% File     : SWV775_5 : TPTP v6.4.0. Released v6.0.0.
% Domain   : Software Verification
% Problem  : Needham-Schroeder shared-key protocol line 398
% Version  : Especial.
% English  : 

% Refs     : [BN10]  Boehme & Nipkow (2010), Sledgehammer: Judgement Day
%          : [Bla13] Blanchette (2011), Email to Geoff Sutcliffe
% Source   : [Bla13]
% Names    : ns_398 [Bla13]

% Status   : Unknown
% Rating   : 1.00 v6.4.0
% Syntax   : Number of formulae    :  155 (  44 unit;  39 type)
%            Number of atoms       :  264 ( 148 equality)
%            Maximal formula depth :   16 (   6 average)
%            Number of connectives :  206 (  58   ~;  23   |;  35   &)
%                                         (  28 <=>;  62  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   39 (  22   >;  17   *;   0   +;   0  <<)
%            Number of predicates  :   48 (  43 propositional; 0-3 arity)
%            Number of functors    :   37 (  14 constant; 0-5 arity)
%            Number of variables   :  455 (   1 sgn; 396   !;  34   ?)
%                                         ( 455   :;  25  !>;   0  ?*)
%            Maximal term depth    :    7 (   2 average)
% SPC      : TF1_UNK_EQU_NAR

% Comments : This file was generated by Isabelle (most likely Sledgehammer)
%            2011-12-13 16:24:04
%------------------------------------------------------------------------------
%----Should-be-implicit typings (6)
tff(ty_tc_Event_Oevent,type,(
    event: $tType )).

tff(ty_tc_HOL_Obool,type,(
    bool: $tType )).

tff(ty_tc_List_Olist,type,(
    list: $tType > $tType )).

tff(ty_tc_Message_Oagent,type,(
    agent: $tType )).

tff(ty_tc_Message_Omsg,type,(
    msg: $tType )).

tff(ty_tc_fun,type,(
    fun: ( $tType * $tType ) > $tType )).

%----Explicit typings (33)
tff(sy_c_COMBB,type,(
    combb: 
      !>[B: $tType,C: $tType,A: $tType] :
        ( ( fun(B,C) * fun(A,B) ) > fun(A,C) ) )).

tff(sy_c_COMBC,type,(
    combc: 
      !>[A: $tType,B: $tType,C: $tType] :
        ( ( fun(A,fun(B,C)) * B ) > fun(A,C) ) )).

tff(sy_c_COMBS,type,(
    combs: 
      !>[A: $tType,B: $tType,C: $tType] :
        ( ( fun(A,fun(B,C)) * fun(A,B) ) > fun(A,C) ) )).

tff(sy_c_Event_Obad,type,(
    bad: fun(agent,bool) )).

tff(sy_c_Event_Oevent_OGets,type,(
    gets: ( agent * msg ) > event )).

tff(sy_c_Event_Oevent_ONotes,type,(
    notes: ( agent * msg ) > event )).

tff(sy_c_Event_Oevent_OSays,type,(
    says: ( agent * agent * msg ) > event )).

tff(sy_c_Event_OinitState,type,(
    initState: agent > fun(msg,bool) )).

tff(sy_c_Event_Oknows,type,(
    knows: ( agent * list(event) ) > fun(msg,bool) )).

tff(sy_c_List_Oappend,type,(
    append: 
      !>[A: $tType] :
        ( ( list(A) * list(A) ) > list(A) ) )).

tff(sy_c_List_Oinsert,type,(
    insert1: 
      !>[A: $tType] :
        ( ( A * list(A) ) > list(A) ) )).

tff(sy_c_List_Olist_OCons,type,(
    cons: 
      !>[A: $tType] :
        ( ( A * list(A) ) > list(A) ) )).

tff(sy_c_List_Olist_ONil,type,(
    nil: 
      !>[A: $tType] : list(A) )).

tff(sy_c_List_Omaps,type,(
    maps: 
      !>[A: $tType,B: $tType] :
        ( ( fun(A,list(B)) * list(A) ) > list(B) ) )).

tff(sy_c_List_Orotate1,type,(
    rotate1: 
      !>[A: $tType] :
        ( list(A) > list(A) ) )).

tff(sy_c_List_Oset,type,(
    set: 
      !>[A: $tType] :
        ( list(A) > fun(A,bool) ) )).

tff(sy_c_List_Osplice,type,(
    splice: 
      !>[A: $tType] :
        ( ( list(A) * list(A) ) > list(A) ) )).

tff(sy_c_Message_Oagent_OSpy,type,(
    spy: agent )).

tff(sy_c_Orderings_Oord__class_Oless__eq,type,(
    ord_less_eq: 
      !>[A: $tType] :
        ( ( A * A ) > $o ) )).

tff(sy_c_Set_OCollect,type,(
    collect: 
      !>[A: $tType] :
        ( fun(A,bool) > fun(A,bool) ) )).

tff(sy_c_Set_Oinsert,type,(
    insert: 
      !>[A: $tType] :
        ( ( A * fun(A,bool) ) > fun(A,bool) ) )).

tff(sy_c_aa,type,(
    aa: 
      !>[A: $tType,B: $tType] :
        ( ( fun(A,B) * A ) > B ) )).

tff(sy_c_fFalse,type,(
    fFalse: bool )).

tff(sy_c_fNot,type,(
    fNot: fun(bool,bool) )).

tff(sy_c_fTrue,type,(
    fTrue: bool )).

tff(sy_c_fdisj,type,(
    fdisj: fun(bool,fun(bool,bool)) )).

tff(sy_c_fequal,type,(
    fequal: 
      !>[A: $tType] : fun(A,fun(A,bool)) )).

tff(sy_c_fimplies,type,(
    fimplies: fun(bool,fun(bool,bool)) )).

tff(sy_c_member,type,(
    member: 
      !>[A: $tType] : fun(A,fun(fun(A,bool),bool)) )).

tff(sy_c_pp,type,(
    pp: bool > $o )).

tff(sy_v_A,type,(
    a: agent )).

tff(sy_v_X,type,(
    x: msg )).

tff(sy_v_evs,type,(
    evs: list(event) )).

%----Relevant facts (100)
tff(fact_0_knows__Spy__Notes,axiom,(
    ! [Evsa: list(event),Xa: msg,Aa: agent] :
      ( ( pp(aa(fun(agent,bool),bool,aa(agent,fun(fun(agent,bool),bool),member(agent),Aa),bad))
       => knows(spy,cons(event,notes(Aa,Xa),Evsa)) = insert(msg,Xa,knows(spy,Evsa)) )
      & ( ~ pp(aa(fun(agent,bool),bool,aa(agent,fun(fun(agent,bool),bool),member(agent),Aa),bad))
       => knows(spy,cons(event,notes(Aa,Xa),Evsa)) = knows(spy,Evsa) ) ) )).

tff(fact_1_append1__eq__conv,axiom,(
    ! [A: $tType,Y1: A,Ys3: list(A),X1: A,Xs: list(A)] :
      ( append(A,Xs,cons(A,X1,nil(A))) = append(A,Ys3,cons(A,Y1,nil(A)))
    <=> ( Xs = Ys3
        & X1 = Y1 ) ) )).

tff(fact_2_Spy__in__bad,axiom,(
    pp(aa(fun(agent,bool),bool,aa(agent,fun(fun(agent,bool),bool),member(agent),spy),bad)) )).

tff(fact_3_Nil__is__append__conv,axiom,(
    ! [A: $tType,Ys3: list(A),Xs: list(A)] :
      ( nil(A) = append(A,Xs,Ys3)
    <=> ( Xs = nil(A)
        & Ys3 = nil(A) ) ) )).

tff(fact_4_self__append__conv,axiom,(
    ! [A: $tType,Ys3: list(A),Xs: list(A)] :
      ( Xs = append(A,Xs,Ys3)
    <=> Ys3 = nil(A) ) )).

tff(fact_5_self__append__conv2,axiom,(
    ! [A: $tType,Xs: list(A),Ys3: list(A)] :
      ( Ys3 = append(A,Xs,Ys3)
    <=> Xs = nil(A) ) )).

tff(fact_6_append__is__Nil__conv,axiom,(
    ! [A: $tType,Ys3: list(A),Xs: list(A)] :
      ( append(A,Xs,Ys3) = nil(A)
    <=> ( Xs = nil(A)
        & Ys3 = nil(A) ) ) )).

tff(fact_7_append__self__conv,axiom,(
    ! [A: $tType,Ys3: list(A),Xs: list(A)] :
      ( append(A,Xs,Ys3) = Xs
    <=> Ys3 = nil(A) ) )).

tff(fact_8_append__self__conv2,axiom,(
    ! [A: $tType,Ys3: list(A),Xs: list(A)] :
      ( append(A,Xs,Ys3) = Ys3
    <=> Xs = nil(A) ) )).

tff(fact_9_append__Cons,axiom,(
    ! [A: $tType,Ys2: list(A),Xs1: list(A),X: A] : append(A,cons(A,X,Xs1),Ys2) = cons(A,X,append(A,Xs1,Ys2)) )).

tff(fact_10_knows__Notes,axiom,(
    ! [Evsa: list(event),Xa: msg,Aa: agent] : knows(Aa,cons(event,notes(Aa,Xa),Evsa)) = insert(msg,Xa,knows(Aa,Evsa)) )).

tff(fact_11_append__eq__Cons__conv,axiom,(
    ! [A: $tType,Xs: list(A),X1: A,Zs2: list(A),Ys3: list(A)] :
      ( append(A,Ys3,Zs2) = cons(A,X1,Xs)
    <=> ( ( Ys3 = nil(A)
          & Zs2 = cons(A,X1,Xs) )
        | ? [Ys4: list(A)] :
            ( Ys3 = cons(A,X1,Ys4)
            & append(A,Ys4,Zs2) = Xs ) ) ) )).

tff(fact_12_list_Oinject,axiom,(
    ! [A: $tType,List3: list(A),A5: A,List2: list(A),A2: A] :
      ( cons(A,A2,List2) = cons(A,A5,List3)
    <=> ( A2 = A5
        & List2 = List3 ) ) )).

tff(fact_13_append__same__eq,axiom,(
    ! [A: $tType,Zs2: list(A),Xs: list(A),Ys3: list(A)] :
      ( append(A,Ys3,Xs) = append(A,Zs2,Xs)
    <=> Ys3 = Zs2 ) )).

tff(fact_14_same__append__eq,axiom,(
    ! [A: $tType,Zs2: list(A),Ys3: list(A),Xs: list(A)] :
      ( append(A,Xs,Ys3) = append(A,Xs,Zs2)
    <=> Ys3 = Zs2 ) )).

tff(fact_15_append__assoc,axiom,(
    ! [A: $tType,Zs3: list(A),Ys2: list(A),Xs1: list(A)] : append(A,append(A,Xs1,Ys2),Zs3) = append(A,Xs1,append(A,Ys2,Zs3)) )).

tff(fact_16_event_Osimps_I3_J,axiom,(
    ! [Msg3: msg,Agent5: agent,Msg2: msg,Agent4: agent] :
      ( notes(Agent4,Msg2) = notes(Agent5,Msg3)
    <=> ( Agent4 = Agent5
        & Msg2 = Msg3 ) ) )).

tff(fact_17_not__Cons__self2,axiom,(
    ! [A: $tType,Xs1: list(A),X: A] : cons(A,X,Xs1) != Xs1 )).

tff(fact_18_not__Cons__self,axiom,(
    ! [A: $tType,X: A,Xs1: list(A)] : Xs1 != cons(A,X,Xs1) )).

tff(fact_19_append__eq__appendI,axiom,(
    ! [A: $tType,Us1: list(A),Ys2: list(A),Zs3: list(A),Xs11: list(A),Xs1: list(A)] :
      ( append(A,Xs1,Xs11) = Zs3
     => ( Ys2 = append(A,Xs11,Us1)
       => append(A,Xs1,Ys2) = append(A,Zs3,Us1) ) ) )).

tff(fact_20_append__eq__append__conv2,axiom,(
    ! [A: $tType,Ts: list(A),Zs2: list(A),Ys3: list(A),Xs: list(A)] :
      ( append(A,Xs,Ys3) = append(A,Zs2,Ts)
    <=> ? [Us: list(A)] :
          ( ( Xs = append(A,Zs2,Us)
            & append(A,Us,Ys3) = Ts )
          | ( append(A,Xs,Us) = Zs2
            & Ys3 = append(A,Us,Ts) ) ) ) )).

tff(fact_21_list_Osimps_I3_J,axiom,(
    ! [A: $tType,List1: list(A),A4: A] : cons(A,A4,List1) != nil(A) )).

tff(fact_22_list_Osimps_I2_J,axiom,(
    ! [A: $tType,List1: list(A),A4: A] : nil(A) != cons(A,A4,List1) )).

tff(fact_23_Cons__eq__appendI,axiom,(
    ! [A: $tType,Zs3: list(A),Xs1: list(A),Ys2: list(A),Xs11: list(A),X: A] :
      ( cons(A,X,Xs11) = Ys2
     => ( Xs1 = append(A,Xs11,Zs3)
       => cons(A,X,Xs1) = append(A,Ys2,Zs3) ) ) )).

tff(fact_24_eq__Nil__appendI,axiom,(
    ! [A: $tType,Ys2: list(A),Xs1: list(A)] :
      ( Xs1 = Ys2
     => Xs1 = append(A,nil(A),Ys2) ) )).

tff(fact_25_append__Nil2,axiom,(
    ! [A: $tType,Xs1: list(A)] : append(A,Xs1,nil(A)) = Xs1 )).

tff(fact_26_append__Nil,axiom,(
    ! [A: $tType,Ys2: list(A)] : append(A,nil(A),Ys2) = Ys2 )).

tff(fact_27_Cons__eq__append__conv,axiom,(
    ! [A: $tType,Zs2: list(A),Ys3: list(A),Xs: list(A),X1: A] :
      ( cons(A,X1,Xs) = append(A,Ys3,Zs2)
    <=> ( ( Ys3 = nil(A)
          & cons(A,X1,Xs) = Zs2 )
        | ? [Ys4: list(A)] :
            ( cons(A,X1,Ys4) = Ys3
            & Xs = append(A,Ys4,Zs2) ) ) ) )).

tff(fact_28_rev__cases,axiom,(
    ! [A: $tType,Xs1: list(A)] :
      ( Xs1 != nil(A)
     => ~ ! [Ys: list(A),Y3: A] : Xs1 != append(A,Ys,cons(A,Y3,nil(A))) ) )).

tff(fact_29_rev__induct,axiom,(
    ! [A: $tType,Xs: list(A),P1: fun(list(A),bool)] :
      ( pp(aa(list(A),bool,P1,nil(A)))
     => ( ! [X3: A,Xs2: list(A)] :
            ( pp(aa(list(A),bool,P1,Xs2))
           => pp(aa(list(A),bool,P1,append(A,Xs2,cons(A,X3,nil(A))))) )
       => pp(aa(list(A),bool,P1,Xs)) ) ) )).

tff(fact_30_spies__Gets__rev,axiom,(
    ! [Xa: msg,Aa: agent,Evsa: list(event)] : knows(spy,append(event,Evsa,cons(event,gets(Aa,Xa),nil(event)))) = knows(spy,Evsa) )).

tff(fact_31_insertCI,axiom,(
    ! [A: $tType,B3: A,B1: fun(A,bool),A2: A] :
      ( ( ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),B1))
       => A2 = B3 )
     => pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),insert(A,B3,B1))) ) )).

tff(fact_32_insertE,axiom,(
    ! [A: $tType,Aa: fun(A,bool),B3: A,A2: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),insert(A,B3,Aa)))
     => ( A2 != B3
       => pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),Aa)) ) ) )).

tff(fact_33_insert__iff,axiom,(
    ! [A: $tType,Aa: fun(A,bool),B3: A,A2: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),insert(A,B3,Aa)))
    <=> ( A2 = B3
        | pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),Aa)) ) ) )).

tff(fact_34_insert__absorb2,axiom,(
    ! [A: $tType,Aa: fun(A,bool),X1: A] : insert(A,X1,insert(A,X1,Aa)) = insert(A,X1,Aa) )).

tff(fact_35_list_Oexhaust,axiom,(
    ! [A: $tType,Y: list(A)] :
      ( Y != nil(A)
     => ~ ! [A3: A,List: list(A)] : Y != cons(A,A3,List) ) )).

tff(fact_36_neq__Nil__conv,axiom,(
    ! [A: $tType,Xs: list(A)] :
      ( Xs != nil(A)
    <=> ? [Y2: A,Ys1: list(A)] : Xs = cons(A,Y2,Ys1) ) )).

tff(fact_37_event_Osimps_I2_J,axiom,(
    ! [Msg3: msg,Agent5: agent,Msg2: msg,Agent4: agent] :
      ( gets(Agent4,Msg2) = gets(Agent5,Msg3)
    <=> ( Agent4 = Agent5
        & Msg2 = Msg3 ) ) )).

tff(fact_38_knows__Spy__Gets,axiom,(
    ! [Evsa: list(event),Xa: msg,Aa: agent] : knows(spy,cons(event,gets(Aa,Xa),Evsa)) = knows(spy,Evsa) )).

tff(fact_39_event_Osimps_I8_J,axiom,(
    ! [Msg: msg,Agent: agent,Msg1: msg,Agent3: agent] : gets(Agent3,Msg1) != notes(Agent,Msg) )).

tff(fact_40_event_Osimps_I9_J,axiom,(
    ! [Msg1: msg,Agent3: agent,Msg: msg,Agent: agent] : notes(Agent,Msg) != gets(Agent3,Msg1) )).

tff(fact_41_insertI1,axiom,(
    ! [A: $tType,B1: fun(A,bool),A2: A] : pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),insert(A,A2,B1))) )).

tff(fact_42_insert__compr,axiom,(
    ! [A: $tType,B1: fun(A,bool),A2: A] : insert(A,A2,B1) = collect(A,combs(A,bool,bool,combb(bool,fun(bool,bool),A,fdisj,combc(A,A,bool,fequal(A),A2)),combc(A,fun(A,bool),bool,member(A),B1))) )).

tff(fact_43_insert__Collect,axiom,(
    ! [A: $tType,P1: fun(A,bool),A2: A] : insert(A,A2,collect(A,P1)) = collect(A,combs(A,bool,bool,combb(bool,fun(bool,bool),A,fimplies,combb(bool,bool,A,fNot,combc(A,A,bool,fequal(A),A2))),P1)) )).

tff(fact_44_insert__commute,axiom,(
    ! [A: $tType,Aa: fun(A,bool),Y1: A,X1: A] : insert(A,X1,insert(A,Y1,Aa)) = insert(A,Y1,insert(A,X1,Aa)) )).

tff(fact_45_insert__code,axiom,(
    ! [A: $tType,X1: A,Aa: fun(A,bool),Y1: A] :
      ( pp(aa(A,bool,insert(A,Y1,Aa),X1))
    <=> ( Y1 = X1
        | pp(aa(A,bool,Aa,X1)) ) ) )).

tff(fact_46_insert__ident,axiom,(
    ! [A: $tType,B1: fun(A,bool),Aa: fun(A,bool),X1: A] :
      ( ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),Aa))
     => ( ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),B1))
       => ( insert(A,X1,Aa) = insert(A,X1,B1)
        <=> Aa = B1 ) ) ) )).

tff(fact_47_insert__eq__iff,axiom,(
    ! [A: $tType,B1: fun(A,bool),B3: A,Aa: fun(A,bool),A2: A] :
      ( ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),Aa))
     => ( ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),B3),B1))
       => ( insert(A,A2,Aa) = insert(A,B3,B1)
        <=> ( ( A2 = B3
             => Aa = B1 )
            & ( A2 != B3
             => ? [C2: fun(A,bool)] :
                  ( Aa = insert(A,B3,C2)
                  & ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),B3),C2))
                  & B1 = insert(A,A2,C2)
                  & ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),C2)) ) ) ) ) ) ) )).

tff(fact_48_insertI2,axiom,(
    ! [A: $tType,B3: A,B1: fun(A,bool),A2: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),B1))
     => pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),insert(A,B3,B1))) ) )).

tff(fact_49_insert__absorb,axiom,(
    ! [A: $tType,Aa: fun(A,bool),A2: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),A2),Aa))
     => insert(A,A2,Aa) = Aa ) )).

tff(fact_50_knows__Gets,axiom,(
    ! [Evsa: list(event),Xa: msg,Aa: agent] :
      ( Aa != spy
     => knows(Aa,cons(event,gets(Aa,Xa),Evsa)) = insert(msg,Xa,knows(Aa,Evsa)) ) )).

tff(fact_51_spies__Says__rev,axiom,(
    ! [Xa: msg,B1: agent,Aa: agent,Evsa: list(event)] : knows(spy,append(event,Evsa,cons(event,says(Aa,B1,Xa),nil(event)))) = insert(msg,Xa,knows(spy,Evsa)) )).

tff(fact_52_insert__Nil,axiom,(
    ! [A: $tType,X: A] : insert1(A,X,nil(A)) = cons(A,X,nil(A)) )).

tff(fact_53_knows__Nil,axiom,(
    ! [Aa: agent] : knows(Aa,nil(event)) = initState(Aa) )).

tff(fact_54_knows__Spy__Says,axiom,(
    ! [Evsa: list(event),Xa: msg,B1: agent,Aa: agent] : knows(spy,cons(event,says(Aa,B1,Xa),Evsa)) = insert(msg,Xa,knows(spy,Evsa)) )).

tff(fact_55_rotate__simps,axiom,(
    ! [A: $tType,B: $tType,Xs1: list(B),X: B] :
      ( rotate1(A,nil(A)) = nil(A)
      & rotate1(B,cons(B,X,Xs1)) = append(B,Xs1,cons(B,X,nil(B))) ) )).

tff(fact_56_event_Osimps_I1_J,axiom,(
    ! [Msg3: msg,Agent22: agent,Agent12: agent,Msg2: msg,Agent21: agent,Agent11: agent] :
      ( says(Agent11,Agent21,Msg2) = says(Agent12,Agent22,Msg3)
    <=> ( Agent11 = Agent12
        & Agent21 = Agent22
        & Msg2 = Msg3 ) ) )).

tff(fact_57_rotate1__is__Nil__conv,axiom,(
    ! [A: $tType,Xs: list(A)] :
      ( rotate1(A,Xs) = nil(A)
    <=> Xs = nil(A) ) )).

tff(fact_58_event_Osimps_I6_J,axiom,(
    ! [Msg: msg,Agent: agent,Msg1: msg,Agent2: agent,Agent1: agent] : says(Agent1,Agent2,Msg1) != notes(Agent,Msg) )).

tff(fact_59_event_Osimps_I7_J,axiom,(
    ! [Msg1: msg,Agent2: agent,Agent1: agent,Msg: msg,Agent: agent] : notes(Agent,Msg) != says(Agent1,Agent2,Msg1) )).

tff(fact_60_event_Osimps_I4_J,axiom,(
    ! [Msg: msg,Agent: agent,Msg1: msg,Agent2: agent,Agent1: agent] : says(Agent1,Agent2,Msg1) != gets(Agent,Msg) )).

tff(fact_61_event_Osimps_I5_J,axiom,(
    ! [Msg1: msg,Agent2: agent,Agent1: agent,Msg: msg,Agent: agent] : gets(Agent,Msg) != says(Agent1,Agent2,Msg1) )).

tff(fact_62_knows__Says,axiom,(
    ! [Evsa: list(event),Xa: msg,B1: agent,Aa: agent] : knows(Aa,cons(event,says(Aa,B1,Xa),Evsa)) = insert(msg,Xa,knows(Aa,Evsa)) )).

tff(fact_63_knows__imp__Says__Gets__Notes__initState,axiom,(
    ! [Evsa: list(event),Aa: agent,Xa: msg] :
      ( pp(aa(fun(msg,bool),bool,aa(msg,fun(fun(msg,bool),bool),member(msg),Xa),knows(Aa,Evsa)))
     => ( Aa != spy
       => ? [B2: agent] :
            ( pp(aa(fun(event,bool),bool,aa(event,fun(fun(event,bool),bool),member(event),says(Aa,B2,Xa)),set(event,Evsa)))
            | pp(aa(fun(event,bool),bool,aa(event,fun(fun(event,bool),bool),member(event),gets(Aa,Xa)),set(event,Evsa)))
            | pp(aa(fun(event,bool),bool,aa(event,fun(fun(event,bool),bool),member(event),notes(Aa,Xa)),set(event,Evsa)))
            | pp(aa(fun(msg,bool),bool,aa(msg,fun(fun(msg,bool),bool),member(msg),Xa),initState(Aa))) ) ) ) )).

tff(fact_64_maps__simps_I1_J,axiom,(
    ! [A: $tType,B: $tType,Xs: list(B),X1: B,F: fun(B,list(A))] : maps(B,A,F,cons(B,X1,Xs)) = append(A,aa(B,list(A),F,X1),maps(B,A,F,Xs)) )).

tff(fact_65_splice_Osimps_I2_J,axiom,(
    ! [A: $tType,Va: list(A),V: A] : splice(A,cons(A,V,Va),nil(A)) = cons(A,V,Va) )).

tff(fact_66_set__rotate1,axiom,(
    ! [A: $tType,Xs: list(A)] : set(A,rotate1(A,Xs)) = set(A,Xs) )).

tff(fact_67_splice_Osimps_I3_J,axiom,(
    ! [A: $tType,Ys2: list(A),Y: A,Xs1: list(A),X: A] : splice(A,cons(A,X,Xs1),cons(A,Y,Ys2)) = cons(A,X,cons(A,Y,splice(A,Xs1,Ys2))) )).

tff(fact_68_List_Oset_Osimps_I2_J,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] : set(A,cons(A,X1,Xs)) = insert(A,X1,set(A,Xs)) )).

tff(fact_69_List_Oset__insert,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] : set(A,insert1(A,X1,Xs)) = insert(A,X1,set(A,Xs)) )).

tff(fact_70_in__set__insert,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Xs)))
     => insert1(A,X1,Xs) = Xs ) )).

tff(fact_71_set__ConsD,axiom,(
    ! [A: $tType,Xs: list(A),X1: A,Y1: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),Y1),set(A,cons(A,X1,Xs))))
     => ( Y1 = X1
        | pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),Y1),set(A,Xs))) ) ) )).

tff(fact_72_Says__imp__knows,axiom,(
    ! [Evsa: list(event),Xa: msg,B1: agent,Aa: agent] :
      ( pp(aa(fun(event,bool),bool,aa(event,fun(fun(event,bool),bool),member(event),says(Aa,B1,Xa)),set(event,Evsa)))
     => pp(aa(fun(msg,bool),bool,aa(msg,fun(fun(msg,bool),bool),member(msg),Xa),knows(Aa,Evsa))) ) )).

tff(fact_73_Notes__imp__knows,axiom,(
    ! [Evsa: list(event),Xa: msg,Aa: agent] :
      ( pp(aa(fun(event,bool),bool,aa(event,fun(fun(event,bool),bool),member(event),notes(Aa,Xa)),set(event,Evsa)))
     => pp(aa(fun(msg,bool),bool,aa(msg,fun(fun(msg,bool),bool),member(msg),Xa),knows(Aa,Evsa))) ) )).

tff(fact_74_List_Oinsert__def,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] :
      ( ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Xs)))
       => insert1(A,X1,Xs) = Xs )
      & ( ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Xs)))
       => insert1(A,X1,Xs) = cons(A,X1,Xs) ) ) )).

tff(fact_75_ext,axiom,(
    ! [B: $tType,A: $tType,G: fun(A,B),F: fun(A,B)] :
      ( ! [X3: A] : aa(A,B,F,X3) = aa(A,B,G,X3)
     => F = G ) )).

tff(fact_76_mem__def,axiom,(
    ! [A: $tType,Aa: fun(A,bool),X1: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),Aa))
    <=> pp(aa(A,bool,Aa,X1)) ) )).

tff(fact_77_Collect__def,axiom,(
    ! [A: $tType,P1: fun(A,bool)] : collect(A,P1) = P1 )).

tff(fact_78_not__in__set__insert,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] :
      ( ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Xs)))
     => insert1(A,X1,Xs) = cons(A,X1,Xs) ) )).

tff(fact_79_splice_Osimps_I1_J,axiom,(
    ! [A: $tType,Ys2: list(A)] : splice(A,nil(A),Ys2) = Ys2 )).

tff(fact_80_splice__Nil2,axiom,(
    ! [A: $tType,Xs1: list(A)] : splice(A,Xs1,nil(A)) = Xs1 )).

tff(fact_81_maps__simps_I2_J,axiom,(
    ! [B: $tType,A: $tType,F: fun(B,list(A))] : maps(B,A,F,nil(B)) = nil(A) )).

tff(fact_82_Says__imp__spies,axiom,(
    ! [Evsa: list(event),Xa: msg,B1: agent,Aa: agent] :
      ( pp(aa(fun(event,bool),bool,aa(event,fun(fun(event,bool),bool),member(event),says(Aa,B1,Xa)),set(event,Evsa)))
     => pp(aa(fun(msg,bool),bool,aa(msg,fun(fun(msg,bool),bool),member(msg),Xa),knows(spy,Evsa))) ) )).

tff(fact_83_Gets__imp__knows__agents,axiom,(
    ! [Evsa: list(event),Xa: msg,Aa: agent] :
      ( Aa != spy
     => ( pp(aa(fun(event,bool),bool,aa(event,fun(fun(event,bool),bool),member(event),gets(Aa,Xa)),set(event,Evsa)))
       => pp(aa(fun(msg,bool),bool,aa(msg,fun(fun(msg,bool),bool),member(msg),Xa),knows(Aa,Evsa))) ) ) )).

tff(fact_84_Notes__imp__knows__Spy,axiom,(
    ! [Evsa: list(event),Xa: msg,Aa: agent] :
      ( pp(aa(fun(event,bool),bool,aa(event,fun(fun(event,bool),bool),member(event),notes(Aa,Xa)),set(event,Evsa)))
     => ( pp(aa(fun(agent,bool),bool,aa(agent,fun(fun(agent,bool),bool),member(agent),Aa),bad))
       => pp(aa(fun(msg,bool),bool,aa(msg,fun(fun(msg,bool),bool),member(msg),Xa),knows(spy,Evsa))) ) ) )).

tff(fact_85_knows__Spy__imp__Says__Notes__initState,axiom,(
    ! [Evsa: list(event),Xa: msg] :
      ( pp(aa(fun(msg,bool),bool,aa(msg,fun(fun(msg,bool),bool),member(msg),Xa),knows(spy,Evsa)))
     => ? [A1: agent,B2: agent] :
          ( pp(aa(fun(event,bool),bool,aa(event,fun(fun(event,bool),bool),member(event),says(A1,B2,Xa)),set(event,Evsa)))
          | pp(aa(fun(event,bool),bool,aa(event,fun(fun(event,bool),bool),member(event),notes(A1,Xa)),set(event,Evsa)))
          | pp(aa(fun(msg,bool),bool,aa(msg,fun(fun(msg,bool),bool),member(msg),Xa),initState(spy))) ) ) )).

tff(fact_86_split__list__propE,axiom,(
    ! [A: $tType,P1: fun(A,bool),Xs: list(A)] :
      ( ? [X4: A] :
          ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X4),set(A,Xs)))
          & pp(aa(A,bool,P1,X4)) )
     => ~ ! [Ys: list(A),X3: A] :
            ( ? [Zs: list(A)] : Xs = append(A,Ys,cons(A,X3,Zs))
           => ~ pp(aa(A,bool,P1,X3)) ) ) )).

tff(fact_87_in__set__conv__decomp__first,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Xs)))
    <=> ? [Ys1: list(A),Zs1: list(A)] :
          ( Xs = append(A,Ys1,cons(A,X1,Zs1))
          & ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Ys1))) ) ) )).

tff(fact_88_in__set__conv__decomp,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Xs)))
    <=> ? [Ys1: list(A),Zs1: list(A)] : Xs = append(A,Ys1,cons(A,X1,Zs1)) ) )).

tff(fact_89_in__set__conv__decomp__last,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Xs)))
    <=> ? [Ys1: list(A),Zs1: list(A)] :
          ( Xs = append(A,Ys1,cons(A,X1,Zs1))
          & ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Zs1))) ) ) )).

tff(fact_90_split__list__first__propE,axiom,(
    ! [A: $tType,P1: fun(A,bool),Xs: list(A)] :
      ( ? [X4: A] :
          ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X4),set(A,Xs)))
          & pp(aa(A,bool,P1,X4)) )
     => ~ ! [Ys: list(A),X3: A] :
            ( ? [Zs: list(A)] : Xs = append(A,Ys,cons(A,X3,Zs))
           => ( pp(aa(A,bool,P1,X3))
             => ~ ! [Xa2: A] :
                    ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),Xa2),set(A,Ys)))
                   => ~ pp(aa(A,bool,P1,Xa2)) ) ) ) ) )).

tff(fact_91_split__list__last__propE,axiom,(
    ! [A: $tType,P1: fun(A,bool),Xs: list(A)] :
      ( ? [X4: A] :
          ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X4),set(A,Xs)))
          & pp(aa(A,bool,P1,X4)) )
     => ~ ! [Ys: list(A),X3: A,Zs: list(A)] :
            ( Xs = append(A,Ys,cons(A,X3,Zs))
           => ( pp(aa(A,bool,P1,X3))
             => ~ ! [Xa2: A] :
                    ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),Xa2),set(A,Zs)))
                   => ~ pp(aa(A,bool,P1,Xa2)) ) ) ) ) )).

tff(fact_92_split__list__last__prop__iff,axiom,(
    ! [A: $tType,P1: fun(A,bool),Xs: list(A)] :
      ( ? [X2: A] :
          ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X2),set(A,Xs)))
          & pp(aa(A,bool,P1,X2)) )
    <=> ? [Ys1: list(A),X2: A,Zs1: list(A)] :
          ( Xs = append(A,Ys1,cons(A,X2,Zs1))
          & pp(aa(A,bool,P1,X2))
          & ! [Xa1: A] :
              ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),Xa1),set(A,Zs1)))
             => ~ pp(aa(A,bool,P1,Xa1)) ) ) ) )).

tff(fact_93_split__list__first__prop__iff,axiom,(
    ! [A: $tType,P1: fun(A,bool),Xs: list(A)] :
      ( ? [X2: A] :
          ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X2),set(A,Xs)))
          & pp(aa(A,bool,P1,X2)) )
    <=> ? [Ys1: list(A),X2: A] :
          ( ? [Zs1: list(A)] : Xs = append(A,Ys1,cons(A,X2,Zs1))
          & pp(aa(A,bool,P1,X2))
          & ! [Xa1: A] :
              ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),Xa1),set(A,Ys1)))
             => ~ pp(aa(A,bool,P1,Xa1)) ) ) ) )).

tff(fact_94_split__list__first,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Xs)))
     => ? [Ys: list(A),Zs: list(A)] :
          ( Xs = append(A,Ys,cons(A,X1,Zs))
          & ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Ys))) ) ) )).

tff(fact_95_split__list__last,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Xs)))
     => ? [Ys: list(A),Zs: list(A)] :
          ( Xs = append(A,Ys,cons(A,X1,Zs))
          & ~ pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Zs))) ) ) )).

tff(fact_96_split__list,axiom,(
    ! [A: $tType,Xs: list(A),X1: A] :
      ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),X1),set(A,Xs)))
     => ? [Ys: list(A),Zs: list(A)] : Xs = append(A,Ys,cons(A,X1,Zs)) ) )).

tff(fact_97_knows__Spy__subset__knows__Spy__Gets,axiom,(
    ! [Xa: msg,Aa: agent,Evsa: list(event)] : ord_less_eq(fun(msg,bool),knows(spy,Evsa),knows(spy,cons(event,gets(Aa,Xa),Evsa))) )).

tff(fact_98_subsetD,axiom,(
    ! [A: $tType,C1: A,B1: fun(A,bool),Aa: fun(A,bool)] :
      ( ord_less_eq(fun(A,bool),Aa,B1)
     => ( pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),C1),Aa))
       => pp(aa(fun(A,bool),bool,aa(A,fun(fun(A,bool),bool),member(A),C1),B1)) ) ) )).

tff(fact_99_equalityI,axiom,(
    ! [A: $tType,B1: fun(A,bool),Aa: fun(A,bool)] :
      ( ord_less_eq(fun(A,bool),Aa,B1)
     => ( ord_less_eq(fun(A,bool),B1,Aa)
       => Aa = B1 ) ) )).

%----Helper facts (15)
tff(help_pp_1_1_U,axiom,(
    ~ pp(fFalse) )).

tff(help_pp_2_1_U,axiom,(
    pp(fTrue) )).

tff(help_fNot_1_1_U,axiom,(
    ! [P: bool] :
      ( ~ pp(aa(bool,bool,fNot,P))
      | ~ pp(P) ) )).

tff(help_fNot_2_1_U,axiom,(
    ! [P: bool] :
      ( pp(P)
      | pp(aa(bool,bool,fNot,P)) ) )).

tff(help_COMBB_1_1_U,axiom,(
    ! [C: $tType,B: $tType,A: $tType,R: A,Q: fun(A,B),P: fun(B,C)] : aa(A,C,combb(B,C,A,P,Q),R) = aa(B,C,P,aa(A,B,Q,R)) )).

tff(help_COMBC_1_1_U,axiom,(
    ! [A: $tType,C: $tType,B: $tType,R: A,Q: B,P: fun(A,fun(B,C))] : aa(A,C,combc(A,B,C,P,Q),R) = aa(B,C,aa(A,fun(B,C),P,R),Q) )).

tff(help_COMBS_1_1_U,axiom,(
    ! [C: $tType,B: $tType,A: $tType,R: A,Q: fun(A,B),P: fun(A,fun(B,C))] : aa(A,C,combs(A,B,C,P,Q),R) = aa(B,C,aa(A,fun(B,C),P,R),aa(A,B,Q,R)) )).

tff(help_fdisj_1_1_U,axiom,(
    ! [Q: bool,P: bool] :
      ( ~ pp(P)
      | pp(aa(bool,bool,aa(bool,fun(bool,bool),fdisj,P),Q)) ) )).

tff(help_fdisj_2_1_U,axiom,(
    ! [P: bool,Q: bool] :
      ( ~ pp(Q)
      | pp(aa(bool,bool,aa(bool,fun(bool,bool),fdisj,P),Q)) ) )).

tff(help_fdisj_3_1_U,axiom,(
    ! [Q: bool,P: bool] :
      ( ~ pp(aa(bool,bool,aa(bool,fun(bool,bool),fdisj,P),Q))
      | pp(P)
      | pp(Q) ) )).

tff(help_fequal_1_1_T,axiom,(
    ! [A: $tType,Y: A,X: A] :
      ( ~ pp(aa(A,bool,aa(A,fun(A,bool),fequal(A),X),Y))
      | X = Y ) )).

tff(help_fequal_2_1_T,axiom,(
    ! [A: $tType,Y: A,X: A] :
      ( X != Y
      | pp(aa(A,bool,aa(A,fun(A,bool),fequal(A),X),Y)) ) )).

tff(help_fimplies_1_1_U,axiom,(
    ! [Q: bool,P: bool] :
      ( pp(P)
      | pp(aa(bool,bool,aa(bool,fun(bool,bool),fimplies,P),Q)) ) )).

tff(help_fimplies_2_1_U,axiom,(
    ! [P: bool,Q: bool] :
      ( ~ pp(Q)
      | pp(aa(bool,bool,aa(bool,fun(bool,bool),fimplies,P),Q)) ) )).

tff(help_fimplies_3_1_U,axiom,(
    ! [Q: bool,P: bool] :
      ( ~ pp(aa(bool,bool,aa(bool,fun(bool,bool),fimplies,P),Q))
      | ~ pp(P)
      | pp(Q) ) )).

%----Conjectures (1)
tff(conj_0,conjecture,
    ( ( pp(aa(fun(agent,bool),bool,aa(agent,fun(fun(agent,bool),bool),member(agent),a),bad))
     => knows(spy,append(event,evs,cons(event,notes(a,x),nil(event)))) = insert(msg,x,knows(spy,evs)) )
    & ( ~ pp(aa(fun(agent,bool),bool,aa(agent,fun(fun(agent,bool),bool),member(agent),a),bad))
     => knows(spy,append(event,evs,cons(event,notes(a,x),nil(event)))) = knows(spy,evs) ) )).

%------------------------------------------------------------------------------
