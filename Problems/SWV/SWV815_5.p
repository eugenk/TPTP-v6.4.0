%------------------------------------------------------------------------------
% File     : SWV815_5 : TPTP v6.4.0. Released v6.0.0.
% Domain   : Software Verification
% Problem  : Needham-Schroeder shared-key protocol line 502
% Version  : Especial.
% English  : 

% Refs     : [BN10]  Boehme & Nipkow (2010), Sledgehammer: Judgement Day
%          : [Bla13] Blanchette (2011), Email to Geoff Sutcliffe
% Source   : [Bla13]
% Names    : ns_502 [Bla13]

% Status   : Unknown
% Rating   : 1.00 v6.4.0
% Syntax   : Number of formulae    :  167 (  48 unit;  50 type)
%            Number of atoms       :  263 (  77 equality)
%            Maximal formula depth :   17 (   5 average)
%            Number of connectives :  223 (  77   ~;  10   |;  11   &)
%                                         (  18 <=>; 107  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   39 (  23   >;  16   *;   0   +;   0  <<)
%            Number of predicates  :   60 (  54 propositional; 0-4 arity)
%            Number of functors    :   47 (  26 constant; 0-5 arity)
%            Number of variables   :  374 (   0 sgn; 358   !;   2   ?)
%                                         ( 374   :;  14  !>;   0  ?*)
%            Maximal term depth    :   10 (   2 average)
% SPC      : TF1_UNK_EQU_NAR

% Comments : This file was generated by Isabelle (most likely Sledgehammer)
%            2011-12-13 16:32:32
%------------------------------------------------------------------------------
%----Should-be-implicit typings (7)
tff(ty_tc_Event_Oevent,type,(
    event: $tType )).

tff(ty_tc_HOL_Obool,type,(
    bool: $tType )).

tff(ty_tc_List_Olist,type,(
    list: $tType > $tType )).

tff(ty_tc_Message_Oagent,type,(
    agent1: $tType )).

tff(ty_tc_Message_Omsg,type,(
    msg: $tType )).

tff(ty_tc_Nat_Onat,type,(
    nat: $tType )).

tff(ty_tc_fun,type,(
    fun: ( $tType * $tType ) > $tType )).

%----Explicit typings (43)
tff(sy_c_COMBB,type,(
    combb: 
      !>[B: $tType,C: $tType,A: $tType] :
        ( ( fun(B,C) * fun(A,B) ) > fun(A,C) ) )).

tff(sy_c_COMBC,type,(
    combc: 
      !>[A: $tType,B: $tType,C: $tType] :
        ( ( fun(A,fun(B,C)) * B ) > fun(A,C) ) )).

tff(sy_c_Event_Obad,type,(
    bad: fun(agent1,bool) )).

tff(sy_c_Event_Oevent_ONotes,type,(
    notes: ( agent1 * msg ) > event )).

tff(sy_c_Event_Oevent_OSays,type,(
    says: ( agent1 * agent1 * msg ) > event )).

tff(sy_c_Event_Oknows,type,(
    knows: ( agent1 * list(event) ) > fun(msg,bool) )).

tff(sy_c_List_Olist_OCons,type,(
    cons: 
      !>[A: $tType] :
        ( ( A * list(A) ) > list(A) ) )).

tff(sy_c_List_Orev,type,(
    rev: 
      !>[A: $tType] :
        ( list(A) > list(A) ) )).

tff(sy_c_List_Oset,type,(
    set: 
      !>[A: $tType] :
        ( list(A) > fun(A,bool) ) )).

tff(sy_c_List_OtakeWhile,type,(
    takeWhile: 
      !>[A: $tType] :
        ( ( fun(A,bool) * list(A) ) > list(A) ) )).

tff(sy_c_Message_Oagent_OServer,type,(
    server: agent1 )).

tff(sy_c_Message_Oagent_OSpy,type,(
    spy: agent1 )).

tff(sy_c_Message_Oanalz,type,(
    analz: fun(msg,bool) > fun(msg,bool) )).

tff(sy_c_Message_Omsg_OAgent,type,(
    agent: agent1 > msg )).

tff(sy_c_Message_Omsg_OCrypt,type,(
    crypt: ( nat * msg ) > msg )).

tff(sy_c_Message_Omsg_OKey,type,(
    key: nat > msg )).

tff(sy_c_Message_Omsg_OMPair,type,(
    mPair: ( msg * msg ) > msg )).

tff(sy_c_Message_Omsg_ONonce,type,(
    nonce: nat > msg )).

tff(sy_c_Message_Oparts,type,(
    parts: fun(msg,bool) > fun(msg,bool) )).

tff(sy_c_Message_OsymKeys,type,(
    symKeys: fun(nat,bool) )).

tff(sy_c_NS__Shared__Mirabelle__iywubrjwsc_OIssues,type,(
    nS_Sha512322870Issues: ( agent1 * agent1 * msg * list(event) ) > $o )).

tff(sy_c_NS__Shared__Mirabelle__iywubrjwsc_Ons__shared,type,(
    nS_Sha254967238shared: fun(list(event),bool) )).

tff(sy_c_Public_OshrK,type,(
    shrK: agent1 > nat )).

tff(sy_c_aa,type,(
    aa1: 
      !>[A: $tType,B: $tType] :
        ( ( fun(A,B) * A ) > B ) )).

tff(sy_c_fFalse,type,(
    fFalse: bool )).

tff(sy_c_fNot,type,(
    fNot: fun(bool,bool) )).

tff(sy_c_fTrue,type,(
    fTrue: bool )).

tff(sy_c_fequal,type,(
    fequal: 
      !>[A: $tType] : fun(A,fun(A,bool)) )).

tff(sy_c_member,type,(
    member: 
      !>[A: $tType] :
        ( ( A * fun(A,bool) ) > $o ) )).

tff(sy_c_pp,type,(
    pp: bool > $o )).

tff(sy_v_A,type,(
    a: agent1 )).

tff(sy_v_Aa,type,(
    aa: agent1 )).

tff(sy_v_B,type,(
    b: agent1 )).

tff(sy_v_B_H,type,(
    b1: agent1 )).

tff(sy_v_Ba,type,(
    ba: agent1 )).

tff(sy_v_K,type,(
    k: nat )).

tff(sy_v_Ka,type,(
    ka: nat )).

tff(sy_v_NA,type,(
    na: nat )).

tff(sy_v_NB,type,(
    nb: nat )).

tff(sy_v_NBa,type,(
    nBa: nat )).

tff(sy_v_S,type,(
    s: agent1 )).

tff(sy_v_X,type,(
    x: msg )).

tff(sy_v_evs5,type,(
    evs5: list(event) )).

%----Relevant facts (100)
tff(fact_0_spies__evs__rev,axiom,(
    ! [Evsa: list(event)] : knows(spy,Evsa) = knows(spy,rev(event,Evsa)) )).

tff(fact_1_Spy__analz__shrK,axiom,(
    ! [Aa: agent1,Evsa: list(event)] :
      ( member(list(event),Evsa,nS_Sha254967238shared)
     => ( member(msg,key(shrK(Aa)),analz(knows(spy,Evsa)))
      <=> member(agent1,Aa,bad) ) ) )).

tff(fact_2_Spy__see__shrK,axiom,(
    ! [Aa: agent1,Evsa: list(event)] :
      ( member(list(event),Evsa,nS_Sha254967238shared)
     => ( member(msg,key(shrK(Aa)),parts(knows(spy,Evsa)))
      <=> member(agent1,Aa,bad) ) ) )).

tff(fact_3_A__trusts__NS5,axiom,(
    ! [X1: msg,Ba: agent1,NA: nat,Aa: agent1,Evsa: list(event),NBa: nat,Ka: nat] :
      ( member(msg,crypt(Ka,mPair(nonce(NBa),nonce(NBa))),parts(knows(spy,Evsa)))
     => ( member(msg,crypt(shrK(Aa),mPair(nonce(NA),mPair(agent(Ba),mPair(key(Ka),X1)))),parts(knows(spy,Evsa)))
       => ( ~ member(msg,key(Ka),analz(knows(spy,Evsa)))
         => ( ~ member(agent1,Aa,bad)
           => ( ~ member(agent1,Ba,bad)
             => ( member(list(event),Evsa,nS_Sha254967238shared)
               => member(event,says(Aa,Ba,crypt(Ka,mPair(nonce(NBa),nonce(NBa)))),set(event,Evsa)) ) ) ) ) ) ) )).

tff(fact_4_NS3__msg__in__parts__spies,axiom,(
    ! [Evsa: list(event),X1: msg,Ka: msg,Ba: msg,N: msg,KA: nat,Aa: agent1,S: agent1] :
      ( member(event,says(S,Aa,crypt(KA,mPair(N,mPair(Ba,mPair(Ka,X1))))),set(event,Evsa))
     => member(msg,X1,parts(knows(spy,Evsa))) ) )).

tff(fact_5_Spy__spies__bad__shrK,axiom,(
    ! [Evsa: list(event),Aa: agent1] :
      ( member(agent1,Aa,bad)
     => member(msg,key(shrK(Aa)),knows(spy,Evsa)) ) )).

tff(fact_6_A__authenticates__and__keydist__to__B,axiom,(
    ! [X1: msg,Ba: agent1,NA: msg,Aa: agent1,Evsa: list(event),NBa: nat,Ka: nat] :
      ( member(msg,crypt(Ka,nonce(NBa)),parts(knows(spy,Evsa)))
     => ( member(msg,crypt(shrK(Aa),mPair(NA,mPair(agent(Ba),mPair(key(Ka),X1)))),parts(knows(spy,Evsa)))
       => ( ~ member(msg,key(Ka),analz(knows(spy,Evsa)))
         => ( ~ member(agent1,Aa,bad)
           => ( ~ member(agent1,Ba,bad)
             => ( member(list(event),Evsa,nS_Sha254967238shared)
               => nS_Sha512322870Issues(Ba,Aa,crypt(Ka,nonce(NBa)),Evsa) ) ) ) ) ) ) )).

tff(fact_7_B__Issues__A,axiom,(
    ! [Evsa: list(event),Nb: nat,Ka: nat,Aa: agent1,Ba: agent1] :
      ( member(event,says(Ba,Aa,crypt(Ka,nonce(Nb))),set(event,Evsa))
     => ( ~ member(msg,key(Ka),analz(knows(spy,Evsa)))
       => ( ~ member(agent1,Aa,bad)
         => ( ~ member(agent1,Ba,bad)
           => ( member(list(event),Evsa,nS_Sha254967238shared)
             => nS_Sha512322870Issues(Ba,Aa,crypt(Ka,nonce(Nb)),Evsa) ) ) ) ) ) )).

tff(fact_8_shrK__in__knows,axiom,(
    ! [Evsa: list(event),Aa: agent1] : member(msg,key(shrK(Aa)),knows(Aa,Evsa)) )).

tff(fact_9_Crypt__Spy__analz__bad,axiom,(
    ! [Evsa: list(event),X1: msg,Aa: agent1] :
      ( member(msg,crypt(shrK(Aa),X1),analz(knows(spy,Evsa)))
     => ( member(agent1,Aa,bad)
       => member(msg,X1,analz(knows(spy,Evsa))) ) ) )).

tff(fact_10_sym__shrK,axiom,(
    ! [X1: agent1] : member(nat,shrK(X1),symKeys) )).

tff(fact_11_Spy__in__bad,axiom,(
    member(agent1,spy,bad) )).

tff(fact_12_Says__imp__parts__knows__Spy,axiom,(
    ! [Evsa: list(event),X1: msg,Ba: agent1,Aa: agent1] :
      ( member(event,says(Aa,Ba,X1),set(event,Evsa))
     => member(msg,X1,parts(knows(spy,Evsa))) ) )).

tff(fact_13_Says__imp__analz__Spy,axiom,(
    ! [Evsa: list(event),X1: msg,Ba: agent1,Aa: agent1] :
      ( member(event,says(Aa,Ba,X1),set(event,Evsa))
     => member(msg,X1,analz(knows(spy,Evsa))) ) )).

tff(fact_14_takeWhile__eq__all__conv,axiom,(
    ! [A: $tType,Xs: list(A),P1: fun(A,bool)] :
      ( takeWhile(A,P1,Xs) = Xs
    <=> ! [X4: A] :
          ( member(A,X4,set(A,Xs))
         => pp(aa1(A,bool,P1,X4)) ) ) )).

tff(fact_15_set__rev,axiom,(
    ! [A: $tType,Xs: list(A)] : set(A,rev(A,Xs)) = set(A,Xs) )).

tff(fact_16_MPair__parts,axiom,(
    ! [H: fun(msg,bool),Y2: msg,X1: msg] :
      ( member(msg,mPair(X1,Y2),parts(H))
     => ~ ( member(msg,X1,parts(H))
         => ~ member(msg,Y2,parts(H)) ) ) )).

tff(fact_17_analz_OInj,axiom,(
    ! [H: fun(msg,bool),X1: msg] :
      ( member(msg,X1,H)
     => member(msg,X1,analz(H)) ) )).

tff(fact_18_analz__idem,axiom,(
    ! [H: fun(msg,bool)] : analz(analz(H)) = analz(H) )).

tff(fact_19_msg_Osimps_I6_J,axiom,(
    ! [Msg24: msg,Msg14: msg,Msg23: msg,Msg13: msg] :
      ( mPair(Msg13,Msg23) = mPair(Msg14,Msg24)
    <=> ( Msg13 = Msg14
        & Msg23 = Msg24 ) ) )).

tff(fact_20_msg_Osimps_I4_J,axiom,(
    ! [Nat3: nat,Nat2: nat] :
      ( key(Nat2) = key(Nat3)
    <=> Nat2 = Nat3 ) )).

tff(fact_21_parts_OInj,axiom,(
    ! [H: fun(msg,bool),X1: msg] :
      ( member(msg,X1,H)
     => member(msg,X1,parts(H)) ) )).

tff(fact_22_parts__idem,axiom,(
    ! [H: fun(msg,bool)] : parts(parts(H)) = parts(H) )).

tff(fact_23_msg_Osimps_I7_J,axiom,(
    ! [Msg3: msg,Nat3: nat,Msg2: msg,Nat2: nat] :
      ( crypt(Nat2,Msg2) = crypt(Nat3,Msg3)
    <=> ( Nat2 = Nat3
        & Msg2 = Msg3 ) ) )).

tff(fact_24_msg_Osimps_I3_J,axiom,(
    ! [Nat3: nat,Nat2: nat] :
      ( nonce(Nat2) = nonce(Nat3)
    <=> Nat2 = Nat3 ) )).

tff(fact_25_msg_Osimps_I1_J,axiom,(
    ! [Agent4: agent1,Agent3: agent1] :
      ( agent(Agent3) = agent(Agent4)
    <=> Agent3 = Agent4 ) )).

tff(fact_26_event_Osimps_I1_J,axiom,(
    ! [Msg3: msg,Agent22: agent1,Agent12: agent1,Msg2: msg,Agent21: agent1,Agent11: agent1] :
      ( says(Agent11,Agent21,Msg2) = says(Agent12,Agent22,Msg3)
    <=> ( Agent11 = Agent12
        & Agent21 = Agent22
        & Msg2 = Msg3 ) ) )).

tff(fact_27_shrK__injective,axiom,(
    ! [Y1: agent1,X3: agent1] :
      ( shrK(X3) = shrK(Y1)
    <=> X3 = Y1 ) )).

tff(fact_28_rev__is__rev__conv,axiom,(
    ! [A: $tType,Ys: list(A),Xs: list(A)] :
      ( rev(A,Xs) = rev(A,Ys)
    <=> Xs = Ys ) )).

tff(fact_29_MPair__analz,axiom,(
    ! [H: fun(msg,bool),Y2: msg,X1: msg] :
      ( member(msg,mPair(X1,Y2),analz(H))
     => ~ ( member(msg,X1,analz(H))
         => ~ member(msg,Y2,analz(H)) ) ) )).

tff(fact_30_analz__disj__parts,axiom,(
    ! [H: fun(msg,bool),X1: msg] :
      ( ( member(msg,X1,analz(H))
        | member(msg,X1,parts(H)) )
    <=> member(msg,X1,parts(H)) ) )).

tff(fact_31_analz__conj__parts,axiom,(
    ! [H: fun(msg,bool),X1: msg] :
      ( ( member(msg,X1,analz(H))
        & member(msg,X1,parts(H)) )
    <=> member(msg,X1,analz(H)) ) )).

tff(fact_32_parts__analz,axiom,(
    ! [H: fun(msg,bool)] : parts(analz(H)) = parts(H) )).

tff(fact_33_analz__parts,axiom,(
    ! [H: fun(msg,bool)] : analz(parts(H)) = parts(H) )).

tff(fact_34_analz__analzD,axiom,(
    ! [H: fun(msg,bool),X1: msg] :
      ( member(msg,X1,analz(analz(H)))
     => member(msg,X1,analz(H)) ) )).

tff(fact_35_parts__partsD,axiom,(
    ! [H: fun(msg,bool),X1: msg] :
      ( member(msg,X1,parts(parts(H)))
     => member(msg,X1,parts(H)) ) )).

tff(fact_36_rev__swap,axiom,(
    ! [A: $tType,Ys: list(A),Xs: list(A)] :
      ( rev(A,Xs) = Ys
    <=> Xs = rev(A,Ys) ) )).

tff(fact_37_rev__rev__ident,axiom,(
    ! [A: $tType,Xs1: list(A)] : rev(A,rev(A,Xs1)) = Xs1 )).

tff(fact_38_symKeys__neq__imp__neq,axiom,(
    ! [K: nat,Ka: nat] :
      ( ~ ( member(nat,Ka,symKeys)
        <=> member(nat,K,symKeys) )
     => Ka != K ) )).

tff(fact_39_analz_OSnd,axiom,(
    ! [H: fun(msg,bool),Y2: msg,X1: msg] :
      ( member(msg,mPair(X1,Y2),analz(H))
     => member(msg,Y2,analz(H)) ) )).

tff(fact_40_analz_OFst,axiom,(
    ! [H: fun(msg,bool),Y2: msg,X1: msg] :
      ( member(msg,mPair(X1,Y2),analz(H))
     => member(msg,X1,analz(H)) ) )).

tff(fact_41_msg_Osimps_I41_J,axiom,(
    ! [Nat1: nat,Msg21: msg,Msg11: msg] : mPair(Msg11,Msg21) != key(Nat1) )).

tff(fact_42_msg_Osimps_I40_J,axiom,(
    ! [Msg21: msg,Msg11: msg,Nat1: nat] : key(Nat1) != mPair(Msg11,Msg21) )).

tff(fact_43_analz__into__parts,axiom,(
    ! [H: fun(msg,bool),C1: msg] :
      ( member(msg,C1,analz(H))
     => member(msg,C1,parts(H)) ) )).

tff(fact_44_not__parts__not__analz,axiom,(
    ! [H: fun(msg,bool),C1: msg] :
      ( ~ member(msg,C1,parts(H))
     => ~ member(msg,C1,analz(H)) ) )).

tff(fact_45_parts_OSnd,axiom,(
    ! [H: fun(msg,bool),Y2: msg,X1: msg] :
      ( member(msg,mPair(X1,Y2),parts(H))
     => member(msg,Y2,parts(H)) ) )).

tff(fact_46_parts_OFst,axiom,(
    ! [H: fun(msg,bool),Y2: msg,X1: msg] :
      ( member(msg,mPair(X1,Y2),parts(H))
     => member(msg,X1,parts(H)) ) )).

tff(fact_47_msg_Osimps_I48_J,axiom,(
    ! [Msg1: msg,Nat: nat,Msg22: msg,Msg12: msg] : mPair(Msg12,Msg22) != crypt(Nat,Msg1) )).

tff(fact_48_msg_Osimps_I49_J,axiom,(
    ! [Msg22: msg,Msg12: msg,Msg1: msg,Nat: nat] : crypt(Nat,Msg1) != mPair(Msg12,Msg22) )).

tff(fact_49_msg_Osimps_I43_J,axiom,(
    ! [Nat1: nat,Msg1: msg,Nat: nat] : crypt(Nat,Msg1) != key(Nat1) )).

tff(fact_50_msg_Osimps_I42_J,axiom,(
    ! [Msg1: msg,Nat: nat,Nat1: nat] : key(Nat1) != crypt(Nat,Msg1) )).

tff(fact_51_spies__partsEs_I2_J,axiom,(
    ! [H: fun(msg,bool),X1: msg,Ka: nat] :
      ( member(msg,crypt(Ka,X1),parts(H))
     => member(msg,X1,parts(H)) ) )).

tff(fact_52_parts_OBody,axiom,(
    ! [H: fun(msg,bool),X1: msg,Ka: nat] :
      ( member(msg,crypt(Ka,X1),parts(H))
     => member(msg,X1,parts(H)) ) )).

tff(fact_53_msg_Osimps_I35_J,axiom,(
    ! [Nat1: nat,Msg21: msg,Msg11: msg] : mPair(Msg11,Msg21) != nonce(Nat1) )).

tff(fact_54_msg_Osimps_I34_J,axiom,(
    ! [Msg21: msg,Msg11: msg,Nat1: nat] : nonce(Nat1) != mPair(Msg11,Msg21) )).

tff(fact_55_msg_Osimps_I30_J,axiom,(
    ! [Nat: nat,Nat1: nat] : nonce(Nat1) != key(Nat) )).

tff(fact_56_msg_Osimps_I31_J,axiom,(
    ! [Nat1: nat,Nat: nat] : key(Nat) != nonce(Nat1) )).

tff(fact_57_msg_Osimps_I17_J,axiom,(
    ! [Agent5: agent1,Msg21: msg,Msg11: msg] : mPair(Msg11,Msg21) != agent(Agent5) )).

tff(fact_58_msg_Osimps_I16_J,axiom,(
    ! [Msg21: msg,Msg11: msg,Agent5: agent1] : agent(Agent5) != mPair(Msg11,Msg21) )).

tff(fact_59_msg_Osimps_I12_J,axiom,(
    ! [Nat: nat,Agent5: agent1] : agent(Agent5) != key(Nat) )).

tff(fact_60_msg_Osimps_I13_J,axiom,(
    ! [Agent5: agent1,Nat: nat] : key(Nat) != agent(Agent5) )).

tff(fact_61_msg_Osimps_I37_J,axiom,(
    ! [Nat1: nat,Msg1: msg,Nat: nat] : crypt(Nat,Msg1) != nonce(Nat1) )).

tff(fact_62_msg_Osimps_I36_J,axiom,(
    ! [Msg1: msg,Nat: nat,Nat1: nat] : nonce(Nat1) != crypt(Nat,Msg1) )).

tff(fact_63_msg_Osimps_I19_J,axiom,(
    ! [Agent5: agent1,Msg1: msg,Nat: nat] : crypt(Nat,Msg1) != agent(Agent5) )).

tff(fact_64_msg_Osimps_I18_J,axiom,(
    ! [Msg1: msg,Nat: nat,Agent5: agent1] : agent(Agent5) != crypt(Nat,Msg1) )).

tff(fact_65_set__takeWhileD,axiom,(
    ! [A: $tType,Xs: list(A),P1: fun(A,bool),X3: A] :
      ( member(A,X3,set(A,takeWhile(A,P1,Xs)))
     => ( member(A,X3,set(A,Xs))
        & pp(aa1(A,bool,P1,X3)) ) ) )).

tff(fact_66_msg_Osimps_I11_J,axiom,(
    ! [Agent5: agent1,Nat: nat] : nonce(Nat) != agent(Agent5) )).

tff(fact_67_msg_Osimps_I10_J,axiom,(
    ! [Nat: nat,Agent5: agent1] : agent(Agent5) != nonce(Nat) )).

tff(fact_68_analz__impI,axiom,(
    ! [Q1: bool,Evsa: list(event),Y2: msg] :
      ( ( ~ member(msg,Y2,analz(knows(spy,Evsa)))
       => pp(Q1) )
     => ( ~ member(msg,Y2,analz(knows(spy,Evsa)))
       => pp(Q1) ) ) )).

tff(fact_69_Says__imp__knows,axiom,(
    ! [Evsa: list(event),X1: msg,Ba: agent1,Aa: agent1] :
      ( member(event,says(Aa,Ba,X1),set(event,Evsa))
     => member(msg,X1,knows(Aa,Evsa)) ) )).

tff(fact_70_analz__shrK__Decrypt,axiom,(
    ! [H: fun(msg,bool),X1: msg,Aa: agent1] :
      ( member(msg,crypt(shrK(Aa),X1),analz(H))
     => ( member(msg,key(shrK(Aa)),analz(H))
       => member(msg,X1,analz(H)) ) ) )).

tff(fact_71_Says__imp__spies,axiom,(
    ! [Evsa: list(event),X1: msg,Ba: agent1,Aa: agent1] :
      ( member(event,says(Aa,Ba,X1),set(event,Evsa))
     => member(msg,X1,knows(spy,Evsa)) ) )).

tff(fact_72_analz__Decrypt_H,axiom,(
    ! [H: fun(msg,bool),X1: msg,Ka: nat] :
      ( member(msg,crypt(Ka,X1),analz(H))
     => ( member(nat,Ka,symKeys)
       => ( member(msg,key(Ka),analz(H))
         => member(msg,X1,analz(H)) ) ) ) )).

tff(fact_73_B__trusts__NS5__lemma,axiom,(
    ! [NBa: nat,NA: msg,Aa: agent1,Ka: nat,Evsa: list(event),Ba: agent1] :
      ( ~ member(agent1,Ba,bad)
     => ( member(list(event),Evsa,nS_Sha254967238shared)
       => ( ~ member(msg,key(Ka),analz(knows(spy,Evsa)))
         => ( member(event,says(server,Aa,crypt(shrK(Aa),mPair(NA,mPair(agent(Ba),mPair(key(Ka),crypt(shrK(Ba),mPair(key(Ka),agent(Aa)))))))),set(event,Evsa))
           => ( member(msg,crypt(Ka,mPair(nonce(NBa),nonce(NBa))),parts(knows(spy,Evsa)))
             => member(event,says(Aa,Ba,crypt(Ka,mPair(nonce(NBa),nonce(NBa)))),set(event,Evsa)) ) ) ) ) ) )).

tff(fact_74_A__trusts__NS4__lemma,axiom,(
    ! [NBa: nat,X1: msg,Ba: agent1,NA: msg,Aa: agent1,Ka: nat,Evsa: list(event)] :
      ( member(list(event),Evsa,nS_Sha254967238shared)
     => ( ~ member(msg,key(Ka),analz(knows(spy,Evsa)))
       => ( member(event,says(server,Aa,crypt(shrK(Aa),mPair(NA,mPair(agent(Ba),mPair(key(Ka),X1))))),set(event,Evsa))
         => ( member(msg,crypt(Ka,nonce(NBa)),parts(knows(spy,Evsa)))
           => member(event,says(Ba,Aa,crypt(Ka,nonce(NBa))),set(event,Evsa)) ) ) ) ) )).

tff(fact_75_mem__def,axiom,(
    ! [A: $tType,Aa: fun(A,bool),X3: A] :
      ( member(A,X3,Aa)
    <=> pp(aa1(A,bool,Aa,X3)) ) )).

tff(fact_76_A__trusts__NS2,axiom,(
    ! [Evsa: list(event),X1: msg,Ka: nat,Ba: agent1,NA: msg,Aa: agent1] :
      ( member(msg,crypt(shrK(Aa),mPair(NA,mPair(agent(Ba),mPair(key(Ka),X1)))),parts(knows(spy,Evsa)))
     => ( ~ member(agent1,Aa,bad)
       => ( member(list(event),Evsa,nS_Sha254967238shared)
         => member(event,says(server,Aa,crypt(shrK(Aa),mPair(NA,mPair(agent(Ba),mPair(key(Ka),X1))))),set(event,Evsa)) ) ) ) )).

tff(fact_77_B__trusts__NS5,axiom,(
    ! [Aa: agent1,Ba: agent1,Evsa: list(event),NBa: nat,Ka: nat] :
      ( member(msg,crypt(Ka,mPair(nonce(NBa),nonce(NBa))),parts(knows(spy,Evsa)))
     => ( member(msg,crypt(shrK(Ba),mPair(key(Ka),agent(Aa))),parts(knows(spy,Evsa)))
       => ( ! [NA2: msg,NB: msg] : ~ member(event,notes(spy,mPair(NA2,mPair(NB,key(Ka)))),set(event,Evsa))
         => ( ~ member(agent1,Aa,bad)
           => ( ~ member(agent1,Ba,bad)
             => ( member(list(event),Evsa,nS_Sha254967238shared)
               => member(event,says(Aa,Ba,crypt(Ka,mPair(nonce(NBa),nonce(NBa)))),set(event,Evsa)) ) ) ) ) ) ) )).

tff(fact_78_A__trusts__NS4,axiom,(
    ! [X1: msg,Ba: agent1,NA: msg,Aa: agent1,Evsa: list(event),NBa: nat,Ka: nat] :
      ( member(msg,crypt(Ka,nonce(NBa)),parts(knows(spy,Evsa)))
     => ( member(msg,crypt(shrK(Aa),mPair(NA,mPair(agent(Ba),mPair(key(Ka),X1)))),parts(knows(spy,Evsa)))
       => ( ! [NB: msg] : ~ member(event,notes(spy,mPair(NA,mPair(NB,key(Ka)))),set(event,Evsa))
         => ( ~ member(agent1,Aa,bad)
           => ( ~ member(agent1,Ba,bad)
             => ( member(list(event),Evsa,nS_Sha254967238shared)
               => member(event,says(Ba,Aa,crypt(Ka,nonce(NBa))),set(event,Evsa)) ) ) ) ) ) ) )).

tff(fact_79_NS4__implies__NS3,axiom,(
    ! [NBa: nat,X1: msg,Ba: agent1,NA: msg,Aa: agent1,Ka: nat,Evsa: list(event)] :
      ( member(list(event),Evsa,nS_Sha254967238shared)
     => ( ~ member(msg,key(Ka),analz(knows(spy,Evsa)))
       => ( member(event,says(server,Aa,crypt(shrK(Aa),mPair(NA,mPair(agent(Ba),mPair(key(Ka),X1))))),set(event,Evsa))
         => ( member(msg,crypt(Ka,nonce(NBa)),parts(knows(spy,Evsa)))
           => ? [A4: agent1] : member(event,says(A4,Ba,X1),set(event,Evsa)) ) ) ) ) )).

tff(fact_80_B__trusts__NS3,axiom,(
    ! [Evsa: list(event),Aa: agent1,Ka: nat,Ba: agent1] :
      ( member(msg,crypt(shrK(Ba),mPair(key(Ka),agent(Aa))),parts(knows(spy,Evsa)))
     => ( ~ member(agent1,Ba,bad)
       => ( member(list(event),Evsa,nS_Sha254967238shared)
         => ? [NA2: msg] : member(event,says(server,Aa,crypt(shrK(Aa),mPair(NA2,mPair(agent(Ba),mPair(key(Ka),crypt(shrK(Ba),mPair(key(Ka),agent(Aa)))))))),set(event,Evsa)) ) ) ) )).

tff(fact_81_ns__shared_ONS5,axiom,(
    ! [X1: msg,Ba: agent1,NA: nat,S: agent1,NBa: nat,Aa: agent1,B1: agent1,Ka: nat,Evs5: list(event)] :
      ( member(list(event),Evs5,nS_Sha254967238shared)
     => ( member(nat,Ka,symKeys)
       => ( member(event,says(B1,Aa,crypt(Ka,nonce(NBa))),set(event,Evs5))
         => ( member(event,says(S,Aa,crypt(shrK(Aa),mPair(nonce(NA),mPair(agent(Ba),mPair(key(Ka),X1))))),set(event,Evs5))
           => member(list(event),cons(event,says(Aa,Ba,crypt(Ka,mPair(nonce(NBa),nonce(NBa)))),Evs5),nS_Sha254967238shared) ) ) ) ) )).

tff(fact_82_list_Oinject,axiom,(
    ! [A: $tType,List1: list(A),A3: A,List: list(A),A2: A] :
      ( cons(A,A2,List) = cons(A,A3,List1)
    <=> ( A2 = A3
        & List = List1 ) ) )).

tff(fact_83_event_Osimps_I3_J,axiom,(
    ! [Msg3: msg,Agent4: agent1,Msg2: msg,Agent3: agent1] :
      ( notes(Agent3,Msg2) = notes(Agent4,Msg3)
    <=> ( Agent3 = Agent4
        & Msg2 = Msg3 ) ) )).

tff(fact_84_not__Cons__self,axiom,(
    ! [A: $tType,X: A,Xs1: list(A)] : Xs1 != cons(A,X,Xs1) )).

tff(fact_85_not__Cons__self2,axiom,(
    ! [A: $tType,Xs1: list(A),X: A] : cons(A,X,Xs1) != Xs1 )).

tff(fact_86_set__ConsD,axiom,(
    ! [A: $tType,Xs: list(A),X3: A,Y1: A] :
      ( member(A,Y1,set(A,cons(A,X3,Xs)))
     => ( Y1 = X3
        | member(A,Y1,set(A,Xs)) ) ) )).

tff(fact_87_agent_Osimps_I5_J,axiom,(
    spy != server )).

tff(fact_88_agent_Osimps_I4_J,axiom,(
    server != spy )).

tff(fact_89_event_Osimps_I7_J,axiom,(
    ! [Msg: msg,Agent2: agent1,Agent1: agent1,Msg1: msg,Agent: agent1] : notes(Agent,Msg1) != says(Agent1,Agent2,Msg) )).

tff(fact_90_event_Osimps_I6_J,axiom,(
    ! [Msg1: msg,Agent: agent1,Msg: msg,Agent2: agent1,Agent1: agent1] : says(Agent1,Agent2,Msg) != notes(Agent,Msg1) )).

tff(fact_91_Server__not__bad,axiom,(
    ~ member(agent1,server,bad) )).

tff(fact_92_analz__mono__contra_I2_J,axiom,(
    ! [Evsa: list(event),X1: msg,Aa: agent1,C1: msg] :
      ( ~ member(msg,C1,analz(knows(spy,cons(event,notes(Aa,X1),Evsa))))
     => ~ member(msg,C1,analz(knows(spy,Evsa))) ) )).

tff(fact_93_Notes__imp__knows,axiom,(
    ! [Evsa: list(event),X1: msg,Aa: agent1] :
      ( member(event,notes(Aa,X1),set(event,Evsa))
     => member(msg,X1,knows(Aa,Evsa)) ) )).

tff(fact_94_ns__shared_OOops,axiom,(
    ! [X1: msg,NA: nat,NBa: nat,Ka: nat,Aa: agent1,Ba: agent1,Evso: list(event)] :
      ( member(list(event),Evso,nS_Sha254967238shared)
     => ( member(event,says(Ba,Aa,crypt(Ka,nonce(NBa))),set(event,Evso))
       => ( member(event,says(server,Aa,crypt(shrK(Aa),mPair(nonce(NA),mPair(agent(Ba),mPair(key(Ka),X1))))),set(event,Evso))
         => member(list(event),cons(event,notes(spy,mPair(nonce(NA),mPair(nonce(NBa),key(Ka)))),Evso),nS_Sha254967238shared) ) ) ) )).

tff(fact_95_analz__mono__contra_I1_J,axiom,(
    ! [Evsa: list(event),X1: msg,Ba: agent1,Aa: agent1,C1: msg] :
      ( ~ member(msg,C1,analz(knows(spy,cons(event,says(Aa,Ba,X1),Evsa))))
     => ~ member(msg,C1,analz(knows(spy,Evsa))) ) )).

tff(fact_96_ns__shared_ONS3,axiom,(
    ! [X1: msg,Ka: nat,Ba: agent1,NA: nat,S: agent1,Aa: agent1,Evs3: list(event)] :
      ( member(list(event),Evs3,nS_Sha254967238shared)
     => ( Aa != server
       => ( member(event,says(S,Aa,crypt(shrK(Aa),mPair(nonce(NA),mPair(agent(Ba),mPair(key(Ka),X1))))),set(event,Evs3))
         => ( member(event,says(Aa,server,mPair(agent(Aa),mPair(agent(Ba),nonce(NA)))),set(event,Evs3))
           => member(list(event),cons(event,says(Aa,Ba,X1),Evs3),nS_Sha254967238shared) ) ) ) ) )).

tff(fact_97_Notes__imp__knows__Spy,axiom,(
    ! [Evsa: list(event),X1: msg,Aa: agent1] :
      ( member(event,notes(Aa,X1),set(event,Evsa))
     => ( member(agent1,Aa,bad)
       => member(msg,X1,knows(spy,Evsa)) ) ) )).

tff(fact_98_Oops__parts__spies,axiom,(
    ! [Evsa: list(event),X1: msg,Ka: msg,Ba: msg,NA: msg,Aa: agent1] :
      ( member(event,says(server,Aa,crypt(shrK(Aa),mPair(NA,mPair(Ba,mPair(Ka,X1))))),set(event,Evsa))
     => member(msg,Ka,parts(knows(spy,Evsa))) ) )).

tff(fact_99_unique__session__keys,axiom,(
    ! [X2: msg,B1: agent1,NA1: msg,A1: agent1,Evsa: list(event),X1: msg,Ka: nat,Ba: agent1,NA: msg,Aa: agent1] :
      ( member(event,says(server,Aa,crypt(shrK(Aa),mPair(NA,mPair(agent(Ba),mPair(key(Ka),X1))))),set(event,Evsa))
     => ( member(event,says(server,A1,crypt(shrK(A1),mPair(NA1,mPair(agent(B1),mPair(key(Ka),X2))))),set(event,Evsa))
       => ( member(list(event),Evsa,nS_Sha254967238shared)
         => ( Aa = A1
            & NA = NA1
            & Ba = B1
            & X1 = X2 ) ) ) ) )).

%----Helper facts (8)
tff(help_pp_1_1_U,axiom,(
    ~ pp(fFalse) )).

tff(help_pp_2_1_U,axiom,(
    pp(fTrue) )).

tff(help_fNot_1_1_U,axiom,(
    ! [P: bool] :
      ( ~ pp(aa1(bool,bool,fNot,P))
      | ~ pp(P) ) )).

tff(help_fNot_2_1_U,axiom,(
    ! [P: bool] :
      ( pp(P)
      | pp(aa1(bool,bool,fNot,P)) ) )).

tff(help_COMBB_1_1_U,axiom,(
    ! [C: $tType,B: $tType,A: $tType,R: A,Q: fun(A,B),P: fun(B,C)] : aa1(A,C,combb(B,C,A,P,Q),R) = aa1(B,C,P,aa1(A,B,Q,R)) )).

tff(help_COMBC_1_1_U,axiom,(
    ! [A: $tType,C: $tType,B: $tType,R: A,Q: B,P: fun(A,fun(B,C))] : aa1(A,C,combc(A,B,C,P,Q),R) = aa1(B,C,aa1(A,fun(B,C),P,R),Q) )).

tff(help_fequal_1_1_T,axiom,(
    ! [A: $tType,Y: A,X: A] :
      ( ~ pp(aa1(A,bool,aa1(A,fun(A,bool),fequal(A),X),Y))
      | X = Y ) )).

tff(help_fequal_2_1_T,axiom,(
    ! [A: $tType,Y: A,X: A] :
      ( X != Y
      | pp(aa1(A,bool,aa1(A,fun(A,bool),fequal(A),X),Y)) ) )).

%----Conjectures (9)
tff(conj_0,hypothesis,(
    ~ member(agent1,a,bad) )).

tff(conj_1,hypothesis,(
    ~ member(agent1,b,bad) )).

tff(conj_2,hypothesis,(
    member(list(event),evs5,nS_Sha254967238shared) )).

tff(conj_3,hypothesis,(
    member(nat,ka,symKeys) )).

tff(conj_4,hypothesis,(
    member(event,says(b1,aa,crypt(ka,nonce(nBa))),set(event,evs5)) )).

tff(conj_5,hypothesis,(
    member(event,says(s,aa,crypt(shrK(aa),mPair(nonce(na),mPair(agent(ba),mPair(key(ka),x))))),set(event,evs5)) )).

tff(conj_6,hypothesis,(
    ~ member(msg,key(k),analz(knows(spy,evs5))) )).

tff(conj_7,hypothesis,
    ( member(event,says(a,b,crypt(k,mPair(nonce(nb),nonce(nb)))),set(event,evs5))
   => ~ member(msg,crypt(k,mPair(nonce(nb),nonce(nb))),parts(knows(spy,takeWhile(event,combb(bool,bool,event,fNot,combc(event,event,bool,fequal(event),says(a,b,crypt(k,mPair(nonce(nb),nonce(nb)))))),rev(event,evs5))))) )).

tff(conj_8,conjecture,
    ( a != aa
    | b != ba
    | k != ka
    | nb != nBa
    | ~ member(msg,crypt(ka,mPair(nonce(nBa),nonce(nBa))),parts(knows(spy,takeWhile(event,combb(bool,bool,event,fNot,combc(event,event,bool,fequal(event),says(aa,ba,crypt(ka,mPair(nonce(nBa),nonce(nBa)))))),rev(event,evs5))))) )).

%------------------------------------------------------------------------------
