%------------------------------------------------------------------------------
% File     : SWW431-1 : TPTP v6.4.0. Released v5.2.0.
% Domain   : Software Verification
% Problem  : Randomly generated entailment of the form F -> \bot (n = 13)
% Version  : Especial.
% English  : A randomly generated entailment with n program variables.
%            Negated equalities and list segments are added at random, with
%            specific paramenters so that about half of the generated
%            entailments are valid (or, equivalently, F is unsatisfiable).
%            Normalization and well-formedness axioms should be enough to
%            decide these entailments.

% Refs     : [RN11]  Rybalchenko & Navarro Perez (2011), Separation Logic +
%          : [Nav11] Navarro Perez (2011), Email to Geoff Sutcliffe
% Source   : [Nav11]
% Names    : spaguetti-13-e01 [Nav11]

% Status   : Unsatisfiable
% Rating   : 0.53 v6.3.0, 0.45 v6.2.0, 0.50 v6.1.0, 0.64 v6.0.0, 0.70 v5.5.0, 0.90 v5.4.0, 0.85 v5.3.0, 0.89 v5.2.0
% Syntax   : Number of clauses     :   25 (   3 non-Horn;  17 unit;  23 RR)
%            Number of atoms       :   36 (  21 equality)
%            Maximal clause size   :    3 (   1 average)
%            Number of predicates  :    2 (   0 propositional; 1-2 arity)
%            Number of functors    :   18 (  15 constant; 0-2 arity)
%            Number of variables   :   38 (   9 singleton)
%            Maximal term depth    :   14 (   2 average)
% SPC      : CNF_UNS_RFO_SEQ_NHN

% Comments :
%------------------------------------------------------------------------------
%----Include axioms for Lists in Separation Logic
include('Axioms/SWV013-0.ax').
%------------------------------------------------------------------------------
cnf(premise_1,hypothesis,
    ( x8 != x12 )).

cnf(premise_2,hypothesis,
    ( x6 != x7 )).

cnf(premise_3,hypothesis,
    ( x6 != x13 )).

cnf(premise_4,hypothesis,
    ( x3 != x8 )).

cnf(premise_5,hypothesis,
    ( x3 != x5 )).

cnf(premise_6,hypothesis,
    ( x7 != x8 )).

cnf(premise_7,hypothesis,
    ( x7 != x11 )).

cnf(premise_8,hypothesis,
    ( x10 != x11 )).

cnf(premise_9,hypothesis,
    ( x2 != x6 )).

cnf(premise_10,hypothesis,
    ( x2 != x3 )).

cnf(premise_11,hypothesis,
    ( x2 != x9 )).

cnf(premise_12,hypothesis,
    ( x5 != x7 )).

cnf(premise_13,hypothesis,
    ( heap(sep(lseg(x5,x3),sep(lseg(x10,x8),sep(lseg(x13,x8),sep(lseg(x1,x11),sep(lseg(x4,x9),sep(lseg(x2,x9),sep(lseg(x12,x7),sep(lseg(x9,x2),sep(lseg(x7,x10),sep(lseg(x11,x12),sep(lseg(x11,x7),sep(lseg(x11,x1),emp))))))))))))) )).

cnf(conclusion_1,negated_conjecture,
    ( x1 = x1
    | ~ heap(emp) )).

%------------------------------------------------------------------------------
