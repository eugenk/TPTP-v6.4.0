%------------------------------------------------------------------------------
% File     : SWW609=2 : TPTP v6.4.0. Released v6.1.0.
% Domain   : Software Verification
% Problem  : Isqrt-T-WP parameter sqrt
% Version  : Especial : Let and conditional terms encoded away.
% English  :

% Refs     : [Fil14] Filliatre (2014), Email to Geoff Sutcliffe
%          : [BF+]   Bobot et al. (URL), Toccata: Certified Programs and Cert
% Source   : [Fil14]
% Names    : isqrt-T-WP_parameter_sqrt [Fil14]

% Status   : Theorem
% Rating   : 0.57 v6.4.0, 0.67 v6.3.0, 0.86 v6.2.0, 0.75 v6.1.0
% Syntax   : Number of formulae    :   51 (   9 unit;  21 type)
%            Number of atoms       :   77 (  24 equality)
%            Maximal formula depth :   12 (   4 average)
%            Number of connectives :   57 (  10   ~;   1   |;  20   &)
%                                         (   1 <=>;  25  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   17 (   9   >;   8   *;   0   +;   0  <<)
%            Number of predicates  :   28 (  24 propositional; 0-2 arity)
%            Number of functors    :   18 (   7 constant; 0-4 arity)
%            Number of variables   :   59 (   0 sgn;  59   !;   0   ?)
%                                         (  59   :;   0  !>;   0  ?*)
%            Maximal term depth    :    4 (   1 average)
%            Arithmetic symbols    :   47 (   2 prd;   3 fun;   4 num;  38 var)
% SPC      : TF0_THM_EQU_ARI

% Comments :
%------------------------------------------------------------------------------
tff(uni,type,(
    uni: $tType )).

tff(ty,type,(
    ty: $tType )).

tff(sort,type,(
    sort1: ( ty * uni ) > $o )).

tff(witness,type,(
    witness1: ty > uni )).

tff(witness_sort1,axiom,(
    ! [A: ty] : sort1(A,witness1(A)) )).

tff(int,type,(
    int: ty )).

tff(real,type,(
    real: ty )).

tff(bool,type,(
    bool1: $tType )).

tff(bool1,type,(
    bool: ty )).

tff(true,type,(
    true1: bool1 )).

tff(false,type,(
    false1: bool1 )).

tff(match_bool,type,(
    match_bool1: ( ty * bool1 * uni * uni ) > uni )).

tff(match_bool_sort1,axiom,(
    ! [A: ty,X: bool1,X1: uni,X2: uni] : sort1(A,match_bool1(A,X,X1,X2)) )).

tff(match_bool_True,axiom,(
    ! [A: ty,Z: uni,Z1: uni] :
      ( sort1(A,Z)
     => match_bool1(A,true1,Z,Z1) = Z ) )).

tff(match_bool_False,axiom,(
    ! [A: ty,Z: uni,Z1: uni] :
      ( sort1(A,Z1)
     => match_bool1(A,false1,Z,Z1) = Z1 ) )).

tff(true_False,axiom,(
    true1 != false1 )).

tff(bool_inversion,axiom,(
    ! [U: bool1] :
      ( U = true1
      | U = false1 ) )).

tff(tuple0,type,(
    tuple02: $tType )).

tff(tuple01,type,(
    tuple0: ty )).

tff(tuple02,type,(
    tuple03: tuple02 )).

tff(tuple0_inversion,axiom,(
    ! [U: tuple02] : U = tuple03 )).

tff(qtmark,type,(
    qtmark: ty )).

tff(compatOrderMult,axiom,(
    ! [X: $int,Y: $int,Z: $int] :
      ( $lesseq(X,Y)
     => ( $lesseq(0,Z)
       => $lesseq($product(X,Z),$product(Y,Z)) ) ) )).

tff(abs,type,(
    abs1: $int > $int )).

tff(abs_def,axiom,(
    ! [X: $int] :
      ( ( $lesseq(0,X)
       => abs1(X) = X )
      & ( ~ $lesseq(0,X)
       => abs1(X) = $uminus(X) ) ) )).

tff(abs_le,axiom,(
    ! [X: $int,Y: $int] :
      ( $lesseq(abs1(X),Y)
    <=> ( $lesseq($uminus(Y),X)
        & $lesseq(X,Y) ) ) )).

tff(abs_pos,axiom,(
    ! [X: $int] : $lesseq(0,abs1(X)) )).

tff(div,type,(
    div1: ( $int * $int ) > $int )).

tff(mod,type,(
    mod1: ( $int * $int ) > $int )).

tff(div_mod,axiom,(
    ! [X: $int,Y: $int] :
      ( Y != 0
     => X = $sum($product(Y,div1(X,Y)),mod1(X,Y)) ) )).

tff(div_bound,axiom,(
    ! [X: $int,Y: $int] :
      ( ( $lesseq(0,X)
        & $less(0,Y) )
     => ( $lesseq(0,div1(X,Y))
        & $lesseq(div1(X,Y),X) ) ) )).

tff(mod_bound,axiom,(
    ! [X: $int,Y: $int] :
      ( Y != 0
     => ( $less($uminus(abs1(Y)),mod1(X,Y))
        & $less(mod1(X,Y),abs1(Y)) ) ) )).

tff(div_sign_pos,axiom,(
    ! [X: $int,Y: $int] :
      ( ( $lesseq(0,X)
        & $less(0,Y) )
     => $lesseq(0,div1(X,Y)) ) )).

tff(div_sign_neg,axiom,(
    ! [X: $int,Y: $int] :
      ( ( $lesseq(X,0)
        & $less(0,Y) )
     => $lesseq(div1(X,Y),0) ) )).

tff(mod_sign_pos,axiom,(
    ! [X: $int,Y: $int] :
      ( ( $lesseq(0,X)
        & Y != 0 )
     => $lesseq(0,mod1(X,Y)) ) )).

tff(mod_sign_neg,axiom,(
    ! [X: $int,Y: $int] :
      ( ( $lesseq(X,0)
        & Y != 0 )
     => $lesseq(mod1(X,Y),0) ) )).

tff(rounds_toward_zero,axiom,(
    ! [X: $int,Y: $int] :
      ( Y != 0
     => $lesseq(abs1($product(div1(X,Y),Y)),abs1(X)) ) )).

tff(div_1,axiom,(
    ! [X: $int] : div1(X,1) = X )).

tff(mod_1,axiom,(
    ! [X: $int] : mod1(X,1) = 0 )).

tff(div_inf,axiom,(
    ! [X: $int,Y: $int] :
      ( ( $lesseq(0,X)
        & $less(X,Y) )
     => div1(X,Y) = 0 ) )).

tff(mod_inf,axiom,(
    ! [X: $int,Y: $int] :
      ( ( $lesseq(0,X)
        & $less(X,Y) )
     => mod1(X,Y) = X ) )).

tff(div_mult,axiom,(
    ! [X: $int,Y: $int,Z: $int] :
      ( ( $less(0,X)
        & $lesseq(0,Y)
        & $lesseq(0,Z) )
     => div1($sum($product(X,Y),Z),X) = $sum(Y,div1(Z,X)) ) )).

tff(mod_mult,axiom,(
    ! [X: $int,Y: $int,Z: $int] :
      ( ( $less(0,X)
        & $lesseq(0,Y)
        & $lesseq(0,Z) )
     => mod1($sum($product(X,Y),Z),X) = mod1(Z,X) ) )).

tff(ref,type,(
    ref: ty > ty )).

tff(mk_ref,type,(
    mk_ref: ( ty * uni ) > uni )).

tff(mk_ref_sort1,axiom,(
    ! [A: ty,X: uni] : sort1(ref(A),mk_ref(A,X)) )).

tff(contents,type,(
    contents: ( ty * uni ) > uni )).

tff(contents_sort1,axiom,(
    ! [A: ty,X: uni] : sort1(A,contents(A,X)) )).

tff(contents_def1,axiom,(
    ! [A: ty,U: uni] :
      ( sort1(A,U)
     => contents(A,mk_ref(A,U)) = U ) )).

tff(ref_inversion1,axiom,(
    ! [A: ty,U: uni] :
      ( sort1(ref(A),U)
     => U = mk_ref(A,contents(A,U)) ) )).

tff(wP_parameter_sqrt,conjecture,(
    ! [X: $int] :
      ( $lesseq(0,X)
     => ( X != 0
       => ( ~ $lesseq(X,3)
         => ! [Z: $int,Y: $int] :
              ( ( $less(0,Z)
                & $less(0,Y)
                & Z = div1($sum(div1(X,Y),Y),2)
                & $less(X,$product($sum(Y,1),$sum(Y,1)))
                & $less(X,$product($sum(Z,1),$sum(Z,1))) )
             => ( ~ $less(Z,Y)
               => ( $lesseq($product(Y,Y),X)
                  & $less(X,$product($sum(Y,1),$sum(Y,1))) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
