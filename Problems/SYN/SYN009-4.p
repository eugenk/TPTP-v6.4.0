%--------------------------------------------------------------------------
% File     : SYN009-4 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Syntactic
% Problem  : A problem to demonstrate the usefulness of relevancy testing
% Version  : Especial.
% English  :

% Refs     : [He01]  UNSEARCHMO: Eliminating Redundant Search Space on Back
% Source   : [He01]
% Names    : Example 5.3 [He01]

% Status   : Unsatisfiable
% Rating   : 0.00 v2.5.0
% Syntax   : Number of clauses     :    9 (   2 non-Horn;   4 unit;   9 RR)
%            Number of atoms       :   21 (   0 equality)
%            Maximal clause size   :    6 (   2 average)
%            Number of predicates  :    6 (   0 propositional; 1-3 arity)
%            Number of functors    :    3 (   3 constant; 0-0 arity)
%            Number of variables   :   15 (   0 singleton)
%            Maximal term depth    :    1 (   1 average)
% SPC      : CNF_UNS_EPR

% Comments :
%--------------------------------------------------------------------------
cnf(clause_1,negated_conjecture,
    ( ~ p(X,Y,Z)
    | ~ m(X,Y,Z) )).

cnf(clause_2,negated_conjecture,
    ( ~ q(X,Y,Z)
    | ~ m(X,Y,Z) )).

cnf(clause_3,negated_conjecture,
    ( ~ r(X,Y,Z)
    | ~ m(X,Y,Z) )).

cnf(clause_4,negated_conjecture,
    ( s(a) )).

cnf(clause_5,negated_conjecture,
    ( s(b) )).

cnf(clause_6,negated_conjecture,
    ( s(c) )).

cnf(clause_7,negated_conjecture,
    ( ~ n(c,c,c) )).

cnf(clause_8,negated_conjecture,
    ( ~ s(X)
    | ~ s(Y)
    | ~ s(Z)
    | p(X,Y,Z)
    | q(X,Y,Z)
    | r(X,Y,Z) )).

cnf(clause_9,negated_conjecture,
    ( ~ s(X)
    | ~ s(Y)
    | ~ s(Z)
    | m(X,Y,Z)
    | n(X,Y,Z) )).

%--------------------------------------------------------------------------
