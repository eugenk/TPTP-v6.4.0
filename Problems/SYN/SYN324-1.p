%--------------------------------------------------------------------------
% File     : SYN324-1 : TPTP v6.4.0. Released v1.2.0.
% Domain   : Syntactic
% Problem  : Church problem 46.9 (1)
% Version  : Especial.
% English  :

% Refs     : [Chu56] Church (1956), Introduction to Mathematical Logic I
%          : [FL+93] Fermuller et al. (1993), Resolution Methods for the De
%          : [Tam94] Tammet (1994), Email to Geoff Sutcliffe.
%          : [Pel98] Peltier (1998), A New Method for Automated Finite Mode
% Source   : [Tam94]
% Names    : Ch9N1 [Tam94]
%          : 4.2.7 [Pel98]

% Status   : Satisfiable
% Rating   : 0.00 v5.4.0, 0.33 v5.3.0, 0.29 v5.0.0, 0.25 v4.1.0, 0.29 v4.0.0, 0.25 v3.5.0, 0.29 v3.4.0, 0.50 v3.3.0, 0.33 v3.2.0, 0.20 v3.1.0, 0.29 v2.7.0, 0.00 v2.6.0, 0.25 v2.5.0, 0.17 v2.4.0, 0.00 v2.2.0, 0.33 v2.1.0, 0.00 v2.0.0
% Syntax   : Number of clauses     :    4 (   1 non-Horn;   0 unit;   3 RR)
%            Number of atoms       :    8 (   0 equality)
%            Maximal clause size   :    2 (   2 average)
%            Number of predicates  :    1 (   0 propositional; 2-2 arity)
%            Number of functors    :    1 (   0 constant; 1-1 arity)
%            Number of variables   :    4 (   0 singleton)
%            Maximal term depth    :    2 (   2 average)
% SPC      : CNF_SAT_RFO_NEQ

% Comments : All the problems here can be decided by using a certain
%            completeness-preserving term ordering strategies. See [FL+93].
%          : The conversion from the full 1st order form in [Chu56]
%            to clause form was done by hand by Tanel Tammet.
%--------------------------------------------------------------------------
cnf(clause1,negated_conjecture,
    ( f(X,y(X))
    | ~ f(X,X) )).

cnf(clause2,negated_conjecture,
    ( ~ f(X,y(X))
    | f(X,X) )).

cnf(clause3,negated_conjecture,
    ( f(X,y(X))
    | f(y(X),y(X)) )).

cnf(clause4,negated_conjecture,
    ( ~ f(X,y(X))
    | ~ f(y(X),y(X)) )).

%--------------------------------------------------------------------------
