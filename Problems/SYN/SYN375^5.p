%------------------------------------------------------------------------------
% File     : SYN375^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-FO-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0832 [Bro09]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.17 v6.1.0, 0.00 v5.3.0, 0.25 v5.2.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type;   0 defn)
%            Number of atoms       :   12 (   0 equality;   6 variable)
%            Maximal formula depth :    6 (   4 average)
%            Number of connectives :   11 (   0   ~;   0   |;   1   &;   6   @)
%                                         (   1 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    1 (   1   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :;   0   =)
%            Number of variables   :    6 (   0 sgn;   3   !;   3   ?;   0   ^)
%                                         (   6   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cP,type,(
    cP: $i > $o )).

thf(cX2126_BUG,conjecture,
    ( ( ( ! [Xx: $i] :
            ( cP @ Xx )
       => ? [Xy: $i] :
            ( cP @ Xy ) )
      & ( ? [Xy: $i] :
            ( cP @ Xy )
       => ! [Xx: $i] :
            ( cP @ Xx ) ) )
   => ( ? [Xy: $i] :
          ( cP @ Xy )
    <=> ! [Xx: $i] :
          ( cP @ Xx ) ) )).

%------------------------------------------------------------------------------
