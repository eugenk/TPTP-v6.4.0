%------------------------------------------------------------------------------
% File     : SYN386^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem X2138
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0369 [Bro09]
%          : tps_0402 [Bro09]
%          : X2138 [TPS]
%          : THM82 [TPS]
%          : THM82A [TPS]

% Status   : Theorem
% Rating   : 0.25 v6.4.0, 0.29 v6.3.0, 0.33 v6.1.0, 0.17 v6.0.0, 0.00 v5.5.0, 0.20 v5.4.0, 0.25 v5.3.0, 0.50 v5.2.0, 0.25 v5.1.0, 0.50 v5.0.0, 0.25 v4.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :   34 (   0 equality;  24 variable)
%            Maximal formula depth :   15 (   7 average)
%            Number of connectives :   33 (   0   ~;   0   |;   3   &;  24   @)
%                                         (   0 <=>;   6  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    7 (   7   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :;   0   =)
%            Number of variables   :   17 (   0 sgn;  11   !;   6   ?;   0   ^)
%                                         (  17   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cD,type,(
    cD: $i > $i > $i > $o )).

thf(cF,type,(
    cF: $i > $i > $o )).

thf(cS,type,(
    cS: $i > $i > $o )).

thf(cX2138,conjecture,
    ( ( ! [Xx: $i] :
        ? [Xy: $i] :
          ( cF @ Xx @ Xy )
      & ? [Xx: $i] :
        ! [Xe: $i] :
        ? [Xn: $i] :
        ! [Xw: $i] :
          ( ( cS @ Xn @ Xw )
         => ( cD @ Xw @ Xx @ Xe ) )
      & ! [Xe: $i] :
        ? [Xd: $i] :
        ! [Xa: $i,Xb: $i] :
          ( ( cD @ Xa @ Xb @ Xd )
         => ! [Xy: $i,Xz: $i] :
              ( ( ( cF @ Xa @ Xy )
                & ( cF @ Xb @ Xz ) )
             => ( cD @ Xy @ Xz @ Xe ) ) ) )
   => ? [Xy: $i] :
      ! [Xe: $i] :
      ? [Xm: $i] :
      ! [Xw: $i] :
        ( ( cS @ Xm @ Xw )
       => ! [Xz: $i] :
            ( ( cF @ Xw @ Xz )
           => ( cD @ Xz @ Xy @ Xe ) ) ) )).

%------------------------------------------------------------------------------
