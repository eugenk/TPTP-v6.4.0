%--------------------------------------------------------------------------
% File     : SYN720-1 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Syntactic (Random Prolog Theory)
% Problem  : Synthetic domain theory for EBL
% Version  : [SE94] axioms : Especial.
% English  :

% Refs     : [SE94]  Segre & Elkan (1994), A High-Performance Explanation-B
% Source   : [SE94]
% Names    :

% Status   : Satisfiable
% Rating   : 0.00 v3.2.0, 0.17 v3.1.0, 0.00 v2.5.0
% Syntax   : Number of clauses     :  368 (   0 non-Horn;  38 unit; 361 RR)
%            Number of atoms       : 1059 (   0 equality)
%            Maximal clause size   :    5 (   3 average)
%            Number of predicates  :   48 (   0 propositional; 1-3 arity)
%            Number of functors    :    5 (   5 constant; 0-0 arity)
%            Number of variables   :  626 ( 160 singleton)
%            Maximal term depth    :    1 (   1 average)
% SPC      : CNF_SAT_EPR

% Comments :
%--------------------------------------------------------------------------
%----Include Synthetic domain theory for EBL
include('Axioms/SYN001-0.ax').
%--------------------------------------------------------------------------
