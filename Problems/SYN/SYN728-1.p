%--------------------------------------------------------------------------
% File     : SYN728-1 : TPTP v6.4.0. Released v2.5.0.
% Domain   : Syntactic
% Problem  : Peter Andrews Problem THM69
% Version  : Especial.
% English  :

% Refs     : [And97] Andrews (1994), Email to G. Sutcliffe
% Source   : [And97]
% Names    : THM69 [And97]

% Status   : Unsatisfiable
% Rating   : 0.12 v6.3.0, 0.14 v6.2.0, 0.00 v2.5.0
% Syntax   : Number of clauses     :    5 (   2 non-Horn;   0 unit;   2 RR)
%            Number of atoms       :   10 (   0 equality)
%            Maximal clause size   :    2 (   2 average)
%            Number of predicates  :    3 (   0 propositional; 1-2 arity)
%            Number of functors    :    4 (   1 constant; 0-2 arity)
%            Number of variables   :    7 (   3 singleton)
%            Maximal term depth    :    3 (   1 average)
% SPC      : CNF_UNS_RFO_NEQ_NHN

% Comments :
%--------------------------------------------------------------------------
cnf(thm69_1,negated_conjecture,
    ( p(C,C)
    | ~ p(A,B) )).

cnf(thm69_2,negated_conjecture,
    ( m(A)
    | p(A,sk1(A)) )).

cnf(thm69_3,negated_conjecture,
    ( q(f(A,sk1(A)))
    | p(A,sk1(A)) )).

cnf(thm69_4,negated_conjecture,
    ( ~ m(g(A))
    | ~ q(A) )).

cnf(thm69_5,negated_conjecture,
    ( ~ p(sk2,sk2)
    | ~ p(g(sk2),A) )).

%--------------------------------------------------------------------------
