%------------------------------------------------------------------------------
% File     : SYO030^1 : TPTP v6.4.0. Released v3.7.0.
% Domain   : Syntactic
% Problem  : Not every unary connective is the identity
% Version  : Especial.
% English  :

% Refs     : [BB05]  Benzmueller & Brown (2005), A Structured Set of Higher
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
% Source   : [Ben09]
% Names    : Example 26a [BB05]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.29 v6.1.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.40 v4.1.0, 0.33 v4.0.0, 0.67 v3.7.0
% Syntax   : Number of formulae    :    3 (   0 unit;   1 type;   1 defn)
%            Number of atoms       :   10 (   1 equality;   7 variable)
%            Maximal formula depth :    7 (   6 average)
%            Number of connectives :    7 (   1   ~;   0   |;   0   &;   5   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    2 (   1   :;   0   =)
%            Number of variables   :    5 (   0 sgn;   2   !;   1   ?;   2   ^)
%                                         (   5   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : Requires set comprehension
%          : 
%------------------------------------------------------------------------------
thf(leibeq_decl,type,(
    leibeq: $o > $o > $o )).

thf(leibeq,definition,
    ( leibeq
    = ( ^ [X: $o,Y: $o] :
        ! [P: $o > $o] :
          ( ( P @ X )
         => ( P @ Y ) ) ) )).

thf(conj,conjecture,(
    ~ ( ! [F: $o > $o] :
        ? [X: $o] :
          ( leibeq @ ( F @ X ) @ X ) ) )).

%------------------------------------------------------------------------------
