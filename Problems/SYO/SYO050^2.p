%------------------------------------------------------------------------------
% File     : SYO050^2 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Logic Calculi (Quantified multimodal logic)
% Problem  : Simple textbook example 7
% Version  : [Ben09] axioms.
%          : Theorem formulation : Accessibility relation not valid
% English  :

% Refs     : [Gol92] Goldblatt (1992), Logics of Time and Computation
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
% Source   : [Ben09]
% Names    : ex7a.p [Ben09]

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.29 v6.1.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.40 v5.3.0, 0.60 v4.1.0, 0.33 v4.0.1, 0.00 v4.0.0
% Syntax   : Number of formulae    :   64 (   0 unit;  32 type;  31 defn)
%            Number of atoms       :  229 (  36 equality; 133 variable)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :  130 (   5   ~;   4   |;   8   &; 105   @)
%                                         (   0 <=>;   8  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  171 ( 171   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   36 (  32   :;   0   =)
%            Number of variables   :   86 (   3 sgn;  29   !;   7   ?;  50   ^)
%                                         (  86   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include embedding of quantified multimodal logic in simple type theory
include('Axioms/LCL013^0.ax').
%------------------------------------------------------------------------------
thf(conj,conjecture,(
    ? [R: $i > $i > $o] :
      ~ ( mvalid
        @ ( mforall_prop
          @ ^ [A: $i > $o] :
              ( mimplies @ ( mbox @ R @ A ) @ A ) ) ) )).

%------------------------------------------------------------------------------
