%------------------------------------------------------------------------------
% File     : SYO064^4.001 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Logic Calculi (Intuitionistic logic)
% Problem  : ILTP Problem SYJ107+1.001
% Version  : [Goe33] axioms.
% English  :

% Refs     : [Goe33] Goedel (1933), An Interpretation of the Intuitionistic
%          : [Gol06] Goldblatt (2006), Mathematical Modal Logic: A View of
%          : [ROK06] Raths et al. (2006), The ILTP Problem Library for Intu
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
%          : [BP10]  Benzmueller & Paulson (2009), Exploring Properties of
% Source   : [Ben09]
% Names    : SYJ107+1.001 [ROK06]

% Status   : Theorem
% Rating   : 0.29 v6.4.0, 0.33 v6.3.0, 0.40 v6.2.0, 0.43 v6.1.0, 0.29 v5.5.0, 0.17 v5.4.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v4.0.0
% Syntax   : Number of formulae    :   47 (   0 unit;  23 type;  19 defn)
%            Number of atoms       :  132 (  19 equality;  48 variable)
%            Maximal formula depth :    8 (   5 average)
%            Number of connectives :   73 (   3   ~;   1   |;   2   &;  65   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   98 (  98   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   26 (  23   :;   0   =)
%            Number of variables   :   40 (   1 sgn;   7   !;   2   ?;  31   ^)
%                                         (  40   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This is an ILTP problem embedded in TH0
%------------------------------------------------------------------------------
include('Axioms/LCL010^0.ax').
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $i > $o )).

thf(b_type,type,(
    b: $i > $o )).

thf(c_type,type,(
    c: $i > $o )).

thf(axiom1,axiom,
    ( ivalid @ ( iatom @ c ) )).

thf(axiom2,axiom,
    ( ivalid @ ( ior @ ( ior @ ( iatom @ b ) @ ( iatom @ a ) ) @ ( iatom @ b ) ) )).

thf(con,conjecture,
    ( ivalid @ ( ior @ ( iatom @ a ) @ ( iand @ ( iatom @ b ) @ ( iatom @ c ) ) ) )).

%------------------------------------------------------------------------------
