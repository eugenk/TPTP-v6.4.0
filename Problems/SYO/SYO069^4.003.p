%------------------------------------------------------------------------------
% File     : SYO069^4.003 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Logic Calculi (Intuitionistic logic)
% Problem  : ILTP Problem SYJ205+1.003
% Version  : [Goe33] axioms.
% English  :

% Refs     : [Goe33] Goedel (1933), An Interpretation of the Intuitionistic
%          : [Gol06] Goldblatt (2006), Mathematical Modal Logic: A View of
%          : [ROK06] Raths et al. (2006), The ILTP Problem Library for Intu
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
%          : [BP10]  Benzmueller & Paulson (2009), Exploring Properties of
% Source   : [Ben09]
% Names    : SYJ205+1.003 [ROK06]

% Status   : Theorem
% Rating   : 0.86 v6.4.0, 0.83 v6.3.0, 0.80 v6.2.0, 1.00 v6.1.0, 0.86 v5.5.0, 1.00 v5.2.0, 0.80 v4.1.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :   51 (   0 unit;  29 type;  19 defn)
%            Number of atoms       :  201 (  19 equality;  48 variable)
%            Maximal formula depth :   16 (   5 average)
%            Number of connectives :  144 (   3   ~;   1   |;   2   &; 136   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  104 ( 104   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   32 (  29   :;   0   =)
%            Number of variables   :   40 (   1 sgn;   7   !;   2   ?;  31   ^)
%                                         (  40   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This is an ILTP problem embedded in TH0
%------------------------------------------------------------------------------
include('Axioms/LCL010^0.ax').
%------------------------------------------------------------------------------
thf(a0_type,type,(
    a0: $i > $o )).

thf(a1_type,type,(
    a1: $i > $o )).

thf(a2_type,type,(
    a2: $i > $o )).

thf(a3_type,type,(
    a3: $i > $o )).

thf(b0_type,type,(
    b0: $i > $o )).

thf(b1_type,type,(
    b1: $i > $o )).

thf(b2_type,type,(
    b2: $i > $o )).

thf(b3_type,type,(
    b3: $i > $o )).

thf(f_type,type,(
    f: $i > $o )).

thf(con,conjecture,
    ( ivalid @ ( iand @ ( iimplies @ ( iand @ ( iimplies @ ( iatom @ a0 ) @ ( iatom @ f ) ) @ ( iand @ ( iimplies @ ( iimplies @ ( iatom @ b3 ) @ ( iatom @ b0 ) ) @ ( iatom @ a3 ) ) @ ( iand @ ( iimplies @ ( iimplies @ ( iatom @ b0 ) @ ( iatom @ a1 ) ) @ ( iatom @ a0 ) ) @ ( iand @ ( iimplies @ ( iimplies @ ( iatom @ b1 ) @ ( iatom @ a2 ) ) @ ( iatom @ a1 ) ) @ ( iimplies @ ( iimplies @ ( iatom @ b2 ) @ ( iatom @ a3 ) ) @ ( iatom @ a2 ) ) ) ) ) ) @ ( iatom @ f ) ) @ ( iimplies @ ( iand @ ( iimplies @ ( iimplies @ ( iatom @ b2 ) @ ( iatom @ a3 ) ) @ ( iatom @ a2 ) ) @ ( iand @ ( iimplies @ ( iimplies @ ( iatom @ b1 ) @ ( iatom @ a2 ) ) @ ( iatom @ a1 ) ) @ ( iand @ ( iimplies @ ( iimplies @ ( iatom @ b0 ) @ ( iatom @ a1 ) ) @ ( iatom @ a0 ) ) @ ( iand @ ( iimplies @ ( iimplies @ ( iatom @ b3 ) @ ( iatom @ b0 ) ) @ ( iatom @ a3 ) ) @ ( iimplies @ ( iatom @ a0 ) @ ( iatom @ f ) ) ) ) ) ) @ ( iatom @ f ) ) ) )).

%------------------------------------------------------------------------------
