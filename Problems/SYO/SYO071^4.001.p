%------------------------------------------------------------------------------
% File     : SYO071^4.001 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Logic Calculi (Intuitionistic logic)
% Problem  : ILTP Problem SYJ207+1.001
% Version  : [Goe33] axioms.
% English  :

% Refs     : [Goe33] Goedel (1933), An Interpretation of the Intuitionistic
%          : [Gol06] Goldblatt (2006), Mathematical Modal Logic: A View of
%          : [ROK06] Raths et al. (2006), The ILTP Problem Library for Intu
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
%          : [BP10]  Benzmueller & Paulson (2009), Exploring Properties of
% Source   : [Ben09]
% Names    : SYJ207+1.001 [ROK06]

% Status   : CounterSatisfiable
% Rating   : 0.67 v6.2.0, 0.33 v5.4.0, 1.00 v5.0.0, 0.33 v4.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :   47 (   0 unit;  23 type;  19 defn)
%            Number of atoms       :  148 (  19 equality;  48 variable)
%            Maximal formula depth :    8 (   5 average)
%            Number of connectives :   89 (   3   ~;   1   |;   2   &;  81   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   98 (  98   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   26 (  23   :;   0   =)
%            Number of variables   :   40 (   1 sgn;   7   !;   2   ?;  31   ^)
%                                         (  40   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU_NAR

% Comments : This is an ILTP problem embedded in TH0
%          : In classical logic this is a Theorem.
%------------------------------------------------------------------------------
include('Axioms/LCL010^0.ax').
%------------------------------------------------------------------------------
thf(p0_type,type,(
    p0: $i > $o )).

thf(p1_type,type,(
    p1: $i > $o )).

thf(p2_type,type,(
    p2: $i > $o )).

thf(axiom1,axiom,
    ( ivalid @ ( iimplies @ ( iequiv @ ( iatom @ p1 ) @ ( iatom @ p2 ) ) @ ( iand @ ( iatom @ p1 ) @ ( iatom @ p2 ) ) ) )).

thf(axiom2,axiom,
    ( ivalid @ ( iimplies @ ( iequiv @ ( iatom @ p2 ) @ ( iatom @ p1 ) ) @ ( iand @ ( iatom @ p1 ) @ ( iatom @ p2 ) ) ) )).

thf(con,conjecture,
    ( ivalid @ ( ior @ ( iatom @ p0 ) @ ( ior @ ( iand @ ( iatom @ p1 ) @ ( iatom @ p2 ) ) @ ( inot @ ( iatom @ p0 ) ) ) ) )).

%------------------------------------------------------------------------------
