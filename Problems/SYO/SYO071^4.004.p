%------------------------------------------------------------------------------
% File     : SYO071^4.004 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Logic Calculi (Intuitionistic logic)
% Problem  : ILTP Problem SYJ207+1.004
% Version  : [Goe33] axioms.
% English  :

% Refs     : [Goe33] Goedel (1933), An Interpretation of the Intuitionistic
%          : [Gol06] Goldblatt (2006), Mathematical Modal Logic: A View of
%          : [ROK06] Raths et al. (2006), The ILTP Problem Library for Intu
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
%          : [BP10]  Benzmueller & Paulson (2009), Exploring Properties of
% Source   : [Ben09]
% Names    : SYJ207+1.004 [ROK06]

% Status   : CounterSatisfiable
% Rating   : 0.67 v5.4.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :   59 (   0 unit;  29 type;  19 defn)
%            Number of atoms       :  382 (  19 equality;  48 variable)
%            Maximal formula depth :   14 (   6 average)
%            Number of connectives :  317 (   3   ~;   1   |;   2   &; 309   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  104 ( 104   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   32 (  29   :;   0   =)
%            Number of variables   :   40 (   1 sgn;   7   !;   2   ?;  31   ^)
%                                         (  40   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU_NAR

% Comments : This is an ILTP problem embedded in TH0
%          : In classical logic this is a Theorem.
%------------------------------------------------------------------------------
include('Axioms/LCL010^0.ax').
%------------------------------------------------------------------------------
thf(p0_type,type,(
    p0: $i > $o )).

thf(p1_type,type,(
    p1: $i > $o )).

thf(p2_type,type,(
    p2: $i > $o )).

thf(p3_type,type,(
    p3: $i > $o )).

thf(p4_type,type,(
    p4: $i > $o )).

thf(p5_type,type,(
    p5: $i > $o )).

thf(p6_type,type,(
    p6: $i > $o )).

thf(p7_type,type,(
    p7: $i > $o )).

thf(p8_type,type,(
    p8: $i > $o )).

thf(axiom1,axiom,
    ( ivalid @ ( iimplies @ ( iequiv @ ( iatom @ p1 ) @ ( iatom @ p2 ) ) @ ( iand @ ( iatom @ p1 ) @ ( iand @ ( iatom @ p2 ) @ ( iand @ ( iatom @ p3 ) @ ( iand @ ( iatom @ p4 ) @ ( iand @ ( iatom @ p5 ) @ ( iand @ ( iatom @ p6 ) @ ( iand @ ( iatom @ p7 ) @ ( iatom @ p8 ) ) ) ) ) ) ) ) ) )).

thf(axiom2,axiom,
    ( ivalid @ ( iimplies @ ( iequiv @ ( iatom @ p2 ) @ ( iatom @ p3 ) ) @ ( iand @ ( iatom @ p1 ) @ ( iand @ ( iatom @ p2 ) @ ( iand @ ( iatom @ p3 ) @ ( iand @ ( iatom @ p4 ) @ ( iand @ ( iatom @ p5 ) @ ( iand @ ( iatom @ p6 ) @ ( iand @ ( iatom @ p7 ) @ ( iatom @ p8 ) ) ) ) ) ) ) ) ) )).

thf(axiom3,axiom,
    ( ivalid @ ( iimplies @ ( iequiv @ ( iatom @ p3 ) @ ( iatom @ p4 ) ) @ ( iand @ ( iatom @ p1 ) @ ( iand @ ( iatom @ p2 ) @ ( iand @ ( iatom @ p3 ) @ ( iand @ ( iatom @ p4 ) @ ( iand @ ( iatom @ p5 ) @ ( iand @ ( iatom @ p6 ) @ ( iand @ ( iatom @ p7 ) @ ( iatom @ p8 ) ) ) ) ) ) ) ) ) )).

thf(axiom4,axiom,
    ( ivalid @ ( iimplies @ ( iequiv @ ( iatom @ p4 ) @ ( iatom @ p5 ) ) @ ( iand @ ( iatom @ p1 ) @ ( iand @ ( iatom @ p2 ) @ ( iand @ ( iatom @ p3 ) @ ( iand @ ( iatom @ p4 ) @ ( iand @ ( iatom @ p5 ) @ ( iand @ ( iatom @ p6 ) @ ( iand @ ( iatom @ p7 ) @ ( iatom @ p8 ) ) ) ) ) ) ) ) ) )).

thf(axiom5,axiom,
    ( ivalid @ ( iimplies @ ( iequiv @ ( iatom @ p5 ) @ ( iatom @ p6 ) ) @ ( iand @ ( iatom @ p1 ) @ ( iand @ ( iatom @ p2 ) @ ( iand @ ( iatom @ p3 ) @ ( iand @ ( iatom @ p4 ) @ ( iand @ ( iatom @ p5 ) @ ( iand @ ( iatom @ p6 ) @ ( iand @ ( iatom @ p7 ) @ ( iatom @ p8 ) ) ) ) ) ) ) ) ) )).

thf(axiom6,axiom,
    ( ivalid @ ( iimplies @ ( iequiv @ ( iatom @ p6 ) @ ( iatom @ p7 ) ) @ ( iand @ ( iatom @ p1 ) @ ( iand @ ( iatom @ p2 ) @ ( iand @ ( iatom @ p3 ) @ ( iand @ ( iatom @ p4 ) @ ( iand @ ( iatom @ p5 ) @ ( iand @ ( iatom @ p6 ) @ ( iand @ ( iatom @ p7 ) @ ( iatom @ p8 ) ) ) ) ) ) ) ) ) )).

thf(axiom7,axiom,
    ( ivalid @ ( iimplies @ ( iequiv @ ( iatom @ p7 ) @ ( iatom @ p8 ) ) @ ( iand @ ( iatom @ p1 ) @ ( iand @ ( iatom @ p2 ) @ ( iand @ ( iatom @ p3 ) @ ( iand @ ( iatom @ p4 ) @ ( iand @ ( iatom @ p5 ) @ ( iand @ ( iatom @ p6 ) @ ( iand @ ( iatom @ p7 ) @ ( iatom @ p8 ) ) ) ) ) ) ) ) ) )).

thf(axiom8,axiom,
    ( ivalid @ ( iimplies @ ( iequiv @ ( iatom @ p8 ) @ ( iatom @ p1 ) ) @ ( iand @ ( iatom @ p1 ) @ ( iand @ ( iatom @ p2 ) @ ( iand @ ( iatom @ p3 ) @ ( iand @ ( iatom @ p4 ) @ ( iand @ ( iatom @ p5 ) @ ( iand @ ( iatom @ p6 ) @ ( iand @ ( iatom @ p7 ) @ ( iatom @ p8 ) ) ) ) ) ) ) ) ) )).

thf(con,conjecture,
    ( ivalid @ ( ior @ ( iatom @ p0 ) @ ( ior @ ( iand @ ( iatom @ p1 ) @ ( iand @ ( iatom @ p2 ) @ ( iand @ ( iatom @ p3 ) @ ( iand @ ( iatom @ p4 ) @ ( iand @ ( iatom @ p5 ) @ ( iand @ ( iatom @ p6 ) @ ( iand @ ( iatom @ p7 ) @ ( iatom @ p8 ) ) ) ) ) ) ) ) @ ( inot @ ( iatom @ p0 ) ) ) ) )).

%------------------------------------------------------------------------------
