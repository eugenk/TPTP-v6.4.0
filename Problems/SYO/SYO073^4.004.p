%------------------------------------------------------------------------------
% File     : SYO073^4.004 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Logic Calculi (Intuitionistic logic)
% Problem  : ILTP Problem SYJ209+1.004
% Version  : [Goe33] axioms.
% English  :

% Refs     : [Goe33] Goedel (1933), An Interpretation of the Intuitionistic
%          : [Gol06] Goldblatt (2006), Mathematical Modal Logic: A View of
%          : [ROK06] Raths et al. (2006), The ILTP Problem Library for Intu
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
%          : [BP10]  Benzmueller & Paulson (2009), Exploring Properties of
% Source   : [Ben09]
% Names    : SYJ209+1.004 [ROK06]

% Status   : CounterSatisfiable
% Rating   : 0.67 v5.4.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :   48 (   0 unit;  25 type;  19 defn)
%            Number of atoms       :  155 (  19 equality;  48 variable)
%            Maximal formula depth :   12 (   5 average)
%            Number of connectives :   97 (   3   ~;   1   |;   2   &;  89   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  100 ( 100   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   28 (  25   :;   0   =)
%            Number of variables   :   40 (   1 sgn;   7   !;   2   ?;  31   ^)
%                                         (  40   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU_NAR

% Comments : This is an ILTP problem embedded in TH0
%          : In classical logic this is a Theorem.
%------------------------------------------------------------------------------
include('Axioms/LCL010^0.ax').
%------------------------------------------------------------------------------
thf(f_type,type,(
    f: $i > $o )).

thf(p1_type,type,(
    p1: $i > $o )).

thf(p2_type,type,(
    p2: $i > $o )).

thf(p3_type,type,(
    p3: $i > $o )).

thf(p4_type,type,(
    p4: $i > $o )).

thf(axiom1,axiom,
    ( ivalid @ ( iimplies @ ( ior @ ( iand @ ( iatom @ p1 ) @ ( iand @ ( iatom @ p2 ) @ ( iand @ ( iatom @ p3 ) @ ( iatom @ p4 ) ) ) ) @ ( ior @ ( iimplies @ ( inot @ ( inot @ ( iatom @ p1 ) ) ) @ ( iatom @ f ) ) @ ( ior @ ( iimplies @ ( iatom @ p2 ) @ ( iatom @ f ) ) @ ( ior @ ( iimplies @ ( iatom @ p3 ) @ ( iatom @ f ) ) @ ( iimplies @ ( iatom @ p4 ) @ ( iatom @ f ) ) ) ) ) ) @ ( iatom @ f ) ) )).

thf(con,conjecture,
    ( ivalid @ ( iatom @ f ) )).

%------------------------------------------------------------------------------
