%------------------------------------------------------------------------------
% File     : SYO092^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem Y2141
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0200 [Bro09]
%          : Y2141 [TPS]

% Status   : Theorem
% Rating   : 0.12 v6.4.0, 0.14 v6.3.0, 0.17 v6.0.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    3 (   0 unit;   2 type;   0 defn)
%            Number of atoms       :   10 (   0 equality;   5 variable)
%            Maximal formula depth :    7 (   4 average)
%            Number of connectives :    9 (   0   ~;   1   |;   2   &;   5   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   2   :;   0   =)
%            Number of variables   :    3 (   0 sgn;   1   !;   2   ?;   0   ^)
%                                         (   3   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
% Proposed additional exercise
thf(cQ,type,(
    cQ: $i > $o )).

thf(cP,type,(
    cP: $i > $o )).

thf(cY2141,conjecture,
    ( ! [Xx: $i] :
      ? [Xy: $i] :
        ( ( cP @ Xx )
        & ( ( cQ @ Xy )
          | ( cQ @ Xx ) ) )
   => ? [Xz: $i] :
        ( ( cP @ Xz )
        & ( cQ @ Xz ) ) )).

%------------------------------------------------------------------------------
