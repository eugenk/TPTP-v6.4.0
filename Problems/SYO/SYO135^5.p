%------------------------------------------------------------------------------
% File     : SYO135^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-FO-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0717 [Bro09]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.17 v6.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    6 (   0 unit;   5 type;   0 defn)
%            Number of atoms       :   10 (   0 equality;   1 variable)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    9 (   0   ~;   2   |;   1   &;   5   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    7 (   5   :;   0   =)
%            Number of variables   :    1 (   0 sgn;   1   !;   0   ?;   0   ^)
%                                         (   1   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(x,type,(
    x: $i )).

thf(cQ,type,(
    cQ: $i > $o )).

thf(b,type,(
    b: $i )).

thf(cP,type,(
    cP: $i > $o )).

thf(a,type,(
    a: $i )).

thf(cDUP_BUG,conjecture,
    ( ( ! [Xx0: $i] :
          ( cP @ Xx0 )
      | ( cQ @ x ) )
   => ( ( ( cP @ a )
        & ( cP @ b ) )
      | ( cQ @ x ) ) )).

%------------------------------------------------------------------------------
