%------------------------------------------------------------------------------
% File     : SYO171^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-FO-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_1002 [Bro09]

% Status   : Theorem
% Rating   : 0.12 v6.4.0, 0.14 v6.3.0, 0.17 v6.0.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :   34 (   0 equality;  17 variable)
%            Maximal formula depth :   12 (   5 average)
%            Number of connectives :   38 (   5   ~;   5   |;   4   &;  24   @)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :;   0   =)
%            Number of variables   :    8 (   0 sgn;   8   !;   0   ?;   0   ^)
%                                         (   8   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a,type,(
    a: $i )).

thf(g,type,(
    g: $i > $i > $o )).

thf(f,type,(
    f: $i > $i )).

thf(cSYN031_1,conjecture,(
    ~ ( ! [A: $i] :
          ( ( g @ A @ a )
          | ( g @ ( f @ A ) @ A ) )
      & ! [A: $i] :
          ( ( g @ A @ a )
          | ( g @ A @ ( f @ A ) ) )
      & ! [A: $i,B: $i] :
          ( ~ ( g @ A @ B )
          | ( g @ ( f @ B ) @ B ) )
      & ! [A: $i,B: $i] :
          ( ~ ( g @ A @ B )
          | ( g @ B @ ( f @ B ) ) )
      & ! [A: $i,B: $i] :
          ( ~ ( g @ A @ B )
          | ~ ( g @ B @ a ) ) ) )).

%------------------------------------------------------------------------------
