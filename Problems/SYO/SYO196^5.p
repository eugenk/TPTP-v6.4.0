%------------------------------------------------------------------------------
% File     : SYO196^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem CT21
% Version  : Especial.
% English  : 

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0028 [Bro09]
%          : CT21 [TPS]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.33 v6.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :    3 (   0 equality;   3 variable)
%            Maximal formula depth :    6 (   6 average)
%            Number of connectives :    3 (   1   ~;   0   |;   1   &;   1   @)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    1 (   1   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    1 (   0   :;   0   =)
%            Number of variables   :    2 (   0 sgn;   1   !;   1   ?;   0   ^)
%                                         (   2   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cCT21,conjecture,(
    ? [Xp: $o > $o] :
      ( Xp
      @ ! [Xy: $o] :
          ( Xy
          & ~ ( Xy ) ) ) )).

%------------------------------------------------------------------------------
