%------------------------------------------------------------------------------
% File     : SYO239^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-HO-EQ-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0856 [Bro09]

% Status   : Theorem
% Rating   : 0.71 v6.4.0, 0.67 v6.3.0, 0.60 v6.2.0, 0.86 v6.1.0, 0.71 v6.0.0, 0.86 v5.5.0, 0.83 v5.4.0, 0.80 v5.3.0, 1.00 v5.2.0, 0.80 v4.1.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :   16 (   2 equality;   8 variable)
%            Maximal formula depth :    9 (   4 average)
%            Number of connectives :   13 (   2   ~;   0   |;   0   &;   8   @)
%                                         (   2 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :;   0   =)
%            Number of variables   :    5 (   0 sgn;   2   !;   3   ?;   0   ^)
%                                         (   5   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(s,type,(
    s: $i > $i )).

thf(c2,type,(
    c2: $i )).

thf(c_star,type,(
    c_star: $i > $i > $i )).

thf(cBLEDSOE_FENG_SV_EO1_W_LEM,conjecture,
    ( ! [Xx: $i] :
        ( ? [Xu: $i] :
            ( Xx
            = ( c_star @ c2 @ Xu ) )
      <=> ~ ( ? [Xv: $i] :
                ( ( s @ Xx )
                = ( c_star @ c2 @ Xv ) ) ) )
   => ? [A: $i > $o] :
      ! [Xx: $i] :
        ( ( A @ Xx )
      <=> ~ ( A @ ( s @ Xx ) ) ) )).

%------------------------------------------------------------------------------
