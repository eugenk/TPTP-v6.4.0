%------------------------------------------------------------------------------
% File     : SYO240^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-HO-EQ-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0877 [Bro09]

% Status   : Unknown
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :   18 (   1 equality;  14 variable)
%            Maximal formula depth :   11 (   5 average)
%            Number of connectives :   15 (   0   ~;   0   |;   0   &;  11   @)
%                                         (   0 <=>;   4  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   11 (  11   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :;   0   =)
%            Number of variables   :    7 (   0 sgn;   6   !;   1   ?;   0   ^)
%                                         (   7   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_UNK_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(b_type,type,(
    b: $tType )).

thf(cK,type,(
    cK: ( a > b > $o ) > a > b > $o )).

thf(cTHM2B,conjecture,
    ( ! [Xu: a > b > $o,Xv: a > b > $o] :
        ( ! [Xx: a,Xy: b] :
            ( ( Xu @ Xx @ Xy )
           => ( Xv @ Xx @ Xy ) )
       => ! [Xx: a,Xy: b] :
            ( ( cK @ Xu @ Xx @ Xy )
           => ( cK @ Xv @ Xx @ Xy ) ) )
   => ? [Xu: a > b > $o] :
        ( ( cK @ Xu )
        = Xu ) )).

%------------------------------------------------------------------------------
