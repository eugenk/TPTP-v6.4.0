%------------------------------------------------------------------------------
% File     : SYO244^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-HO-EQ-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0994 [Bro09]

% Status   : Theorem
% Rating   : 0.57 v6.4.0, 0.67 v6.3.0, 0.60 v6.2.0, 0.86 v6.1.0, 0.71 v5.5.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type;   0 defn)
%            Number of atoms       :   26 (   1 equality;  25 variable)
%            Maximal formula depth :   12 (   7 average)
%            Number of connectives :   23 (   0   ~;   0   |;   3   &;  14   @)
%                                         (   0 <=>;   6  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   24 (  24   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :;   0   =)
%            Number of variables   :   13 (   0 sgn;   5   !;   6   ?;   2   ^)
%                                         (  13   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cTHM535B,conjecture,
    ( ? [Xf: ( ( a > $o ) > $o ) > a > $o] :
      ! [X: ( a > $o ) > $o] :
        ( ? [Xt: a > $o] :
            ( X @ Xt )
       => ( X @ ( Xf @ X ) ) )
   => ! [A: ( ( a > $o ) > $o ) > $o] :
        ( ( ? [X: ( a > $o ) > $o] :
              ( A @ X )
          & ! [X: ( a > $o ) > $o] :
              ( ( A @ X )
             => ? [Xu: a > $o] :
                  ( X @ Xu ) ) )
       => ( ( ^ [Xx: a] :
              ! [Xa: ( a > $o ) > $o] :
                ( ( A @ Xa )
               => ? [Xb: a > $o] :
                    ( ( Xa @ Xb )
                    & ( Xb @ Xx ) ) ) )
          = ( ^ [Xx: a] :
              ? [Xf: ( ( a > $o ) > $o ) > a > $o] :
              ! [Xa: ( a > $o ) > $o] :
                ( ( A @ Xa )
               => ( ( Xa @ ( Xf @ Xa ) )
                  & ( Xf @ Xa @ Xx ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
