%------------------------------------------------------------------------------
% File     : SYO300^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-HO-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0770 [Bro09]

% Status   : Theorem
% Rating   : 0.25 v6.4.0, 0.29 v6.3.0, 0.33 v6.0.0, 0.17 v5.5.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :    8 (   0 equality;   8 variable)
%            Maximal formula depth :   10 (  10 average)
%            Number of connectives :    7 (   0   ~;   0   |;   0   &;   6   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   11 (  11   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    2 (   0   :;   0   =)
%            Number of variables   :    5 (   0 sgn;   2   !;   1   ?;   2   ^)
%                                         (   5   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cUNIFTHM1,conjecture,(
    ! [P: ( ( $i > $i ) > $i ) > ( ( $i > $i ) > $i ) > $o] :
    ? [X: ( $i > $i ) > $i] :
      ( ! [Xz: ( $i > $i ) > $i] :
          ( P @ Xz @ Xz )
     => ( P
        @ ^ [U: $i > $i] :
            ( U
            @ ( X
              @ ^ [V: $i] : V ) )
        @ X ) ) )).

%------------------------------------------------------------------------------
