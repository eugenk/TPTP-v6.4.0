%------------------------------------------------------------------------------
% File     : SYO311^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-HO-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0845 [Bro09]

% Status   : Theorem
% Rating   : 0.25 v6.4.0, 0.29 v6.3.0, 0.33 v6.2.0, 0.50 v6.1.0, 0.33 v6.0.0, 0.17 v5.5.0, 0.00 v5.4.0, 0.25 v5.3.0, 0.50 v5.1.0, 0.75 v4.1.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :   17 (   0 equality;   6 variable)
%            Maximal formula depth :    9 (   9 average)
%            Number of connectives :   19 (   3   ~;   0   |;   4   &;  10   @)
%                                         (   1 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   0   :;   0   =)
%            Number of variables   :    2 (   0 sgn;   1   !;   1   ?;   0   ^)
%                                         (   2   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cOMEGA_EXAMPLE2,conjecture,
    ( ? [Xop: $o > $o > $o] :
        ( ( Xop @ $true @ $true )
        & ~ ( Xop @ $false @ $false )
        & ~ ( Xop @ $false @ $true )
        & ~ ( Xop @ $true @ $false ) )
    & ! [Xm: $o > $o] :
        ( ( Xm @ $true )
      <=> ( Xm
          @ ( $true
           => $true ) ) ) )).

%------------------------------------------------------------------------------
