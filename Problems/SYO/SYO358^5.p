%------------------------------------------------------------------------------
% File     : SYO358^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem E2LEIBEQ1
% Version  : Especial.
% English  : Example from [Ben99] about alternative defns of equality.

% Refs     : [Ben99] Benzmueller (1999), Equality and Extensionality in Hig
%          : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0222 [Bro09]
%          : E2LEIBEQ1 [TPS]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.17 v6.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    6 (   0 unit;   5 type;   0 defn)
%            Number of atoms       :   12 (   0 equality;   4 variable)
%            Maximal formula depth :    7 (   3 average)
%            Number of connectives :   13 (   2   ~;   2   |;   2   &;   4   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    7 (   5   :;   0   =)
%            Number of variables   :    2 (   0 sgn;   2   !;   0   ?;   0   ^)
%                                         (   2   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    atype: $tType )).

thf(a,type,(
    a: $o )).

thf(v,type,(
    v: atype )).

thf(b,type,(
    b: $o )).

thf(u,type,(
    u: atype )).

thf(cE2LEIBEQ1_pme,conjecture,
    ( ! [Xq: atype > $o] :
        ( ( Xq @ u )
       => ( Xq @ v ) )
   => ! [P: atype > $o] :
        ( ( ( a
            | ~ ( a ) )
          & ( P @ u ) )
       => ( ( b
            | ~ ( b ) )
          & ( P @ v ) ) ) )).

%------------------------------------------------------------------------------
