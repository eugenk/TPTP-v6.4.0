%------------------------------------------------------------------------------
% File     : SYO361^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem THM47
% Version  : Especial.
% English  : Shows equivalence of two definitions of =.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0406 [Bro09]
%          : THM47 [TPS]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.29 v6.1.0, 0.14 v6.0.0, 0.29 v5.5.0, 0.33 v5.4.0, 0.40 v4.1.0, 0.33 v4.0.1, 0.67 v4.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :    9 (   1 equality;   8 variable)
%            Maximal formula depth :    9 (   9 average)
%            Number of connectives :    6 (   0   ~;   0   |;   0   &;   4   @)
%                                         (   1 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    2 (   0   :;   0   =)
%            Number of variables   :    4 (   0 sgn;   4   !;   0   ?;   0   ^)
%                                         (   4   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cTHM47,conjecture,(
    ! [X: $i,Y: $i] :
      ( ( X = Y )
    <=> ! [R: $i > $i > $o] :
          ( ! [Z: $i] :
              ( R @ Z @ Z )
         => ( R @ X @ Y ) ) ) )).

%------------------------------------------------------------------------------
