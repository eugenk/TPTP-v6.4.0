%------------------------------------------------------------------------------
% File     : SYO374^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from EXTENSIONALITY
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0758 [Bro09]

% Status   : Theorem
% Rating   : 0.25 v6.4.0, 0.29 v6.3.0, 0.33 v6.2.0, 0.50 v6.1.0, 0.33 v5.5.0, 0.40 v5.4.0, 0.75 v4.1.0, 0.67 v4.0.1, 1.00 v4.0.0
% Syntax   : Number of formulae    :    3 (   0 unit;   2 type;   0 defn)
%            Number of atoms       :   10 (   0 equality;   6 variable)
%            Maximal formula depth :    7 (   4 average)
%            Number of connectives :    9 (   0   ~;   0   |;   0   &;   6   @)
%                                         (   2 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    5 (   5   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   2   :;   0   =)
%            Number of variables   :    3 (   0 sgn;   3   !;   0   ?;   0   ^)
%                                         (   3   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cN,type,(
    cN: $i > $i )).

thf(cM,type,(
    cM: $i > $i )).

thf(cEXT_A_LEIB2,conjecture,
    ( ! [Xx: $i,Xp: $i > $o] :
        ( ( Xp @ ( cM @ Xx ) )
      <=> ( Xp @ ( cN @ Xx ) ) )
   => ! [Xq: ( $i > $i ) > $o] :
        ( ( Xq @ cM )
      <=> ( Xq @ cN ) ) )).

%------------------------------------------------------------------------------
