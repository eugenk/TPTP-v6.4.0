%------------------------------------------------------------------------------
% File     : SYO379^5 : TPTP v6.4.0. Bugfixed v5.2.0.
% Domain   : Syntactic
% Problem  : TPS problem from QUANTDEPTH-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0590 [Bro09]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.40 v5.2.0
% Syntax   : Number of formulae    :    6 (   0 unit;   3 type;   2 defn)
%            Number of atoms       :   14 (   4 equality;   5 variable)
%            Maximal formula depth :    6 (   4 average)
%            Number of connectives :    3 (   0   ~;   0   |;   1   &;   2   @)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    5 (   5   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :;   0   =)
%            Number of variables   :    4 (   0 sgn;   0   !;   2   ?;   2   ^)
%                                         (   4   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
% Bugfixes : v5.2.0 - Added missing type declarations.
%------------------------------------------------------------------------------
thf(c_type,type,(
    c: $i )).

thf(cQDP0_type,type,(
    cQDP0: $i > $o )).

thf(cQDP1_type,type,(
    cQDP1: ( $i > $o ) > $o )).

thf(cQDP0_def,definition,
    ( cQDP0
    = ( ^ [Xz: $i] : ( Xz = c ) ) )).

thf(cQDP1_def,definition,
    ( cQDP1
    = ( ^ [Xz: $i > $o] :
          ( ( Xz = cQDP0 )
          & ? [Xt: $i] :
              ( Xz @ Xt ) ) ) )).

thf(cQDTHM1,conjecture,(
    ? [Xs: $i > $o] :
      ( cQDP1 @ Xs ) )).

%------------------------------------------------------------------------------
