%------------------------------------------------------------------------------
% File     : SYO382^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem THM407
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0165 [Bro09]
%          : THM407 [TPS]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.17 v6.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :   32 (   0 equality;  20 variable)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :   35 (   4   ~;   0   |;   4   &;  20   @)
%                                         (   0 <=>;   7  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :    5 (   5   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :;   0   =)
%            Number of variables   :   14 (   0 sgn;   8   !;   6   ?;   0   ^)
%                                         (  14   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ_NAR

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cS,type,(
    cS: $i > $o )).

thf(cQ,type,(
    cQ: $i > $i > $o )).

thf(cP,type,(
    cP: $i > $i > $o )).

thf(cTHM407,conjecture,
    ( ( ( ? [Xv: $i] :
          ! [Xx: $i] :
            ( cP @ Xx @ Xv )
        & ! [Xx: $i] :
            ( ( cS @ Xx )
           => ? [Xy: $i] :
                ( cQ @ Xy @ Xx ) )
        & ! [Xx: $i,Xy: $i] :
            ( ( cP @ Xx @ Xy )
           => ~ ( cQ @ Xx @ Xy ) ) )
     => ? [Xu: $i] :
          ~ ( cS @ Xu ) )
   => ( ( ? [Xv: $i] :
          ! [Xx: $i] :
            ( cP @ Xx @ Xv )
        & ! [Xx: $i] :
            ( ( cS @ Xx )
           => ? [Xy: $i] :
                ( cQ @ Xy @ Xx ) )
        & ! [Xx: $i,Xy: $i] :
            ( ( cP @ Xx @ Xy )
           => ~ ( cQ @ Xx @ Xy ) ) )
     => ? [Xu: $i] :
          ~ ( cS @ Xu ) ) )).

%------------------------------------------------------------------------------
