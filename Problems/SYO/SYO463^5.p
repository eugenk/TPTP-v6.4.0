%------------------------------------------------------------------------------
% File     : SYO463^5 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : Ted Sider's propositional modal logic wff 14
% Version  : Especial.
%          : Theorem formulation : Uses system S4 axioms.
% English  :

% Refs     : [Sid09] Sider (2009), Logic for Philosophy
% Source   : [Sid09]
% Names    :

% Status   : Theorem
% Rating   : 0.14 v6.4.0, 0.17 v6.3.0, 0.20 v6.2.0, 0.29 v6.1.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.33 v4.0.0
% Syntax   : Number of formulae    :   73 (   0 unit;  37 type;  33 defn)
%            Number of atoms       :  251 (  38 equality; 135 variable)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :  144 (   5   ~;   5   |;   8   &; 118   @)
%                                         (   0 <=>;   8  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  180 ( 180   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   41 (  37   :;   0   =)
%            Number of variables   :   88 (   3 sgn;  30   !;   6   ?;  52   ^)
%                                         (  88   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include axioms for Modal logic S4
include('Axioms/LCL013^0.ax').
include('Axioms/LCL013^5.ax').
%------------------------------------------------------------------------------
thf(p_type,type,(
    p: $i > $o )).

thf(q_type,type,(
    q: $i > $o )).

thf(prove,conjecture,
    ( mvalid @ ( mimplies @ ( mbox_s4 @ ( mequiv @ p @ q ) ) @ ( mbox_s4 @ ( mequiv @ ( mbox_s4 @ p ) @ ( mbox_s4 @ q ) ) ) ) )).
%------------------------------------------------------------------------------
