%------------------------------------------------------------------------------
% File     : SYO467^2 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : Ted Sider's propositional modal logic wff 18
% Version  : Especial.
%          : Theorem formulation : Uses system D axioms.
% English  :

% Refs     : [Sid09] Sider (2009), Logic for Philosophy
% Source   : [Sid09]
% Names    :

% Status   : CounterSatisfiable
% Rating   : 0.67 v6.2.0, 0.33 v5.0.0, 0.67 v4.1.0, 0.50 v4.0.0
% Syntax   : Number of formulae    :   71 (   0 unit;  36 type;  33 defn)
%            Number of atoms       :  245 (  38 equality; 135 variable)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :  139 (   5   ~;   5   |;   8   &; 113   @)
%                                         (   0 <=>;   8  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  179 ( 179   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   40 (  36   :;   0   =)
%            Number of variables   :   88 (   3 sgn;  30   !;   6   ?;  52   ^)
%                                         (  88   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include axioms for Modal logic D
include('Axioms/LCL013^0.ax').
include('Axioms/LCL013^2.ax').
%------------------------------------------------------------------------------
thf(p_type,type,(
    p: $i > $o )).

thf(prove,conjecture,
    ( mvalid @ ( mequiv @ ( mdia_d @ ( mbox_d @ p ) ) @ ( mbox_d @ ( mdia_d @ p ) ) ) )).
%------------------------------------------------------------------------------
