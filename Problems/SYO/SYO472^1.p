%------------------------------------------------------------------------------
% File     : SYO472^1 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : Ted Sider's propositional modal logic wff 23
% Version  : Especial.
%          : Theorem formulation : Uses system K axioms.
% English  :

% Refs     : [Sid09] Sider (2009), Logic for Philosophy
% Source   : [Sid09]
% Names    :

% Status   : CounterSatisfiable
% Rating   : 0.33 v6.4.0, 0.00 v6.3.0, 0.33 v5.4.0, 0.00 v5.0.0, 0.33 v4.1.0, 0.50 v4.0.0
% Syntax   : Number of formulae    :   72 (   0 unit;  38 type;  33 defn)
%            Number of atoms       :  250 (  38 equality; 135 variable)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :  145 (   5   ~;   5   |;   8   &; 119   @)
%                                         (   0 <=>;   8  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  181 ( 181   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   42 (  38   :;   0   =)
%            Number of variables   :   88 (   3 sgn;  30   !;   6   ?;  52   ^)
%                                         (  88   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include axioms for Modal logic K
include('Axioms/LCL013^0.ax').
include('Axioms/LCL013^1.ax').
%------------------------------------------------------------------------------
thf(p_type,type,(
    p: $i > $o )).

thf(q_type,type,(
    q: $i > $o )).

thf(r_type,type,(
    r: $i > $o )).

thf(prove,conjecture,
    ( mvalid @ ( mimplies @ ( mbox_k @ ( mimplies @ p @ ( mbox_k @ ( mimplies @ q @ r ) ) ) ) @ ( mimplies @ q @ ( mbox_k @ ( mimplies @ p @ r ) ) ) ) )).
%------------------------------------------------------------------------------
