%------------------------------------------------------------------------------
% File     : SYO486^6 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : Ted Sider's S5 quantified modal logic wff 12
% Version  : Especial.
% English  :

% Refs     : [Sid09] Sider (2009), Logic for Philosophy
% Source   : [Sid09]
% Names    :

% Status   : CounterSatisfiable
% Rating   : 0.33 v6.2.0, 0.00 v5.4.0, 0.67 v5.0.0, 0.33 v4.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :   73 (   0 unit;  36 type;  33 defn)
%            Number of atoms       :  251 (  38 equality; 137 variable)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :  143 (   5   ~;   5   |;   8   &; 117   @)
%                                         (   0 <=>;   8  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  180 ( 180   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   40 (  36   :;   0   =)
%            Number of variables   :   90 (   3 sgn;  30   !;   6   ?;  54   ^)
%                                         (  90   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include axioms for modal logic S5
include('Axioms/LCL013^0.ax').
include('Axioms/LCL013^6.ax').
%------------------------------------------------------------------------------
thf(f_type,type,(
    f: mu > $i > $o )).

thf(prove,conjecture,
    ( mvalid
    @ ( mimplies
      @ ( mbox_s5
        @ ( mexists_ind
          @ ^ [X: mu] :
              ( f @ X ) ) )
      @ ( mdia_s5
        @ ( mforall_ind
          @ ^ [X: mu] :
              ( f @ X ) ) ) ) )).

%------------------------------------------------------------------------------
