%------------------------------------------------------------------------------
% File     : SYO489^6 : TPTP v6.4.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : Ted Sider's S5 quantified modal logic wff 15
% Version  : Especial.
% English  :

% Refs     : [Sid09] Sider (2009), Logic for Philosophy
% Source   : [Sid09]
% Names    :

% Status   : CounterSatisfiable
% Rating   : 0.33 v6.2.0, 0.00 v5.4.0, 0.67 v5.0.0, 0.33 v4.1.0, 0.50 v4.0.0
% Syntax   : Number of formulae    :   75 (   0 unit;  38 type;  33 defn)
%            Number of atoms       :  256 (  38 equality; 135 variable)
%            Maximal formula depth :   11 (   6 average)
%            Number of connectives :  148 (   5   ~;   5   |;   8   &; 122   @)
%                                         (   0 <=>;   8  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :  182 ( 182   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   42 (  38   :;   0   =)
%            Number of variables   :   88 (   3 sgn;  30   !;   6   ?;  52   ^)
%                                         (  88   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
%----Include axioms for modal logic S5
include('Axioms/LCL013^0.ax').
include('Axioms/LCL013^6.ax').
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: mu )).

thf(f_type,type,(
    f: mu > $i > $o )).

thf(g_type,type,(
    g: mu > $i > $o )).

thf(prove,conjecture,
    ( mvalid @ ( mimplies @ ( mand @ ( mdia_s5 @ ( f @ a ) ) @ ( mdia_s5 @ ( g @ a ) ) ) @ ( mdia_s5 @ ( mand @ ( f @ a ) @ ( g @ a ) ) ) ) )).

%------------------------------------------------------------------------------
