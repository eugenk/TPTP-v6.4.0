%------------------------------------------------------------------------------
% File     : SYO542^1 : TPTP v6.4.0. Released v5.2.0.
% Domain   : Syntactic
% Problem  : If-then-else on $i>$i defined from choice on $i>$i
% Version  : Especial.
% English  : A choice operator on ($i>$i) is used to define an if-then-else 
%            operator at ($i>$i). Check that it always returns the then-part 
%            or the else-part.

% Refs     : [Bro11] Brown E. (2011), Email to Geoff Sutcliffe
% Source   : [Bro11]
% Names    : CHOICE16b [Bro11]

% Status   : Theorem
% Rating   : 0.29 v6.4.0, 0.33 v6.3.0, 0.40 v6.2.0, 0.57 v6.1.0, 0.43 v5.5.0, 0.33 v5.4.0, 0.40 v5.2.0
% Syntax   : Number of formulae    :    5 (   0 unit;   2 type;   1 defn)
%            Number of atoms       :   28 (   5 equality;  18 variable)
%            Maximal formula depth :   10 (   7 average)
%            Number of connectives :   16 (   1   ~;   2   |;   2   &;  10   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&)
%            Number of type conns  :   18 (  18   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   2   :;   0   =)
%            Number of variables   :    9 (   0 sgn;   4   !;   1   ?;   4   ^)
%                                         (   9   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU_NAR

% Comments : 
%------------------------------------------------------------------------------
thf(epsii,type,(
    epsii: ( ( $i > $i ) > $o ) > $i > $i )).

thf(choiceaxii,axiom,(
    ! [P: ( $i > $i ) > $o] :
      ( ? [X: $i > $i] :
          ( P @ X )
     => ( P @ ( epsii @ P ) ) ) )).

thf(if,type,(
    if: $o > ( $i > $i ) > ( $i > $i ) > $i > $i )).

thf(ifd,definition,
    ( if
    = ( ^ [B: $o,X: $i > $i,Y: $i > $i] :
          ( epsii
          @ ^ [Z: $i > $i] :
              ( ( B
                & ( Z = X ) )
              | ( ~ ( B )
                & ( Z = Y ) ) ) ) ) )).

thf(conj,conjecture,(
    ! [B: $o,X: $i > $i,Y: $i > $i] :
      ( ( ( if @ B @ X @ Y )
        = X )
      | ( ( if @ B @ X @ Y )
        = Y ) ) )).

%------------------------------------------------------------------------------
